-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 07, 2022 at 10:53 AM
-- Server version: 5.7.27-30
-- PHP Version: 7.3.29-buster

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thegiftdev`
--

-- --------------------------------------------------------

--
-- Table structure for table `log_item`
--

CREATE TABLE `log_item` (
  `log_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `action` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_json` longtext COLLATE utf8mb4_unicode_ci,
  `updated_json` longtext COLLATE utf8mb4_unicode_ci,
  `updated_by_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `log_location_item`
--

CREATE TABLE `log_location_item` (
  `log_id` int(11) NOT NULL,
  `map_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `action` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `log_product_item_content`
--

CREATE TABLE `log_product_item_content` (
  `log_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `action` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_json` longtext COLLATE utf8mb4_unicode_ci,
  `updated_json` longtext COLLATE utf8mb4_unicode_ci,
  `updated_by_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_addon`
--

CREATE TABLE `tbl_addon` (
  `addon_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inhouse_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inhouse_name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gift_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_addon_group`
--

CREATE TABLE `tbl_addon_group` (
  `addon_group_id` int(11) NOT NULL,
  `inhouse_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inhouse_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login_on` datetime DEFAULT NULL,
  `last_login_ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login_device` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login_location` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suspended` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `default_shop_id` int(11) DEFAULT '1',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin_role`
--

CREATE TABLE `tbl_admin_role` (
  `role_id` int(11) NOT NULL,
  `adm_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_right` longtext COLLATE utf8mb4_unicode_ci,
  `zuri_right` longtext COLLATE utf8mb4_unicode_ci,
  `ltp_right` longtext COLLATE utf8mb4_unicode_ci,
  `mobile_right` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `driver_right` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `warehouse_right` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `packing_right` varchar(1) CHARACTER SET armscii8 COLLATE armscii8_bin DEFAULT 'N',
  `compose_right` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `stocktake_approver_right` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_alert`
--

CREATE TABLE `tbl_alert` (
  `alert_id` int(11) NOT NULL,
  `type` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_id` longtext COLLATE utf8mb4_unicode_ci,
  `alert_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `alert_status` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT '10_reminding',
  `create_time` datetime DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  `last_update_time` datetime DEFAULT NULL,
  `last_update_by` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_alert_admin`
--

CREATE TABLE `tbl_alert_admin` (
  `map_id` int(11) NOT NULL,
  `alert_id` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `ignore_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  `last_update_time` datetime DEFAULT NULL,
  `last_update_by` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_car`
--

CREATE TABLE `tbl_car` (
  `car_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci,
  `create_time` datetime DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  `last_update_time` datetime DEFAULT NULL,
  `last_update_by` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cate`
--

CREATE TABLE `tbl_cate` (
  `cate_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `parent_cate_id` int(11) DEFAULT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_tc` longtext COLLATE utf8mb4_unicode_ci,
  `description_en` longtext COLLATE utf8mb4_unicode_ci,
  `background_color` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cate_banner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `show_in_web` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_client`
--

CREATE TABLE `tbl_client` (
  `client_id` int(11) NOT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `image_path` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company_discount`
--

CREATE TABLE `tbl_company_discount` (
  `discount_id` int(11) NOT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit` int(11) DEFAULT NULL,
  `quota` int(11) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company_discount_applicant`
--

CREATE TABLE `tbl_company_discount_applicant` (
  `applicant_id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_coupon`
--

CREATE TABLE `tbl_coupon` (
  `coupon_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `adm_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` int(11) NOT NULL,
  `type` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `minimum_order_amt` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `allow_voucher` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `member_only` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `usage_limit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `sales_admin_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deli_cust_grp`
--

CREATE TABLE `tbl_deli_cust_grp` (
  `grp_id` int(11) NOT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `adm_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deli_cust_region`
--

CREATE TABLE `tbl_deli_cust_region` (
  `cust_id` int(11) NOT NULL,
  `grp_id` int(11) DEFAULT NULL,
  `region_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_fee` int(11) DEFAULT NULL,
  `address_required` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_in_web` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deli_cust_timeslot`
--

CREATE TABLE `tbl_deli_cust_timeslot` (
  `cust_id` int(11) NOT NULL,
  `grp_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `slot_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ava_b4_day` int(11) DEFAULT NULL,
  `ava_b4_cutoff` time DEFAULT NULL,
  `deli_fee` int(11) DEFAULT NULL,
  `online_pay_same_day` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `same_day_deli_fee` int(11) DEFAULT NULL,
  `show_in_web` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deli_district`
--

CREATE TABLE `tbl_deli_district` (
  `district_id` int(11) NOT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_in_web` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `sort` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deli_group`
--

CREATE TABLE `tbl_deli_group` (
  `deli_group_id` int(11) NOT NULL,
  `name_tc` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deli_group_region`
--

CREATE TABLE `tbl_deli_group_region` (
  `deli_group_region_id` int(11) NOT NULL,
  `deli_group_id` int(11) NOT NULL,
  `shipping_fee` int(11) DEFAULT NULL,
  `address_required` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_in_web` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deli_group_region_timeslot`
--

CREATE TABLE `tbl_deli_group_region_timeslot` (
  `map_id` int(11) NOT NULL,
  `deli_group_region_id` int(11) DEFAULT NULL,
  `slot_id` int(11) DEFAULT NULL,
  `ava_b4_day` int(11) DEFAULT NULL,
  `ava_b4_cutoff` time DEFAULT NULL,
  `deli_fee` int(11) DEFAULT NULL,
  `same_day_deli_fee` int(11) DEFAULT NULL,
  `online_pay_same_day` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `show_in_web` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deli_group_timeslot`
--

CREATE TABLE `tbl_deli_group_timeslot` (
  `map_id` int(11) NOT NULL,
  `deli_group_id` int(11) DEFAULT NULL,
  `slot_id` int(11) DEFAULT NULL,
  `ava_b4_day` int(11) DEFAULT NULL,
  `ava_b4_cutoff` time DEFAULT NULL,
  `deli_fee` int(11) DEFAULT NULL,
  `same_day_deli_fee` int(11) DEFAULT NULL,
  `show_in_web` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `online_pay_same_day` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deli_item`
--

CREATE TABLE `tbl_deli_item` (
  `map_id` int(11) NOT NULL,
  `deli_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deli_region`
--

CREATE TABLE `tbl_deli_region` (
  `region_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_fee` int(11) DEFAULT '0',
  `show_in_web` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `address_required` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deli_timeslot`
--

CREATE TABLE `tbl_deli_timeslot` (
  `slot_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `adm_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `ava_b4_day` int(11) DEFAULT NULL,
  `ava_b4_cutoff` time DEFAULT NULL,
  `deli_fee` int(11) DEFAULT NULL,
  `same_day_deli_fee` int(11) DEFAULT NULL,
  `default` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `online_pay_same_day` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `cutoff_time` time DEFAULT NULL,
  `cutoff_day` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `show_in_web` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_driver_schedule`
--

CREATE TABLE `tbl_driver_schedule` (
  `driver_schedule_id` int(11) NOT NULL,
  `driver_admin_id` int(11) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci,
  `create_time` datetime DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  `last_update_time` datetime DEFAULT NULL,
  `last_update_by` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email`
--

CREATE TABLE `tbl_email` (
  `email_id` int(11) NOT NULL,
  `to_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `tbl` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email_tpl`
--

CREATE TABLE `tbl_email_tpl` (
  `tpl_id` int(11) NOT NULL,
  `adm_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gift`
--

CREATE TABLE `tbl_gift` (
  `gift_id` int(11) NOT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `required_point` int(11) DEFAULT NULL,
  `quota` int(11) DEFAULT NULL,
  `gift_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gift_card`
--

CREATE TABLE `tbl_gift_card` (
  `card_id` int(11) NOT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit` int(11) DEFAULT '0',
  `expiry_date` datetime DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gift_card_key`
--

CREATE TABLE `tbl_gift_card_key` (
  `map_id` int(11) NOT NULL,
  `card_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `use_datetime` datetime DEFAULT NULL,
  `used_by_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gift_deli`
--

CREATE TABLE `tbl_gift_deli` (
  `deli_id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `gift_id` int(11) DEFAULT NULL,
  `deli_status` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_greeting`
--

CREATE TABLE `tbl_greeting` (
  `greeting_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `subcate_id` int(11) NOT NULL,
  `msg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_greeting_cate`
--

CREATE TABLE `tbl_greeting_cate` (
  `cate_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_greeting_group`
--

CREATE TABLE `tbl_greeting_group` (
  `greeting_group_id` int(11) NOT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_greeting_subcate`
--

CREATE TABLE `tbl_greeting_subcate` (
  `subcate_id` int(11) NOT NULL,
  `parent_cate_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_holiday`
--

CREATE TABLE `tbl_holiday` (
  `holiday_id` int(11) NOT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `adm_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `remark_tc` longtext COLLATE utf8mb4_unicode_ci,
  `remark_en` longtext COLLATE utf8mb4_unicode_ci,
  `quota_everyday` int(11) DEFAULT NULL,
  `isDayOff` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item`
--

CREATE TABLE `tbl_item` (
  `item_id` int(11) NOT NULL,
  `various_item_id` int(11) DEFAULT NULL,
  `item_cate_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `country_tc` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_en` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku_prefix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sku` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inhouse_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inhouse_name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier_name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pattern` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `gift_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplier_stock_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplier_barcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cost` decimal(7,2) DEFAULT NULL,
  `stockin_price` decimal(7,2) DEFAULT NULL,
  `suggested_price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alert_qty` int(11) DEFAULT '0',
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `item_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `ava_qty` int(11) DEFAULT '0',
  `reserved_qty` int(11) DEFAULT '0',
  `is_unlimited_qty` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `is_product_page_show_qty` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `replace_sku_id` int(11) DEFAULT NULL,
  `is_replace_period` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'F',
  `replace_period_start` timestamp NULL DEFAULT NULL,
  `replace_period_end` timestamp NULL DEFAULT NULL,
  `is_replace` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `various_activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_brand`
--

CREATE TABLE `tbl_item_brand` (
  `brand_id` int(11) NOT NULL,
  `brand_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_cate`
--

CREATE TABLE `tbl_item_cate` (
  `cate_id` int(11) NOT NULL,
  `parent_cate_id` int(11) DEFAULT NULL,
  `cate_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cate_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_cate_various_type`
--

CREATE TABLE `tbl_item_cate_various_type` (
  `type_id` int(11) NOT NULL,
  `cate_id` int(11) DEFAULT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_cate_various_val`
--

CREATE TABLE `tbl_item_cate_various_val` (
  `val_id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `val` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isHide` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_country`
--

CREATE TABLE `tbl_item_country` (
  `country_id` int(11) NOT NULL,
  `country_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT '1',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_grp`
--

CREATE TABLE `tbl_item_grp` (
  `grp_id` int(11) NOT NULL,
  `adm_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inhouse_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inhouse_name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gift_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_tpl` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_grp_content`
--

CREATE TABLE `tbl_item_grp_content` (
  `content_id` int(11) NOT NULL,
  `grp_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT '1',
  `is_auto_replace` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `additional_price` int(11) DEFAULT '0',
  `is_client_optional` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `is_product_page_show_qty` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `sorting` int(11) DEFAULT '0',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_stock_in`
--

CREATE TABLE `tbl_item_stock_in` (
  `in_id` int(11) NOT NULL,
  `po_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `avg_price` decimal(7,2) NOT NULL,
  `barcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_stock_out`
--

CREATE TABLE `tbl_item_stock_out` (
  `out_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `ref_no` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_tag`
--

CREATE TABLE `tbl_item_tag` (
  `tag_id` int(11) NOT NULL,
  `parent_tag_id` int(11) DEFAULT NULL,
  `tag_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_location`
--

CREATE TABLE `tbl_location` (
  `location_id` int(11) NOT NULL,
  `adm_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member`
--

CREATE TABLE `tbl_member` (
  `member_id` int(11) NOT NULL,
  `session_id` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_id` int(11) NOT NULL,
  `member_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verify` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `sales_admin_id` int(11) DEFAULT NULL,
  `remark` longtext COLLATE utf8mb4_unicode_ci,
  `point` int(11) DEFAULT '0',
  `cash_dollar` int(11) DEFAULT '0',
  `exp` int(11) DEFAULT '0',
  `language` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT 'tc',
  `membership_id` int(11) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `facebook_id` int(11) DEFAULT NULL,
  `facebook_access_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valid_code` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pw_valid_code` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_charge_cash_dollar` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `is_monthly_payment` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_membership`
--

CREATE TABLE `tbl_membership` (
  `membership_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` int(11) DEFAULT '0',
  `upgrade_point` int(11) DEFAULT '0',
  `upgrade_to_membership_id` int(11) DEFAULT '0',
  `upgrade_period` int(11) DEFAULT '0',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member_fav`
--

CREATE TABLE `tbl_member_fav` (
  `fav_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member_voucher`
--

CREATE TABLE `tbl_member_voucher` (
  `member_voucher_id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `voucher_id` int(11) DEFAULT NULL,
  `redeemed` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `discount` int(11) DEFAULT '0',
  `type` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'A',
  `minimum_order_amt` int(11) DEFAULT '0',
  `ava_day` int(11) DEFAULT '0',
  `expiry_date` date DEFAULT NULL,
  `sales_admin_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nav`
--

CREATE TABLE `tbl_nav` (
  `nav_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `order_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `order_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `handle_time` datetime DEFAULT NULL,
  `handle_admin_id` int(11) DEFAULT NULL,
  `order_no` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `sales_admin_id` int(11) DEFAULT NULL,
  `sender_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_price` int(11) DEFAULT NULL,
  `shipping_fee` int(11) NOT NULL,
  `timeslot_fee` int(11) NOT NULL,
  `cash_dollar_used` int(11) DEFAULT '0',
  `coupon_discount` int(11) DEFAULT '0',
  `voucher_discount` int(11) DEFAULT '0',
  `membership_discount` int(11) DEFAULT '0',
  `coupon_code_id` int(11) DEFAULT NULL,
  `coupon_code` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_voucher_id` int(11) DEFAULT NULL,
  `final_price` int(11) NOT NULL,
  `point_gain` int(11) DEFAULT '0',
  `cash_dollar_gain` int(11) DEFAULT '0',
  `exp_gain` int(11) DEFAULT '0',
  `remarks_from_client` longtext COLLATE utf8mb4_unicode_ci,
  `remarks_internal` longtext COLLATE utf8mb4_unicode_ci,
  `remarks_external` longtext COLLATE utf8mb4_unicode_ci,
  `excel_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reserve_time` timestamp NULL DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `order_datetime` timestamp NULL DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_deli`
--

CREATE TABLE `tbl_order_deli` (
  `deli_id` int(11) NOT NULL,
  `deli_no` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `driver_admin_id` int(11) DEFAULT NULL,
  `composer_admin_id` int(11) DEFAULT NULL,
  `incharge_admin_id` int(11) DEFAULT NULL,
  `is_greeting_card_printed` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `is_compose_note_printed` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `is_receive_note_printed` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `is_blank_card_provide` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `is_client_card` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `deli_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `recipient_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recipient_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recipient_company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_id` int(11) DEFAULT NULL,
  `card_head` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_body` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_sign` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_json` longtext COLLATE utf8mb4_unicode_ci,
  `card_activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `card_completed` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `card_img_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deli_region_id` int(11) DEFAULT NULL,
  `deli_address` longtext COLLATE utf8mb4_unicode_ci,
  `deli_date` date DEFAULT NULL,
  `deli_timeslot_id` int(11) DEFAULT NULL,
  `deli_before_photo_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deli_after_photo_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acknowledgement` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_price` int(11) NOT NULL,
  `addon_price` int(11) NOT NULL,
  `shipping_fee` int(11) NOT NULL,
  `timeslot_fee` int(11) NOT NULL,
  `deli_price` int(11) NOT NULL,
  `remarks_internal` longtext COLLATE utf8mb4_unicode_ci,
  `remarks_external` longtext COLLATE utf8mb4_unicode_ci,
  `remarks_packing` longtext COLLATE utf8mb4_unicode_ci,
  `remarks_driver` longtext COLLATE utf8mb4_unicode_ci,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_deli_addon`
--

CREATE TABLE `tbl_order_deli_addon` (
  `map_id` int(11) NOT NULL,
  `deli_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_deli_item`
--

CREATE TABLE `tbl_order_deli_item` (
  `map_id` int(11) NOT NULL,
  `deli_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `packed_qty` int(11) NOT NULL,
  `isClientOption` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `isAddon` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_deposit_slip`
--

CREATE TABLE `tbl_order_deposit_slip` (
  `slip_id` int(11) NOT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `order_no` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `submitter_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `deposit_slip` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_confirmed` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_excel`
--

CREATE TABLE `tbl_order_excel` (
  `excel_id` int(11) NOT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `order_no` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `excel_path` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_confirmed` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_payment`
--

CREATE TABLE `tbl_order_payment` (
  `payment_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `payment_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bankslip_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `raw_return` longtext COLLATE utf8mb4_unicode_ci,
  `expected_payment_value` int(11) DEFAULT NULL,
  `actual_payment_value` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_page`
--

CREATE TABLE `tbl_page` (
  `page_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `editable` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `removeable` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `show_in_web` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `see_through_nav` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_page_content`
--

CREATE TABLE `tbl_page_content` (
  `content_id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html_tc` longtext COLLATE utf8mb4_unicode_ci,
  `html_en` longtext COLLATE utf8mb4_unicode_ci,
  `html2_tc` longtext COLLATE utf8mb4_unicode_ci,
  `html2_en` longtext COLLATE utf8mb4_unicode_ci,
  `html3_tc` longtext COLLATE utf8mb4_unicode_ci,
  `html3_en` longtext COLLATE utf8mb4_unicode_ci,
  `lr_img_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lr_img_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lr_img2_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lr_img2_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mini_banner_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mini_banner_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_popup`
--

CREATE TABLE `tbl_popup` (
  `popup_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `adm_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_tc` longtext COLLATE utf8mb4_unicode_ci,
  `content_en` longtext COLLATE utf8mb4_unicode_ci,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiry_datetime` timestamp NULL DEFAULT NULL,
  `bg_color` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_in_web` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `product_id` int(11) NOT NULL,
  `addon_group_id` int(11) DEFAULT NULL,
  `card_group_id` int(11) DEFAULT NULL,
  `deli_group_id` int(11) DEFAULT NULL,
  `greeting_group_id` int(11) DEFAULT NULL,
  `gift_product_tpl_id` int(11) DEFAULT NULL,
  `gift_product_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_product_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_product_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_deli_id` int(11) DEFAULT NULL,
  `gift_cust_deli_act` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `gift_cust_deli_tc` longtext COLLATE utf8mb4_unicode_ci,
  `gift_cust_deli_en` longtext COLLATE utf8mb4_unicode_ci,
  `gift_cust_info_act` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `gift_cust_info_tc` longtext COLLATE utf8mb4_unicode_ci,
  `gift_cust_info_en` longtext COLLATE utf8mb4_unicode_ci,
  `gift_price` int(11) DEFAULT NULL,
  `gift_cust_addinfo_act` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `gift_cust_addinfo_tc` longtext COLLATE utf8mb4_unicode_ci,
  `gift_cust_addinfo_en` longtext COLLATE utf8mb4_unicode_ci,
  `gift_sp_price` int(11) DEFAULT NULL,
  `gift_cutoff_tpl_id` int(11) DEFAULT NULL,
  `gift_ava_b4_day` int(11) DEFAULT NULL,
  `gift_ava_b4_cutoff` time DEFAULT NULL,
  `gift_sales_status` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `gift_show_in_web` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `gift_display_in_cate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `gift_deli_start_date` date DEFAULT NULL,
  `gift_is_force_checkout` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `gift_force_checkout_start_date` date DEFAULT NULL,
  `gift_force_checkout_end_date` date DEFAULT NULL,
  `gift_publish_date` date DEFAULT NULL,
  `gift_delivery_start_date` date DEFAULT NULL,
  `gift_delivery_end_date` date DEFAULT NULL,
  `gift_is_new` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `gift_product_tag_tc` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_product_tag_en` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_bottom_product_tag_tc` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_bottom_product_tag_en` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_bottom_product_tag_2_tc` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_bottom_product_tag_2_en` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_product_tpl_id` int(11) DEFAULT NULL,
  `zuri_product_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_product_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_product_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_deli_id` int(11) DEFAULT NULL,
  `zuri_cust_deli_act` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `zuri_cust_deli_tc` longtext COLLATE utf8mb4_unicode_ci,
  `zuri_cust_deli_en` longtext COLLATE utf8mb4_unicode_ci,
  `zuri_cust_info_act` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `zuri_cust_info_tc` longtext COLLATE utf8mb4_unicode_ci,
  `zuri_cust_info_en` longtext COLLATE utf8mb4_unicode_ci,
  `zuri_cust_addinfo_act` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `zuri_cust_addinfo_tc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `zuri_cust_addinfo_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `zuri_price` int(11) DEFAULT NULL,
  `zuri_sp_price` int(11) DEFAULT NULL,
  `zuri_cutoff_tpl_id` int(11) DEFAULT NULL,
  `zuri_ava_b4_day` int(11) DEFAULT NULL,
  `zuri_ava_b4_cutoff` time DEFAULT NULL,
  `zuri_onsale` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `zuri_sales_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_show_in_web` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `zuri_display_in_cate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `zuri_deli_start_date` date DEFAULT NULL,
  `zuri_is_force_checkout` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `zuri_force_checkout_start_date` date DEFAULT NULL,
  `zuri_force_checkout_end_date` date DEFAULT NULL,
  `zuri_publish_date` date DEFAULT NULL,
  `zuri_delivery_start_date` date DEFAULT NULL,
  `zuri_delivery_end_date` date DEFAULT NULL,
  `zuri_is_new` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `zuri_product_tag_tc` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_product_tag_en` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_bottom_product_tag_tc` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_bottom_product_tag_en` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_bottom_product_tag_2_tc` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_bottom_product_tag_2_en` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_product_tpl_id` int(11) DEFAULT NULL,
  `ltp_product_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_product_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_product_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_deli_id` int(11) DEFAULT NULL,
  `ltp_cust_deli_act` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `ltp_cust_deli_tc` longtext COLLATE utf8mb4_unicode_ci,
  `ltp_cust_deli_en` longtext COLLATE utf8mb4_unicode_ci,
  `ltp_cust_info_act` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `ltp_cust_info_tc` longtext COLLATE utf8mb4_unicode_ci,
  `ltp_cust_info_en` longtext COLLATE utf8mb4_unicode_ci,
  `ltp_cust_addinfo_act` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `ltp_cust_addinfo_tc` longtext COLLATE utf8mb4_unicode_ci,
  `ltp_cust_addinfo_en` longtext COLLATE utf8mb4_unicode_ci,
  `ltp_price` int(11) DEFAULT NULL,
  `ltp_sp_price` int(11) DEFAULT NULL,
  `ltp_cutoff_tpl_id` int(11) DEFAULT NULL,
  `ltp_ava_b4_day` int(11) DEFAULT NULL,
  `ltp_ava_b4_cutoff` time DEFAULT NULL,
  `ltp_sales_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_onsale` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `ltp_show_in_web` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `ltp_display_in_cate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `ltp_deli_start_date` date DEFAULT NULL,
  `ltp_is_force_checkout` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `ltp_force_checkout_start_date` date DEFAULT NULL,
  `ltp_force_checkout_end_date` date DEFAULT NULL,
  `ltp_publish_date` date DEFAULT NULL,
  `ltp_delivery_start_date` date DEFAULT NULL,
  `ltp_delivery_end_date` date DEFAULT NULL,
  `ltp_is_new` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `ltp_product_tag_tc` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_product_tag_en` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_bottom_product_tag_tc` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_bottom_product_tag_en` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_bottom_product_tag_2_tc` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_bottom_product_tag_2_en` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_addon_cate`
--

CREATE TABLE `tbl_product_addon_cate` (
  `addon_cate_id` int(11) NOT NULL,
  `original_addon_id` int(11) DEFAULT NULL,
  `shop_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `inhouse_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inhouse_name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gift_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT '1',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_card`
--

CREATE TABLE `tbl_product_card` (
  `card_id` int(11) NOT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `height` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_card_group`
--

CREATE TABLE `tbl_product_card_group` (
  `card_group_id` int(11) NOT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks_tc` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks_en` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_cutoff_tpl`
--

CREATE TABLE `tbl_product_cutoff_tpl` (
  `cutoff_tpl_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `adm_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ava_b4_day` int(11) DEFAULT NULL,
  `ava_b4_cutoff` time DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_img`
--

CREATE TABLE `tbl_product_img` (
  `map_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT '1',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_item_group`
--

CREATE TABLE `tbl_product_item_group` (
  `product_item_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `grp_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `is_product_page_show_qty` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `is_force_checkout` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_tpl`
--

CREATE TABLE `tbl_product_tpl` (
  `tpl_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `adm_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deli_msg_tc` longtext COLLATE utf8mb4_unicode_ci,
  `deli_msg_en` longtext COLLATE utf8mb4_unicode_ci,
  `info_msg_tc` longtext COLLATE utf8mb4_unicode_ci,
  `info_msg_en` longtext COLLATE utf8mb4_unicode_ci,
  `markup` int(11) DEFAULT '0',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_purchase`
--

CREATE TABLE `tbl_purchase` (
  `po_id` int(11) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `ref_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shop`
--

CREATE TABLE `tbl_shop` (
  `shop_id` int(11) NOT NULL,
  `adm_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prefix` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domain` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nav_color` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commission` int(11) DEFAULT NULL,
  `platform_commission` int(11) DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_member_credit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `point_ratio` int(11) DEFAULT '0',
  `google_x` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_y` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `markup` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `header_text` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auto_accept_start_time` timestamp NULL DEFAULT NULL,
  `auto_accept_end_time` timestamp NULL DEFAULT NULL,
  `product_list_img_1_path` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_list_img_1_link` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_list_img_2_path` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_list_img_2_link` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_list_img_3_path` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_list_img_3_link` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sku`
--

CREATE TABLE `tbl_sku` (
  `sku_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `gift_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zuri_name_en` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ltp_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sAlias` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `ava_qty` int(11) DEFAULT '0',
  `reserved_qty` int(11) DEFAULT '0',
  `alert_qty` int(11) DEFAULT '0',
  `cost` int(11) DEFAULT '0',
  `sku_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku_val` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auto` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sku_code`
--

CREATE TABLE `tbl_sku_code` (
  `code_id` int(11) NOT NULL,
  `code` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sku_po`
--

CREATE TABLE `tbl_sku_po` (
  `po_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `ref_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sku_val`
--

CREATE TABLE `tbl_sku_val` (
  `val_id` int(11) NOT NULL,
  `code_id` int(11) NOT NULL,
  `val` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lbl_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lbl_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sql_audit_log`
--

CREATE TABLE `tbl_sql_audit_log` (
  `log_id` int(11) NOT NULL,
  `table` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `row_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_value` longtext COLLATE utf8mb4_unicode_ci,
  `updated_value` longtext COLLATE utf8mb4_unicode_ci,
  `updated_by_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_stocktake`
--

CREATE TABLE `tbl_stocktake` (
  `stocktake_id` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '10_incomplete',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_stocktake_sku`
--

CREATE TABLE `tbl_stocktake_sku` (
  `map_id` int(11) NOT NULL,
  `stocktake_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `sku_id` int(11) NOT NULL,
  `expected_qty` int(11) NOT NULL,
  `actual_qty` int(11) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `activate` varchar(1) DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier`
--

CREATE TABLE `tbl_supplier` (
  `supplier_id` int(11) NOT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_form` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `MOQ` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_detail` longtext COLLATE utf8mb4_unicode_ci,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier_contact`
--

CREATE TABLE `tbl_supplier_contact` (
  `contact_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `contact_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier_tag`
--

CREATE TABLE `tbl_supplier_tag` (
  `tag_id` int(11) NOT NULL,
  `parent_tag_id` int(11) DEFAULT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_voucher`
--

CREATE TABLE `tbl_voucher` (
  `voucher_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks_tc` longtext COLLATE utf8mb4_unicode_ci,
  `remarks_en` longtext COLLATE utf8mb4_unicode_ci,
  `sales_admin_id` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT '0',
  `type` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `minimum_order_amt` int(11) DEFAULT '0',
  `ava_day` int(11) DEFAULT '0',
  `expiry_date` datetime DEFAULT NULL,
  `redeem` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `is_ava_discount_product` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_addonCate_product`
--

CREATE TABLE `tmap_addonCate_product` (
  `map_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `addon_cate_id` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_addon_group`
--

CREATE TABLE `tmap_addon_group` (
  `map_id` int(11) NOT NULL,
  `addon_id` int(11) DEFAULT NULL,
  `addon_group_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_addon_product`
--

CREATE TABLE `tmap_addon_product` (
  `map_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `addon_id` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_cate_coupon`
--

CREATE TABLE `tmap_cate_coupon` (
  `map_id` int(11) NOT NULL,
  `cate_id` int(11) DEFAULT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_cate_greeting`
--

CREATE TABLE `tmap_cate_greeting` (
  `map_id` int(11) NOT NULL,
  `greeting_cate_id` int(11) DEFAULT NULL,
  `cate_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_cate_nav`
--

CREATE TABLE `tmap_cate_nav` (
  `map_id` int(11) NOT NULL,
  `nav_id` int(11) DEFAULT NULL,
  `cate_id` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_cate_product`
--

CREATE TABLE `tmap_cate_product` (
  `map_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `cate_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_cate_voucher`
--

CREATE TABLE `tmap_cate_voucher` (
  `map_id` int(11) NOT NULL,
  `cate_id` int(11) DEFAULT NULL,
  `voucher_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_combo_product`
--

CREATE TABLE `tmap_combo_product` (
  `map_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `src_product_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_company_discount_email`
--

CREATE TABLE `tmap_company_discount_email` (
  `map_id` int(11) NOT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_content_product`
--

CREATE TABLE `tmap_content_product` (
  `map_id` int(11) NOT NULL,
  `content_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_content_slider`
--

CREATE TABLE `tmap_content_slider` (
  `slider_id` int(11) NOT NULL,
  `content_id` int(11) DEFAULT NULL,
  `img_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_coupon_cateBlackList`
--

CREATE TABLE `tmap_coupon_cateBlackList` (
  `map_id` int(11) NOT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `cate_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_coupon_cateWhiteList`
--

CREATE TABLE `tmap_coupon_cateWhiteList` (
  `map_id` int(11) NOT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `cate_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_coupon_email`
--

CREATE TABLE `tmap_coupon_email` (
  `map_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_coupon_productBlackList`
--

CREATE TABLE `tmap_coupon_productBlackList` (
  `map_id` int(11) NOT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_coupon_productWhiteList`
--

CREATE TABLE `tmap_coupon_productWhiteList` (
  `map_id` int(11) NOT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_deli_group_region`
--

CREATE TABLE `tmap_deli_group_region` (
  `map_id` int(11) NOT NULL,
  `deli_group_region_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_deli_product`
--

CREATE TABLE `tmap_deli_product` (
  `map_id` int(11) NOT NULL,
  `deli_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_greeting_group`
--

CREATE TABLE `tmap_greeting_group` (
  `map_id` int(11) NOT NULL,
  `cate_id` int(11) DEFAULT NULL,
  `greeting_group_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_greeting_product`
--

CREATE TABLE `tmap_greeting_product` (
  `map_id` int(11) NOT NULL,
  `greeting_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_grp_product`
--

CREATE TABLE `tmap_grp_product` (
  `map_id` int(11) NOT NULL,
  `grp_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_holiday_product`
--

CREATE TABLE `tmap_holiday_product` (
  `map_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `holiday_id` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_item_barcode`
--

CREATE TABLE `tmap_item_barcode` (
  `map_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `barcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_item_shop`
--

CREATE TABLE `tmap_item_shop` (
  `map_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_item_tag`
--

CREATE TABLE `tmap_item_tag` (
  `map_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_item_various`
--

CREATE TABLE `tmap_item_various` (
  `map_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `val_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_item_various_val`
--

CREATE TABLE `tmap_item_various_val` (
  `map_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `val_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_location_item`
--

CREATE TABLE `tmap_location_item` (
  `map_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT '0',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_member_product`
--

CREATE TABLE `tmap_member_product` (
  `map_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_member_region`
--

CREATE TABLE `tmap_member_region` (
  `map_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_member_timeslot`
--

CREATE TABLE `tmap_member_timeslot` (
  `map_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `slot_id` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_nav_page`
--

CREATE TABLE `tmap_nav_page` (
  `map_id` int(11) NOT NULL,
  `nav_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_new_member_voucher`
--

CREATE TABLE `tmap_new_member_voucher` (
  `map_id` int(11) NOT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `voucher_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_popup_url`
--

CREATE TABLE `tmap_popup_url` (
  `map_id` int(11) NOT NULL,
  `popup_id` int(11) DEFAULT NULL,
  `url` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_product_addon`
--

CREATE TABLE `tmap_product_addon` (
  `map_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `addon_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_product_addon_cate`
--

CREATE TABLE `tmap_product_addon_cate` (
  `map_id` int(11) NOT NULL,
  `addon_cate_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_product_card`
--

CREATE TABLE `tmap_product_card` (
  `map_id` int(11) NOT NULL,
  `product_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_product_card_group`
--

CREATE TABLE `tmap_product_card_group` (
  `map_id` int(11) NOT NULL,
  `card_id` int(11) DEFAULT NULL,
  `card_group_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_role_product`
--

CREATE TABLE `tmap_role_product` (
  `map_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_sku_code_val`
--

CREATE TABLE `tmap_sku_code_val` (
  `map_id` int(11) NOT NULL,
  `sku_id` int(11) NOT NULL,
  `val_id` int(11) NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_sku_tag`
--

CREATE TABLE `tmap_sku_tag` (
  `map_id` int(11) NOT NULL,
  `sku_id` int(11) NOT NULL,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_supplier_sku`
--

CREATE TABLE `tmap_supplier_sku` (
  `map_id` int(11) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `sku_id` int(11) DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmap_supplier_tag`
--

CREATE TABLE `tmap_supplier_tag` (
  `map_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tpl_item_cate_various_type`
--

CREATE TABLE `tpl_item_cate_various_type` (
  `type_id` int(11) NOT NULL,
  `cate_id` int(11) DEFAULT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tpl_item_cate_various_val`
--

CREATE TABLE `tpl_item_cate_various_val` (
  `val_id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `val` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isHide` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `log_item`
--
ALTER TABLE `log_item`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `log_location_item`
--
ALTER TABLE `log_location_item`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `log_product_item_content`
--
ALTER TABLE `log_product_item_content`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `tbl_addon`
--
ALTER TABLE `tbl_addon`
  ADD PRIMARY KEY (`addon_id`);

--
-- Indexes for table `tbl_addon_group`
--
ALTER TABLE `tbl_addon_group`
  ADD PRIMARY KEY (`addon_group_id`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_admin_role`
--
ALTER TABLE `tbl_admin_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `tbl_alert`
--
ALTER TABLE `tbl_alert`
  ADD PRIMARY KEY (`alert_id`);

--
-- Indexes for table `tbl_alert_admin`
--
ALTER TABLE `tbl_alert_admin`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tbl_car`
--
ALTER TABLE `tbl_car`
  ADD PRIMARY KEY (`car_id`);

--
-- Indexes for table `tbl_cate`
--
ALTER TABLE `tbl_cate`
  ADD PRIMARY KEY (`cate_id`);

--
-- Indexes for table `tbl_client`
--
ALTER TABLE `tbl_client`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `tbl_company_discount`
--
ALTER TABLE `tbl_company_discount`
  ADD PRIMARY KEY (`discount_id`);

--
-- Indexes for table `tbl_company_discount_applicant`
--
ALTER TABLE `tbl_company_discount_applicant`
  ADD PRIMARY KEY (`applicant_id`);

--
-- Indexes for table `tbl_coupon`
--
ALTER TABLE `tbl_coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `tbl_deli_cust_grp`
--
ALTER TABLE `tbl_deli_cust_grp`
  ADD PRIMARY KEY (`grp_id`);

--
-- Indexes for table `tbl_deli_cust_region`
--
ALTER TABLE `tbl_deli_cust_region`
  ADD PRIMARY KEY (`cust_id`);

--
-- Indexes for table `tbl_deli_cust_timeslot`
--
ALTER TABLE `tbl_deli_cust_timeslot`
  ADD PRIMARY KEY (`cust_id`);

--
-- Indexes for table `tbl_deli_district`
--
ALTER TABLE `tbl_deli_district`
  ADD PRIMARY KEY (`district_id`);

--
-- Indexes for table `tbl_deli_group`
--
ALTER TABLE `tbl_deli_group`
  ADD PRIMARY KEY (`deli_group_id`);

--
-- Indexes for table `tbl_deli_group_region`
--
ALTER TABLE `tbl_deli_group_region`
  ADD PRIMARY KEY (`deli_group_region_id`);

--
-- Indexes for table `tbl_deli_group_region_timeslot`
--
ALTER TABLE `tbl_deli_group_region_timeslot`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tbl_deli_group_timeslot`
--
ALTER TABLE `tbl_deli_group_timeslot`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tbl_deli_item`
--
ALTER TABLE `tbl_deli_item`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tbl_deli_region`
--
ALTER TABLE `tbl_deli_region`
  ADD PRIMARY KEY (`region_id`);

--
-- Indexes for table `tbl_deli_timeslot`
--
ALTER TABLE `tbl_deli_timeslot`
  ADD PRIMARY KEY (`slot_id`);

--
-- Indexes for table `tbl_driver_schedule`
--
ALTER TABLE `tbl_driver_schedule`
  ADD PRIMARY KEY (`driver_schedule_id`);

--
-- Indexes for table `tbl_email`
--
ALTER TABLE `tbl_email`
  ADD PRIMARY KEY (`email_id`);

--
-- Indexes for table `tbl_email_tpl`
--
ALTER TABLE `tbl_email_tpl`
  ADD PRIMARY KEY (`tpl_id`);

--
-- Indexes for table `tbl_gift`
--
ALTER TABLE `tbl_gift`
  ADD PRIMARY KEY (`gift_id`);

--
-- Indexes for table `tbl_gift_card`
--
ALTER TABLE `tbl_gift_card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `tbl_gift_card_key`
--
ALTER TABLE `tbl_gift_card_key`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tbl_gift_deli`
--
ALTER TABLE `tbl_gift_deli`
  ADD PRIMARY KEY (`deli_id`);

--
-- Indexes for table `tbl_greeting`
--
ALTER TABLE `tbl_greeting`
  ADD PRIMARY KEY (`greeting_id`);

--
-- Indexes for table `tbl_greeting_cate`
--
ALTER TABLE `tbl_greeting_cate`
  ADD PRIMARY KEY (`cate_id`);

--
-- Indexes for table `tbl_greeting_group`
--
ALTER TABLE `tbl_greeting_group`
  ADD PRIMARY KEY (`greeting_group_id`);

--
-- Indexes for table `tbl_greeting_subcate`
--
ALTER TABLE `tbl_greeting_subcate`
  ADD PRIMARY KEY (`subcate_id`);

--
-- Indexes for table `tbl_holiday`
--
ALTER TABLE `tbl_holiday`
  ADD PRIMARY KEY (`holiday_id`);

--
-- Indexes for table `tbl_item`
--
ALTER TABLE `tbl_item`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `tbl_item_brand`
--
ALTER TABLE `tbl_item_brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `tbl_item_cate`
--
ALTER TABLE `tbl_item_cate`
  ADD PRIMARY KEY (`cate_id`);

--
-- Indexes for table `tbl_item_cate_various_type`
--
ALTER TABLE `tbl_item_cate_various_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `tbl_item_cate_various_val`
--
ALTER TABLE `tbl_item_cate_various_val`
  ADD PRIMARY KEY (`val_id`);

--
-- Indexes for table `tbl_item_country`
--
ALTER TABLE `tbl_item_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `tbl_item_grp`
--
ALTER TABLE `tbl_item_grp`
  ADD PRIMARY KEY (`grp_id`);

--
-- Indexes for table `tbl_item_grp_content`
--
ALTER TABLE `tbl_item_grp_content`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `tbl_item_stock_in`
--
ALTER TABLE `tbl_item_stock_in`
  ADD PRIMARY KEY (`in_id`);

--
-- Indexes for table `tbl_item_stock_out`
--
ALTER TABLE `tbl_item_stock_out`
  ADD PRIMARY KEY (`out_id`);

--
-- Indexes for table `tbl_item_tag`
--
ALTER TABLE `tbl_item_tag`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `tbl_location`
--
ALTER TABLE `tbl_location`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `tbl_member`
--
ALTER TABLE `tbl_member`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `tbl_membership`
--
ALTER TABLE `tbl_membership`
  ADD PRIMARY KEY (`membership_id`);

--
-- Indexes for table `tbl_member_fav`
--
ALTER TABLE `tbl_member_fav`
  ADD PRIMARY KEY (`fav_id`);

--
-- Indexes for table `tbl_member_voucher`
--
ALTER TABLE `tbl_member_voucher`
  ADD PRIMARY KEY (`member_voucher_id`);

--
-- Indexes for table `tbl_nav`
--
ALTER TABLE `tbl_nav`
  ADD PRIMARY KEY (`nav_id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_order_deli`
--
ALTER TABLE `tbl_order_deli`
  ADD PRIMARY KEY (`deli_id`);

--
-- Indexes for table `tbl_order_deli_addon`
--
ALTER TABLE `tbl_order_deli_addon`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tbl_order_deli_item`
--
ALTER TABLE `tbl_order_deli_item`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tbl_order_deposit_slip`
--
ALTER TABLE `tbl_order_deposit_slip`
  ADD PRIMARY KEY (`slip_id`);

--
-- Indexes for table `tbl_order_excel`
--
ALTER TABLE `tbl_order_excel`
  ADD PRIMARY KEY (`excel_id`);

--
-- Indexes for table `tbl_order_payment`
--
ALTER TABLE `tbl_order_payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `tbl_page`
--
ALTER TABLE `tbl_page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `tbl_page_content`
--
ALTER TABLE `tbl_page_content`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `tbl_popup`
--
ALTER TABLE `tbl_popup`
  ADD PRIMARY KEY (`popup_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_product_addon_cate`
--
ALTER TABLE `tbl_product_addon_cate`
  ADD PRIMARY KEY (`addon_cate_id`);

--
-- Indexes for table `tbl_product_card`
--
ALTER TABLE `tbl_product_card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `tbl_product_card_group`
--
ALTER TABLE `tbl_product_card_group`
  ADD PRIMARY KEY (`card_group_id`);

--
-- Indexes for table `tbl_product_cutoff_tpl`
--
ALTER TABLE `tbl_product_cutoff_tpl`
  ADD PRIMARY KEY (`cutoff_tpl_id`);

--
-- Indexes for table `tbl_product_img`
--
ALTER TABLE `tbl_product_img`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tbl_product_item_group`
--
ALTER TABLE `tbl_product_item_group`
  ADD PRIMARY KEY (`product_item_id`);

--
-- Indexes for table `tbl_product_tpl`
--
ALTER TABLE `tbl_product_tpl`
  ADD PRIMARY KEY (`tpl_id`);

--
-- Indexes for table `tbl_purchase`
--
ALTER TABLE `tbl_purchase`
  ADD PRIMARY KEY (`po_id`);

--
-- Indexes for table `tbl_shop`
--
ALTER TABLE `tbl_shop`
  ADD PRIMARY KEY (`shop_id`);

--
-- Indexes for table `tbl_sku`
--
ALTER TABLE `tbl_sku`
  ADD PRIMARY KEY (`sku_id`);

--
-- Indexes for table `tbl_sku_code`
--
ALTER TABLE `tbl_sku_code`
  ADD PRIMARY KEY (`code_id`);

--
-- Indexes for table `tbl_sku_po`
--
ALTER TABLE `tbl_sku_po`
  ADD PRIMARY KEY (`po_id`);

--
-- Indexes for table `tbl_sku_val`
--
ALTER TABLE `tbl_sku_val`
  ADD PRIMARY KEY (`val_id`);

--
-- Indexes for table `tbl_sql_audit_log`
--
ALTER TABLE `tbl_sql_audit_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `tbl_stocktake`
--
ALTER TABLE `tbl_stocktake`
  ADD PRIMARY KEY (`stocktake_id`);

--
-- Indexes for table `tbl_stocktake_sku`
--
ALTER TABLE `tbl_stocktake_sku`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `tbl_supplier_contact`
--
ALTER TABLE `tbl_supplier_contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `tbl_supplier_tag`
--
ALTER TABLE `tbl_supplier_tag`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `tbl_voucher`
--
ALTER TABLE `tbl_voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Indexes for table `tmap_addonCate_product`
--
ALTER TABLE `tmap_addonCate_product`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_addon_group`
--
ALTER TABLE `tmap_addon_group`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_addon_product`
--
ALTER TABLE `tmap_addon_product`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_cate_coupon`
--
ALTER TABLE `tmap_cate_coupon`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_cate_greeting`
--
ALTER TABLE `tmap_cate_greeting`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_cate_nav`
--
ALTER TABLE `tmap_cate_nav`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_cate_product`
--
ALTER TABLE `tmap_cate_product`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_cate_voucher`
--
ALTER TABLE `tmap_cate_voucher`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_combo_product`
--
ALTER TABLE `tmap_combo_product`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_company_discount_email`
--
ALTER TABLE `tmap_company_discount_email`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_content_product`
--
ALTER TABLE `tmap_content_product`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_content_slider`
--
ALTER TABLE `tmap_content_slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `tmap_coupon_cateBlackList`
--
ALTER TABLE `tmap_coupon_cateBlackList`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_coupon_cateWhiteList`
--
ALTER TABLE `tmap_coupon_cateWhiteList`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_coupon_email`
--
ALTER TABLE `tmap_coupon_email`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_coupon_productBlackList`
--
ALTER TABLE `tmap_coupon_productBlackList`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_coupon_productWhiteList`
--
ALTER TABLE `tmap_coupon_productWhiteList`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_deli_group_region`
--
ALTER TABLE `tmap_deli_group_region`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_deli_product`
--
ALTER TABLE `tmap_deli_product`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_greeting_group`
--
ALTER TABLE `tmap_greeting_group`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_greeting_product`
--
ALTER TABLE `tmap_greeting_product`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_grp_product`
--
ALTER TABLE `tmap_grp_product`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_holiday_product`
--
ALTER TABLE `tmap_holiday_product`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_item_barcode`
--
ALTER TABLE `tmap_item_barcode`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_item_shop`
--
ALTER TABLE `tmap_item_shop`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_item_tag`
--
ALTER TABLE `tmap_item_tag`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_item_various`
--
ALTER TABLE `tmap_item_various`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_item_various_val`
--
ALTER TABLE `tmap_item_various_val`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_location_item`
--
ALTER TABLE `tmap_location_item`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_member_product`
--
ALTER TABLE `tmap_member_product`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_member_region`
--
ALTER TABLE `tmap_member_region`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_member_timeslot`
--
ALTER TABLE `tmap_member_timeslot`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_nav_page`
--
ALTER TABLE `tmap_nav_page`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_new_member_voucher`
--
ALTER TABLE `tmap_new_member_voucher`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_popup_url`
--
ALTER TABLE `tmap_popup_url`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_product_addon`
--
ALTER TABLE `tmap_product_addon`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_product_addon_cate`
--
ALTER TABLE `tmap_product_addon_cate`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_product_card`
--
ALTER TABLE `tmap_product_card`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_product_card_group`
--
ALTER TABLE `tmap_product_card_group`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_role_product`
--
ALTER TABLE `tmap_role_product`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_sku_code_val`
--
ALTER TABLE `tmap_sku_code_val`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_sku_tag`
--
ALTER TABLE `tmap_sku_tag`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_supplier_sku`
--
ALTER TABLE `tmap_supplier_sku`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tmap_supplier_tag`
--
ALTER TABLE `tmap_supplier_tag`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `tpl_item_cate_various_type`
--
ALTER TABLE `tpl_item_cate_various_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `tpl_item_cate_various_val`
--
ALTER TABLE `tpl_item_cate_various_val`
  ADD PRIMARY KEY (`val_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `log_item`
--
ALTER TABLE `log_item`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log_location_item`
--
ALTER TABLE `log_location_item`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log_product_item_content`
--
ALTER TABLE `log_product_item_content`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_addon`
--
ALTER TABLE `tbl_addon`
  MODIFY `addon_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_addon_group`
--
ALTER TABLE `tbl_addon_group`
  MODIFY `addon_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_admin_role`
--
ALTER TABLE `tbl_admin_role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_alert`
--
ALTER TABLE `tbl_alert`
  MODIFY `alert_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_alert_admin`
--
ALTER TABLE `tbl_alert_admin`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_car`
--
ALTER TABLE `tbl_car`
  MODIFY `car_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_cate`
--
ALTER TABLE `tbl_cate`
  MODIFY `cate_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_client`
--
ALTER TABLE `tbl_client`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_company_discount`
--
ALTER TABLE `tbl_company_discount`
  MODIFY `discount_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_company_discount_applicant`
--
ALTER TABLE `tbl_company_discount_applicant`
  MODIFY `applicant_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_coupon`
--
ALTER TABLE `tbl_coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_deli_cust_grp`
--
ALTER TABLE `tbl_deli_cust_grp`
  MODIFY `grp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_deli_cust_region`
--
ALTER TABLE `tbl_deli_cust_region`
  MODIFY `cust_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_deli_cust_timeslot`
--
ALTER TABLE `tbl_deli_cust_timeslot`
  MODIFY `cust_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_deli_district`
--
ALTER TABLE `tbl_deli_district`
  MODIFY `district_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_deli_group`
--
ALTER TABLE `tbl_deli_group`
  MODIFY `deli_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_deli_group_region`
--
ALTER TABLE `tbl_deli_group_region`
  MODIFY `deli_group_region_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_deli_group_region_timeslot`
--
ALTER TABLE `tbl_deli_group_region_timeslot`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_deli_group_timeslot`
--
ALTER TABLE `tbl_deli_group_timeslot`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_deli_item`
--
ALTER TABLE `tbl_deli_item`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_deli_region`
--
ALTER TABLE `tbl_deli_region`
  MODIFY `region_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_deli_timeslot`
--
ALTER TABLE `tbl_deli_timeslot`
  MODIFY `slot_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_driver_schedule`
--
ALTER TABLE `tbl_driver_schedule`
  MODIFY `driver_schedule_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_email`
--
ALTER TABLE `tbl_email`
  MODIFY `email_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_email_tpl`
--
ALTER TABLE `tbl_email_tpl`
  MODIFY `tpl_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_gift`
--
ALTER TABLE `tbl_gift`
  MODIFY `gift_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_gift_card`
--
ALTER TABLE `tbl_gift_card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_gift_card_key`
--
ALTER TABLE `tbl_gift_card_key`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_gift_deli`
--
ALTER TABLE `tbl_gift_deli`
  MODIFY `deli_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_greeting`
--
ALTER TABLE `tbl_greeting`
  MODIFY `greeting_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_greeting_cate`
--
ALTER TABLE `tbl_greeting_cate`
  MODIFY `cate_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_greeting_group`
--
ALTER TABLE `tbl_greeting_group`
  MODIFY `greeting_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_greeting_subcate`
--
ALTER TABLE `tbl_greeting_subcate`
  MODIFY `subcate_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_holiday`
--
ALTER TABLE `tbl_holiday`
  MODIFY `holiday_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_item`
--
ALTER TABLE `tbl_item`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_item_brand`
--
ALTER TABLE `tbl_item_brand`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_item_cate`
--
ALTER TABLE `tbl_item_cate`
  MODIFY `cate_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_item_cate_various_type`
--
ALTER TABLE `tbl_item_cate_various_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_item_cate_various_val`
--
ALTER TABLE `tbl_item_cate_various_val`
  MODIFY `val_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_item_country`
--
ALTER TABLE `tbl_item_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_item_grp`
--
ALTER TABLE `tbl_item_grp`
  MODIFY `grp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_item_grp_content`
--
ALTER TABLE `tbl_item_grp_content`
  MODIFY `content_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_item_stock_in`
--
ALTER TABLE `tbl_item_stock_in`
  MODIFY `in_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_item_stock_out`
--
ALTER TABLE `tbl_item_stock_out`
  MODIFY `out_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_item_tag`
--
ALTER TABLE `tbl_item_tag`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_location`
--
ALTER TABLE `tbl_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_member`
--
ALTER TABLE `tbl_member`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_membership`
--
ALTER TABLE `tbl_membership`
  MODIFY `membership_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_member_fav`
--
ALTER TABLE `tbl_member_fav`
  MODIFY `fav_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_member_voucher`
--
ALTER TABLE `tbl_member_voucher`
  MODIFY `member_voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_nav`
--
ALTER TABLE `tbl_nav`
  MODIFY `nav_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_order_deli`
--
ALTER TABLE `tbl_order_deli`
  MODIFY `deli_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_order_deli_addon`
--
ALTER TABLE `tbl_order_deli_addon`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_order_deli_item`
--
ALTER TABLE `tbl_order_deli_item`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_order_deposit_slip`
--
ALTER TABLE `tbl_order_deposit_slip`
  MODIFY `slip_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_order_excel`
--
ALTER TABLE `tbl_order_excel`
  MODIFY `excel_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_order_payment`
--
ALTER TABLE `tbl_order_payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_page`
--
ALTER TABLE `tbl_page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_page_content`
--
ALTER TABLE `tbl_page_content`
  MODIFY `content_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_popup`
--
ALTER TABLE `tbl_popup`
  MODIFY `popup_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_product_addon_cate`
--
ALTER TABLE `tbl_product_addon_cate`
  MODIFY `addon_cate_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_product_card`
--
ALTER TABLE `tbl_product_card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_product_card_group`
--
ALTER TABLE `tbl_product_card_group`
  MODIFY `card_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_product_cutoff_tpl`
--
ALTER TABLE `tbl_product_cutoff_tpl`
  MODIFY `cutoff_tpl_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_product_img`
--
ALTER TABLE `tbl_product_img`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_product_item_group`
--
ALTER TABLE `tbl_product_item_group`
  MODIFY `product_item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_product_tpl`
--
ALTER TABLE `tbl_product_tpl`
  MODIFY `tpl_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_purchase`
--
ALTER TABLE `tbl_purchase`
  MODIFY `po_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_shop`
--
ALTER TABLE `tbl_shop`
  MODIFY `shop_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_sku`
--
ALTER TABLE `tbl_sku`
  MODIFY `sku_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_sku_code`
--
ALTER TABLE `tbl_sku_code`
  MODIFY `code_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_sku_po`
--
ALTER TABLE `tbl_sku_po`
  MODIFY `po_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_sku_val`
--
ALTER TABLE `tbl_sku_val`
  MODIFY `val_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_sql_audit_log`
--
ALTER TABLE `tbl_sql_audit_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_stocktake`
--
ALTER TABLE `tbl_stocktake`
  MODIFY `stocktake_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_stocktake_sku`
--
ALTER TABLE `tbl_stocktake_sku`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_supplier_contact`
--
ALTER TABLE `tbl_supplier_contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_supplier_tag`
--
ALTER TABLE `tbl_supplier_tag`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_voucher`
--
ALTER TABLE `tbl_voucher`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_addonCate_product`
--
ALTER TABLE `tmap_addonCate_product`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_addon_group`
--
ALTER TABLE `tmap_addon_group`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_addon_product`
--
ALTER TABLE `tmap_addon_product`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_cate_coupon`
--
ALTER TABLE `tmap_cate_coupon`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_cate_greeting`
--
ALTER TABLE `tmap_cate_greeting`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_cate_nav`
--
ALTER TABLE `tmap_cate_nav`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_cate_product`
--
ALTER TABLE `tmap_cate_product`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_cate_voucher`
--
ALTER TABLE `tmap_cate_voucher`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_combo_product`
--
ALTER TABLE `tmap_combo_product`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_company_discount_email`
--
ALTER TABLE `tmap_company_discount_email`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_content_product`
--
ALTER TABLE `tmap_content_product`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_content_slider`
--
ALTER TABLE `tmap_content_slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_coupon_cateBlackList`
--
ALTER TABLE `tmap_coupon_cateBlackList`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_coupon_cateWhiteList`
--
ALTER TABLE `tmap_coupon_cateWhiteList`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_coupon_email`
--
ALTER TABLE `tmap_coupon_email`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_coupon_productBlackList`
--
ALTER TABLE `tmap_coupon_productBlackList`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_coupon_productWhiteList`
--
ALTER TABLE `tmap_coupon_productWhiteList`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_deli_group_region`
--
ALTER TABLE `tmap_deli_group_region`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_deli_product`
--
ALTER TABLE `tmap_deli_product`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_greeting_group`
--
ALTER TABLE `tmap_greeting_group`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_greeting_product`
--
ALTER TABLE `tmap_greeting_product`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_grp_product`
--
ALTER TABLE `tmap_grp_product`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_holiday_product`
--
ALTER TABLE `tmap_holiday_product`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_item_barcode`
--
ALTER TABLE `tmap_item_barcode`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_item_shop`
--
ALTER TABLE `tmap_item_shop`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_item_tag`
--
ALTER TABLE `tmap_item_tag`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_item_various`
--
ALTER TABLE `tmap_item_various`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_item_various_val`
--
ALTER TABLE `tmap_item_various_val`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_location_item`
--
ALTER TABLE `tmap_location_item`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_member_product`
--
ALTER TABLE `tmap_member_product`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_member_region`
--
ALTER TABLE `tmap_member_region`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_member_timeslot`
--
ALTER TABLE `tmap_member_timeslot`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_nav_page`
--
ALTER TABLE `tmap_nav_page`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_new_member_voucher`
--
ALTER TABLE `tmap_new_member_voucher`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_popup_url`
--
ALTER TABLE `tmap_popup_url`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_product_addon`
--
ALTER TABLE `tmap_product_addon`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_product_addon_cate`
--
ALTER TABLE `tmap_product_addon_cate`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_product_card`
--
ALTER TABLE `tmap_product_card`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_product_card_group`
--
ALTER TABLE `tmap_product_card_group`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_role_product`
--
ALTER TABLE `tmap_role_product`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_sku_code_val`
--
ALTER TABLE `tmap_sku_code_val`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_sku_tag`
--
ALTER TABLE `tmap_sku_tag`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_supplier_sku`
--
ALTER TABLE `tmap_supplier_sku`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmap_supplier_tag`
--
ALTER TABLE `tmap_supplier_tag`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tpl_item_cate_various_type`
--
ALTER TABLE `tpl_item_cate_various_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tpl_item_cate_various_val`
--
ALTER TABLE `tpl_item_cate_various_val`
  MODIFY `val_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
