<?php
// responseCode pattern:

// Positive: success
// 0:      general success

// Negative: failure
// -6x:     user right
// -7x:     3rd party api Return
// -8x:     mode related 
// -9x:     parameter related
// -999:    unknown


class apiDataBuilder{
    const codeMsgMapping = [
        ['code' => 32,   'msg' => ['tc' => '此產品已完成包裝。', 'en' => 'The product is packed completely.']],
        ['code' => 31,   'msg' => ['tc' => '盤點完結', 'en' => 'stocktake completed.']],
        ['code' => 21,   'msg' => ['tc' => '交易有異常，請聯絡我們的客戶服務。', 'en' => 'Transaction processed with error.']],
        ['code' => 20,   'msg' => ['tc' => '交易進行中。', 'en' => 'Transaction processing.']],
        ['code' => 17,   'msg' => ['tc' => '積分不夠兌換禮物。', 'en' => 'Point is not enough.']],
        ['code' => 16,   'msg' => ['tc' => '此URL已使用。', 'en' => 'The URL is used already.']],
        ['code' => 15,   'msg' => ['tc' => '禮物無效。', 'en' => 'Gift is not valid.']],
        ['code' => 14,   'msg' => ['tc' => '禮物卡兌換碼無效。', 'en' => 'Redeem code is not valid.']],
        ['code' => 13,   'msg' => ['tc' => '假期制限。', 'en' => 'Dayoff.']],
        ['code' => 12,   'msg' => ['tc' => '超過假期制限。', 'en' => 'Holiday product everyday quota is over.']],
        ['code' => 11,   'msg' => ['tc' => '優惠碼無效。', 'en' => 'Coupon is not valid.']],
        ['code' => 10,   'msg' => ['tc' => '優惠卷無效', 'en' => 'Voucher is not valid.']],
        ['code' => 0,    'msg' => ['tc' => '成功', 'en' => 'Success.']],

        ['code' => -39,  'msg' => ['tc' => '條碼無效', 'en' => 'The barcode is invalid.']],
        ['code' => -38,  'msg' => ['tc' => 'SKU值已存在。', 'en' => 'The SKU is existed already.']],
        ['code' => -37,  'msg' => ['tc' => '數量已超過此產品需求', 'en' => 'The quantity over the product required.']],
        ['code' => -36,  'msg' => ['tc' => '此運單並不需要此物品或已經包裝好。', 'en' => 'This item is not included in this delivery record or packed already.']],
        ['code' => -35,  'msg' => ['tc' => 'SKU值不能以SKUID:開頭。', 'en' => 'The SKU cannot start with SKUID:.']],
        ['code' => -34,  'msg' => ['tc' => '貨架沒有足夠數量。', 'en' => 'The quantity is not enough in this shelft(location).']],
        ['code' => -33,  'msg' => ['tc' => 'ITEM值不存在。', 'en' => 'The SKU is not existed.']],
        ['code' => -32,  'msg' => ['tc' => '貨架不存在。', 'en' => 'The shelf(location) is not existed.']],
        ['code' => -31,  'msg' => ['tc' => '條碼不存在。', 'en' => 'The barcode is not existed.']],
        ['code' => -30,  'msg' => ['tc' => '條碼已使用在其它SKU值', 'en' => 'The barcode is already in use in the other SKU.']],
        
        ['code' => -40,  'msg' => ['tc' => '此功能只限會員使用', 'en' => 'Member function only.']],
        ['code' => -41,  'msg' => ['tc' => '此運單不適用於此司機', 'en' => 'Driver is not match.']],
        
        ['code' => -50,  'msg' => ['tc' => '登入失敗。', 'en' => 'Login failure.']],
        ['code' => -51,  'msg' => ['tc' => '購物車不存在。', 'en' => 'Cart is not existed.']],
        ['code' => -52,  'msg' => ['tc' => '帳戶並不存在。', 'en' => 'Account is not existed.']],
        ['code' => -53,  'msg' => ['tc' => '供應商並不存在。', 'en' => 'Supplier is not existed.']],
        
        ['code' => -60,  'msg' => ['tc' => '此頁面或功需要管理員權限。', 'en' => 'Admin right is needed.']],
        ['code' => -61,  'msg' => ['tc' => '此頁面或功需要閱覽權限。', 'en' => 'Viewer right is needed.']],
        ['code' => -62,  'msg' => ['tc' => '此頁面或功需要修改權限。', 'en' => 'Editor right is needed.']],
        
        ['code' => -70,  'msg' => ['tc' => 'API return error.', 'en' => 'Api Return error.']],
        
        ['code' => -81,  'msg' => ['tc' => 'Mode not found.', 'en' => "Mode not found"]],
        ['code' => -82,  'msg' => ['tc' => 'Submode not found.', 'en' => "Submode not found"]],
        ['code' => -83,  'msg' => ['tc' => 'Thirdmode not found.', 'en' => "Thirdmode is required but not found"]],
        ['code' => -84,  'msg' => ['tc' => 'API not found.', 'en' => "API not found"]],
        
        ['code' => -90,  'msg' => ['tc' => '不支援此檔案格式。', 'en' => 'File type is not support.']],
        ['code' => -91,  'msg' => ['tc' => '檔案大小過大。', 'en' => 'File size is too large.']],
        ['code' => -92,  'msg' => ['tc' => '資料無效。', 'en' => 'Data invalid.']],
        ['code' => -93,  'msg' => ['tc' => '電郵無效。', 'en' => 'Email invalid.']],
        ['code' => -94,  'msg' => ['tc' => '密碼不乎合規格，密碼需要8位英數字。', 'en' => 'Password invalid, password format should be 8 digits.']],
        ['code' => -95,  'msg' => ['tc' => '此電郵已註冊', 'en' => 'This email is registered already.']],
        ['code' => -99,  'msg' => ['tc' => 'Missing paramter(s).', 'en' => 'Missing parameter(s).']],
        ['code' => -991,  'msg' => ['tc' => 'Missing paramter(s) invalid (not allow zero).', 'en' => 'Missing parameter(s) invalid (not allow zero).']],
        ['code' => -992,  'msg' => ['tc' => 'Missing paramter(s) invalid (not allow null).', 'en' => 'Missing parameter(s) invalid (not allow null).']],
        ['code' => -993,  'msg' => ['tc' => 'Missing paramter(s) invalid (not allow blank).', 'en' => 'Missing parameter(s) invalid (not allow blank).']],
        ['code' => -994,  'msg' => ['tc' => 'Missing paramter(s): date or drug_id is necessary.', 'en' => 'Missing parameter(s): date or drug_id is necessary.']],
        ['code' => -999, 'msg' => ['tc' => '不明錯誤, 請聯絡The Gift跟進.', 'en' => 'Unknown error, please contact to administrator.']]
    ]; 


    public function __construct($code = 0, $apiReturn = null, $displayMsg = null, $lang = 'tc')
    {
        $this->code = $code;
        try {
            $jsonObj = json_encode($apiReturn);
            if ($jsonObj === null && json_last_error() !== JSON_ERROR_NONE) {
                $this->apiReturn = $jsonObj;
            } 
        } catch (Exception $ex){
            $this->apiReturn = $apiReturn;
        }
        
        $this->lang = $lang;
        $this->apiReturn = $apiReturn;
        $this->displayMsg = $displayMsg;
        $this->returnArr = [];
        $this->init();
    }

    function init(){
        header('Content-Type: application/json; charset=utf-8');
        $codeMsg = self::codeMsgMapping[array_search($this->code, array_column(self::codeMsgMapping, 'code'))];

        if(isset($codeMsg['msg'][$this->lang]))
            $msg = $codeMsg['msg'][$this->lang];
        else 
            $msg = $codeMsg['msg']['tc'];

        $tpl = [
            'code' => $codeMsg['code'],
            'msg' => $msg,
            'server_time' => $this->get_current_time(),
            'language' => isset($_SESSION['lang']) ? $_SESSION['lang'] : 'tc',
            'result' => $this->apiReturn
        ];

        if (isset($this->displayMsg)){
            $tpl['msg'] = $this->displayMsg;
        }

        $this->returnArr  = $tpl; 
        $this->dispatcher();
    }

    function dispatcher($datatype = 'json'){
        switch ($datatype){
            case 'json':
            default:
            print_r(json_encode($this->returnArr));
            break;
        }
        die();
    }

    function get_current_time(){
		return date('Y-m-d H:i:s');
	}

	

}
?>
