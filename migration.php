<?php
require_once './include/baseModel.class.php';
class index extends baseModel
{
	public $webpSupport;

	public function __construct()
	{
		parent::__construct('');
		$this->https_handling('Y'); // force https
		$this->headerCheck(); // handle request header
		$this->mobileCheck(); // check mobile setting
		$this->memberCheck(); // member check
		$this->webpSupport(); // check is support webp	

		//if($_SERVER['SERVER_NAME'] != 'dev.fulffy.com' && ENV != 'PROD'){
		//	die('');
		//}
		$this->init();
	}

    public function display(){}

	public function init()
	{
		$this->migration();
	}

	function migration(){	

		require_once ROOT.'/include/SQLDBi.class.php';
		$this->odb = new SQLDBi('thegiftold',"thegiftold",O_DBPWD,O_HOST); // old db

		$this->mysqli = new mysqli(O_HOST,O_DBUSER,O_DBPWD,O_DB);
		// Check connection
		if ($this->mysqli -> connect_errno) {
			echo "Failed to connect to MySQL: " . $this->mysqli -> connect_error;
			exit();
		} 
		switch($this->mode){
			case 'order_discount': $this->order_discount(); break;


			case 'tbl_addon': $this->tbl_addon(); break;
			case 'tbl_admin': $this->tbl_admin(); break;
			case 'tbl_admin_role': $this->tbl_admin_role(); break;
			case 'tbl_cate': $this->tbl_cate(); break;
			case 'tbl_combo_product': $this->tbl_combo_product(); break;
			case 'tbl_company_discount': $this->tbl_company_discount(); break;
			case 'tbl_coupon': $this->tbl_coupon(); break;
			case 'tbl_deli_cust_grp': $this->tbl_deli_cust_grp(); break;
			case 'tbl_deli_cust_region': $this->tbl_deli_cust_region(); break;
			case 'tbl_deli_cust_timeslot': $this->tbl_deli_cust_timeslot(); break;
			case 'tbl_deli_item': $this->tbl_deli_item(); break;
			case 'tbl_deli_district':
			case 'tbl_deli_region': $this->tbl_deli_region(); break;
			case 'tbl_deli_timeslot': $this->tbl_deli_timeslot(); break;
			case 'tbl_gift': $this->tbl_gift(); break;
			case 'tbl_gift_card': $this->tbl_gift_card(); break;
			case 'tbl_gift_card_key': $this->tbl_gift_card_key(); break;
			case 'tbl_greeting_cate':
			case 'tbl_greeting_subcate':
			case 'tbl_greeting': $this->tbl_greeting(); break;
			case 'tbl_item': $this->tbl_item(); break;
			case 'tbl_member': $this->tbl_member(); break;
			case 'tbl_membership': $this->tbl_membership(); break;
			case 'tbl_nav': $this->tbl_nav(); break;
			case 'tbl_order': $this->tbl_order(); break;
			case 'tbl_order_deli': $this->tbl_order_deli(); break;
			case 'tbl_order_deli_item': $this->tbl_order_deli_item(); break;
			case 'tbl_order_payment': $this->tbl_order_payment(); break;
			case 'tbl_page_content':
			case 'tbl_page': $this->tbl_page(); break;
			case 'tbl_member_voucher': $this->tbl_member_voucher(); break;
			case 'tbl_popup': $this->tbl_popup(); break;
			case 'tbl_product': $this->tbl_product(); break;
			case 'tbl_product_addon_cate': $this->tbl_product_addon_cate(); break;
			case 'tbl_product_img': $this->tbl_product_img(); break;
			case 'tbl_product_item_content': $this->tbl_product_item_content(); break;
			case 'tbl_shop': $this->tbl_shop(); break;
			case 'tbl_sku': $this->tbl_sku(); break;
			case 'tbl_sql_audit_log': $this->tbl_sql_audit_log(); break;
			case 'tbl_voucher': $this->tbl_voucher(); break;
			case 'tmap_addon_product': $this->tmap_addon_product(); break;
			case 'tmap_cate_coupon': $this->tmap_cate_coupon(); break;
			case 'tmap_cate_greeting': $this->tmap_cate_greeting(); break;
			case 'tmap_cate_product': $this->tmap_cate_product(); break;
			case 'tmap_cate_voucher': $this->tmap_cate_voucher(); break;
			case 'tmap_combo_product': $this->tmap_combo_product(); break;
			case 'tmap_content_product': $this->tmap_content_product(); break;
			case 'tmap_content_slider': $this->tmap_content_slider(); break;
			case 'tmap_deli_product': $this->tmap_deli_product(); break;
			case 'tmap_greeting_product': $this->tmap_greeting_product(); break;
			case 'tmap_grp_product': $this->tmap_grp_product(); break;
			case 'tmap_item_shop': $this->tmap_item_shop(); break;
			case 'tmap_new_member_voucher': $this->tmap_new_member_voucher(); break;
			case 'tmap_product_addon_cate': $this->tmap_product_addon_cate(); break;
			case 'all':
				$this->tbl_addon(); // DONE 
				$this->tbl_admin(); // DONE
				$this->tbl_admin_role(); // DONE
				$this->tbl_cate(); // DONE
				$this->tbl_combo_product();
				$this->tbl_company_discount();
				$this->tbl_coupon(); // DONE
				$this->tbl_deli_cust_grp();
				$this->tbl_deli_cust_region();
				$this->tbl_deli_cust_timeslot();
				$this->tbl_deli_district();
				$this->tbl_deli_item(); // DONE
				$this->tbl_deli_region(); // DONE
				$this->tbl_deli_timeslot(); // DONE
				$this->tbl_gift();
				$this->tbl_gift_card();
				$this->tbl_gift_card_key();
				$this->tbl_greeting();
				$this->tbl_item();
				$this->tbl_member(); // DONE
				$this->tbl_membership(); // DONE
				$this->tbl_nav();
				$this->tbl_order();
				$this->tbl_order_deli();
				$this->tbl_order_deli_item();
				$this->tbl_order_payment();
				$this->tbl_page();
				$this->tbl_popup();
				$this->tbl_product();
				$this->tbl_product_addon_cate();
				$this->tbl_product_img();
				$this->tbl_product_item_content();
				$this->tbl_shop(); // DONE
				$this->tbl_sku();
				$this->tbl_sql_audit_log();
				$this->tbl_voucher();
				$this->tmap_addon_product();
				$this->tmap_cate_coupon();
				$this->tmap_cate_greeting();
				$this->tmap_cate_product();
				$this->tmap_cate_voucher();
				$this->tmap_combo_product();
				$this->tmap_content_product();
				$this->tmap_content_slider();
				$this->tmap_deli_product();
				$this->tmap_greeting_product();
				$this->tmap_grp_product();
				$this->tmap_item_shop();
				$this->tmap_new_member_voucher();
				$this->tmap_product_addon_cate();
				break;
		}

		die("COMPLETE");
	}	

	function order_discount(){
		$sql = "SELECT * FROM order_discounts LIMIT 50";
		$res = $this->odb->get_Sql($sql);

		foreach($res as $row){

			$data = [];
			$row['user_voucher_id']? $data['member_voucher_id'] = $row['user_voucher_id'] : null;
			$row['voucher_discount_amount']? $data['voucher_discount'] = round($row['voucher_discount_amount']) : null;
			$row['membership_discount_amount']? $data['membership_discount'] = round($row['membership_discount_amount']) : null;
			$sql = $this->DB->buildSql_update("tbl_order", "order_id", $row['order_id'], $data);
			echo $sql . "<BR/>";

			$this->DB->update($sql);
			echo "tbl_member_voucher row: ".$row['order_id']." || updated <br/>";
		}
	}

	function tbl_member_voucher(){
		$sql = "DROP TABLE IF EXISTS `tbl_member_voucher`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_member_voucher` (
			`member_voucher_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`member_id` int(11) DEFAULT NULL, 
			`voucher_id` int(11) DEFAULT NULL, 
			`redeemed` varchar(1) DEFAULT 'N',
			`discount` int(11) DEFAULT '0',
			`type` varchar(1) DEFAULT 'A',
			`minimum_order_amt` int(11) DEFAULT '0',
			`ava_day` int(11) DEFAULT '0',
			`expiry_date` date DEFAULT NULL,
			`sales_admin_id` int (11) DEFAULT NULL,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);

		$sql = "SELECT * FROM user_vouchers";
		$res = $this->odb->get_Sql($sql);

		foreach($res as $row){
			$data = [
				"member_id" => $row['user_id'],
				"voucher_id" => 1,
				"discount" => $row['amount'],
				"type" => "A",
				"minimum_order_amt" => $row['minimum_order_amount'],
				"expiry_date" => $row['expiry_date'],
			];


			if ($row['is_used'] == 0){
				$data['redeemed'] = 'N';
			} else {
				$data['redeemed'] = 'Y';
			}

			$id = $this->DB->insert_db("tbl_member_voucher", $data);
			echo "tbl_member_voucher row: $id || insert completed<br/>";
		}
		echo "tbl_member_voucher: DONE <hr/>";
		

	}

	function tbl_addon(){
		$sql = "DROP TABLE IF EXISTS `tbl_addon`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_addon` (
			`addon_id` int (11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`shop_id` int(11) NOT NULL,
			`name_tc` varchar(255) DEFAULT NULL,
			`name_en` varchar(255) DEFAULT NULL,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);

		$sql = "SELECT * FROM add_on_categories";
		$res = $this->odb->get_Sql($sql);

		foreach($res as $row){
			$data = [
				"shop_id" => 1,
				"name_en" => $row['name_en'],
				"name_tc" => $row['name_zh_hk']
			];

			$id = $this->DB->insert_db("tbl_addon", $data);
			echo "tbl_addon row: $id || insert completed<br/>";
		}
		echo "tbl_addon: DONE <hr/>";
	}

	function tbl_admin(){
		$sql = "DROP TABLE IF EXISTS `tbl_admin`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_admin` (
			`admin_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`username` varchar(20) DEFAULT NULL,
			`email` varchar(100) DEFAULT NULL,
			`password` varchar(32) DEFAULT NULL,
			`two_factor_code` varchar(20) DEFAULT NULL,
			`last_login_on` datetime DEFAULT NULL,
			`last_login_ip` varchar(30) DEFAULT NULL,
			`last_login_device` varchar(255) DEFAULT NULL,
			`last_login_location` varchar(30) DEFAULT NULL,
			`role_id` varchar(10) DEFAULT NULL,
			`suspended` varchar(1) DEFAULT 'N',
			`activate` varchar(1) DEFAULT 'Y',
			`default_shop_id` int(11) DEFAULT 1,
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);
	
		echo "tbl_admin: Without data migration, data will resume default<br/>";

		$sql = "INSERT INTO `tbl_admin` (`username`, `email`, `password`, `two_factor_code`, `last_login_on`, `last_login_ip`, `last_login_device`, `last_login_location`, `role_id`, `suspended`, `activate`) VALUES
		('admin', NULL, '5f4dcc3b5aa765d61d8327deb882cf99', 'QQUZ3VZXVXFTLQA4', NULL, NULL, NULL, NULL, '1', 'N', 'Y'),
		('gift', NULL, '5f4dcc3b5aa765d61d8327deb882cf99', 'QQUZ3VZXVXFTLQA4', NULL, NULL, NULL, NULL, '2', 'N', 'Y'),
		('zuri', NULL, '5f4dcc3b5aa765d61d8327deb882cf99', 'QQUZ3VZXVXFTLQA4', NULL, NULL, NULL, NULL, '3', 'N', 'Y'),
		('lotaipo', NULL, '5f4dcc3b5aa765d61d8327deb882cf99', 'QQUZ3VZXVXFTLQA4', NULL, NULL, NULL, NULL, '4', 'N', 'Y'),
		('viewer', NULL, '5f4dcc3b5aa765d61d8327deb882cf99', 'QQUZ3VZXVXFTLQA4', NULL, NULL, NULL, NULL, '5', 'N', 'Y'),
		('singsales', NULL, '5f4dcc3b5aa765d61d8327deb882cf99', NULL, NULL, NULL, NULL, NULL, '6', 'N', 'Y');";
		$this->mysqli->query($sql);		
		

		echo "tbl_admin: DONE <hr/>";
	}

	function tbl_admin_role(){
		$sql = "DROP TABLE IF EXISTS `tbl_admin_role`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_admin_role` (
			`role_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`adm_name` varchar(20) DEFAULT NULL,
			`gift_right` longtext DEFAULT NULL,
			`zuri_right` longtext DEFAULT NULL,
			`ltp_right` longtext DEFAULT NULL,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);
	
		echo "tbl_admin_role: Without data migration, data will resume default<br/>";
		
		$sql = "INSERT INTO `tbl_admin_role` (`role_id`, `adm_name`, `gift_right`, `zuri_right`, `ltp_right`) VALUES
		(1, '超級管理員', '{\"quickorder\":50,\"holiday\":50,\"gift_card\":50,\"dashboard\":50,\"sku\":50,\"item\":50,\"deli\":50,\"style\":50,\"shop\":50,\"nav\":50,\"page\":50,\"cate\":50,\"product\":50,\"addon\":50,\"member\":50,\"membership\":50,\"voucher\":50,\"coupon\":50,\"company_discount\":50,\"gift\":50,\"card\":null,\"new_member_discount\":50,\"admin\":50,\"admin_role\":50,\"greeting\":50,\"order\":50}', '{\"quickorder\":50,\"holiday\":10,\"gift_card\":null,\"dashboard\":50,\"sku\":50,\"item\":50,\"deli\":10,\"style\":50,\"shop\":50,\"nav\":50,\"page\":50,\"cate\":50,\"product\":50,\"addon\":50,\"member\":50,\"membership\":10,\"voucher\":50,\"coupon\":10,\"company_discount\":50,\"gift\":null,\"card\":null,\"new_member_discount\":10,\"admin\":10,\"admin_role\":10,\"greeting\":10,\"order\":10}', '{\"quickorder\":50,\"holiday\":10,\"gift_card\":null,\"dashboard\":50,\"sku\":50,\"item\":50,\"deli\":10,\"style\":50,\"shop\":10,\"nav\":50,\"page\":50,\"cate\":50,\"product\":50,\"addon\":50,\"member\":50,\"membership\":10,\"voucher\":50,\"coupon\":10,\"company_discount\":50,\"gift\":null,\"card\":null,\"new_member_discount\":10,\"admin\":10,\"admin_role\":10,\"greeting\":10,\"order\":10}'),
		(2, 'The Gift 管理員', '{\"dashboard\":50,\"sku\":50,\"item\":50,\"style\":50,\"shop_info\":50,\"nav\":50,\"page\":50,\"cate\":50,\"product\":50,\"addon\":50,\"combo_product\":50,\"normal_member\":50,\"company_member\":50,\"member\":50,\"voucher\":50,\"coupon_code\":50,\"company_discount\":50,\"point\":50,\"gift_card\":50}', '{\"dashboard\":10,\"sku\":10,\"item\":10,\"style\":10,\"shop_info\":10,\"nav\":10,\"page\":10,\"cate\":10,\"product\":10,\"addon\":10,\"combo_product\":10,\"normal_member\":10,\"company_member\":10,\"member\":10,\"voucher\":10,\"coupon_code\":10,\"company_discount\":10,\"point\":10,\"gift_card\":10}', '{\"dashboard\":10,\"sku\":10,\"item\":10,\"style\":10,\"shop_info\":10,\"nav\":10,\"page\":10,\"cate\":10,\"product\":10,\"addon\":10,\"combo_product\":10,\"normal_member\":10,\"company_member\":10,\"member\":10,\"voucher\":10,\"coupon_code\":10,\"company_discount\":10,\"point\":10,\"gift_card\":10}'),
		(3, 'Little Zuri 管理員', '{\"dashboard\":10,\"sku\":10,\"item\":10,\"style\":10,\"shop_info\":10,\"nav\":10,\"page\":10,\"cate\":10,\"product\":10,\"addon\":10,\"combo_product\":10,\"normal_member\":10,\"company_member\":10,\"member\":10,\"voucher\":10,\"coupon_code\":10,\"company_discount\":10,\"point\":10,\"gift_card\":10}', '{\"dashboard\":50,\"sku\":50,\"item\":50,\"style\":50,\"shop_info\":50,\"nav\":50,\"page\":50,\"cate\":50,\"product\":50,\"addon\":50,\"combo_product\":50,\"normal_member\":50,\"company_member\":50,\"member\":50,\"voucher\":50,\"coupon_code\":50,\"company_discount\":50,\"point\":50,\"gift_card\":50}', '{\"dashboard\":10,\"sku\":10,\"item\":10,\"style\":10,\"shop_info\":10,\"nav\":10,\"page\":10,\"cate\":10,\"product\":10,\"addon\":10,\"combo_product\":10,\"normal_member\":10,\"company_member\":10,\"member\":10,\"voucher\":10,\"coupon_code\":10,\"company_discount\":10,\"point\":10,\"gift_card\":10}'),
		(4, 'LoTaiPo 管理員', '{\"dashboard\":10,\"sku\":10,\"item\":10,\"style\":10,\"shop_info\":10,\"nav\":10,\"page\":10,\"cate\":10,\"product\":10,\"addon\":10,\"combo_product\":10,\"normal_member\":10,\"company_member\":10,\"member\":10,\"voucher\":10,\"coupon_code\":10,\"company_discount\":10,\"point\":10,\"gift_card\":10}', '{\"dashboard\":10,\"sku\":10,\"item\":10,\"style\":10,\"shop_info\":10,\"nav\":10,\"page\":10,\"cate\":10,\"product\":10,\"addon\":10,\"combo_product\":10,\"normal_member\":10,\"company_member\":10,\"member\":10,\"voucher\":10,\"coupon_code\":10,\"company_discount\":10,\"point\":10,\"gift_card\":10}', '{\"dashboard\":50,\"sku\":50,\"item\":50,\"style\":50,\"shop_info\":50,\"nav\":50,\"page\":50,\"cate\":50,\"product\":50,\"addon\":50,\"combo_product\":50,\"normal_member\":50,\"company_member\":50,\"member\":50,\"voucher\":50,\"coupon_code\":50,\"company_discount\":50,\"point\":50,\"gift_card\":50}'),
		(5, '觀察者', '{\"dashboard\":30,\"sku\":30,\"item\":30,\"style\":30,\"shop_info\":30,\"nav\":30,\"page\":30,\"cate\":30,\"product\":30,\"addon\":30,\"combo_product\":30,\"normal_member\":30,\"company_member\":30,\"member\":30,\"voucher\":30,\"coupon_code\":30,\"company_discount\":30,\"point\":30,\"gift_card\":30}', '{\"dashboard\":30,\"sku\":30,\"item\":30,\"style\":30,\"shop_info\":30,\"nav\":30,\"page\":30,\"cate\":30,\"product\":30,\"addon\":30,\"combo_product\":30,\"normal_member\":30,\"company_member\":30,\"member\":30,\"voucher\":30,\"coupon_code\":30,\"company_discount\":30,\"point\":30,\"gift_card\":30}', '{\"dashboard\":30,\"sku\":30,\"item\":30,\"style\":30,\"shop_info\":30,\"nav\":30,\"page\":30,\"cate\":30,\"product\":30,\"addon\":30,\"combo_product\":30,\"normal_member\":30,\"company_member\":30,\"member\":30,\"voucher\":30,\"coupon_code\":30,\"company_discount\":30,\"point\":30,\"gift_card\":30}'),
		(6, 'The Gift sales', '{\"quickorder\":50,\"holiday\":10,\"gift_card\":10,\"dashboard\":10,\"sku\":10,\"item\":10,\"deli\":10,\"style\":10,\"shop\":10,\"nav\":10,\"page\":10,\"cate\":10,\"product\":10,\"addon\":10,\"member\":10,\"membership\":10,\"voucher\":10,\"coupon\":10,\"company_discount\":10,\"gift\":10,\"card\":null,\"new_member_discount\":10,\"admin\":10,\"admin_role\":10,\"greeting\":10,\"order\":10}', '{\"quickorder\":10,\"holiday\":10,\"gift_card\":10,\"dashboard\":10,\"sku\":10,\"item\":10,\"deli\":10,\"style\":10,\"shop\":10,\"nav\":10,\"page\":10,\"cate\":10,\"product\":10,\"addon\":10,\"member\":10,\"membership\":10,\"voucher\":10,\"coupon\":10,\"company_discount\":10,\"gift\":10,\"card\":null,\"new_member_discount\":10,\"admin\":10,\"admin_role\":10,\"greeting\":10,\"order\":10}', '{\"quickorder\":10,\"holiday\":10,\"gift_card\":10,\"dashboard\":10,\"sku\":10,\"item\":10,\"deli\":10,\"style\":10,\"shop\":10,\"nav\":10,\"page\":10,\"cate\":10,\"product\":10,\"addon\":10,\"member\":10,\"membership\":10,\"voucher\":10,\"coupon\":10,\"company_discount\":10,\"gift\":10,\"card\":null,\"new_member_discount\":10,\"admin\":10,\"admin_role\":10,\"greeting\":10,\"order\":10}');";
		$this->mysqli->query($sql);

		echo "tbl_admin_role: DONE <hr/>";
	}


	function tbl_cate(){
		$sql = "DROP TABLE IF EXISTS `tbl_cate`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_cate` (
			`cate_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`shop_id` int(11) NOT NULL, -- gift || ltp || zuri, in pattern: gift,ltp
			`parent_cate_id` int(11) DEFAULT NULL,
			`name_tc` varchar(255) DEFAULT NULL,
			`name_en` varchar(255) DEFAULT NULL,
			`cate_banner` varchar(255) DEFAULT NULL,
			`sort` int(11) DEFAULT NULL,
			`show` varchar(1) DEFAULT 'Y',
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);
		
		$sql = "SELECT * 
				FROM product_categories
				WHERE visibility = 1 ";
		$res = $this->odb->get_Sql($sql);
			
		foreach($res as $row){
			$data = [
				"shop_id" => 1,
				"name_tc" => $row['name_zh_hk'],
				"name_en" => $row['name_en'],
				"sort" => $row['list_order'],
			];

			$parent_cate_id = $this->DB->insert_db("tbl_cate", $data);
			echo "tbl_cate row: $parent_cate_id || insert completed<br/>";

			$sql = "SELECT * 
				FROM product_subcategories
				WHERE visibility = 1 AND product_category_id = $parent_cate_id";
			
			$res2 = $this->odb->get_Sql($sql);
			foreach ($res2 as $row2){
				$data = [
					"shop_id" => 1,
					"parent_cate_id" => $parent_cate_id,
					"name_tc" => $row2['name_zh_hk'],
					"name_en" => $row2['name_en'],
					"sort" => $row2['list_order'],
				];

				$id = $this->DB->insert_db("tbl_cate", $data);

				echo "tbl_cate row: $id || pid: $parent_cate_id || insert completed<br/>";
			}


		}
		echo "tbl_cate: DONE <hr/>";
	}



	function tbl_combo_product(){}
	function tbl_company_discount(){}
	function tbl_coupon(){
		$sql = "DROP TABLE IF EXISTS `tbl_coupon`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_coupon` (
			`coupon_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`shop_id` int(11) NOT NULL,
			`adm_name` varchar(255) DEFAULT NULL,
			`discount` int(11) NOT NULL,
			`type` varchar(1) NOT NULL DEFAULT 'A', -- A for amt || P for percentage off
			`minimum_order_amt` int(11) DEFAULT NULL,
			`code` varchar(255) DEFAULT NULL,
			`expiry_date` datetime DEFAULT NULL,
			`allow_voucher` varchar(1) NOT NULL DEFAULT 'Y',
			`member_only` varchar(1) NOT NULL DEFAULT 'Y',
			`usage_limit` varchar(255) DEFAULT NULL,
			`sales_admin_id` int (11) DEFAULT NULL,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);
		

		$sql = "SELECT * FROM coupon_codes";
		$res = $this->odb->get_Sql($sql);

		foreach($res as $row){
			$data = [
				"shop_id" => 1,
				"adm_name" => $row['code'],
				"minimum_order_amt" => $row['minimum_amount'],
				"code" => $row['code'],
			];

			if ($row['usage_time_limit'])
				$data["usage_limit"] = $row['usage_time_limit'];

			if ($row['expiry_date'])
				$data["expiry_date"] = $row['expiry_date'];

			if ($row['type'] == 1){
				$data['type'] = 'P';
				$data['discount'] = $row['percentage_off'];
			} else if ($row['type'] == 2){
				$data['type'] = 'A';
				$data['discount'] = $row['amount'];
			}

			if ($row['is_only_allow_member_use'] == 1)
				$data['member_only'] = 'Y';
			else 
				$data['member_only'] = 'N';

			if ($row['is_allow_to_use_with_user_voucher'] == 1)
				$data['allow_voucher'] = 'Y';
			else 
				$data['allow_voucher'] = 'N';

			$id = $this->DB->insert_db("tbl_coupon", $data);
			echo "tbl_coupon row: $id || insert completed<br/>";
		}
		echo "tbl_coupon: DONE <hr/>";
	}

	function tbl_deli_cust_grp(){
		$sql = "DROP TABLE IF EXISTS `tbl_deli_cust_grp`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_deli_cust_grp`(
			`grp_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`shop_id` int(11) DEFAULT NULL, 
			`adm_name` varchar(255) DEFAULT NULL,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
		";
		$this->mysqli->query($sql);
		

		$sql = "SELECT * FROM custom_delivery_rule_groups";
		$res = $this->odb->get_Sql($sql);

		foreach($res as $row){
			$data = [
				"shop_id" => 1,
				"adm_name" => $row['name'],
			];

			$id = $this->DB->insert_db("tbl_deli_cust_grp", $data);
			echo "tbl_deli_cust_grp row: $id || insert completed<br/>";
		}
		echo "tbl_deli_cust_grp: DONE <hr/>";
	}


	function tbl_deli_cust_region(){}
	function tbl_deli_cust_timeslot(){}
	function tbl_deli_district(){}
	
	function tbl_deli_item(){}
	function tbl_deli_region(){
		$sql = "DROP TABLE IF EXISTS `tbl_deli_region`;";
		$this->mysqli->query($sql);
		$sql = "DROP TABLE IF EXISTS `tbl_deli_district`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_deli_region` (
			`region_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`shop_id` int(11) NOT NULL,
			`district_id` int(11) NOT NULL,
			`name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			`name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			`shipping_fee` int(11) DEFAULT '0',
			`show_in_web` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
			`address_required` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
			`activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
		  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_deli_district`(
			`district_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`shop_id` int(11) DEFAULT NULL,
			`name_tc` varchar(255) DEFAULT NULL, 
			`name_en` varchar(255) DEFAULT NULL, 
			`show_in_web` varchar(1) DEFAULT 'Y',
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);
		
		$sql = "INSERT INTO `tbl_deli_region` (`shop_id`, `region_id`, `district_id`, `name_tc`, `name_en`, `shipping_fee`, `show_in_web`, `address_required`, `activate`, `updated_by_id`, `last_update`, `create_datetime`) VALUES
		(1,1, 11, '香港仔', 'ABERDEEN', 50, 'Y', 'Y', 'Y', NULL, '2021-09-10 07:00:56', '2021-09-07 07:39:36'),
		(1,2, 15, '金鐘', 'ADMIRALTY', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,3, 11, '鴨脷洲', 'AP LEI CHAU', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,4, 15, '銅鑼灣', 'CAUSEWAY BAY', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,5, 1, '中環', 'CENTRAL', 10, 'Y', 'Y', 'Y', 1, '2021-09-20 06:09:53', '2021-09-07 07:39:36'),
		(1,6, 2, '柴灣', 'CHAI WAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,7, 11, '舂坎角', 'CHUNG HOM KOK', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,8, 11, '深水灣', 'DEEP WATER BAY', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,9, 2, '炮台山', 'FORTRESS HILL', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,10, 1, '跑馬地', 'HAPPY VALLEY', 20, 'Y', 'Y', 'Y', 1, '2021-09-20 06:11:03', '2021-09-07 07:39:36'),
		(1,11, 2, '杏花邨', 'HENG FA CHUEN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,12, 1, '堅尼地城', 'KENNEDY TOWN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,13, 11, '利東', 'LEI TUNG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,14, 1, '半山', 'MID-LEVEL', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,15, 11, '中灣', 'MIDDLE BAY', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,16, 2, '北角', 'NORTH POINT', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,17, 11, '薄扶林', 'POK FU LAM', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,18, 2, '鰂魚涌', 'QUARRY BAY', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,19, 11, '淺水灣', 'REPULSE BAY', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,20, 1, '西環', 'SAI WAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,21, 1, '西灣河', 'SAI WAN HO', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,22, 1, '西營盤', 'SAI YING PUN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,23, 2, '筲箕灣', 'SHAU KEI WAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,24, 11, '石澳', 'SHEK O', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,25, 11, '石排灣', 'SHEK PAI WAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,26, 1, '石塘咀', 'SHEK TONG TSUI', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,27, 1, '上環', 'SHEUNG WAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,28, 11, '壽臣山', 'SHOUSON HILL', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,29, 2, '小西灣', 'SIU SAI WAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,30, 11, '赤柱', 'STANLEY', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,31, 15, '大坑', 'TAI HANG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,32, 2, '太古', 'TAI KOO', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,33, 2, '太古城', 'TAI KOO SHING', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,34, 11, '大潭', 'TAI TAM', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,35, 11, '大潭水庫', 'TAI TAM RESERVOIR', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,36, 1, '山頂', 'THE PEAK', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,37, 15, '天后', 'TIN HAU', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,38, 11, '田灣', 'TIN WAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,39, 11, '華富', 'WAH FU', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,40, 11, '華貴', 'WAH KWAI', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,41, 15, '灣仔', 'WAN CHAI', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,42, 1, '西區', 'WESTERN DISTRICT', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,43, 11, '黃竹坑', 'WONG CHUK HANG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,44, 3, '赤臘角', 'CHEK LAP KOK', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,45, 3, '長洲', 'CHEUNG CHAU', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,46, 3, '愉景灣', 'DISCOVERY BAY', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,47, 3, '南丫島', 'LAMMA ISLAND', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,48, 3, '大嶼山', 'LANTAU ISLAND', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,49, 3, '馬灣', 'MA WAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,50, 3, '其他離島', 'OUTLYING ISLANDS', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,51, 3, '坪洲', 'PENG CHAU', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,52, 3, '蒲台島', 'PO TOI ISLAND GROUP', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,53, 3, '貝澳', 'PUI O', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,54, 3, '石角咀', 'SHEK KOK TSUI', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,55, 3, '索罟群島', 'SOKO ISLANDS', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,56, 3, '塘福', 'TONG FUK', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,57, 3, '東涌', 'TUNG CHUNG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,58, 6, '茶果嶺', 'CHA KWO LING', 150, 'Y', 'Y', 'Y', 1, '2021-09-07 07:56:53', '2021-09-07 07:39:36'),
		(1,59, 9, '長沙灣', 'CHEUNG SHA WAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,60, 16, '彩虹', 'CHOI HUNG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,61, 8, '清水灣', 'CLEAR WATER BAY', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,62, 16, '鑽石山', 'DIAMOND HILL', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,63, 16, '鳳凰', 'FUNG WONG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,64, 4, '何文田', 'HO MAN TIN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,65, 4, '紅磡', 'HUNG HOM', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,66, 17, '佐敦', 'JORDAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,67, 6, '九龍灣', 'KOWLOON BAY', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,68, 4, '九龍城', 'KOWLOON CITY', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,69, 4, '九龍塘', 'KOWLOON TONG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,70, 6, '觀塘', 'KWUN TONG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,71, 9, '荔枝角', 'LAI CHI KOK', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,72, 6, '藍田', 'LAM TIN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,73, 16, '樂富', 'LOK FU', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,74, 9, '美孚', 'MEI FOO', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,75, 17, '旺角', 'MONG KOK', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,76, 16, '牛池灣', 'NGAU CHI WAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,77, 6, '牛頭角', 'NGAU TAU KOK', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,78, 17, '太子', 'PRINCE EDWARD', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,79, 8, '西貢', 'SAI KUNG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,80, 16, '新蒲崗', 'SAN PO KONG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,81, 9, '深水埗', 'SHAM SHUI PO', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,82, 9, '石硤尾', 'SHEK KIP MEI', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,83, 18, '石崗', 'SHEK KONG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,84, 17, '大角咀', 'TAI KOK TSUI', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,85, 4, '土瓜灣', 'TO KWA WAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,86, 8, '將軍澳', 'TSEUNG KWAN O', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,87, 17, '尖沙咀', 'TSIM SHA TSUI', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,88, 16, '慈雲山', 'TSZ WAN SHAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,89, 16, '東頭', 'TUNG TAU', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,90, 16, '橫頭磡', 'WANG TAU HOM', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,91, 16, '黃大仙', 'WONG TAI SIN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,92, 17, '油麻地', 'YAU MA TEI', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,93, 6, '油塘', 'YAU TONG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,94, 9, '又一村', 'YAU YAT CHUEN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,95, 10, '中文大學', 'CHINESE UNIVERSITY', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,96, 12, '中文大學', 'CHINESE UNIVERSITY', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,97, 7, '粉嶺', 'FANLING', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,98, 10, '火炭', 'FO TAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,99, 18, '廈村', 'HA TSUEN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,100, 18, '錦田', 'KAM TIN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,101, 5, '葵涌', 'KWAI CHUNG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,102, 5, '葵芳', 'KWAI FONG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,103, 5, '葵興', 'KWAI HING', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,104, 5, '葵盛', 'KWAI SHING', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,105, 5, '荔景', 'LAI KING', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,106, 5, '麗瑤', 'LAI YIU', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,107, 13, '梨木樹', 'LEI MUK SHUE', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,108, 14, '龍鼓灘', 'LUNG KWU TAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,109, 10, '馬鞍山', 'MA ON SHAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,110, 5, '安蔭', 'ON YAM', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,111, 7, '沙頭角', 'SHA TAU KOK', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,112, 13, '深井', 'SHAM TSENG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,113, 10, '沙田', 'SHATIN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,114, 5, '石籬', 'SHEK LEI', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,115, 10, '石門', 'SHEK MUN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,116, 7, '石湖墟', 'SHEK WU HUI', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,117, 5, '石蔭', 'SHEK YAM', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,118, 7, '上水', 'SHEUNG SHUI', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,119, 10, '小瀝源', 'SIU LEK YUEN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,120, 14, '大欖涌', 'TAI LAM CHUNG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,121, 12, '大埔', 'TAI PO', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,122, 10, '大圍', 'TAI WAI', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,123, 13, '大窩口', 'TAI WO HAU', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,124, 18, '天水圍', 'TIN SHUI WAI', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,125, 13, '青龍頭', 'TSING LUNG TAU', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,126, 5, '青衣', 'TSING YI', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,127, 13, '荃灣', 'TSUEN WAN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,128, 14, '屯門', 'TUEN MUN', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,129, 18, '元朗', 'YUEN LONG', 0, 'Y', 'Y', 'Y', NULL, '2021-09-07 07:39:36', '2021-09-07 07:39:36'),
		(1,130, 0, NULL, NULL, 0, 'Y', 'Y', 'Y', 1, '2021-09-10 08:50:19', '2021-09-10 08:50:19'),
		(1,131, 0, 'abc', 'abc', 123, 'Y', 'Y', 'Y', 1, '2021-09-10 08:50:39', '2021-09-10 08:50:39'),
		(1,132, 0, 'tc name', 'en name', 123435, 'Y', 'Y', 'Y', 1, '2021-09-10 08:52:22', '2021-09-10 08:52:22'),
		(1,133, 0, '海獺', 'sea otter', 100, 'Y', 'Y', 'Y', 1, '2021-09-10 08:57:29', '2021-09-10 08:57:29'),
		(1,134, 0, '哈嘍', 'hello', 123456, 'Y', 'Y', 'Y', 1, '2021-09-10 09:01:44', '2021-09-10 09:01:44'),
		(1,135, 0, NULL, NULL, 0, 'Y', 'Y', 'Y', 1, '2021-09-10 09:03:35', '2021-09-10 09:03:35'),
		(1,136, 0, NULL, NULL, 0, 'Y', 'Y', 'Y', 1, '2021-09-10 09:07:57', '2021-09-10 09:07:57'),
		(1,137, 0, NULL, NULL, 0, 'Y', 'Y', 'Y', 1, '2021-09-10 09:10:25', '2021-09-10 09:10:25'),
		(1,138, 0, 'hello tc', 'hello en ', 123456, 'Y', 'Y', 'Y', 1, '2021-09-10 09:10:42', '2021-09-10 09:10:42'),
		(1,139, 0, 'd sa da s', 'da s da s', 1231312, 'Y', 'Y', 'Y', 1, '2021-09-10 09:13:12', '2021-09-10 09:13:12'),
		(1,140, 0, '哦搲奧運', 'fdsf', 123124, 'Y', 'Y', 'Y', 1, '2021-09-10 09:14:09', '2021-09-10 09:14:09'),
		(1,141, 0, '蘋果a', 'sea otter', 100, 'Y', 'Y', 'Y', 1, '2021-09-10 09:29:21', '2021-09-10 09:29:21'),
		(1,142, 1, '蘋果a', 'sea otter', 100, 'Y', 'Y', 'Y', 1, '2021-09-10 09:30:17', '2021-09-10 09:30:17'),
		(1,143, 1, 'abc', 'abc', 123, 'Y', 'Y', 'Y', 1, '2021-09-10 09:30:49', '2021-09-10 09:30:49');";
		$this->mysqli->query($sql);

		$sql = "INSERT INTO `tbl_deli_district` (`district_id`, `shop_id`, `name_tc`, `name_en`, `show_in_web`, `activate`, `updated_by_id`, `last_update`, `create_datetime`) VALUES
		(1, 1, '中西區', 'Central And Western', 'Y', 'Y', 1, '2021-09-13 07:52:16', '2021-09-07 07:04:26'),
		(2, 1, '東區', 'Eastern District', 'Y', 'Y', NULL, '2021-09-07 07:04:26', '2021-09-07 07:04:26'),
		(3, 1, '離島區', 'Island District', 'Y', 'Y', NULL, '2021-09-07 07:04:26', '2021-09-07 07:04:26'),
		(4, 1, '九龍城區', 'Kowloon City', 'Y', 'Y', NULL, '2021-09-07 07:04:26', '2021-09-07 07:04:26'),
		(5, 1, '葵青區', 'Kwai Tsing', 'Y', 'Y', NULL, '2021-09-07 07:04:26', '2021-09-07 07:04:26'),
		(6, 1, '觀塘區a', 'Kwun Tong', 'Y', 'Y', 1, '2021-09-07 07:54:49', '2021-09-07 07:04:26'),
		(7, 1, '北區', 'North District', 'Y', 'Y', NULL, '2021-09-07 07:04:26', '2021-09-07 07:04:26'),
		(8, 1, '西貢區', 'Sai Kung', 'Y', 'Y', NULL, '2021-09-07 07:04:26', '2021-09-07 07:04:26'),
		(9, 1, '深水埗區', 'Sham Shui Po', 'Y', 'Y', NULL, '2021-09-07 07:04:26', '2021-09-07 07:04:26'),
		(10, 1, '沙田區', 'Shatin', 'Y', 'Y', NULL, '2021-09-07 07:04:26', '2021-09-07 07:04:26'),
		(11, 1, '南區', 'Southern District', 'Y', 'Y', NULL, '2021-09-07 07:04:26', '2021-09-07 07:04:26'),
		(12, 1, '大埔區', 'Tai Po', 'Y', 'Y', NULL, '2021-09-07 07:04:26', '2021-09-07 07:04:26'),
		(13, 1, '荃灣區', 'Tsuen Wan', 'Y', 'Y', NULL, '2021-09-07 07:04:26', '2021-09-07 07:04:26'),
		(14, 1, '屯門區', 'Tuen Mun', 'Y', 'Y', NULL, '2021-09-07 07:04:26', '2021-09-07 07:04:26'),
		(15, 1, '灣仔區', 'Wan Chai', 'Y', 'Y', NULL, '2021-09-07 07:04:26', '2021-09-07 07:04:26'),
		(16, 1, '黃大仙區', 'Wong Tai Sin', 'Y', 'Y', NULL, '2021-09-07 07:04:26', '2021-09-07 07:04:26'),
		(17, 1, '油尖旺區', 'Yau Tsim Mong', 'Y', 'Y', NULL, '2021-09-07 07:04:26', '2021-09-07 07:04:26'),
		(18, 1, '元朗區', 'Yuen Long', 'Y', 'Y', NULL, '2021-09-07 07:04:26', '2021-09-07 07:04:26'),
		(19, 1, NULL, NULL, 'Y', 'Y', 1, '2021-09-10 07:34:09', '2021-09-10 07:34:09'),
		(20, 1, NULL, NULL, 'Y', 'Y', 1, '2021-09-10 07:36:27', '2021-09-10 07:36:27'),
		(21, 1, 'ffff', 'ffff', 'Y', 'Y', 1, '2021-09-10 07:39:02', '2021-09-10 07:39:02'),
		(22, 1, NULL, NULL, 'Y', 'Y', 1, '2021-09-10 08:28:54', '2021-09-10 08:28:54'),
		(23, 1, NULL, NULL, 'Y', 'Y', 1, '2021-09-10 08:29:03', '2021-09-10 08:29:03');";
		$this->mysqli->query($sql);

		echo "tbl_deli_region: DONE <hr/>";
	}
	function tbl_deli_timeslot(){
		$sql = "DROP TABLE IF EXISTS `tbl_deli_timeslot`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_deli_timeslot` (
			`slot_id` int (11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`shop_id` int(11) NOT NULL,
			`adm_name` varchar(255) DEFAULT NULL,
			`start_time` TIME NOT NULL,
			`end_time` TIME NOT NULL,
			`ava_b4_day` int(11) DEFAULT NULL,
			`ava_b4_cutoff` time DEFAULT NULL,
			`deli_fee` int(11) DEFAULT NULL, 
			`same_day_deli_fee` int(11) DEFAULT NULL, 
			`default` varchar(1) DEFAULT 'Y',
			`online_pay_same_day` varchar(1) DEFAULT 'N', 
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);
		

		$sql = "SELECT * FROM delivery_time_slots";
		$res = $this->odb->get_Sql($sql);

		foreach($res as $row){
			$data = [];
			$data['shop_id'] = 1;
			$data['adm_name'] = $row['name'];
			$data['start_time'] = $row['start_time'];
			$data['end_time'] = $row['end_time'];
			$data['deli_fee'] = $row['delivery_fee'];
			$data['same_day_deli_fee'] = $row['same_day_delivery_fee'];
			isset($row['is_default']) ? $data['default'] = ($row['is_default'] == 1 ? 'Y': 'N'): null;
			isset($row['is_online_payment_on_same_day']) ? $data['online_pay_same_day'] = ($row['is_online_payment_on_same_day'] == 1 ? 'Y' : 'N') : null;

			$id = $this->DB->insert_db("tbl_deli_timeslot", $data);
			echo "tbl_deli_timeslot row: $id || insert completed<br/>";
		}
		echo "tbl_deli_region: DONE <hr/>";
	}

	function tbl_gift(){}
	function tbl_gift_card(){}
	function tbl_gift_card_key(){}
	function tbl_greeting(){
		$sql = "DROP TABLE IF EXISTS `tbl_greeting_cate`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_greeting_cate` (
			`cate_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`shop_id` int(11) NOT NULL,
			`name_tc` varchar(255) DEFAULT NULL, 
			`name_en` varchar(255) DEFAULT NULL, 
			`sort` int(11) NOT NULL,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
			) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);

		$sql = "SELECT * FROM suggested_message_categories";
		$res = $this->odb->get_Sql($sql);
		foreach($res as $row){
			$data = [];
			$data['shop_id'] = 1;
			$data["name_en"] = $row['name_en'];
			$data["name_tc"] = $row['name_zh_hk'];
			$data["sort"] = $row['list_order'];
			
			$id = $this->DB->insert_db("tbl_greeting_cate", $data);
			echo "tbl_greeting_cate row: $id || insert completed<br/>";
		}
		echo "tbl_greeting_cate: DONE <hr/>";

		$sql = "DROP TABLE IF EXISTS `tbl_greeting_subcate`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_greeting_subcate` (
			`subcate_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`parent_cate_id` int(11) NOT NULL,
			`shop_id` int(11) NOT NULL,
			`name_tc` varchar(255) DEFAULT NULL, 
			`name_en` varchar(255) DEFAULT NULL, 
			`sort` int(11) NOT NULL,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
			) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);

		$sql = "SELECT * FROM suggested_message_subcategories";
		$res = $this->odb->get_Sql($sql);

		foreach($res as $row){
			$data = [];
			$data['shop_id'] = 1;
			$data["name_en"] = $row['name_en'];
			$data["name_tc"] = $row['name_zh_hk'];
			$data["sort"] = $row['list_order'];
			$data['parent_cate_id'] = $row['suggested_message_category_id'];


			$id = $this->DB->insert_db("tbl_greeting_subcate", $data);
			echo "tbl_greeting_subcate row: $id || insert completed<br/>";
		}
		echo "tbl_greeting_subcate: DONE <hr/>";

		$sql = "DROP TABLE IF EXISTS `tbl_greeting`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_greeting` (
			`greeting_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`shop_id` int(11) NOT NULL,
			`subcate_id` int(11) NOT NULL,
			`msg` varchar(255) DEFAULT NULL, 
			`sort` int(11) NOT NULL,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
			) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);

		$sql = "SELECT * FROM suggested_messages";
		$res = $this->odb->get_Sql($sql);

		foreach($res as $row){
			$data = [];
			$data['shop_id'] = 1;
			$data["subcate_id"] = $row['suggested_message_subcategory_id'];
			$data["msg"] = $row['message'];
			$data["sort"] = $row['list_order'];


			$id = $this->DB->insert_db("tbl_greeting", $data);
			echo "tbl_greeting row: $id || insert completed<br/>";
		}
		echo "tbl_greeting: DONE <hr/>";
	}

	function tbl_item(){
		$sql = "DROP TABLE IF EXISTS `tbl_item`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_item` (
			`item_id` int (11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`sku_prefix` varchar(255) NOT NULL,
			`name_tc` varchar(255) DEFAULT NULL,
			`name_en` varchar(255) DEFAULT NULL,
			`item_img` varchar(255) DEFAULT NULL, 
			`qty` int(11) DEFAULT NULL,
			`replace_sku_id` int(11) DEFAULT NULL,
			`is_replace_period` varchar(1) DEFAULT 'F',
			`replace_period` timestamp DEFAULT NULL,
			`replace` varchar(1) DEFAULT 'F',
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);

		echo "tbl_item: Without data migration, data will resume default<br/>";

		$sql = "INSERT INTO `tbl_item` (`item_id`, `sku_prefix`, `name_tc`, `name_en`, `item_img`, `qty`, `replace_sku_id`, `is_replace_period`, `replace_period`, `replace`, `activate`) VALUES
		(1, 'apple', '蘋果', 'Apple', '../upload/item/1/1630403512apple-5012196_1280.png', 30, 12, 'Y', '2021-09-28', 'N', 'Y'),
		(2, 'orange', '橙', 'orange', '../upload/item/2/1630459125orange.png', 50, NULL, 'N', NULL, 'N', 'Y'),
		(3, 'banana', '蕉', 'Banana', '../upload/item/3/1630459136HK000132-Banana.jpeg', 100, 13, 'Y', '2021-09-28', 'Y', 'Y'),
		(4, 'sea_otter', '海獺', 'sea otter', '../upload/item/4/1631773057otter4.jpeg', NULL, 6, 'N', NULL, 'N', 'Y'),
		(5, 'fengshui_pear', '豐水梨', 'Fengshui Pear', '../upload/item/5/16306363812sq000o01400p0422s3.jpeg', NULL, NULL, 'N', NULL, 'N', 'Y'),
		(6, 'pitaya', '火龍果', 'Pitaya', '../upload/item/6/16306364575b5fa523-ae66-4184-9e18-ab9584762a4a.jpeg', NULL, NULL, 'N', NULL, 'N', 'Y'),
		(7, 'tiantao', '天桃', 'tiantao', '../upload/item/7/1630636594tiantao.png', NULL, NULL, 'N', NULL, 'N', 'Y'),
		(8, 'cloth', '布冧', 'Cloth', '../upload/item/8/1630636634cloth.png', NULL, NULL, 'N', NULL, 'N', 'Y'),
		(9, 'fragrant_pear', '香梨', 'Fragant pear', '../upload/item/9/1630636702fragant_pear.png', NULL, NULL, 'N', NULL, 'N', 'Y'),
		(10, 'blueberry', '藍莓', 'blueberry', '../upload/item/10/1630636738blueberry.png', NULL, NULL, 'N', NULL, 'N', 'Y'),
		(11, 'honeydew', '蜜瓜', 'Honeydew', '../upload/item/11/1630636781honeydew.png', NULL, NULL, 'N', NULL, 'N', 'Y'),
		(12, 'godiva', 'Godiva 朱古力', 'Godiva chocolate', '../upload/item/12/1630636906Screenshot at Sep 03 10-41-37.png', NULL, NULL, 'N', NULL, 'N', 'Y'),
		(13, 'champagne', '香賓', 'Champagne', '../upload/item/13/1630636964champagne.png', NULL, NULL, 'N', NULL, 'N', 'Y'),
		(14, 'peninsula_mooncake', '半島月餅', 'Peninsula Moon Cake', '../upload/item/14/1630637027mooncake.png', NULL, NULL, 'N', NULL, 'N', 'Y'),
		(15, 'rattan_basket', '滕籃', 'Rattan basket', '../upload/item/15/1630637105bu.png', NULL, NULL, 'N', NULL, 'N', 'Y'),
		(16, 'otter', '水獺', 'otter', '../upload/item/4/1631773057otter4.jpeg', NULL, 6, 'N', NULL, 'N', 'Y');";
		$this->mysqli->query($sql);
		
		echo "tbl_item DONE <hr/>";
	}
	function tbl_member(){
		$sql = "DROP TABLE IF EXISTS `tbl_member`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_member` (
					`member_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
					`shop_id` int(11) NOT NULL,
					`password` varchar(255) DEFAULT NULL,
					`member_no` varchar(255) DEFAULT NULL,
					`company_name` varchar(255) DEFAULT NULL,
					`language` varchar(255) DEFAULT NULL,
					`name` varchar(255) DEFAULT NULL,
					`email` varchar(255) DEFAULT NULL,
					`mobile` varchar(255) DEFAULT NULL,
					`method` varchar(255) DEFAULT NULL, -- google || facebook || N
					`verify` varchar(1) DEFAULT 'N', 
					`sales_admin_id` int(11) DEFAULT NULL,
					`remark` longtext DEFAULT NULL,
					`point` int(11) DEFAULT 0,
					`cash_dollar` int(11) DEFAULT 0,
					`exp` int(11) DEFAULT 0,
					`membership_id` int(11) DEFAULT NULL,
					`expiry_date` datetime DEFAULT NULL,
					`facebook_id` int(11) DEFAULT NULL,
					`facebook_access_token` varchar(255) DEFAULT NULL,
					`valid_code` varchar (16) DEFAULT NULL,
					`pw_valid_code` varchar (16) DEFAULT NULL,
					`activate` varchar(1) DEFAULT 'Y',
					`updated_by_id` int(11) DEFAULT NULL,
					`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
				) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);

		$sql = "SELECT * FROM users";
		$res = $this->odb->get_Sql($sql);

		foreach($res as $user){
			$data = [
				"shop_id" => 1,
				"member_no" => $user['member_no'],
				"company_name" => $user['company_name'],
				"email" => $user['email'],
				"mobile" => $user['phone'],
				"remark" => $user['remark'],
				"name" => $user['name'],
				"membership_id" => $user['membership_id'],
				"password" => $user['password']
			];

			if ($user['membership_expiry_date'])
				$data["expiry_date"] = $user['membership_expiry_date'];


			if ($user['type'] == "1"){
				$data['method'] = "google";
			} else if ($user['type'] == "2"){
				$data['method'] = "facebook";
			}

			if ($user['verified'] == "1")
				$data['verify'] = 'Y';

			$id = $this->DB->insert_db("tbl_member", $data);
			echo "tbl_member row: $id || insert completed<br/>";
		}

		echo "tbl_member DONE <hr/>";
	}
	
	function tbl_membership(){
		$sql = "DROP TABLE IF EXISTS `tbl_membership`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_membership` (
			`membership_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`shop_id` int(11) NOT NULL,
			`name_tc` varchar(255) DEFAULT NULL,
			`name_en` varchar(255) DEFAULT NULL,
			`discount` int(11) DEFAULT 0,
			`upgrade_point` int(11) DEFAULT 0,
			`upgrade_to_membership_id` int(11) DEFAULT 0,
			`upgrade_period` int(11) DEFAULT 0,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);

		$sql = "SELECT * FROM memberships";
		$res = $this->odb->get_Sql($sql);

        foreach($res as $row){
			$data = [
				"membership_id" => $row['id'],
				"shop_id" => 1,
				"name_en" => $row['name_en'],
				"name_tc" => $row['name_zh_hk'],
				"discount" => $row['discount_percentage_off'],
				"upgrade_point" => $row['upgrade_points'],
				"upgrade_period" => $row['upgrade_period'],
				"upgrade_to_membership_id" => $row['upgrade_membership_id']

			];
			$id = $this->DB->insert_db("tbl_membership", $data);
			echo "tbl_membership row: $id || insert completed<br/>";
        }
		echo "tbl_membership DONE <hr/>";
	}

	function tbl_nav(){
		$sql = "DROP TABLE IF EXISTS `tbl_nav`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_nav` (
			`nav_id` int (11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`shop_id` int(11) NOT NULL,
			`name_tc` varchar(255) DEFAULT NULL,
			`name_en` varchar(255) DEFAULT NULL,
			`link` varchar(255) DEFAULT NULL, 
			`sort` int(11) DEFAULT NULL,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);

		$sql = "DROP TABLE IF EXISTS `tmap_cate_nav`";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tmap_cate_nav`(
			`map_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`nav_id` int(11) DEFAULT NULL, 
			`cate_id` int(11) DEFAULT NULL, 
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);

		$sql = "DROP TABLE IF EXISTS `tmap_nav_page`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tmap_nav_page`(
			`map_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`nav_id` int(11) DEFAULT NULL, 
			`page_id` int(11) DEFAULT NULL, 
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);
		
		echo "tbl_nav: Without data migration, data will resume default<br/>";
		echo "tmap_cate_nav: Without data migration, data will resume default<br/>";
		echo "tmap_nav_page: Without data migration, data will resume default<br/>";
		$sql = "INSERT INTO `tbl_nav` (`shop_id`, `name_tc`, `name_en`, `link`) VALUES
		('1', '全部商品', 'Shop All', 'product/productList'),
		('1', '禮盤種類', 'Hampers', null),
		('1', '花藝禮品', 'Floral', null),
		('1', '外部連結', 'Link', 'http://hk.yahoo.com'),
		('1', '常見問題', 'FAQ', null);";
		$this->mysqli->query($sql);
		
		$sql = "INSERT INTO `tmap_cate_nav` (`nav_id`, `cate_id`) VALUES
		(2,1),
		(3,2),(3,3),(3,4),(3,5);";
		$this->mysqli->query($sql);
		$sql = "INSERT INTO `tmap_nav_page` (`nav_id`, `page_id`) VALUES
		(5,1),(5,2),(5,3),(5,4),(5,5);";
		$this->mysqli->query($sql);
	
		echo "tbl_nav, tmap_nav_page, tmap_cate_nav DONE <hr/>";
	}
	function tbl_order(){
		$sql = "DROP TABLE IF EXISTS `tbl_order`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_order` (
			`order_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`shop_id` int(11) NOT NULL, 
			`order_status` varchar(255) DEFAULT '', -- waitingPayment, paid, deliProcess, complete   等待收款，已付款， 。。。 ，已進入運輸流程，已完成    
			`order_no` varchar (64) NOT NULL, -- invoice
			`member_id` int(11) DEFAULT NULL,
			`sales_admin_id` int (11) DEFAULT NULL,
			`sender_name` varchar (255) NOT NULL,
			`sender_email` varchar (255) DEFAULT NULL,
			`sender_phone` varchar (255) DEFAULT NULL,
			`product_price` int(11) DEFAULT NULL,
			`shipping_fee` int(11) NOT NULL,
			`timeslot_fee` int(11) NOT NULL,
			`cash_dollar_used` int(11) DEFAULT 0,
			`coupon_discount` int(11) DEFAULT 0,
			`voucher_discount` int(11) DEFAULT 0,
			`membership_discount` int(11) DEFAULT 0,
			`coupon_code_id` int(11) DEFAULT NULL,
			`coupon_code` varchar(64) DEFAULT NULL,
			`member_voucher_id` int(11) DEFAULT NULL,
			`final_price` int(11) NOT NULL,
			`point_gain` int(11) DEFAULT 0,
			`cash_dollar_gain` int(11) DEFAULT 0,
			`exp_gain` int(11) DEFAULT 0,
			`remarks_from_client` longtext DEFAULT NULL,
			`remarks_internal` longtext DEFAULT NULL,
			`remarks_external` longtext DEFAULT NULL,
			`excel_path` varchar(255) DEFAULT NULL,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`order_datetime` timestamp DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);


		$sql = "SELECT * FROM orders ";
		$res = $this->odb->get_Sql($sql);

		foreach($res as $row){
			$data = [
				"shop_id" => 1,
				"member_id" => $row['user_id'],
				"order_no" => $row['order_no'],
				"sender_name" => $row['sender_name'],
				"sender_email" => $row['sender_email'],
				"sender_phone" => $row['sender_phone_num'],
				"final_price" => $row['subtotal'],
				"shipping_fee" => $row['shipping_fee'],
				"timeslot_fee" => $row['delivery_time_slot_fee'],
				"cash_dollar_used" => $row['points_used_for_extending_black_membership'],
				"cash_dollar_gain" => $row['points'],
				"point_gain" => $row['points'],
				"exp_gain" => $row['points'],
				"remarks_from_client" => $row['remarks'],
				"remarks_internal" => "此訂單由舊數據庫複製而來 ".$row['remark_internal'],
				"remarks_external" => $row['remarks_to_client'],
				"order_datetime" => $row['created_at'],
				"create_datetime" => $row['created_at'],
				"last_update" => $row['updated_at']
				// "excel_path" => $row['excel_filepath']
			];

			switch($row['payment_status']){
				case '1':
					$data['order_status'] = 'waitingPayment';
					break;
				case '2':
					$data['order_status'] = 'completed';
					break;
				case '4':
					$data['order_status'] = 'cancelled';
					break;
			}

			$id = $this->DB->insert_db("tbl_order", $data);
			echo "tbl_order row: $id || insert completed<br/>";
		}
	}
	function tbl_order_deli(){
		$sql = "DROP TABLE IF EXISTS `tbl_order_deli`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_order_deli` (
			`deli_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`deli_no` varchar(64) DEFAULT NULL,
			`shop_id` int(11) DEFAULT NULL,
			`order_id` int(11) DEFAULT NULL,
			`deli_status` varchar(255) DEFAULT NULL, -- INCART, ....  PACKING, 運輸中，已簽收，確認完成
			`product_id` int(11) NOT NULL,
			`recipient_name` varchar (255) DEFAULT NULL,
			`recipient_phone` varchar (255) DEFAULT NULL,
			`recipient_company` varchar (255) DEFAULT NULL,
			`card_id` int(11) DEFAULT NULL,
			`card_head` varchar(255) DEFAULT NULL,
			`card_body` varchar(255) DEFAULT NULL,
			`card_sign` varchar(255) DEFAULT NULL,
			`card_json` longtext DEFAULT NULL,
			`card_activate` varchar(1) DEFAULT 'Y',
			`card_img_path` varchar(255) DEFAULT NULL,
			`deli_region_id` int(11) DEFAULT NULL,
			`deli_address` longtext DEFAULT NULL,
			`deli_date` DATE DEFAULT NULL,
			`deli_timeslot_id` int(11) DEFAULT NULL,
			`deli_before_photo_path` varchar(255) DEFAULT NULL,
			`deli_after_photo_path` varchar(255) DEFAULT NULL,
			`acknowledgement` varchar(255) DEFAULT NULL,
			`product_price` int(11) NOT NULL,
			`addon_price` int(11) NOT NULL,
			`shipping_fee` int(11) NOT NULL,
			`timeslot_fee` int(11) NOT NULL,
			`deli_price` int(11) NOT NULL,
			`remarks_internal` longtext DEFAULT NULL,
			`remarks_external` longtext DEFAULT NULL,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
		";
		$this->mysqli->query($sql);


		$sql = "SELECT * FROM order_items LIMIT 100";
		$res = $this->odb->get_Sql($sql);

		foreach($res as $row){
			$data = [
				"shop_id" => 1,
				"order_id" => $row['order_id'],
				"product_id" => $row['product_id'],
				"recipient_name" => $row['recipient_name'],
				"recipient_phone" => $row['recipient_phone_number'],
				"recipient_company" => $row['recipient_company_name'],
				"card_head" => $row['quick_message_card_headling'],
				"card_body" => $row['quick_message_card_body'],
				"card_sign" => $row['quick_message_card_signature'],
				"card_img_path" => $row['message_card_preview_image'],
				"product_price" => $row['product_price'],
				
			];

			$id = $this->DB->insert_db("tbl_order_deli", $data);
			echo "tbl_order row: $id || insert completed<br/>";
		}


	}
	function tbl_order_deli_item(){}
	function tbl_order_payment(){
		$sql = "DROP TABLE IF EXISTS `tbl_order_payment`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_order_payment` (
			`payment_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`order_id` int(11) DEFAULT NULL,
			`payment_status` varchar(255) DEFAULT NULL,
			`payment_method` varchar(255) DEFAULT NULL,
			`transaction_id` varchar(255) DEFAULT NULL,
			`bankslip_path` varchar(255) DEFAULT NULL,
			`raw_return` longtext DEFAULT NULL,
			`expected_payment_value` int(11) DEFAULT NULL,
			`actual_payment_value` int(11) DEFAULT NULL,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);

		echo "tbl_order_payment DONE <hr/>";
	}
	function tbl_page(){
		$sql = "DROP TABLE IF EXISTS `tbl_page`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_page` (
			`page_id` int (11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`shop_id` int(11) NOT NULL,
			`adm_name` varchar(255) NOT NULL,
			`url` varchar(255) NOT NULL,
			`removeable` varchar(1) DEFAULT 'Y',
			`activate` varchar(1) DEFAULT 'Y',
			`see_through_nav` varchar(1) DEFAULT 'N',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);


		$sql = "DROP TABLE IF EXISTS `tbl_page_content`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_page_content` (
			`content_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`page_id` int(11) DEFAULT NULL, 
			`type` varchar(255) DEFAULT NULL, 
			`html_tc` longtext DEFAULT NULL, -- html editor
			`html_en` longtext DEFAULT NULL, -- html editor
			`lr_img_tc` varchar(255) DEFAULT NULL,
			`lr_img_en` varchar(255) DEFAULT NULL,
			`sort` int(11) DEFAULT NULL, 
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);
    
		$sql = "INSERT INTO `tbl_page_content` (`content_id`, `page_id`, `type`, `html_tc`, `html_en`, `lr_img_tc`, `lr_img_en`, `sort`, `activate`) VALUES
		(1, 1, 'slider', NULL, NULL, NULL, NULL, 2, 'Y'),
		(2, 1, 'product_grp', NULL, NULL, NULL, NULL, 6, 'Y'),
		(3, 1, 'left_img', 'CONTENT AT RIGHT TC', 'CONTENT AT RIGHT EN', '../upload/page_content/3/1630459629otter3.jpeg', '../upload/page_content/3/1630459629otter3.jpeg', 3, 'Y'),
		(4, 1, 'right_img', 'CONTENT AT LEFT TC', 'CONTENT AT LEFT EN', '../upload/page_content/4/1630459645seaOtter4.jpeg', '../upload/page_content/4/1630459645seaOtter4.jpeg', 4, 'Y'),
		(5, 1, 'pure_html', 'CONTENT TC', 'CONTETN EN', NULL, NULL, 5, 'Y'),
		(6, 1, 'marquee', 'MARQUEE CONTENT TC', 'CONTENT EN', NULL, NULL, 1, 'Y'),
		(7, 2, 'slider', NULL, NULL, NULL, NULL, 2, 'Y');";
		$this->mysqli->query($sql);
		
		
		$sql = "INSERT INTO `tbl_page` (`page_id`, `shop_id`, `adm_name`, `url`, `removeable`, `activate`, `see_through_nav`) VALUES
		(1, 1, '主頁', '', 'N', 'Y', 'N'),
		(2, 1, '關於我們', 'about-us', 'N', 'Y', 'N'),
		(3, 1, '聯絡我們', 'contact-us', 'N', 'Y', 'N'),
		(4, 1, '所有物品', 'product/productList', 'N', 'Y', 'N'),
		(5, 1, '新頁面1', 'page1', 'Y', 'Y', 'N'),
		(6, 1, '新頁面2', 'page2', 'Y', 'Y', 'N'),
		(7, 1, '新頁面3', 'page3', 'Y', 'Y', 'N');";
		$this->mysqli->query($sql);


		echo "tbl_page DONE <hr/>";
	}
	
	function tbl_popup(){}
	function tbl_product(){
		$sql = "DROP TABLE IF EXISTS `tbl_product`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_product` ( 
			`product_id` int (11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
		
			`gift_product_code` varchar(255) DEFAULT NULL,
			`gift_product_name_tc` varchar(255) DEFAULT NULL,
			`gift_product_name_en` varchar(255) DEFAULT NULL,
			`gift_deli_id` int(11) DEFAULT NULL,
			`gift_cust_deli_act` varchar(1) DEFAULT 'N',
			`gift_cust_deli_tc` longtext DEFAULT NULL,
			`gift_cust_deli_en` longtext DEFAULT NULL,
			`gift_cust_info_act` varchar(1) DEFAULT 'N',
			`gift_cust_info_tc` longtext DEFAULT NULL,
			`gift_cust_info_en` longtext DEFAULT NULL,
			`gift_hover_img` varchar(255) DEFAULT NULL,
			`gift_price` int(11) DEFAULT NULL,
			`gift_sp_price` int(11) DEFAULT NULL,
			`gift_ava_b4_day` int(11) DEFAULT NULL,
			`gift_ava_b4_cutoff` time DEFAULT NULL,
			`gift_sort` int(11) DEFAULT NULL,
			`gift_onsale` varchar(1) DEFAULT 'Y', -- 發售中
			`gift_display_in_cate` varchar(1) DEFAULT 'Y',

			`zuri_product_code` varchar(255) DEFAULT NULL,
			`zuri_product_name_tc` varchar(255) DEFAULT NULL,
			`zuri_product_name_en` varchar(255) DEFAULT NULL,
			`zuri_deli_id` int(11) DEFAULT NULL,
			`zuri_cust_deli_act` varchar(1) DEFAULT 'N',
			`zuri_cust_deli_tc` longtext DEFAULT NULL,
			`zuri_cust_deli_en` longtext DEFAULT NULL,
			`zuri_cust_info_act` varchar(1) DEFAULT 'N',
			`zuri_cust_info_tc` longtext DEFAULT NULL,
			`zuri_cust_info_en` longtext DEFAULT NULL,
			`zuri_hover_img` varchar(255) DEFAULT NULL,
			`zuri_price` int(11) DEFAULT NULL,
			`zuri_sp_price` int(11) DEFAULT NULL,
			`zuri_ava_b4_day` int(11) DEFAULT NULL,
			`zuri_ava_b4_cutoff` time DEFAULT NULL,
			`zuri_sort` int(11) DEFAULT NULL,
			`zuri_onsale` varchar(1) DEFAULT 'Y', -- 發售中
			`zuri_display_in_cate` varchar(1) DEFAULT 'Y',

			`ltp_product_code` varchar(255) DEFAULT NULL,
			`ltp_product_name_tc` varchar(255) DEFAULT NULL,
			`ltp_product_name_en` varchar(255) DEFAULT NULL,
			`ltp_deli_id` int(11) DEFAULT NULL,
			`ltp_cust_deli_act` varchar(1) DEFAULT 'N',
			`ltp_cust_deli_tc` longtext DEFAULT NULL,
			`ltp_cust_deli_en` longtext DEFAULT NULL,
			`ltp_cust_info_act` varchar(1) DEFAULT 'N',
			`ltp_cust_info_tc` longtext DEFAULT NULL,
			`ltp_cust_info_en` longtext DEFAULT NULL,
			`ltp_hover_img` varchar(255) DEFAULT NULL,
			`ltp_price` int(11) DEFAULT NULL,
			`ltp_sp_price` int(11) DEFAULT NULL,
			`ltp_ava_b4_day` int(11) DEFAULT NULL,
			`ltp_ava_b4_cutoff` time DEFAULT NULL,
			`ltp_sort` int(11) DEFAULT NULL,
			`ltp_onsale` varchar(1) DEFAULT 'Y', -- 發售中
			`ltp_display_in_cate` varchar(1) DEFAULT 'Y',     
		
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);


		$sql = "INSERT INTO `tbl_product` (`product_id`, `gift_product_code`, `gift_product_name_tc`, `gift_product_name_en`, `gift_deli_id`, `gift_cust_deli_act`, `gift_cust_deli_tc`, `gift_cust_deli_en`, `gift_cust_info_act`, `gift_cust_info_tc`, `gift_cust_info_en`, `gift_hover_img`, `gift_price`, `gift_sp_price`, `gift_sort`, `gift_onsale`, `gift_display_in_cate`, `zuri_product_code`, `zuri_product_name_tc`, `zuri_product_name_en`, `zuri_deli_id`, `zuri_cust_deli_act`, `zuri_cust_deli_tc`, `zuri_cust_deli_en`, `zuri_cust_info_act`, `zuri_cust_info_tc`, `zuri_cust_info_en`, `zuri_hover_img`, `zuri_price`, `zuri_sp_price`, `zuri_onsale`, `zuri_sort`, `zuri_display_in_cate`, `ltp_product_code`, `ltp_product_name_tc`, `ltp_product_name_en`, `ltp_deli_id`, `ltp_cust_deli_act`, `ltp_cust_deli_tc`, `ltp_cust_deli_en`, `ltp_cust_info_act`, `ltp_cust_info_tc`, `ltp_cust_info_en`, `ltp_hover_img`, `ltp_price`, `ltp_sp_price`, `ltp_onsale`, `ltp_sort`, `ltp_display_in_cate`, `activate`, `updated_by_id`, `last_update`, `create_datetime`) VALUES
		(1, NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', 'N', 1, '2021-09-03 02:57:10', '2021-09-03 01:47:41'),
		(2, NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', 'N', 1, '2021-09-03 02:57:08', '2021-09-03 02:21:07'),
		(3, 'MAF146R', 'Moet 配 Godiva 朱古力中秋果籃', 'Moet Champagne with Godiva Chocolate Mid Autumn Festival', 1, 'N', '<div class=\"tabs-container\" style=\"color: #212529;\">\r\n<div class=\"tab-contents\">\r\n<div id=\"tab-delivery-options\" class=\"tab-content cke-area active\">\r\n<p>中秋佳節期間(8月26日 至 9月13日)將不設即日單及指定時間送貨服務，所有訂單將於 9am-6pm送出。</p>\r\n</div>\r\n</div>\r\n</div>', '<div class=\"tabs-container\" style=\"color: #212529;\">\r\n<div class=\"tab-contents\">\r\n<div id=\"tab-delivery-options\" class=\"tab-content cke-area active\">\r\n<p>All the orders will be delivered at 9am-6pm during the mid-autumn festival (26 Aug -13 sept).</p>\r\n</div>\r\n</div>\r\n</div>', 'N', '<p style=\"color: #212529;\">此禮品附送 賀卡牌(插置於禮品上) 或 THEGIFT 個人心意卡(放在信封)</p>\r\n<p style=\"color: #212529;\"><br />圖中產品款式會因應季節/貨源而可能調動</p>', '<div class=\"tabs\" style=\"color: #212529;\">\r\n<div class=\"tab\" data-target=\"tab-detail\">\r\n<p style=\"color: #212529;\">This gift comes with a message card board (Place on top ) OR a THEGIFT &nbsp;message card (in an envelop)</p>\r\n<p style=\"color: #212529;\"><br />Due to seasonality, some products have seasonal style varies</p>\r\n</div>\r\n</div>', NULL, 2228, 1500, NULL, 'N', 'N', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', 'Y', 1, '2021-09-20 05:27:45', '2021-09-03 02:23:46'),
		(4, 'APPLE001', '蘋果', 'gift籃aEN', 1, 'F', '<p>deli_hello<img src=\"../../upload/product/0/1632462041mceclip0.jpg\" /></p>', '<p>deli_hello_EN</p>', 'N', '<p>gift_cust_info_tc</p>', '<p>gift_cust_info_en</p>', '../upload/1/1628674186seaOtter4.jpeg', 500, 480, NULL, 'Y', 'Y', 'ZURI001', 'zuri_product_name_tc', 'zuri_product_name_en', 1, 'Y', 'zuri_cust_deli_tc', 'zuri_cust_deli_en', 'Y', 'zuri_cust_info_tc', 'zuri_cust_info_en', NULL, 400, 380, 'Y', NULL, 'Y', 'LTP001', 'ltp_product_name_tc', 'ltp_product_name_en', 1, 'F', 'ltp_cust_deli_tc', 'ltp_cust_deli_en', 'Y', 'ltp_cust_info_tc', 'ltp_cust_info_en', NULL, 450, 430, 'Y', NULL, 'Y', 'Y', 1, '2021-09-24 05:40:45', '2021-09-07 06:05:20'),
		(5, 'ORANGE001', '橙', 'gift籃bEN', 1, 'F', '<p>deli_hello</p>', '<p>deli_hello_EN</p>', 'N', '<p>gift_cust_info_tc</p>', '<p>gift_cust_info_en</p>', '../upload/1/1628674186seaOtter4.jpeg', 500, 480, NULL, 'Y', 'Y', 'ZURI002', 'zuri_product_name_tc', 'zuri_product_name_en', 1, 'Y', 'zuri_cust_deli_tc', 'zuri_cust_deli_en', 'Y', 'zuri_cust_info_tc', 'zuri_cust_info_en', NULL, 400, 380, 'Y', NULL, 'Y', 'LTP002', 'ltp_product_name_tc', 'ltp_product_name_en', 1, 'F', 'ltp_cust_deli_tc', 'ltp_cust_deli_en', 'Y', 'ltp_cust_info_tc', 'ltp_cust_info_en', NULL, 450, 430, 'Y', NULL, 'Y', 'Y', 1, '2021-09-15 04:09:50', '2021-09-07 06:05:20'),
		(6, 'BANANA001', '蕉', 'gift籃cEN', 1, 'F', '<p>deli_hello</p>', '<p>deli_hello_EN</p>', 'N', '<p>gift_cust_info_tc</p>', '<p>gift_cust_info_en</p>', '../upload/1/1628674186seaOtter4.jpeg', 500, 480, NULL, 'Y', 'Y', 'ZURI003', 'zuri_product_name_tc', 'zuri_product_name_en', 1, 'Y', 'zuri_cust_deli_tc', 'zuri_cust_deli_en', 'Y', 'zuri_cust_info_tc', 'zuri_cust_info_en', NULL, 400, 380, 'Y', NULL, 'Y', 'LTP003', 'ltp_product_name_tc', 'ltp_product_name_en', 1, 'F', 'ltp_cust_deli_tc', 'ltp_cust_deli_en', 'Y', 'ltp_cust_info_tc', 'ltp_cust_info_en', NULL, 450, 430, 'Y', NULL, 'Y', 'Y', 1, '2021-09-15 04:10:17', '2021-09-07 06:05:20'),
		(7, 'GIFT004', 'gift籃d', 'gift籃dEN', 1, 'F', '<p>deli_hello</p>', '<p>deli_hello_EN</p>', 'N', '<p>gift_cust_info_tc</p>', '<p>gift_cust_info_en</p>', '../upload/1/1628674186seaOtter4.jpeg', 5, 480, NULL, 'Y', 'Y', 'ZURI004', 'zuri_product_name_tc', 'zuri_product_name_en', 1, 'Y', 'zuri_cust_deli_tc', 'zuri_cust_deli_en', 'Y', 'zuri_cust_info_tc', 'zuri_cust_info_en', NULL, 400, 380, 'Y', NULL, 'Y', 'LTP004', 'ltp_product_name_tc', 'ltp_product_name_en', 1, 'F', 'ltp_cust_deli_tc', 'ltp_cust_deli_en', 'Y', 'ltp_cust_info_tc', 'ltp_cust_info_en', NULL, 450, 430, 'Y', NULL, 'Y', 'Y', 1, '2021-09-16 08:51:48', '2021-09-07 06:05:20'),
		(8, 'GIFT005', 'gift籃e', 'gift籃eEN', 1, 'F', '<p>deli_hello</p>', '<p>deli_hello_EN</p>', 'N', '<p>gift_cust_info_tc</p>', '<p>gift_cust_info_en</p>', '../upload/1/1628674186seaOtter4.jpeg', 5, 480, NULL, 'Y', 'Y', 'ZURI005', 'zuri_product_name_tc', 'zuri_product_name_en', 1, 'Y', 'zuri_cust_deli_tc', 'zuri_cust_deli_en', 'Y', 'zuri_cust_info_tc', 'zuri_cust_info_en', NULL, 400, 380, 'Y', NULL, 'Y', 'LTP005', 'ltp_product_name_tc', 'ltp_product_name_en', 1, 'F', 'ltp_cust_deli_tc', 'ltp_cust_deli_en', 'Y', 'ltp_cust_info_tc', 'ltp_cust_info_en', NULL, 450, 430, 'Y', NULL, 'Y', 'Y', 1, '2021-09-16 08:51:48', '2021-09-07 06:05:20'),
		(9, 'GIFT006', 'gift籃f', 'gift籃fEN', 1, 'F', '<p>deli_hello</p>', '<p>deli_hello_EN</p>', 'N', '<p>gift_cust_info_tc</p>', '<p>gift_cust_info_en</p>', '../upload/1/1628674186seaOtter4.jpeg', 500, 480, NULL, 'Y', 'Y', 'ZURI006', 'zuri_product_name_tc', 'zuri_product_name_en', 1, 'Y', 'zuri_cust_deli_tc', 'zuri_cust_deli_en', 'Y', 'zuri_cust_info_tc', 'zuri_cust_info_en', NULL, 400, 380, 'Y', NULL, 'Y', 'LTP006', 'ltp_product_name_tc', 'ltp_product_name_en', 1, 'F', 'ltp_cust_deli_tc', 'ltp_cust_deli_en', 'Y', 'ltp_cust_info_tc', 'ltp_cust_info_en', NULL, 450, 430, 'Y', NULL, 'Y', 'Y', 1, '2021-09-23 10:06:05', '2021-09-07 06:05:20'),
		(10, 'TEMP-001', '時尚紳士美食禮籃', '時尚紳士美食禮籃 EN TEXT', 1, 'N', NULL, NULL, 'N', NULL, NULL, NULL, 1088, 988, NULL, 'Y', 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', 'Y', 1, '2021-09-17 04:06:29', '2021-09-17 03:53:51'),
		(11, 'TEMP-002', '時尚紳士美食禮籃', '時尚紳士美食禮籃 EN TEXT', 1, 'N', NULL, NULL, 'N', NULL, NULL, NULL, 1088, 988, NULL, 'Y', 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', 'Y', 1, '2021-09-17 04:06:29', '2021-09-17 03:53:53'),
		(12, 'TEMP-003', '時尚紳士美食禮籃', '時尚紳士美食禮籃 EN TEXT', 1, 'N', NULL, NULL, 'N', NULL, NULL, NULL, 1088, 988, NULL, 'Y', 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', 'Y', 1, '2021-09-17 04:06:29', '2021-09-17 03:53:54'),
		(13, 'TEMP-004', '時尚紳士美食禮籃', '時尚紳士美食禮籃 EN TEXT', 1, 'N', NULL, NULL, 'N', NULL, NULL, NULL, 1088, 988, NULL, 'Y', 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', 'Y', 1, '2021-09-17 04:06:29', '2021-09-17 03:53:54'),
		(14, 'TEMP-005', '時尚紳士美食禮籃', '時尚紳士美食禮籃 EN TEXT', 1, 'N', NULL, NULL, 'N', NULL, NULL, NULL, 1088, 988, NULL, 'Y', 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', 'Y', 1, '2021-09-17 04:06:29', '2021-09-17 03:53:54'),
		(15, 'TEMP-006', '時尚紳士美食禮籃', '時尚紳士美食禮籃 EN TEXT', 1, 'N', NULL, NULL, 'N', NULL, NULL, NULL, 1088, 988, NULL, 'Y', 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', 'Y', 1, '2021-09-17 04:06:29', '2021-09-17 03:53:55'),
		(16, 'TEMP-007', '時尚紳士美食禮籃', '時尚紳士美食禮籃 EN TEXT', 1, 'N', NULL, NULL, 'N', NULL, NULL, NULL, 1088, 988, NULL, 'Y', 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', 'Y', 1, '2021-09-17 04:06:29', '2021-09-17 03:53:55'),
		(17, 'TEMP-008', '時尚紳士美食禮籃', '時尚紳士美食禮籃 EN TEXT', 1, 'N', NULL, NULL, 'N', NULL, NULL, NULL, 1088, 988, NULL, 'Y', 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', 'Y', 1, '2021-09-17 04:06:29', '2021-09-17 03:53:55'),
		(18, 'TEMP-009', '皇牌經濟送禮果籃009', '皇牌經濟送禮果籃009EN', 1, 'N', NULL, NULL, 'N', NULL, NULL, NULL, 1000, 800, NULL, 'Y', 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', NULL, NULL, NULL, NULL, 'N', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y', 'Y', 1, '2021-09-24 08:45:19', '2021-09-24 08:45:19');
		";
		$this->mysqli->query($sql);

		// $sql = "SELECT * FROM products";
		// $res = $this->odb->get_Sql($sql);

		// foreach($res as $row){
		// 	$data = [
		// 		"gift_product_code" => $row['product_code'],
		// 		"gift_product_name_tc" => $row['name_zh_hk'],
		// 		"gift_product_name_en" => $row['name_en'],
		// 		"gift_cust_deli_act" => $this->noToNY($row['is_custom_delivery_option']),
		// 		"gift_cust_deli_tc" => $row['delivery_option_zh_hk'],
		// 		"gift_cust_deli_en" => $row['delivery_option_en'],
		// 		"gift_cust_info_act" => $this->noToNY($row['is_custom_additional_information']),
		// 		"gift_cust_info_tc" => $row['addtional_information_zh_hk'],
		// 		"gift_cust_info_en" => $row['addtional_information_en'],
		// 		"gift_onsale" => $this->noToNY($row['visibility']),
		// 		"gift_sort" => $row['shop_all_list_order'],
		// 		"gift_sp_price" => $row['price'],
		// 		"gift_price" => $row['original_price'],
		// 	];

		// 	$id = $this->DB->insert_db("tbl_product", $data);
			
		// 	echo "tbl_product row: $id || insert completed<br/>";
		// }
	}
	function tbl_product_addon_cate(){}
	function tbl_product_img(){}
	function tbl_product_item_content(){
		$sql = "DROP TABLE IF EXISTS `tbl_product_item_content`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_product_item_content` (
			`content_id` int(11) NOT NULL,
			`product_id` int(11) DEFAULT NULL,
			`sku_id` int(11) DEFAULT NULL,
			`gift_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			`gift_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			`zuri_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			`zuri_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			`ltp_name_tc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			`ltp_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			`qty` int(11) DEFAULT NULL,
			`additional_price` int(11) DEFAULT '0',
			`group_item` int(11) DEFAULT NULL,
			`client_option` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
			`activate` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
		  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
		";
		$this->mysqli->query($sql);
		
		echo "tbl_item_content: Without data migration, data will resume default<br/>";

		$sql = "
		INSERT INTO `tbl_product_item_content` (`content_id`, `product_id`, `sku_id`, `gift_name_tc`, `gift_name_en`, `zuri_name_tc`, `zuri_name_en`, `ltp_name_tc`, `ltp_name_en`, `qty`, `additional_price`, `group_item`, `client_option`, `activate`, `updated_by_id`, `last_update`, `create_datetime`) VALUES
(1, 3, 1, '蘋果', 'Apple', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'N', 1, '2021-09-06 03:51:34', '2021-09-06 03:48:59'),
(2, 3, 7, '橙', 'orange', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'N', 1, '2021-09-06 03:51:34', '2021-09-06 03:48:59'),
(3, 3, 17, '天桃', 'tiantao', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'N', 1, '2021-09-14 06:59:38', '2021-09-06 03:48:59'),
(4, 3, 19, '布冧', 'tiantao', NULL, NULL, NULL, NULL, 2, 0, 1, 'N', 'N', 1, '2021-09-14 06:59:38', '2021-09-06 03:48:59'),
(5, 3, 27, 'Godiva', 'Godiva', NULL, NULL, NULL, NULL, 1, 0, 2, 'N', 'N', 1, '2021-09-06 03:54:16', '2021-09-06 03:48:59'),
(6, 3, 25, '日本蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'N', 'N', 1, '2021-09-14 06:59:38', '2021-09-06 03:48:59'),
(7, 3, 31, '滕籃', 'Rattan', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'N', 1, '2021-09-15 04:07:19', '2021-09-06 03:54:16'),
(9, 4, 1, '蘋果c', 'Apple', NULL, NULL, NULL, NULL, 15, 0, 1, 'N', 'N', 1, '2021-09-14 06:59:39', '2021-09-14 06:59:38'),
(10, 4, 7, '橙', 'orange', NULL, NULL, NULL, NULL, 10, 0, 2, 'N', 'N', 1, '2021-09-14 06:59:39', '2021-09-14 06:59:38'),
(11, 4, 1, '蘋果c', 'Apple', NULL, NULL, NULL, NULL, 15, 0, 1, 'N', 'N', 1, '2021-09-14 06:59:40', '2021-09-14 06:59:39'),
(12, 4, 7, '橙', 'orange', NULL, NULL, NULL, NULL, 10, 0, 2, 'N', 'N', 1, '2021-09-14 06:59:40', '2021-09-14 06:59:39'),
(13, 4, 1, '蘋果c', 'Apple', NULL, NULL, NULL, NULL, 15, 0, 1, 'N', 'N', 1, '2021-09-14 06:59:40', '2021-09-14 06:59:40'),
(14, 4, 7, '橙', 'orange', NULL, NULL, NULL, NULL, 10, 0, 2, 'N', 'N', 1, '2021-09-14 06:59:40', '2021-09-14 06:59:40'),
(15, 4, 1, '蘋果c', 'Apple', NULL, NULL, NULL, NULL, 15, 0, 1, 'N', 'N', 1, '2021-09-14 07:00:41', '2021-09-14 06:59:40'),
(16, 4, 7, '橙', 'orange', NULL, NULL, NULL, NULL, 10, 0, 2, 'N', 'N', 1, '2021-09-14 07:00:41', '2021-09-14 06:59:40'),
(17, 5, 16, '火龍果', 'Pitaya', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'N', 1, '2021-09-14 07:00:42', '2021-09-14 07:00:41'),
(18, 5, 21, '香梨', 'Fragant pear', NULL, NULL, NULL, NULL, 1, 20, 2, 'N', 'N', 1, '2021-09-14 07:00:42', '2021-09-14 07:00:41'),
(19, 5, 16, '火龍果', 'Pitaya', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'N', 1, '2021-09-14 07:02:14', '2021-09-14 07:00:42'),
(20, 5, 21, '香梨', 'Fragant pear', NULL, NULL, NULL, NULL, 1, 20, 2, 'N', 'N', 1, '2021-09-14 08:14:51', '2021-09-14 07:00:42'),
(21, 6, 19, '布冧', 'Cloth', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'N', 1, '2021-09-14 08:14:51', '2021-09-14 07:02:14'),
(22, 6, 20, '布冧', 'Cloth', NULL, NULL, NULL, NULL, 1, 10, 1, 'Y', 'N', 1, '2021-09-14 08:14:51', '2021-09-14 07:02:14'),
(23, 3, 19, '布冧', 'Cloth', NULL, NULL, NULL, NULL, 1, 20, 2, 'N', 'N', 1, '2021-09-15 04:07:19', '2021-09-14 08:14:51'),
(24, 3, 9, '蕉', 'Banana', NULL, NULL, NULL, NULL, 1, 0, 2, 'N', 'N', 1, '2021-09-15 04:07:19', '2021-09-14 08:14:51'),
(25, 3, 23, '藍莓', 'blueberry', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'N', 1, '2021-09-20 05:33:46', '2021-09-14 08:14:51'),
(26, 3, 18, '天桃', 'tiantao', NULL, NULL, NULL, NULL, 1, 0, 2, 'N', 'N', 1, '2021-09-20 05:33:46', '2021-09-14 08:14:51'),
(27, 3, 6, '蘋果c', 'Apple', NULL, NULL, NULL, NULL, 1, 10, 1, 'Y', 'N', 1, '2021-09-20 05:33:46', '2021-09-14 08:15:19'),
(28, 4, 1, '蘋果c', 'Apple', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'N', 1, '2021-09-15 04:07:27', '2021-09-15 04:07:19'),
(29, 4, 2, '蘋果c', 'Apple', NULL, NULL, NULL, NULL, 5, 0, 2, 'N', 'N', 1, '2021-09-15 04:07:27', '2021-09-15 04:07:19'),
(30, 4, 1, '蘋果c', 'Apple', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'Y', 1, '2021-09-15 04:07:27', '2021-09-15 04:07:27'),
(31, 4, 2, '蘋果c', 'Apple', NULL, NULL, NULL, NULL, 5, 0, 2, 'N', 'Y', 1, '2021-09-15 04:07:27', '2021-09-15 04:07:27'),
(32, 3, 24, '蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 3, 'N', 'N', 1, '2021-09-15 04:09:23', '2021-09-15 04:09:12'),
(33, 3, 25, '蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 100, 3, 'Y', 'N', 1, '2021-09-15 04:09:23', '2021-09-15 04:09:12'),
(34, 3, 24, '蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 3, 'N', 'N', 1, '2021-09-20 05:33:46', '2021-09-15 04:09:23'),
(35, 3, 25, '蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 100, 3, 'Y', 'N', 1, '2021-09-20 05:33:46', '2021-09-15 04:09:23'),
(36, 3, 21, '香梨', 'Fragant', NULL, NULL, NULL, NULL, 1, 0, 4, 'N', 'N', 1, '2021-09-20 05:33:46', '2021-09-15 04:09:23'),
(37, 3, 27, 'Godiva', 'Godiva', NULL, NULL, NULL, NULL, 1, 0, 5, 'N', 'N', 1, '2021-09-20 05:35:54', '2021-09-15 04:09:23'),
(38, 3, 28, '香賓', 'Champagne', NULL, NULL, NULL, NULL, 1, 0, 6, 'N', 'N', 1, '2021-09-20 05:35:59', '2021-09-15 04:09:23'),
(39, 5, 7, '橙', 'orange', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'N', 1, '2021-09-15 04:09:53', '2021-09-15 04:09:50'),
(40, 5, 7, '橙', 'orange', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'Y', 1, '2021-09-15 04:09:53', '2021-09-15 04:09:53'),
(41, 6, 9, '蕉', 'Banana', NULL, NULL, NULL, NULL, 10, 0, 1, 'N', 'Y', 1, '2021-09-15 04:10:17', '2021-09-15 04:10:17'),
(42, 7, 1, '蘋果c', 'Apple', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'N', 1, '2021-09-16 03:19:38', '2021-09-16 03:19:23'),
(43, 7, 1, '蘋果c', 'Apple', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'N', 1, '2021-09-16 03:20:06', '2021-09-16 03:19:38'),
(44, 8, 1, '蘋果c', 'Apple', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'N', 1, '2021-09-16 03:20:06', '2021-09-16 03:19:38'),
(45, 7, 1, '蘋果c', 'Apple', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'Y', 1, '2021-09-16 03:20:06', '2021-09-16 03:20:06'),
(46, 7, 16, '火龍果', 'Pitaya', NULL, NULL, NULL, NULL, 1, 0, 1, 'Y', 'Y', 1, '2021-09-16 03:20:06', '2021-09-16 03:20:06'),
(47, 7, 20, '布冧', 'Cloth', NULL, NULL, NULL, NULL, 1, 0, 2, 'N', 'Y', 1, '2021-09-16 03:20:06', '2021-09-16 03:20:06'),
(48, 8, 1, '蘋果c', 'Apple', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'Y', 1, '2021-09-16 03:20:06', '2021-09-16 03:20:06'),
(49, 8, 16, '火龍果', 'Pitaya', NULL, NULL, NULL, NULL, 1, 0, 1, 'Y', 'Y', 1, '2021-09-16 03:20:06', '2021-09-16 03:20:06'),
(50, 8, 20, '布冧', 'Cloth', NULL, NULL, NULL, NULL, 1, 0, 2, 'N', 'Y', 1, '2021-09-16 03:20:06', '2021-09-16 03:20:06'),
(51, 10, 13, '豐水梨', 'Fengshui', NULL, NULL, NULL, NULL, 5, 0, 1, 'N', 'Y', 1, '2021-09-20 03:57:23', '2021-09-17 03:55:29'),
(52, 10, 22, '香梨', 'Fragant', NULL, NULL, NULL, NULL, 3, 0, 1, 'N', 'Y', 1, '2021-09-20 03:57:23', '2021-09-17 03:55:29'),
(53, 10, 24, '蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'N', 'Y', 1, '2021-09-17 03:55:29', '2021-09-17 03:55:29'),
(54, 10, 25, '日本蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'Y', 'Y', 1, '2021-09-17 03:55:29', '2021-09-17 03:55:29'),
(55, 11, 13, '豐水梨', 'Fengshui', NULL, NULL, NULL, NULL, 5, 0, 1, 'N', 'Y', 1, '2021-09-17 04:04:38', '2021-09-17 03:55:29'),
(56, 11, 22, '香梨', 'Fragant', NULL, NULL, NULL, NULL, 3, 0, 1, 'N', 'Y', 1, '2021-09-17 04:04:38', '2021-09-17 03:55:29'),
(57, 11, 24, '蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'N', 'Y', 1, '2021-09-17 03:55:29', '2021-09-17 03:55:29'),
(58, 11, 25, '日本蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'Y', 'Y', 1, '2021-09-17 03:55:29', '2021-09-17 03:55:29'),
(59, 12, 13, '豐水梨', 'Fengshui', NULL, NULL, NULL, NULL, 5, 0, 1, 'N', 'Y', 1, '2021-09-17 04:04:43', '2021-09-17 03:55:29'),
(60, 12, 22, '香梨', 'Fragant', NULL, NULL, NULL, NULL, 3, 0, 1, 'N', 'Y', 1, '2021-09-17 04:04:43', '2021-09-17 03:55:29'),
(61, 12, 24, '蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'N', 'Y', 1, '2021-09-17 03:55:29', '2021-09-17 03:55:29'),
(62, 12, 25, '日本蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'Y', 'Y', 1, '2021-09-17 03:55:29', '2021-09-17 03:55:29'),
(63, 13, 13, '豐水梨', 'Fengshui', NULL, NULL, NULL, NULL, 5, 0, 1, 'N', 'Y', 1, '2021-09-17 04:04:48', '2021-09-17 03:55:29'),
(64, 13, 22, '香梨', 'Fragant', NULL, NULL, NULL, NULL, 3, 0, 1, 'N', 'Y', 1, '2021-09-17 04:04:48', '2021-09-17 03:55:29'),
(65, 13, 24, '蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'N', 'Y', 1, '2021-09-17 03:55:29', '2021-09-17 03:55:29'),
(66, 13, 25, '日本蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'Y', 'Y', 1, '2021-09-17 03:55:29', '2021-09-17 03:55:29'),
(67, 14, 13, '豐水梨', 'Fengshui', NULL, NULL, NULL, NULL, 5, 0, 1, 'N', 'Y', 1, '2021-09-17 04:04:52', '2021-09-17 03:55:29'),
(68, 14, 22, '香梨', 'Fragant', NULL, NULL, NULL, NULL, 3, 0, 1, 'N', 'Y', 1, '2021-09-17 04:04:52', '2021-09-17 03:55:29'),
(69, 14, 24, '蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'N', 'Y', 1, '2021-09-17 03:55:29', '2021-09-17 03:55:29'),
(70, 14, 25, '日本蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'Y', 'Y', 1, '2021-09-17 03:55:29', '2021-09-17 03:55:29'),
(71, 15, 13, '豐水梨', 'Fengshui', NULL, NULL, NULL, NULL, 5, 0, 1, 'N', 'Y', 1, '2021-09-17 04:04:00', '2021-09-17 03:55:29'),
(72, 15, 22, '香梨', 'Fragant', NULL, NULL, NULL, NULL, 3, 0, 1, 'N', 'Y', 1, '2021-09-17 04:04:00', '2021-09-17 03:55:29'),
(73, 15, 24, '蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'N', 'Y', 1, '2021-09-17 03:55:29', '2021-09-17 03:55:29'),
(74, 15, 25, '日本蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'Y', 'Y', 1, '2021-09-17 03:55:29', '2021-09-17 03:55:29'),
(75, 16, 13, '豐水梨', 'Fengshui', NULL, NULL, NULL, NULL, 5, 0, 1, 'N', 'Y', 1, '2021-09-17 04:05:05', '2021-09-17 03:55:29'),
(76, 16, 22, '香梨', 'Fragant', NULL, NULL, NULL, NULL, 3, 0, 1, 'N', 'Y', 1, '2021-09-17 04:05:05', '2021-09-17 03:55:29'),
(77, 16, 24, '蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'N', 'Y', 1, '2021-09-17 03:55:29', '2021-09-17 03:55:29'),
(78, 16, 25, '日本蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'Y', 'Y', 1, '2021-09-17 03:55:29', '2021-09-17 03:55:29'),
(79, 17, 13, '豐水梨', 'Fengshui', NULL, NULL, NULL, NULL, 5, 0, 1, 'N', 'Y', 1, '2021-09-17 04:05:10', '2021-09-17 03:55:29'),
(80, 17, 22, '香梨', 'Fragant', NULL, NULL, NULL, NULL, 3, 0, 1, 'N', 'Y', 1, '2021-09-17 04:05:10', '2021-09-17 03:55:29'),
(81, 17, 24, '蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'N', 'Y', 1, '2021-09-17 03:55:29', '2021-09-17 03:55:29'),
(82, 17, 25, '日本蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 2, 'Y', 'Y', 1, '2021-09-17 03:55:29', '2021-09-17 03:55:29'),
(83, 3, 14, '豐水梨', 'Fengshui', NULL, NULL, NULL, NULL, 1, 0, 1, 'N', 'Y', 1, '2021-09-20 05:33:57', '2021-09-20 05:33:46'),
(84, 3, 16, '火龍果', 'Pitaya', NULL, NULL, NULL, NULL, 2, 0, 2, 'N', 'Y', 1, '2021-09-20 05:33:46', '2021-09-20 05:33:46'),
(85, 3, 17, '天桃', 'tiantao', NULL, NULL, NULL, NULL, 1, 0, 3, 'N', 'Y', 1, '2021-09-20 05:33:46', '2021-09-20 05:33:46'),
(86, 3, 19, '布冧', 'Cloth', NULL, NULL, NULL, NULL, 2, 0, 3, 'N', 'Y', 1, '2021-09-20 05:33:46', '2021-09-20 05:33:46'),
(87, 3, 23, '進口盒裝藍莓', 'blueberry', NULL, NULL, NULL, NULL, 1, 0, 4, 'N', 'Y', 1, '2021-09-20 05:33:46', '2021-09-20 05:33:46'),
(88, 3, 24, '溫室蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 0, 5, 'N', 'Y', 1, '2021-09-20 05:36:29', '2021-09-20 05:36:29'),
(89, 3, 25, '日本溫室蜜瓜', 'Honeydew', NULL, NULL, NULL, NULL, 1, 280, 5, 'Y', 'Y', 1, '2021-09-20 05:39:28', '2021-09-20 05:36:29'),
(90, 3, 27, '比利時GODIVA16片裝朱古力禮盒', 'Godiva', NULL, NULL, NULL, NULL, 1, 0, 6, 'N', 'Y', 1, '2021-09-20 05:39:28', '2021-09-20 05:37:24'),
(91, 3, 28, 'Moet', 'Champagne', NULL, NULL, NULL, NULL, 1, 0, 7, 'N', 'Y', 1, '2021-09-20 05:39:28', '2021-09-20 05:37:24'),
(92, 3, 29, '半島精品店迷你奶黃月餅', 'Peninsula', NULL, NULL, NULL, NULL, 1, 0, 8, 'N', 'Y', 1, '2021-09-20 05:39:28', '2021-09-20 05:37:24'),
(93, 3, 31, '滕籃', 'Rattan', NULL, NULL, NULL, NULL, 1, 0, 9, 'N', 'Y', 1, '2021-09-20 05:39:28', '2021-09-20 05:37:24'),
(94, 9, 9, '蕉', 'Banana', NULL, NULL, NULL, NULL, 20, 0, 1, 'N', 'Y', 1, '2021-09-24 01:22:15', '2021-09-23 10:06:05');
		";
		$this->mysqli->query($sql);

	
		echo "tbl_item_content DONE <hr/>";
	}
	function tbl_shop(){
		$sql = "DROP TABLE IF EXISTS `tbl_shop`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_shop` (
			`shop_id` int (11) NOT NULL PRIMARY KEY AUTO_INCREMENT, -- 1 gift, 2 zuri, 3 ltp
			`adm_name` varchar(255) DEFAULT NULL,
			`nav_color` varchar(8) DEFAULT NULL, -- #000000
			`whatsapp` varchar(255) DEFAULT NULL,
			`whatsapp2` varchar(255) DEFAULT NULL,
			`facebook` varchar(255) DEFAULT NULL,
			`ig` varchar(255) DEFAULT NULL,
			`twitter` varchar(255) DEFAULT NULL,
			`phone` varchar(255) DEFAULT NULL, 
			`fax` varchar(255) DEFAULT NULL,
			`markup` varchar(255) DEFAULT NULL,
			`logo` varchar(255) DEFAULT NULL,
			`new_member_credit` varchar(255) DEFAULT NULL,
			`point_ratio` int(11) DEFAULT 0,
			`google_x` varchar(128) DEFAULT NULL,
			`google_y` varchar(128) DEFAULT NULL,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);
		
		echo "tbl_shop: Without data migration, data will resume default<br/>";

		$sql = "INSERT INTO `tbl_shop` (`shop_id`, `adm_name`, `nav_color`, `whatsapp`, `whatsapp2`, `facebook`, `ig`, `twitter`, `phone`, `fax`, `markup`, `logo`, `new_member_credit`, `point_ratio`, `google_x`, `google_y`, `activate`) VALUES
		(1, 'The Gift', '#538432', '98765432', '65432187', NULL, NULL, NULL, '21800000', '21800001', NULL, '../upload/logo/1/1630402926seaOtter5.jpeg', NULL, 0, '22.3117046', '114.2156481', 'Y'),
		(2, 'Little Zuri', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Y'),
		(3, 'LoTaiPo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Y');";
		$this->mysqli->query($sql);

	
		echo "tbl_shop DONE <hr/>";
	}
	function tbl_sku(){
		$sql = "DROP TABLE IF EXISTS `tbl_sku`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_sku` (
			`sku_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`item_id` int(11) NOT NULL,
			`qty` int(11) DEFAULT 0,
			`alert_qty` int(11) DEFAULT 0,
			`cost` int(11) DEFAULT 0,
			`sku_code` varchar(255) DEFAULT NULL,
			`sku_val` varchar(255) DEFAULT NULL,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);

		echo "tbl_sku: Without data migration, data will resume default<br/>";

		$sql = "INSERT INTO `tbl_sku` (`item_id`, `sku_code`, `sku_val`, `default_price`, `qty`, `alert_qty`) VALUES
		('1', 'size-place', 'apple-lg-fuji', '20', '5', '0'),
		('1', 'size-place', 'apple-md-fuji', '10', '5', '0'),
		('1', 'size-place', 'apple-sm-fuji', '5', '5', '0'),
		('1', 'size-place', 'apple-sm-hk', '5', '5', '0'),
		('1', 'size-color-place', 'apple-sm-red-hk', '10', '5', '0'),
		('1', 'size-color', 'apple-sm-green', '10', '5', '0'),
		('2', 'size', 'orange-lg', '30', '10', '20'),
		('2', 'size', 'orange-md', '20', '5', '20'),
		('3', 'size-color', 'banana-md-yellow', '30', '10', '20'),
		('3', 'size-color', 'banana-sm-yellow', '20', '5', '20'),
		('3', 'place', 'banana-jp', '30', '10', '20'),
		('3', 'place', 'banana-cn', '20', '5', '20');";
		$this->mysqli->query($sql);

		echo "tbl_sku DONE <hr/>";
	}
	
	function tbl_sql_audit_log(){
		$sql = "DROP TABLE IF EXISTS `tbl_sql_audit_log`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_sql_audit_log` (
			`log_id` int(11) NOT NULL,
			`table` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			`field` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			`row_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			`action` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			`original_value` longtext COLLATE utf8mb4_unicode_ci,
			`updated_value` longtext COLLATE utf8mb4_unicode_ci,
			`updated_by_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			`create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
		  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);
	}
	function tbl_voucher(){
		$sql = "DROP TABLE IF EXISTS `tbl_voucher`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tbl_voucher` (
			`voucher_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`shop_id` int(11) NOT NULL,
			`name_tc` varchar(255) DEFAULT NULL,
			`name_en` varchar(255) DEFAULT NULL,
			`remarks` longtext DEFAULT NULL,
			`sales_admin_id` int (11) DEFAULT NULL,
			`discount` int(11) DEFAULT 0,
			`type` varchar(1) NOT NULL DEFAULT 'A', -- A for amt || P for percentage off
			`minimum_order_amt` int(11) DEFAULT 0,
			`ava_day` int(11) DEFAULT 0, -- 0 for unlimit
			`expiry_date` datetime DEFAULT NULL, -- replace the existing the validity Period(Months)
			`redeem` varchar(1) DEFAULT 'Y',
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);

		$sql = "SELECT * FROM vouchers";
		$res = $this->odb->get_Sql($sql);

		foreach($res as $row){
			$data = [
				"shop_id" => 1,
				"name_en" => $row["name_en"],
				"name_tc" => $row["name_zh_hk"],
				"ava_day" => intval($row["period"]) * 30,
				"minimum_order_amt" => $row['minimum_order_amount'],
				"type" => 'A',
				"discount" => $row['amount']
			];


			$id = $this->DB->insert_db("tbl_voucher", $data);
			echo "tbl_voucher row: $id || insert completed<br/>";
		}

	}
	function tmap_addon_product(){
		$sql = "DROP TABLE IF EXISTS `tmap_addon_product`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tmap_addon_product` (
			`map_id` int (11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`product_id` int(11) NOT NULL,
			`addon_id` int(11) NOT NULL,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

		$sql = "INSERT INTO `tmap_addon_product` (`product_id`, `addon_id`) VALUES
		(1, 1),(1, 2),(1, 3),
		(2, 1),(2, 2),
		(3, 3);";
		$this->mysqli->query($sql);

		echo "tmap_addon_product DONE<hr/>";
	}
	function tmap_cate_coupon(){}
	function tmap_cate_greeting(){}
	function tmap_cate_product(){
		
	}
	function tmap_cate_voucher(){}
	function tmap_combo_product(){}
	function tmap_content_product(){}
	function tmap_content_slider(){}
	function tmap_deli_product(){}
	function tmap_greeting_product(){}
	function tmap_grp_product(){}
	function tmap_item_shop(){
		$sql = "DROP TABLE IF EXISTS `tmap_item_shop`;";
		$this->mysqli->query($sql);
		$sql = "CREATE TABLE `tmap_item_shop` (
			`map_id` int (11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`item_id` int (11) NOT NULL,
			`shop_id` int (11) NOT NULL,
			`activate` varchar(1) DEFAULT 'Y',
			`updated_by_id` int(11) DEFAULT NULL,
			`last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`create_datetime` timestamp DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
		$this->mysqli->query($sql);

		
		$sql = "INSERT INTO `tmap_item_shop` (`item_id`, `shop_id`) VALUES
		(1, 1),(1, 2),(1, 3),
		(2, 1),(2, 2),
		(3, 1),(4, 1),(4, 2);";
		$this->mysqli->query($sql);
		echo "tmap_item_shop DONE <hr/>";
	}
	
	function tmap_new_member_voucher(){}
	function tmap_product_addon_cate(){}

	private function noToNY($val){
		if ($val == 1)
			return 'Y';
		else if ($val == 0)
			return 'N';
		else {
			return null;
		}
	}
	

	function home(){
		
	}
}
$HOME = new index;
$HOME->display();
