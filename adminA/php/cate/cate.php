<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;

		require_once ROOT.'/include/class/item.class.php';
		$this->item = new item($this->DB, $this->lang, $this->isAdmin);

		switch ($this->gthis->submode) {
			case 'cateList': 
				$result_array = $this->cateList();
				break;
			case 'cateDtl': // aka subcateList
				$result_array = $this->cateDtl();
				break;
			case 'subcateDtl':
				$result_array = $this->subcateDtl();
				break;
			
			default:
				break;
		}

		$this->gthis->assign('result_array',$result_array);
	}

	function cateList(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/cate.class.php';
		$cate = new cate($this->DB, $this->lang, $this->isAdmin);
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$res['cateList'] = $cate->getCateList($shop_id);


		array_push($this->gthis->breadcrumb, [
			"code" => "cateList",
			"url" => "admin/cate/cateList",
			"page_title" => $this->gthis->RMBC->getReturnString('cateList')
		]);
		return $res;
	}

	function cateDtl(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/cate.class.php';
		$cate = new cate($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/greeting.class.php';
		$greetingCls = new greeting($this->DB, $this->lang, $this->isAdmin);

		
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$cate_id = $this->gthis->parm_sql('request', 'cate_id', 'N');

		$cateDtl = $cate->getCateDtl($cate_id);
		$cateDtl['greetingCateList'] = $greetingCls->getGreetingCateList($shop_id, ["cate_id" => $cate_id]);

		$res['cateDtl'] = $cateDtl;
		// $res['greetingCateList'] = $greeting->getGreetingCateList($shop_id);


		array_push($this->gthis->breadcrumb, [
			"code" => "cateList",
			"url" => "admin/cate/cateList",
			"page_title" => $this->gthis->RMBC->getReturnString('cateList')
		],[
			"code" => "subcateDtl",
			"url" => "admin/cate/cateDtl?cate_id=" . ($cate_id ? $cate_id : "0"),
			"page_title" => $cate_id ? $res['cateDtl']['name_tc'] : $this->gthis->RMBC->getReturnString('addCate')
		]);
		return $res;
	}

	function subcateDtl(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/cate.class.php';
		$cate = new cate($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/greeting.class.php';
		$greetingCls = new greeting($this->DB, $this->lang, $this->isAdmin);

		
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$cate_id = $this->gthis->parm_sql('request', 'cate_id', 'N');
		$parent_cate_id = $this->gthis->parm_sql('request', 'parent_cate_id', 'N');

		$cateDtl = $cate->getCateDtl($cate_id);
		$cateDtl['greetingCateList'] = $greetingCls->getGreetingCateList($shop_id, ["cate_id" => $cate_id]);

		$res['cateDtl'] = $cateDtl;
		$res['parentCateDtl'] = $cate->getCateDtl($parent_cate_id);
		
		// $res['greetingCateList'] = $greeting->getGreetingCateList($shop_id);


		array_push($this->gthis->breadcrumb, [
			"code" => "cateList",
			"url" => "admin/cate/cateList",
			"page_title" => $this->gthis->RMBC->getReturnString('cateList')
		],[
			"code" => "cateDtl",
			"url" => "admin/cate/cateDtl?cate_id=$parent_cate_id",
			"page_title" => $res['parentCateDtl']['name_tc']
		],[
			"code" => "subcateDtl",
			"url" => "admin/cate/cateDtl?cate_id=" . ($cate_id ? $cate_id : "0"),
			"page_title" => $cate_id ? $res['cateDtl']['name_tc'] : $this->gthis->RMBC->getReturnString('addCate')
		]);
		return $res;
	}

}
?>