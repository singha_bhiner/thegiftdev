<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;
		
		require_once ROOT. '/include/apiDataBuilder.class.php';
		
		switch ($this->gthis->thirdmode) {
			case 'getMemberList':
				$this->getMemberList();
				break;
            case 'editMember':
                $this->editMember();
                break;
            case 'editMembership':
                $this->editMembership();
                break; 
			case 'addVoucher':
				$this->addVoucher();
				break;
			case 'delVoucher':
				$this->delVoucher();
				break;
			default:
				new apiDataBuilder(-84);
				break;
		}

		$result_array['lang'] = $this->lang;
		@$this->gthis->assign('result_array',$result_array);
	}

	function getMemberList(){
		require_once ROOT.'/include/class/member.class.php';
		$member = new member($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey');
		$page = $this->gthis->parm_sql('request', 'page');
		$limit = $this->gthis->parm_sql('request', 'limit');
		$sort = $this->gthis->parm_sql('request', 'sort');
		$order = $this->gthis->parm_sql('request', 'order');
		$sales = $this->gthis->parm_sql('request', 'admin_id'); // sales admin_id

		$filter = [
			"skey" => $skey ? $skey : null,
			"admin_id" => $sales ? $sales : null,
			"member_no" => 'Y'
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "member_id",
			"order" => $order ? $order : "DESC"
		];
		
		$res['memberList'] = $member->getMemberList($shop_id, $filter, $limiter, $sorting);

		new apiDataBuilder(0, $res);
	}

	function delVoucher(){
		if (intval($_SESSION['right']['member']) < 50){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/member.class.php';
		$member = new member($this->DB, $this->lang, $this->isAdmin);

		$member_voucher_id = $this->gthis->parm_sql('request', 'member_voucher_id');

		$res = $member->delVoucher($member_voucher_id);

		new apiDataBuilder(0, $res);
	}

	function addVoucher(){
		if (intval($_SESSION['right']['member']) < 50){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/member.class.php';
		$member = new member($this->DB, $this->lang, $this->isAdmin);

		$voucher_id = $this->gthis->parm_sql('request', 'voucher_id');
		$member_id = $this->gthis->parm_sql('request', 'member_id');

		$res = $member->addVoucher($voucher_id, $member_id);

		new apiDataBuilder(0, $res);
	}

    function editMembership(){
        if (intval($_SESSION['right']['member']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/member.class.php';
		$member = new member($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "membership_id"],
			["key" => "name_tc"],
			["key" => "name_en"],
			["key" => "discount", "type" => "int"],
			["key" => "upgrade_point", "type" => "int"],
			["key" => "upgrade_to_membership_id", "type" => "int"],
			["key" => "activate", "type" => "NY"]
		]);

		$res = $member->editMembership($data);
		
        new apiDataBuilder(0, $res);
    }

    function editMember(){
		if (intval($_SESSION['right']['member']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/member.class.php';
		$member = new member($this->DB, $this->lang, $this->isAdmin);

        $member_id = $this->gthis->parm_sql('request', 'member_id');
		$data = [];
		$fieldArr = ["is_monthly_payment", "is_charge_cash_dollar", "name", "product_id_arr", "slot_id_arr", "region_id_arr",  "nav_color", "member_id", "membership_id", "member_no", "email", "mobile", "method", "verify", "sales_admin_id", "point", "exp", "cash_dollar", "remark", "company_name", "shop_id", "activate"];

		foreach ($fieldArr as $field){
			if (isset($_REQUEST[$field])){
				$$field = $this->gthis->parm_sql('request', $field);
				switch ($field){
					case 'shop_id':
						if (!$$field){
							new apiDataBuilder(-99, null, "$field cannot be null");
						}
						break;
					case 'member_id':
					case 'membership_id':
						$$field = $this->DB->parseInt($$field);
						break;
					case 'product_id_arr':
					case 'slot_id_arr':
					case 'region_id_arr':
						$$field = $this->DB->parseIntArr($$field);
						break;
				}
				$data[$field] = $$field;
			}
		}

		$password = $this->gthis->parm_sql('request', 'password');
		if ($password){
			$password = $this->DB->parsePassword($password);
			$data['password'] = $password;
		}

        
		$res = $member->editMember($member_id, $data);

		new apiDataBuilder(0, $res);
	}

	
}
?>