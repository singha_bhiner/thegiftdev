<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->DB = $g_DB;

		require_once ROOT. '/include/apiDataBuilder.class.php';
		
		switch ($this->gthis->thirdmode) {
			case 'productDtl':
				$this->productDtl();
				break;
			case 'paypalCheckout':
				$this->paypalCheckout();
				break;
			case 'aeCheckout':
				$this->aeCheckout();
				break;
			case 'paypalCheckoutApproved': // webhook
				$this->paypalCheckoutApproved();
				break;
			case 'aeCheckoutApproved': // webhook
				$this->aeCheckoutApproved();
				break;
			case 'couponApply':
				$this->couponApply();
				break;
			case 'voucherApply': 
				$this->voucherApply();
				break;
			case 'getDistrictList':
				$this->getDistrictList();
				break;
			case 'getTimeslotList':
				$this->getTimeslotList();
				break;
			case 'saveAddress':
				$this->saveAddress();
				break;
			case 'paypalStatusUpdate':
				$this->paypalStatusUpdate();
				break;
			case 'checkHolidayAva':
				$this->checkHolidayAva();
				break;
			case 'offlineCheckout':
				$this->offlineCheckout();
				break;
			default:
				new apiDataBuilder(-84);
				break;
		}

		$result_array['lang'] = $this->lang;
		@$this->gthis->assign('result_array',$result_array);
	}

	function productDtl(){
		$product_id = $this->gthis->parm_in('request', 'product_id', 'Y');
		$shop_id = $this->gthis->parm_in('session', 'shop_id', 'Y');

		require_once ROOT.'/include/class/product.class.php';
        $productCls = new product($this->DB, $this->gthis->lang);

		$product = $productCls->getProductDtl($product_id, $shop_id);

		$res = $product;

		new apiDataBuilder(0, $res);
	}

	function paypalStatusUpdate(){
		$member = $_SESSION['member'];
		$member_id = $member['member_id'];
		$shop_id = $member['shop_id'];
		

		$payment_id = $this->gthis->parm_in('request', 'payment_id', 'Y');
		$payment_id = base64_decode($payment_id);

		require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->gthis->lang);

		$payment = $orderCls->getOrderPaymentDtl($payment_id);
		
		if ($payment['payment_status'] == 'completed')
			new apiDataBuilder(0, $res);
		else if ($payment['payment_status'] == 'paymentValueNotMatch')
			new apiDataBuilder(21, $res);
		else
			new apiDataBuilder(20, $res);


	}

	function checkHolidayAva(){
		$shop_id = $this->gthis->parm_sql('request', 'shop_id', 'Y');

		require_once ROOT.'/include/class/holiday.class.php';
		$holidayCls = new holiday($this->DB, $this->gthis->lang);

		$holidayList = $holidayCls->getHolidayList($shop_id, ["isCurrent" => 'Y']);
		

		$deli_arr = $this->gthis->parm_sql('request', "deli");
		
		$deliList = json_decode(str_replace("\\", "", $deli_arr), true);
		foreach ($deliList as $k => $deli){
			$deli_date = intval(strtotime($deli['deli_date']));
			
			foreach ($holidayList as $kk => $holi){
				if (!$holi['product_id_arr'] || in_array($deli['product_id'], $holi['product_id_arr'])){
					// check is the product including in this holiday rule

					if (intval(strtotime($holi['start_date'])) <= $deli_date && intval(strtotime($holi['end_date'])) >= $deli_date) {
						// check it is within the time of holiday rule

						$assumeConsumed = $holi['assumeConsumed']? $holi['assumeConsumed'] + 1: $holi['consumed'] + 1;
						if ($holi['isDayoff'] == 'Y'){
							$errorMsg = "產品(".$deli['product_name'].")不能在假期制限(".$holi['adm_name'].")下單";
							new apiDataBuilder(13, $holi, $errorMsg);
						} else if ($assumeConsumed > $holi['quota_everyday']){
							$errorMsg = "產品(".$deli['product_name'].")已超過假期制限(".$holi['adm_name'].")每天可以下單數量<br/>每天可下單數量: ". $holi['quota_everyday'] . "<br/>今天已下單數量: ". $holi['consumed'];
							new apiDataBuilder(12, null, $errorMsg);
						} else {
							$holidayList[$kk]['assumeConsumed'] = $assumeConsumed;
						}
					} else {
						// echo "outsided<br/>";
						
						
						// if (strtotime($holi['start_date']) <= $deli_date ){
							
						// 	echo "star: ". strtotime($holi['start_date']) ."<br/>";
						// 	echo "date: ". $deli_date . "<BR/>";
						// }
						// if (strtotime($holi['end_date'] >= $deli_date)){
						// 	echo "B";
						// 	echo "date: ". $deli_date . "<BR/>";
						// 	echo "ende: ". strtotime($holi['end_date']) ."<br/>";
							
						// }
					}

					
				}
			}
		}

		new apiDataBuilder(0);
	}

	function saveAddress(){
		$member = $_SESSION['member'];
		$member_id = $member['member_id'];
		$shop_id = $this->gthis->parm_sql("request", "shop_id", 'Y');

		require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->gthis->lang);

		$order = $orderCls->getOrderDtlByMemberid($shop_id, $member_id); // get the updated order
		$order_id = $order['order_id'];
	
		$orderData = [];
		
		$orderFieldArr = ["sender_name", "sender_email", "sender_phone", "remarks_from_client", "remarks_internal", "remarks_external"];
		$deliFieldArr = ["deli_arr"];
		
		foreach ($orderFieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
				case 'sender_email':
					$$field = $this->DB->parseEmail($$field);
					break;
				case 'sender_phone':
					$$field = $this->DB->parseInt($$field);
					break;
			}
			$$field? $orderData[$field] = $$field: null;
		}

		foreach ($deliFieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
				case "deli_arr":
					$tmp = str_replace("\\\"", "\"", $$field); // parse /" to "
					$tmp = str_replace("\\\\\"", "\\\"", $tmp); // parse //" to /" (escape the json escape)
					$$field = json_decode($tmp, true);
					break;
			}
			$$field? $deliData[$field] = $$field: null;
		}
		
		$orderCls->editOrder($order_id, $orderData);
		
		foreach($deli_arr as $k => $deli){
			$deli['shop_id'] = $shop_id;
			$orderCls->editDeli($deli['deli_id'], $deli);
		}

		$orderCls->updateOrderDeliPrice($order, $member_id); // re-calculate the price
		$order = $orderCls->getOrderDtlByMemberid($shop_id, $member_id); // get the updated order


		new apiDataBuilder(0);
	}

	function getDistrictList(){
		$deli_group_id_arr = $this->gthis->param(["key" => "deli_group_id_arr", "type" => "intArrV1"]);
		$shop_id = $this->gthis->parm_sql("request", "shop_id", "Y");


		// $deli_group_id_arr = array_unique($deli_group_id_arr);
		

		require_once ROOT.'/include/class/delivery.class.php';
        $deliveryCls = new delivery($this->DB, $this->gthis->lang, true);
		$custList = [];

		foreach ($deli_group_id_arr as $deli_group_id){

			$districtList = $deliveryCls->getDistrictList($shop_id);
			foreach ($districtList as $k => $district){
				// $regionList = $deliveryCls->getRegionList($shop_id, ["deli_group_id" => $deli_group_id, "district_id" => $district['district_id']]);
				$regionList = $deliveryCls->getRegionListClient($district['district_id'], [$deli_group_id], $shop_id);
				
				// foreach ($regionList as $kk => $region){
				// 	$regionList[$kk]['timeslotList'] = $deliveryCls->getTimeslotList($shop_id, ["deli_group_id" => $deli_group_id, "region_id" => $region['region_id']]);
				// }
				$districtList[$k]['region_list'] = $regionList;
			}
			array_push($custList, [
				"deli_group_id" => $deli_group_id,
				"districtList" => $districtList
			]);
		}

		$res = $custList;

		new apiDataBuilder(0, $res);
	}

	function getTimeslotList(){
		require_once ROOT.'/include/class/delivery.class.php';
        $deliveryCls = new delivery($this->DB, $this->gthis->lang, true);

		$deli_group_id_arr = $this->gthis->param(["key" => "deli_group_id_arr", "type" => "intArrV1"]);
		$deli_group_id_arr = array_unique($deli_group_id_arr);

		$shop_id = $this->gthis->parm_in('session', 'shop_id', 'Y');
		$region_id = $this->gthis->parm_sql("request", "region_id", "Y");
		
		$timeslotList = $deliveryCls->getTimeslotListClient($region_id, $deli_group_id_arr, $shop_id);

		$res['timeslotList'] = $timeslotList;

		new apiDataBuilder(0, $res);
	}

	// function getTimeslotList(){

	// 	$region_id = $this->gthis->parm_sql("request", "region_id", "Y");
	// 	$member = $_SESSION['member'];
	// 	$member_id = $member['member_id'];
	// 	$shop_id = $this->gthis->parm_in('session', 'shop_id', 'Y');

	// 	require_once ROOT.'/include/class/order.class.php';
	// 	$orderCls = new order($this->DB, $this->gthis->lang);
	// 	require_once ROOT.'/include/class/delivery.class.php';
	// 	$deliveryCls = new delivery($this->DB, $this->gthis->lang);
		
	// 	$order = $orderCls->getCart($shop_id, $member_id);

	// 	$ava_b4_day = 0;
		
	// 	$nowTime = date('H:i:s');
		
	// 	$bool_ava_cut_off = false;
	// 	$cust_deli_id_arr = [];


	// 	foreach ($order['deliList'] as $deli){
	// 		if ($deli['productDtl']['ava_b4_day'] > $ava_b4_day){
	// 			$ava_b4_day = $deli['productDtl']['ava_b4_day'];
	// 		}
	// 		if (!$bool_ava_cut_off && $deli['productDtl']['ava_b4_cutoff']){
	// 			$cutoff = date('H:i:s', strtotime($deli['productDtl']['ava_b4_cutoff']));
	// 			if ($nowTime > $cutoff){
	// 				$bool_ava_cut_off = true;
	// 			}
	// 		}

	// 		array_push($cust_deli_id_arr, $deli['productDtl']['deli_id']);
	// 	}

	// 	$cust_deli_id_arr = array_unique($cust_deli_id_arr);

	// 	$timeslotList = $deliveryCls->getTimeslotListByGrpIdArrInRegion($shop_id, $cust_deli_id_arr, $region_id);

		
	// 	if ($bool_ava_cut_off)
	// 		$ava_b4_day++;
			
	// 	$res = [
	// 		"ava_b4_day" => $ava_b4_day,
	// 		"timeslotList" => $timeslotList
	// 	];


	// 	new apiDataBuilder(0, $res);
	// }

	// function getTimeslotList(){
	// 	$product_id = $this->gthis->parm_sql("request", "product_id", 'Y');
	// 	$shop_id = $this->gthis->parm_sql("request", "shop_id", "Y");

	// 	require_once ROOT.'/include/class/delivery.class.php';
    //     $deliveryCls = new delivery($this->DB, $this->gthis->lang, true);
	// 	require_once ROOT.'/include/class/product.class.php';
    //     $productCls = new product($this->DB, $this->gthis->lang, true);

	// 	$productDtl = $productCls->getProductDtl($product_id, $shop_id);

	// 	$timeslotList = $deliveryCls->getTimeslotList($shop_id, ["cust_deli_id" => $productDtl['deli_id']]);

	// 	$res = $timeslotList;

	// 	new apiDataBuilder(0, $res);
	// }
	
	function couponApply(){
		$coupon_code = $this->gthis->parm_sql("request", "coupon_code", 'Y');
		// $final_price = $this->gthis->parm_sql("request", "final_price", 'Y');
		// $deli_arr = $this->gthis->parm_sql("request", "deli_arr", 'Y');
		$shop_id = $_SESSION['member']['shop_id'];
		$member_id = $_SESSION['member']['member_id'];

		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);
        require_once ROOT.'/include/class/coupon.class.php';
        $couponCls = new coupon($this->DB, $this->gthis->lang);
        require_once ROOT.'/include/class/product.class.php';
        $productCls = new product($this->DB, $this->gthis->lang, true);
		
		$order = $orderCls->getOrderDtlByMemberid($shop_id, $member_id); // get the updated order
		$deliList = $order['deliList'];
		$final_price = $order['product_price'] + $order['addon_price'];
		
		// get the productList related category and productList
		$cate_arr = [];
		$product_arr = [];
		foreach($deliList AS $k => $deli){
			array_push($product_arr, $deli['product_id']);
			$product = $productCls->getProductDtl($deli['product_id'], $shop_id);
			foreach($product['cate'] as $kk => $cate){
				foreach($cate['subcate'] as $kkk => $subcate){
					array_push($cate_arr, $subcate['cate_id']);
				}
			}
		}

		$res['couponDtl'] = $couponCls->getCouponDtlByCode($coupon_code, $final_price, $cate_arr, $product_arr);

		if ($res['couponDtl']){
			new apiDataBuilder(0, $res);
		} else {
			new apiDataBuilder(11);
		}
	}

	function voucherApply(){
        $member_voucher_id = $this->gthis->parm_sql("request", "member_voucher_id", 'Y');
		// $final_price = $this->gthis->parm_sql("request", "final_price", 'Y');
		// $deli_arr = $this->gthis->parm_sql("request", "deli_arr", 'Y');
		$shop_id = $_SESSION['member']['shop_id'];
		$member_id = $_SESSION['member']['member_id'];

		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);
        require_once ROOT.'/include/class/voucher.class.php';
        $voucherCls = new voucher($this->DB, $this->gthis->lang);
        require_once ROOT.'/include/class/product.class.php';
        $productCls = new product($this->DB, $this->gthis->lang, true);

		$order = $orderCls->getOrderDtlByMemberid($shop_id, $member_id); // get the updated order
		$deliList = $order['deliList'];
		$final_price = $order['product_price'] + $order['addon_price'];
		
		// get the productList related category
		$cate_arr = [];
		foreach($deliList as $deli){
			$product = $productCls->getProductDtl($deli['product_id'], $shop_id);
			foreach($product['cate'] as $cate){
				foreach($cate['subcate'] as $subcate){
					array_push($cate_arr, $subcate['cate_id']);
				}
			}
		}

		$res['voucherDtl'] = $voucherCls->getVoucherDtlByMemberVoucher($member_voucher_id, $final_price, $cate_arr);
		
		if ($res['voucherDtl']){
			new apiDataBuilder(0, $res);
		} else {
			new apiDataBuilder(10);
		}
	}

	function aeCheckoutApproved(){
		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);
		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang);
		
		$aeBody = file_get_contents('php://input');
		$aeBody = json_decode($aeBody, true);
		
		if ($aeBody['transaction']['type'] == 'CAPTURE'){
			$payment_id = $aeBody['order']['id']; 
			$act_payment_value = $aeBody['transaction']['amount'];
			$transaction_id = $aeBody['transaction']['receipt'];



			$payment = $orderCls->getOrderPaymentDtl($payment_id);

			$order = $orderCls->getOrderDtl($payment['order_id']);
			$order_id = $order['order_id'];
			
			$data = [
				"raw_return" => json_encode($aeBody),
				"order_id" => $order_id,
				"actual_payment_value" => $act_payment_value,
				"transaction_id" => $transaction_id
			];
			

			if ($act_payment_value == $payment['expected_payment_value']){
				$data["payment_status"] = "completed";
			} else {
				$data["payment_status"] = "paymentValueNotMatch";
			}

			$orderCls->editOrderPayment($payment_id, $data);
			$order_no = $orderCls->genOrderNo($shop_id);
			$orderCls->editOrder($order_id, [
				"order_no" => $order_no,
				"order_status" => "paid"
			]);

			foreach ($order['deliList'] as $deli){
				$data = [
					"deli_status" => 'paid', 
					// "shop_id" => $shop_id,
					"deli_no" => $orderCls->genDeliNo($order_no, $deli['deli_date'])
				];
				$orderCls->editDeli($deli['deli_id'], $data);
			}

			$member_id = $order['member_id'];
			if ($order['member_voucher_id']){
				require_once ROOT.'/include/class/voucher.class.php';
				$voucherCls = new voucher($this->DB, $this->gthis->lang);
				$voucherCls->executeVoucher($order['member_voucher_id']);
			}
			if ($order['cash_dollar_used'] && $member_id )
				$memberCls->cashDollarUsed($order['cash_dollar_used'], $member_id);
			if ($order['cash_dollar_gain'] && $member_id)
				$memberCls->cashDollarGain($order['cash_dollar_gain'], $member_id);

		}
		

		new apiDataBuilder(0, true);
	}
	
	function paypalCheckoutApproved(){
		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);
		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang);
	
		$paypalBody = file_get_contents('php://input');
		$paypalBody = json_decode($paypalBody, true);
		
		$payment_id = $paypalBody['resource']['purchase_units'][0]['reference_id'];
		$order_id = $paypalBody['resource']['purchase_units'][0]['invoice_id'];
		$act_payment_value = $paypalBody['resource']['purchase_units'][0]['amount']['value'];
		$transaction_id = $paypalBody['resource']['purchase_units'][0]['payments']['captures'][0]['id'];
		
		$payment = $orderCls->getOrderPaymentDtl($payment_id);
		$data = [
			"raw_return" => json_encode($paypalBody),
			"order_id" => $order_id,
			"actual_payment_value" => $act_payment_value,
			"transaction_id" => $transaction_id
		];

		if ($act_payment_value == $payment['expected_payment_value']){
			$data["payment_status"] = "completed";
		} else {
			$data["payment_status"] = "paymentValueNotMatch";
		}

		$orderCls->editOrderPayment($payment_id, $data);
		$order_no = $orderCls->genOrderNo($shop_id);
		$orderCls->editOrder($order_id, [
			"order_no" => $order_no,
			"order_status" => "paid"
		]);

		$order = $orderCls->getOrderDtl($order_id);
		foreach ($order['deliList'] as $deli){
			$data = [
				"deli_status" => 'paid', 
				"deli_no" => $orderCls->genDeliNo($order_no, $deli['deli_date'])
			];
			$orderCls->editDeli($deli['deli_id'], $data);
		}

		$member_id = $order['member_id'];
		if ($order['member_voucher_id']){
			require_once ROOT.'/include/class/voucher.class.php';
			$voucherCls = new voucher($this->DB, $this->gthis->lang);
			$voucherCls->executeVoucher($order['member_voucher_id']);
		}
		if ($order['cash_dollar_used'] && $member_id )
			$memberCls->cashDollarUsed($order['cash_dollar_used'], $member_id);
		// if ($order['cash_dollar_gain'] && $member_id)
		// 	$memberCls->cashDollarGain($order['cash_dollar_gain'], $member_id);
		// if ($order['point_gain'] && $member_id)
		// 	$memberCls->pointGain($order['point_gain'], $member_id);
		// if ($order['exp_gain'] && $member_id)
		// 	$memberCls->expGain($order['exp_gain'], $member_id);


		// require_once ROOT.'/include/pushNotification.php';
		// $pnCls = new pushNotification();
		
		// $pnCls->createPushNotification($this->gthis->baseURL."/admin/shipment/orderDtl?order_id=$order_id", "有新下單, 按此查看");

		new apiDataBuilder(0, $res);
	}
	
	function paypalCheckout(){
		$member = $_SESSION['member'];
		$shop_id = $this->gthis->parm_in('session', 'shop_id', 'Y');
		$coupon_code = $this->gthis->parm_sql("request", "coupon_code");
		$member_voucher_id = $this->gthis->parm_sql("request", "member_voucher_id");
		// $cash_dollar = $this->gthis->parm_sql("request", "cash_dollar");
		// assume use all cash dollar every time
		$cash_dollar = $member['cash_dollar'];
		
		$remarks_from_client = $this->gthis->parm_sql("request", "remarks_from_client");
		
		$member_id = $member['member_id'];

		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);

		$order = $orderCls->getOrderDtlByMemberid($shop_id, $member_id);
		if (!$order){
			new apiDataBuilder(-51);
		}
		
		$orderCls->updateOrderDeliPrice($order, $member_id, $coupon_code, $member_voucher_id, $cash_dollar); // re-calculate the price
		$order = $orderCls->getOrderDtlByMemberid($shop_id, $member_id); // get the updated order

		$orderData = [
			"order_datetime" => date("Y-m-d H:i:s"),
			"remarks_from_client" => $remarks_from_client
		];
		$orderCls->editOrder($order['order_id'], $orderData);
		
		foreach($order['deliList'] as $k => $deli){
			$deliData = [
				"shop_id" => $shop_id,
			];
			$orderCls->editDeli($deli['deli_id'], $deliData);
		}

		$payment_id = $orderCls->createPaypalPayment($order['order_id'], $order['final_price']);

		$waiting_code = [
			"payment_id" => $payment_id,
			"member_voucher_id" => $member_voucher_id,
			"coupon_code" => $coupon_code
		];
		
		$res = [
			"waiting_code" => base64_encode(json_encode($waiting_code)),
			"payment_id" => $payment_id,
			"member_id" => $member_id,
			"order_id" => $order['order_id'],
			"final_price" => $order['final_price']
		];
		
		new apiDataBuilder(0, $res);
	}

	function aeCheckout(){
		$member = $_SESSION['member'];
		$shop_id = $this->gthis->parm_in('session', 'shop_id', 'Y');
		$coupon_code = $this->gthis->parm_sql("request", "coupon_code");
		$member_voucher_id = $this->gthis->parm_sql("request", "member_voucher_id");
		// $cash_dollar = $this->gthis->parm_sql("request", "cash_dollar");
		// assume use all cash dollar every time
		$cash_dollar = $member['cash_dollar'];
		
		$remarks_from_client = $this->gthis->parm_sql("request", "remarks_from_client");
		
		$member_id = $member['member_id'];

		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);

		$order = $orderCls->getOrderDtlByMemberid($shop_id, $member_id);
		if (!$order){
			new apiDataBuilder(-51);
		}

		$orderCls->updateOrderDeliPrice($order, $member_id, $coupon_code, $member_voucher_id, $cash_dollar); // re-calculate the price
		$order = $orderCls->getOrderDtlByMemberid($shop_id, $member_id); // get the updated order

		$orderData = [
			"order_datetime" => date("Y-m-d H:i:s"),
			"remarks_from_client" => $remarks_from_client
		];
		$orderCls->editOrder($order['order_id'], $orderData);
		
		foreach($order['deliList'] as $k => $deli){
			$deliData = [
				"shop_id" => $shop_id,
			];
			$orderCls->editDeli($deli['deli_id'], $deliData);
		}

		$payment_id = $orderCls->createAePayment($order['order_id'], $order['final_price']);

		$waiting_code = [
			"payment_id" => $payment_id,
			"member_voucher_id" => $member_voucher_id,
			"coupon_code" => $coupon_code
		];

		/////////////////////////////// AE CHECKOUT START ///////////////////////////////
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://gateway-japa.americanexpress.com/api/nvp/version/63");
		curl_setopt($ch, CURLOPT_POST, 1);


		$field_list = [
			"apiOperation=INITIATE_CHECKOUT",
			"apiPassword=".AE_API_PASSWORD,
			"apiUsername=".AE_API_USERNAME,
			"merchant=".AE_MERCHANT,
			"interaction.operation=PURCHASE",
			"order.id=". $payment_id,
			"order.amount=" . $order['final_price'],
			"order.currency=HKD"
		];

		$postField = join("&", $field_list);
		// echo $postField;

		curl_setopt($ch, CURLOPT_POSTFIELDS, $postField);

		// In real life you should use something like:
		// curl_setopt($ch, CURLOPT_POSTFIELDS, 
		//          http_build_query(array('postvar1' => 'value1')));

		// Receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec($ch);
		
		$ae_list = [];
		$return_list = explode("&", $server_output);

		foreach ($return_list as $return){
			$tmp = explode("=", $return );

			if (str_contains($tmp[0], ".")){
				$t2 = explode(".", $tmp[0]);
				$ae_list[$t2[0]][$t2[1]] = $tmp[1];
			} else {
				$ae_list[$tmp[0]] = $tmp[1];
			}
		}

		curl_close ($ch);

		/////////////////////////////// AE CHECKOUT END   ///////////////////////////////
		
		$res = [
			"waiting_code" => base64_encode(json_encode($waiting_code)),
			"ae_session_id" => $ae_list['session']['id'],
			"order_id" => $order_no
		];
		
		new apiDataBuilder(0, $res);
	}

	function offlineCheckout(){
		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang);

		$member = $_SESSION['member'];
		$shop_id = $this->gthis->parm_in('session', 'shop_id', 'Y');
		$coupon_code = $this->gthis->parm_sql("request", "coupon_code");
		$member_voucher_id = $this->gthis->parm_sql("request", "member_voucher_id");
		// $cash_dollar = $this->gthis->parm_sql("request", "cash_dollar");
		// assume use all cash dollar every time
		$cash_dollar = $member['cash_dollar'];
		
		$remarks_from_client = $this->gthis->parm_sql("request", "remarks_from_client");
		
		$member_id = $member['member_id'];

		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);

		$order = $orderCls->getOrderDtlByMemberid($shop_id, $member_id);
		if (!$order){
			new apiDataBuilder(-51);
		}
		
		$orderCls->updateOrderDeliPrice($order, $member_id, $coupon_code, $member_voucher_id, $cash_dollar); // re-calculate the price
		$order = $orderCls->getOrderDtlByMemberid($shop_id, $member_id); // get the updated order
		$order_no = $orderCls->genOrderNo($shop_id);

		$orderData = [
			"reserve_time" => date("Y-m-d ") . DAILY_CUTOFF_TIME,
			"order_status" => "waitingPayment",
			"order_datetime" => date("Y-m-d H:i:s"),
			"remarks_from_client" => $remarks_from_client,
			"order_no" => $order_no,
		];
		$orderCls->editOrder($order['order_id'], $orderData);
		
		foreach($order['deliList'] as $k => $deli){
			$deliData = [
				"deli_status" => "waitingPayment",
				"deli_no" => $orderCls->genDeliNo($order_no, $deli['deli_date'])
			];
			$orderCls->editDeli($deli['deli_id'], $deliData);
		}

		$member_id = $order['member_id'];
		if ($order['member_voucher_id']){
			require_once ROOT.'/include/class/voucher.class.php';
			$voucherCls = new voucher($this->DB, $this->gthis->lang);
			$voucherCls->executeVoucher($order['member_voucher_id']);
		}
		if ($order['cash_dollar_used'] && $member_id )
			$memberCls->cashDollarUsed($order['cash_dollar_used'], $member_id);

		$payment_id = $orderCls->createOfflinePayment($order['order_id'], $order['final_price']);
		
		$res = [
			"order_info" => base64_encode(json_encode([
				"order_no" => $order_no,
				"final_price" => $order['final_price']
			])),
		];
		
		new apiDataBuilder(0, $res);
	}
} 
?>