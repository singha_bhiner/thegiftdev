<?php
require_once ROOT. '/include/class/abstract.class.php';
class extraPHP extends baseClass{
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->DB = $g_DB;
        $this->res = null;

		require_once ROOT. '/include/apiDataBuilder.class.php';
		
		switch ($this->gthis->thirdmode) {
			case 'getAlertList':
				$this->getAlertList();
				break;
			case 'getAlertSummary':
				$this->getAlertSummary();
				break;
            case 'editAlert':
                $this->editAlert();
                break;
            case 'ignore':
                $this->ignore();
                break;
            case 'getNotificationList':
                $this->getNotificationList();
                break;
            case 'refreshAlertStatus':
                $this->refreshAlertStatus();
                break;
            
			default:
				new apiDataBuilder(-84);
				break;
		}

		new apiDataBuilder(0, $this->res);
	}

    function getNotificationList(){
        require_once ROOT.'/include/class/alert.class.php';
        $alertCls = new alert($this->DB, $this->gthis->lang, true);
        require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->gthis->lang, true);

        $data = $this->gthis->paramBuilder([
            ["key" => "shop_id"],
            ["key" => "admin", "request" => "session"],
        ]);

        $alertCls->refreshAlertStatus($data['shop_id'], ['new_order']);

        $alert_list = $alertCls->getAlertList([
            "type" => "new_order", 
            "admin_id" => $data['admin']['admin_id'],
            "alert_status" => "10_reminding",
            "shop_id" => $data['shop_id']
        ], "alert_time DESC");

        foreach($alert_list as $k => $alert){
            $alert_list[$k]['order'] = $orderCls->getOrderDtl($alert['ref_id']);
        }

        $res['alert_list'] = $alert_list;

        $this->res = $res;
    }

    function editAlert(){
        require_once ROOT.'/include/class/alert.class.php';
        $alertCls = new alert($this->DB, $this->gthis->lang, true);

        $data = $this->gthis->paramBuilder([
            ["key" => "remarks"],
            ["key" => "alert_id", "type" => "int"]
        ]);

        $alertCls->editAlert($data);
    }

    function refreshAlertStatus(){
        require_once ROOT.'/include/class/alert.class.php';
        $alertCls = new alert($this->DB, $this->gthis->lang, true);

        $data = $this->gthis->paramBuilder([
            ["key" => "type_arr", "type" => "arr"],
            ["key" => "admin", "request" => "session"],
            ["key" => "shop_id"]
        ]);

        $time_list = $alertCls->getAlertTimeSession();
        foreach($data['type_arr'] as $k => $type){
            $alertCls->refreshAlertStatus($data['shop_id'], [$type]);
            $alert_list = $alertCls->getAlertList([
                "type" => $type, 
                "admin_id" => $data['admin']['admin_id'],
                "alert_status" => "10_reminding",
                "shop_id" => $data['shop_id']

            ]);

            foreach ($alert_list as $alert) {
                $tidx = array_search($alert['rounded_alert_time'], array_column($time_list, "rounded_alert_time"));
                if (!isset($time_list[$tidx][$type. "_list"]))
                    $time_list[$tidx][$type. "_list"] = []; 


                array_push($time_list[$tidx][$type. "_list"], $alert);
            }
        }

        $res['time_list'] = $time_list;

        $this->res = $res;
    }

    function ignore(){
        $data = $this->gthis->paramBuilder([
            ["key" => "ignore_time", "type" => "date"],
            ["key" => "alert_id", "type" => "int"],
            ["key" => "admin", "request" => "session"],
        ]);

        $data['admin_id'] = $data['admin']['admin_id'];

        require_once ROOT.'/include/class/alert.class.php';
        $alertCls = new alert($this->DB, $this->gthis->lang, $this->isAdmin);

        $alertCls->editIgnore($data);

    }

    function getAlertList(){
        $data = $this->gthis->paramBuilder([
            ["key" => "type_arr", "type" => "arr"],
            ["key" => "admin", "request" => "session"],
            ["key" => "shop_id"]
        ]);
        
        require_once ROOT.'/include/class/alert.class.php';
        $alertCls = new alert($this->DB, $this->gthis->lang, true);
        
        // $alertCls->refreshAlertStatus();
        $res = [];
        
        $time_list = $alertCls->getAlertTimeSession();
        foreach($data['type_arr'] as $k => $type){
            $alert_list = $alertCls->getAlertList([
                "type" => $type, 
                "admin_id" => $data['admin']['admin_id'],
                "alert_status" => "10_reminding",
                "shop_id" => $data['shop_id']
            ]);

            foreach ($alert_list as $alert) {
                $tidx = array_search($alert['rounded_alert_time'], array_column($time_list, "rounded_alert_time"));
                if (!isset($time_list[$tidx][$type . "_list"]))
                    $time_list[$tidx][$type . "_list"] = []; 

                array_push($time_list[$tidx][$type . "_list"], $alert);
            }
        }
        
        $res['time_list'] = $time_list;
    

        $this->res = $res;
    }

    function getAlertSummary(){
        $data = $this->gthis->paramBuilder([
            ["key" => "shop_id", "request" => "session"],
            ["key" => "admin", "request" => "session"],
        ]);

        require_once ROOT.'/include/class/alert.class.php';
        $alertCls = new alert($this->DB, $this->gthis->lang, true);

        $res['product_out_of_stock_len'] = sizeof($alertCls->getAlertList(["type" => "product_out_of_stock", "alert_status" => "10", "admin_id" => $data['admin']['admin_id']]));
        $res['item_out_of_stock_len'] = sizeof($alertCls->getAlertList(["type" => "item_out_of_stock", "alert_status" => "10", "admin_id" => $data['admin']['admin_id']]));
        $res['publish_len'] = sizeof($alertCls->getAlertList(["shop_id" => $data['shop_id'], "type" => "publish", "alert_status" => "10", "admin_id" => $data['admin']['admin_id']]));
        $res['deposit_slip_len'] = sizeof($alertCls->getAlertList(["shop_id" => $data['shop_id'], "type" => "deposit_slip", "alert_status" => "10", "admin_id" => $data['admin']['admin_id']]));



        $this->res = $res;
    }

    
}
?>