<?php
class fileHandler {
	private $DB;

	public function __construct($xthis,$DB) {
		$this->gthis = $xthis;
		$this->DB = $DB;
		require_once ROOT.'/include/config.php';
	}

	/* ***************************************************************************************** */
	/*	Author : Leo Liao
	/*	Date : 19 Jun 2021
	/*  LUD  : 19 Jun 2021
	 *  Desc : transfer html file[] object to seperate file */
	/** 	   
	 * 	@param $html_files			   
	 *  @return $files (object)	    
	/* ***************************************************************************************** */

	function htmlFileArrayToSperateFileObject($html_files){
		// build stand alone file obect
		$files = array();
		if(is_array($html_files['name'])){
			foreach($html_files['name'] as $k=>$i){
				$file = array();
				$file['name'] = $i;
				$file['type'] = $html_files['type'][$k];
				$file['tmp_name'] = $html_files['tmp_name'][$k];
				$file['error'] = $html_files['error'][$k];
				$file['size'] = $html_files['size'][$k];
				array_push($files,$file);
			}	
		} else {
			$file = $html_files;
			array_push($files,$file);
		}
		
		return $files;
	}

	/* ***************************************************************************************** */
	/*	Author : Leo Liao
	/*	Date : 19 Jun 2021
	/*  LUD  : 19 Jun 2021
	 *  Desc : transfer html file[] object to seperate file */
	/** 	   
	 * 	@param $file			   
	 * 	@param $dest_file_name			   
	 * 	@param $destination_dir			   
	 *  @return $files (object)	    
	/* ***************************************************************************************** */

	function uploadFileGeneral($file,$dest_file_name=null,$destination_dir,$allowed_ext=null,$max_file_size_in_bytes=3000000){
		$ori_file_name = $file['name'];

		$temp = explode('.', $ori_file_name);
		$ext = $temp[sizeOf($temp)-1];
		$ext = strtolower($ext);

		if(isset($allowed_ext)){
			if (!in_array($ext, $allowed_ext)) {
			    $result['code'] = '-1';
			    $result['msg'] = 'Invalid extension';
			    $result['data'] = $this->gthis->dataBuilder(array(),'invalidFileExtension');
				return $result;
			}
		}

		if($file['size']>$max_file_size_in_bytes){
			$result['code'] = '-1';
		    $result['msg'] = 'File size too big. (max:'.($max_file_size_in_bytes/1000000)."mb)";
		    $result['data'] = $this->gthis->dataBuilder(array(),'maxFileSizeReached');
			return $result;
		}

		if(isset($dest_file_name)){
			$file_name = $dest_file_name;
		} else {
			$temp_file_name = $file['tmp_name'];
			$temp_file_name = explode('/',$temp_file_name);
			$temp_file_name = $temp_file_name[sizeOf($temp_file_name)-1];
			$file_name =  $temp_file_name.'_'.time().'.'.$ext;	
		}
		
		//$uploaddir = "./upload/postImages/$today/";
		$uploaddir = $destination_dir;
		if(!file_exists($uploaddir)){
			mkdir($uploaddir, 0, true);
			chmod($uploaddir, 0755);
		}
		$path = $uploaddir.$file_name;
		if(move_uploaded_file($file['tmp_name'], $path)){
			$result['code'] = '0';
			$result['msg'] = 'Success';
			$result['abs_path'] = $this->gthis->baseURL.'/'.$path;
			$result['path'] = $path;
			$result['html_relative_path'] = str_replace(ROOT,'.',$path);
			return $result;
		} else {
			$result['code'] = '-1';
			$result['msg'] = 'Failed to copy file';
			$result['data'] = $this->gthis->dataBuilder(array(),'failToCopyFile');
			return $result;
		}
	}

}