<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;
		
		require_once ROOT. '/include/apiDataBuilder.class.php';
		
		switch ($this->gthis->thirdmode) {
			case 'getDeliGroupList':
				$this->getDeliGroupList();
				break;
			case 'getDeliGroup':
				$this->getDeliGroup();
				break;
			case 'editDeliGroup':
				$this->editDeliGroup();
				break;
			case 'delDeliGroup':
				$this->delDeliGroup();
				break;
			case 'getCardList':
				$this->getCardList();
				break;
			case 'editCutoffTpl':
				$this->editCutoffTpl();
				break;
			case 'productList':
				$this->productList();
				break;
			case 'productDtl':
				$this->productDtl();
				break;
			case 'itemDtl':
				$this->itemDtl();
				break;
			case 'uploadImg':
				$this->uploadImg();
				break;
			case 'sortSubcateProduct':
				$this->sortSubcateProduct();
				break;
			case 'editCate':
				$this->editCate();
				break;
			case 'editProduct':
				$this->editProduct();
				break;
			case 'getProductDtl':
				$this->getProductDtl();
				break;
			case 'editAddon':
				$this->editAddon();
				break;
			case 'editAddonGroup':
				$this->editAddonGroup();
				break;
			case 'delAddon':
				$this->delAddon();
				break;
			case 'delAddonGroup':
				$this->delAddonGroup();
				break;
			case 'getCateList':
				$this->getCateList();
				break;
			case 'getCateGreeting':
				$this->getCateGreeting();
				break;
			case 'getAddonList':
				$this->getAddonList();
				break;
			case 'getAddonDtl':
				$this->getAddonDtl();
				break;
			case 'getAddonGroupList':
				$this->getAddonGroupList();
				break;
			case 'getAddonGroupDtl':
				$this->getAddonGroupDtl();
				break;
			case 'uploadProductImg':
				$this->uploadProductImg();
				break;
			case 'sortProductImg':
				$this->sortProductImg();
				break;
			case 'delProductImg':
				$this->delProductImg();
				break;
			case 'duplicateProduct':
				$this->duplicateProduct();
				break;
			case 'editProductTpl':
				$this->editProductTpl();
				break;
			case 'getProductItemContentLog':
				$this->getProductItemContentLog();
				break;
			case 'editProductItemGroup':
				$this->editProductItemGroup();
				break;
			case 'getProductItemList':
				$this->getProductItemList();
				break;
			case 'batchEditProduct':
				$this->batchEditProduct();
				break;

			case 'getCardGroupList':
				$this->getCardGroupList();
				break;
			case 'getCardGroupDtl':
				$this->getCardGroupDtl();
				break;
			case 'editCardGroup':
				$this->editCardGroup();
				break;
			case 'delCardGroup':
				$this->delCardGroup();
				break;

			case 'getGreetingGroupList':
				$this->getGreetingGroupList();
				break;
			case 'getGreetingGroupDtl':
				$this->getGreetingGroupDtl();
				break;
			case 'editGreetingGroup':
				$this->editGreetingGroup();
				break;
			case 'delGreetingGroup':
				$this->delGreetingGroup();
				break;

			case 'getGreetingCateList':
				$this->getGreetingCateList();
				break;

			case 'sortCateSubcate':
				$this->sortCateSubcate();
				break;

			default:
				new apiDataBuilder(-84);
				break;
		}

		$result_array['lang'] = $this->lang;
		@$this->gthis->assign('result_array',$result_array);
	}


	function getDeliGroupList(){
		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);

		$res['delivery_group_list'] = $deliveryCls->getDeliveryGroupList();

		new apiDataBuilder(0, $res);
	}

	function delDeliGroup(){
		if (intval($_SESSION['right']['product']) < 50){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "deli_group_id", "type" => "int"],
			["key" => "activate", "request" => "self", "defaultValue" => 'N']
		]);

		$deliveryCls->editDeliveryGroup($data);

		new apiDataBuilder(0);
	}

	function editDeliGroup(){
		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);
		
		$data = $this->gthis->paramBuilder([
			["key" => "deli_group_id"],
			["key" => "defaultJson", "type" => "json"],
			["key" => "ruleJson", "type" => "json"],
			["key" => "name_tc"],
			["key" => "name_en"],
			["key" => "product_id_arr", "type" => "intArrV1"]
		]);

		$deliveryCls->editDeliveryGroup($data);

		$delivery_group = $deliveryCls->getDeliveryGroup($data['deli_group_id']);
		$existing_list = $delivery_group['product_list'];

		foreach ($existing_list as $product){
			$productCls->editProduct([
				"product_id" => $product['product_id'],
				"deli_group_id" => "NULL"
			]);
		}

		foreach ($data['product_id_arr'] as $product_id){
			$productCls->editProduct([
				"product_id" => $product_id,
				"deli_group_id" => $data['deli_group_id']
			]);
		}

		new apiDataBuilder(0, $data);
	}

	function getDeliGroup(){
		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);

		$deli_group_id = $this->gthis->param(["key" => "deli_group_id"]);

		$res['delivery_group'] = $deliveryCls->getDeliveryGroup($deli_group_id);
		$res['delivery_default_timeslot_list'] = $deliveryCls->getTimeSlotList(1, ["default" => "Y", "deli_group_id" => $deli_group_id]);

		if ($deli_group_id){
			$delivery_cust_region_list = $deliveryCls->getDeliGroupRegionList($deli_group_id);		
			$res['delivery_cust_region_list'] = $delivery_cust_region_list;
		}

		new apiDataBuilder(0, $res);
	}

	function sortCateSubcate(){
		if (intval($_SESSION['right']['product']) < 30){
			new apiDataBuilder(-62);
		}

		$data = $this->gthis->paramBuilder([
			["key" => "subcate_arr", "type" => "intArr"], 
			["key" => "parent_cate_id"],
		]);

		require_once ROOT.'/include/class/cate.class.php';
		$cateCls = new cate($this->DB, $this->lang, $this->isAdmin);

		$cateCls->sortCateSubcate($data['parent_cate_id'], $data['subcate_arr']);

		new apiDataBuilder(0);
	}

	function batchEditProduct(){
		if (intval($_SESSION['right']['product']) < 30){
			new apiDataBuilder(-62);
		}

		$srcData = $this->gthis->paramBuilder([
			["key" => "product_id_arr", "type" => "intArrV1"], 
			["key" => "action"],
			["key" => "shop_id", "request" => "session"],
			["key" => "param"],
		]);

		switch ($srcData['action']){
			case 'product_tag':
				$data = [
					"product_tag_tc" => $srcData['param'],
					"product_tag_en" => $this->gthis->param(["key" => 'param_2']),
				];
				break;
			case 'bottom_product_tag':
				$data = [
					"bottom_product_tag_tc" => $srcData['param'],
					"bottom_product_tag_en" => $this->gthis->param(["key" => 'param_2']),
				];
				break;
			case 'bottom_product_tag_2':
				$data = [
					"bottom_product_tag_2_tc" => $srcData['param'],
					"bottom_product_tag_2_en" => $this->gthis->param(["key" => 'param_2']),
				];
				break;
			case 'is_new':
				$data = [
					"is_new" => 'Y'
				];
				break;
			case 'is_not_new':
				$data = [
					"is_new" => 'N'
				];
				break;
			case 'delivery_date':
				$data = [
					"delivery_start_date" => $srcData['param'],
					"delivery_end_date" => $this->gthis->param(["key" => 'param_2'])
				];
				break;
			case 'force_checkout':
				$data = [
					"is_force_checkout" => $srcData['param'],
					"force_checkout_start_date" => $this->gthis->param(["key" => 'param_2']),
					"force_checkout_end_date" => $this->gthis->param(["key" => 'param_3'])
				];
				break;
			case 'publish_date':
				$data = [
					"publish_date" => $srcData['param']
				];
				break;
			case 'product_tpl_id':
				$data = [
					"product_tpl_id" => $srcData['param']
				];
				break;
			case 'del':
				$data = [
					"activate" => "N"
				];
				break;
			case '25_schdeuled_sale':
				$data = [
					"sales_status" => $srcData['action'],
					"publish_date" => $srcData['param']
				];
				break;
			case '10_to_be_confirm':
			case '20_not_for_sale':
			case '30_on_sale':
			case '40_out_of_stock':
				$data = [
					"sales_status" => $srcData['action']
				];
				break;
			case 'show_in_web_N':
				$data = [
					"show_in_web" => 'N'
				];
				break;
			case 'show_in_web_Y':
				$data = [
					"show_in_web" => 'Y'
				];
				break;
			case 'addon_group':
				$data = [
					"addon_group_id" => $srcData['param']
				];
				break;
			case 'card_group':
				$data = [
					"card_group_id" => $srcData['param']
				];
				break;
			case 'greeting_group':
				$data = [
					"greeting_group_id" => $srcData['param']
				];
				break;
			case 'add_cate':
				require_once ROOT.'/include/class/product.class.php';
				$productCls = new product($this->DB, $this->lang, $this->isAdmin);

				$productCls->addProductCate($srcData['product_id_arr'], $srcData['param'], $srcData["shop_id"]);

				new apiDataBuilder(0, null);
				break;
		}

		$data['shop_id'] = $srcData["shop_id"];

		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		foreach ($srcData['product_id_arr'] as $product_id){
			$data["product_id"] = $product_id;
			$productCls->editProduct($data);
		}

		new apiDataBuilder(0, $data);
	}

	function getProductItemList(){
		if (intval($_SESSION['right']['product']) < 30){
			new apiDataBuilder(-62);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "skey"]
		]);

		$item_grp_list = $itemCls->getItemGroupList($data);
		$item_list = $itemCls->getItemList($data);
		foreach ($item_list as $k => $item){
			$itemData = [
				"grp_id" => "0",
				"adm_name" => $item['adm_name'],
				"alias" => $item['alias'],
				"inhouse_name_tc" => $item['inhouse_name_tc'],
				"inhouse_name_en" => $item['inhouse_name_en'],
				"gift_name_tc" => $item['gift_name_tc'],
				"gift_name_en" => $item['gift_name_en'],
				"zuri_name_tc" => $item['zuri_name_tc'],
				"zuri_name_en" => $item['zuri_name_en'],
				"ltp_name_tc" => $item['ltp_name_tc'],
				"ltp_name_en" => $item['ltp_name_en'],
				"is_tpl" => "N",
				"is_product_page_show_qty" => $item['is_product_page_show_qty'],
				"qty" => 1,
				"ava_qty" => $item['ava_qty'],
				"item_list" => [
					[
						"item_id" => $item['item_id'],
						"qty" => 1,
						"is_auto_replace" => "N",
						"is_client_optional" => "N",
						"item" => $item
					]
				]
			];
			$item_list[$k] = $itemData;
		}

		$res['item_list'] = array_merge($item_list, $item_grp_list);
		// $res['item_group_list'] = $item_grp_list;


		new apiDataBuilder(0, $res);
	}

	function uploadProductImg(){
		if (intval($_SESSION['right']['product']) < 30){
			new apiDataBuilder(-62);
		}

		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		
		$res['img_path_arr'] = $productCls->uploadProductImgs($_FILES['input_product_img']);
		

		new apiDataBuilder(0, $res);
	}

	function getProductItemContentLog(){
		if (intval($_SESSION['right']['product']) < 30){
			new apiDataBuilder(-62);
		}

		require_once ROOT.'/include/class/audit.class.php';
		$auditCls = new audit($this->DB, $this->lang, $this->isAdmin);
		
		$product_id = $this->DB->parseInt($this->gthis->parm_sql('request', 'product_id', 'Y'));
		$res = $auditCls->getProductItemContentLog($product_id);

		new apiDataBuilder(0, $res); 
	}

	function editProductItemGroup(){
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "product_item_id"],
			["key" => "product_id", "type" => "int"],
			["key" => "grp_id", "type" => "int"],
			["key" => "is_force_checkout", "type" => "NY"],
		]);

		$res = $productCls->editProductItemGroup($data);

		new apiDataBuilder(0, $res); 
	}

	function getCardList(){
		if (intval($_SESSION['right']['product']) < 50){
			new apiDataBuilder(-62);
		}

		require_once ROOT.'/include/class/card.class.php';
		$cardCls = new card($this->DB, $this->lang, $this->isAdmin);

		$res = $cardCls->getCardList();

		new apiDataBuilder(0, $res); 
	}

	function editCutoffTpl(){
		if (intval($_SESSION['right']['product']) < 50){
			new apiDataBuilder(-62);
		}

		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$cutoff_tpl_id = $this->gthis->parm_sql('request', 'cutoff_tpl_id');

		$data = $this->gthis->paramBuilder([
			["key" => "cutoff_tpl_id"],
			["key" => "ava_b4_day"],
			["key" => "ava_b4_cutoff"],
			["key" => "activate", "type" => "NY"],
			["key" => "adm_name"],
			["key" => "shop_id"],
			["key" => "product_id_arr", "type" => "intArrV1"],
		]);

		$res = $productCls->editCutoffTpl($cutoff_tpl_id, $data);

		foreach($data['product_id_arr'] as $product_id){
			$productCls->editProduct([
				"product_id" => $product_id, 
				"shop_id" => $data['shop_id'], 
				"cutoff_tpl_id" => $res['cutoff_tpl_id']
			]);
		}


		new apiDataBuilder(0, $res);
	}

	function editProductTpl(){
		if (intval($_SESSION['right']['product']) < 50){
			new apiDataBuilder(-62);
		}

		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$tpl_id = $this->gthis->parm_sql('request', 'tpl_id');

		$data = $this->gthis->paramBuilder([
			["key" => "deli_msg_tc"],
			["key" => "deli_msg_en"],
			["key" => "info_msg_tc"],
			["key" => "info_msg_en"],
			["key" => "markup", "type" => "int"],
			["key" => "activate", "type" => "NY"],
			["key" => "adm_name"],
			["key" => "shop_id", "type" => "int"],
		]);


		$res = $productCls->editProductTpl($tpl_id, $data);

		new apiDataBuilder(0, $res);
	}
	
	function duplicateProduct(){
		if (intval($_SESSION['right']['product']) < 50){
			new apiDataBuilder(-62);
		}

		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);
		
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$product_id = $this->DB->parseInt($this->gthis->parm_sql('request', 'product_id', 'Y'));
		$product = $productCls->duplicateProduct($product_id, $shop_id);

		new apiDataBuilder(0);
	}

	function delProductImg(){
		if (intval($_SESSION['right']['product']) < 50){
			new apiDataBuilder(-62);
		}

		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$map_id = $this->DB->parseInt($this->gthis->parm_sql('request', 'map_id', 'Y'));

		$res = $productCls->delProductImg($map_id);

		new apiDataBuilder(0, $res);
	}

	function sortProductImg(){
		if (intval($_SESSION['right']['product']) < 50){
			new apiDataBuilder(-62);
		}

		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$map_id_arr = $this->DB->parseIntArr($this->gthis->parm_sql('request', 'map_id_arr', 'Y'));
		// $product_id = $this->gthis->parm_sql('request', 'product_id', 'Y');

		$res = $productCls->sortProductImg($map_id_arr);

		new apiDataBuilder(0, $res);
	}

	function productDtl(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/shop.class.php';
		$shopCls = new shop($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "shop_id", "request" => "session", "type" => "int"],
			["key" => "product_id", "type" => "int"]
		]);
		$shop_id = $data['shop_id'];


		$res['productDtl'] = $productCls->getProductDtl($shop_id, $data['product_id']);
		$res['productTplList'] = $productCls->getProductTplList($shop_id);
		$res['cutoffTplList'] = $productCls->getCutoffTplList($shop_id);
		$res['custDeliList'] = $deliveryCls->getCustDeliGrpList($shop_id);
		$res['shop'] = $shopCls->getShopDtl($shop_id);

		new apiDataBuilder(0, $res);
	}

	function productList(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/product.class.php';
		$product = new product($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "shop_id", "request"=>"session", "compulsory" => true],
			["key" => "addon_id", "type" => "int"],
			["key" => "page", "defaultValue" => "1"],
			["key" => "limit", "defaultValue" => "50"],
			["key" => "sort", "defaultValue" => "product_code"],
			["key" => "order", "defaultValue" => "DESC"],
		]);

		$filter = $this->gthis->paramBuilder([
			["key" => "skey"],
			["key" => "product_id", "type" => "int"],
			["key" => "grp_id", "type" => "int"],
			["key" => "subcate_id", "type" => "int"],
			["key" => "cate_id", "type" => "int"],
			["key" => "addon_product_addon_id", "type" => "int"],
			["key" => "greeting_group_id", "type" => "int"],
			["key" => "addon_group_id", "type" => "int"],
			["key" => "cate_id_arr", "type" => "intArr"],
		]);

		$limiter = [
			"page" => $data['page'],
			"limit" => $data['limit']
		];
		
		$productList = $product->getProductList($data['shop_id'], $filter, $limiter);
		$sort = array_column($productList, $data['sort']);
		array_multisort($sort, $data['order'] == "desc" ? SORT_DESC : SORT_ASC, $productList);
		
		$res['productList'] = $productList;

		
		new apiDataBuilder(0, $res);
		
	}

	function uploadImg(){
		$tmp_img = $this->gthis->parm_in('file', 'file');
		$res['img_path'] = $this->DB->uploadImg($tmp_img, "product", 0);
		new apiDataBuilder(0, $res);
	}

	function sortSubcateProduct(){
		if (intval($_SESSION['right']['product']) < 50){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/cate.class.php';
		$cate = new cate($this->DB, $this->lang, $this->isAdmin);

		$product_id_arr = $this->DB->parseIntArr($this->gthis->parm_sql('request', 'product_id_arr', 'Y'));
		$cate_id = $this->gthis->parm_sql('request', 'cate_id', 'Y');

		$res = $cate->sortSubcateProduct($cate_id, $product_id_arr);

		new apiDataBuilder(0, $res);
	}

	function getAddonList(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/product.class.php';
		$product = new product($this->DB, $this->lang, $this->isAdmin);

		// $shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$filter = $this->gthis->paramBuilder([
			["key" => "skey"]
		]);

		$shop_id = $this->gthis->param(["key" => "shop_id", "request" => "session"]);

		$res['addon_list'] = $product->getAddonList($shop_id, $filter);

		new apiDataBuilder(0, $res);
	}

	function getAddonDtl(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/product.class.php';
		$product = new product($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "addon_id", "type" => "int"],
			["key" => "shop_id", "request" => "session"]
		]);

		// $shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$res['addon'] = $product->getAddonDtl($data['shop_id'], $data['addon_id']);

		new apiDataBuilder(0, $res);
	}

	function getAddonGroupList(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/product.class.php';
		$product = new product($this->DB, $this->lang, $this->isAdmin);

		$filter = $this->gthis->paramBuilder([
			["key" => "skey"]
		]);

		$shop_id = $this->gthis->param(["key" => "shop_id", "request" => "session"]);

		$res = $product->getAddonGroupList($shop_id, $filter);

		new apiDataBuilder(0, $res);
	}

	function getAddonGroupDtl(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/product.class.php';
		$product = new product($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "addon_group_id", "type" => "int"],
			["key" => "shop_id", "request" => "session"]
		]);

		// $shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$res['addon'] = $product->getAddonGroupDtl($data['shop_id'], $data['addon_group_id']);

		new apiDataBuilder(0, $res);
	}

	function getCateGreeting(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/greeting.class.php';
		$greeting = new greeting($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$cate_id = $this->gthis->parm_sql('request', 'cate_id');

		$filter = [
			"cate_id" => $cate_id
		];

		$res = $greeting->getGreetingCateList($shop_id, $filter);

		new apiDataBuilder(0, $res);
	}

	function getCateList(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/cate.class.php';
		$cate = new cate($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$res = $cate->getCateList($shop_id);

		new apiDataBuilder(0, $res);
	}

	function delAddon(){
		if (intval($_SESSION['right']['product']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "addon_id", "type" => "int"],
			["key" => "activate", "request" => "self", "defaultValue" => 'N']
		]);

		$res = $productCls->editAddon($data['addon_id'], $data);

		new apiDataBuilder(0, $res);
	}

	function delAddonGroup(){
		if (intval($_SESSION['right']['product']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "addon_group_id", "type" => "int"],
			["key" => "activate", "request" => "self", "defaultValue" => 'N']
		]);

		$res = $productCls->editAddonGroup($data);

		new apiDataBuilder(0, $res);
	}

	function editAddonGroup(){
		if (intval($_SESSION['right']['product']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "addon_group_id", "type" => "int"],
			["key" => "alias"],
			["key" => "inhouse_name_tc"],
			["key" => "inhouse_name_en"],
			["key" => "gift_name_tc"],
			["key" => "gift_name_en"],
			["key" => "zuri_name_tc"],
			["key" => "zuri_name_en"],
			["key" => "ltp_name_tc"],
			["key" => "ltp_name_en"],
			["key" => "addon_id_arr", "type" => "intArrV1"]
		]);

		$res = $productCls->editAddonGroup($data);

		new apiDataBuilder(0, $res);
	}

	function editAddon(){
		// $this->gthis->showError();
		if (intval($_SESSION['right']['product']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "addon_id", "type" => "int"],
			["key" => "alias"],
			["key" => "inhouse_name_tc"],
			["key" => "inhouse_name_en"],
			["key" => "gift_name_tc"],
			["key" => "gift_name_en"],
			["key" => "zuri_name_tc"],
			["key" => "zuri_name_en"],
			["key" => "ltp_name_tc"],
			["key" => "ltp_name_en"],
			["key" => "product_id_arr", "type" => "intArrV1"]
		]);

		$res = $productCls->editAddon($data['addon_id'], $data);

		new apiDataBuilder(0, $res);
	}


	function editAddon_bak(){
		if (intval($_SESSION['right']['product']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/product.class.php';
		$product = new product($this->DB, $this->lang, $this->isAdmin);

		$addon_id = $this->gthis->parm_sql('request', 'addon_id');
		$data = [];
		$fieldArr = ["shop_id", "addon_id", "name_tc", "name_en", "product_id_arr", "activate"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
				case 'shop_id':
					if (!$$field){
						new apiDataBuilder(-99, null, "$field cannot be null");
					}
					break;
				case 'addon_id':
					$$field = $this->DB->parseInt($$field);
					break;
				case 'product_id_arr':
					$$field = $this->DB->parseIntArr($$field);
					break;
			}
			$$field? $data[$field] = $$field: null;
		}

		$res = $product->editAddon($addon_id, $data);

		new apiDataBuilder(0, $res);
	}

	function editProduct(){
		if (intval($_SESSION['right']['product']) < 50){
			new apiDataBuilder(-62);
		}
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "product_id", "type" => "int"],
			["key" => "addon_group_id", "type" => "int"],
			["key" => "card_group_id", "type" => "int"],
			["key" => "greeting_group_id", "type" => "int"],
			["key" => "product_img_path_arr", "type" => "arr"],
			["key" => "show_in_web", "type" => "NY"],
			["key" => "card_id_arr", "type" => "intArr"],
			["key" => "deli_start_date", "type" => "date"],
			["key" => "cutoff_tpl_id", "type" => "int"],
			["key" => "product_tpl_id", "type" => "int"],
			["key" => "ava_b4_day"],
			["key" => "ava_b4_cutoff", "type" => "time"],
			["key" => "addon_id_arr", "type" => "intArrV1"],
			["key" => "item_json", "type" => "json"],
			["key" => "src_product_id_arr", "type" => "intArrV1"],
			["key" => "shop_id", "type" => "int"],
			["key" => "product_code"],
			["key" => "product_name_tc"],
			["key" => "product_name_en"],
			["key" => "price", "type" => "int"],
			["key" => "sp_price", "type" => "int"],
			// ["key" => "onsale", "type" => "NY"],
			["key" => "sales_status", "type" => "salesStatus"],
			["key" => "display_in_cate", "type" => "NY"],
			["key" => "cate_id_arr", "type" => "intArrV1"],
			["key" => "greeting_id_arr", "type" => "intArrV1"],
			["key" => "deli_id", "type" => "int"],
			["key" => "cust_deli_act", "type" => "NY"],
			["key" => "cust_deli_tc"],
			["key" => "cust_deli_en"],
			["key" => "cust_info_act", "type" => "NY"],
			["key" => "cust_info_tc"],
			["key" => "cust_info_en"],
			["key" => "cust_addinfo_act", "type" => "NY"],
			["key" => "cust_addinfo_tc"],
			["key" => "cust_addinfo_en"],
			["key" => "is_new", "type" => "NY"],
			["key" => "product_tag_tc"],
			["key" => "product_tag_en"],
			["key" => "bottom_product_tag_tc"],
			["key" => "bottom_product_tag_en"],
			["key" => "bottom_product_tag_2_tc"],
			["key" => "bottom_product_tag_2_en"],
			["key" => "activate", "type" => "NY"],
			["key" => "deli_group_id", "type" => "int"],
			["key" => "is_force_checkout", "type" => "NY"],
			["key" => "publish_date", "type" => "date"],
			["key" => "force_checkout_start_date", "type" => "date"],
			["key" => "force_checkout_end_date", "type" => "date"],
			["key" => "delivery_start_date", "type" => "date"],
			["key" => "delivery_end_date", "type" => "date"],
		]);
	
		if ($data['item_json']){
			require_once ROOT.'/include/class/item.class.php';
			$itemCls = new item($this->DB, $this->lang, $this->isAdmin);
			foreach($data['item_json'] as $k => $item_grp){
				if ($item_grp['grp_id'] == 0){
					// create new item_grp
					
					$itemGrpData = [
						"grp_id" => $item_grp['grp_id'],
						"adm_name" => $item_grp['inhouse_name_tc'],
						"inhouse_name_tc" => $item_grp['inhouse_name_tc'],
						"inhouse_name_en" => $item_grp['inhouse_name_en'],
						"gift_name_tc" => $item_grp['gift_name_tc'],
						"gift_name_en" => $item_grp['gift_name_en'],
						"zuri_name_tc" => $item_grp['zuri_name_tc'],
						"zuri_name_en" => $item_grp['zuri_name_en'],
						"ltp_name_tc" => $item_grp['ltp_name_tc'],
						"ltp_name_en" => $item_grp['ltp_name_en'],
						"alias" => $item_grp['alias'],
						"item_json" => $item_grp['item_list'],
						"is_tpl" => 'N'
					];
					
					$itemGroup = $itemCls->editItemGroup($itemGrpData);

					$data['item_json'][$k]['grp_id'] = $itemGroup['grp_id'];
				} else {
					// update the item_grp qty only

					$itemGrpData = [
						"grp_id" => $item_grp['grp_id'],
						"item_json" => $item_grp['item_list']
					];
					
					$itemGroup = $itemCls->editItemGroup($itemGrpData);
				}
			}
		}

		$res['product'] = $productCls->editProduct($data);

		new apiDataBuilder(0, $res);
	}

	function editCate(){
		if (intval($_SESSION['right']['product']) < 50){
			new apiDataBuilder(-62);
		}
		require_once ROOT.'/include/class/cate.class.php';
		$cate = new cate($this->DB, $this->lang, $this->isAdmin);
		
		$cate_id = $this->gthis->parm_sql('request', 'cate_id');
		$data = [];
		$fieldArr = ["description_tc", "description_en", "background_color", "shop_id", "cate_id", "name_tc", "name_en", "parent_cate_id", "greeting_cate_id_arr", "activate", "show_in_web"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
				
				case 'cate_id':
				case 'parent_cate_id':
					$$field = $this->DB->parseInt($$field);
					break;
			}
			$$field? $data[$field] = $$field: null;
		}

		$banner = $this->gthis->parm_in('file', 'cate_banner');
		if ($banner && $banner['name'])
			$data['cate_banner'] = $banner;

		$res = $cate->editCate($cate_id, $data);

		new apiDataBuilder(0, $res);
	}

	function getGreetingGroupList(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/greeting.class.php';
		$greetingCls = new greeting($this->DB, $this->lang, $this->isAdmin);

		$filter = $this->gthis->paramBuilder([
			["key" => "skey"]
		]);

		$res['greeting_group_list'] = $greetingCls->getGreetingGroupList($filter);

		new apiDataBuilder(0, $res);
	}

	function getGreetingGroupDtl(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/greeting.class.php';
		$greetingCls = new greeting($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "greeting_group_id", "type" => "int"],
			["key" => "shop_id", "request" => "session"]
		]);

		$greeting_group = $greetingCls->getGreetingGroupDtl($data['greeting_group_id']);
		if ($data['greeting_group_id']){
			$greeting_group['product_list'] =  $productCls->getProductList($data['shop_id'], ["greeting_group_id" => $data['greeting_group_id']], null, null, false);
		}

		$res['greeting_group'] = $greeting_group;

		new apiDataBuilder(0, $res);
	}

	function editGreetingGroup(){
		if (intval($_SESSION['right']['product']) < 50){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/greeting.class.php';
		$greetingCls = new greeting($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "greeting_group_id", "type" => "int"],
			["key" => "name_tc"],
			["key" => "name_en"],
			["key" => "cate_id_arr", "type" => "intArrV1"],
			["key" => "product_id_arr", "type" => "intArrV1"]
		]);

		$greetingGroup = $greetingCls->editGreetingGroup($data);
		
		foreach ($data['product_id_arr'] as $product_id){
			$productCls->editProduct([
				"product_id" => $product_id,
				"greeting_group_id" => $greetingGroup['greeting_group_id']
			]);
		}

		$res['greeting_group'] = $greeting_group;
		
		new apiDataBuilder(0, $res);
	}

	function delGreetingGroup(){
		if (intval($_SESSION['right']['product']) < 50){
			return array("right"=>"Failure");
		}
		
		require_once ROOT.'/include/class/greeting.class.php';
		$greetingCls = new greeting($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "greeting_group_id", "type" => "int"],
			["key" => "activate", "request" => "self", "defaultValue" => 'N']
		]);

		$res = $greetingCls->editGreetingGroup($data);

		new apiDataBuilder(0, $res);
	}

	function getCardGroupList(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/card.class.php';
		$cardCls = new card($this->DB, $this->lang, $this->isAdmin);

		$filter = $this->gthis->paramBuilder([
			["key" => "skey"]
		]);

		$shop_id = $this->gthis->param(["key" => "shop_id", "request" => "session"]);

		$res['card_group_list'] = $cardCls->getCardGroupList($filter);

		new apiDataBuilder(0, $res);
	}

	function getCardGroupDtl(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/card.class.php';
		$cardCls = new card($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "card_group_id", "type" => "int"],
			["key" => "shop_id", "request" => "session"]
		]);

		$card_group = $cardCls->getCardGroupDtl($data['card_group_id']);
		if ($data['card_group_id']){
			$card_group['product_list'] =  $productCls->getProductList($data['shop_id'], ["card_group_id" => $data['card_group_id']], null, null, false);
		}

		$res['card_group'] = $card_group;
		
		new apiDataBuilder(0, $res);
	}

	function editCardGroup(){
		if (intval($_SESSION['right']['product']) < 50){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/card.class.php';
		$cardCls = new card($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "card_group_id", "type" => "int"],
			["key" => "name_tc"],
			["key" => "name_en"],
			["key" => "remarks_tc"],
			["key" => "remarks_en"],
			["key" => "card_id_arr", "type" => "intArrV1"],
			["key" => "product_id_arr", "type" => "intArrV1"]
		]);

		$card_group = $cardCls->editCardGroup($data);

		foreach ($data['product_id_arr'] as $product_id){
			$productCls->editProduct([
				"product_id" => $product_id,
				"card_group_id" => $card_group['card_group_id']
			]);
		}

		$res['card_group'] = $card_group;

		new apiDataBuilder(0, $res);
	}

	function delCardGroup(){
		if (intval($_SESSION['right']['product']) < 50){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/card.class.php';
		$cardCls = new card($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "card_group_id", "type" => "int"],
			["key" => "activate", "request" => "self", "defaultValue" => 'N']
		]);

		$res['card_group'] = $cardCls->editCardGroup($data);

		new apiDataBuilder(0, $res);
	}

	function getGreetingCateList(){
		if (intval($_SESSION['right']['product']) < 50){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/greeting.class.php';
		$greetingCls = new greeting($this->DB, $this->lang, $this->isAdmin);

		$res['greeting_cate_list'] = $greetingCls->getGreetingCateList();

		new apiDataBuilder(0, $res);
	}

}
?>