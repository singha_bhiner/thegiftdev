<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;

		switch ($this->gthis->submode) {
            case 'memberList':
                $result_array = $this->memberList();
                break;
            case 'memberDtl':
                $result_array = $this->memberDtl();
                break;
			case 'membershipList':
				$result_array = $this->membershipList();
				break;
			case 'membershipDtl':
				$result_array = $this->membershipDtl();
				break;
			default:
				break;
		}

		$this->gthis->assign('result_array',$result_array);
	}

	function membershipList(){
        if (intval($_SESSION['right']['member']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/member.class.php';
		$member = new member($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sort = $this->gthis->parm_sql('request', 'sort', 'N');
		$order = $this->gthis->parm_sql('request', 'order', 'N');
        
		$filter = [
			"skey" => $skey ? $skey : null
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "membership_id",
			"order" => $order ? $order : "ASC"
		];
		
		$res['skey'] = $filter['skey'];
		$res['page'] = $limiter['page'];
		$res['limit'] = $limiter['limit'];
		$res['membershipList'] = $member->getMembershipList($shop_id, $filter, $limiter, $sorting);

		
		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($member->getMembershipList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "membershipList",
			"url" => "admin/member/membershipList",
			"page_title" => $this->gthis->RMBC->getReturnString('membershipList')
		]);

		return $res;
	}

	function membershipDtl(){
        if (intval($_SESSION['right']['member']) < 30){
			return array("right"=>"Failure");
		}
		
		require_once ROOT.'/include/class/member.class.php';
		$member = new member($this->DB, $this->lang, $this->isAdmin);

        $membership_id = $this->DB->parseInt($this->gthis->parm_sql('request', 'membership_id', 'N'));
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

        $res['membershipDtl'] = $member->getMembershipDtl($membership_id);
		$res['membershipList'] = $member->getMembershipList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "membershipList",
			"url" => "admin/member/membershipList",
			"page_title" => $this->gthis->RMBC->getReturnString('membershipList')
		],[
			"code" => "membershipDtl",
			"url" => "admin/member/membershipDtl?membership_id=" . ($membership_id ? $membership_id : "0"),
			"page_title" => $membership_id ? $res['membershipDtl']['name_tc'] : $this->gthis->RMBC->getReturnString('addMembership')
		]);
		return $res;
	}

    function memberList(){
        if (intval($_SESSION['right']['member']) < 30){
			return array("right"=>"Failure");
		}
		
        require_once ROOT.'/include/class/member.class.php';
		$member = new member($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/user.class.php';
		$userCls = new user($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
        $business = $this->DB->parseNY($this->gthis->parm_sql('request', 'business', 'N'));
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sort = $this->gthis->parm_sql('request', 'sort', 'N');
		$order = $this->gthis->parm_sql('request', 'order', 'N');
		$sales = $this->gthis->parm_sql('request', 'admin_id'); // sales admin_id

		if ($sales){
			$tArray = explode(",", $sales);
			$sales = "'" . implode("','", $tArray) . "'";
		}

		$filter = [
			"skey" => $skey ? $skey : null,
            "business" => $business ? $business : null,
			"sales" => $sales ? $sales : null,
			"member_no" => "Y"
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "member_id",
			"order" => $order ? $order : "ASC"
		];
		
		$res['skey'] = $filter['skey'];
		$res['page'] = $limiter['page'];
		$res['limit'] = $limiter['limit'];
		$res['memberList'] = $member->getMemberList($shop_id, $filter, $limiter, $sorting);
		$res['membershipList'] = $member->getMembershipList($shop_id);
		$res['userList'] = $userCls->getAdminList();
		
		$res['page_config'] = [
			"skey" => $skey,
			"business" => $business,
			"ttlCount" => sizeof($member->getMemberList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "memberList",
			"url" => "admin/member/memberList" . ($business ? "?business=$business" : null),
			"page_title" => $this->gthis->RMBC->getReturnString('memberList')
		]);
		return $res;
    }

    function memberDtl(){
        if (intval($_SESSION['right']['member']) < 30){
			return array("right"=>"Failure");
		}

        require_once ROOT.'/include/class/member.class.php';
		$member = new member($this->DB, $this->lang, $this->isAdmin);

        $member_id = $this->gthis->parm_sql('request', 'member_id', 'N');
        $shop_id = $this->gthis->parm_sql('session', 'shop_id', 'N');

        $res['memberDtl'] = $member->getMemberDtl($member_id);
		$res['membershipList'] = $member->getMembershipList($shop_id);

		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);
		$res['productList'] = $productCls->getProductList($shop_id);

		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);
		$res['regionList'] = $deliveryCls->getRegionList($shop_id);
		$res['timeslotList'] = $deliveryCls->getTimeslotList($shop_id);

		require_once ROOT.'/include/class/voucher.class.php';
		$voucherCls = new voucher($this->DB, $this->lang, $this->isAdmin);
		$res['voucherList'] = $voucherCls->getVoucherList($shop_id);

		require_once ROOT.'/include/class/user.class.php';
        $userCls = new user($this->DB, $this->lang, $this->isAdmin);
		$res['salesList'] = $userCls->getAdminList();

		array_push($this->gthis->breadcrumb, [
			"code" => "memberList",
			"url" => "admin/member/memberList?business=N",
			"page_title" => $this->gthis->RMBC->getReturnString('memberList')
		],[
			"code" => "memberDtl",
			"url" => "admin/member/memberDtl?member_id=" . ($member_id ? $member_id : "0"),
			"page_title" => $member_id ? $res['memberDtl']['member_no'] . " - " . $res['memberDtl']['name'] : $this->gthis->RMBC->getReturnString('addMember')
		]);
        return $res;
    }
}
?>    