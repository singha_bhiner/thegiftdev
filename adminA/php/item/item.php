<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;

		switch ($this->gthis->submode) {
			case 'itemReplaceList':
				$result_array = $this->itemReplaceList();
				break;
			case 'itemDtlContent': 
				$this->gthis->assign("page_only" , "Y");
				$result_array = $this->itemDtlContent();
				break;
			case 'itemDtl': 
				$result_array = $this->itemDtl();
				break;
			case 'createItem': 
				$result_array = $this->createItem();
				break;
			case 'itemList':
				$result_array = $this->itemList();
				break;
			case 'itemListWithSupplier':
				$result_array = $this->itemList();
				break;
			case 'itemGrpDtl': 
				$result_array = $this->itemGrpDtl();
				break;
			case 'itemGrpList':
				//$result_array = $this->itemGrpList();
				break;
			case 'supplierList':
				$result_array = $this->supplierList();
				break;
			case 'supplierDtl':
				$result_array = $this->supplierDtl();
				break;
			case 'stockInDtl':
				$result_array = $this->stockInDtl();
				break;
			case 'stockInList':
				$result_array = $this->stockInList();
				break;
			case 'stockOutList':
				$result_array = $this->stockOutList();
				break;
			case 'locationList':
				$result_array = $this->locationList();
				break;
			case 'stocktakeList':
				$result_array = $this->stocktakeList();
				break;
			case 'stocktakeDtl':
				$result_array = $this->stocktakeDtl();
				break;
			// case 'supplierDtl_2022':
			// 	$result_array = $this->supplierDtl();
			// 	break;
			case 'brandEditorContent':
				$this->gthis->assign("page_only", "Y");
				break;
			case 'itemCateEditor':
				array_push($this->gthis->breadcrumb, [
					"code" => "itemCateEditor",
					"url" => "admin/item/itemCateEditor",
					"page_title" => $this->gthis->RMBC->getReturnString('itemCateEditor')
				]);
				break;
			case 'itemTagEditor':
				array_push($this->gthis->breadcrumb, [
					"code" => "itemTagEditor",
					"url" => "admin/item/itemTagEditor",
					"page_title" => $this->gthis->RMBC->getReturnString('itemTagEditor')
				]);
				break;
			case 'brandEditor':
				array_push($this->gthis->breadcrumb, [
					"code" => "brandEditor",
					"url" => "admin/item/brandEditor",
					"page_title" => $this->gthis->RMBC->getReturnString('brandEditor')
				]);
				break;
			case 'supplierCateEditor':
				array_push($this->gthis->breadcrumb, [
					"code" => "supplierCateEditor",
					"url" => "admin/item/supplierCateEditor",
					"page_title" => $this->gthis->RMBC->getReturnString('supplierCateEditor')
				]);
				break;
			case 'stockInStockOut':
				array_push($this->gthis->breadcrumb, [
					"code" => "stockInStockOut",
					"url" => "admin/item/stockInStockOut",
					"page_title" => $this->gthis->RMBC->getReturnString('stockInStockOut')
				]);
				// $this->gthis->assign("no_header", "Y");
				break;
			default:
				break;
		}

		$this->gthis->assign('result_array',$result_array);
	}

	function stocktakeList(){
		if ($_SESSION['right']['warehouse'] < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);

		$skey = $this->gthis->parm_sql('request', 'skey');
		$page = $this->gthis->parm_sql('request', 'page');
		$limit = $this->gthis->parm_sql('request', 'limit');
		$sort = $this->gthis->parm_sql('request', 'sort');
		$order = $this->gthis->parm_sql('request', 'order');
        
		$filter = [
			"skey" => $skey ? $skey : null
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "create_datetime",
			"order" => $order ? $order : "DESC"
		];

		$stocktakeList = $warehouseCls->getStocktakeList($filter, $limiter, $sorting);

		$res['stocktakeList'] = $stocktakeList;
		$res['page_config'] = ["skey" => $skey];
		$res['page_config']['ttlCount'] = sizeof($warehouseCls->getStocktakeList($filter));
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "stocktakeList",
			"url" => "admin/item/stocktakeList",
			"page_title" => $this->gthis->RMBC->getReturnString('stocktakeList')
		]);

		return $res;
	}

	function stocktakeDtl(){
		if ($_SESSION['right']['warehouse'] < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);

		$stocktake_id = $this->gthis->parm_sql('request', 'stocktake_id', 'Y');

		$stocktakeDtl = $warehouseCls->getStocktakeDtl($stocktake_id);
		

		$res['stocktakeDtl'] = $stocktakeDtl;

		array_push($this->gthis->breadcrumb, [
			"code" => "stocktakeList",
			"url" => "admin/item/stocktakeList",
			"page_title" => $this->gthis->RMBC->getReturnString('stocktakeList')
		],[
			"code" => "stocktakeDtl",
			"url" => "admin/item/stocktakeDtl",
			"page_title" => substr($stocktakeDtl['create_datetime'],0,10)
		]);

		return $res;
	}

	function itemReplaceList(){
		if (intval($_SESSION['right']['item']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

        $itemReplaceList = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$filter = [
			"replacing" => 'Y'
		];


		$itemReplaceList = $itemCls->getItemList($shop_id, $filter);
	
		foreach($itemReplaceList as $k => $itemReplace){
			$itemReplaceList[$k]['replaceSku'] = $itemCls->getSkuDtl($itemReplace['replace_sku_id']);
		}
		$res['itemReplaceList'] = $itemReplaceList;

		array_push($this->gthis->breadcrumb, [
			"code" => "itemReplaceList",
			"url" => "admin/item/itemReplaceList",
			"page_title" => $this->gthis->RMBC->getReturnString('itemReplaceList')
		]);
		return $res;
	}

	function locationList(){
		if ($_SESSION['right']['warehouse'] < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);

		$skey = $this->gthis->parm_sql('request', 'skey');
		$page = $this->gthis->parm_sql('request', 'page');
		$limit = $this->gthis->parm_sql('request', 'limit');
		$sort = $this->gthis->parm_sql('request', 'sort');
		$order = $this->gthis->parm_sql('request', 'order');
        
		$filter = [
			"skey" => $skey ? $skey : null
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "adm_name",
			"order" => $order ? $order : "ASC"
		];

		$locationList = $warehouseCls->getLocationList($filter, $limiter, $sorting);
		foreach($locationList as $k => $i){
			$locationList[$k]['containingList'] = $warehouseCls->getLocationContainingList($i['location_id']);
		}

		$res['locationList'] = $locationList;
		$res['page_config'] = ["skey" => $skey];
		$res['page_config']['ttlCount'] = sizeof($warehouseCls->getLocationList($filter));
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "locationList",
			"url" => "admin/item/locationList",
			"page_title" => $this->gthis->RMBC->getReturnString('locationList')
		]);

		return $res;
	}

	function stockOutList(){
		if ($_SESSION['right']['warehouse'] < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		require_once ROOT.'/include/class/user.class.php';
		$userCls = new user($this->DB, $this->lang, $this->isAdmin);

		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);

		$skey = $this->gthis->parm_sql('request', 'skey');
		$page = $this->gthis->parm_sql('request', 'page');
		$limit = $this->gthis->parm_sql('request', 'limit');
		$sort = $this->gthis->parm_sql('request', 'sort');
		$order = $this->gthis->parm_sql('request', 'order');
        
		$filter = [
			"skey" => $skey ? $skey : null
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "create_datetime",
			"order" => $order ? $order : "DESC"
		];

		$outList = $itemCls->getStockOutList($filter, $limiter, $sorting);

		foreach($outList as $k => $out){
			$skuDtl = $itemCls->getSkuDtl($out['sku_id']);
			$skuDtl['itemDtl'] = $itemCls->getItemDtl($skuDtl['item_id']);
			$outList[$k]['skuDtl'] = $skuDtl;
			$outList[$k]['adminDtl'] = $userCls->getAdminDtl($out['updated_by_id']);
		}

		$locationList = $warehouseCls->getLocationList();
		$res['locationList'] = $locationList;



		$res['outList'] = $outList;

		$res['page_config'] = ["skey" => $skey];
		$res['page_config']['ttlCount'] = sizeof($itemCls->getStockOutList($filter));
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "stockOutList",
			"url" => "admin/item/stockOutList",
			"page_title" => $this->gthis->RMBC->getReturnString('stockOutList')
		]);

		return $res;
	}

	function stockInList(){
		if ($_SESSION['right']['warehouse'] < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/user.class.php';
		$userCls = new user($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/supplier.class.php';
		$supplierCls = new supplier($this->DB, $this->lang, $this->isAdmin);

		$skey = $this->gthis->parm_sql('request', 'skey');
		$page = $this->gthis->parm_sql('request', 'page');
		$limit = $this->gthis->parm_sql('request', 'limit');
		$sort = $this->gthis->parm_sql('request', 'sort');
		$order = $this->gthis->parm_sql('request', 'order');
        
		$filter = [
			"skey" => $skey ? $skey : null
		];

		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "create_datetime",
			"order" => $order ? $order : "DESC"
		];

		$purchaseList = $itemCls->getPurchaseList($filter, $limiter, $sorting);

        foreach($purchaseList as $k => $row){
            $purchaseList[$k]['inList'] = $itemCls->getStockInList(["po_id" => $row['po_id']]);
			$purchaseList[$k]['adminDtl'] = $userCls->getAdminDtl($row['updated_by_id']);
			$purchaseList[$k]['supplierDtl'] = $supplierCls->getSupplierDtl($row['supplier_id']);
        } 
		// foreach($purchaseList as $k => $purchase){
		// 	$purchaseList[$k]['skuDtl'] = $itemCls->getSkuDtl($purchase['sku_id']);
		// }

		$res['purchaseList'] = $purchaseList;
		$res['itemList'] = $itemCls->getItemList(["item_level_only" => "Y"]);

		$res['page_config'] = ["skey" => $skey];
		$res['page_config']['ttlCount'] = sizeof($itemCls->getPurchaseList($filter));
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "stockInList",
			"url" => "admin/item/stockInList",
			"page_title" => $this->gthis->RMBC->getReturnString('stockInList')
		]);
		return $res;
	}

	function stockInDtl(){
		if ($_SESSION['right']['warehouse'] < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/supplier.class.php';
		$supplierCls = new supplier($this->DB, $this->lang, $this->isAdmin);

		$po_id = $this->gthis->parm_sql('request', 'po_id');

		$purchaseDtl = $itemCls->getPurchaseDtl($po_id);
		$supplier = $supplierCls->getSupplierDtl($purchaseDtl['supplier_id']);

		$purchaseDtl['supplier'] = $supplier;
		$res['purchaseDtl'] = $purchaseDtl;

		// $purchaseDtl['inList'] = $itemCls->getStockInList(["po_id" => $purchaseDtl['po_id']]);
		// foreach($purchaseDtl['inList'] as $k => $in){
		// 	$purchaseDtl['inList'][$k]['skuDtl'] = $itemCls->getSkuDtl($in['sku_id']);
		// }

		// $purchaseDtl['supplierDtl'] = $itemCls->getSupplierDtl($purchaseDtl['supplier_id']);
		// $res['purchaseDtl'] = $purchaseDtl;
			
		array_push($this->gthis->breadcrumb, [
			"code" => "stockInList",
			"url" => "admin/item/stockInList",
			"page_title" => $this->gthis->RMBC->getReturnString('stockInList')
		],[
			"code" => "stockInDtl",
			"url" => "admin/item/stockInDtl",
			"page_title" => $purchaseDtl && $purchaseDtl['po_id']? $purchaseDtl['ref_no'] : $this->gthis->RMBC->getReturnString('stockInDtl')
		]);

		return $res;
	}

	function supplierDtl(){
		if ($_SESSION['right']['warehouse'] < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/supplier.class.php';
		$supplierCls = new supplier($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "supplier_id", "compulsory" => true, "type" => "int"],
		]);

		$supplier = $supplierCls->getSupplierDtl($data['supplier_id']);
		$res['supplier'] = $supplier;
		$res['supplier_tag_list'] = $supplierCls->getSupplierTagList();
		
		return $res;
	}

	function supplierList(){
		if ($_SESSION['right']['warehouse'] < 30){
			return array("right"=>"Failure");
		}

		// require_once ROOT.'/include/class/item.class.php';
		// $itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		// $shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		// $skey = $this->gthis->parm_sql('request', 'skey');
		// $page = $this->gthis->parm_sql('request', 'page');
		// $limit = $this->gthis->parm_sql('request', 'limit');
		// $sort = $this->gthis->parm_sql('request', 'sort');
		// $order = $this->gthis->parm_sql('request', 'order');
        
		// $filter = [
		// 	"skey" => $skey ? $skey : null
		// ];
		
		// $limiter = [
		// 	"page" => $page ? $page : 1,
		// 	"limit" => $limit ? $limit : 50
		// ];

		// $sorting = [
		// 	"sort" => $sort ? $sort : "supplier_id",
		// 	"order" => $order ? $order : "ASC"
		// ];

		// $result_array['skey'] = $filter['skey'];
		// $result_array['page'] = $limiter['page'];
		// $result_array['limit'] = $limiter['limit'];

		// $result_array['itemList'] = $itemCls->getSupplierList($filter, $limiter, $sorting);
		

		// $result_array['page_config'] = ["skey" => $skey];
		// $result_array['page_config']['ttlCount'] = sizeof($itemCls->getSupplierList($filter));
		// $result_array['page_config'] = array_merge($result_array['page_config'], $limiter);
		// $result_array['page_config'] = array_merge($result_array['page_config'], $sorting);

		// array_push($this->gthis->breadcrumb, [
		// 	"code" => "supplierList",
		// 	"url" => "admin/item/supplierList",
		// 	"page_title" => $this->gthis->RMBC->getReturnString('supplierList')
		// ]);
		return null;
	}

	function itemGrpDtl(){
		if ($_SESSION['right']['item'] < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$grp_id = $this->gthis->parm_sql('request', 'grp_id');
		$sku_id = $this->gthis->parm_sql('request', 'sku_id');
		$item_id = $this->gthis->parm_sql('request', 'item_id');
		
		$itemGrpDtl = $itemCls->getItemGrpDtl($grp_id);
		if ($sku_id){
			$skuDtl = $itemCls->getSkuDtl($sku_id);
			$skuList = $itemCls->getSkuList(['item_id' => $skuDtl['item_id']], null, ["sort" => "s.sku_id=$sku_id DESC, sku_val"]);

			foreach($skuList as $k => $sku){
				$skuList[$k]['qty'] = '1';
				$skuList[$k]['is_active'] = 'Y';
			}

			$itemGrpDtl['item_grp_content'] = $skuList;
		} else if ($item_id){
			$skuList = $itemCls->getSkuList(['item_id' => $item_id]);
			foreach($skuList as $k => $sku){
				$skuList[$k]['qty'] = '1';
				$skuList[$k]['is_active'] = 'Y';
			}
			$itemGrpDtl['item_grp_content'] = $skuList;
		}
		
		$res['itemGrpDtl'] = $itemGrpDtl;
		array_push($this->gthis->breadcrumb, [
			"code" => "itemList",
			"url" => "admin/item/itemGrpList",
			"page_title" => $this->gthis->RMBC->getReturnString('itemGrpList')
		],[
			"code" => "itemDtl",
			"url" => "admin/item/itemGrpDtl?grp_id=" . ($grp_id ? $grp_id : "0"),
			"page_title" => $grp_id ? $itemGrpDtl['adm_name'] : $this->gthis->RMBC->getReturnString('addItemGrp')
		]);
		return $res;
	}

	function itemGrpList(){
		if ($_SESSION['right']['item'] < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey');
		$page = $this->gthis->parm_sql('request', 'page');
		$limit = $this->gthis->parm_sql('request', 'limit');
		$sort = $this->gthis->parm_sql('request', 'sort');
		$order = $this->gthis->parm_sql('request', 'order');
        
		$filter = [
			"skey" => $skey ? $skey : null
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "grp_id",
			"order" => $order ? $order : "ASC"
		];
		
		$result_array['skey'] = $filter['skey'];
		$result_array['page'] = $limiter['page'];
		$result_array['limit'] = $limiter['limit'];
		$result_array['itemList'] = $itemCls->getItemGrpList($shop_id, $filter, $limiter, $sorting);


		$result_array['page_config'] = ["skey" => $skey];
		$result_array['page_config']['ttlCount'] = sizeof($itemCls->getItemGrpList($shop_id, $filter));
		$result_array['page_config'] = array_merge($result_array['page_config'], $limiter);
		$result_array['page_config'] = array_merge($result_array['page_config'], $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "itemGrpList",
			"url" => "admin/item/itemGrpList",
			"page_title" => $this->gthis->RMBC->getReturnString('itemGrpList')
		]);
		return $result_array;
	}

	function itemDtlContent(){
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		$data = $this->gthis->paramBuilder([
			["key" => "item_id", "type" => "int"],
		]);
		
		$item = $this->dispatchItemDtl($data['item_id']);
		$res['item'] = $item;
		
		require_once ROOT.'/include/class/supplier.class.php';
		$supplierCls = new supplier($this->DB, $this->lang, $this->isAdmin);
		$res['supplier_list'] = $supplierCls->getSupplierList();

		if ($data['supplier_id']){
			$res['supplier'] = $supplierCls->getSupplierDtl($data['supplier_id']);
		}

		require_once ROOT.'/include/class/itemCountry.class.php';
		$countryCls = new itemCountry($this->DB, $this->lang, $this->isAdmin);
		$res['country_list'] = $countryCls->getCountryList();
		
		require_once ROOT.'/include/class/itemBrand.class.php';
		$brandCls = new itemBrand($this->DB, $this->lang, $this->isAdmin);
		$res['brand_list'] = $brandCls->getBrandList();

		require_once ROOT.'/include/class/itemCate.class.php';
		$itemCateCls = new itemCate($this->DB, $this->lang, $this->isAdmin);
		$res['cate_list'] = $itemCateCls->getItemCateList();

		require_once ROOT.'/include/class/itemTag.class.php';
		$itemTagCls = new itemTag($this->DB, $this->lang, $this->isAdmin);
		$res['tag_list'] = $itemTagCls->getItemTagList();

		require_once ROOT.'/include/class/audit.class.php';
		$auditCls = new audit($this->DB, $this->lang, $this->isAdmin);
		$res['log_list'] = $auditCls->getItemLog($data['item_id']);

		return $res;
	}

	function itemDtl(){
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "item_id", "type" => "int"],
			["key" => "child_item_id", "type" => "int"],
			["key" => "supplier_id", "type" => "int"],
			["key" => "duplicate_item_id", "type" => "int"]
		]);

		if ($data['duplicate_item_id']){
			$item = $this->dispatchItemDtl($data['duplicate_item_id']);
			unset($item['item_id']); // item_id resume to 0 as create a new item
			unset($item['supplier_stock_code']);
			unset($item['supplier_barcode']);
			unset($item['sku']);
			unset($item['various_item_list']);

			$data['supplier_id'] = $item['supplier_id'];

			$res['item'] = $item;

		} else {
			$item = $this->dispatchItemDtl($data['item_id']);
			if ($item['various_item_id']){
				$childItem = $item;
				$item = $this->dispatchItemDtl($item['various_item_id']);
				$res['popup_item_id'] = $data['item_id'];
				$res['childItem'] = $childItem;
			} 

			if ($data['child_item_id']){
				$res['childItem'] = $this->dispatchItemDtl($data['child_item_id']);
				$res['popup_item_id'] = $data['child_item_id'];
			}

			$res['item'] = $item;
		}
		require_once ROOT.'/include/class/supplier.class.php';
		$supplierCls = new supplier($this->DB, $this->lang, $this->isAdmin);
		$res['supplier_list'] = $supplierCls->getSupplierList();

		if ($data['supplier_id']){
			$res['supplier'] = $supplierCls->getSupplierDtl($data['supplier_id']);
		}

		require_once ROOT.'/include/class/itemCountry.class.php';
		$countryCls = new itemCountry($this->DB, $this->lang, $this->isAdmin);
		$res['country_list'] = $countryCls->getCountryList();
		
		require_once ROOT.'/include/class/itemBrand.class.php';
		$brandCls = new itemBrand($this->DB, $this->lang, $this->isAdmin);
		$res['brand_list'] = $brandCls->getBrandList();

		require_once ROOT.'/include/class/itemCate.class.php';
		$itemCateCls = new itemCate($this->DB, $this->lang, $this->isAdmin);
		$res['cate_list'] = $itemCateCls->getItemCateList();

		require_once ROOT.'/include/class/itemTag.class.php';
		$itemTagCls = new itemTag($this->DB, $this->lang, $this->isAdmin);
		$res['tag_list'] = $itemTagCls->getItemTagList();

		if ($data['item_id']){
			require_once ROOT.'/include/class/audit.class.php';
			$auditCls = new audit($this->DB, $this->lang, $this->isAdmin);
			$res['log_list'] = $auditCls->getItemLog($data['item_id']);

			$res['stockInStockOutList'] = $itemCls->getStockInStockOutList(["item_id_arr" => $data['item_id']], "0, 20");

		}
		


		array_push($this->gthis->breadcrumb, [
			"code" => "itemList",
			"url" => "admin/item/itemList",
			"page_title" => $this->gthis->RMBC->getReturnString('itemList')
		],[
			"code" => "itemDtl",
			"url" => "admin/item/itemDtl?item_id=" . ($data['item_id'] ? $data['item_id'] : "0"),
			"page_title" => $data['item_id'] ? $item['inhouse_name_tc'] : $this->gthis->RMBC->getReturnString('addItem')
		]);

		return $res;
	}

	function createItem(){
		
		require_once ROOT.'/include/class/supplier.class.php';
		$supplierCls = new supplier($this->DB, $this->lang, $this->isAdmin);
		$res['supplier_list'] = $supplierCls->getSupplierList();

		require_once ROOT.'/include/class/itemCountry.class.php';
		$countryCls = new itemCountry($this->DB, $this->lang, $this->isAdmin);
		$res['country_list'] = $countryCls->getCountryList();
		
		require_once ROOT.'/include/class/itemBrand.class.php';
		$brandCls = new itemBrand($this->DB, $this->lang, $this->isAdmin);
		$res['brand_list'] = $brandCls->getBrandList();

		require_once ROOT.'/include/class/itemCate.class.php';
		$itemCateCls = new itemCate($this->DB, $this->lang, $this->isAdmin);
		$res['cate_list'] = $itemCateCls->getItemCateList();

		require_once ROOT.'/include/class/itemTag.class.php';
		$itemTagCls = new itemTag($this->DB, $this->lang, $this->isAdmin);
		$res['tag_list'] = $itemTagCls->getItemTagList();
		return $res;
	}

	function itemList(){
		if (intval($_SESSION['right']['item']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/itemCate.class.php';
		$itemCateCls = new itemCate($this->DB, $this->lang, $this->isAdmin);

		$res['item_cate_list'] = $itemCateCls->getItemCateList();

		array_push($this->gthis->breadcrumb, [
			"code" => "itemList",
			"url" => "admin/item/itemList",
			"page_title" => $this->gthis->RMBC->getReturnString('itemList')
		]);
		return $res;
	}

	function itemListWithSupplier(){
		if (intval($_SESSION['right']['item']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

        $shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$alert = $this->gthis->parm_sql('request', 'alert', 'N');
		$sort = $this->gthis->parm_sql('request', 'sort', 'N');
		$order = $this->gthis->parm_sql('request', 'order', 'N');
        
		$filter = [
			"skey" => $skey ? $skey : null,
			"alert" => $alert ? $alert : null
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "item_id",
			"order" => $order ? $order : "ASC"
		];
		
		$result_array['skey'] = $filter['skey'];
		$result_array['page'] = $limiter['page'];
		$result_array['limit'] = $limiter['limit'];
		$itemList = $itemCls->getItemList($shop_id, $filter, $limiter, $sorting);
		$result_array['itemList'] = $itemList;

		$filter = [
			"alert" => 'Y'	
		];
		$result_array['alertCount'] = sizeof($itemCls->getItemList($shop_id, $filter, null));

		$result_array['page_config'] = ["skey" => $skey];
		$result_array['page_config']['ttlCount'] = sizeof($itemCls->getItemList($shop_id, $filter));
		$result_array['page_config'] = array_merge($result_array['page_config'], $limiter);
		$result_array['page_config'] = array_merge($result_array['page_config'], $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "itemList",
			"url" => "admin/item/itemList",
			"page_title" => $this->gthis->RMBC->getReturnString('itemList')
		]);
		return $result_array;
	}

	private function dispatchItemDtl($item_id){
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);
		
		$item = $itemCls->getItemDtl($item_id);
		if ($item){
			if ($item['various_item_id']){
				$item['parent_item'] = $itemCls->getItemDtl($item['various_item_id']);
				$item['various_item_list'] = $itemCls->getItemList(["various_item_id" => $item['various_item_id']]);
			} else {
				$item['parent_item'] = null;
				$item['various_item_list'] = $itemCls->getItemList(["various_item_id" => $item['item_id']]);
			}
		}

		return $item;
	}

}
?>