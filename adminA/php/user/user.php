<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;
		
		require_once ROOT.'/include/class/user.class.php';
		$this->user = new user($this->DB, $this->lang, $this->isAdmin);

		switch ($this->gthis->submode) {
			case 'login':
				$this->gthis->assign('page_only','Y');
				$this->login();
				break;
			case 'adminList': 
				$result_array = $this->adminList();
				break;
			case 'adminDtl':
				$result_array = $this->adminDtl();
				break;
			case 'adminRoleList':
				$result_array = $this->adminRoleList();
				break;
			case 'adminRoleDtl':
				$result_array = $this->adminRoleDtl();
				break;
			default:
				break;
		}

		$this->gthis->assign('result_array',$result_array);
	}

	function login(){
		$_SESSION['otlk'] = $this->gthis->generateRandomString(6);
		$this->gthis->assign('otlk',$_SESSION['otlk']);
	}

	function adminList(){
		if (intval($_SESSION['right']['admin']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/user.class.php';
		$admin = new user($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sort = $this->gthis->parm_sql('request', 'sort', 'N');
		$order = $this->gthis->parm_sql('request', 'order', 'N');
        
		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "admin_id",
			"order" => $order ? $order : "ASC"
		];

		$res['adminList'] = $admin->getAdminList($filter, $limiter, $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "adminList",
			"url" => "admin/user/adminList",
			"page_title" => $this->gthis->RMBC->getReturnString('adminList')
		]);

		$res['page_config']['ttlCount'] = sizeof($admin->getAdminList($filter));
		$res['page_config'] = array_merge($res['page_config'], $filter);
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
	}

	function adminDtl(){
		if (intval($_SESSION['right']['admin']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/user.class.php';
		$admin = new user($this->DB, $this->lang, $this->isAdmin);
		
		$admin_id = $this->gthis->parm_sql('request', 'admin_id', 'N');

		$res['adminDtl'] = $admin->getAdminDtl($admin_id);
		$res['adminRoleList'] = $admin->getAdminRoleList();

		array_push($this->gthis->breadcrumb, [
			"code" => "adminList",
			"url" => "admin/user/adminList",
			"page_title" => $this->gthis->RMBC->getReturnString('adminList')
		],[
			"code" => "adminDtl",
			"url" => "admin/uesr/adminDtl?admin_id=" . ($admin_id ? $admin_id : "0"),
    		"page_title" => $admin_id ? $res['adminDtl']['adm_name'] : $this->gthis->RMBC->getReturnString('addAdmin')
		]);
		return $res;
	}

	function adminRoleList(){
		if (intval($_SESSION['right']['admin']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/user.class.php';
		$admin = new user($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sort = $this->gthis->parm_sql('request', 'sort', 'N');
		$order = $this->gthis->parm_sql('request', 'order', 'N');
        
		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "role_id",
			"order" => $order ? $order : "ASC"
		];

		$res['adminRoleList'] = $admin->getAdminRoleList();

		array_push($this->gthis->breadcrumb, [
			"code" => "adminRoleList",
			"url" => "admin/user/adminRoleList",
			"page_title" => $this->gthis->RMBC->getReturnString('adminRoleList')
		]);

		$res['page_config']['ttlCount'] = sizeof($admin->getAdminRoleList($filter));
		$res['page_config'] = array_merge($res['page_config'], $filter);
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
	}

	function adminRoleDtl(){
		if (intval($_SESSION['right']['admin']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/user.class.php';
		$admin = new user($this->DB, $this->lang, $this->isAdmin);
		
		$shop_id = $this->gthis->parm_sql('session', 'shop_id');
		$role_id = $this->gthis->parm_sql('request', 'role_id');

		$res['roleDtl'] = $admin->getAdminRoleDtl($role_id);

		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);
		$res['productList'] = $productCls->getProductList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "adminRoleList",
			"url" => "admin/user/adminRoleList",
			"page_title" => $this->gthis->RMBC->getReturnString('adminRoleList')
		],[
			"code" => "adminDtl",
			"url" => "admin/uesr/adminRoleDtl?role_id=" . ($role_id ? $role_id : "0"),
    		"page_title" => $role_id ? $res['roleDtl']['adm_name'] : $this->gthis->RMBC->getReturnString('addAdminRole')
		]);
		return $res;
	}
}
?>