/* ************************************************************************ */
/* Author : BTL
/* Version : 1.0.1
/* Prerequisite : jquery 1.7 或以上
/* Desc   : Poopup Modal for tailwind css
/*          針對tailwind css 配置的popup modal
/* Last Modify : David 
/* Last Update On : 02 Dec 2020
/* Last Update : - fix modal clean width
/* ************************************************************************ */
(function($) {
    var preSpinnerHtml = '';
    $.bmodal = function(settings) {
        settings = jQuery.extend({
            type: 'modal',
            close: 'N',
            title: (window.jstxt.jstxt0001 ? window.jstxt.jstxt0001 : 'loading') + '...',
            content: preSpinnerHtml,
            trueBtnName: 'confirm',
            falseBtnName: 'close',
            trueBtnClose: true,
            falseBtnClose: true,
            trueBtnCustomClass: 'bg-secondary text-white font-semibold px-4 py-2 rounded-lg hover:opacity-75 transition-all duration-100',
            falseBtnCustomClass: 'bg-gray-100 text-gray-800 font-semibold px-4 py-2 mr-2 rounded-lg hover:text-gray-60 hover:opacity-75  transition-all duration-100',
            trueBtnTWTextColor: 'text-primary',
            trueBtnTWHoverTextColor: 'text-primary',
            falseBtnTWTextColor: 'text-gray-700',
            falseBtnTWHoverTextColor: 'text-gray-600',
            headerCloseBtn: false,
            footerClose: false,
            ajaxPath: null,
            elementId: 'defaultBmodalId' + Date.now(),
            containerWidthOverride: null,
            containerHeightOverride: null,
            containerBgColorOverride: null,
            trueBtnCloseDestroy: true,
            contentRefresh: false,
            noToggle: false,
            closeOnBackgroundClick: false,
            ajaxDelayLoadTime: false,
            toastTime: 2000,
            screenSizeMode: false,
            callback: function(obj) {},
            trueBtncallback: function(obj) {

            },
            falseBtncallback: function(obj) {

            },
            callbackAfterClose: function(obj) {

            }
        }, settings);

        if (settings.close == 'Y') {
            $('#' + settings.elementId).remove();
            // remove scroll setting
            resumeBodyOverflow();
            return;
        }

        var cssLoading = `  <style>
                            html,
                            body { width: 100%; height: 99999px; overflow:hidden;}
                            </style>
                            `;

        if (settings.type == 'alert') {
            var modal = `<div ` + (settings.elementId ? "id='" + settings.elementId + "'" : "") + ` class="bmodal modal z-50 opacity-0 pointer-events-none fixed ${settings.screenSizeMode == true ? 'w-screen h-screen' : 'w-full h-full'} top-0 left-0">
                            <div class="relative w-full h-full">
                                <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>
                                <div class="modal-container absolute w-full h-full flex justify-center items-center">
                                    <div class="mx-3 bg-white rounded-lg ` + (settings.containerWidthOverride != null ? settings.containerWidthOverride : "min-w-48") + ' ' + (settings.containerHeightOverride != null ? settings.containerHeightOverride : "") + `">
                                        <!-- Add margin if you want to see some of the overlay behind the modal-->
                                        <div class="py-4 text-left px-6 flex flex-col h-full">
                                            <!--Title-->
                                            <div class="flex justify-between items-center pb-3">
                                                <p class="text-2xl font-bold">` + settings.title + `</p>
                                                ` + (settings.headerCloseBtn ? `<div><a href="#" class="text-2xl modal-close cross-close"><i class="ri-close-line"></i></a></div>` : "") + `
                                            </div>
                                            <!--Body-->
                                            <div class="flex-6/10 scrolling-touch overflow-y-auto">
                                                ` + settings.content + `
                                            </div>
                                            <!--Footer-->
                                            <div class="flex-2/10 flex justify-end pt-4 ` + (settings.footerClose ? "hidden " : "") + `">
                                                <button class="capitalize trueBtn ${settings.trueBtnClose ? "modal-close " : ""} ${settings.trueBtnCustomClass}">` + settings.trueBtnName + `</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>` + cssLoading + `
                            </div>
                        </div>`;
        }

        if (settings.type == 'confirm') {
            var modal = `<div ` + (settings.elementId ? "id='" + settings.elementId + "'" : "") + ` class="bmodal modal z-50 opacity-0 pointer-events-none fixed ${settings.screenSizeMode == true ? 'w-screen h-screen' : 'w-full h-full'} top-0 left-0">
                            <div class="relative w-full h-full">
                                <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>
                                <div class="modal-container absolute w-full h-full flex justify-center items-center">
                                    <div class="mx-3 bg-white rounded-lg ` + (settings.containerWidthOverride != null ? settings.containerWidthOverride : "min-w-48") + ' ' + (settings.containerHeightOverride != null ? settings.containerHeightOverride : "") + `">
                                        <!-- Add margin if you want to see some of the overlay behind the modal-->
                                        <div class="py-4 text-left px-6 h-full">
                                            <!--Title-->
                                            <div class="flex justify-between items-center pb-3">
                                                <p class="text-2xl font-bold">` + settings.title + `</p>
                                                ` + (settings.headerCloseBtn ? `<div><a href="#" class="text-2xl modal-close cross-close"><i class="ri-close-line"></i></a></div>` : "") + `
                                            </div>
                                        <!--Body-->
                                        <div>
                                            ` + settings.content + `
                                        </div>
                                        <!--Footer-->
                                        <div class="flex justify-end pt-2 ` + (settings.footerClose ? "hidden" : "") + `">
                                            <button class="falseBtn ${settings.falseBtnClose ? "modal-close " : ""} ${settings.falseBtnCustomClass}">` + settings.falseBtnName + `</button>
                                            <button class="trueBtn ${settings.trueBtnClose ? "modal-close " : ""} ${settings.trueBtnCustomClass}">` + settings.trueBtnName + `</button>
                                        </div>
                                    </div>
                                </div>` + cssLoading + `
                            </div>
                        </div>`;
        }

        if (settings.type == 'modal') {
            var modal = `<div ` + (settings.elementId ? "id='" + settings.elementId + "'" : "") + ` class="bmodal modal z-50 opacity-0 pointer-events-none fixed ${settings.screenSizeMode == true ? 'w-screen h-screen' : 'w-full h-full'} top-0 left-0">
                            <div class="relative w-full h-full">
                                <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>
                                <div class="modal-container absolute w-full h-full flex justify-center items-center pointer-events-none">
                                    <div class="mx-3 bg-white rounded-lg pointer-events-auto ` + (settings.containerWidthOverride != null ? settings.containerWidthOverride : "min-w-48") + ' ' + (settings.containerHeightOverride != null ? settings.containerHeightOverride : "") + `">
                                        <!-- Add margin if you want to see some of the overlay behind the modal-->
                                        <div class="py-4 text-left px-6">
                                            <!--Title-->
                                            <div class="flex justify-between items-center pb-3">
                                                <p class="text-2xl font-bold">` + settings.title + `</p>
                                                ` + (settings.headerCloseBtn ? `<div><a href="#" class="text-2xl modal-close cross-close"><i class="ri-close-line"></i></a></div>` : "") + `
                                            </div>
                                        <!--Body-->
                                        <div class="bmodal-ajaxContent">
                                            ` + (settings.content ? settings.content : "Loading...") + `
                                        </div>
                                        <!--Footer-->
                                        <div class="flex justify-end pt-2 ` + (settings.footerClose ? "hidden" : "") + `">
                                            <button class="falseBtn ${settings.falseBtnClose ? "modal-close " : ""} ${settings.falseBtnCustomClass}">` + settings.falseBtnName + `</button>
                                            <button class="trueBtn ${settings.trueBtnClose ? "modal-close " : ""} ${settings.trueBtnCustomClass}">` + settings.trueBtnName + `</button>
                                        </div>
                                    </div>
                                </div>` + cssLoading + `
                            </div>
                        </div>`;
        }
        if (settings.type == 'modalClean' && settings.ajaxPath) {
            var modal = `<div ` + (settings.elementId ? "id='" + settings.elementId + "'" : "") + ` class="bmodal modal z-50 opacity-0 pointer-events-none fixed ${settings.screenSizeMode == true ? 'w-screen h-screen' : 'w-full h-full'} top-0 left-0">
                            <div class="relative w-full h-full">
                                <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>
                                <div class="modal-container absolute w-full h-full flex justify-center items-center pointer-events-none">
                                    <div class="bmodal-ajaxContent h-auto flex flex-col items-center justify-center pointer-events-auto">
                                        <div class="bg-white rounded-lg p-8 overflow-hidden ease-in-out md:h-auto">
                                            <div class="flex flex-col">
                                                    <img src="../img/design/character/box_character.svg" class="mx-auto block w-16 h-16 animate-bounce" />
                                                    <div class="text-center text-gray-600 mt-4 font-semibold ">${(settings.title ? settings.title : 'loading...')}</div>
                                               </div>
                                    
                                        </div>
                                    </div>
                                </div>` + cssLoading + `
                            </div>
                        </div>`;
        }
        if (settings.type == 'modalClean' && settings.content != '') {
            var modal = `<div ` + (settings.elementId ? "id='" + settings.elementId + "'" : "") + ` class="bmodal modal z-50 opacity-0 pointer-events-none fixed ${settings.screenSizeMode == true ? 'w-screen h-screen' : 'w-full h-full'} top-0 left-0">
                            <div class="relative w-full h-full">
                                <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>
                                <div class="modal-container absolute w-full h-full flex justify-center items-center pointer-events-none">
                                    <div class="bmodal-ajaxContent h-auto flex flex-col items-center justify-center pointer-events-auto">
                                        ` + settings.content + `
                                    </div>
                                </div>` + cssLoading + `
                            </div>
                        </div>`;
        }
        if (settings.type == 'modalloader') {
            //     var cssLoading = `  <style>
            // html, body {
            //   overflow-y: hidden;
            // }
            // </style>
            // `;

            var modal = `<div ` + (settings.elementId ? "id='" + settings.elementId + "'" : "") + ` class="bmodal modal opacity-0 pointer-events-none fixed ${settings.screenSizeMode == true ? 'w-screen h-screen' : 'w-full h-full'} top-0 left-0" style="z-index:999999999">
                            <div class="relative w-full h-full">
                                <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>
                                <div class="modal-container absolute w-full h-full flex justify-center items-center pointer-events-none">
                                    <div class="mx-3 bg-white rounded-lg ` + (settings.containerWidthOverride != null ? settings.containerWidthOverride : "min-w-48") + ' ' + (settings.containerHeightOverride != null ? settings.containerHeightOverride : "") + `">
                                        <div class="bg-white rounded-lg p-8 overflow-hidden ease-in-out md:h-auto">
                                            <div class="bloader relative flex justify-center">
                                               
                                               <div class="flex flex-col">
                                                    <img src="../img/design/character/box_character.svg" class="mx-auto block w-16 h-16 animate-bounce" />
                                                    <div class="text-center text-gray-600 mt-4 font-semibold ">${settings.title}</div>
                                               </div>
                                                       
                                            </div>
                                        </div>
                                    </div>
                                </div>` + cssLoading + `
                            </div>
                        </div>`;
        }

        if (settings.type == 'toast') {
            /*var cssLoading = `  <style>
                                html, body {
                                  overflow-y: hidden;
                                }
                                </style>
                                `;*/


            var modal = `<div ` + (settings.elementId ? "id='" + settings.elementId + "'" : "") + ` class="bmodal modal z-50 opacity-0 pointer-events-none fixed ${settings.screenSizeMode == true ? 'w-screen h-screen' : 'w-full h-full'} top-0 left-0">
                            <div class="relative w-full h-full">
                                <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>
                                <div class="modal-container absolute w-full h-full flex justify-center items-center">
                                    <div class="mx-3 bg-white rounded-lg ` + (settings.containerWidthOverride != null ? settings.containerWidthOverride : "min-w-48") + ' ' + (settings.containerHeightOverride != null ? settings.containerHeightOverride : "") + `">
                                        <!-- Add margin if you want to see some of the overlay behind the modal-->
                                        <div class="py-4 text-left px-6 flex flex-col h-full">
                                            <!--Body-->
                                            <div class="scrolling-touch overflow-y-auto">
                                                 <div class="flex flex-col">
                                                    <div class="bmodal-ajaxContent h-auto flex-1 py-2">
                                                        ` + settings.content + `
                                                    </div>
                                                    <div class="text-center font-semibold ml-2">` + settings.title + `</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>` + cssLoading + `
                            </div>
                        </div>`;
        }

        /*$('html').attr('data-scroll-top', $('html').scrollTop())    
        $('html, body').css({
            'overflow-y': 'hidden',
        });*/

        if (settings.contentRefresh) {
            // do nth
            if (settings.ajaxPath) {
                $.bajax({
                    url: settings.ajaxPath,
                    debug: 'N',
                    pureString: 'Y',
                    callback: function(data) {
                        try {
                            var result = data.split('||xx||success||xx||');
                            if (result[0] == '0') {
                                if (settings.ajaxDelayLoadTime) {
                                    setTimeout(function() {
                                        $("#" + settings.elementId).find('.bmodal-ajaxContent').html(result[1]);
                                    }, settings.ajaxDelayLoadTime);
                                } else {
                                    $("#" + settings.elementId).find('.bmodal-ajaxContent').html(result[1]);
                                }
                            } else {
                                if (settings.ajaxDelayLoadTime) {
                                    setTimeout(function() {
                                        $("#" + settings.elementId).find('.bmodal-ajaxContent').html(data);
                                    }, settings.ajaxDelayLoadTime);
                                } else {
                                    $("#" + settings.elementId).find('.bmodal-ajaxContent').html(data);
                                }
                            }
                        } catch (e) {
                            //debug(e);
                            //debug(data);
                        }
                    },
                });
            }
        } else {
            //debug(settings.elementId);
            if ($("#" + settings.elementId).length > 0) {
                //debug('clear');
                //debug(settings.elementId);
                $("#" + settings.elementId).closest('.bmodal').remove();
            }
            $('body').append(modal);

            if (settings.ajaxPath) {
                $.bajax({
                    url: settings.ajaxPath,
                    debug: 'N',
                    pureString: 'Y',
                    callback: function(data) {
                        //debug(settings.elementId);
                        try {
                            var result = data.split('||xx||success||xx||');
                            if (result[0] == '0') {
                                if (settings.ajaxDelayLoadTime) {
                                    setTimeout(function() {
                                        $("#" + settings.elementId).find('.bmodal-ajaxContent').html(result[1]);
                                    }, settings.ajaxDelayLoadTime);
                                } else {
                                    $("#" + settings.elementId).find('.bmodal-ajaxContent').html(result[1]);
                                }
                            } else {
                                if (settings.ajaxDelayLoadTime) {
                                    setTimeout(function() {
                                        $("#" + settings.elementId).find('.bmodal-ajaxContent').html(data);
                                    }, settings.ajaxDelayLoadTime);
                                } else {
                                    $("#" + settings.elementId).find('.bmodal-ajaxContent').html(data);
                                }
                            }
                        } catch (e) {
                            //debug(e);
                            //debug(data);
                        }
                    },
                });
            }
        }

        if (settings.closeOnBackgroundClick) {
            //debug('raiser');
            $("#" + settings.elementId).find('.modal-overlay').addClass('modal-close');
            $("#" + settings.elementId).find('.modal-overlay').addClass('cursor-pointer');

            // setup esc for close modal
            if (window.bmodalKeypressBinded) {
                // skip
            } else {
                $(document).on('keyup', function(e) {
                    //debug(e.key);
                    //debug("#" + settings.elementId);
                    if (e.key === "Escape") {
                        $("#" + settings.elementId).find('.modal-close:first').trigger('click');
                    }
                    window.bmodalKeypressBinded = true; // do not allow rebind
                });
            }
        }

        // handle toast auto dimiss
        if (settings.type == 'toast') {
            setTimeout(function() {
                $("#" + settings.elementId).closest('.bmodal').remove();
            }, settings.toastTime);
        }

        run(settings.callback());
        var object = [];
        object['element'] = $('.bmodal:last');
        object['settings'] = settings;
        object['elementId'] = settings.elementId;
        return object;

        function resumeBodyOverflow() {
            /*var $html = $('html')
            $('html, body').scrollTop($html.attr('data-scroll-top'))
            $('html').removeAttr('data-scroll-top');    
            $('html, body').css({
                'overflow-y': '',
            });*/
        }

        function run(funcToExecute) {

            $(document).off('click', '.modal-close');
            $(document).on("click", '.modal-close', function(e) {
                var runResume = false;
                var parentSetting = $(this).closest('.bmodal').attr('id');
                var isdefaultModal = false;
                if (parentSetting.indexOf('defaultBmodalId') > -1) {
                    isdefaultModal = true;
                }
                //debug(parentSetting)
                if (parentSetting && !isdefaultModal && window[parentSetting].settings) {
                    //debug(window[parentSetting].settings)
                    // provide element id, can find setting from the provided element id
                    if ($(this).hasClass('trueBtn')) {
                        if (window[parentSetting].settings.trueBtncallback) {
                            window[parentSetting].settings.trueBtncallback();
                        }
                    }
                    if ($(this).hasClass('falseBtn')) {
                        if (window[parentSetting].settings.falseBtncallback) {
                            window[parentSetting].settings.falseBtncallback();
                        }
                    }
                    if ($(this).hasClass('modal-overlay')) {
                        if (window[parentSetting].settings.closeOnBackgroundClick) {
                            $(this).closest('.bmodal').remove();
                            runResume = true;
                        }
                    }
                    if (window[parentSetting].settings.trueBtnCloseDestroy) {
                        $(this).closest('.bmodal').remove();
                        runResume = true;
                    }
                } else {
                    if ($(this).hasClass('trueBtn')) {
                        if (settings.trueBtncallback) {
                            settings.trueBtncallback();
                        }
                    }
                    if ($(this).hasClass('falseBtn')) {
                        if (settings.falseBtncallback) {
                            settings.falseBtncallback();
                        }
                    }
                    if ($(this).hasClass('modal-overlay')) {
                        if (settings.closeOnBackgroundClick) {
                            $(this).closest('.bmodal').remove();
                            runResume = true;
                        }
                    }
                    if (settings.trueBtnCloseDestroy) {
                        $(this).closest('.bmodal').remove();
                        runResume = true;
                    }
                }


                if ($(this).hasClass('falseBtn')) {
                    $(this).closest('.bmodal').remove();
                    runResume = true;
                }

                if ($(this).hasClass('cross-close')) {
                    $(this).closest('.bmodal').remove();
                    runResume = true;
                }

                if (runResume) {
                    resumeBodyOverflow();
                }
            });


            function toggleModal() {
                try {
                    const body = document.querySelector('body');
                    const modal = document.querySelector('#' + settings.elementId);
                    modal.classList.toggle('opacity-0');
                    modal.classList.toggle('pointer-events-none');
                    //body.classList.toggle('modal-active');    
                } catch (e) {

                }

            }
            if (settings.contentRefresh) {} else {
                toggleModal();
            }
        }
    }
}(jQuery));