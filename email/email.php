<?php	
require_once '../include/baseModel.class.php';

class emailHandler extends baseModel{

	public function __construct(){
		parent::__construct('./email');
		require_once ROOT.'/include/langMap/returnMessage.php';
		$this->RM = new returnMessage($this->lang);

		$this->init();
		if(!in_array($this->connectionIp, $this->whiteListIpArray)){
			die('close on:'.$this->connectionIp);
		}
	}

	public function init(){
		switch($this->mode){
			case 'emailActivationCode':
				$this->emailActivationCode();
				$this->assign('page_only','Y');
				$this->page = 'emailActivationCode';
				break;
			case 'resetPasswordEmail':
				$this->resetPasswordEmail();
				$this->assign('page_only','Y');
				$this->page = 'resetPasswordEmail';
				break;
			case 'orderInvoice':
				$this->orderInvoice();
				$this->assign('page_only','Y');
				$this->page = 'orderInvoice';
				break;
			case 'orderConfirmation':
				$this->orderConfirmation();
				$this->assign('page_only','Y');
				$this->page = 'orderConfirmation';
				break;
			case 'pickUpConfirmationLetter':
				$this->pickUpConfirmationLetter();
				$this->assign('page_only','Y');
				$this->page = 'pickUpConfirmationLetter';
				break;
			case 'orderShipOutNotice':
				$this->orderShipOutNotice();
				$this->assign('page_only','Y');
				$this->page = 'orderShipOutNotice';
				break;
			case 'finalDeliveryConfirmation':
				$this->finalDeliveryConfirmation();
				$this->assign('page_only','Y');
				$this->page = 'finalDeliveryConfirmation';
				break;
			case 'sendServiceTicketReplyEmail':
				$this->sendServiceTicketReplyEmail();
				$this->assign('page_only','Y');
				$this->page = 'sendServiceTicketReplyEmail';
				break;
			case 'sendOrderCancelAndRefundEmail':
				$this->sendOrderCancelAndRefundEmail();
				$this->assign('page_only','Y');
				$this->page = 'sendOrderCancelAndRefundEmail';
				break;
			case '2021easter':
				$this->assign('page_only','Y');
				$this->page = '2021easter';
				break;
            case 'deliveryFailureEmail':
                $this->deliveryFailureEmail();
                $this->assign('page_only','Y');
                $this->page = 'deliveryFailureEmail';
                break;
            case 'customsDeclarationEmail':
                $this->customsDeclarationEmail();
                $this->assign('page_only','Y');
                $this->page = 'customsDeclarationEmail';
                break;
            case 'addresseeEmail':
                $this->addresseeEmail();
                $this->assign('page_only','Y');
                $this->page = 'addresseeEmail';
                break;
			default:
				$this->home();
				$this->page = 'home';
				break;
		}
	}

	public function display(){
		$this->assign('RM', $this->array_merge_keep_key($this->RM->csv,$this->RM->csv_string));
		$this->assign('RMS', $this->RM->csv_string);
		$this->assign('RMJS', json_encode($this->RM->csv_js_string));
		$this->assign('mode',isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$this->assign('pageStatus',$this->page);
		$this->assign('page',$this->page . '.html');
		$this->assign('business_email',BUSINESS_EMAIL);
		$this->displayPage('index.html');
	}

	/* ******************************************************************** */
	/* ** Author        : David Chung                                       */
	/* ** Last Updat On : 30 Mar 2020                                       */
	/* ** Desciption    : 獲取驗証email                                      */
	/* ******************************************************************** */

	function emailActivationCode(){
		$result_array['member_name'] = $this->parm_in('request','member_name','Y');
		$result_array['pins'] = $this->parm_in('request','pins','Y');
		$this->assign('result_array',$result_array);
	}
	
	function resetPasswordEmail(){
		$result_array['member_name'] = $this->parm_in('request','member_name','Y');
		$result_array['reset_password'] = $this->parm_in('request','reset_password','Y');
		$this->assign('result_array',$result_array);
	}

	function orderInvoice(){
		$order_id = $this->parm_in('request','order_id','Y');
		$member_id = $this->parm_in('request','member_id','Y');

		require_once dirname(__FILE__).'/../include/order.app.class.php';
		$orderClass = new order($this,$this->DB);
		$order = $orderClass->memberGetOrder($member_id,$order_id);

		$query = "select * from `app_tbl_member_charge_records` where `link_id` = '".$this->escape_string($order_id)."' and `member_id` = '".$this->escape_string($member_id)."' and `confirmed` = 'Y';";
		$charge_record = $this->DB->get_Sql($query)[0];

		/*$address_mask = '';
		$address_mask_length = mb_strlen($order['pickup_address']);
		$address_mask_length = intval($address_mask_length);
		for($i=0;$i<$address_mask_length/2;$i++){
			$address_mask = $address_mask.'x';
			if($i>50){
				break;
			}
		}
		$order['pickup_address'] =  substr($order['pickup_address'], 0 , $address_mask_length/2).$address_mask;
		$order['sender_mobile'] =  substr($order['sender_mobile'], 0,8).'xxxx';*/

		require_once dirname(__FILE__).'/../include/coupon.class.php';
		$couponClass = new coupon($this, $this->DB);


		$quote_price = $order['quote_price'];
		$pickup_fee = $order['pickup_fee'];
		$ori_amount = $quote_price+$pickup_fee;

		$amount = 0;
		// check coupon code here
		if($order['coupon_code']){
			$coupon_code = $order['coupon_code'];
			$amount = $couponClass->getCouponEffect($coupon_code,$ori_amount,true);
		} else {
			$amount = $ori_amount;
		}

		$result_array['order'] = $order;
		$result_array['order_id'] = $order_id;
		$result_array['bill_amount'] = $amount;
		if($amount>0){
			$result_array['discounted_value'] = $ori_amount-$amount;	
		}
		$result_array['charge_record'] = $charge_record;
		$result_array['current_time'] = $this->get_current_time();
		$this->assign('result_array',$result_array);
	}

	function orderConfirmation(){
		$this->orderInvoice();
	}

	function pickUpConfirmationLetter(){
		$order_id = $this->parm_in('request','order_id','Y');
		$member_id = $this->parm_in('request','member_id','Y');
		$is_document = $this->parm_in('request','is_document','Y');
		$contains_battery = $this->parm_in('request','contains_battery','Y');
		$need3481 = $this->parm_in('request','need3481','Y');
		$need3091 = $this->parm_in('request','need3091','Y');

		require_once ROOT.'/include/order.app.class.php';
		$orderClass = new order($this,$this->DB);
		$order = $orderClass->memberGetOrder($member_id,$order_id);
		$result_array['order'] = $order;


		$result_array['is_document'] = $is_document;
		$result_array['contains_battery'] = $contains_battery;
		$result_array['need3481'] = $need3481;
		$result_array['need3091'] = $need3091;
		$this->assign('result_array',$result_array);
	}

	function orderShipOutNotice(){
		$this->orderInvoice();
	}

	function finalDeliveryConfirmation(){
		$this->orderInvoice();
	}

	function deliveryFailureEmail(){
		$this->orderInvoice();
	}

	function customsDeclarationEmail(){
		$this->orderInvoice();
	}

	function addresseeEmail(){
		$this->orderInvoice();
	}

	function sendOrderCancelAndRefundEmail(){
		$this->orderInvoice();
	}

	function sendServiceTicketReplyEmail(){
		$ticket_id = $this->parm_in('request','ticket_id','Y');
		$member_id = $this->parm_in('request','member_id','Y');

		require_once dirname(__FILE__).'/../include/serviceTicket.class.php';
		$serviceTicketClass = new serviceTicket($this->gthis,$this->DB);
		$service_ticket = $serviceTicketClass->getSupportTicket($member_id,$ticket_id);

		$result_array['service_ticket'] = $service_ticket[0];
		$this->assign('result_array',$result_array);
	}
}
$HOME = new emailHandler;
$HOME->display();

?>