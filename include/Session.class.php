<?php

class Session{
	function __construct(){
		/*
		 use memory cache to store the session into memory.
		
		$host="127.0.0.1"; 
		$port=11211;
		
		$session_save_path = "tcp://$host:$port?persistent=1&weight=2&timeout=2&retry_interval=10,  ,tcp://$host:$port";
		ini_set('session.save_handler', 'memcache');
		ini_set('session.save_path', $session_save_path);
		ini_set("session.gc_maxlifetime", 24*60*60); //24hour
		*/
		// session will be valid for one month
		// todo security : make it configurable
		
		session_set_cookie_params(60*60*60);
		session_cache_expire(360);
		
		//session_start();
		if (!isset($_SESSION)){
			if (!headers_sent())
            {	
				set_time_limit(360);
				session_set_cookie_params(0, '/', SESSION_DOMAIN);
                session_start();
				header("Cache-control: private");
			}
            //else
            // {
			//	trigger_error('Cannot start session, headers already sent. This a bad error. Aborting');
			//	die();
			//}
		} else {
			//
		}
	}
	
	function register($id){
		session_register($id);
	}
	
	function set($id,$value){
		$_SESSION[$id]=$value;
	}
	
	
	function get($id){
		if (isset($_SESSION[$id])){
				return $_SESSION[$id];
		}else{
				return false;
		}
	}
	
	function delete($id){
		if ( isset ( $_SESSION[$id] ) ){
				unset ( $_SESSION[$id] );
				return true;
		}else{
				return false;
			}
	}
		
	function destroy(){
		session_destroy();
	}
		
	/* 
	Given an $object, it will persist it. The object must implement :
	getIt()
	getSettings -> array
	setSettings <- array
	*/
	function persist(&$object){
		die ('deprecated');
		if ( method_exists($object, 'getId') and method_exists($object, 'getSettings') and method_exists($object, 'setSettings') )
		{
				$object_id = $object->getId();
				$object_settings = $object->getSettings();
				$saved_settings = $this->get($object_id);
				
				if (is_array($object_settings)){
						$result = array_merge($saved_settings, $object_settings);
				}else{
						$result = $saved_settings;
				}
				
				if (is_array($result)){
						$this->set($id, $result);
						$object->setSettings($result);
				}
				
				debug($result, 'results from array merge');
				debug($_SESSION, 'Session data');
				return true;
		}else{
				trigger_error('session::persist() the object you try to persist doesn\'t provide the persistence API, getId, get- and setSettings()');
		}
			
	}
}
?>