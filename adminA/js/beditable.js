/* ************************************************************************ */
/* Author : BTL
/* Prerequisite : jquery 1.7 或以上
/* Desc   : This is a focus out update callback trigger, together with use with beditable.php
/*          這是一個自動更新回調觸發器，與beditable.php一起使用
/* Last Modify : David 
/* Last Update : 25 May 2020
/* ************************************************************************ */

var beditable_snap_val;
$(document).ready(function() {
    $(document).off('change', '.beditablec');
    $(document).on('change', '.beditablec', function() {
        var value;
        if ($(this).is(':checked')) {
            value = $(this).val();
        } else {
            value = '';
        }
        var element = $(this);
        var record_id = $(this).attr('record_id');
        var ftype = $(this).attr('ftype');
        var field = $(this).attr('field');
        var action_prefix = $(this).attr('action_prefix');
        var show_color = 'N';
        var new_value = value;
        if ($(this).hasClass('show_color')) {
            show_color = 'Y';
        }
        if ($(this).hasClass('show_color_de')) {
            show_color = 'DE';
        }
        update_beditable_field(new_value, record_id, element, ftype, field, show_color,action_prefix);
    })

    $(document).off('focusin', '.beditable');
    $(document).on("focusin", ".beditable", function(e) {
        beditable_snap_val = $(this).val();
        return;
    });

    $(document).off('focusout', '.beditable');
    $(document).on("focusout", ".beditable", function(e) {
        var current_value = $(this).val();

        if (beditable_snap_val != current_value) {
            console.log('value changed');
        } else {
            return;
        }
        var element = $(this);
        var is_element_select = $(this).is("select");
        var new_value = "";
        if (is_element_select) {
            new_value = $(this).children('option:selected').val();
        } else {
            new_value = $(this).val();
        }
        var record_id = $(this).attr('record_id');
        var ftype = $(this).attr('ftype');
        var field = $(this).attr('field');
        var action_prefix = $(this).attr('action_prefix');
        var show_color = 'N';
        if ($(this).hasClass('show_color')) {
            show_color = 'Y';
        }
        if ($(this).hasClass('show_color_de')) {
            show_color = 'DE';
        }
        update_beditable_field(new_value, record_id, element, ftype, field, show_color,action_prefix);
    });

    function update_beditable_field(new_value, record_id, element, ftype, field, show_color,action_prefix) {
        var xdebug = 'N';
        if (show_color == 'DE') {
            xdebug = 'Y';
        }
        var url = "/beditable.php?mode=field_update&record_id=" + encodeURIComponent(record_id) + "&ftype=" + encodeURIComponent(ftype) + "&field=" + encodeURIComponent(field);
        if(action_prefix){
            url = action_prefix+url;
        }
        $('#beditableForm').remove();
        var formHtml = "<form id='beditableForm' method='post' action='"+url+"' style='display:none;'><textarea name='value'>"+encodeURIComponent(new_value)+"</textarea></form>";
        $('body').append(formHtml);
        $.bajax({
            url: url,
            debug: xdebug,
            formElement: "Y",
            formElementID: "beditableForm", 
            callback: function(data) {
                if (data.result.code != '0') {
                    if (show_color == 'Y') { element.css('background-color', 'red'); }
                    element.addClass('beditable-error')
                    setTimeout(function() {
                        if (show_color == 'Y') { element.css('background-color', ''); }
                        element.removeClass('beditable-error')
                    }, 1400);
                    //toast({content:data.msg,backgroundColor:'rgba(0,0,0,.7)',Color:'#fff'}) ;
                    return;
                }

                if (show_color == 'Y') { element.css('background-color', 'green'); }
                element.addClass('beditable-success')
                setTimeout(function() {
                    if (show_color == 'Y') { element.css('background-color', ''); }
                    element.removeClass('beditable-success')
                }, 1400);

                //toast({content:data.msg,backgroundColor:'rgba(0,0,0,.7)',Color:'#fff'}) ;
                try {
                    // try on JS function
                    beditable_local_callback(element, data);
                } catch (e) {
                    //console.log('No Callback');
                }
            }
        });
    }
});