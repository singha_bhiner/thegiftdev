<?php
/* 
    Objective: holiday setting for out of stock, day off etc.
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_holiday
        tmap_holiday_product

        
*/


require_once dirname(__FILE__). '/abstract.class.php';
class holiday extends baseClass{

    function editHoliday($holiday_id, $data){
        // edit holiday setting
        // $holiday_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($holiday_id == 0)
            $holiday_id = $this->DB->insert_db('tbl_holiday', null);
            
        $this->DB->updateMap("tmap_holiday_product", "holiday_id", $holiday_id, "product_id", $data['product_id_arr']);
        unset($data['product_id_arr']);

        $sql = $this->DB->buildSql_update("tbl_holiday", "holiday_id", $holiday_id, $data);
        $this->DB->update($sql);

        return $this->getHolidayDtl($holiday_id);
    }

    function getHolidayList($shop_id, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $sql = "SELECT * FROM (SELECT h.*, GROUP_CONCAT(m.product_id) as product_id_str
                FROM `tbl_holiday` AS h
                LEFT JOIN `tmap_holiday_product` AS m ON h.holiday_id = m.holiday_id AND m.`activate` = 'Y'
                WHERE h.`activate` ='Y' AND h.shop_id = '$shop_id' ";
        $afterSql = "";

        if ($filter){
            foreach($filter as $k => $i){
                if ($i || $i == '0'){
                    switch ($k){
                        case 'isCurrent':
                            if ($i == 'Y')
                                $sql .= " AND (end_date > NOW())";
                            break;
                        case 'product_id_list':
                            $afterSql .= " AND (";
                            foreach (explode(",", $i) as $k => $product_id){
                                if ($k != 0)
                                    $afterSql .= " OR";
                                $afterSql .= " product_id_str in ($product_id)";
                            }
                            $afterSql .= " OR product_id_str IS NULL )";
                            break;
                        case 'isDayoff':
                            $sql .= " AND (isDayoff = '$i'";
                            break;
                        default: 
                            break;
                    }                    
                }
            }
        }

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit']; 
        }

        $sql .= " GROUP BY h.holiday_id ) AS src";
        if ($afterSql != "");
            $sql .= " WHERE 1=1 " . $afterSql;

        $res = $this->DB->get_Sql($sql);

        foreach ($res as $k => $holi){
            if ($holi['product_id_str']){
                $sql = "SELECT COUNT(product_id) AS consumed
                        FROM `tbl_order_deli` AS d
                        WHERE d.activate = 'Y' AND product_id in (".$holi['product_id_str'].") AND DATE_FORMAT(DATE_ADD(create_datetime, INTERVAL 8 HOUR), '%Y-%m-%d') =  DATE_FORMAT(DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 8 HOUR), '%Y-%m-%d')";
                $tList = $this->DB->get_Sql($sql)[0];
                $res[$k]['consumed'] = $tList['consumed'];
                $res[$k]['product_id_arr'] = explode(",", $holi['product_id_str']);
            } else{
                $res[$k]['consumed'] = 0;
            }
        }
        return $res;
    }

    function getHolidayDtl($holiday_id){
        // get the holiday setting Detail
        // $holiday_id: PK

        $sql = "SELECT h.*, GROUP_CONCAT(m.product_id) AS product_id_arr
                FROM `tbl_holiday` AS h
                LEFT JOIN `tmap_holiday_product` AS m ON m.holiday_id = h.holiday_id AND m.activate ='Y'
                WHERE h.`activate` ='Y' AND h.holiday_id = '$holiday_id'";
        $res = $this->DB->get_Sql($sql)[0];

        $res['productList'] = explode(",", $res['product_id_arr']);
        // if ($res){
        //     $sql = "SELECT * 
        //             FROM `tmap_holiday_product` AS m
        //             WHERE m.activate ='Y' AND m.holiday_id = '$holiday_id'";
        //     $res['productList'] = $this->DB->get_Sql($sql);
        // }

        return $res;
    }
    
}
?>
