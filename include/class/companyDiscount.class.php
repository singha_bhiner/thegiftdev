<?php
/* 
    Objective: 公司註冊優惠 related 
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_company_discount_applicant
        tbl_company_discount

        
*/

require_once dirname(__FILE__). '/abstract.class.php';
class companyDiscount extends baseClass{

    function getCompanyDiscountApplicantList($discount_id){
        // get the ApplicantList 
        // $discount_id: FK, the PK of the tbl_company_discount, which which member is getting this company discount
        $sql = "SELECT * FROM `tbl_company_discount_applicant` WHERE activate = 'Y' AND discount_id = '$discount_id'";
        $res = $this->DB->get_Sql($sql);

        return $res;
    }

    function applyCompanyDiscount($discount, $member_id){
        // execute the companyDiscount, including logging and cash dollar gain
        // $discount: basically only the discount['discount_id'] is in use for the FK of the log
        // $member_id: which member applying the companyDiscount

        // log applicant
        $this->DB->insert_db('tbl_company_discount_applicant', ["discount_id" => $discount['discount_id'], "member_id" => $member_id]);
        
        // apply the cash dollar
        require_once ROOT.'/include/class/member.class.php';
        $memberCls = new member($this->DB, $this->lang, $this->isAdmin);
        $memberCls->cashDollarGain($discount['credit'], $member_id);
        return true;
    }

    function editCompanyDiscount($data){
        // edit CompanyDiscount
        // $discount_id: tbl_company_discount PK, which edit. Inserting a new row when 0
        // $data: the data of it 
        
        if (isset($data['discount_id']) && $data['discount_id'] != 0) {
            $discount_id = $this->DB->update_Sql("tbl_company_discount", "discount_id", $data);   
        } else {
            $discount_id = $this->DB->insert_db('tbl_company_discount', $data);
        }
            
        $this->DB->updateMap("tmap_company_discount_email", "discount_id", $discount_id, "email", $data['email_suffix']);
        unset($data['email_suffix']);

        return $this->getCompanyDiscountDtl($discount_id);
    }

    function getCompanyDiscountList($shop_id, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: which shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by


        $sql = "SELECT * FROM `tbl_company_discount` WHERE `shop_id` = '$shop_id' && `activate` ='Y'";

        foreach($filter AS $k => $i){
            if ($i){
                switch($k){
                    case 'skey':
                        $sql .= " AND (company_name LIKE '%$i%' OR email_suffix LIKE '%$i%') ";
                        break;
                }
            }
        }

        $sql .= " GROUP BY discount_id";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        return $res;
    }

    function getCompanyDiscountDtl($discount_id){
        // get the companyDisocunt Detail
        // $discount_id: PK
        
        $sql = "SELECT v.*, GROUP_CONCAT(cde.email) AS email_suffix
                FROM `tbl_company_discount` AS v
                LEFT JOIN `tmap_company_discount_email` AS cde ON v.discount_id = cde.discount_id AND cde.activate = 'Y'
                WHERE v.`activate` = 'Y' && v.`discount_id` = '$discount_id'";
        $res = $this->DB->get_Sql($sql)[0];
        return $res;
    }
}
?>
