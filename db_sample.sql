
-- first admin admin|password
INSERT INTO `tbl_admin` (`username`, `email`, `password`, `two_factor_code`, `last_login_on`, `last_login_ip`, `last_login_device`, `last_login_location`, `role_id`, `suspended`, `activate`) VALUES
('admin', NULL, '5f4dcc3b5aa765d61d8327deb882cf99', 'QQUZ3VZXVXFTLQA4', NULL, NULL, NULL, NULL, '1', 'N', 'Y'),
('gift', NULL, '5f4dcc3b5aa765d61d8327deb882cf99', 'QQUZ3VZXVXFTLQA4', NULL, NULL, NULL, NULL, '2', 'N', 'Y'),
('zuri', NULL, '5f4dcc3b5aa765d61d8327deb882cf99', 'QQUZ3VZXVXFTLQA4', NULL, NULL, NULL, NULL, '3', 'N', 'Y'),
('lotaipo', NULL, '5f4dcc3b5aa765d61d8327deb882cf99', 'QQUZ3VZXVXFTLQA4', NULL, NULL, NULL, NULL, '4', 'N', 'Y'),
('viewer', NULL, '5f4dcc3b5aa765d61d8327deb882cf99', 'QQUZ3VZXVXFTLQA4', NULL, NULL, NULL, NULL, '5', 'N', 'Y');

INSERT INTO `tbl_admin_role` (`adm_name`,`gift_right`, `zuri_right`, `ltp_right`) VALUES
('超級管理員', '{"dashboard":50,"sku":50,"item":50,"style":50,"shop_info":50,"nav":50,"page":50,"cate":50,"product":50,"addon":50,"combo_product":50,"normal_member":50,"company_member":50,"member":50,"voucher":50,"coupon_code":50,"company_discount":50,"point":50,"gift_card":50}', '{"dashboard":50,"sku":50,"item":50,"style":50,"shop_info":50,"nav":50,"page":50,"cate":50,"product":50,"addon":50,"combo_product":50,"normal_member":50,"company_member":50,"member":50,"voucher":50,"coupon_code":50,"company_discount":50,"point":50,"gift_card":50}', '{"dashboard":50,"sku":50,"item":50,"style":50,"shop_info":50,"nav":50,"page":50,"cate":50,"product":50,"addon":50,"combo_product":50,"normal_member":50,"company_member":50,"member":50,"voucher":50,"coupon_code":50,"company_discount":50,"point":50,"gift_card":50}'),
('The Gift 管理員', '{"dashboard":50,"sku":50,"item":50,"style":50,"shop_info":50,"nav":50,"page":50,"cate":50,"product":50,"addon":50,"combo_product":50,"normal_member":50,"company_member":50,"member":50,"voucher":50,"coupon_code":50,"company_discount":50,"point":50,"gift_card":50}', '{"dashboard":10,"sku":10,"item":10,"style":10,"shop_info":10,"nav":10,"page":10,"cate":10,"product":10,"addon":10,"combo_product":10,"normal_member":10,"company_member":10,"member":10,"voucher":10,"coupon_code":10,"company_discount":10,"point":10,"gift_card":10}','{"dashboard":10,"sku":10,"item":10,"style":10,"shop_info":10,"nav":10,"page":10,"cate":10,"product":10,"addon":10,"combo_product":10,"normal_member":10,"company_member":10,"member":10,"voucher":10,"coupon_code":10,"company_discount":10,"point":10,"gift_card":10}'),
('Little Zuri 管理員', '{"dashboard":10,"sku":10,"item":10,"style":10,"shop_info":10,"nav":10,"page":10,"cate":10,"product":10,"addon":10,"combo_product":10,"normal_member":10,"company_member":10,"member":10,"voucher":10,"coupon_code":10,"company_discount":10,"point":10,"gift_card":10}','{"dashboard":50,"sku":50,"item":50,"style":50,"shop_info":50,"nav":50,"page":50,"cate":50,"product":50,"addon":50,"combo_product":50,"normal_member":50,"company_member":50,"member":50,"voucher":50,"coupon_code":50,"company_discount":50,"point":50,"gift_card":50}','{"dashboard":10,"sku":10,"item":10,"style":10,"shop_info":10,"nav":10,"page":10,"cate":10,"product":10,"addon":10,"combo_product":10,"normal_member":10,"company_member":10,"member":10,"voucher":10,"coupon_code":10,"company_discount":10,"point":10,"gift_card":10}'),
('LoTaiPo 管理員', '{"dashboard":10,"sku":10,"item":10,"style":10,"shop_info":10,"nav":10,"page":10,"cate":10,"product":10,"addon":10,"combo_product":10,"normal_member":10,"company_member":10,"member":10,"voucher":10,"coupon_code":10,"company_discount":10,"point":10,"gift_card":10}','{"dashboard":10,"sku":10,"item":10,"style":10,"shop_info":10,"nav":10,"page":10,"cate":10,"product":10,"addon":10,"combo_product":10,"normal_member":10,"company_member":10,"member":10,"voucher":10,"coupon_code":10,"company_discount":10,"point":10,"gift_card":10}','{"dashboard":50,"sku":50,"item":50,"style":50,"shop_info":50,"nav":50,"page":50,"cate":50,"product":50,"addon":50,"combo_product":50,"normal_member":50,"company_member":50,"member":50,"voucher":50,"coupon_code":50,"company_discount":50,"point":50,"gift_card":50}'),
('觀察者','{"dashboard":30,"sku":30,"item":30,"style":30,"shop_info":30,"nav":30,"page":30,"cate":30,"product":30,"addon":30,"combo_product":30,"normal_member":30,"company_member":30,"member":30,"voucher":30,"coupon_code":30,"company_discount":30,"point":30,"gift_card":30}','{"dashboard":30,"sku":30,"item":30,"style":30,"shop_info":30,"nav":30,"page":30,"cate":30,"product":30,"addon":30,"combo_product":30,"normal_member":30,"company_member":30,"member":30,"voucher":30,"coupon_code":30,"company_discount":30,"point":30,"gift_card":30}','{"dashboard":30,"sku":30,"item":30,"style":30,"shop_info":30,"nav":30,"page":30,"cate":30,"product":30,"addon":30,"combo_product":30,"normal_member":30,"company_member":30,"member":30,"voucher":30,"coupon_code":30,"company_discount":30,"point":30,"gift_card":30}');

INSERT INTO `tbl_item` (`sku_prefix`, `name_tc`, `name_en`, `qty`) VALUES 
('apple', '蘋果', 'Apple', '30'),
('orange', '橙', 'orange', '50'),
('banana', '蕉', 'Banana', '100');

INSERT INTO `tbl_sku` (`item_id`, `sku_code`, `sku_val`, `default_price`, `qty`, `alert_qty`) VALUES
('1', 'size-place', 'apple-lg-fuji', '20', '5', '0'),
('1', 'size-place', 'apple-md-fuji', '10', '5', '0'),
('1', 'size-place', 'apple-sm-fuji', '5', '5', '0'),
('1', 'size-place', 'apple-sm-hk', '5', '5', '0'),
('1', 'size-color-place', 'apple-sm-red-hk', '10', '5', '0'),
('1', 'size-color', 'apple-sm-green', '10', '5', '0'),
('2', 'size', 'orange-lg', '30', '10', '20'),
('2', 'size', 'orange-md', '20', '5', '20'),
('3', 'size-color', 'banana-md-yellow', '30', '10', '20'),
('3', 'size-color', 'banana-sm-yellow', '20', '5', '20'),
('3', 'place', 'banana-jp', '30', '10', '20'),
('3', 'place', 'banana-cn', '20', '5', '20'),
('5', 'size', 'fengshui_pear-sm', '20', '5', '20'),
('5', 'size', 'fengshui_pear-md', '20', '5', '20'),
('6', 'size', 'pitaya-sm', '20', '5', '20'),
('6', 'size', 'pitaya-md', '20', '5', '20'),
('7', 'size', 'tiantao-sm', '20', '5', '20'),
('7', 'size', 'tiantao-md', '20', '5', '20'),
('8', 'size', 'cloth-sm', '20', '5', '20'),
('8', 'size', 'cloth-md', '20', '5', '20'),
('9', 'size', 'fragrant_pear-sm', '20', '5', '20'),
('9', 'size', 'fragrant_pear-md', '20', '5', '20'),
('10', 'size', 'blueberry-sm', '20', '5', '20'),
('11', 'place', 'honeydew-hk', '20', '5', '20'),
('11', 'place', 'honeydew-jp', '20', '5', '20'),
('12', 'size', 'godiva-sm', '20', '5', '20'),
('12', 'size', 'godiva-md', '20', '5', '20'),
('13', 'bland', 'champagne-moet', '20', '5', '20'),
('14', 'size', 'peninsula_mooncake-sm', '20', '5', '20'),
('14', 'size', 'peninsula_mooncake-md', '20', '5', '20'),
('15', 'size', 'rattan_baskket-sm', '20', '5', '20');

INSERT INTO `tbl_shop` (`adm_name`) VALUES
('The Gift'),
('Little Zuri'),
('LoTaiPo');

INSERT INTO `tbl_nav` (`shop_id`, `name_tc`, `name_en`, `link`) VALUES
('1', "全部商品", "Shop All", "product/productList"),
('1', "禮盤種類", "Hampers", null),
('1', "花藝禮品", "Floral", null),
('1', "外部連結", "Link", "http://hk.yahoo.com"),
('1', "常見問題", "FAQ", null);

INSERT INTO `tmap_cate_nav` (`nav_id`, `cate_id`) VALUES
(2,1),
(3,2),(3,3),(3,4),(3,5);
INSERT INTO `tmap_nav_page` (`nav_id`, `page_id`) VALUES
(5,1),(5,2),(5,3),(5,4),(5,5);

INSERT INTO `tbl_page` (`shop_id`, `url`, `removeable`, `adm_name`) VALUES
('1', '', 'N', '主頁'),
('1', 'about-us', 'N', '關於我們'),
('1', 'contact-us', 'N', '聯絡我們'),
('1', 'product/productList', 'N', '所有物品'),
('1', 'page1','Y', '新頁面1'),
('1', 'page2','Y', '新頁面2'),
('1', 'page3','Y', '新頁面3');

INSERT INTO `tmap_item_shop` (`item_id`, `shop_id`) VALUES
(1, 1),(1, 2),(1, 3),
(2, 1),(2, 2),
(3, 1);

INSERT INTO `tmap_cate_greeting` (`cate_id`, `greeting_id`) VALUES
(1, 1),(1, 2),(1, 6),
(2, 1),(2, 2),(2, 3),
(3, 4),(3, 5),(3, 6);

INSERT INTO `tmap_greeting_product` (`product_id`, `greeting_id`) VALUES
(1, 1),(1, 2),(1, 6),
(2, 1),(2, 2),(2, 3),
(3, 4),(3, 5),(3, 6);


INSERT INTO `tmap_addon_product` (`product_id`, `addon_id`) VALUES
(1, 1),(1, 2),(1, 3),
(2, 1),(2, 2),
(3, 3);





INSERT INTO `tbl_addon` (`shop_id`, `name_tc`, `name_en`) VALUES
(1, '酒類', 'Liquor'),
(1, '嬰兒用品', 'Baby products'),
(1, '朱古力', 'Choco');

INSERT INTO `tbl_product` ( `gift_hover_img`, `gift_product_code`, `gift_product_name_tc`, `gift_product_name_en`, `gift_cust_deli_act`, `gift_cust_deli_tc`, `gift_cust_deli_en`, `gift_cust_info_act`, `gift_cust_info_tc`, `gift_cust_info_en`, `gift_price`, `gift_sp_price`, `gift_onsale`, `gift_display_in_cate`, `zuri_product_code`, `zuri_product_name_tc`, `zuri_product_name_en`, `zuri_cust_deli_act`, `zuri_cust_deli_tc`, `zuri_cust_deli_en`, `zuri_cust_info_act`, `zuri_cust_info_tc`, `zuri_cust_info_en`, `zuri_price`, `zuri_sp_price`, `zuri_onsale`, `zuri_display_in_cate`, `ltp_product_code`, `ltp_product_name_tc`, `ltp_product_name_en`, `ltp_cust_deli_act`, `ltp_cust_deli_tc`, `ltp_cust_deli_en`, `ltp_cust_info_act`, `ltp_cust_info_tc`, `ltp_cust_info_en`, `ltp_price`, `ltp_sp_price`, `ltp_onsale`, `ltp_display_in_cate`, `gift_deli_id`, `zuri_deli_id`, `ltp_deli_id`) VALUES
('../upload/1/1628674186seaOtter4.jpeg', 'GIFT001', 'gift籃a','gift籃aEN','F', 'deli_hello', 'deli_hello_EN', 'N', 'gift_cust_info_tc', 'gift_cust_info_en', '500', '480', 'Y', 'Y', 'ZURI001', 'zuri_product_name_tc', 'zuri_product_name_en', 'Y', 'zuri_cust_deli_tc', 'zuri_cust_deli_en', 'Y', 'zuri_cust_info_tc', 'zuri_cust_info_en', '400', '380', 'Y', 'Y', 'LTP001', 'ltp_product_name_tc', 'ltp_product_name_en', 'F', 'ltp_cust_deli_tc', 'ltp_cust_deli_en', 'Y', 'ltp_cust_info_tc', 'ltp_cust_info_en', '450', '430', 'Y', 'Y',1,1,1),
('../upload/1/1628674186seaOtter4.jpeg', 'GIFT002', 'gift籃b','gift籃bEN','F', 'deli_hello', 'deli_hello_EN', 'N', 'gift_cust_info_tc', 'gift_cust_info_en', '500', '480', 'Y', 'Y', 'ZURI002', 'zuri_product_name_tc', 'zuri_product_name_en', 'Y', 'zuri_cust_deli_tc', 'zuri_cust_deli_en', 'Y', 'zuri_cust_info_tc', 'zuri_cust_info_en', '400', '380', 'Y', 'Y', 'LTP002', 'ltp_product_name_tc', 'ltp_product_name_en', 'F', 'ltp_cust_deli_tc', 'ltp_cust_deli_en', 'Y', 'ltp_cust_info_tc', 'ltp_cust_info_en', '450', '430', 'Y', 'Y',1,1,1),
('../upload/1/1628674186seaOtter4.jpeg', 'GIFT003', 'gift籃c','gift籃cEN','F', 'deli_hello', 'deli_hello_EN', 'N', 'gift_cust_info_tc', 'gift_cust_info_en', '500', '480', 'Y', 'Y', 'ZURI003', 'zuri_product_name_tc', 'zuri_product_name_en', 'Y', 'zuri_cust_deli_tc', 'zuri_cust_deli_en', 'Y', 'zuri_cust_info_tc', 'zuri_cust_info_en', '400', '380', 'Y', 'Y', 'LTP003', 'ltp_product_name_tc', 'ltp_product_name_en', 'F', 'ltp_cust_deli_tc', 'ltp_cust_deli_en', 'Y', 'ltp_cust_info_tc', 'ltp_cust_info_en', '450', '430', 'Y', 'Y',1,1,1),
('../upload/1/1628674186seaOtter4.jpeg', 'GIFT004', 'gift籃d','gift籃dEN','F', 'deli_hello', 'deli_hello_EN', 'N', 'gift_cust_info_tc', 'gift_cust_info_en', '500', '480', 'Y', 'Y', 'ZURI004', 'zuri_product_name_tc', 'zuri_product_name_en', 'Y', 'zuri_cust_deli_tc', 'zuri_cust_deli_en', 'Y', 'zuri_cust_info_tc', 'zuri_cust_info_en', '400', '380', 'Y', 'Y', 'LTP004', 'ltp_product_name_tc', 'ltp_product_name_en', 'F', 'ltp_cust_deli_tc', 'ltp_cust_deli_en', 'Y', 'ltp_cust_info_tc', 'ltp_cust_info_en', '450', '430', 'Y', 'Y',1,1,1),
('../upload/1/1628674186seaOtter4.jpeg', 'GIFT005', 'gift籃e','gift籃eEN','F', 'deli_hello', 'deli_hello_EN', 'N', 'gift_cust_info_tc', 'gift_cust_info_en', '500', '480', 'Y', 'Y', 'ZURI005', 'zuri_product_name_tc', 'zuri_product_name_en', 'Y', 'zuri_cust_deli_tc', 'zuri_cust_deli_en', 'Y', 'zuri_cust_info_tc', 'zuri_cust_info_en', '400', '380', 'Y', 'Y', 'LTP005', 'ltp_product_name_tc', 'ltp_product_name_en', 'F', 'ltp_cust_deli_tc', 'ltp_cust_deli_en', 'Y', 'ltp_cust_info_tc', 'ltp_cust_info_en', '450', '430', 'Y', 'Y',1,1,1),
('../upload/1/1628674186seaOtter4.jpeg', 'GIFT006', 'gift籃f','gift籃fEN','F', 'deli_hello', 'deli_hello_EN', 'N', 'gift_cust_info_tc', 'gift_cust_info_en', '500', '480', 'Y', 'Y', 'ZURI006', 'zuri_product_name_tc', 'zuri_product_name_en', 'Y', 'zuri_cust_deli_tc', 'zuri_cust_deli_en', 'Y', 'zuri_cust_info_tc', 'zuri_cust_info_en', '400', '380', 'Y', 'Y', 'LTP006', 'ltp_product_name_tc', 'ltp_product_name_en', 'F', 'ltp_cust_deli_tc', 'ltp_cust_deli_en', 'Y', 'ltp_cust_info_tc', 'ltp_cust_info_en', '450', '430', 'Y', 'Y',1,1,1);

INSERT INTO `tmap_cate_product` (`product_id`, `cate_id`) VALUES
(1,12),(1,13),(1,14),(1,24),(1,25),
(2,12),(2,13),(2,14),(2,15),
(3,20),
(4,20),(4,21),(4,22),
(4,20),(4,21),(4,22),(4,23),
(5,24),(5,25),(5,26),(5,27),(5,28),(5,29),(5,30),(5,31),
(6,12),(6,13),(6,14),(6,15);

INSERT INTO `tbl_cate` (`shop_id`, `parent_cate_id`, `name_tc`, `name_en`) VALUES
('1', null, "鮮果禮籃", "Fruit Hamper"),
('1', null, "美食禮籃", "tmp"),
('1', null, "嬰兒禮籃", "tmp"),
('1', null, "過大禮禮籃", "tmp"),
('1', null, "開張套餐優惠 鮮果禮籃", "tmp"),
('1', null, "慶賀花籃系列", "tmp"),
('1', null, "開張優惠套餐 花籃系列", "tmp"),
('1', null, "帛事花籃", "tmp"),
('1', null, "保鮮花系列", "tmp"),
('1', null, "鮮花系列", "tmp"),
('1', '1', "純鮮果禮籃", "Fresh Fruit Hamper"),
('1', '1', "鮮果禮籃配果汁", "Fruit Hamper with Beverages"),
('1', '1', "鮮果禮籃配美酒", "Fruit Hamper With Wine"),
('1', '1', "鮮果禮籃配美食", "Fruit Hamper With Gourmet"),
('1', '1', "至尊果籃", "Supreme Fruit Hamper"),
('1', '1', "果花優惠套餐", "Fruit &amp; Flower Combo"),
('1', '2', "純美食禮籃", "tmp"),
('1', '2', "美食禮籃配果汁", "tmp"),
('1', '2', "美食禮籃配美酒", "tmp"),
('1', '3', "男孩子", "tmp"),
('1', '3', "女孩子", "tmp"),
('1', '3', "中性", "tmp"),
('1', '3', "雙生兒", "tmp"),
('1', '5', "實惠慶賀花籃", "tmp"),
('1', '5', "高級慶賀花籃", "tmp"),
('1', '5', "花果優惠套餐", "tmp"),
('1', '6', "保鮮花束", "tmp"),
('1', '6', "保鮮花藝擺設A", "tmp"),
('1', '7', "鮮花花束", "tmp"),
('1', '7', "鮮花藝擺設", "tmp"),
('1', '7', "蘭花擺設", "tmp");

INSERT INTO `tbl_member` (`shop_id`, `member_no`, `email`, `mobile`, `method`, `verify`, `sales`, `remark`, `membership_id`, `company_name`, `cash_dollar`, `point`, `exp`) VALUES
(1, '0002817', 'wias@gmail.com', '62718271', 'google', 'N', 'Sing', 'nothing1', '1', null, '100', '100', '100'),
(1, '0002818', 'wiasa@gmail.com', '62711324', 'facebook', 'N', 'Sing', 'nothing2', '1', 'MICROSOFT', '50', '70', '100'),
(1, '0002819', 'wiasb@gmail.com', '62711234', 'N', 'N', 'Sing', 'nothing3', '1', null, '50', '70', '100');

INSERT INTO `tbl_membership` (`shop_id`, `name_tc`, `name_en`, `discount`, `upgrade_point`, `upgrade_to_membership_id`, `upgrade_period`) VALUES
(1, '普通會員', 'Basic Membership', '0', '100', '2', '6'),
(1, '白金會員', 'Platinum Membership', '5', '1000', '3', '6'),
(1, 'VIP', 'VIP Membership', '10', null, null, '6');

INSERT INTO `tbl_voucher` (`shop_id`,`name_tc`,`name_en`,`discount`,`type`,`minimum_order_amt`,`ava_day`,`expiry_date`) VALUES
(1, '迎新現金賞優惠', 'NEW COMER DISCOUNT', '20', 'P', 200, 30, null),
(1, '$100 現金券 (品味果籃)', '100$ DISCOUNT COUPON (HAMPERS)', '100', 'A', 200, 30, '2022-07-22'),
(1, '$100 現金券', '100$ DISCOUNT COUPON', '100', 'A', 200, 30, null);

INSERT INTO `tmap_cate_voucher` (`cate_id`, `voucher_id`) VALUES
(2, 1);

INSERT INTO `tbl_greeting` (`shop_id`, `name_tc`, `name_en`, `greeting_arr_tc`, `greeting_arr_en`) VALUES
(1,'父親節', 'Fathers Day', '[父親節快樂,父親快樂]', '[Happy Fathers Day]'),
(1,'母親節', 'Mothers Day', '[母親節快樂,母親快樂]', '[Happy Mothers Day]'),
(1,'testTC', 'testEN', '[test]', '[test2]'),
(1,'testTC2', 'testEN2', '[test]', '[test2]'),
(1,'testTC3', 'testEN3', '[test]', '[test2]'),
(1,'生日', 'Birthday', '[生日快樂,牛一快樂]', '[Happy Birthday Day]');

INSERT INTO `tmap_product_addon_cate` (`addon_cate_id`, `product_id`) VALUES
(1,2),(1,3),(1,4),
(2,5),(2,6);

INSERT INTO `tbl_product_addon_cate` (`shop_id`, `product_id`, `name_tc`, `name_en`, `sort`) VALUES
(1,1, "書", "Book",1),
(1,1, "衫", "Shrit",2),
(1,1, "凳", "Chair",3),
(1,1, "枱", "Table",4),
(1,1, "餅", "Biscuit",5);

INSERT INTO `tbl_product_img` (`product_id`, `shop_id`, `img`) VALUES 
('1', '1', '../upload/product/1/1628674131seaOtter2.jpeg'),
('1', '1', '../upload/product/1/1628674131seaOtter4.jpeg');

INSERT INTO `tbl_product_item_content` (`product_id`, `sku_id`, `qty`, `group_item`, `optional`) VALUES
(1, 1, 5, 1, 'Y'), (1, 2, 6, 1, 'Y'), (1, 3, 7, 1, 'Y'),
(1, 4, 5, 2, 'Y'), (1, 5, 5, 2, 'Y'),
(1, 7, 5, 3, 'N'), (1, 8, 6, 3, 'N');

INSERT INTO `tmap_cate_voucher` (`cate_id`, `voucher_id`) VALUES
(11,1),(12,1),(13,1),(14,1);

INSERT INTO `tbl_coupon` (`shop_id`, `adm_name`, `discount`, `type`, `minimum_order_amt`, `code`, `usage_limit`) VALUES
(1, 'FB coupon', 10, 'P', 100, 'fb_coupon', 100),
(1, 'IG coupon', 10, 'P', 100, 'fb_coupon', 100),
(1, '香港早晨', 10, 'A', 50, 'hk_morning', 50);

INSERT INTO `tmap_cate_coupon` (`cate_id`, `coupon_id`) VALUES
(11,1),(12,1),(13,1),(14,1),
(11,2),(12,2),(13,2),
(11,3),(12,3);

INSERT INTO `tbl_company_discount` (`shop_id`, `company_name`, `email_suffix`, `credit`, `quota`, `expiry_date`) VALUES
(1, 'HSBC', '@hsbc.com', 800, 30, '2022-07-22'),
(1, 'Bhiner Tech', '@bhiner-tech.com', 1000, 10, '2022-07-22');

INSERT INTO `tbl_gift` (`shop_id`, `name_tc`, `name_en`, `required_point`, `quota`) VALUES
(1, '百佳現金卷100$', 'Parknshop coupon $100', 100, 50),
(1, '惠康現金卷100$', 'Wellcome coupon $100', 100, 50);

INSERT INTO `tmap_new_member_voucher` (`shop_id`, `voucher_id`) VALUES
(1,1);

INSERT INTO `tbl_gift_card` (`shop_id`,`name_tc`, `name_en`, `credit`, `expiry_date`) VALUES
(1, '100$禮物卡', '100 HKD Gift Card', '100', '2022-07-22'),
(1, '150$禮物卡', '150 HKD Gift Card', '150', '2022-07-22');

INSERT INTO `tbl_gift_card_key` (`card_id`, `code`, `use_datetime`) VALUES
(1,'CtmW3H9Oku', '2021-05-01 12:56:34'), (1,'ZT9wUuliN0', null), (1,'Ubc3xSBA3A', null), (1,'UQl59BJK3j', null), (1,'cnx4dYoLQc', null), (1,'cwlNuncWZu', null), (1,'shB1L8REnq', null), (1,'ULrfxUjvFz', null), (1,'cbCX8QDZnT', null), 
(2,'BrNQIjFEYj', null), (2,'jZRSqx0StZ', null), (2,'wtk3if6aLY', null), (2,'nm3D4OcKK3', null), (2,'KG13WKNQak', null);

INSERT INTO `tbl_page_content` (`page_id`, `type`, `html_tc`, `html_en`, `lr_img_tc`, `lr_img_en`, `sort`) VALUES
(1, "slider", null, null, null, null, 2),
(1, "product_grp", null, null, null, null, 6),
(1, "left_img", "CONTENT AT RIGHT TC", "CONTENT AT RIGHT EN", "../upload/page_content/1/seaOtter.jpg", "../upload/page_content/1/seaOtter.jpg", 3),
(1, "right_img", "CONTENT AT LEFT TC", "CONTENT AT LEFT EN", "../upload/page_content/1/seaOtter.jpg", "../upload/page_content/1/seaOtter.jpg", 4),
(1, "pure_html", "CONTENT TC", "CONTETN EN", null, null, 5),
(1, "marquee", "MARQUEE CONTENT TC", "CONTENT EN", null, null, 1),
(2, "slider", null, null, null, null, 2);

INSERT INTO `tmap_content_product` (`content_id`, `product_id`, `sort`) VALUES
(2, 1, 1), (2, 2, 2), (2, 3, 3);

INSERT INTO `tmap_content_slider` (`content_id`, `img_tc`, `img_en`, `sort`) VALUES 
(1, "../upload/page_slider/1/seaOtter1.jpg", "../upload/page_slider/1/seaOtter1.jpg", 1),
(1, "../upload/page_slider/1/seaOtter2.jpg", "../upload/page_slider/1/seaOtter2.jpg", 2),
(1, "../upload/page_slider/1/seaOtter3.jpg", "../upload/page_slider/1/seaOtter3.jpg", 3),
(1, "../upload/page_slider/1/seaOtter4.jpg", "../upload/page_slider/1/seaOtter4.jpg", 4),
(7, "../upload/page_slider/2/seaOtter1.jpg", "../upload/page_slider/2/seaOtter1.jpg", 1),
(7, "../upload/page_slider/2/seaOtter2.jpg", "../upload/page_slider/2/seaOtter2.jpg", 2);

INSERT INTO `tbl_deli_timeslot` (`shop_id`, `adm_name`, `start_time`, `end_time`, `ava_b4_day`, `deli_fee`, `same_day_deli_fee`, `default`, `online_pay_same_day`) VALUES
(1, '0800-1300', '08:00:00', '13:00:00', 14, NULL, NULL, 'Y', 'N'),
(1, '1400-1800', '14:00:00', '18:00:00', 1, NULL, 100, 'Y', 'Y'),
(1, '1700-2000', '17:00:00', '20:00:00', 2, NULL, 100, 'N', 'N'),
(1, 'REMOTES AREAS', '08:00:00', '18:00:00', 14, NULL, NULL, 'N', 'N'),
(1, 'OUTLITING ISLANDS', '08:00:00', '18:00:00', 14, NULL, NULL, 'N', 'N'),
(1, 'TERMINAL', '08:00:00', '18:00:00', 14, NULL, NULL, 'N', 'N'),
(1, '08AM - 6PM (節日)', '08:00:00', '18:00:00', 14, NULL, NULL, 'N', 'N'),
(1, 'Specific Time 0800-0900', '08:00:00', '09:00:00', 14, 100, 200, 'Y', 'N'),
(1, 'Specific Time Offline', '08:00:00', '09:00:00', 14, 200, 200, 'Y', 'N'),
(1, 'Specific Time 0900-1000', '09:00:00', '10:00:00', 15, 100, 200, 'Y', 'N'),
(1, 'Specific Time Offline', '09:00:00', '10:00:00', 15, 200, 200, 'Y', 'N'),
(1, 'Specific Time 1000-1100', '10:00:00', '11:00:00', 16, 100, 200, 'Y', 'N'),
(1, 'Specific Time Offline', '10:00:00', '11:00:00', 16, 100, 200, 'Y', 'N'),
(1, 'Specific Time 1100-1200', '11:00:00', '12:00:00', 17, 100, 200, 'Y', 'N'),
(1, 'Specific Time Offline', '11:00:00', '12:00:00', 17, 100, 200, 'Y', 'N'),
(1, 'Specific Time 1200-1300', '12:00:00', '13:00:00', 18, 100, 200, 'Y', 'N'),
(1, 'Specific Time Offline', '12:00:00', '13:00:00', 18, 100, 200, 'Y', 'N'),
(1, 'Specific Time 1300-1400', '13:00:00', '14:00:00', 19, 100, 200, 'Y', 'N'),
(1, 'Specific Time Offline', '13:00:00', '14:00:00', 19, 100, 200, 'Y', 'N'),
(1, 'Specific Time 1400-1500', '14:00:00', '15:00:00', 3, 100, 200, 'Y', 'Y'),
(1, 'Specific Time Offline', '14:00:00', '15:00:00', 20, 100, 200, 'Y', 'N'),
(1, 'Specific Time 1500-1600', '15:00:00', '16:00:00', 3, 100, 200, 'Y', 'Y'),
(1, 'Specific Time Offline', '15:00:00', '16:00:00', 21, 100, 200, 'Y', 'N'),
(1, 'Specific Time 1600-1700', '16:00:00', '17:00:00', 3, 100, 200, 'Y', 'Y'),
(1, 'Specific Time Offline', '16:00:00', '17:00:00', 5, 100, 200, 'Y', 'N'),
(1, 'Specific Time 1700-1800', '17:00:00', '18:00:00', 3, 100, 200, 'Y', 'Y'),
(1, 'Specific Time Offline', '17:00:00', '18:00:00', 6, 100, 200, 'Y', 'N'),
(1, 'Specific Time 1800-1900', '18:00:00', '19:00:00', 3, 200, 200, 'Y', 'Y'),
(1, 'Specific Time Offline', '18:00:00', '19:00:00', 7, 100, 200, 'Y', 'N'),
(1, 'Specific Time 1900-2000', '19:00:00', '20:00:00', 3, 200, 300, 'Y', 'Y'),
(1, 'Specific Time Offline', '19:00:00', '20:00:00', 8, 200, 200, 'Y', 'N'),
(1, 'Specific Time 2000-2100', '20:00:00', '21:00:00', 3, 200, 300, 'Y', 'Y'),
(1, 'Specific Time Offline', '20:00:00', '21:00:00', 9, 200, 200, 'Y', 'N'),
(1, '08:00-15:00 (節日)', '08:00:00', '15:00:00', 14, NULL, NULL, 'N', 'N'),
(1, 'Outskirt', '08:00:00', '18:00:00', 14, NULL, NULL, 'N', 'N'),
(1, '0800-1800 (BABY HAMPER)', '08:00:00', '18:00:00', 14, NULL, NULL, 'Y', 'N'),
(1, '0800-1300 Flora Time Slot', '08:00:00', '13:00:00', 17, NULL, NULL, 'N', 'N'),
(1, 'Balloon Grand Opening', '08:00:00', '18:00:00', 62, NULL, NULL, 'N', 'N'),
(1, 'Specific Time 0700-0800', '07:00:00', '08:00:00', 13, 200, 200, 'Y', 'N'),
(1, '3天後送貨 氣球', '08:00:00', '18:00:00', 86, 350, 10000, 'N', 'N'),
(1, '澳門 / 中國', '08:00:00', '18:00:00', 15, NULL, NULL, 'N', 'N'),
(1, '順豐送貨時間 (免費)', '08:00:00', '18:00:00', 14, NULL, NULL, 'N', 'N'),
(1, 'Firday (For Monday)', '08:00:00', '13:00:00', 62, NULL, NULL, 'N', 'N'),
(1, 'Pickup', '09:00:00', '18:00:00', 15, NULL, NULL, 'N', 'N'),
(1, '順豐送貨時間 (收費)', '08:00:00', '18:00:00', 14, 50, NULL, 'N', 'N'),
(1, 'HKBNBG', '08:00:00', '18:00:00', 14, NULL, NULL, 'N', 'N');

INSERT INTO `tbl_deli_cust_grp` (`shop_id`, `adm_name`) VALUES
(1, 'FLORAL'),
(1, 'BABY HAMPER'),
(1, 'non online payment'),
(1, '3 天後送貨(氣球)'),
(1, '4-5層 尿片蛋糕送貨時段'),
(1, '合作公司 Delivery Rule'),
(1, '順豐速運'),
(1, '正常送貨時間'),
(1, '只限自取'),
(1, 'DX 火腿Delivery Rule (只適用於火腿)'),
(1, '特別產品'),
(1, '3天後送貨(product)'),
(1, '毛巾蛋糕 3天後送貨'),
(1, 'HKBNBG (只適用於百虹源)');

INSERT INTO `tmap_grp_product` (`grp_id`, `product_id`)VALUES
(1, 1),(1, 2),(1, 3),
(2, 1),(2, 3);

INSERT INTO `tbl_order_deli_item` (`deli_id`, `sku_id`, `qty`, `remarks`) VALUES
(2, 1, 5, null),
(2, 4, 5, null),
(2, 6, 5, null),
(2, 7, 6, null),
(2, 10, 2, '客人個別要求追加');

INSERT INTO `tbl_order` (`order_status`, `shop_id`,`order_no`,`member_id`,`sender_name`,`sender_email`,`sender_phone`,`remarks_from_client`,`remarks_external`,`final_price`,`shipping_fee`,`timeslot_fee`,`remarks_internal`,`excel_path`,`point_used`,`point_gain`,`create_datetime`,`last_update`) VALUES
('waitingPayment',1,'20180625-ZQALE',null,'kfaawkmfawke','wajnfkjern@gmail.com','98765432',null,null,0,0,0,null,null,0,0,'2018-06-25 10:15:02','2018-07-30 07:56:19'),
('waitingPayment',1,'20180627-B9Y6G',null,'will','will@indzz.com','23912442',null,null,330,0,0,null,null,0,0,'2018-06-26 16:07:07','2018-07-30 07:56:19'),
('waitingPayment',1,'20180627-E9VGW',2,'Mrs Chung','tcay1128@yahoo.com.hk','93221674',null,null,2056,0,0,null,null,0,0,'2018-06-27 03:28:08','2018-07-30 07:56:19'),
('waitingPayment',1,'20180627-TMTN3',2,'Mrs Chung','tcay1128@yahoo.com.hk','9221674',null,null,4325,0,0,null,null,0,0,'2018-06-27 03:51:21','2018-07-30 07:56:19'),
('waitingPayment',1,'20180627-D7NI5',2,'Mrs Chung','tcay1128@yahoo.com.hk','9221674',null,null,1099,0,200,null,null,0,0,'2018-06-27 04:03:35','2018-07-30 07:56:19'),
('waitingPayment',1,'20180627-SFOCT',2,'Mrs Chung','tcay1128@yahoo.com.hk','9221674',null,null,580,0,0,null,null,0,0,'2018-06-27 04:17:23','2018-07-30 07:56:19'),
('waitingPayment',1,'20180627-ZJMM2',null,'Ivan Chung','ieaj1128@yahoo.com.hk','65330334',null,null,1388,0,0,null,null,0,0,'2018-06-27 04:36:49','2018-07-30 07:56:19'),
('waitingPayment',1,'20180627-F5IYR',null,'Ivan Chung','ieaj1128@yahoo.com.hk','65330334',null,null,2098,0,0,null,null,0,0,'2018-06-27 06:45:05','2018-07-30 07:56:19'),
('waitingPayment',1,'20180627-EJVDN',3,'jen wong','wongyyjen@gmail.com','64344412',null,null,1068,0,0,null,null,0,0,'2018-06-27 09:34:52','2018-07-30 07:56:19'),
('waitingPayment',1,'20180627-G4A8F',3,'jen wong','wongyyjen@gmail.com','64344412','gdsdsyastayu',null,880,0,0,null,null,0,0,'2018-06-27 09:38:00','2018-07-30 07:56:19'),
('paid',1,'20180628-O9I2E',4,'Vienna Lee','kamekwan@hotmail.com','62722353','Hello',null,698,0,0,null,null,0,0,'2018-06-28 02:57:12','2018-07-30 07:56:19'),
('paid',1,'20180628-JECZI',null,'will','will@indzz.com','23912442',null,null,388,0,0,null,null,0,0,'2018-06-28 09:28:08','2018-07-30 07:56:19'),
('paid',1,'20180629-9Z8KA',null,'dmasd','nkjkj@gmail.com','88888888',null,null,388,0,0,null,null,0,0,'2018-06-29 05:29:53','2018-07-30 07:56:19'),
('paid',1,'180702-VKUUD',null,'Eva Mok','Evayu8924@gmail.com','62330696','Utciiiyyi',null,330,0,0,null,null,0,0,'2018-07-02 09:35:33','2018-07-30 07:56:19'),
('paid',1,'180704-5GOOG',null,'will','will@indzz.com','12345678',null,null,388,0,0,null,null,0,0,'2018-07-04 07:25:37','2018-07-30 07:56:19'),
('paid',1,'180705-RJHXA',null,'Jen Wong','wongyyjen@gmail.com','98765432',null,null,2156,0,0,null,null,0,0,'2018-07-05 09:06:40','2018-07-30 07:56:19'),
('paid',1,'180705-6RBD2',3,'Jen Wong','wongyyjen@gmail.com','98765432',null,null,2834,0,0,null,null,0,0,'2018-07-05 09:16:59','2018-07-30 07:56:19'),
('paid',1,'180705-IBLPK',3,'Jen Wong','wongyyjen@gmail.com','98765432',null,null,2834,0,0,null,null,0,0,'2018-07-05 09:20:19','2018-07-30 07:56:19'),
('paid',1,'180705-TNDHS',3,'Jen Wong','wongyyjen@gmail.com','98765432',null,null,2834,0,0,'1234567',null,0,0,'2018-07-05 09:21:58','2018-07-30 07:56:19'),
('paid',1,'180708-IPDIJ',null,'Frank Mok','frankmok80@gmail.com','59921981',null,null,1,0,0,null,null,0,0,'2018-07-08 07:15:27','2018-07-30 07:56:19'),
('paid',1,'180709-6C2KC',null,'eva','tcay1128@yahoo.com.hk','93221674',null,null,1088,0,0,null,null,0,0,'2018-07-09 03:46:17','2018-07-30 07:56:19'),
('paid',1,'180709-HKWLY',null,'will','will@indzz.com','23912442',null,null,1088,0,0,null,null,0,0,'2018-07-09 08:16:18','2018-07-30 07:56:19'),
('paid',1,'180709-W3M6I',null,'will','will@indzz.com','23912442',null,null,1088,0,0,null,null,0,0,'2018-07-09 08:17:38','2018-07-30 07:56:19'),
('paid',1,'180709-6LBHK',null,'will','will@indzz.com','23912442',null,null,1088,0,0,null,null,0,0,'2018-07-09 08:18:29','2018-07-30 07:56:19'),
('paid',1,'180710-CYITH',null,'test','test@gmail.com','11112222',null,null,1998,0,0,null,null,0,0,'2018-07-10 03:04:59','2018-07-30 07:56:19'),
('paid',1,'180710-BSXZY',null,'Eva Yeung','tcay1128@yahoo.com.hk','93221674',null,null,10000,1000,1000,'中秋大量訂單','/storage/orders/180710-BSXZY/bank_slips/JeR2oOcVDnvPfKdtlAdlsNgnggZFMitw3vNt8EKv.xlsx',0,0,'2018-07-10 07:51:48','2018-07-30 07:56:19'),
('paid',1,'180710-QMA0K',null,'vie','kamekwan@hotmail.com','62722353',null,null,388,0,0,null,null,0,0,'2018-07-10 08:06:25','2018-07-30 07:56:19'),
('paid',1,'180710-PEFZR',null,'vie','kamekwan@hotmail.com','62722353',null,null,388,0,0,null,null,0,0,'2018-07-10 08:08:20','2018-07-30 07:56:19'),
('deliProcess',1,'180710-IFFST',null,'vie','kamekwan@hotmail.com','62722353',null,null,918,0,0,null,null,0,0,'2018-07-10 08:19:21','2018-07-30 07:56:19'),
('deliProcess',1,'180711-7DXHN',null,'Frank','frank@jfkdjfj.com','12345678',null,null,330,0,200,null,null,0,0,'2018-07-11 02:33:55','2018-07-30 07:56:19'),
('deliProcess',1,'180711-KB4PM',10,'Frank','frank@jfkdjfj.com','12345678',null,null,1118,0,400,null,null,0,0,'2018-07-11 02:58:00','2018-07-30 07:56:19'),
('deliProcess',1,'180711-XQIO8',10,'Frank','frank@jfkdjfj.com','12345678',null,null,1118,0,400,null,null,0,0,'2018-07-11 03:25:36','2018-07-30 07:56:19'),
('deliProcess',1,'180711-FTQWC',null,'vienn','kamekwan@hotmail.com','62722353',null,null,388,0,0,null,null,0,0,'2018-07-11 09:44:45','2018-07-30 07:56:19'),
('deliProcess',1,'180711-M9RPA',null,'Test','ks.lau@indzz.com','92235607','Remark test',null,388,0,0,null,null,0,0,'2018-07-11 10:13:23','2018-07-30 07:56:19'),
('deliProcess',1,'180712-0UJXH',12,'jen','cs@ltpgift.com','12341234',null,null,660,0,0,null,null,0,0,'2018-07-12 03:55:32','2018-07-30 07:56:19'),
('deliProcess',1,'180712-RLAYD',null,'Frank','frankmok80@gmail.com','59921981',null,null,10,0,0,null,null,0,0,'2018-07-12 07:44:55','2018-07-30 07:56:19'),
('complete',1,'180712-QDA1C',null,'will','will@indzz.com','23912442',null,null,388,0,200,null,null,0,0,'2018-07-12 10:24:49','2018-07-30 07:56:19'),
('complete',1,'180714-CNWPN',1,'will','will@indzz.com','23912442',null,null,1576,1100,0,'testing',null,0,0,'2018-07-14 10:01:02','2018-07-30 07:56:19'),
('complete',1,'180715-XABT2',10,'Eva Yu','evayu8924@gmail.com','62330696',null,null,10,0,0,null,null,0,0,'2018-07-15 06:06:52','2018-07-30 07:56:19'),
('complete',1,'180715-74LBM',10,'Eva Yu','evayu8924@gmail.com','62330696',null,null,788,0,0,null,null,0,0,'2018-07-15 06:25:29','2018-07-30 07:56:19'),
('complete',1,'180716-4BYCI',null,'evayeung','tcay1128@yahoo.com.hk','93221674',null,null,1499,0,200,null,null,0,0,'2018-07-16 05:02:46','2018-07-30 07:56:19'),
('complete',1,'180716-DTAG0',null,'evayeung','tcay1128@yahoo.com.hk','93221674',null,null,1499,0,0,null,null,0,0,'2018-07-16 05:10:55','2018-07-30 07:56:19'),
('complete',1,'180716-2Y1QX',null,'jnkjbij','avjienvjear@gmail.com','12341234',null,null,388,0,0,null,null,0,0,'2018-07-16 05:37:31','2018-07-30 07:56:19');

INSERT INTO `tbl_deli_district` (`district_id`, `name_tc`, `name_en`, `shop_id`) VALUES
(1, '中西區', 'Cental And Western',1),
(2, '東區', 'Eastern District',1),
(3, '離島區', 'Island District',1),
(4, '九龍城區', 'Kowloon City',1),
(5, '葵青區', 'Kwai Tsing',1),
(6, '觀塘區', 'Kwun Tong',1),
(7, '北區', 'North District',1),
(8, '西貢區', 'Sai Kung',1),
(9, '深水埗區', 'Sham Shui Po',1),
(10, '沙田區', 'Shatin',1),
(11, '南區', 'Southern District',1),
(12, '大埔區', 'Tai Po',1),
(13, '荃灣區', 'Tsuen Wan',1),
(14, '屯門區', 'Tuen Mun',1),
(15, '灣仔區', 'Wan Chai',1),
(16, '黃大仙區', 'Wong Tai Sin',1),
(17, '油尖旺區', 'Yau Tsim Mong',1),
(18, '元朗區', 'Yuen Long',1);

INSERT INTO `tbl_deli_region` (`region_id`, `name_en`, `name_tc`, `district_id`) VALUES
(1, 'ABERDEEN', '香港仔', 11),
(2, 'ADMIRALTY', '金鐘', 15),
(3, 'AP LEI CHAU', '鴨脷洲', 11),
(4, 'CAUSEWAY BAY', '銅鑼灣', 15),
(5, 'CENTRAL', '中環', 1),
(6, 'CHAI WAN', '柴灣', 2),
(7, 'CHUNG HOM KOK', '舂坎角', 11),
(8, 'DEEP WATER BAY', '深水灣', 11),
(9, 'FORTRESS HILL', '炮台山', 2),
(10, 'HAPPY VALLEY', '跑馬地', 1),
(11, 'HENG FA CHUEN', '杏花邨', 2),
(12, 'KENNEDY TOWN', '堅尼地城', 1),
(13, 'LEI TUNG', '利東', 11),
(14, 'MID-LEVEL', '半山', 1),
(15, 'MIDDLE BAY', '中灣', 11),
(16, 'NORTH POINT', '北角', 2),
(17, 'POK FU LAM', '薄扶林', 11),
(18, 'QUARRY BAY', '鰂魚涌', 2),
(19, 'REPULSE BAY', '淺水灣', 11),
(20, 'SAI WAN', '西環', 1),
(21, 'SAI WAN HO', '西灣河', 1),
(22, 'SAI YING PUN', '西營盤', 1),
(23, 'SHAU KEI WAN', '筲箕灣', 2),
(24, 'SHEK O', '石澳', 11),
(25, 'SHEK PAI WAN', '石排灣', 11),
(26, 'SHEK TONG TSUI', '石塘咀', 1),
(27, 'SHEUNG WAN', '上環', 1),
(28, 'SHOUSON HILL', '壽臣山', 11),
(29, 'SIU SAI WAN', '小西灣', 2),
(30, 'STANLEY', '赤柱', 11),
(31, 'TAI HANG', '大坑', 15),
(32, 'TAI KOO', '太古', 2),
(33, 'TAI KOO SHING', '太古城', 2),
(34, 'TAI TAM', '大潭', 11),
(35, 'TAI TAM RESERVOIR', '大潭水庫', 11),
(36, 'THE PEAK', '山頂', 1),
(37, 'TIN HAU', '天后', 15),
(38, 'TIN WAN', '田灣', 11),
(39, 'WAH FU', '華富', 11),
(40, 'WAH KWAI', '華貴', 11),
(41, 'WAN CHAI', '灣仔', 15),
(42, 'WESTERN DISTRICT', '西區', 1),
(43, 'WONG CHUK HANG', '黃竹坑', 11),
(44, 'CHEK LAP KOK', '赤臘角', 3),
(45, 'CHEUNG CHAU', '長洲', 3),
(46, 'DISCOVERY BAY', '愉景灣', 3),
(47, 'LAMMA ISLAND', '南丫島', 3),
(48, 'LANTAU ISLAND', '大嶼山', 3),
(49, 'MA WAN', '馬灣', 3),
(50, 'OUTLYING ISLANDS', '其他離島', 3),
(51, 'PENG CHAU', '坪洲', 3),
(52, 'PO TOI ISLAND GROUP', '蒲台島', 3),
(53, 'PUI O', '貝澳', 3),
(54, 'SHEK KOK TSUI', '石角咀', 3),
(55, 'SOKO ISLANDS', '索罟群島', 3),
(56, 'TONG FUK', '塘福', 3),
(57, 'TUNG CHUNG', '東涌', 3),
(58, 'CHA KWO LING', '茶果嶺', 6),
(59, 'CHEUNG SHA WAN', '長沙灣', 9),
(60, 'CHOI HUNG', '彩虹', 16),
(61, 'CLEAR WATER BAY', '清水灣', 8),
(62, 'DIAMOND HILL', '鑽石山', 16),
(63, 'FUNG WONG', '鳳凰', 16),
(64, 'HO MAN TIN', '何文田', 4),
(65, 'HUNG HOM', '紅磡', 4),
(66, 'JORDAN', '佐敦', 17),
(67, 'KOWLOON BAY', '九龍灣', 6),
(68, 'KOWLOON CITY', '九龍城', 4),
(69, 'KOWLOON TONG', '九龍塘', 4),
(70, 'KWUN TONG', '觀塘', 6),
(71, 'LAI CHI KOK', '荔枝角', 9),
(72, 'LAM TIN', '藍田', 6),
(73, 'LOK FU', '樂富', 16),
(74, 'MEI FOO', '美孚', 9),
(75, 'MONG KOK', '旺角', 17),
(76, 'NGAU CHI WAN', '牛池灣', 16),
(77, 'NGAU TAU KOK', '牛頭角', 6),
(78, 'PRINCE EDWARD', '太子', 17),
(79, 'SAI KUNG', '西貢', 8),
(80, 'SAN PO KONG', '新蒲崗', 16),
(81, 'SHAM SHUI PO', '深水埗', 9),
(82, 'SHEK KIP MEI', '石硤尾', 9),
(83, 'SHEK KONG', '石崗', 18),
(84, 'TAI KOK TSUI', '大角咀', 17),
(85, 'TO KWA WAN', '土瓜灣', 4),
(86, 'TSEUNG KWAN O', '將軍澳', 8),
(87, 'TSIM SHA TSUI', '尖沙咀', 17),
(88, 'TSZ WAN SHAN', '慈雲山', 16),
(89, 'TUNG TAU', '東頭', 16),
(90, 'WANG TAU HOM', '橫頭磡', 16),
(91, 'WONG TAI SIN', '黃大仙', 16),
(92, 'YAU MA TEI', '油麻地', 17),
(93, 'YAU TONG', '油塘', 6),
(94, 'YAU YAT CHUEN', '又一村', 9),
(95, 'CHINESE UNIVERSITY', '中文大學', 10),
(96, 'CHINESE UNIVERSITY', '中文大學', 12),
(97, 'FANLING', '粉嶺', 7),
(98, 'FO TAN', '火炭', 10),
(99, 'HA TSUEN', '廈村', 18),
(100, 'KAM TIN', '錦田', 18),
(101, 'KWAI CHUNG', '葵涌', 5),
(102, 'KWAI FONG', '葵芳', 5),
(103, 'KWAI HING', '葵興', 5),
(104, 'KWAI SHING', '葵盛', 5),
(105, 'LAI KING', '荔景', 5),
(106, 'LAI YIU', '麗瑤', 5),
(107, 'LEI MUK SHUE', '梨木樹', 13),
(108, 'LUNG KWU TAN', '龍鼓灘', 14),
(109, 'MA ON SHAN', '馬鞍山', 10),
(110, 'ON YAM', '安蔭', 5),
(111, 'SHA TAU KOK', '沙頭角', 7),
(112, 'SHAM TSENG', '深井', 13),
(113, 'SHATIN', '沙田', 10),
(114, 'SHEK LEI', '石籬', 5),
(115, 'SHEK MUN', '石門', 10),
(116, 'SHEK WU HUI', '石湖墟', 7),
(117, 'SHEK YAM', '石蔭', 5),
(118, 'SHEUNG SHUI', '上水', 7),
(119, 'SIU LEK YUEN', '小瀝源', 10),
(120, 'TAI LAM CHUNG', '大欖涌', 14),
(121, 'TAI PO', '大埔', 12),
(122, 'TAI WAI', '大圍', 10),
(123, 'TAI WO HAU', '大窩口', 13),
(124, 'TIN SHUI WAI', '天水圍', 18),
(125, 'TSING LUNG TAU', '青龍頭', 13),
(126, 'TSING YI', '青衣', 5),
(127, 'TSUEN WAN', '荃灣', 13),
(128, 'TUEN MUN', '屯門', 14),
(129, 'YUEN LONG', '元朗', 18);

INSERT INTO `tbl_order_deli` (`shop_id`,`order_id`,`deli_status`,`product_id`,`recipient_name`,`recipient_phone`,`recipient_company`,`deli_district_id`,`deli_address`,`deli_date`,`deli_timeslot_id`,`card_type`,`card_head`,`card_body`,`card_sign`,`card_json`,`card_bg_img_path`,`card_img_path`,`product_price`,`shipping_fee`,`timeslot_fee`,`deli_before_photo_path`,`acknowledgement`,`create_datetime`,`last_update`,`addon_price`) VALUES 
(1,1,'completed',3,'mlnfejknc','98765432','knjwnfeaf',1,'anjfekjlvnksznaelfveava','2018-06-26',2,1,'Baby Kenzo!','Welcome of the world, You\’re so lucky to be born into such a wonderful family with a gorgeous mama and Papa, and a big brother that will protect you! Meet you soon!','Love, Auntie Vani and Uncle Cris',null,null,null,388,0,0,null,null,'2018-06-25 10:15:02','2018-06-25 10:15:02',0),
(1,2,'completed',2,'will','23912442','indzz',1,'abc','2018-06-29',2,1,null,null,null,null,null,null,330,0,0,'/storage/orders/20180627-B9Y6G/order_items/2//VQbLQovLKjSxvqABqAbIQKNFT6IsePbH5DirvJ1u.png',null,'2018-06-26 16:07:07','2018-07-04 07:30:48',0),
(1,3,'completed',1,'Eva Yu','62330696','Eva Yu',1,'石塘咀ABC','2018-06-28',1,2,null,null,null,'<p>&nbsp;</p><p><span class="text-huge" style=“font-family:\'Courier New\’, Courier, monospace;"><strong>Dear Eva</strong></span></p><p>&nbsp;</p><p style="text-align:center;"><span class="text-huge" style="font-family:\'Courier New\', Courier, monospace;"><strong>TEST</strong></span></p><p>&nbsp;</p><p style="text-align:right;"><span class="text-huge" style=“font-family:\'Courier New\’, Courier, monospace;"><strong>From Eva</strong></span></p>',null,null,388,0,0,null,null,'2018-06-27 03:28:08','2018-06-27 03:28:08',0),
(1,3,'completed',273,'Eva Yu','62330696','Eva Yu',1,'石塘咀ABC','2018-06-28',1,1,null,null,null,null,null,null,680,0,0,null,null,'2018-06-27 03:28:08','2018-06-27 03:28:08',0),
(1,3,'completed',282,'Eva Yu','62330696','Eva Yu',1,'石塘咀ABC','2018-06-28',1,3,null,null,null,'<p>&nbsp;</p><p>&nbsp;</p><p>Dear Eva</p><p>&nbsp;</p><p style="text-align:center;">Test</p><p>&nbsp;</p><p style="text-align:right;">From Eva</p>',null,null,988,0,200,null,null,'2018-06-27 03:28:08','2018-06-27 03:28:08',0),
(1,4,'completed',284,'Eva YU','62330696','Eva Yu',1,'ahfj','2018-07-01',2,2,'eva','test','eva',null,null,null,865,0,0,null,null,'2018-06-27 03:51:21','2018-06-27 03:51:21',0),
(1,4,'completed',284,'eva yu','62330696','evayu',1,'ahfj','2018-07-01',2,2,'eva','test','eva',null,null,null,865,0,0,null,null,'2018-06-27 03:51:21','2018-06-27 03:51:21',0),
(1,4,'acknowledged',284,'evayu','62330696','evayu',1,'ahfj','2018-07-01',2,3,'eva','test','eva',null,null,null,865,0,0,null,null,'2018-06-27 03:51:21','2018-06-27 03:51:21',0),
(1,4,'acknowledged',284,'frank','59921981','frank',1,'觀塘jsjdfb','2018-07-02',2,2,null,null,null,'<p>Frank</p><p>&nbsp;</p><p>Test</p><p>&nbsp;</p><p>From Eva</p>',null,null,865,0,0,null,null,'2018-06-27 03:51:21','2018-06-27 03:51:21',0),
(1,4,'acknowledged',284,'jenny','27398499','jenny',1,'TST gdhgjhj','2018-07-03',1,3,null,null,null,'<p>Jenny</p><p>&nbsp;</p><p style="text-align:center;">Test</p><p>&nbsp;</p><p style="text-align:right;">Eva</p>',null,null,865,0,0,null,null,'2018-06-27 03:51:21','2018-06-27 03:51:21',0),
(1,5,'acknowledged',293,'eva','62330696','eva',1,'jghu','2018-06-27',3,2,'eva','test','eva',null,null,null,1099,200,0,null,null,'2018-06-27 04:03:35','2018-06-27 04:03:35',0),
(1,6,'acknowledged',272,'eva','62330696','eva',1,'bjkfh','2018-06-28',3,1,'eva','test','eva',null,null,null,580,0,0,null,null,'2018-06-27 04:17:23','2018-06-27 04:17:23',0),
(1,7,'acknowledged',233,'eva','28988019','lotaipo',1,'sdjkkj','2018-06-28',1,3,null,null,null,null,null,null,1388,0,0,null,null,'2018-06-27 04:36:49','2018-06-27 04:36:49',0),
(1,8,'delivered',2,'eva','62330696','eva',1,'jgutjo','2018-06-28',1,2,null,null,null,null,null,null,330,0,0,null,null,'2018-06-27 06:45:05','2018-06-27 06:45:05',0),
(1,8,'delivered',133,'eva','62330696','eva',1,'jgutjo','2018-06-28',1,3,'eva','test','eva',null,null,null,1380,0,0,null,null,'2018-06-27 06:45:05','2018-06-27 06:45:05',0),
(1,8,'delivered',12,'eva','62330696','eva',1,'jgutjo','2018-06-28',1,1,'eva','test','eva',null,null,null,388,0,0,null,null,'2018-06-27 06:45:05','2018-06-27 06:45:05',0),
(1,9,'delivering',1,'jen','65331212','na',1,'lam tin','2018-06-28',1,1,'fjlf','djfsgawiegf','sdafuswr',null,null,null,388,0,0,null,null,'2018-06-27 09:34:52','2018-06-27 09:34:52',0),
(1,9,'delivering',273,'kjawdfln','66665555','jhdfgiawy',1,'tai po','2018-06-29',3,1,'hvwdjgwdv','ygvy','bjhkb',null,null,null,680,0,0,null,null,'2018-06-27 09:34:52','2018-06-27 09:34:52',0),
(1,10,'delivering',285,'hdsjfjkds','65432124','no',1,'tai po','2018-06-29',1,1,'dssd','dsd','ds',null,null,null,880,0,0,'/storage/orders/20180627-G4A8F/order_items/19//60ubCyvhY5nusSIEL8J8G0Z8Yv7ryBlUs9J5F5sh.jpeg',null,'2018-06-27 09:38:00','2018-07-05 09:29:57',0),
(1,11,'delivering',2,'jen jen','64344412','N/A',1,'tai po','2018-06-29',1,2,'dfg','dfg','dfg',null,null,null,330,0,0,'/storage/orders/20180628-O9I2E/order_items/20//VduReELfTGqm1j2yPhk6M0F3Pe4K4TDsEYSWELGq.jpeg',null,'2018-06-28 02:57:12','2018-07-04 07:47:07',0),
(1,11,'delivering',6,'jjj','52541253','mmm',1,'lam tin','2018-06-30',2,1,'dsf','sdfs','sdf',null,null,null,368,0,0,'/storage/orders/20180628-O9I2E/order_items/21//b6rUrh03R0P6Fsb4pDy4D2vyBjiBT8jOxLMPhGhZ.jpeg',null,'2018-06-28 02:57:12','2018-07-04 07:47:19',0),
(1,12,'delivering',1,'will','23912442','indzz',1,'adadsa','2018-06-29',1,1,null,null,null,null,null,null,388,0,0,'/storage/orders/20180628-JECZI/order_items/22//0rhVEwoTZpDIYURrIqIVF3eH9R3EEgTeeFj1b10x.jpeg',null,'2018-06-28 09:28:08','2018-07-04 07:46:12',0),
(1,13,'delivering',1,'cdscac','11111111','dwcwcvw',1,'asfvmefv','2018-07-07',1,1,null,null,null,null,null,null,388,0,0,null,null,'2018-06-29 05:29:53','2018-06-29 05:29:53',0),
(1,14,'delivering',2,'Frank Mok','59921981',null,128,'Fyiviyviyguvi','2018-07-03',2,3,'Gcyufiyfkyg','Utcyifyi','Hfxtudtud',null,null,null,330,0,0,'/storage/orders/180702-VKUUD/order_items/24//0nolP18J89ohbFkKXCIfkgaURMRrMAgj9QNGTueW.png',null,'2018-07-02 09:35:33','2018-07-14 10:01:53',0),
(1,15,'delivering',1,'will','23912442','indzz',128,'testing','2018-07-26',1,4,null,null,null,null,null,null,388,0,0,'/storage/orders/180704-5GOOG/order_items/25//QpjZ5yHMd91L7xZXhZvBPHKOJfPMdgOWktLPr7BU.jpeg',null,'2018-07-04 07:25:37','2018-07-04 07:38:33',0),
(1,16,'delivering',282,'Peter Chan','12345678','lol',128,'qwert qwweiopk asdfgh','2018-07-07',1,4,'MARY','HAPPY BIRTHDAY','LEE',null,null,null,988,0,0,null,null,'2018-07-05 09:06:40','2018-07-05 09:06:40',0),
(1,16,'delivering',303,'Mary Lee','76548907','lol',100,'mnbv hjkl oiuter','2018-07-13',2,3,'Dear Mary',null,'LHHJGH',null,null,null,1168,0,0,null,null,'2018-07-05 09:06:40','2018-07-05 09:06:40',0),
(1,17,'packed',303,'Mary Lee','76548907','lol',100,'mnbv hjkl oiuter','2018-07-13',2,3,'Dear Mary','AWESOME!','LHHJGH',null,null,null,1168,0,300,null,null,'2018-07-05 09:16:59','2018-07-05 09:16:59',0),
(1,17,'packed',4,'SUSAN','65431234',null,141,'TESJRDYMGJKDKYTDSK','2018-07-13',3,4,null,null,null,null,null,null,380,0,0,null,null,'2018-07-05 09:16:59','2018-07-05 09:16:59',0),
(1,17,'packed',271,'FXKYFJGV.','87654321',null,130,'GRSEARHEDSJ','2018-07-13',1,4,null,null,null,null,null,null,550,0,0,null,null,'2018-07-05 09:16:59','2018-07-05 09:16:59',0),
(1,17,'packed',12,'JOHN','12345432','HDBJAWKFJHEW',160,'FEWKJHfbdewhjbdfehjw','2018-07-13',2,4,null,null,null,null,null,null,388,0,0,null,null,'2018-07-05 09:16:59','2018-07-05 09:16:59',0),
(1,17,'packed',269,'GEORGE','67896789',null,153,'FCJYFC','2018-07-13',2,4,null,null,null,null,null,null,348,0,0,null,null,'2018-07-05 09:16:59','2018-07-05 09:16:59',0),
(1,18,'packed',303,'Mary Lee','76548907','lol',100,'mnbv hjkl oiuter','2018-07-13',2,3,'Dear Mary','AWESOME!','LHHJGH',null,null,null,1168,0,0,null,null,'2018-07-05 09:20:19','2018-07-05 09:20:19',0),
(1,18,'packed',4,'SUSAN','65431234',null,141,'TESJRDYMGJKDKYTDSK','2018-07-13',3,4,null,null,null,null,null,null,380,0,0,null,null,'2018-07-05 09:20:19','2018-07-05 09:20:19',0),
(1,18,'packed',271,'FXKYFJGV.','87654321',null,130,'GRSEARHEDSJ','2018-07-13',1,4,null,null,null,null,null,null,550,0,0,null,null,'2018-07-05 09:20:19','2018-07-05 09:20:19',0),
(1,18,'packed',12,'JOHN','12345432','HDBJAWKFJHEW',160,'FEWKJHfbdewhjbdfehjw','2018-07-13',2,4,null,null,null,null,null,null,388,0,0,null,null,'2018-07-05 09:20:19','2018-07-05 09:20:19',0),
(1,18,'packed',269,'GEORGE','67896789',null,153,'FCJYFC','2018-07-13',2,4,null,null,null,null,null,null,348,0,0,null,null,'2018-07-05 09:20:19','2018-07-05 09:20:19',0),
(1,19,'packed',303,'Mary Lee','76548907','lol',100,'mnbv hjkl oiuter','2018-07-13',2,3,'Dear Mary','AWESOME!','LHHJGH',null,null,null,1168,0,0,null,null,'2018-07-05 09:21:58','2018-07-05 09:21:58',0),
(1,19,'packed',4,'SUSAN','65431234',null,141,'TESJRDYMGJKDKYTDSK','2018-07-13',3,4,null,null,null,null,null,null,380,0,0,null,null,'2018-07-05 09:21:58','2018-07-05 09:21:58',0),
(1,19,'packed',271,'FXKYFJGV.','87654321',null,130,'GRSEARHEDSJ','2018-07-13',1,4,null,null,null,null,null,null,550,0,0,null,null,'2018-07-05 09:21:58','2018-07-05 09:21:58',0),
(1,19,'packed',12,'JOHN','12345432','HDBJAWKFJHEW',160,'FEWKJHfbdewhjbdfehjw','2018-07-13',2,4,null,null,null,null,null,null,388,0,0,null,null,'2018-07-05 09:21:58','2018-07-05 09:21:58',0),
(1,19,'packed',269,'GEORGE','67896789',null,153,'FCJYFC','2018-07-13',2,4,null,null,null,null,null,null,348,0,0,null,null,'2018-07-05 09:21:59','2018-07-05 09:21:59',0),
(1,20,'packed',284,'Eva Yu','62330690','LOTAIPO',128,'Flat H, 12/F High Win Factory Building','2018-07-09',1,2,'Eva Yu','生意定會滾滾來、財源肯定不間斷。','Frank Mok',null,null,null,1,0,0,null,null,'2018-07-08 07:15:27','2018-07-08 07:15:27',0),
(1,21,'packing',202,'eva','62330696','eva',149,'hdhueghr','2018-07-11',2,4,null,null,null,null,null,null,1088,0,0,null,null,'2018-07-09 03:46:17','2018-07-16 07:57:58',0),
(1,22,'packing',198,'will','23912442','indzz',128,'abc','2018-07-11',4,4,null,null,null,null,null,null,1088,0,0,null,null,'2018-07-09 08:16:18','2018-07-09 08:16:18',0),
(1,23,'packing',198,'will','23912442','indzz',128,'abc','2018-07-11',4,4,null,null,null,null,null,null,1088,0,0,null,null,'2018-07-09 08:17:38','2018-07-09 08:17:38',0),
(1,null,'incart',198,'will','23912442','indzz',128,'abc','2018-07-11',4,4,null,null,null,null,null,null,1088,0,0,null,null,'2018-07-09 08:18:29','2018-07-09 08:18:29',0),
(1,null,'incart',249,'kndjafjw','98765431',null,130,'JNDKJDNW','2018-07-13',4,1,'njnkjknkjn','huihnjknkjmn','jknjjn',null,null,null,1610,0,0,null,null,'2018-07-10 03:04:59','2018-07-10 03:04:59',0),
(1,null,'incart',1,'kndjafjw','98765431',null,130,'JNDKJDNW','2018-07-13',4,2,'jnkkkjnk','hblhnmkb','jknoinpi',null,null,null,388,0,0,null,null,'2018-07-10 03:04:59','2018-07-10 03:04:59',0),
(1,null,'incart',1,'vur','90987654','tyh',104,'dfdfddtetgr','2018-07-11',2,4,null,null,null,null,null,null,388,0,0,null,null,'2018-07-10 08:06:25','2018-07-10 08:06:25',0);



-------------- phase 2 ---------------

-- INSERT INTO `tbl_sku_key_val` (`key`, `val`) VALUES 
-- ('size', 'lg'),
-- ('size', 'md'),
-- ('size', 'sm'),
-- ('place', 'fuji'),
-- ('place', 'hk'),
-- ('place', 'jp'),
-- ('place', 'cn'),
-- ('color', 'red'),
-- ('color', 'yellow'),
-- ('color', 'green');







