<?php
class translateObject {

	private $g = ''; // get parent $this object e.g. DB setting
	
	public function __construct($g='') {
		$this->g = $g;
	}

	function amazonTranslate($string,$currentLanguage='zh',$targetLang='en'){
		require_once __DIR__.'/translateObjectClass/awsTranslateObject.php';
		$awsTranslateObject = new awsTranslateObject;
		$translated_text = $awsTranslateObject->translate($string,$currentLanguage,$targetLang);
		return $translated_text;
	}

	function isChineseWords($input){
		$input_check = preg_match("/(\p{Han}|\p{Katakana}|\p{Hiragana})+/u", $input);
		return $input_check;
	}
}
?>