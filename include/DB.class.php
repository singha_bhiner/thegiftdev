<?php
class DB{
	public function __construct($xthis) {
		$this->gthis = $xthis;
		require_once dirname(__FILE__).'/config.php';

		$this->getUnique = [
			"page" => 1,
			"limit" => 1
		];
	}

	function get_Sql($query){
		$result = $this->gthis->DB()->get_Sql($query);
		// $this->gthis->DB()->insert_db('tbl_sql_audit_log', $query);
		return $result;
	}

	function get_One($query){
		$result = $this->gthis->DB()->get_One($query);
		// $this->gthis->DB()->insert_db('tbl_sql_audit_log', $query);
		return $result;
	}

	function update($query){
		$result = $this->gthis->DB()->update($query);
		// $this->gthis->DB()->insert_db('tbl_sql_audit_log', $query);
		return $result;
	}

	function get_Sql_Mode($sql, $mode, $option){
		$tArr = $this->get_Sql($sql);

		$res = [];
		switch ($mode){
			case 'singleColumn':
				// option [col => val]
				foreach ($tArr as $k => $i){
					array_push($res, $i[$option['col']]);
				}
				break;
		}
		
		return $res;
	}

	function insert_db($tbl,$data = null){
		if (!$data){
			$data =[];
		}
		// re-constructure the $data
		$sql = "SELECT COLUMN_NAME 
				FROM INFORMATION_SCHEMA.COLUMNS 
				WHERE TABLE_NAME = '$tbl' 
					AND COLUMN_KEY NOT LIKE '%PRI%'";
	
		$colArr = $this->get_Sql_Mode($sql, "singleColumn", ["col"=> "COLUMN_NAME"]);

		if (!array_key_exists("last_update", $data)){
			$data["last_update"] = "NOW()";
		}

		if (!array_key_exists("create_datetime", $data)){
			$data["create_datetime"] = "NOW()";
		}

		if (!array_key_exists("updated_by_id", $data)){
			$data["updated_by_id"] = $_SESSION['admin']['admin_id'];
		}

		$insertData = [];
        foreach($colArr as $key => $col){
			if (isset($data[$col])){
				$insertData[$col] = $data[$col];
			}
        }

		$id = $this->gthis->DB()->insert_db($tbl, $insertData);
		$this->auditLog($tbl, null, "add", $id, null, null, $_SESSION['admin']['admin_id'], null);
		return $id;
		// return $id;
	}

	function auditLog($tbl, $fld, $act, $rowId, $org = null, $updated, $updatedBy, $rowKey = null){
		if ($fld == "updated_by_id" || $fld == "last_update" || $fld == "create_datetime" || ($fld == $rowKey && $act != "add")){

		} else {
			if (!$org && $rowKey){
				$sql = "SELECT `$fld` from `$tbl` WHERE $rowKey = '$rowId'";
				$tr = $this->get_Sql($sql)[0];
				$org = $tr[$fld];
			} 
			if ($org != $updated || $act == "add"){

				$this->gthis->DB()->insert_db(
					'tbl_sql_audit_log',
					[
						"table" => $tbl,
						"field" => $fld,
						"row_id" => $rowId,
						"action" => $act,
						"original_value" => $org,
						"updated_value" => $updated,
						"updated_by_id" => $updatedBy,
					]
				);
			}
		}
	}

	function buildSql_update($tbl, $rowKey, $rowID, $data){
		$sql = "SELECT COLUMN_NAME 
				FROM INFORMATION_SCHEMA.COLUMNS 
				WHERE TABLE_NAME = '$tbl' 
					AND COLUMN_KEY NOT LIKE '%PRI%' 
					AND TABLE_SCHEMA = '".O_DB."'
					AND COLUMN_NAME NOT IN ('$rowKey')";
		$colArr = $this->get_Sql_Mode($sql, "singleColumn", ["col"=> "COLUMN_NAME"]);

		if (!array_key_exists("last_update_time", $data)){
			$data["last_update_time"] = "NOW()";
		}

		if (!array_key_exists("last_update_by", $data)){
			$data["last_update_by"] = $this->gthis->member_id;
		}

		$pid = $data[$rowKey];
		unset($data[$rowKey]);


		$sql = "UPDATE `$tbl` SET ";
        
		if (!array_key_exists("last_update", $data)){
			$data["last_update"] = "NOW()";
		}

		// if (!array_key_exists("create_datetime", $data)){
		// 	$data["create_datetime"] = "NOW()";
		// }

		if (!array_key_exists("updated_by_id", $data)){
			$data["updated_by_id"] = $_SESSION['admin']['admin_id'];
		}

        $updateFieldArr = [];
        foreach($colArr as $key => $col){
			if (isset($data[$col])){
				$val = $data[$col];
				if ($val && ($val == "NOW()" || $val == "CURRENT_TIMESTAMP()")){
					array_push($updateFieldArr, "`$col` = $val");
				} else if ($val || $val == 0){
					$val = addslashes($val);
					array_push($updateFieldArr, "`$col` = '$val' ");
				} else {
					array_push($updateFieldArr, "`$col` = null ");
				}
			}
        }
        $sql .= implode(',', $updateFieldArr);

        $sql .= " WHERE `$rowKey` = '$rowID'";
		return $sql;
	}

	function parseInt($val){
		if ($val == "NULL"){
			return "NULL";
		}
		
		$val = preg_replace("/[^0-9\.]/", "", $val );
		
		if ($val == ''){
			return null;
		}
		return intval($val);
	}

	function parseTimestamp($val){
		$arr = explode(" ", $val);
		if (sizeof($arr) < 2)
			return null;

		if ($this->parseDate($arr[0]) == null)
			return null;

		if ($this->parseTime($arr[1]) == null)
			return null;

		return $val;
	}

	function parseDate($val){
		if ($val == "NULL")
			return $val;
			
		$arr = explode("-", $val);

		if (sizeof($arr) != 3)
			return null;

		if (!checkdate($arr[1],$arr[2],$arr[0]))
			return null;

		return $val;
	}

	function parseTime($val){
		$arr = explode(":", $val);

		if (sizeof($arr) == 2)
			array_push($arr, "00");

		if (sizeof($arr) != 3)	
			return null;
	
		foreach ($arr as $t){
			$t = floatval($t);
			if ($t <0 || $t > 60)
				return null;
		}

		return $val;
	}

	function parseNY($val){
		if($val == 'Y')
			return 'Y';
		else if ($val == 'N')
			return 'N';
		else 
			return null;
	}

	function parseIntArr($val){
		$tr = explode(",", $val);
		$res = [];
		foreach($tr as $val){
			if ($val && $val !=''){
				array_push($res, floatval($val));
			}
		}
		return implode(",", $res);
	}

	function parseIntArrV1($val){
		$val = ltrim($val, '[');
		$val = rtrim($val, ']');
		$tr = explode(",", $val);
		$res = [];
		foreach($tr as $val){
			if (!is_null($val) && $val != '')
				array_push($res, floatval($val));
		}
		return $res;
	}

	function parseJson($val){
		json_decode($val);
   		if (json_last_error() === JSON_ERROR_NONE)
		   	return $val;
		else 
			return  null;

	}

	function uploadExcel($file, $folder, $id){
		$target_dir = "upload/$folder";
		$imageFileType = strtolower(pathinfo($target_dir . "/".basename($file["name"]),PATHINFO_EXTENSION));
		mkdir(ROOT."/".$target_dir . "/" .$id);
		$target_file = $target_dir . "/" .$id ."/".time(). $file['name'];
		
		// Check file size
		if ($file["size"] > 5000000) {
			new apiDataBuilder(-91, null, null, $this->lang);
		}
		
		// Allow certain file formats
		if($imageFileType != "xls" && $imageFileType != "xlsx") {
			new apiDataBuilder(-90, null, $imageFileType, $this->lang);
		}
		
		// Check if $uploadOk is set to 0 by an error

		if (move_uploaded_file($file["tmp_name"], ROOT."/".$target_file)) {
		} else {   
			new apiDataBuilder(-999, null, null, $this->lang);
		}

		return $target_file;
	}

	function uploadImgs($files, $folder){
		$res =[];

		if ($files){
			$target_dir = "upload/$folder";
			foreach($files['error'] as $k=>$v)
			{
				$name = $files['name'][$k];

				$imageFileType = strtolower(pathinfo($target_dir . "/".basename($name),PATHINFO_EXTENSION));
				mkdir(ROOT."/".$target_dir);
				$target_file = $target_dir ."/".time(). $name;


				// Check file size
				if ($files["size"][$k] > 5000000) {
					new apiDataBuilder(-91, null, null, $this->lang);
				}
				
				// Allow certain file formats
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
					new apiDataBuilder(-90, null, $imageFileType, $this->lang);
				}


				if (move_uploaded_file($files["tmp_name"][$k], ROOT."/".$target_file)) {
					array_push($res, $target_file);
				} else {
					array_push($res, $name . " upload failure");
					// new apiDataBuilder(-999, null, null, $this->lang);
				}



				// $uploadfile = 'uploads/'. basename($_FILES['userfile']['name'][$k]);
				// if (move_uploaded_file($_FILES['userfile']['tmp_name'][$k], $uploadfile)) 
				// {
				// 	echo "File : ", $_FILES['userfile']['name'][$k] ," is valid, and was successfully uploaded.\n";
				// } else {
				// 	echo "Possible file : ", $_FILES['userfile']['name'][$k], " upload attack!\n";
				// }   

			}
		}

		return $res;
		
		
			
		// 	// Check if $uploadOk is set to 0 by an error
	
		// 	if (move_uploaded_file($img["tmp_name"], ROOT."/".$target_file)) {
		// 	} else {   
		// 		new apiDataBuilder(-999, null, null, $this->lang);
		// 	}
		// 	return $target_file;
		// } else {
		// 	return null;
		// }

	}

	function uploadImg($img, $folder, $id){
		if ($img && $img['name']){
			$target_dir = "upload/$folder";
			$imageFileType = strtolower(pathinfo($target_dir . "/".basename($img["name"]),PATHINFO_EXTENSION));
			mkdir(ROOT."/".$target_dir . "/" .$id);
			$target_file = $target_dir . "/" .$id ."/".time(). $img['name'];
						
			// Check file size
			if ($img["size"] > 5000000) {
				new apiDataBuilder(-91, null, null, $this->lang);
			}
			
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
				new apiDataBuilder(-90, null, $imageFileType, $this->lang);
			}
			
			// Check if $uploadOk is set to 0 by an error
	
			if (move_uploaded_file($img["tmp_name"], ROOT."/".$target_file)) {
			} else {   
				new apiDataBuilder(-999, null, null, $this->lang);
			}
			return $target_file;
		} else {
			return null;
		}

	}

	function update_Sql($tbl, $primayKey, $data , $whereClause = null){
		// David Jan 02 20222 : whereClause should be a object with key value pair 
		// select table scheme without PRIMARY KEY
		$sql = "SELECT COLUMN_NAME 
				FROM INFORMATION_SCHEMA.COLUMNS 
				WHERE TABLE_NAME = '$tbl' 
					AND COLUMN_KEY NOT LIKE '%PRI%' 
					AND TABLE_SCHEMA = '".O_DB."'
					AND COLUMN_NAME NOT IN ('$primayKey')";
		$colArr = $this->get_Sql_Mode($sql, "singleColumn", ["col"=> "COLUMN_NAME"]);
		
		if (!array_key_exists("last_update", $data)){
			$data["last_update"] = "NOW()";
		}

		if (!array_key_exists("updated_by_id", $data)){
			$data["updated_by_id"] = $_SESSION['admin']['admin_id'];
		}

		$pid = $data[$primayKey];
		unset($data[$primayKey]);

		$sql = "UPDATE `$tbl` SET ";
        
        $updateFieldArr = [];
        foreach($colArr as $key => $col){
			if (isset($data[$col])){
				$val = $data[$col];
				if ($val && ($val == "NOW()" || $val == "CURRENT_TIMESTAMP()" || $val == "NULL")){
					array_push($updateFieldArr, "`$col` = $val");
				} else if ($val || $val == 0){
					$val = addslashes($val);
					array_push($updateFieldArr, "`$col` = '$val' ");
				} else {
					array_push($updateFieldArr, "`$col` = null ");
				}

				$this->auditLog($tbl, $col, "updated", $pid, null, $val, $_SESSION['admin']['admin_id'], $primayKey);
			}
        }

        $sql .= implode(',', $updateFieldArr);

        $sql .= " WHERE `$primayKey` = '$pid'";

        if($whereClause){
        	foreach($whereClause as $k=>$i){
        		$sql .= "AND `$k` = '$i' ";
        	}
        }

		$this->update($sql);
		
		return $pid;
	}

	function updateForeignMap($tbl, $foreign_key, $foreign_id, $primary_key, $dataList){
		$sql = "UPDATE `$tbl` set activate = 'N' WHERE `$foreign_key` = '$foreign_id'";
		$this->update($sql);
		
		$primary_key_id_arr =  [];

		foreach ($dataList as $k => $data){
			$data['activate'] = 'Y';
			$data[$foreign_key] = $foreign_id;
			
			$sql = "SELECT `$primary_key` 
					FROM `$tbl` 
					WHERE `$foreign_key` = '$foreign_id' AND `activate` ='N' LIMIT 1";
			$existingRow = $this->get_Sql($sql)[0];

			if ($existingRow){
				$data[$primary_key] = $existingRow[$primary_key];
				$id = $this->update_Sql($tbl, $primary_key, $data);
			} else {
				$data[$primary_key] = 0;
				$id = $this->insert_db($tbl, $data);
			}

			array_push($primary_key_id_arr, $id);
		}

		return $primary_key_id_arr;
	}


	function updateDataMapByRefId($tbl, $base_key, $base_id, $ref_key, $dataList){
		foreach ($dataList as $k => $data){
			$ref_id = $data[$ref_key];
			$sql = "SELECT * FROM $tbl WHERE $base_key = $base_id AND $ref_key = $ref_id AND activate = 'Y'";
			$map = $this->get_Sql($sql)[0];
			
			if ($map){
				$map_id = $map['map_id'];
				$dataList[$k]['map_id'] = $map_id;
			} else {
				$dataList[$k]['map_id'] = 0;
			}
		}
		$this->updateDataMap($tbl, $base_key, $base_id, $ref_key, $dataList);
	}

	function updateDataMap($tbl, $base_key, $base_id, $ref_key, $dataList){
		$shop_id = null;
		$id_arr = []; // for "remove" use
		foreach ($dataList as $k => $data){
			$map_id = $data['map_id'];
			if ($map_id == 0){
				$map_id = $this->insert_db($tbl, null);
				$data['map_id'] = $map_id;
			}
			array_push($id_arr, $data['map_id']);
			unset($data['map_id']);
			$data['activate'] = 'Y';
			
			if ($data['shop_id'])
				$shop_id = $data['shop_id'];

			$sql = $this->buildSql_update($tbl, "map_id", $map_id, $data);
			$this->update($sql);
		}
		// remove the removed items (switch activate = 'N') if the item not in the dataList
		$sql = "UPDATE `$tbl` SET `activate` ='N', updated_by_id = '".$_SESSION['admin']['admin_id']."' WHERE $base_key = '$base_id'  ";
		if ($id_arr && implode(',', $id_arr) != ""){
			$sql .= " AND map_id NOT IN ('".implode("','", $id_arr)."')";
		}

		if ($shop_id){
			$sql .= " AND shop_id = '$shop_id'";
		}
		$this->update($sql);
	}

	function updateMap($tbl, $base_key, $base_id, $ref_key, $ref_id_arrstr){
		if (is_array($ref_id_arrstr)){
			$arr = $ref_id_arrstr;
		} else {
			$arr = array_filter(explode(",", $ref_id_arrstr));
		}

		

		$sql = "UPDATE `$tbl` SET `activate` ='N', updated_by_id = '".$_SESSION['admin']['admin_id']."' WHERE $base_key = '$base_id'  ";
		$this->update($sql);
		
		$sql = "SELECT * FROM `$tbl` WHERE `activate` = 'N' AND $base_key = '$base_id'";
		$rowList = $this->get_Sql($sql);

		foreach($arr as $ref_id){
			$ref_id = ltrim($ref_id, "'");
			$ref_id = rtrim($ref_id, "'");

			if ($rowList[0]){
				// update
				$row = $rowList[0];
				$sql = "UPDATE `$tbl` SET `activate` = 'Y', $ref_key = '$ref_id' WHERE map_id = '".$row['map_id']."'";
				array_shift($rowList);
				$this->update($sql);
			} else {
				// insert
				$this->insert_DB($tbl, [
					$base_key => $base_id,
					$ref_key => $ref_id
				]);
			}

		}
	}

	// function updateMap($tbl, $base_key, $base_id, $ref_key, $ref_id_arrstr){
	// 	if (is_array($ref_id_arrstr)){
	// 		$arr = $ref_id_arrstr;
	// 	} else {
	// 		$arr = array_filter(explode(",", $ref_id_arrstr));
	// 	}

	// 	$sql = "UPDATE `$tbl` SET `activate` ='N', updated_by_id = '".$_SESSION['admin']['admin_id']."' WHERE $base_key = '$base_id'  ";
	// 	if ($arr && implode(',', $arr) != ""){
	// 		$sql .= " AND $ref_key NOT IN ('".implode("','", $arr)."')";
	// 	}
	// 	$this->auditLog($tbl, 'activate', "map_updated", "NOT IN ".implode(",", $arr) ,'Y' , 'N', $_SESSION['admin']['admin_id'], null);
	// 	$this->update($sql);

	// 	try {
	// 		$sql = "SELECT $ref_key FROM `$tbl` WHERE `activate` = 'Y' AND $base_key = '$base_id'";
	// 		$tmp_arr = $this->get_Sql($sql);
	// 	} catch (Exception $e) {
			
	// 	}
	// 	$ref_key_arr = [];

	// 	foreach ($tmp_arr as $i){
	// 		array_push($ref_key_arr, $i[$ref_key]);
	// 	}

	// 	foreach($arr as $ref_id){			
	// 		if (array_search($ref_id, $ref_key_arr) === false){
	// 			$data = [
	// 				$base_key => $base_id,
	// 				$ref_key => $ref_id, 
	// 			];
	// 			$tid = $this->insert_db($tbl, $data);
	// 			$this->auditLog($tbl, $base_key, "updated", $tid, null, $base_id, $_SESSION['admin']['admin_id'], null);
	// 			$this->auditLog($tbl, $ref_key, "updated", $tid, null, $ref_id, $_SESSION['admin']['admin_id'], null);
	// 		}
	// 	}
	// }

	function sortMap($tbl, $base_key, $base_id, $ref_key, $ref_id_arrstr){
		$ta = explode(",", $ref_id_arrstr);

		foreach ($ta as $k => $ref_id){
			$sql = "UPDATE `$tbl` SET `sort` = '".($k+1)."', updated_by_id = '".$_SESSION['admin']['admin_id']. "' WHERE activate = 'Y' AND $base_key = '$base_id' AND $ref_key = '$ref_id'";
			$this->update($sql);
		}
	}

	function sort($tbl, $rowKey, $target_id_arr){
		$ta = explode(",", $target_id_arr);

		foreach ($ta as $k => $target_id){
			if ($target_id){
				$sql = $this->buildSql_update($tbl, $rowKey, $target_id, ["sort" => ($k+1)]);
				$this->update($sql);
			}
		}
	}

	function parseEmail($val){
		if(!filter_var($val, FILTER_VALIDATE_EMAIL)) {
			return null;
		} else {
			return $val;
		}
	}

	function parsePassword($val){
		if(strlen($val) < 8) {
			new apiDataBuilder(-94, null, null, $this->lang);
		} else {
			return password_hash($val, PASSWORD_BCRYPT);
		}
	}
	
	function parseArr($val){
		$val = ltrim($val, '[');
		$val = rtrim($val, ']');
		$tr = str_getcsv($val, ",", "\"", "\\");
		$res = [];
		foreach($tr as $val){
			$val = ltrim($val, ' ');
			$val = rtrim($val, ' ');

			if ($val && $val !=''){
				array_push($res, $val);
			}
		}
		return $res;
	}

	function parseArrV1($val){
		$val = ltrim($val, '[');
		$val = rtrim($val, ']');
		$tr = str_getcsv($val, ",", "\"", "\\");
		
		$res = [];
		foreach($tr as $val){
			if ($val && $val !=''){
				array_push($res, "'" . $val . "'");
			}
		}
		return implode(",", $res);
	}

	function parseFloat($val){
		$val = preg_replace("/[^0-9\.]/", "", $val );
		try {
			return floatval($val);
		} catch(Exception $e) {
			return null;
		}
	}

	function adjustFloatVal($tbl, $ref_key, $ref_id, $column, $adjustVal){
        // function for int/float value adjustment
		// $tbl: table, which is adjusting
        // $ref_key: PK of $tbl
        // $ref_id: PK value 
        // $column: column, which is adjusting 
		// $adjustVal: [float] the value of adjustment
		
		$sql = "UPDATE `$tbl` 
				SET `$column` = `$column` + $adjustVal ,
				updated_by_id = '".$_SESSION['admin']['admin_id']."'
				WHERE `$ref_key` = $ref_id";


        $this->update($sql);
    }
}
?>