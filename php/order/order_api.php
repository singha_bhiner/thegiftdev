<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->DB = $g_DB;

		require_once ROOT. '/include/apiDataBuilder.class.php';
		
		switch ($this->gthis->thirdmode) {
			case 'uploadDepositSlip':
				$this->uploadDepositSlip();
				break;
			case 'uploadExcel':
				$this->uploadExcel();
				break;
			default:
				new apiDataBuilder(-84);
				break;
		}

		$result_array['lang'] = $this->lang;
		@$this->gthis->assign('result_array',$result_array);
	}

	function uploadDepositSlip(){
		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);
		require_once ROOT.'/include/class/shop.class.php';
		$shopCls = new shop($this->DB, $this->gthis->lang);

        $shop_id = $this->gthis->parm_sql('request', 'shop_id', 'Y');
        $order_no = $this->gthis->parm_sql('request', 'order_no', 'Y');
        $deposit_slip = $this->gthis->parm_in('file', 'deposit_slip', 'Y');
		$phone = $this->gthis->parm_sql('request', 'phone');
		$submitter_name = $this->gthis->parm_sql('request', 'submitter_name');


		$data = [
            "shop_id" => $shop_id,
            "order_no" => $order_no,
            "deposit_slip" => $deposit_slip,
			"submitter_name" => $submitter_name,
			"phone" => $phone
        ];

		$res = $orderCls->editDepositSlip(0, $data);

		// special handling for auto accept
		$shop = $shopCls->getShopDtl($shop_id);

		//compare
		if (date("H:i:s") > $shop['auto_accept_start_time'] || date("H:i:s") < $shop['auto_accept_end_time']){
			//Auto Accept
			$order = $orderCls->getOrderDtlByOrderNo($shop_id, $order_no); 
			$orderCls->editOrder($order['order_id'], [
				"order_status" => "autoAccept"
			]);

			foreach($order['deliList'] as $k => $deli){
				$deliData = [
					"deli_status" => "autoAccept"
				];
				$orderCls->editDeli($deli['deli_id'], $deliData);
			}
	
		}

		new apiDataBuilder(0, $res);
	}

	function uploadExcel(){
		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);

        $shop_id = $this->gthis->parm_sql('request', 'shop_id', 'Y');
        $excel_path = $this->gthis->parm_in('file', 'excel_path', 'Y');

		$name = $this->gthis->parm_sql('request', 'name');
		$phone = $this->gthis->parm_sql('request', 'phone');
		$email = $this->gthis->parm_sql('request', 'email');

		$data = [
            "shop_id" => $shop_id,
            "excel_path" => $excel_path,
			"name" => $name,
			"phone" => $phone,
			"email" => $email
        ];

		$res = $orderCls->editExcel(0, $data);

		new apiDataBuilder(0, $res);
	}

}
?>