<?php	
require_once '../include/baseModel.class.php';

class beditable extends baseModel{	

	public function __construct(){
		parent::__construct('');
		$this->init();	
	}

	public function init(){
		$this->mode=urlencode($this->mode);	
		
		switch($this->mode){
			case 'field_update':
				$this->field_update();
				break;
			default :
				die('');
				break;
		}
	}

	function field_update(){
		switch ($_REQUEST['ftype']) {
			default:
				$tbl = "tbl_" . $_REQUEST['ftype'];
				$record_field = $_REQUEST['ftype'] . "_id";
				$admin_only ='Y';
				break;
		}
		if($tbl){
			if($admin_only=='Y'){
				if($_SESSION['admin']){
					$_REQUEST['value'] = urldecode($_REQUEST['value']);
					$query = "update `$tbl` set `".$this->escape_string($_REQUEST['field'])."` = '".$this->escape_string($_REQUEST['value'])."' where `$record_field` = '".$this->escape_string($_REQUEST['record_id'])."';";
					$this->DB->update($query);	

					$result['code'] = '0';
					$result['msg'] = 'Save and updated';
					$result['query'] = $query;
					$result['req'] = $_REQUEST;
					$this->json_return('result',$result,'Y');	
				}
			}
		}

		$result['code'] = '-1';
		$this->json_return('result',$result,'Y');
	}

	public function display(){
		
	}
}
$HOME = new beditable;
$HOME->display();

?>