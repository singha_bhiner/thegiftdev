<?php
/* 
    Objective: coupon code related 
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_item_country
        tbl_item_country_tag
        tmp_country_tag
        tbl_item_country_contact


        
*/
require_once dirname(__FILE__). '/abstract.class.php';
class itemCountry extends baseClass{

    function editCountry($data){
        if ($data['country_id'] == 0){
            $country_id = $this->DB->insert_db('tbl_item_country', $data);
        } else {
            $country_id = $this->DB->update_Sql("tbl_item_country", "country_id", $data);
        }
        
        return $this->getCountryDtl($country_id);
    }

    function getCountryList($filter = null, $limiter = null, $sorting = ["sort" => "sort", "order" => "ASC"]){
        $sql = "SELECT * FROM `tbl_item_country` WHERE `activate` ='Y'";

        if ($filter){
            foreach($filter as $k => $i){
                if ($i || $i == '0'){
                    switch ($k){
                        case 'country_id':
                            $sql .= " AND (country_id = '$i')";
                            break;
                        
                        default: 
                            break;
                    }                    
                }
            }
        }

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        return $res;
    }

    function getCountryDtl($country_id){ 
        if ($country_id)
            return $this->dispatchCountry($this->getCountryList(['country_id' => $country_id])[0]);
        else 
            return null;
    }

    function dispatchCountry($country){
        return $country;
    }
}
?>