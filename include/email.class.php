<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class email {
	private $DB;
	private $gthis;

	public function __construct($xthis,$DB) {
		$this->gthis = $xthis;
		$this->DB = $DB;
		require_once dirname(__FILE__).'/config.php';
	}

	function obfuscate_email($email){
	    $em   = explode("@",$email);
	    $name = implode('@',array_slice($em, 0, count($em)-1));
	    $len  = floor(strlen($name)/2);

	    return substr($name,0, $len) . str_repeat('*', $len) . "@" . end($em);   
	}


	function sendPickUpConfimrationEmail($member,$order_id,$resent='null'){
		// 發送取件確認信
		$email = (PERSONAL_DATA_ENCRYPT ? $this->gthis->dataDecrypt($member['email']) : $member['email']);
		$member_name = empty($member['member_name']) ? $this->obfuscate_email($email) : $member['member_name'];
		$user_lang = $member['user_lang'];
		$email = $member['email'];
		$member_id = $member['member_id'];

		require_once ROOT.'/include/langMap/returnMessage.php';
		$RMG = new returnMessage($user_lang);

		$title = $RMG->getReturnCode('10571');
		if($resent=='Y'){
			$title = '('.$RMG->getReturnCode('10575').') '.$RMG->getReturnCode('10571');
		}
		$title = $title.'OR'.$order_id;

		if (isset($user_lang)) {
			$this->gthis->lang = $user_lang;
		}

		require_once dirname(__FILE__).'/../include/order.app.class.php';
		$orderObject = new order($this->gthis,$this->DB);
		$order = $orderObject->memberGetOrder($member_id,$order_id);

		$ship_label_path = $order['ship_label_path'];
		$invoice_path = $order['invoice_path'];
		$shipping_provider = $order['shipping_provider'];
		$counting_weight = $order['counting_weight'];
		$contains_battery = $order['contains_battery'];
		
		$is_document = 'N';
		foreach($order['items'] as $k=>$i){
			if($i['item_type']=='document'){
				$is_document = 'Y';
				if($shipping_provider=='aramexhk' && $counting_weight>0.5){
					$is_document = 'N';	
				} 
			}
		}

		if(!$invoice_path){
			$opts = [
			    "http" => [
			        "method" => "GET",
			        "header" => "Accept-language: en\r\n"
			            
			    ]
			];
			$context = stream_context_create($opts);
			// generate pdf from system templates
			$invoice_html = file_get_contents($this->gthis->baseURL."/commercialInvoice/".base64_encode($this->gthis->dataEncrypt($order['order_id'])),false,$context);
			$filename = time().'.pdf';
			require_once dirname(__FILE__).'/../include/mpdf/vendor/autoload.php';
			$mpdf = new \Mpdf\Mpdf();
			$mpdf->WriteHTML($invoice_html);
			$mpdf->Output(dirname(__FILE__)."/../upload/order/".$order['order_id']."/".$filename, \Mpdf\Output\Destination::FILE);
			
			$path = "../upload/order/".$order['order_id']."/".$filename;
			$order_id = $order['order_id'];
			$query = "select `invoice_path` from `app_tbl_orders` where `order_id` = '".$this->gthis->escape_string($order_id)."'";
			$invoice_path = $this->DB->get_One($query);

			if(file_exists($invoice_path)){
				unlink($invoice_path);
			}

			$query = "update `app_tbl_orders` set `invoice_path` = '".$this->gthis->escape_string($path)."' where `order_id` = '".$this->gthis->escape_string($order_id)."';";
			$this->DB->update($query);

			$invoice_path = $path;
		}
		if($is_document=='Y'){
			if(strpos($ship_label_path, '.gif')>-1){
				$attachments = array($ship_label_path.'^^'.'Shipping Label.gif');		
			} else {
				$attachments = array($ship_label_path.'^^'.'Shipping Label.pdf');	
			}
		} else {
			// check $ship_label_path count
			$ship_label_path_array = explode(',',$ship_label_path);
			$attachments = array();
			foreach($ship_label_path_array as $k=>$i){
				if(strpos($i, '.gif')>-1){
					array_push($attachments,$i.'^^'.'Shipping Label-'.($k+1).'.gif');
				} else {
					array_push($attachments,$i.'^^'.'Shipping Label-'.($k+1).'.pdf');	
				}
			}
			array_push($attachments, $invoice_path.'^^'.'Invoice.pdf');
		}

		$need3481 = 'N';
		$need3091 = 'N';

		if($contains_battery=='Y' && $is_document !='Y'){
			//
		}
		if($is_document!='Y'){
			// UN 標簽
			// 檢查種賞
			foreach($order['items'] as $k=>$i){
				foreach($i['declares'] as $m=>$n){
					if(strpos(strtolower($n['content_dtl']),'un3481')>-1){
						$need3481 = 'Y';
					}
					if(strpos(strtolower($n['content_dtl']),'un3091')>-1){
						$need3091 = 'Y';
					}
				}
			}	
		}

		if($need3481=='Y'){
			array_push($attachments, '../img/un3481.png'.'^^'.'UN3481(Color Printing Required).png');
			$contains_battery = 'Y';
		}
		if($need3091=='Y'){
			array_push($attachments, '../img/un3091.jpg'.'^^'.'UN3091(Color Printing Required).png');
			$contains_battery = 'Y';
		}
		
		$attachments = implode('||', $attachments);

		$email_content = file_get_contents($this->gthis->baseURL.'/email/email.php?mode=pickUpConfirmationLetter&order_id='.$order_id.'&member_id='.$member_id.'&is_document='.$is_document."&language_set=".$this->gthis->lang."&contains_battery=$contains_battery&need3481=$need3481&need3091=$need3091");
		$email_id = $this->DB->insert_db('tbl_email_sender', array(
			'email_id' => '0',
			'to_email' => $email,
			'title'=> $title,
			'content'=>$email_content,
			'attachments'=>$attachments,
			'register_on'=>$this->gthis->get_current_time(),
			'status'=>'O'
		));
		return $email_id;
	}

	function sendOrderConfimrationEmail($member,$order_id){
		// 發送訂單確認信
		$email = (PERSONAL_DATA_ENCRYPT ? $this->gthis->dataDecrypt($member['email']) : $member['email']);
		$member_name = empty($member['member_name']) ? $this->obfuscate_email($email) : $member['member_name'];
		$user_lang = $member['user_lang'];
		$email = $member['email'];
		$member_id = $member['member_id'];

		require_once ROOT.'/include/langMap/returnMessage.php';
		$RMG = new returnMessage($user_lang);

		$title = $RMG->getReturnCode('10585').' OR'.$order_id;

		require_once dirname(__FILE__).'/../include/order.app.class.php';
		$orderObject = new order($this->gthis,$this->DB);
		$order = $orderObject->memberGetOrder($member_id,$order_id);

		$email_content = file_get_contents($this->gthis->baseURL.'/email/email.php?mode=orderConfirmation&order_id='.$order_id.'&member_id='.$member_id."&language_set=".$user_lang);
		$email_id = $this->DB->insert_db('tbl_email_sender', array(
			'email_id' => '0',
			'to_email' => $email,
			'title'=> $title,
			'content'=>$email_content,
			//'attachments'=>$order['ship_label_path'],
			'register_on'=>$this->gthis->get_current_time(),
			'status'=>'O'
		));
		return $email_id;
	}

	function sendOrderInvoice($member,$order_id){
		// 訂單收據
		$email = (PERSONAL_DATA_ENCRYPT ? $this->gthis->dataDecrypt($member['email']) : $member['email']);
		$member_name = empty($member['member_name']) ? $this->obfuscate_email($email) : $member['member_name'];
		$user_lang = $member['user_lang'];
		$email = $member['email'];
		$member_id = $member['member_id'];

		require_once ROOT.'/include/langMap/returnMessage.php';
		$RMG = new returnMessage($user_lang);
		$title = $RMG->getReturnCode('10519');

		require_once dirname(__FILE__).'/../include/order.app.class.php';
		$orderObject = new order($this->gthis,$this->DB);
		$order = $orderObject->memberGetOrder($member_id,$order_id);

		$email_content = file_get_contents($this->gthis->baseURL.'/email/email.php?mode=orderInvoice&order_id='.$order_id.'&member_id='.$member_id."&language_set=".$user_lang);
		$email_id = $this->DB->insert_db('tbl_email_sender', array(
			'email_id' => '0',
			'to_email' => $email,
			'title'=> $title,
			'content'=>$email_content,
			//'attachments'=>$order['ship_label_path'],
			'register_on'=>$this->gthis->get_current_time(),
			'status'=>'O'
		));
		return $email_id;
	}
	
	function sendMemberVerificationEmail($member,$spare=null){
		// 用session 防止不斷發電郵
		if(empty($_SESSION['last_send_time'])){
			$_SESSION['last_send_time'] = $this->gthis->get_current_time();	
		} 
		$minDiff = $this->gthis->dateDifference($this->gthis->get_current_time(),$_SESSION['last_send_time'],'min');
		if($minDiff->invert == 1 && $minDiff->i < 1){ // only can resend after one min
			return null;
		}
		$_SESSION['last_send_time'] = $this->gthis->get_current_time();	
		$pins = $member['validate_token'];
		if ($spare == 'spare') {
			$member['email'] = $member['spare_email'];
			$pins = $member['spare_validate_token'];
		}
		$email = (PERSONAL_DATA_ENCRYPT ? $this->gthis->dataDecrypt($member['email']) : $member['email']);
		$member_name = empty($member['member_name']) ? $this->obfuscate_email($email) : $member['member_name'];
		$user_lang = $member['user_lang'];
		$email = $member['email'];

		// 發送電郵驗證碼
		require_once ROOT.'/include/langMap/returnMessage.php';
		$RMG = new returnMessage($user_lang);
		$title = $RMG->getReturnCode('10245');

		$email_content = file_get_contents($this->gthis->baseURL.'/email/emailActivationCode?member_name='.$member_name.'&pins='.$pins."&language_set=".$user_lang );
		$email_id = $this->DB->insert_db('tbl_email_sender', array(
			'email_id' => '0',
			'to_email' => $email,
			'title'=> $title,
			'content'=>$email_content,
			'register_on'=>$this->gthis->get_current_time(),
			'status'=>'O'
		));
		return $email_id;
	}

	function sendMemberResetPasswordEmail($member,$spare=null){
		// 重設密碼
		$reset_password = $member['reset_password'];
		$email = (PERSONAL_DATA_ENCRYPT ? $this->gthis->dataDecrypt($member['email']) : $member['email']);
		$member_name = empty($member['member_name']) ? $this->obfuscate_email($email) : $member['member_name'];
		$user_lang = $member['user_lang'];
		$email = $member['email'];

		require_once ROOT.'/include/langMap/returnMessage.php';
		$RMG = new returnMessage($user_lang);
		$title = $RMG->getReturnCode('10551');

		$email_content = file_get_contents($this->gthis->baseURL.'/email/resetPasswordEmail?member_name='.$member_name.'&reset_password='.$reset_password."&language_set=".$user_lang);
		$email_id = $this->DB->insert_db('tbl_email_sender', array(
			'email_id' => '0',
			'to_email' => $email,
			'title'=> $title,
			'content'=>$email_content,
			'register_on'=>$this->gthis->get_current_time(),
			'status'=>'O'
		));
		return $email_id;
	}

	function sendOrderShipoutConfirmation($member,$order_id){
		// 包裏出庫
		$email = (PERSONAL_DATA_ENCRYPT ? $this->gthis->dataDecrypt($member['email']) : $member['email']);
		$member_name = empty($member['member_name']) ? $this->obfuscate_email($email) : $member['member_name'];
		$user_lang = $member['user_lang'];
		$email = $member['email'];
		$member_id = $member['member_id'];

		require_once ROOT.'/include/langMap/returnMessage.php';
		$RMG = new returnMessage($user_lang);
		$title = $RMG->getReturnCode('10533');

		require_once dirname(__FILE__).'/../include/order.app.class.php';
		$orderObject = new order($this->gthis,$this->DB);
		$order = $orderObject->memberGetOrder($member_id,$order_id);

		$email_content = file_get_contents($this->gthis->baseURL.'/email/email.php?mode=orderShipOutNotice&order_id='.$order_id.'&member_id='.$member_id."&language_set=".$user_lang);
		$email_id = $this->DB->insert_db('tbl_email_sender', array(
			'email_id' => '0',
			'to_email' => $email,
			'title'=> $title,
			'content'=>$email_content,
			//'attachments'=>$order['ship_label_path'],
			'register_on'=>$this->gthis->get_current_time(),
			'status'=>'O'
		));
		return $email_id;
	}

	function sendFinalDeliveryConfirmation($member,$order_id){
		// 成功派送電郵
		$email = (PERSONAL_DATA_ENCRYPT ? $this->gthis->dataDecrypt($member['email']) : $member['email']);
		$member_name = empty($member['member_name']) ? $this->obfuscate_email($email) : $member['member_name'];
		$user_lang = $member['user_lang'];
		$email = $member['email'];
		$member_id = $member['member_id'];

		require_once ROOT.'/include/langMap/returnMessage.php';
		$RMG = new returnMessage($user_lang);
		$title = $RMG->getReturnCode('10497');

		require_once dirname(__FILE__).'/../include/order.app.class.php';
		$orderObject = new order($this->gthis,$this->DB);
		$order = $orderObject->memberGetOrder($member_id,$order_id);

		$email_content = file_get_contents($this->gthis->baseURL.'/email/email.php?mode=finalDeliveryConfirmation&order_id='.$order_id.'&member_id='.$member_id."&language_set=".$user_lang);
		$email_id = $this->DB->insert_db('tbl_email_sender', array(
			'email_id' => '0',
			'to_email' => $email,
			'title'=> $title,
			'content'=>$email_content,
			//'attachments'=>$order['ship_label_path'],
			'register_on'=>$this->gthis->get_current_time(),
			'status'=>'O'
		));
		return $email_id;
	}

	function sendOrderCancelAndRefundEmail($member,$order_id){
		$email = (PERSONAL_DATA_ENCRYPT ? $this->gthis->dataDecrypt($member['email']) : $member['email']);
		$member_name = empty($member['member_name']) ? $this->obfuscate_email($email) : $member['member_name'];
		$user_lang = $member['user_lang'];
		$email = $member['email'];
		$member_id = $member['member_id'];

		require_once ROOT.'/include/langMap/returnMessage.php';
		$RMG = new returnMessage($user_lang);
		$title = $RMG->getReturnCode('10587');

		$email_content = file_get_contents($this->gthis->baseURL.'/email/email.php?mode=sendOrderCancelAndRefundEmail&order_id='.$order_id.'&member_id='.$member_id);
		$email_id = $this->DB->insert_db('tbl_email_sender', array(
			'email_id' => '0',
			'to_email' => $email,
			'title'=> $title,
			'content'=>$email_content,
			//'attachments'=>$order['ship_label_path'],
			'register_on'=>$this->gthis->get_current_time(),
			'status'=>'O'
		));
		return $email_id;
	}

	function sendServiceTicketReplyEmail($member,$ticket_id){
		$email = (PERSONAL_DATA_ENCRYPT ? $this->gthis->dataDecrypt($member['email']) : $member['email']);
		$member_name = empty($member['member_name']) ? $this->obfuscate_email($email) : $member['member_name'];
		$user_lang = $member['user_lang'];
		$email = $member['email'];
		$member_id = $member['member_id'];

		require_once ROOT.'/include/langMap/returnMessage.php';
		$RMG = new returnMessage($user_lang);
		$title = $RMG->getReturnCode('10588').":[$ticket_id] ".$RMG->getReturnCode('10589');

		$email_content = file_get_contents($this->gthis->baseURL.'/email/email.php?mode=sendServiceTicketReplyEmail&ticket_id='.$ticket_id.'&member_id='.$member_id);
		$email_id = $this->DB->insert_db('tbl_email_sender', array(
			'email_id' => '0',
			'to_email' => $email,
			'title'=> $title,
			'content'=>$email_content,
			//'attachments'=>$order['ship_label_path'],
			'register_on'=>$this->gthis->get_current_time(),
			'status'=>'O'
		));
		return $email_id;
	}

	function sendInternalEmail($to_email,$title,$content){
		// 內部email
		$email = $to_email;
		$title = $title;
		$email_content =$content;
		$email_id = $this->DB->insert_db('tbl_email_sender', array(
			'email_id' => '0',
			'to_email' => $email,
			'title'=> $title,
			'content'=>$email_content,
			'register_on'=>$this->gthis->get_current_time(),
			'status'=>'O'
		));
		return $email_id;
	}

	function sendEmailGoZoho($to_emails,$title,$emailContent,$enable_bcc='N',$attachments=null){
		require_once dirname(__FILE__).'/phpmailer/Exception.php';
		require_once dirname(__FILE__).'/phpmailer/PHPMailer.php';
		require_once dirname(__FILE__).'/phpmailer/SMTP.php';

		$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
		$subject = $title;
		$emailTemplate = $emailContent;
		try {
			mb_internal_encoding('UTF-8');   					 // 內部預設編碼改為UTF-8
			$subject = mb_encode_mimeheader($subject, "UTF-8");
		    //Server settings
		    if($this->gthis->parm_in('request','debug_go','N') =='Y'){
		    	$mail->SMTPDebug = 2;                                 // Enable verbose debug output	
		    } else {
		    	$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		    }
		    $mail->isSMTP();                                      // Set mailer to use SMTP
		    $mail->CharSet = 'UTF-8';

		   
		    $mail->Host = '';  					  // Specify main and backup SMTP servers
		    $mail->SMTPAuth = true;                               // Enable SMTP authentication
		    $mail->Username = '';                 // SMTP username
		    $mail->Password = '';                           // SMTP password
		    $mail->Port = 587;                                    // TCP port to connect to
		    $mail->SMTPSecure = 'tls';
	    	$mail->setFrom('' , 'The Gift');
	    	//$mail->addReplyTo(VMRU_SMTP_AC, VMRU_SMTP_SENDER_NAME);
	    	if($enable_bcc=='Y' || 1==1){
		    }
		    // recipient
		    if(is_array($to_emails)){
		    	foreach($to_emails as $k=>$i){
			    	$the_email = $i;
			    	if($the_email && $the_email!=''){
			    		$mail->addAddress($the_email);     // Add a recipient		
			    	}
			    }
		    } else {
		    	$mail->addAddress($to_emails);     // Add a recipient		
		    }
		    
		    //Attachments
		    if($attachments){
		    	// try for delimiter
		    	$attachments = explode('||', $attachments);
		    	foreach($attachments as $k=>$i){
		    		if($i){
		    			$attachment_obj = explode('^^', $i);
		    			if(!empty($attachment_obj[1])){
		    				$pos = strpos($attachment_obj[0], '../');
					        if($pos>-1){
					            $mail->addAttachment(str_replace('../', './', $attachment_obj[0]),$attachment_obj[1]);
					        } else {
					            // do nth
					        }
		    			} else {
		    				$pos = strpos($i, '../');
					        if($pos>-1){
					            $mail->addAttachment(str_replace('../', './', $i));
					        } else {
					            // do nth
					        }
		    			}
		    		}
		    	}
		    }
		    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

		    //Content
		    $mail->isHTML(true);                                  // Set email format to HTML
		    $mail->Subject = $subject;    
		    $mail->Body    = $emailTemplate;
		    $mail->AltBody = $emailTemplate;

		    $mail->send();
		    //echo 'Message has been sent';		
		    return '0';    
		} catch (Exception $e) {
		    //echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
		    if($this->gthis->parm_in('request','debug_go','N') =='Y'){
		    	print_r($mail->ErrorInfo);
		    }
		    $error_info = json_encode($mail->ErrorInfo);
		    $result['code'] = '-2';
		    $result['error_info'] = $error_info;
		    return '-2';
		}
		return '-1';
	}

	function sendEmailGo($address,$title,$emailContent){
		$params['personalizations'] = array( array(
		    'recipient' => $address,
		    'attributes' => array('NAME' => $address) 
		    )
		);
		$params2 = array('from' => array('fromEmail'=>'','fromName'=>$this->gthis->business_name),
		    'subject' => $title,
		    'content' => $emailContent,
		    'settings' => array('clicktrack'=>1,'opentrack'=>1),
		    'replyToId' => '');
		$result = $params +$params2;
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.pepipost.com/v2/sendEmail",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => json_encode($result),
		CURLOPT_HTTPHEADER => array(
		    "api_key: bed039fd992b8b3fea2892af46d043ba",
		    "content-type: application/json"
		  ),
		));
		$response = curl_exec($curl);
		$responseJson = json_decode($response,1);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err || isset($responseJson['error_info'])) {
		  $returnCode = -1;
		} else {
		  $returnCode = 1;
		}
		return $returnCode;
	}
}
?>