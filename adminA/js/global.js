function unserialize(serializedData) {
    var urlParams = new URLSearchParams(serializedData); // get interface / iterator
    unserializedData = {}; // prepare result object
    for ([key, value] of urlParams) { // get pair > extract it to key/value
        unserializedData[key] = value;
    }

    return unserializedData;
}

$(document).ready(function() {
    $(document).on("click", ".btn_popup", function(){
        // related code at header.html

        var html = $(this).attr("data-popup")
        if ($(this).attr("data-popup").indexOf("?") >= 0)
            html += "&";
        else 
            html += "?";
        html += "is_popup=Y";

        $vkmodal({
            extraClass: "page_popup",
            fullScreen: false,
            tapBackgroundToClose: true,
            ajaxPath: html,
        });
    });

    $(".selectize").selectize({});
    $(".selectizeMulti").selectize({
        delimiter: ",",
        persist: true,
        create: function (input) {
            return {
                value: input,
                text: input,
            };
        },
    });
    
    $(document).off("click", ".sortable");
    $(document).on("click", ".sortable", function(e){
        e.preventDefault();
        $("#list_form [name=sort]").val($(this).attr("data-sort_field"));
        if ($(this).hasClass("desc"))
            $("#list_form [name=order]").val("asc");
        else
            $("#list_form [name=order]").val("desc");

        $("#list_form").submit();
    });

    $(document).off("change", ".sort-select");
    $(document).on("change", ".sort-select", function(e){
        e.preventDefault();
        $(".sort-select [name=sort]").val($(this).attr("data-sort_field"));

        $(".sort-select-form").submit();
    });

    $(document).off('click', '#account_dropdown');
    $(document).on("click", "#account_dropdown", function(e) {
        if ($('#account_dropdown_menu').hasClass('hidden')) {
            $('#account_dropdown_menu').removeClass('hidden');
            $(document).off('click', '*');
            $(document).on("click", '*', function(e) {
                if ($(e.target).closest("#account_dropdown_menu").length === 0) {
                    $('#account_dropdown_menu').addClass('hidden');
                    $(document).off('click', '*');
                }
            });
        } else {
            $('#account_dropdown_menu').addClass('hidden');
        }
    });

    $(document).off('click', '#mobile_menu_trigger');
    $(document).on("click", "#mobile_menu_trigger", function(e) {
        if ($('#mobile_menu').hasClass('hidden')) {
            $('#mobile_menu').removeClass('hidden');
            $(document).off('click', '*');
            $(document).on("click", '*', function(e) {
                if ($(e.target).closest("#mobile_menu").length === 0) {
                    $('#mobile_menu').addClass('hidden');
                    $(this).find('.ri-menu-line').removeClass('hidden');
            		$(this).find('.ri-close-line').addClass('hidden');
                    $(document).off('click', '*');
                }
            });
            $(this).find('.ri-menu-line').addClass('hidden');
            $(this).find('.ri-close-line').removeClass('hidden');
        } else {
            $('#mobile_menu').addClass('hidden');
            $(this).find('.ri-menu-line').removeClass('hidden');
            $(this).find('.ri-close-line').addClass('hidden');
        }
    });

    $(document).off("click", ".radioToggle")
    $(document).on("click", ".radioToggle", function(){
        if ($(this).val() === 'Y') {
            $(this).val('N')
            $(this).removeClass('active-toggle')
        } else {
            $(this).val('Y')
            $(this).addClass('active-toggle')
        }
    });

    $(document).off('click', 'a');
    $(document).on("click", "a", function(e) {
        if ($(this).attr('href') == '#') {
            e.preventDefault();
        }
    });

    $(document).off('click', '.back_page');
    $(document).on("click", ".back_page", function(e) {
        e.preventDefault();
        window.history.back();
    });

    $(document).off('click', '#asideNav .nav-item');
    $(document).on("click", "#asideNav .nav-item", function(e) {
        $('.active_nav').removeClass('active_nav');
        if($(this).hasClass('nav-item-skip')){
            // do nth
        } else {
            $(this).addClass('active_nav');
        }
    });

    $(document).off('click', '.sideBarChange');
    $(document).on("click", ".sideBarChange", function(e) {
        if ($(this).attr('data-open') == 'Y') {
            menuBarChange($(this));
            $(this).attr('data-open', 'N');
            updateMenuSetting('N');
        } else {
            menuBarChange($(this));
            $(this).attr('data-open', 'Y');
            updateMenuSetting('Y');
        }
    });

    $(document).off("change", "[name=shop_id]");
    $(document).on("change", "[name=shop_id]", function(){
        var url = '/admin/setting/api/changeShop';
        $.bajax({
            url: url,
            data: {shop_id: $(this).val()},
            method: "POST",
            callback: function(data) {
                if (!data.code == 0){
                    globalErrorReturn(data);
                } else {
                    location.reload();
                }
            },
        });
    });

    jqueryBindEvent('click','.filter_search_form_submit',function(e){
        $(this).closest('.filter_search_form').submit();
    })

    if ($('.sideBarChange').attr('data-open') == 'N') {
	    $('.sideBarChange').attr('data-open', 'Y');
	    menuBarChange($('.sideBarChange'));
	    $('.sideBarChange').attr('data-open', 'N');
	}
	$('body').removeClass('opacity-0');

});

function globalErrorReturn(data){
    vkAlertDefault({
        title: "錯誤",
        content: data.msg,
        // content: data.code + ": " +data.msg,
        confirmButtonHtml: "確認"
    });
}

function menuBarChange(ele) {
    if (ele.attr('data-open') == 'Y') {
        $('.sideBarFull').addClass('hidden');
        $('#desktopSideBar').removeClass('w-56');
        $('#asideNav').removeClass('pr-4');
        ele.find('i').removeClass('ri-menu-fold-line');
        ele.find('i').addClass('ri-menu-unfold-line');
        $(".nav_2Lv_wrapper").addClass("hidden")
    } else {
        $('.sideBarFull').removeClass('hidden');
        $('#desktopSideBar').addClass('w-56');
        $('#asideNav').addClass('pr-4');
        ele.find('i').removeClass('ri-menu-unfold-line');
        ele.find('i').addClass('ri-menu-fold-line');
        $(".nav_2Lv_wrapper").addClass("hidden")
        $("#asideNav").find(".current_page").removeClass("hidden")
    }
}

function updateMenuSetting(open) {
    var url = "admin/global/api/menuBarStatus?open=" + open;
    $.bajax({
        url: url,
        debug: 'N',
        callback: function(data) {
            console.log(data);
        },
        errorCallback: function(jqXHR, textStatus, errorThrown) {

        }
    });
}

function fullScreenLoader(onOff, title = "Loading", content = "Loading") {
    if (onOff == 'Y') {
        /*window.modalLoader = $.bmodal({
            type: 'modalloader',
            title: title,
            elementId: 'modalLoader',
            content: content,
        });*/
        vkfullScreenLoader('show')
    } else {
        /*window.modalLoader = $.bmodal({
            close: 'Y',
            elementId: 'modalLoader',
        });*/
        vkfullScreenLoader('close')
    }
}

function errorMessageShow(title="Error",content="Error Content",trueBtnName="Ok"){
    window.errorMsg = $.bmodal({
        type: 'alert',
        elementId: 'errorMsg',
        footerClose: false,
        closeOnBackgroundClick : false,
        title: title,
        trueBtnName:trueBtnName,
        content: content,
    }); 
}

function alertMessageShow(title="Alert",content="Alert rContent",trueBtnName="Ok",trueCallBack=function(){}){
    window.alertMsg = $.bmodal({
        type: 'alert',
        elementId: 'alertMsg',
        footerClose: false,
        closeOnBackgroundClick : false,
        title: title,
        trueBtnName:trueBtnName,
        content: content,
        trueBtncallback: trueCallBack,
    }); 
}

function refreshCurrentPage(){
    pageHandler(window.location.href,'Y','',null,null,'N','N');
}

//window.container = document.getElementById("container");
var prevScrollpos = window.pageYOffset;

window.onscroll = function() {
    
}

function buttonSetLoading(ele, isload) {
    if (isload == 'Y') {
        ele.attr('ori-html', ele.html());
        ele.html('<div class="loading_gif">load</div>');
        ele.addClass('is_processing');
    } else {
        ele.html(ele.attr('ori-html'));
        ele.removeAttr('ori-html');
        ele.removeClass('is_processing');
    }
}

function twbuttonSetLoading(ele, isload) {
    if (isload == 'Y') {
        ele.find('.buttonLoader').removeClass('hidden');
        ele.addClass('is_processing');
    } else {
        ele.find('.buttonLoader').addClass('hidden');
        ele.removeClass('is_processing');
    }
}

function debug($msg){
    if(window.dev_mode=='Y'){
        console.log($msg);
    } 
}

function jqueryBindEvent(event,element,functionCallback){
    $(document).off(event, element);
    $(document).on(event ,element,functionCallback);
}

function jqueryCheckBoxChecked(element){
    if (element.is(":checked")) {
        return element.val();
    } else {
        return null;
    }
}

async function globalLoader(dom = null){
    if (dom){
        killVkmodal(dom.timestamp)
    } else {
        return await vkmodal({
            modalMode : 'loading',
        })
    }
}

function thousand_separator(x) {
    x = x.toString();
    x = parseFloat(x).toFixed(2);
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(x))
        x = x.replace(pattern, "$1,$2");
    return x;
}

function getQueryString(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function random(length = 10){
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

function resetSession(){
    $.bajax({
        url: "https://devthegift.wwdrfive.com/admin/item/api/reset_session",
        debug: 'N',
        callback: function(data) {
        }
    });
}

function session(){
    $.bajax({
        url: "https://devthegift.wwdrfive.com/admin/item/api/session",
        debug: 'N',
        callback: function(data) {
            console.log(data.result.callPath, data.result.shop_id);
        }
    });
}

function infoPopup(title = "成功", content = "已更新資料。", type = null){
    contentHtml = `
    <div aria-live="assertive" class="fixed inset-0 flex items-end px-4 py-6 pointer-events-none sm:p-6 sm:items-start z-50">
        <div class="w-full flex flex-col items-center space-y-4 sm:items-end">
            <div class="max-w-sm w-full bg-white shadow-lg rounded-lg pointer-events-auto ring-1 ring-black ring-opacity-5 overflow-hidden">
                <div class="p-4">
                    <div class="flex items-start">
                        <div class="flex-shrink-0">
                            <svg class="h-6 w-6 text-green-400" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                        </div>
                        <div class="ml-3 w-0 flex-1 pt-0.5">
                            <p class="text-sm font-medium text-gray-900">
                                `+ title +`
                            </p>
                            <p class="mt-1 text-sm text-gray-500">
                                `+ content +`
                            </p>
                        </div>
                        <div class="ml-4 flex-shrink-0 flex">
                            <button class="bg-white rounded-md inline-flex text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                <span class="sr-only">Close</span>
                                <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>`;

    $divPopup = $("<div/>").html(contentHtml).addClass("popup_modal").attr("style", "z-index: 100;");
    $divPopup.find("button").on("click", function(){
        $(this).parents(".popup_modal").remove();
    });

    setInterval(function(){
        $divPopup.remove();
    }, 3000);
    $("body").append($divPopup);
}