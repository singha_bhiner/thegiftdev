<?php
/* 
    Objective: item and SKU and item group related 
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_item
        tbl_item_grp
        tbl_item_grp_content
        tmap_item_shop
        tmap_item_tag
        tmap_sku_tag


        tbl_sku
        tbl_sku_supplier
        tbl_sku_code
        tbl_sku_stock_in
        tbl_sku_val
        
*/


require_once dirname(__FILE__). '/abstract.class.php';
class item extends baseClass{

    function getItemLocationList($sku_id){
        $sql = "SELECT * 
                FROM `tmap_location_sku` AS m 
                WHERE `sku_id` = '$sku_id' AND `qty` > 0";
        return $this->DB->get_Sql($sql);
    }

    function codeToItem($code){
        if (substr($code,0,7) == 'ITEMID:'){
            return substr($code, 7);
        } else {
            $sql = "SELECT * 
                    FROM `tmap_item_barcode`
                    WHERE barcode = '$code'";
            $map = $this->DB->get_Sql($sql)[0];
            if ($map){
                return $map['item_id'];
            } else {
                new apiDataBuilder(-31, null, null, $this->lang);
            }
        }
    }

    function codeToItemId($code){
        if (substr($code,0,7) == 'ITEMID:'){
            return  substr($code, 7);
        } else {
            $sql = "SELECT * 
                    FROM `tmap_sku_barcode`
                    WHERE barcode = '$code'";
            $map = $this->DB->get_Sql($sql)[0];
            if ($map){
                return $map['sku_id'];
            } else {
                new apiDataBuilder(-31, null, null, $this->lang);
            }
        }
    }

    function addSkuBarcodeMatch($sku_id, $barcode){
        // if the barcode is not in the list create it
        $this->DB->insert_db("tmap_sku_barcode", [
            "sku_id" => $sku_id,
            "barcode" => $barcode,
            "updated_by_id" => $_SESSION['admin']['admin_id']
        ]);
        return true;
    }

    function isSkuBarcodeMatch($sku_id, $barcode){
        // check the barcode match with the SKU or not
        $sql = "SELECT * FROM tmap_sku_barcode WHERE activate = 'Y' AND barcode = '$barcode'";
        $map = $this->DB->get_Sql($sql)[0];

        if ($map && $map['sku_id'] != $sku_id)
            return false;
        else if (!$map)
            return true;
        else
            return true;
    }

    function voidSku($data){
        $location_id = $data['location_id'];
        unset($data['location_id']);
        $this->stock_out($data, $location_id);
    }

    function editPurchase($po_id, $data){
        // edit purchase record 
        // $po_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($po_id == 0){
            $po_id = $this->DB->insert_db('tbl_purchase', null);
        } 


        if ($data['ref_file'])
            $data['ref_file'] = $this->DB->uploadImg($data['ref_file'], "ref_file", $po_id);

        if ($data['inList']){
            $inList = $data['inList'];
            unset($data['inList']);

            foreach($inList as $in){
                $in['po_id'] = $po_id;
                if ($in['location_id']){
                    $location_id = $in['location_id'];
                    unset($in['location_id']);
                    $this->stock_in($in, $location_id);
                } else {
                    $this->stock_in($in);
                }
                
            }
        }

        $sql = $this->DB->buildSql_update("tbl_purchase", "po_id", $po_id, $data);
        $this->DB->update($sql);

        return $this->getPurchaseDtl($po_id);
    }

    function stock_reserve($sku_id, $qty){
        $this->DB->adjustFloatVal("tbl_sku", "sku_id", $sku_id, 'reserved_qty', $qty); 
        $this->DB->adjustFloatVal("tbl_sku", "sku_id", $sku_id, 'ava_qty', $qty * -1); 

        $skuDtl = $this->getSkuDtl($sku_id);

        $this->DB->adjustFloatVal("tbl_item", "item_id", $skuDtl['item_id'], 'reserved_qty', $qty); 
        $this->DB->adjustFloatVal("tbl_item", "item_id", $skuDtl['item_id'], 'ava_qty', $qty * -1); 
    }

    function getPurchaseDtl($po_id){
        if ($po_id){
            return $this->getPurchaseList(["po_id" => $po_id], $this->DB->getUnique)[0];
        } else {
            return null;
        }
    }

    function getPurchaseList($filter = null, $limiter = null, $sorting = null){
        // get the list
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by
        $fromShort = "p";
        $selectSql = "SELECT $fromShort.* ";
        $fromSql = "FROM `tbl_purchase` AS $fromShort ";
        $whereSql = "WHERE $fromShort.`activate` = 'Y' ";

        foreach($filter AS $k => $i){
            if ($i){
                switch($k){
                    case 'skey':
                        $fromSql .= " LEFT JOIN `tbl_supplier` AS s ON s.supplier_id = $fromShort.supplier_id ";
                        $whereSql .= " AND (
                                $fromShort.ref_no LIKE '%$i%' OR 
                                s.name_tc LIKE '%$i%' OR s.name_en LIKE '%$i%' OR s.short_form = '$i'
                            )";
                        break;
                    case 'item_id':
                        $selectSql .= ", i.* ";
                        $fromSql .= " LEFT JOIN `tbl_item_stock_in` AS i ON i.`po_id` = $fromShort.`po_id` ";
                        $whereSql .= " AND (i.item_id = '$i')";
                        break;
                    case 'po_id':
                        $whereSql .= " AND ($fromShort.po_id = '$i')";
                        break;
                }
            }
        }

        $sql = $selectSql . $fromSql . $whereSql;

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        foreach($res as $k => $i){
            $res[$k] = $this->dispatchPurchase($i);
        }

        return $res;
    }

    function dispatchPurchase($purchase){
        $po_id = $purchase['po_id'];
        
        $sql = "SELECT isi.*, i.inhouse_name_tc, i.various_item_id
                FROM `tbl_item_stock_in` AS isi
                LEFT JOIN `tbl_item` AS i ON isi.item_id = i.item_id 
                WHERE isi.`activate` = 'Y' AND isi.`po_id` = '$po_id'";
        $purchase['stock_in_list'] = $this->DB->get_Sql($sql);

        return $purchase;
    }

    function getStockInDtl($in_id){
        $sql = "SELECT *
            FROM `tbl_sku_stock_in` as i
            WHERE i.`activate` = 'Y' AND i.`in_id` = '$in_id'";

        return $this->DB->get_Sql($sql)[0];
    }

    function getStockInList($filter){
        $sql = "SELECT *
            FROM `tbl_item_stock_in` as i
            WHERE i.`activate` = 'Y'";

        
        foreach($filter AS $k => $i){
            if ($i){
                switch($k){
                    case 'po_id':
                        $sql .= " AND i.`po_id` = '$i'";
                        break;
                }
            }
        }

        return $this->DB->get_Sql($sql);
    }

    function getStockOutList($filter = null, $limiter = null, $sorting = null){
        // get the list
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $fromShort = "iso";
        $sql = "SELECT $fromShort.*, i.inhouse_name_tc
                FROM `tbl_item_stock_out` AS $fromShort
                LEFT JOIN `tbl_item` AS i ON $fromShort.item_id = i.item_id
                WHERE $fromShort.`activate` = 'Y'";

        foreach($filter AS $k => $i){
            if ($i){
                switch($k){
                    case 'skey':
                        $sql .= " AND (
                            $fromShort.`item_id` = '$i' OR
                            i.`inhouse_name_tc` LIKE '%$i%' OR i.`inhouse_name_en` LIKE '%$i%' OR
                            i.`gift_name_tc` LIKE '%$i%' OR i.`gift_name_en` LIKE '%$i%' OR
                            i.`zuri_name_tc` LIKE '%$i%' OR i.`zuri_name_en` LIKE '%$i%' OR
                            i.`ltp_name_tc` LIKE '%$i%' OR i.`ltp_name_en` LIKE '%$i%' OR
                            i.`alias` LIKE '%$i%')";
                        break;
                }
            }
        }

        if ($sorting){
            $sql .= " ORDER BY $fromShort.`" . $sorting['sort'] . "` " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);


        return $res;
    }

    function addSkuCode($code_id, $data){
        // edit sku code 
        // $val_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($code_id == 0){
            $code_id = $this->DB->insert_db('tbl_sku_code', null);
        } 

        $sql = $this->DB->buildSql_update("tbl_sku_code", "code_id", $code_id, $data);
        $this->DB->update($sql);

        return null;
    }

    function editSkuVal($val_id, $data){
        // edit sku value 
        // $val_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        unset($data['val_id']);

        if (!$val_id || $val_id == 0){
            $val_id = $this->DB->insert_db('tbl_sku_val', null);
        } 

        $sql = $this->DB->buildSql_update("tbl_sku_val", "val_id", $val_id, $data);
        $this->DB->update($sql);

        return null;
    }

    private function renderCodeMap($codeMap, $bdArr = []){
        // recursive function
        // render the code mapping
        // $codeMap: source codeMap
        // $bdArr: bought down array

        $valArr = [];


        if (sizeof($bdArr) == 0){ // check it is the first level
            foreach($codeMap[0]['valList'] as $k => $val){ // create a 2lv array to store the val
                $valArr[$k]  = [$val];
            }

        } else {
            $key = 0;
            for ($i = 0 ; $i < sizeof($bdArr) ; $i++){
                foreach ($codeMap[0]['valList'] as $kk => $val){
                    // multiple the bdArr and combine the val;
                    $valArr[$key] = $bdArr[$i];
                    array_push($valArr[$key], $val);
                    $key++;
                    
                }
            }
        }

        if (sizeof($codeMap) > 1){ // if there is more level
            array_shift($codeMap); // shift the codeMap
            return $this->renderCodeMap($codeMap, $valArr); // recursive run the func
        } else {
            return $valArr;
        }   
    }


    function getSkuCodeList($filter = null){
        // get the SKU code value list

        $sql = "SELECT * 
                FROM `tbl_sku_code` AS c 
                WHERE c.`activate` = 'Y'";

        foreach($filter AS $k => $i){
            if ($i){
                switch($k){
                    case 'skey':
                        $sql .= " AND (code LIKE '%$i%')";
                        break;
                    case 'code':
                        $sql .= " AND (code LIKE '%$i%')";
                        break;
                }
            }
        }

        $res = $this->DB->get_Sql($sql);

        if ($res){
            foreach ($res as $k => $code){
                $sql = "SELECT *
                        FROM `tbl_sku_val` AS v
                        WHERE v.`activate` = 'Y' AND v.`code_id` = '" . $code['code_id'] . "'";
                        
                $res[$k]['valueList'] =  $this->DB->get_Sql($sql);
            }
        }
        
        return $res;
    }

    function editSupplier($supplier_id, $data){
        // edit supplier
        // $supplier_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($supplier_id == 0){
            $supplier_id = $this->DB->insert_db('tbl_sku_supplier', null);
        } 

        // if ($data['skuList']){
        //     $skuList = $data['skuList'];
        //     unset($data['skuList']);
        //     $skuId_arr = [];
        //     foreach ($skuList as $k => $sku){
        //         if ($sku['sku_id'] == 0){
        //             // create a new sku 
        //             $sku = $this->editSku(0,[
        //                 "sku_val" => $sku['sku_val'],
        //                 "sku_code" => $sku['sku_val'],
        //                 "alias" => $sku['alias'],
        //                 "cost" => $sku['cost'],
        //                 "item_id" => $sku['item_id']
        //             ]); 
        //         } 

        //         array_push($skuId_arr, $sku['sku_id']);
        //     }
        //     $this->DB->updateMap("tmap_supplier_sku", "supplier_id", $supplier_id, "sku_id", $skuId_arr);
        // }

        $sql = $this->DB->buildSql_update("tbl_sku_supplier", "supplier_id", $supplier_id, $data);
        $this->DB->update($sql);

        return $this->getSupplierDtl($supplier_id);
    }

    function getSupplierDtl($supplier_id){
        // get the supplier Detail
        // $supplier_id: PK

        $sql = "SELECT s.* , COUNT(i.item_id) AS 'nextId'
                FROM `tbl_sku_supplier` AS s
                LEFT JOIN `tbl_item` AS i ON i.`supplier_id` = s.`supplier_id`
                WHERE s.activate = 'Y' AND s.supplier_id = '1'";
        $res = $this->DB->get_Sql($sql)[0];

        return $res;
    }

    function getSupplierList($filter = null, $limiter = null, $sorting = null){
        // get the list
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $sql = "SELECT s.* 
                FROM `tbl_sku_supplier` AS s
                WHERE s.activate = 'Y'";

        foreach($filter AS $k => $i){
            if ($i){
                switch($k){
                    case 'skey':
                        $sql .= " AND (supplier_name LIKE '%$i%' OR contact_name LIKE '%$i%')";
                        break;
                }
            }
        }

        $sql .= " GROUP BY s.supplier_id";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        return $res;
    }

    function editItemGroup($data){
        // edit item group 
        // $grp_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($data['grp_id'] == 0){
            $grp_id = $this->DB->insert_db('tbl_item_grp', $data);
        } else {
            $grp_id = $this->DB->update_Sql("tbl_item_grp", "grp_id", $data);
        }

        if ($data['item_json']){
            $this->DB->updateForeignMap("tbl_item_grp_content", "grp_id", $grp_id, "content_id", $data['item_json']);
        }
        
        return $this->getItemGroupDtl($grp_id);
    }

    function editItemGrpContent($content_id, $data){
        // edit item group content which the item group containing 
        // $content_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($content_id == 0){
            $content_id = $this->DB->insert_db('tbl_item_grp_content', null);
        } 

        $sql = $this->DB->buildSql_update("tbl_item_grp_content", "content_id", $content_id, $data);
        $this->DB->update($sql);

        return true;
    }

    function getItemGroupDtl($grp_id){
        if ($grp_id) {
            return $this->getItemGroupList(["grp_id" => $grp_id, "is_tpl" => "N"], $this->DB->getUnique)[0];
        } else {
            return false;
        }
    }


    // function getItemGrpDtl($grp_id){
    //     // get the item group Detail
    //     // $grp_id: PK

    //     $sql = "SELECT g.* 
    //             FROM `tbl_item_grp` AS g
    //             WHERE g.`activate` = 'Y' AND g.`grp_id` = '$grp_id'";
    //     $res = $this->DB->get_Sql($sql)[0];

    //     $sql = "SELECT c.*, i.item_img, c.name_".$this->lang." AS `name`, i.item_img
    //             FROM `tbl_item_grp_content` AS c
    //             LEFT JOIN `tbl_item` AS i on i.item_id = c.item_id
    //             WHERE c.`activate` = 'Y' AND c.`grp_id` = '$grp_id'
    //             ORDER BY sorting ";
    //     $res['item_grp_content'] = $this->DB->get_Sql($sql);

    //     $i = 0;
    //     $bool = false;
    //     foreach($res['item_grp_content'] as $gc){
    //         if ($gc['client_optional'] == 'Y'){
    //             $bool = true;
    //         } else {
    //             $i++;
    //         }
    //     }

    //     if ($bool)
    //         $res['client_optional'] = 'Y';
    //     else 
    //         $res['client_optional'] = 'N';

    //     if ($i > 1)
    //         $res['auto_replace'] = 'Y';
    //     else 
    //         $res['auto_replace'] = 'N';
        

    //     return $res;
    // }
    
    function getItemGrpList($filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by
        
        $sql = "SELECT g.*
            FROM `tbl_item_grp` AS g
            WHERE g.activate = 'Y'";

        foreach($filter AS $k => $i){
            if ($i){
                switch($k){
                    case 'skey':
                        $sql .= " AND (g.adm_name LIKE '%$i%' OR g.grp_id LIKE '%$i%')";
                        break;
                }
            }
        }

        $sql .= " GROUP BY g.grp_id";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);



        return $res;
    }

    function getSkuList($filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by
        
        $sql = "SELECT s.* , m.shop_id, i.item_img, i.".$this->getPrefix($this->shop_id)."_name_tc AS name_tc , i.".$this->getPrefix($this->shop_id)."_name_en AS name_en
                FROM `tbl_sku` AS s
                LEFT JOIN `tbl_item` AS i ON i.item_id = s.item_id AND i.activate ='Y' 
                LEFT JOIN `tmap_item_shop` AS m ON i.item_id = m.item_id AND m.activate ='Y'
                LEFT JOIN `tmap_sku_barcode` AS msb ON msb.sku_id = s.sku_id AND msb.activate ='Y'
                WHERE s.activate = 'Y'";

        foreach($filter AS $k => $i){
            if ($i){
                switch($k){
                    case 'shop_id':
                        $sql .= " AND m.shop_id = '$i'";
                        break;
                    case 'shop_id_arr':
                        $sql .= " AND m.shop_id in ($i)";
                        break;
                    case 'item_id':
                        $sql .= " AND s.item_id = '$i'";
                        break;
                    case 'skey':
                        $sql .= " AND (s.sku_val LIKE '%$i%' OR i.".$this->getPrefix($this->shop_id)."_name_tc LIKE '%$i%' OR i.".$this->getPrefix($this->shop_id)."_name_en LIKE '%$i%' OR s.`sAlias` LIKE '%$i%' OR msb.`barcode` LIKE '$i')";
                        break;
                }
            }
        }

        $sql .= " GROUP BY s.sku_id";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        return $res;
    }

    function getSkuDtl($sku_id){
        // get the sku Detail
        // $sku_id: PK

        $sql = "SELECT s.*, i.item_img, i.".$this->getPrefix($this->shop_id)."_name_en AS name_en, i.".$this->getPrefix($this->shop_id)."_name_tc AS name_tc
                FROM `tbl_sku` AS s
                LEFT JOIN `tbl_item` AS i ON s.item_id = i.item_id
                WHERE s.`activate` = 'Y' AND s.`sku_id` = '$sku_id'";
        $res = $this->DB->get_Sql($sql)[0];

        return $res;
    }

    function editSku($sku_id, $data){
        // edit SKU
        // $sku_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($sku_id == 0){
            $sku_id = $this->DB->insert_db('tbl_sku', null);
        } 

        $sql = $this->DB->buildSql_update("tbl_sku", "sku_id", $sku_id, $data);
        $this->DB->update($sql);

        return $this->getSkuDtl($sku_id);
    }

    private function constructSku($prefix, $codeList){
        $sku_code_arr = ["prefix"];
        $sku_val_arr = [$prefix];
        $lbl_tc_arr = [];
        $lbl_en_arr = [];

        foreach ($codeList as $val_id){
            $codeVal = $this->getSkuCodeValDtl($val_id);
            array_push($sku_code_arr, $codeVal['code']);
            array_push($sku_val_arr, $codeVal['val']);
            array_push($lbl_tc_arr, "(" . $codeVal['lbl_tc'] . ")");
            array_push($lbl_en_arr, "(" . $codeVal['lbl_en'] . ")");
        }

        $sku = [
            "sku_code" => implode("-", $sku_code_arr),
            "sku_val" => implode("-", $sku_val_arr),
            "lbl_tc" => implode("", $lbl_tc_arr),
            "lbl_en" => implode("", $lbl_en_arr),
        ];

        return $sku;
    }

    function getSkuCodeValDtl($val_id){
        $sql = "SELECT sv.val, sc.code, sv.lbl_tc, sv.lbl_en
                FROM `tbl_sku_val` AS sv
                LEFT JOIN `tbl_sku_code` AS sc ON sc.code_id = sv.code_id
                WHERE sv.`val_id` = '$val_id'";
        return $this->DB->get_Sql($sql)[0];
    }

    function uploadItemImg($files, $id = 99){
        return $this->DB->uploadImg($files, "item", $id);
    }

    function editItem($data){
        require_once ROOT.'/include/class/audit.class.php';
        $auditCls = new audit($this->DB, $this->lang, $this->isAdmin); 
        if ($data['item_id'] == 0){
            $item_id = $this->DB->insert_db('tbl_item', $data);
            $data['item_id'] = $item_id;
            $auditCls->insertLog("tbl_item", "add", $data, "item_id");
        } else {
            $auditCls->insertLog("tbl_item", "update", $data, "item_id");
            $item_id = $this->DB->update_Sql("tbl_item", "item_id", $data);
        }

        if ($data['tag_id_arr']){
            $this->DB->updateMap("tmap_item_tag", "item_id", $item_id, "tag_id", $data['tag_id_arr']);
        }

        if ($data['val_list']){
            $this->DB->updateMap("tmap_item_various_val", "item_id", $item_id, "val_id", $data['val_list']);
        }

        
        if ($data['item_img'] && isset($data['item_img']['name'])){
            $data['item_img'] = $this->DB->uploadImg($data['item_img'], "item", $item_id);
        }

        

        return $this->getItemDtl($item_id);
    }

    function getItemGroupList($filter = null, $limiter = null, $sorting = null){
        $fromShort = "ig";
		$selectSql = "SELECT $fromShort.*, COUNT(pig.product_id) AS productCount ";
		$fromSql = " FROM `tbl_item_grp` AS $fromShort
                    LEFT JOIN `tbl_product_item_group` AS pig ON pig.grp_id = $fromShort.grp_id AND pig.activate = 'Y'
                    LEFT JOIN `tbl_item_grp_content` AS igc ON igc.grp_id = $fromShort.grp_id AND igc.activate = 'Y'
                    LEFT JOIN `tbl_item` AS i ON i.item_id = igc.item_id AND i.activate = 'Y'";
		$whereSql = " WHERE $fromShort.`activate` = 'Y'";
        $groupSql = " GROUP BY $fromShort.`grp_id`";
		if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
			$whereSql = " WHERE 1=1";
		if ($filter && $filter['is_tpl'] && $filter['is_tpl'] == 'N'){

        } else {
			$whereSql .= " AND $fromShort.`is_tpl` = 'Y'";
        }

		if ($sorting)
			$sortSql = " ORDER BY ". $sorting['sort'] . " " . $sorting['order'];
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
				if ($i){
					switch ($k){
                        case "grp_id":
                            $whereSql .= " AND ($fromShort.`grp_id` = '$i')";
                            break;
                        case 'skey':
                            //  SKU, 別稱，中文名稱（供應商），英文名稱（供應商），中文顯示名稱，英文顯示名稱，條碼，貨品編號（供應商），標籤
                            $fromSql .= " LEFT JOIN `tmap_item_tag` AS m ON m.`item_id` = i.`item_id` AND m.`activate` = 'Y' 
                                            LEFT JOIN `tbl_item_tag` AS t ON t.`tag_id` = m.`tag_id` AND t.`activate` ='Y'";
                            $whereSql .= " AND (ig.`adm_name` LIKE '%$i%' OR ig.`alias` LIKE '%$i%' OR ig.`inhouse_name_tc` LIKE '%$i%' OR ig.`inhouse_name_en` LIKE '%$i%' OR ig.`gift_name_tc` LIKE '%$i%' OR ig.`gift_name_en` LIKE '%$i%'OR ig.`zuri_name_tc` LIKE '%$i%' OR ig.`zuri_name_en` LIKE '%$i%' OR ig.`ltp_name_tc` LIKE '%$i%' OR ig.`ltp_name_en` LIKE '%$i%' OR i.`sku` = '$i' OR i.`supplier_name_tc` LIKE '%$i%' OR i.`supplier_name_en` LIKE '%$i%' OR i.`inhouse_name_tc` LIKE '%$i%' OR i.`inhouse_name_tc` LIKE '%$i%' OR i.`gift_name_tc` LIKE '%$i%' OR i.`gift_name_en` LIKE '%$i%' OR i.`zuri_name_tc` LIKE '%$i%' OR i.`zuri_name_en` LIKE '%$i%' OR i.`ltp_name_tc` LIKE '%$i%' OR i.`ltp_name_en` LIKE '%$i%' OR i.`supplier_barcode` LIKE '%$i%' OR i.`supplier_stock_code` LIKE '%$i%' OR t.`tag_name_tc` LIKE '%$i%' OR t.`tag_name_en` LIKE '%$i%')";
                            break;
                        case 'cate_id_arr':
                            $whereSql .= " AND (i.`item_cate_id` IN ($i))";
                            break;
					}
				}
			}
		}
		
		$sql = $selectSql . $fromSql . $whereSql . $sortSql . $groupSql . $limitSql;
		$res = $this->DB->get_Sql($sql);
        
        foreach ($res as $k => $i){
            $res[$k] = $this->dispatchItemGroup($i);
        }

		return $res;
    }

    function dispatchItemGroup($itemGroup){
        $storage_qty = 0;
        $ava_qty = 0;
        $grp_id = $itemGroup['grp_id'];
        
        $sql = "SELECT igc.*, i.inhouse_name_tc, i.inhouse_name_en, i.gift_name_tc, i.gift_name_en, i.zuri_name_tc, i.zuri_name_en, i.ltp_name_tc, i.ltp_name_en, i.item_img, i.sku, i.qty as storage_qty, i.various_item_id, igc.qty as consume, i.is_unlimited_qty, igc.remarks as item_remarks, IFNULL(i.remarks,'') AS remarks
                FROM `tbl_item_grp_content` AS igc
                LEFT JOIN `tbl_item` AS i ON i.item_id = igc.item_id
                WHERE igc.activate = 'Y' AND igc.grp_id = '$grp_id'";
        $itemList = $this->DB->get_Sql($sql);
        $bool_client_optional = 'N';

        foreach ($itemList as $k => $item){
            if ($item['is_client_optional'] == 'Y'){
                $bool_client_optional = 'Y';
            }

            $bool_parent_exist = false;    
            $itemDtl = $this->getItemDtl($item['item_id']);
            $itemList[$k]['item'] = $itemDtl;
            $i_ava_qty = floor($itemDtl['ava_qty'] / $item['consume']);
            if ($i_ava_qty >0){
                $ava_qty += $i_ava_qty;
            }
            
            if ($item['various_item_id']){
                $item["parent_item"] = $this->getItemDtl($item['various_item_id']);
            } else {
                $item['parent_item'] = null;
            }
            if (!$bool_parent_exist){
                $storage_qty += $item['storage_qty'];
            }
        }

        $itemGroup['is_client_optional'] = $bool_client_optional;
        $itemGroup['storage_qty'] = $storage_qty;
        $itemGroup['item_list'] = $itemList;
        $itemGroup['ava_qty'] = $ava_qty;
        
        return $itemGroup;
    }

    function getItemList($filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by
        $fromShort = "i";
		$selectSql = "SELECT $fromShort.* ";
		$fromSql = " FROM `tbl_item` as $fromShort";
		$whereSql = " WHERE $fromShort.`activate` = 'Y'";
        $groupSql = " GROUP BY $fromShort.`item_id`";
		if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
			$whereSql = " WHERE 1=1";

		if ($sorting)
			$sortSql = " ORDER BY ". $sorting['sort'] . " " . $sorting['order'];
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
				if ($i){
					switch ($k){
                        case 'supplier_id':
                            $whereSql .= " AND ($fromShort.supplier_id = '$i')";
                            break;
                        case 'item_id':
                            $whereSql .= " AND ($fromShort.item_id = '$i')";
                            break;
                        case 'various_item_id':
                            $whereSql .= " AND ($fromShort.various_item_id = '$i')";
                            break;
                        case 'parent_level_only':
                            $whereSql .= " AND ($fromShort.`various_item_id` IS NULL)";
                            break;
                        case 'child_level_only':
                            $whereSql .= " AND ($fromShort.`various_item_id` IS NOT NULL)";
                            break;
                        case 'item_level_only':
                            $selectSql = "SELECT * 
                                            FROM (
                                                SELECT ip.*
                                                FROM `tbl_item` AS ip  
                                                LEFT JOIN `tbl_item` AS ic ON ic.various_item_id = ip.item_id 
                                                WHERE ip.various_item_id IS NULL AND ip.activate = 'Y' AND ic.item_id IS NULL
                                                UNION
                                            
                                                SELECT ic.*
                                                FROM `tbl_item` AS ip  
                                                LEFT JOIN `tbl_item` AS ic ON ic.various_item_id = ip.item_id 
                                                WHERE ic.various_item_id IS NOT NULL AND ip.activate = 'Y' AND ic.activate ='Y'
                                            ) AS $fromShort
                                            ORDER BY item_id ";
                            $fromSql = "";
                            $whereSql = "";
                            $sorting = "";
                            $limiter = "";
                            $groupSql = "";
                            break;
                        case 'sku':
                            $whereSql .= " AND ($fromShort.`sku` = '$i')";
                            break;
                        case 'cate_id_arr':
                            $whereSql .= " AND ($fromShort.`item_cate_id` IN ($i))";
                            break;
                        case 'skey':
                            //  SKU, 別稱，中文名稱（供應商），英文名稱（供應商），中文顯示名稱，英文顯示名稱，條碼，貨品編號（供應商），標籤
                            $fromSql .= " LEFT JOIN `tmap_item_tag` AS m ON m.`item_id` = $fromShort.`item_id` AND m.`activate` = 'Y' 
                                            LEFT JOIN `tbl_item_tag` AS t ON t.`tag_id` = m.`tag_id` AND t.`activate` ='Y'
                                            LEFT JOIN `tmap_item_various_val` AS mvv ON mvv.`item_id` = m.`item_id` AND mvv.`activate` = 'Y'
                                            LEFT JOIN `tbl_item_cate_various_val` AS icvv ON icvv.`val_id` = mvv.`val_id`
                                            ";
                            $whereSql .= " AND ($fromShort.`sku` = '$i' OR $fromShort.`supplier_barcode` LIKE '%$i%' OR $fromShort.`supplier_stock_code` LIKE '%$i%' 
                                                OR $fromShort.`supplier_name_tc` LIKE '%$i%' OR $fromShort.`supplier_name_en` LIKE '%$i%'
                                                OR $fromShort.`inhouse_name_tc` LIKE '%$i%' OR $fromShort.`inhouse_name_tc` LIKE '%$i%' 
                                                OR $fromShort.`gift_name_tc` LIKE '%$i%' OR $fromShort.`gift_name_en` LIKE '%$i%' 
                                                OR $fromShort.`zuri_name_tc` LIKE '%$i%' OR $fromShort.`zuri_name_en` LIKE '%$i%' 
                                                OR $fromShort.`ltp_name_tc` LIKE '%$i%' OR $fromShort.`ltp_name_en` LIKE '%$i%' 
                                                OR t.`tag_name_tc` LIKE '%$i%' OR t.`tag_name_en` LIKE '%$i%'
                                                OR icvv.`name_tc` LIKE '%$i%' OR icvv.`name_en` LIKE '%$i%' OR icvv.`val` = '$i'
                                                )";

                            break;
                        
					}
				}
			}
		}
		
		$sql = $selectSql . $fromSql . $whereSql . $groupSql . $sortSql . $limitSql;
		$row = $this->DB->get_Sql($sql);
        $res = [];
        foreach ($row as $k => $i){
            $item = $this->dispatchItem($i);

            // after dispatch filter
            if ($filter){
                foreach ($filter as $k => $i){
                    if ($i){
                        switch ($k){
                            case 'is_out_of_stock_alert':
                                if ($item['is_out_of_stock_alert'] != $i){
                                    $item = null;
                                }
                                break;
                        }
                    }
                }
            } 

            if ($item)
                array_push($res, $item);
        }

		return $res;
    }

    function getItemDtl($item_id){
        if ($item_id)
            return $this->getItemList(["item_id" => $item_id], $this->DB->getUnique)[0];
        else
            return null;
    }
    
    function getItemDtlBySku($sku){
        if ($sku)
            return $this->getItemList(["sku" => $sku], $this->DB->getUnique)[0];
        else
            return null;
    }

    private function stock_out($out, $location_id){
        $out["updated_by_id"] = $_SESSION['admin']['admin_id'];
        $this->DB->insert_db('tbl_item_stock_out', $out);

        require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin); 
        
        $warehouseCls->unloadItem($location_id, $out['item_id'], $out['qty']);
    }
 
    private function stock_in($in, $location_id = 1){ // location_id:1 || 未上架location
        $in["updated_by_id"] = $_SESSION['admin']['admin_id'];
        $this->DB->insert_db('tbl_item_stock_in', $in);

        $itemDtl = $this->getItemDtl($in['item_id']);

        if ($in['avg_price'] > $itemDtl['cost']){
            $this->editItem([
                "item_id" => $in['item_id'],
                "cost" => $in['avg_price']
            ]);
        }

        require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin); 

        $warehouseCls->loadItem($location_id, $itemDtl['item_id'], $in['qty']); 
    }

    function getItemTagList($filter = null, $sorting = null, $limiter = null){
		$fromShort = "mit";
		$selectSql = "SELECT $fromShort.* ";
		$fromSql = " FROM `tmap_item_tag` as $fromShort";
		$whereSql = " WHERE $fromShort.`activate` = 'Y'";
        $groupSql = " GROUP BY $fromShort.`tag`";
		if ($sorting)
			$sortSql = " ORDER BY ". $sorting['sort'] . " " . $sorting['order'];
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
				if ($i){
					switch ($k){
                        case 'tag':
                            $whereSql .= " AND ($fromShort.`tag` = '$i')";
                            break;
                        case 'item_id':
                            $whereSql .= " AND ($fromShort.`item_id` = '$i')";
                            break;
					}
				}
			}
		}

		
		$sql = $selectSql . $fromSql . $whereSql . $groupSql . $sortSql . $limitSql;
		$res = $this->DB->get_Sql($sql);

		return $res;
	}

    function getSkuTagList($filter = null, $sorting = null, $limiter = null){
		$fromShort = "mst";
		$selectSql = "SELECT $fromShort.* ";
		$fromSql = " FROM `tmap_sku_tag` as $fromShort";
		$whereSql = " WHERE $fromShort.`activate` = 'Y'";
        $groupSql = " GROUP BY $fromShort.`tag`";
		if ($sorting)
			$sortSql = " ORDER BY ". $sorting['sort'] . " " . $sorting['order'];
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
				if ($i){
					switch ($k){
                        case 'tag':
                            $whereSql .= " AND ($fromShort.`tag` = '$i')";
                            break;
                        case 'sku_id':
                            $whereSql .= " AND ($fromShort.`sku_id` = '$i')";
                            break;
					}
				}
			}
		}

		
		$sql = $selectSql . $fromSql . $whereSql . $groupSql . $sortSql . $limitSql;
		$res = $this->DB->get_Sql($sql);

		return $res;
	}

    function addItemTag($data){
        $this->DB->insert_db("tmap_item_tag", $data);
        return true;
    }

    function addSkuTag($data){
        $this->DB->insert_db("tmap_sku_tag", $data);
        return true;
    }

    function dispatchItem($item){
        $item_id = $item['item_id'];
        require_once ROOT.'/include/class/itemCountry.class.php';
		$countryCls = new itemCountry($this->DB, $this->lang, $this->isAdmin);
        $item['country'] = $countryCls->getCountryDtl($item["country_id"]);

        require_once ROOT.'/include/class/itemBrand.class.php';
		$brandCls = new itemBrand($this->DB, $this->lang, $this->isAdmin);
        $item['brand'] = $brandCls->getBrandDtl($item["brand_id"]);

		require_once ROOT.'/include/class/supplier.class.php';
		$supplierCls = new supplier($this->DB, $this->lang, $this->isAdmin);
        $item['supplier'] = $supplierCls->getSupplierDtl($item['supplier_id']);

        require_once ROOT.'/include/class/itemCate.class.php';
		$itemCateCls = new itemCate($this->DB, $this->lang, $this->isAdmin);
        $item['cate'] = $itemCateCls->getItemCateDtl($item['item_cate_id']);

        require_once ROOT.'/include/class/itemTag.class.php';
		$itemTagCls = new itemTag($this->DB, $this->lang, $this->isAdmin);
        $item['tag_list'] = $itemTagCls->getItemTagList(["item_id" => $item['item_id']]);

        $sql = "SELECT * 
                FROM `tmap_item_various_val` AS m 
                LEFT JOIN `tbl_item_cate_various_val` AS v ON m.`val_id` = v.`val_id` AND v.`activate` ='Y'
                WHERE m.`item_id` = '".$item['item_id']."'";
        $item['various_list'] = $this->DB->get_Sql($sql);


        $sql = "SELECT odi.map_id, odi.qty, odi.packed_qty, odi.qty - odi.packed_qty  AS net_qty, odi.item_id
                FROM `tbl_order_deli_item` AS odi
                LEFT JOIN `tbl_order_deli` AS od ON odi.deli_id = od.deli_id AND od.activate = 'Y'
                LEFT JOIN `tbl_order` AS o ON o.order_id = od.order_id
                LEFT JOIN `tbl_item` AS i ON i.activate = 'Y' AND (i.item_id = '$item_id' OR i.various_item_id = '$item_id') AND i.item_id = odi.item_id
                WHERE odi.activate = 'Y' AND i.item_id IS NOT NULL AND ((o.order_status IN ('incart', 'waitingPayment') AND reserve_time > NOW()) OR o.order_status NOT IN ('incart', 'waitingPayment'))";
        $odi_list = $this->DB->get_sql($sql);
        
        $ava_qty = $item['qty'];
        $reserved_qty = 0;
        foreach ($odi_list as $odi){
            $ava_qty -= $odi['net_qty'];
            $reserved_qty += $odi['net_qty'];
        }
        $item['ava_qty'] = $ava_qty;
        $item['reserved_qty'] = $reserved_qty;

        if ($ava_qty < $item['alert_qty'])
            $item['is_out_of_stock_alert'] = 'Y';
        else
            $item['is_out_of_stock_alert'] = 'N';

        if (!$this->callback || in_array('location', $this->callback)){
            require_once ROOT.'/include/class/warehouse.class.php';
            $warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);
            
            $location_list = $warehouseCls->getLocationList(["item_id" => $item_id]);

            $item['location_list'] = $location_list;
        }

        $sql = "SELECT * 
                FROM `tmap_item_barcode`
                WHERE item_id = $item_id";
        $barcode = $this->DB->get_Sql($sql);
        array_push($barcode, "ITEMID:$item_id");
        array_push($barcode, $item['supplier_barcode']);
        $item['barcode'] = $barcode;
        
        return $item;
    }

    function delItemTag($item_id, $tag){
        $sql = "UPDATE `tmap_item_tag` set `activate` = 'N' WHERE `item_id` = '$item_id' AND `tag` = '$tag'";
        $this->DB->update($sql);
        return true;
    }

    function delSkuTag($sku_id, $tag){
        $sql = "UPDATE `tmap_sku_tag` set `activate` = 'N' WHERE `sku_id` = '$sku_id' AND `tag` = '$tag'";
        $this->DB->update($sql);
        return true;
    }

    function stockInStockOutSumUpList($filter = null, $limiter = null, $sorting = null){
        $isoWhere = "WHERE iso.activate = 'Y'";
        $isiWhere = "WHERE isi.activate = 'Y'";
        if ($filter){
			foreach ($filter as $k => $i){
				if ($i){
					switch ($k){
                        case 'item_id_arr':
                            $sql = "SELECT GROUP_CONCAT(item_id) as item_id_arr
                                    FROM `tbl_item` AS i
                                    WHERE activate = 'Y' AND various_item_id in ($i) OR item_id in ($i)";
                            $item_id_arr = $this->DB->get_Sql($sql)[0]['item_id_arr'];
                            $isoWhere .= " AND iso.item_id in ($item_id_arr)";
                            $isiWhere .= " AND isi.item_id in ($item_id_arr)";
                            break;
                        case 'type':
                            $isoWhere .= " AND iso.type in ($i)";
                            $isiWhere .= " AND p.type in ($i)";
                            break;
                        case 'start_date':
                            $isoWhere .= " AND iso.create_datetime >= '$i'";
                            $isiWhere .= " AND p.create_datetime >= '$i'";
                            break;
                        case 'end_date':
                            $isoWhere .= " AND iso.create_datetime <= '$i'";
                            $isiWhere .= " AND p.create_datetime <= '$i'";
                            break;
					}
				}
			}
		}

        
        $ioFromSql = " SELECT isi.item_id, isi.qty, isi.create_datetime, 'stock_in' AS `io`, p.`type`, p.remarks, p.updated_by_id, p.ref_no
                    FROM `tbl_item_stock_in` AS isi
                    LEFT JOIN `tbl_purchase` AS p ON p.po_id = isi.po_id 
                    $isiWhere
                    UNION 
                    SELECT item_id, qty *-1 AS qty, create_datetime, 'stock_out' AS `io`, `type`, remarks, iso.updated_by_id, iso.ref_no
                    FROM `tbl_item_stock_out` AS iso 
                    $isoWhere";

        $sql = "SELECT * 
                FROM (
                    SELECT SUM(CASE WHEN io.qty<0 THEN io.qty ELSE 0 END) * -1  AS stock_out_qty,
                            SUM(CASE WHEN io.qty>=0 THEN io.qty ELSE 0 END) AS stock_in_qty,
                            SUM(io.qty) AS net_qty, 
                            i.inhouse_name_tc, 
                            io.item_id, 
                            i.various_item_id, i.supplier_stock_code,
                            'child' AS type
                    FROM (
                    $ioFromSql
                    ) AS io
                    LEFT JOIN `tbl_item` AS i ON i.item_id = io.item_id
                    LEFT JOIN `tbl_admin` AS a ON a.admin_id = io.updated_by_id
                    GROUP BY io.item_id

                    UNION 

                    SELECT SUM(CASE WHEN io.qty<0 THEN io.qty ELSE 0 END) * -1  AS stock_out_qty,
                            SUM(CASE WHEN io.qty>=0 THEN io.qty ELSE 0 END) AS stock_in_qty,
                            SUM(io.qty) AS net_qty, 
                            IFNULL(pi.inhouse_name_tc, i.inhouse_name_tc) AS inhouse_name_tc, 
                            IFNULL(pi.item_id, i.item_id) AS item_id, 
                            i.various_item_id,
                            IFNULL(pi.supplier_stock_code, i.supplier_stock_code) AS supplier_stock_code,
                            'parent' AS type
                    FROM (
                    $ioFromSql
                    ) AS io
                    LEFT JOIN `tbl_item` AS i ON i.item_id = io.item_id
                    LEFT JOIN `tbl_item` AS pi ON pi.item_id = i.various_item_id
                    LEFT JOIN `tbl_admin` AS a ON a.admin_id = io.updated_by_id
                    GROUP BY IFNULL(i.various_item_id, i.item_id)
                ) AS src
        
                ORDER BY src.item_id
                ";

        if ($limiter){
            $sql .= "limit $limiter";
        }

        $row = $this->DB->get_Sql($sql);
        return $row;
    }

    function getStockInStockOutList($filter = null, $limiter = null, $sorting = null){
        $isoWhere = "WHERE iso.activate = 'Y'";
        $isiWhere = "WHERE isi.activate = 'Y'";
        $odiWhere = "WHERE odi.activate = 'Y' AND packed_qty > 0";
        if ($filter){
			foreach ($filter as $k => $i){
				if ($i){
					switch ($k){
                        case 'item_id_arr':
                            $sql = "SELECT GROUP_CONCAT(item_id) as item_id_arr
                                    FROM `tbl_item` AS i
                                    WHERE activate = 'Y' AND various_item_id in ($i) OR item_id in ($i)";
                            $item_id_arr = $this->DB->get_Sql($sql)[0]['item_id_arr'];
                            $isoWhere .= " AND iso.item_id in ($item_id_arr)";
                            $isiWhere .= " AND isi.item_id in ($item_id_arr)";
                            $odiWhere .= " AND odi.item_id in ($item_id_arr)";
                            break;
                        case 'type':
                            $isoWhere .= " AND iso.type in ($i)";
                            $isiWhere .= " AND p.type in ($i)";
                            break;
                        case 'start_date':
                            $isoWhere .= " AND iso.create_datetime >= '$i'";
                            $isiWhere .= " AND p.create_datetime >= '$i'";
                            $odiWhere .= " AND od.deli_date >= '$i'";
                            break;
                        case 'end_date':
                            $isoWhere .= " AND iso.create_datetime <= '$i'";
                            $isiWhere .= " AND p.create_datetime <= '$i'";
                            $odiWhere .= " AND od.deli_date <= '$i'";
                            break;
					}
				}
			}
		}

        $sql = "SELECT io.*, i.inhouse_name_tc, a.username, i.supplier_stock_code
                FROM (
                    SELECT isi.item_id, isi.qty, isi.create_datetime, 'stock_in' as `io`, p.`type`, p.remarks, p.updated_by_id, p.ref_no, null AS deli_id
                    FROM `tbl_item_stock_in` AS isi
                    LEFT JOIN `tbl_purchase` AS p ON p.po_id = isi.po_id 
                    $isiWhere
                    UNION 
                    SELECT item_id, qty *-1 AS qty, create_datetime, 'stock_out' as `io`, `type`, remarks, iso.updated_by_id, iso.ref_no, null AS deli_id
                    FROM `tbl_item_stock_out` AS iso 
                    $isoWhere
                    UNION
                    SELECT item_id, packed_qty *-1 AS qty, od.deli_date, 'stock_out' AS `io`, '訂單出庫' AS type, remarks, odi.updated_by_id, CONCAT(od.deli_no, ' (', p.gift_product_name_tc,')') AS ref_no, od.deli_id AS deli_id
                    FROM `tbl_order_deli_item` AS odi
                    LEFT JOIN `tbl_order_deli` AS od ON od.deli_id = odi.deli_id
                    LEFT JOIN `tbl_product` AS p ON p.product_id = od.product_id
                    $odiWhere
                ) AS io
                LEFT JOIN `tbl_item` AS i ON i.item_id = io.item_id
                LEFT JOIN `tbl_admin` AS a ON a.admin_id = io.updated_by_id
                ";

        if ($sorting){
            $sql .= "ORDER BY $sorting";
        }

        if ($limiter){
            $sql .= "limit $limiter";
        }

        // echo $sql;

        $row = $this->DB->get_Sql($sql);
        return $row;
    }

    function getOutOfStockList($shop_id = 1){
        $prefix = $this->getPrefix($shop_id);
        $itemList = $this->getItemList(["is_out_of_stock_alert" => "Y"]);

        foreach($itemList as $k => $item){
            $sql = "SELECT pig.*, p.$prefix"."_product_code AS product_code, p.$prefix"."_product_name_tc AS product_name_tc, p.$prefix"."_is_force_checkout AS is_force_checkout, p.$prefix"."_force_checkout_start_date AS force_checkout_start_date, p.$prefix"."_force_checkout_end_date AS force_checkout_end_date, ig.inhouse_name_tc
                    FROM tbl_product_item_group AS pig
                    LEFT JOIN `tbl_product` AS p ON pig.product_id = p.product_id 
                    LEFT JOIN `tbl_item_grp` AS ig ON ig.grp_id = pig.grp_id
                    WHERE pig.activate = 'Y' AND pig.grp_id IN (
                        SELECT igc.grp_id
                        FROM `tbl_item_grp_content` as igc
                        WHERE igc.item_id = ".$item['item_id']." AND igc.activate ='Y'
                    ) AND p.activate = 'Y' AND pig.is_force_checkout = 'N'
                    GROUP BY pig.product_id";

            $itemList[$k]['product_list'] = $this->DB->get_Sql($sql);
        }
        
        return $itemList;
    }

    function getItemProductList($item, $shop_id = 1){
        $prefix = $this->getPrefix($shop_id);
        $sql = "SELECT pig.*, p.$prefix"."_product_code AS product_code, p.$prefix"."_product_name_tc AS product_name_tc, ig.inhouse_name_tc
            FROM tbl_product_item_group AS pig
            LEFT JOIN `tbl_product` AS p ON pig.product_id = p.product_id 
            LEFT JOIN `tbl_item_grp` AS ig ON ig.grp_id = pig.grp_id
            WHERE pig.activate = 'Y' AND pig.grp_id IN (
                SELECT igc.grp_id
                FROM `tbl_item_grp_content` as igc
                WHERE igc.item_id = ".$item['item_id']." AND igc.activate ='Y'
            ) AND p.activate = 'Y' AND pig.is_force_checkout = 'N'
            GROUP BY pig.product_id";
        return $this->DB->get_Sql($sql);
    }
}
?>