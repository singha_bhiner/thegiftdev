<?php
/* 
    Objective: point to gift related
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_gift

        
*/


require_once dirname(__FILE__). '/abstract.class.php';
class gift extends baseClass{
    function init(){
        $this->multiLangFieldArr = ['name'];

        $this->deliStatusMapping = [
            "cancelled" => "已取消",
            "completed" => "已完成",
            "requested" => "已下單"
        ];
    }

    function editGiftDeli($deli_id, $data){
        if ($deli_id == 0)
            $deli_id = $this->DB->insert_db("tbl_gift_deli", null);
        
        $sql = $this->DB->buildSql_update("tbl_gift_deli", "deli_id", $deli_id, $data);
        $this->DB->update($sql);

        return $this->getGiftDeliList();
    }

    function getGiftDeliList($filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: which shop in use
        // $filter: [obj] not in use
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $sql = "SELECT d.*, m.name, m.mobile, m.email, g.name_tc
                FROM `tbl_gift_deli` AS d 
                LEFT JOIN `tbl_member` AS m ON m.member_id = d.member_id
                LEFT JOIN `tbl_gift` AS g ON g.gift_id = d.gift_id
                WHERE d.`activate` ='Y'";

        foreach($filter AS $k => $i){
            if ($i){
                switch($k){
                    case 'skey':
                        $sql .= " AND (d.`remarks` LIKE '%$i%' OR m.`member_no` LIKE '%$i%' OR m.`name` LIKE '%$i%' OR m.`email` LIKE '%$i%' OR m.`mobile` LIKE '%$i%')";
                        break;
                    case 'deli_status':
                        $i = "'" . implode("','", explode(",", $i)   ) . "'";
                        $sql .= "&& (deli_status in ($i)) ";
                        break;
                }
            }
        }

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit']; 
        }

        $res = $this->DB->get_Sql($sql);

        foreach($res as $k => $row){
            $res[$k]['deliStatusDisplay'] = $this->deliStatusMapping[$row['deli_status']];
        }

        return $res;
    }

    function executeGift($gift_id, $member_id){
        

        $sql = "SELECT * FROM `tbl_gift` WHERE gift_id = $gift_id AND quota > 0 LIMIT 1"; 
        $gift = $this->DB->get_Sql($sql)[0];

        if ($gift){
            $sql = "SELECT m.* 
                    FROM `tbl_member` AS m 
                    WHERE m.member_id = '$member_id'";
            $member = $this->DB->get_Sql($sql)[0];
            if( $member['point'] < $gift['required_point']){
                new apiDataBuilder(17);
            }

            $this->DB->adjustFloatVal("tbl_gift", "gift_id", $gift_id, "quota", -1); // adjust the gift quota
            $this->DB->adjustFloatVal("tbl_member", "member_id", $member_id, "point", $gift['required_point'] * -1); // adjust 
            $this->DB->insert_db("tbl_gift_deli", [
                "member_id" => $member_id, 
                "gift_id" => $gift_id,
                "deli_status" => 'requested'
            ]);
        } else {
            new apiDataBuilder(15);
        }


        return true;
    }

    function editGift($gift_id, $data){
        // edit gift
        // $gift_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        $gift_img = $data['gift_img'];
        unset($data['gift_img']);

        if ($gift_id == 0)
            $gift_id = $this->DB->insert_db('tbl_gift', $data, null);

        if ($gift_img){
            $data['gift_img'] = $this->DB->uploadImg($gift_img, "gift", $gift_id);
        }
        
        $sql = $this->DB->buildSql_update("tbl_gift", "gift_id", $gift_id, $data);
        $this->DB->update($sql);

        return $this->getGiftDtl($gift_id);
    }

    function getGiftList($shop_id, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: which shop in use
        // $filter: [obj] not in use
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $sql = "SELECT * ";
        foreach($this->multiLangFieldArr AS $field){
            $sql .= ", $field" . "_" . $this->lang . " AS $field ";
        }
        $sql .= "FROM `tbl_gift` WHERE `activate` ='Y'";


        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit']; 
        }
        $res = $this->DB->get_Sql($sql);

        return $res;
    }

    function getGiftDtl($gift_id){
        // get the gift Detail
        // $gift_id: PK

        $sql = "SELECT *
                FROM `tbl_gift` AS v
                WHERE v.`activate` = 'Y' && v.`gift_id` = '$gift_id'";
        $res = $this->DB->get_Sql($sql)[0];

        return $res;
    }
}
?>