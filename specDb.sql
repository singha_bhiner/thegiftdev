-- prefix list
-- lotaipo: ltp
-- the gift: gift
-- little zuri: zuri

DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
    `admin_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `username` VARCHAR(20) DEFAULT NULL,
    `email` VARCHAR(100) DEFAULT NULL,
    `password` VARCHAR(32) DEFAULT NULL,
    `two_factor_code` VARCHAR(20) DEFAULT NULL,
    `last_login_on` datetime DEFAULT NULL,
    `last_login_ip` VARCHAR(30) DEFAULT NULL,
    `last_login_device` VARCHAR(255) DEFAULT NULL,
    `last_login_location` VARCHAR(30) DEFAULT NULL,
    `role_id` VARCHAR(10) DEFAULT NULL,
    `suspended` VARCHAR(1) DEFAULT 'N',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `default_shop_id` INT(11) DEFAULT 1,
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_order_deposit_slip`;
CREATE TABLE `tbl_order_deposit_slip` (
    `slip_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) DEFAULT NULL,
    `order_no` VARCHAR(64) DEFAULT NULL,
    `order_id` INT(11) DEFAULT NULL,
    `submitter_name` VARCHAR(255) DEFAULT NULL,
    `phone` INT(11) DEFAULT NULL,
    `deposit_slip` VARCHAR(128) DEFAULT NULL,
    `is_confirmed` VARCHAR(1) DEFAULT 'N',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_order_excel`;
CREATE TABLE `tbl_order_excel` (
    `excel_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) DEFAULT NULL,
    `order_no` VARCHAR(64) DEFAULT NULL,
    `order_id` INT(11) DEFAULT NULL,
    `excel_path` VARCHAR(128) DEFAULT NULL,
    `name` VARCHAR(255) DEFAULT NULL,
    `email` VARCHAR(255) DEFAULT NULL,
    `phone` VARCHAR(255) DEFAULT NULL,
    `is_confirmed` VARCHAR(1) DEFAULT 'N',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_order_deli`;
CREATE TABLE `tbl_order_deli` (
    `deli_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `deli_no` VARCHAR(64) DEFAULT NULL,
    `shop_id` INT(11) DEFAULT NULL,
    `order_id` INT(11) DEFAULT NULL,
    `deli_status` VARCHAR(255) DEFAULT NULL, -- INCART, ....  PACKING, 運輸中，已簽收，確認完成
    `product_id` INT(11) NOT NULL,
    `recipient_name` VARCHAR (255) DEFAULT NULL,
    `recipient_phone` VARCHAR (255) DEFAULT NULL,
    `recipient_company` VARCHAR (255) DEFAULT NULL,
    `card_id` INT(11) DEFAULT NULL,
    `card_head` VARCHAR(255) DEFAULT NULL,
    `card_body` VARCHAR(255) DEFAULT NULL,
    `card_sign` VARCHAR(255) DEFAULT NULL,
    `card_json` LONGTEXT DEFAULT NULL,
    `card_activate` VARCHAR(1) DEFAULT 'Y',
    `card_completed` VARCHAR(1) DEFAULT 'N',
    `card_img_path` VARCHAR(255) DEFAULT NULL,
    `deli_region_id` INT(11) DEFAULT NULL,
    `deli_address` LONGTEXT DEFAULT NULL,
    `deli_date` DATE DEFAULT NULL,
    `deli_timeslot_id` INT(11) DEFAULT NULL,
    `deli_before_photo_path` VARCHAR(255) DEFAULT NULL,
    `deli_after_photo_path` VARCHAR(255) DEFAULT NULL,
    `acknowledgement` VARCHAR(255) DEFAULT NULL,
    `product_price` INT(11) NOT NULL,
    `addon_price` INT(11) NOT NULL,
    `shipping_fee` INT(11) NOT NULL,
    `timeslot_fee` INT(11) NOT NULL,
    `deli_price` INT(11) NOT NULL,
    `remarks_internal` LONGTEXT DEFAULT NULL,
    `remarks_external` LONGTEXT DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_order_deli_item`;
CREATE TABLE `tbl_order_deli_item` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `deli_id` INT(11) NOT NULL,
    `sku_id` INT(11) NOT NULL,
    `qty` INT(11) NOT NULL,
    `packed_qty` INT(11) NOT NULL,
    `isClientOption` VARCHAR(1) DEFAULT 'N',
    `isAddon` VARCHAR(1) DEFAULT 'N',
    `remarks` LONGTEXT DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_order_deli_addon`;
CREATE TABLE `tbl_order_deli_addon` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `deli_id` INT(11) NOT NULL,
    `product_id` INT(11) NOT NULL,
    `qty` INT(11) NOT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_order`;
CREATE TABLE `tbl_order` (
    `order_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) NOT NULL, 
    `order_status` VARCHAR(255) DEFAULT '', -- waitingPayment, paid, deliProcess, complete   等待收款，已付款， 。。。 ，已進入運輸流程，已完成    
    `order_no` VARCHAR (64) NOT NULL, -- invoice
    `member_id` INT(11) DEFAULT NULL,
    `sales_admin_id` INT(11) DEFAULT NULL,
    `sender_name` VARCHAR (255) DEFAULT NULL,
    `sender_company` VARCHAR(255) DEFAULT NULL,
    `sender_email` VARCHAR (255) DEFAULT NULL,
    `sender_phone` VARCHAR (255) DEFAULT NULL,
    `product_price` INT(11) DEFAULT NULL,
    `shipping_fee` INT(11) NOT NULL,
    `timeslot_fee` INT(11) NOT NULL,
    `cash_dollar_used` INT(11) DEFAULT 0,
    `coupon_discount` INT(11) DEFAULT 0,
    `voucher_discount` INT(11) DEFAULT 0,
    `membership_discount` INT(11) DEFAULT 0,
    `coupon_code_id` INT(11) DEFAULT NULL,
    `coupon_code` VARCHAR(64) DEFAULT NULL,
    `member_voucher_id` INT(11) DEFAULT NULL,
    `final_price` INT(11) NOT NULL,
    `point_gain` INT(11) DEFAULT 0,
    `cash_dollar_gain` INT(11) DEFAULT 0,
    `exp_gain` INT(11) DEFAULT 0,
    `remarks_from_client` LONGTEXT DEFAULT NULL,
    `remarks_internal` LONGTEXT DEFAULT NULL,
    `remarks_external` LONGTEXT DEFAULT NULL,
    `excel_path` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `order_datetime` TIMESTAMP DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_order_payment`;
CREATE TABLE `tbl_order_payment` (
    `payment_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `order_id` INT(11) DEFAULT NULL,
    `payment_status` VARCHAR(255) DEFAULT NULL,k
    `payment_method` VARCHAR(255) DEFAULT NULL,
    `transaction_id` VARCHAR(255) DEFAULT NULL,
    `bankslip_path` VARCHAR(255) DEFAULT NULL,
    `raw_return` LONGTEXT DEFAULT NULL,
    `expected_payment_value` INT(11) DEFAULT NULL,
    `actual_payment_value` INT(11) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_admin_role`;
CREATE TABLE `tbl_admin_role` (
    `role_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `adm_name` VARCHAR(20) DEFAULT NULL,
    `gift_right` LONGTEXT DEFAULT NULL,
    `zuri_right` LONGTEXT DEFAULT NULL,
    `ltp_right` LONGTEXT DEFAULT NULL,
    `mobile_right` VARCHAR(1) DEFAULT 'Y',
    `driver_right` VARCHAR(1) DEFAULT 'Y',
    `stocktake_approver_right` VARCHAR(1) DEFAULT 'Y',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_greeting_cate`;
CREATE TABLE `tbl_greeting_cate` (
    `cate_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) NOT NULL,
    `name_tc` VARCHAR(255) DEFAULT NULL, 
    `name_en` VARCHAR(255) DEFAULT NULL, 
    `sort` INT(11) NOT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_greeting_subcate`;
CREATE TABLE `tbl_greeting_subcate` (
    `subcate_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `parent_cate_id` INT(11) NOT NULL,
    `shop_id` INT(11) NOT NULL,
    `name_tc` VARCHAR(255) DEFAULT NULL, 
    `name_en` VARCHAR(255) DEFAULT NULL, 
    `sort` INT(11) NOT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_greeting`;
CREATE TABLE `tbl_greeting` (
    `greeting_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) NOT NULL,
    `subcate_id` INT(11) NOT NULL,
    `msg` VARCHAR(255) DEFAULT NULL, 
    `sort` INT(11) NOT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_greeting_group`;
CREATE TABLE `tbl_greeting_group` (
    `greeting_group_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `name_tc` VARCHAR(255) DEFAULT NULL,
    `name_en` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_greeting_group`;
CREATE TABLE `tmap_greeting_group` ( 
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `cate_id` INT(11) DEFAULT NULL,
    `greeting_group_id` INT(11) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_coupon_productBlackList`;
CREATE TABLE `tmap_coupon_productBlackList` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `coupon_id` INT(11) DEFAULT NULL, 
    `product_id` INT(11) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_coupon_productWhiteList`;
CREATE TABLE `tmap_coupon_productWhiteList` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `coupon_id` INT(11) DEFAULT NULL, 
    `product_id` INT(11) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_coupon_cateBlackList`;
CREATE TABLE `tmap_coupon_cateBlackList` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `coupon_id` INT(11) DEFAULT NULL, 
    `cate_id` INT(11) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_coupon_cateWhiteList`;
CREATE TABLE `tmap_coupon_cateWhiteList` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `coupon_id` INT(11) DEFAULT NULL, 
    `cate_id` INT(11) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_greeting_product`;
CREATE TABLE `tmap_greeting_product` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `greeting_id` INT(11) DEFAULT NULL, 
    `product_id` INT(11) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_cate_greeting`;
CREATE TABLE `tmap_cate_greeting` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `greeting_cate_id` INT(11) DEFAULT NULL, 
    `cate_id` INT(11) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- DROP TABLE IF EXISTS `tmap_supplier_sku`;
-- CREATE TABLE `tmap_supplier_sku` (
--     `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
--     `supplier_id` INT(11) DEFAULT NULL, 
--     `sku_id` INT(11) DEFAULT NULL, 
--     `activate` VARCHAR(1) DEFAULT 'Y',
--     `updated_by_id` INT(11) DEFAULT NULL,
--     `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
-- 	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_cate`;
CREATE TABLE `tbl_cate` (
    `cate_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) NOT NULL, -- gift || ltp || zuri, in pattern: gift,ltp
    `parent_cate_id` INT(11) DEFAULT NULL,
    `name_tc` VARCHAR(255) DEFAULT NULL,
    `name_en` VARCHAR(255) DEFAULT NULL,
    `cate_banner` VARCHAR(255) DEFAULT NULL,
    `sort` INT(11) DEFAULT NULL,
    `show` VARCHAR(1) DEFAULT 'Y',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `log_product_item_content`;
CREATE TABLE `log_product_item_content` (
    `log_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `product_id` INT(11) DEFAULT NULL,
    `content_id` INT(11) DEFAULT NULL,
    `action` VARCHAR(64) DEFAULT NULL,
    `original_json` LONGTEXT DEFAULT NULL,
    `updated_json` LONGTEXT DEFAULT NULL,
    `updated_by_id` VARCHAR(255) DEFAULT NULL,
    `create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_sql_audit_log`;
CREATE TABLE `tbl_sql_audit_log` (
    `log_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `table` VARCHAR(64) DEFAULT NULL,
    `field` VARCHAR(64) DEFAULT NULL,
    `row_id` VARCHAR(255) DEFAULT NULL,
    `action` VARCHAR(64) DEFAULT NULL,
    `original_value` LONGTEXT DEFAULT NULL,
    `updated_value` LONGTEXT DEFAULT NULL,
    `updated_by_id` VARCHAR(255) DEFAULT NULL,
    `create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_member`;
CREATE TABLE `tbl_member` (
    `member_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `session_id` VARCHAR(128) DEFAULT NULL,
    `shop_id` INT(11) NOT NULL,
    `member_no` VARCHAR(255) DEFAULT NULL,
    `email` VARCHAR(255) DEFAULT NULL,
    `password` VARCHAR(255) DEFAULT NULL,
    `company_name` VARCHAR(255) DEFAULT NULL,
    `name` VARCHAR(255) DEFAULT NULL,
    `mobile` VARCHAR(255) DEFAULT NULL,
    `method` VARCHAR(255) DEFAULT NULL, -- google || facebook || N
    `verify` VARCHAR(1) DEFAULT 'N', 
    `sales_admin_id` INT(11) DEFAULT NULL,
    `remark` LONGTEXT DEFAULT NULL,
    `point` INT(11) DEFAULT 0,
    `cash_dollar` INT(11) DEFAULT 0,
    `exp` INT(11) DEFAULT 0,
    `language` VARCHAR(32) DEFAULT "tc",
    `membership_id` INT(11) DEFAULT NULL,
    `expiry_date` datetime DEFAULT NULL,
    `facebook_id` INT(11) DEFAULT NULL,
    `facebook_access_token` VARCHAR(255) DEFAULT NULL,
    `valid_code` VARCHAR(16) DEFAULT NULL,
    `pw_valid_code` VARCHAR(16) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_member_fav`;
CREATE TABLE `tbl_member_fav` (
    `fav_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `product_id` INT(11) NOT NULL,
    `member_id` INT(11) NOT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_member_product`;
CREATE TABLE `tmap_member_product` (
  `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `member_id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  `activate` VARCHAR(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` INT(11) DEFAULT NULL,
  `last_update` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_member_region`;
CREATE TABLE `tmap_member_region` (
  `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `member_id` INT(11) NOT NULL,
  `region_id` INT(11) NOT NULL,
  `activate` VARCHAR(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` INT(11) DEFAULT NULL,
  `last_update` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_member_timeslot`;
CREATE TABLE `tmap_member_timeslot` (
  `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `member_id` INT(11) NOT NULL,
  `slot_id` INT(11) NOT NULL,
  `activate` VARCHAR(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` INT(11) DEFAULT NULL,
  `last_update` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_role_product`;
CREATE TABLE `tmap_role_product` (
  `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `role_id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  `activate` VARCHAR(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` INT(11) DEFAULT NULL,
  `last_update` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_membership`;
CREATE TABLE `tbl_membership` (
    `membership_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) NOT NULL,
    `name_tc` VARCHAR(255) DEFAULT NULL,
    `name_en` VARCHAR(255) DEFAULT NULL,
    `discount` INT(11) DEFAULT 0,
    `upgrade_point` INT(11) DEFAULT 0,
    `upgrade_to_membership_id` INT(11) DEFAULT 0,
    `upgrade_period` INT(11) DEFAULT 0,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_cate_voucher`;
CREATE TABLE `tmap_cate_voucher`(
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `cate_id` INT(11) NOT NULL,
    `voucher_id` INT(11) NOT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_voucher`;
CREATE TABLE `tbl_voucher` (
    `voucher_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) NOT NULL,
    `name_tc` VARCHAR(255) DEFAULT NULL,
    `name_en` VARCHAR(255) DEFAULT NULL,
    `remarks` LONGTEXT DEFAULT NULL,
    `sales_admin_id` INT(11) DEFAULT NULL,
    `discount` INT(11) DEFAULT 0,
    `type` VARCHAR(1) NOT NULL DEFAULT 'A', -- A for amt || P for percentage off
    `minimum_order_amt` INT(11) DEFAULT 0,
    `ava_day` INT(11) DEFAULT 0, -- 0 for unlimit
    `expiry_date` datetime DEFAULT NULL, -- replace the existing the validity Period(Months)
    `redeem` VARCHAR(1) DEFAULT 'Y',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_coupon`;
CREATE TABLE `tbl_coupon` (
    `coupon_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) NOT NULL,
    `adm_name` VARCHAR(255) DEFAULT NULL,
    `discount` INT(11) NOT NULL,
    `type` VARCHAR(1) NOT NULL DEFAULT 'A', -- A for amt || P for percentage off
    `minimum_order_amt` INT(11) DEFAULT NULL,
    `code` VARCHAR(255) DEFAULT NULL,
    `expiry_date` datetime DEFAULT NULL,
    `allow_voucher` VARCHAR(1) NOT NULL DEFAULT 'Y',
    `member_only` VARCHAR(1) NOT NULL DEFAULT 'Y',
    `usage_limit` VARCHAR(255) DEFAULT NULL,
    `sales_admin_id` INT(11) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_coupon_email`;
CREATE TABLE `tbl_coupon_email` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `coupon_id` INT(11) NOT NULL,
    `email` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tmap_item_shop`;
CREATE TABLE `tmap_item_shop` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `item_id` INT(11) NOT NULL,
    `shop_id` INT(11) NOT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_popup`;
CREATE TABLE `tbl_popup` (
    `popup_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) NOT NULL,
    `adm_name` VARCHAR(255) NOT NULL,
    `content_tc` LONGTEXT DEFAULT NULL,
    `content_en` LONGTEXT DEFAULT NULL,
    `location` VARCHAR(255) DEFAULT NULL, 
    `expiry_datetime` TIMESTAMP DEFAULT NULL,
    `overlay` VARCHAR(1) DEFAULT 'Y',
    `show_in_web` VARCHAR(1) DEFAULT 'Y',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_popup_url`;
CREATE TABLE `tmap_popup_url` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `popup_id` INT(11) DEFAULT NULL, 
    `url` VARCHAR(512) NOT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_item_grp`;
CREATE TABLE `tbl_item_grp` (
    `grp_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `adm_name` VARCHAR(255) DEFAULT NULL,
    `alias` VARCHAR(255) NOT NULL,
    `inhouse_name_tc` VARCHAR(255) NOT NULL,
    `inhouse_name_en` VARCHAR(255) NOT NULL,
    `gift_name_tc` VARCHAR(255) DEFAULT NULL,
    `gift_name_en` VARCHAR(255) DEFAULT NULL,
    `zuri_name_tc` VARCHAR(255) DEFAULT NULL,
    `zuri_name_en` VARCHAR(255) DEFAULT NULL,
    `ltp_name_tc` VARCHAR(255) DEFAULT NULL,
    `ltp_name_en` VARCHAR(255) DEFAULT NULL,
    `is_tpl` VARCHAR(1) DEFAULT 'Y',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_item_grp_content`;
CREATE TABLE `tbl_item_grp_content` (
    `content_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `grp_id` INT(11) DEFAULT NULL,
    `item_id` INT(11) DEFAULT NULL,
    `qty` INT(11) DEFAULT 1,
    `additional_price` INT(11) DEFAULT 0,
    `is_client_optional` VARCHAR(1) DEFAULT 'Y',
    `is_auto_replace` VARCHAR(1) DEFAULT 'Y',
    `remarks` LONGTEXT DEFAULT NULL,
    `is_active` VARCHAR(1) DEFAULT 'Y',
    `sorting` INT(11) DEFAULT 0,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_item`;
CREATE TABLE `tbl_item` (
    `item_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `various_item_id` INT(11) DEFAULT NULL,
    `item_cate_id` INT(11) NOT NULL,
    `supplier_id` INT(11) NOT NULL,
    `brand_id` INT(11) DEFAULT NULL,
    `country_id` INT(11) DEFAULT NULL,
    `sku_prefix` VARCHAR(255) NOT NULL,
    `sku` VARCHAR(255) NOT NULL,
    `pattern` LONGTEXT NOT NULL,
    `supplier_name_tc` VARCHAR(255) NOT NULL,
    `supplier_name_en` VARCHAR(255) NOT NULL,
    `alias` VARCHAR(255) NOT NULL,
    `inhouse_name_tc` VARCHAR(255) NOT NULL,
    `inhouse_name_en` VARCHAR(255) NOT NULL,
    `gift_name_tc` VARCHAR(255) DEFAULT NULL,
    `gift_name_en` VARCHAR(255) DEFAULT NULL,
    `zuri_name_tc` VARCHAR(255) DEFAULT NULL,
    `zuri_name_en` VARCHAR(255) DEFAULT NULL,
    `ltp_name_tc` VARCHAR(255) DEFAULT NULL,
    `ltp_name_en` VARCHAR(255) DEFAULT NULL,
    `supplier_stock_code` VARCHAR(255) DEFAULT NULL,
    `supplier_barcode` VARCHAR(255) DEFAULT NULL,
    `unit` VARCHAR(255) DEFAULT NULL,
    `cost` INT(11) DEFAULT NULL,
    `stockin_price` INT(11) DEFAULT NULL,
    `suggested_price` VARCHAR(255) DEFAULT NULL,
    `alert_qty` INT(11) DEFAULT 0,
    `is_unlimited` VARCHAR(1) DEFAULT 'N',
    `remarks` LONGTEXT DEFAULT NULL,
    `item_img` VARCHAR(255) DEFAULT NULL, 
    `qty` INT(11) DEFAULT 0,
    `ava_qty` INT(11) DEFAULT 0,
    `reserved_qty` INT(11) DEFAULT 0,
    `is_unlimited_qty` VARCHAR(1) DEFAULT 'N',
    `is_product_page_show_qty` VARCHAR(1) DEFAULT 'Y',
    `replace_sku_id` INT(11) DEFAULT NULL,
    `is_replace_period` VARCHAR(1) DEFAULT 'N',
    `replace_period_start` TIMESTAMP DEFAULT NULL,
    `replace_period_end` TIMESTAMP DEFAULT NULL,
    `is_replace` VARCHAR(1) DEFAULT 'N',
    `various_activate` VARCHAR(1) DEFAULT 'Y',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `log_item`;
CREATE TABLE `log_item` (
    `log_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `item_id` INT(11) DEFAULT NULL,
    `action` VARCHAR(64) DEFAULT NULL,
    `original_json` LONGTEXT DEFAULT NULL,
    `updated_json` LONGTEXT DEFAULT NULL,
    `updated_by_id` VARCHAR(255) DEFAULT NULL,
    `create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_item_various_val`;
CREATE TABLE `tmap_item_various_val` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `item_id` INT(11) DEFAULT NULL,
    `val_id` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_item_cate`;
CREATE TABLE `tbl_item_cate` (
    `cate_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `parent_cate_id` INT(11) DEFAULT NULL,
    `cate_name_tc` VARCHAR(255) DEFAULT NULL,
    `cate_name_en` VARCHAR(255) DEFAULT NULL,
    `sort` INT(11) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- DROP TABLE IF EXISTS `tmap_item_cate_various_type`;
-- CREATE TABLE `tmap_item_cate_various_type` (
--     `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
--     `cate_id` INT(11) DEFAULT NULL,
--     `type_id` INT(11) DEFAULT NULL,
--     `activate` VARCHAR(1) DEFAULT 'Y',
--     `updated_by_id` INT(11) DEFAULT NULL,
--     `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
-- 	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tpl_item_cate_various_type`;
CREATE TABLE `tpl_item_cate_various_type` (
    `type_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `cate_id` INT(11) DEFAULT NULL,
    `name_tc` VARCHAR(255) DEFAULT NULL,
    `name_en` VARCHAR(255) DEFAULT NULL,
    `remarks` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tpl_item_cate_various_val`;
CREATE TABLE `tpl_item_cate_various_val` (
    `val_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `type_id` INT(11) DEFAULT NULL,
    `val` VARCHAR(255) DEFAULT NULL,
    `name_tc` VARCHAR(255) DEFAULT NULL,
    `name_en` VARCHAR(255) DEFAULT NULL,
    `remarks` VARCHAR(255) DEFAULT NULL,
    `isHide` vARCHAR(1) DEFAULT 'N',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `tpl_item_cate_various_type` (`name_tc`, `name_en`, `remarks`) VALUES
('尺碼', 'size', NULL),
('味道', 'Taste', '餅乾味道'),
('味道', 'Taste', '果汁味道'),
('尺碼', 'size', 'BB衣服尺碼'),
('尿片尺碼', 'size', '尿片尺碼'),
('顏色', 'color', '衣服顏色（主色）');


INSERT INTO `tpl_item_cate_various_val` (`type_id`, `val`, `name_tc`, `name_en`, `remarks`) VALUES
(1, 'XL', '加大碼', "XL", NULL),
(1, 'L', '大碼', "L", NULL),
(1, 'M', '中碼', "M", NULL),
(1, 'S', '細碼', "S", NULL),
(1, 'XS', '加細碼', "XS", NULL),
(2, 'SASEME', '芝麻', "Saseme", NULL),
(2, 'CHEESE', '芝士', "Cheese", NULL),
(2, 'CHOCO', '朱古力', "Chocolate", NULL),
(2, 'MILK', '牛奶', "Milk", NULL),
(2, 'GINGER', '薑味', "Ginger", NULL),
(2, 'STRAWBERRY', '草莓', "Strawberry", NULL),
(2, 'LEMON', '檸檬', "Lemon", NULL),
(2, 'PEANUT', '花生', "Peanut", NULL),
(2, 'ORANGE', '橙味', "Orange", NULL),
(3, 'ORANGE', '橙味', "Orange", NULL),
(3, 'APPLE', '蘋果', "Apple", NULL),
(3, 'GRAPE', '提子', "Grape", NULL),
(4, '1_3MONTH', '1-3個月', "1-3 Months", NULL),
(4, '4_6MONTH', '4-6個月', "4-6 Months", NULL),
(4, '7-9MONTH', '7-9個月', "7-9 Months", NULL),
(4, '10-12MONTH', '10-12個月', "10-12 Months", NULL),
(4, '12MONTH', '1歲', "1 Year", NULL),
(4, '24MONTH', '2歲', "2 Years", NULL),
(4, '36MONTH', '3歲', "3 Years", NULL),
(5, 'XL', '加大碼', "XL", NULL),
(5, 'L', '大碼', "L", NULL),
(5, 'M', '中碼', "M", NULL),
(5, 'S', '細碼', "S", NULL),
(5, 'XS', '加細碼', "XS", NULL),
(6, 'RED', '紅色', 'Red', NULL),
(6, 'ORANGE', '橙色', 'Orange', NULL),
(6, 'YELLOW', '黃色', 'Yellow', NULL),
(6, 'GREEN', '綠色', 'Green', NULL),
(6, 'CYAN', '青色', 'Cyan', NULL),
(6, 'BLUE', '藍色', 'Blue', NULL),
(6, 'MAGENTA', '洋紅色', 'Magenta', NULL),
(6, 'PURPLE', '紫色', 'Purple', NULL),
(6, 'WHITE', '白色', 'White', NULL),
(6, 'BLACK', '黑色', 'Black', NULL),
(6, 'GRAY', '灰色', 'Gray', NULL),
(6, 'SILVER', '銀色', 'Silver', NULL),
(6, 'GOLD', '金色', 'Gold', NULL),
(6, 'PINK', '粉紅色', 'Pink', NULL),
(6, 'MAROON', '褐紫紅色', 'Maroon', NULL),
(6, 'BROWN', '啡色', 'Brown', NULL),
(6, 'BEIGE', '米色', 'Beige', NULL),
(6, 'TAN', '黃褐色', 'Tan', NULL),
(6, 'PEACH', '桃色', 'Peach', NULL),
(6, 'LIME', '青檸綠色', 'Lime', NULL),
(6, 'OLIVE', '橄欖綠色', 'Olive', NULL),
(6, 'TURQUOISE', '綠松色', 'Turquoise', NULL),
(6, 'TEAL', '藍綠色', 'Teal', NULL),
(6, 'NAVY', '海軍藍色', 'Navy', NULL),
(6, 'INDIGO', '靛色', 'Indigo', NULL),
(6, 'VIOLET', '藍紫色', 'Violet', NULL);

DROP TABLE IF EXISTS `tbl_item_cate_various_type`;
CREATE TABLE `tbl_item_cate_various_type` (
    `type_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `cate_id` INT(11) DEFAULT NULL,
    `name_tc` VARCHAR(255) DEFAULT NULL,
    `name_en` VARCHAR(255) DEFAULT NULL,
    `remarks` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_item_cate_various_val`;
CREATE TABLE `tbl_item_cate_various_val` (
    `val_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `type_id` INT(11) DEFAULT NULL,
    `val` VARCHAR(255) DEFAULT NULL,
    `name_tc` VARCHAR(255) DEFAULT NULL,
    `name_en` VARCHAR(255) DEFAULT NULL,
    `remarks` VARCHAR(255) DEFAULT NULL,
    `isHide` vARCHAR(1) DEFAULT 'N',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_item_brand`;
CREATE TABLE `tbl_item_brand` (
    `brand_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `brand_name_tc` VARCHAR(255) DEFAULT NULL,
    `brand_name_en` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_item_country`;
CREATE TABLE `tbl_item_country` (
    `country_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `country_name_tc` VARCHAR(255) DEFAULT NULL,
    `country_name_en` VARCHAR(255) DEFAULT NULL,
    `sort` INT(11) DEFAULT 1,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_item_tag`;
CREATE TABLE `tbl_item_tag` (
    `tag_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `parent_tag_id` INT(11) DEFAULT NULL,
    `tag_name_tc` VARCHAR(255) DEFAULT NULL,
    `tag_name_en` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_item_tag`;
CREATE TABLE `tmap_item_tag` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `item_id` INT(11) NOT NULL, 
    `tag_id` INT(11) NOT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_cate_product`;
CREATE TABLE `tmap_cate_product` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `product_id` INT(11) DEFAULT NULL, 
    `cate_id` INT(11) NOT NULL, 
    `shop_id` INT(11) NOT NULL, 
    `sort` INT(11) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_product_cutoff_tpl`;
CREATE TABLE `tbl_product_cutoff_tpl` (
    `cutoff_tpl_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) NOT NULL,
    `adm_name` VARCHAR(255) DEFAULT NULL,
    `ava_b4_day` INT(11) DEFAULT NULL,
    `ava_b4_cutoff` time DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_product_card`;
CREATE TABLE `tbl_product_card` (
    `card_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `name_tc` VARCHAR(255) DEFAULT NULL,
    `name_en` VARCHAR(255) DEFAULT NULL,
    `size` VARCHAR(255) DEFAULT NULL,
    `img_path` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_product_card_group`;
CREATE TABLE `tbl_product_card_group` (
    `card_group_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `name_tc` VARCHAR(255) DEFAULT NULL,
    `name_en` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_product_card_group`;
CREATE TABLE `tmap_product_card_group` ( 
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `card_id` INT(11) DEFAULT NULL,
    `card_group_id` INT(11) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_product_tpl`;
CREATE TABLE `tbl_product_tpl` (
    `tpl_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) NOT NULL,
    `adm_name` VARCHAR(255) DEFAULT NULL, 
    `deli_msg_tc` LONGTEXT DEFAULT NULL, 
    `deli_msg_en` LONGTEXT DEFAULT NULL, 
    `info_msg_tc` LONGTEXT DEFAULT NULL, 
    `info_msg_en` LONGTEXT DEFAULT NULL,
    `markup` INT(11) DEFAULT 0,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_product_img`;
CREATE TABLE `tbl_product_img` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `product_id` INT(11) NOT NULL, 
    `shop_id` INT(11) NOT NULL,
    `img` VARCHAR(255) DEFAULT NULL, 
    `sort` INT(11) DEFAULT 1,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_product_addon_cate`;
CREATE TABLE `tmap_product_addon_cate` ( 
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `addon_cate_id` VARCHAR(255) DEFAULT NULL,
    `product_id` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_product_addon_cate`;
CREATE TABLE `tbl_product_addon_cate` ( 
    `addon_cate_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `original_addon_id` INT(11) DEFAULT NULL,
    `shop_id` INT(11) NOT NULL,
    `product_id` INT(11) NOT NULL,
    `name_tc` VARCHAR(255) DEFAULT NULL,
    `name_en` VARCHAR(255) DEFAULT NULL,
    `sort` INT(11) DEFAULT 1,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_combo_product`;
CREATE TABLE `tmap_combo_product` ( 
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `product_id` INT(11) DEFAULT NULL,
    `src_product_id` INT(11) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_product_item_content`;
CREATE TABLE `tbl_product_item_content` ( 
    `content_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `product_id` INT(11) DEFAULT NULL,
    `item_id` INT(11) DEFAULT NULL,
    `inhouse_name_tc` VARCHAR(255) NOT NULL,
    `inhouse_name_en` VARCHAR(255) NOT NULL,
    `gift_name_tc` VARCHAR(255) DEFAULT NULL,
    `gift_name_en` VARCHAR(255) DEFAULT NULL,
    `zuri_name_tc` VARCHAR(255) DEFAULT NULL,
    `zuri_name_en` VARCHAR(255) DEFAULT NULL,
    `ltp_name_tc` VARCHAR(255) DEFAULT NULL,
    `ltp_name_en` VARCHAR(255) DEFAULT NULL,
    `qty` INT(11) DEFAULT 1,
    `additional_price` INT(11) DEFAULT 0,
    `client_optional` VARCHAR(1) DEFAULT 'Y',
    `remarks` LONGTEXT DEFAULT NULL,
    `sorting` INT(11) DEFAULT NULL,
    `show_in_web` VARCHAR(1) DEFAULT 'Y',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- DROP TABLE IF EXISTS `tbl_product_item_content`;
-- CREATE TABLE `tbl_product_item_content` ( 
--     `content_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
--     `product_id` INT(11) DEFAULT NULL,
--     `ref_id` INT(11) DEFAULT NULL,
--     `type` VARCHAR(64) DEFAULT NULL,
--     `gift_name_tc` VARCHAR(255) DEFAULT NULL,
--     `gift_name_en` VARCHAR(255) DEFAULT NULL,
--     `zuri_name_tc` VARCHAR(255) DEFAULT NULL,
--     `zuri_name_en` VARCHAR(255) DEFAULT NULL,
--     `ltp_name_tc` VARCHAR(255) DEFAULT NULL,
--     `ltp_name_en` VARCHAR(255) DEFAULT NULL,
--     `qty` INT(11) DEFAULT NULL,
--     `sort` INT(11) DEFAULT NULL,
--     `show_in_web` VARCHAR(1) DEFAULT 'Y',
--     `activate` VARCHAR(1) DEFAULT 'Y',
--     `updated_by_id` INT(11) DEFAULT NULL,
--     `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
-- 	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_product`;
CREATE TABLE `tbl_product` ( 
    `product_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,

    `gift_product_tpl_id` INT(11) DEFAULT NULL,
    `gift_product_code` VARCHAR(255) DEFAULT NULL,
    `gift_product_name_tc` VARCHAR(255) DEFAULT NULL,
    `gift_product_name_en` VARCHAR(255) DEFAULT NULL,
    `gift_deli_id` INT(11) DEFAULT NULL,
    `gift_cust_deli_act` VARCHAR(1) DEFAULT 'N',
    `gift_cust_deli_tc` LONGTEXT DEFAULT NULL,
    `gift_cust_deli_en` LONGTEXT DEFAULT NULL,
    `gift_cust_info_act` VARCHAR(1) DEFAULT 'N',
    `gift_cust_info_tc` LONGTEXT DEFAULT NULL,
    `gift_cust_info_en` LONGTEXT DEFAULT NULL,
    `gift_cust_addinfo_act` VARCHAR(1) DEFAULT 'N',
    `gift_cust_addinfo_tc` LONGTEXT DEFAULT NULL,
    `gift_cust_addinfo_en` LONGTEXT DEFAULT NULL,
    `gift_price` INT(11) DEFAULT NULL,
    `gift_sp_price` INT(11) DEFAULT NULL,
    `gift_cutoff_tpl_id` INT(11) DEFAULT NULL,
    `gift_ava_b4_day` INT(11) DEFAULT NULL,
    `gift_ava_b4_cutoff` time DEFAULT NULL,
    `gift_onsale` VARCHAR(1) DEFAULT 'N', -- 發售中
    `gift_show_in_web` VARCHAR(1) DEFAULT 'N',
    `gift_display_in_cate` VARCHAR(1) DEFAULT 'N',
    `gift_deli_start_date` DATE DEFAULT NULL,
    `gift_deli_start_date` DATE DEFAULT NULL,
    `gift_is_force_checkout` VARCHAR(1) DEFAULT 'N',
    `gift_force_checkout_start_date` DATE DEFAULT NULL,
    `gift_force_checkout_end_date` DATE DEFAULT NULL,
    `gift_publish_date` DATE DEFAULT NULL,
    `gift_delivery_start_date` DATE DEFAULT NULL,
    `gift_delivery_end_date` DATE DEFAULT NULL,

    `zuri_product_tpl_id` INT(11) DEFAULT NULL,
    `zuri_product_code` VARCHAR(255) DEFAULT NULL,
    `zuri_product_name_tc` VARCHAR(255) DEFAULT NULL,
    `zuri_product_name_en` VARCHAR(255) DEFAULT NULL,
    `zuri_deli_id` INT(11) DEFAULT NULL,
    `zuri_cust_deli_act` VARCHAR(1) DEFAULT 'N',
    `zuri_cust_deli_tc` LONGTEXT DEFAULT NULL,
    `zuri_cust_deli_en` LONGTEXT DEFAULT NULL,
    `zuri_cust_info_act` VARCHAR(1) DEFAULT 'N',
    `zuri_cust_info_tc` LONGTEXT DEFAULT NULL,
    `zuri_cust_info_en` LONGTEXT DEFAULT NULL,
    `zuri_cust_addinfo_act` VARCHAR(1) DEFAULT 'N',
    `zuri_cust_addinfo_tc` LONGTEXT DEFAULT NULL,
    `zuri_cust_addinfo_en` LONGTEXT DEFAULT NULL,
    `zuri_price` INT(11) DEFAULT NULL,
    `zuri_sp_price` INT(11) DEFAULT NULL,
    `zuri_cutoff_tpl_id` INT(11) DEFAULT NULL,
    `zuri_ava_b4_day` INT(11) DEFAULT NULL,
    `zuri_ava_b4_cutoff` time DEFAULT NULL,
    `zuri_onsale` VARCHAR(1) DEFAULT 'N', -- 發售中
    `zuri_show_in_web` VARCHAR(1) DEFAULT 'N',
    `zuri_display_in_cate` VARCHAR(1) DEFAULT 'N',
    `zuri_deli_start_date` DATE DEFAULT NULL,
    `zuri_is_force_checkout` VARCHAR(1) DEFAULT 'N',
    `zuri_force_checkout_start_date` DATE DEFAULT NULL,
    `zuri_force_checkout_end_date` DATE DEFAULT NULL,
    `zuri_publish_date` DATE DEFAULT NULL,
    `zuri_delivery_start_date` DATE DEFAULT NULL,
    `zuri_delivery_end_date` DATE DEFAULT NULL,

    `ltp_product_tpl_id` INT(11) DEFAULT NULL,    
    `ltp_product_code` VARCHAR(255) DEFAULT NULL,
    `ltp_product_name_tc` VARCHAR(255) DEFAULT NULL,
    `ltp_product_name_en` VARCHAR(255) DEFAULT NULL,
    `ltp_deli_id` INT(11) DEFAULT NULL,
    `ltp_cust_deli_act` VARCHAR(1) DEFAULT 'N',
    `ltp_cust_deli_tc` LONGTEXT DEFAULT NULL,
    `ltp_cust_deli_en` LONGTEXT DEFAULT NULL,
    `ltp_cust_info_act` VARCHAR(1) DEFAULT 'N',
    `ltp_cust_info_tc` LONGTEXT DEFAULT NULL,
    `ltp_cust_info_en` LONGTEXT DEFAULT NULL,
    `ltp_cust_addinfo_act` VARCHAR(1) DEFAULT 'N',
    `ltp_cust_addinfo_tc` LONGTEXT DEFAULT NULL,
    `ltp_cust_addinfo_en` LONGTEXT DEFAULT NULL,
    `ltp_price` INT(11) DEFAULT NULL,
    `ltp_sp_price` INT(11) DEFAULT NULL,
    `ltp_cutoff_tpl_id` INT(11) DEFAULT NULL,
    `ltp_ava_b4_day` INT(11) DEFAULT NULL,
    `ltp_ava_b4_cutoff` time DEFAULT NULL,
    `ltp_onsale` VARCHAR(1) DEFAULT 'N', -- 發售中
    `ltp_show_in_web` VARCHAR(1) DEFAULT 'N',
    `ltp_display_in_cate` VARCHAR(1) DEFAULT 'N',
    `ltp_deli_start_date` DATE DEFAULT NULL,
    `ltp_is_force_checkout` VARCHAR(1) DEFAULT 'N',
    `ltp_force_checkout_start_date` DATE DEFAULT NULL,
    `ltp_force_checkout_end_date` DATE DEFAULT NULL,
    `ltp_publish_date` DATE DEFAULT NULL,
    `ltp_delivery_start_date` DATE DEFAULT NULL,
    `ltp_delivery_end_date` DATE DEFAULT NULL,

    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_addon`;
CREATE TABLE `tbl_addon` (
    `addon_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) NOT NULL,
    `name_tc` VARCHAR(255) DEFAULT NULL,
    `name_en` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_addon_group`;
CREATE TABLE `tbl_addon_group` (
    `addon_group_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `inhouse_name_tc` VARCHAR(255) DEFAULT NULL,
    `inhouse_name_en` VARCHAR(255) DEFAULT NULL,
    `gift_name_tc` VARCHAR(255) DEFAULT NULL,
    `gift_name_en` VARCHAR(255) DEFAULT NULL,
    `ltp_name_tc` VARCHAR(255) DEFAULT NULL,
    `ltp_name_en` VARCHAR(255) DEFAULT NULL,
    `zuri_name_tc` VARCHAR(255) DEFAULT NULL,
    `zuri_name_en` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_addon_group`;
CREATE TABLE `tmap_addon_group`(
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `addon_id` INT(11) DEFAULT NULL, 
    `addon_group_id` INT(11) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



DROP TABLE IF EXISTS `tbl_nav`;
CREATE TABLE `tbl_nav` (
    `nav_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) NOT NULL,
    `name_tc` VARCHAR(255) DEFAULT NULL,
    `name_en` VARCHAR(255) DEFAULT NULL,
    `link` VARCHAR(255) DEFAULT NULL, 
    `sort` INT(11) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_cate_nav`;
CREATE TABLE `tmap_cate_nav`(
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nav_id` INT(11) DEFAULT NULL, 
    `cate_id` INT(11) DEFAULT NULL, 
    `sort` INT(11) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_nav_page`;
CREATE TABLE `tmap_nav_page`(
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nav_id` INT(11) DEFAULT NULL, 
    `page_id` INT(11) DEFAULT NULL, 
    `sort` INT(11) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_client`;
CREATE TABLE `tbl_client` (
    `client_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) DEFAULT NULL,
    `image_path` VARCHAR(64) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_page`;
CREATE TABLE `tbl_page` (
    `page_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) NOT NULL,
    `name_tc` VARCHAR(255) DEFAULT NULL,
    `name_en` VARCHAR(255) DEFAULT NULL,
    `url` VARCHAR(255) NOT NULL,
    `editable` VARCHAR(1) DEFAULT 'Y',
    `removeable` VARCHAR(1) DEFAULT 'Y',
    `show_in_web` VARCHAR(1) DEFAULT 'Y',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `see_through_nav` VARCHAR(1) DEFAULT 'N',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_page_content`;
CREATE TABLE `tbl_page_content` (
    `content_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `page_id` INT(11) DEFAULT NULL, 
    `type` VARCHAR(255) DEFAULT NULL, 
    `html_tc` LONGTEXT DEFAULT NULL, -- html editor
    `html_en` LONGTEXT DEFAULT NULL, -- html editor
    `lr_img_tc` VARCHAR(255) DEFAULT NULL,
    `lr_img_en` VARCHAR(255) DEFAULT NULL,
    `mini_banner_tc` VARCHAR(255) DEFAULT NULL,
    `mini_banner_en` VARCHAR(255) DEFAULT NULL,
    `sort` INT(11) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_content_product`;
CREATE TABLE `tmap_content_product` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `content_id` INT(11) DEFAULT NULL, 
    `product_id` INT(11) DEFAULT NULL, 
    `sort` INT(11) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_content_slider`;
CREATE TABLE `tmap_content_slider` (
    `slider_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `content_id` INT(11) DEFAULT NULL, 
    `img_tc` VARCHAR(255) DEFAULT NULL, 
    `img_en` VARCHAR(255) DEFAULT NULL, 
    `sort` INT(11) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_shop`;
CREATE TABLE `tbl_shop` (
    `shop_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT, -- 1 gift, 2 zuri, 3 ltp
    `adm_name` VARCHAR(255) DEFAULT NULL,
    `prefix` VARCHAR(64) DEFAULT NULL,
    `domain` VARCHAR(64) DEFAULT NULL,
    `nav_color` VARCHAR(8) DEFAULT NULL, -- #000000
    `whatsapp` VARCHAR(255) DEFAULT NULL,
    `whatsapp2` VARCHAR(255) DEFAULT NULL,
    `facebook` VARCHAR(255) DEFAULT NULL,
    `email` VARCHAR(255) DEFAULT NULL,
    `address_tc` VARCHAR(255) DEFAULT NULL,
    `address_en` VARCHAR(255) DEFAULT NULL,
    `ig` VARCHAR(255) DEFAULT NULL,
    `twitter` VARCHAR(255) DEFAULT NULL,
    `phone` VARCHAR(255) DEFAULT NULL, 
    `fax` VARCHAR(255) DEFAULT NULL,
    `commission` INT(11) DEFAULT NULL,
    `platform_commission` INT(11) DEFAULT NULL,
    `logo` VARCHAR(255) DEFAULT NULL,
    `new_member_credit` VARCHAR(255) DEFAULT NULL,
    `point_ratio` INT(11) DEFAULT 0,
    `google_x` VARCHAR(128) DEFAULT NULL,
    `google_y` VARCHAR(128) DEFAULT NULL,
    `markup` VARCHAR(128) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_deli_district`;
CREATE TABLE `tbl_deli_district`(
    `district_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) DEFAULT NULL,
    `name_tc` VARCHAR(255) DEFAULT NULL, 
    `name_en` VARCHAR(255) DEFAULT NULL, 
    `show_in_web` VARCHAR(1) DEFAULT 'Y',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_deli_region`;
CREATE TABLE `tbl_deli_region` (
    `region_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) NOT NULL,
    `district_id` INT(11) NOT NULL,
    `name_tc` VARCHAR(255) DEFAULT NULL,
    `name_en` VARCHAR(255) DEFAULT NULL,
    `shipping_fee` INT(11) DEFAULT '0', 
    `show_in_web` VARCHAR(1) DEFAULT 'Y',
    `address_required` VARCHAR(1) DEFAULT 'Y',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_deli_timeslot`;
CREATE TABLE `tbl_deli_timeslot` (
    `slot_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) NOT NULL,
    `adm_name` VARCHAR(255) DEFAULT NULL,
    `start_time` TIME NOT NULL,
    `end_time` TIME NOT NULL,
    `ava_b4_day` INT(11) DEFAULT NULL,
    `ava_b4_cutoff` time DEFAULT NULL,
    `deli_fee` INT(11) DEFAULT NULL, 
    `same_day_deli_fee` INT(11) DEFAULT NULL, 
    `default` VARCHAR(1) DEFAULT 'Y',
    `online_pay_same_day` VARCHAR(1) DEFAULT 'N', 
    `show_in_web` VARCHAR(1) DEFAULT 'Y', 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_deli_group`;
CREATE TABLE `tbl_deli_group`(
    `grp_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `adm_name` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_deli_group_timeslot`;
CREATE TABLE `tbl_deli_group_timeslot`(
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `ava_b4_day` INT(11) DEFAULT NULL,
    `ava_b4_cutoff` time DEFAULT NULL,
    `deli_fee` INT(11) DEFAULT NULL, 
    `same_day_deli_fee` INT(11) DEFAULT NULL, 
    `online_pay_same_day` VARCHAR(1) DEFAULT 'N', 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_deli_group_region_edit_group`;
CREATE TABLE `tbl_deli_group_region_edit_group`(
    `edit_group_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `region_id` INT(11) DEFAULT NULL,
    `deli_group_id` INT(11) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_deli_group_region_timeslot`;
CREATE TABLE `tbl_deli_group_region_timeslot`(
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `deli_group_region_id` INT(11) DEFAULT NULL,
    `timeslot_id` INT(11) DEFAULT NULL,
    `ava_b4_day` INT(11) DEFAULT NULL,
    `ava_b4_cutoff` time DEFAULT NULL,
    `deli_fee` INT(11) DEFAULT NULL, 
    `same_day_deli_fee` INT(11) DEFAULT NULL, 
    `online_pay_same_day` VARCHAR(1) DEFAULT 'N', 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_deli_group_region`;
CREATE TABLE `tmap_deli_group_region` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `deli_group_region_id` INT(11) DEFAULT NULL, 
    `region_id` INT(11) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_deli_group_region`;
CREATE TABLE `tbl_deli_group_region` (
    `deli_group_region_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `deli_group_id` INT(11) DEFAULT NULL, 
    `shipping_fee` INT(11) DEFAULT NULL, 
    `address_required` VARCHAR(1) DEFAULT 'Y',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tmap_grp_product`;
CREATE TABLE `tmap_grp_product` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `grp_id` INT(11) DEFAULT NULL, 
    `product_id` INT(11) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_cate_voucher`;
CREATE TABLE `tmap_cate_voucher` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `cate_id` INT(11) DEFAULT NULL, 
    `voucher_id` INT(11) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_cate_coupon`;
CREATE TABLE `tmap_cate_coupon` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `cate_id` INT(11) DEFAULT NULL, 
    `coupon_id` INT(11) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_company_discount`;
CREATE TABLE `tbl_company_discount` (
    `discount_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) DEFAULT NULL, 
    `company_name` VARCHAR(255) DEFAULT NULL, 
    `credit` INT(11) DEFAULT NULL, 
    `quota` INT(11) DEFAULT NULL, 
    `expiry_date` datetime DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_company_discount_email`;
CREATE TABLE `tmap_company_discount_email` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `discount_id` INT(11) DEFAULT NULL, 
    `email` VARCHAR(255) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_company_discount_applicant`;
CREATE TABLE `tbl_company_discount_applicant` (
    `applicant_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `member_id` INT(11) DEFAULT NULL, 
    `discount_id` INT(11) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_gift_deli`;
CREATE TABLE `tbl_gift_deli` (
    `deli_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `member_id` INT(11) DEFAULT NULL,
    `gift_id` INT(11) DEFAULT NULL, 
    `deli_status` VARCHAR(64) DEFAULT NULL, 
    `remarks` LONGTEXT DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_gift`;
CREATE TABLE `tbl_gift` (
    `gift_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) DEFAULT NULL, 
    `name_tc` VARCHAR(255) DEFAULT NULL, 
    `name_en` VARCHAR(255) DEFAULT NULL, 
    `required_point` INT(11) DEFAULT NULL, 
    `quota` INT(11) DEFAULT NULL, 
    `gift_img` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_gift_card`;
CREATE TABLE `tbl_gift_card` (
    `card_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) DEFAULT NULL, 
    `name_tc` VARCHAR(255) DEFAULT NULL, 
    `name_en` VARCHAR(255) DEFAULT NULL, 
    `credit` INT(11) DEFAULT 0,
    `expiry_date` datetime DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_gift_card_key`;
CREATE TABLE `tbl_gift_card_key` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `card_id` INT(11) DEFAULT NULL, 
    `code` VARCHAR(255) DEFAULT NULL, 
    `use_datetime` datetime DEFAULT NULL, 
    `used_by_id` INT(11) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_member_voucher`;
CREATE TABLE `tbl_member_voucher` (
    `member_voucher_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `member_id` INT(11) DEFAULT NULL, 
    `voucher_id` INT(11) DEFAULT NULL, 
    `redeemed` VARCHAR(1) DEFAULT 'N',
    `discount` INT(11) DEFAULT '0',
    `type` VARCHAR(1) DEFAULT 'A',
    `minimum_order_amt` INT(11) DEFAULT '0',
    `ava_day` INT(11) DEFAULT '0',
    `expiry_date` date DEFAULT NULL,
    `sales_admin_id` INT(11) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_new_member_voucher`;
CREATE TABLE `tmap_new_member_voucher` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) DEFAULT NULL, 
    `voucher_id` INT(11) DEFAULT NULL, 
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_addon_product`;
CREATE TABLE `tmap_addon_product` (
  `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `product_id` INT(11) NOT NULL,
  `addon_id` INT(11) NOT NULL,
  `activate` VARCHAR(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` INT(11) DEFAULT NULL,
  `last_update` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_addonCate_product`;
CREATE TABLE `tmap_addonCate_product` (
  `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `product_id` INT(11) NOT NULL,
  `addon_cate_id` INT(11) NOT NULL,
  `activate` VARCHAR(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` INT(11) DEFAULT NULL,
  `last_update` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_holiday`;
CREATE TABLE `tbl_holiday`(
    `holiday_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_id` INT(11) DEFAULT NULL, 
    `adm_name` VARCHAR(255) DEFAULT NULL,
    `start_date` datetime DEFAULT NULL,
    `end_date` datetime DEFAULT NULL,
    `remark_tc` LONGTEXT DEFAULT NULL,
    `remark_en` LONGTEXT DEFAULT NULL,
    `quota_everyday` INT(11) DEFAULT NULL,
    `isDayOff` VARCHAR(1) DEFAULT 'N',
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_holiday_product`;
CREATE TABLE `tmap_holiday_product` (
  `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `product_id` INT(11) NOT NULL,
  `holiday_id` INT(11) NOT NULL,
  `activate` VARCHAR(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `updated_by_id` INT(11) DEFAULT NULL,
  `last_update` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_datetime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_sku`;
CREATE TABLE `tbl_sku` (
    `sku_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `item_id` INT(11) NOT NULL,
    `gift_name_tc` VARCHAR(255) DEFAULT NULL,
    `gift_name_en` VARCHAR(255) DEFAULT NULL,
    `zuri_name_tc` VARCHAR(255) DEFAULT NULL,
    `zuri_name_en` VARCHAR(255) DEFAULT NULL,
    `ltp_name_tc` VARCHAR(255) DEFAULT NULL,
    `ltp_name_en` VARCHAR(255) DEFAULT NULL,
    `alias` VARCHAR(255) DEFAULT NULL,
    `sAlias` VARCHAR(255) DEFAULT NULL,
    `qty` INT(11) DEFAULT 0,
    `ava_qty` INT(11) DEFAULT 0,
    `reserved_qty` INT(11) DEFAULT 0,
    `alert_qty` INT(11) DEFAULT 0,
    `cost` INT(11) DEFAULT 0,
    `sku_code` VARCHAR(255) DEFAULT NULL,
    `sku_val` VARCHAR(255) DEFAULT NULL,
    `auto` VARCHAR(1) DEFAULT 'N', -- this sku will auto packed
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_supplier`;
CREATE TABLE `tbl_supplier` ( 
    `supplier_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `name_tc` VARCHAR(255) DEFAULT NULL,
    `name_en` VARCHAR(255) DEFAULT NULL,
    `short_form` VARCHAR(64) DEFAULT NULL,
    `address` LONGTEXT DEFAULT NULL,
    `phone` VARCHAR(255) DEFAULT NULL,
    `email` VARCHAR(255) DEFAULT NULL,
    `fax` VARCHAR(255) DEFAULT NULL,
    `remarks` LONGTEXT DEFAULT NULL,
    `MOQ` VARCHAR(255) DEFAULT NULL,
    `payment_method` VARCHAR(255) DEFAULT NULL,
    `payment_detail` LONGTEXT DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_supplier_contact`;
CREATE TABLE `tbl_supplier_contact` ( 
    `contact_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `supplier_id` INT(11) NOT NULL,
    `contact_name` VARCHAR(255) DEFAULT NULL,
    `contact_phone` VARCHAR(128) DEFAULT NULL,
    `contact_email` VARCHAR(255) DEFAULT NULL,
    `contact_remarks` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_supplier_tag`;
CREATE TABLE `tbl_supplier_tag` ( 
    `tag_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `parent_tag_id` INT(11) DEFAULT NULL,
    `name` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_supplier_tag`;
CREATE TABLE `tmap_supplier_tag` ( 
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `supplier_id` INT(11) NOT NULL,
    `tag_id` INT(11) NOT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_purchase`;
CREATE TABLE `tbl_purchase` ( 
    `po_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `supplier_id` INT(11) NOT NULL,
    `ref_no` VARCHAR(255),
    `ref_file` VARCHAR(255) DEFAULT NULL,
    `remarks` LONGTEXT DEFAULT NULL,
    `type` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_item_stock_in`;
CREATE TABLE `tbl_item_stock_in` ( 
    `in_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `po_id` INT(11) NOT NULL,
    `item_id` INT(11) NOT NULL,
    `qty` INT(11) NOT NULL,
    `avg_price` INT(11) NOT NULL,
    `barcode` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_item_stock_out`;
CREATE TABLE `tbl_item_stock_out` ( 
    `out_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `item_id` INT(11) NOT NULL,
    `qty` INT(11) NOT NULL,
    `remarks` LONGTEXT DEFAULT NULL,
    `type` LONGTEXT DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_sku_code`;
CREATE TABLE `tbl_sku_code` ( 
    `code_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `code` VARCHAR(11) NOT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_sku_val`;
CREATE TABLE `tbl_sku_val` ( 
    `val_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `code_id` INT(11) NOT NULL,
    `val` VARCHAR(255) NOT NULL,
    `lbl_tc` VARCHAR(255) NOT NULL,
    `lbl_en` VARCHAR(255) NOT NULL,
    `remarks` VARCHAR(255) NOT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_sku_code_val`;
CREATE TABLE `tmap_sku_code_val` ( 
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `sku_id` INT(11) NOT NULL,
    `val_id` INT(11) NOT NULL,
    `display_name` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_location`;
CREATE TABLE `tbl_location` ( 
    `location_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `adm_name` VARCHAR(255) NOT NULL,
    `remarks` LONGTEXT NOT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_location_item`;
CREATE TABLE `tmap_location_item` ( 
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `location_id` INT(11) NOT NULL,
    `item_id` INT(11) NOT NULL,
    `qty` INT(11) DEFAULT 0,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `log_location_item`;
CREATE TABLE `log_location_item` (
    `log_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `map_id` INT(11) DEFAULT NULL,
    `item_id` INT(11) DEFAULT NULL,
    `qty` INT(11) DEFAULT NULL,
    `action` VARCHAR(64) DEFAULT NULL,
    `updated_by_id` VARCHAR(255) DEFAULT NULL,
    `create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_sku_barcode`;
CREATE TABLE `tmap_sku_barcode` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `sku_id` INT(11) NOT NULL,
    `barcode` VARCHAR(255) NOT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_email_tpl`;
CREATE TABLE `tbl_email_tpl` ( 
    `tpl_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `adm_name` VARCHAR(255) NOT NULL,
    `remarks` LONGTEXT NOT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_email`;
CREATE TABLE `tbl_email` ( 
    `email_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `to_address` VARCHAR(255) NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `content` LONGTEXT NOT NULL,
    `ref_id` INT(11) DEFAULT NULL,
    `tbl` VARCHAR(128) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_stocktake`;
CREATE TABLE `tbl_stocktake` ( 
    `stocktake_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `status` VARCHAR(255) NOT NULL DEFAULT '10_incomplete', -- 10_incomplete, 20_completed
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tbl_stocktake_sku`;
CREATE TABLE `tbl_stocktake_sku` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `stocktake_id` INT(11) NOT NULL,
    `location_id` INT(11) NOT NULL,
    `sku_id` INT(11) NOT NULL,
    `expected_qty` INT(11) NOT NULL,
    `actual_qty` INT(11) DEFAULT NULL,
    `remarks` VARCHAR(255) DEFAULT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_item_tag`;
CREATE TABLE `tmap_item_tag` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `item_id` INT(11) NOT NULL,
    `tag` VARCHAR(255) NOT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tmap_sku_tag`;
CREATE TABLE `tmap_sku_tag` (
    `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `sku_id` INT(11) NOT NULL,
    `tag` VARCHAR(255) NOT NULL,
    `activate` VARCHAR(1) DEFAULT 'Y',
    `updated_by_id` INT(11) DEFAULT NULL,
    `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;