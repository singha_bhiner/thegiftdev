<?php
/* 
    Objective: item and SKU related 
    Author: Sing
    Last major update: 202108003

    Related file:
        include/class/item.class.php - DAO
        admin/php/item/item.php - field esacpe, routing
        admin/php/item/item_api.php - field esacpe, routing
        php/item/item.php - field esacpe, routing
        php/item/item_api.php - field esacpe, routing
        admin/templates/item/* - UI 
        templates/item/* - UI

    Table Scheme: 


        
*/

class partner{
    public function __construct($DB, $lang) {
		$this->DB = $DB;
        $this->lang = $lang;
	}

    function editPartner($partner_id, $data){
        if ($partner_id == 0)
            $partner_id = $this->DB->insert_db('tbl_coupon', null);
        
        $sql = $this->DB->buildSql_update("tbl_coupon", "partner_id", $partner_id, $data);
        $this->DB->update($sql);

        return $this->getPartnerDtl($partner_id);
    }

    function getPartnerList($shop_id, $filter = null, $limiter = null, $sorting = null){
        $sql = "SELECT * FROM `tbl_partner` WHERE `shop_id` = '$shop_id' AND `activate` ='Y'";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        return $res;
    }

    function getPartnerDtl($partner_id){
        $sql = "SELECT *
                FROM `tbl_partner` AS p
                WHERE p.`activate` = 'Y' && p.`partner_id` = '$partner_id'";
        $res = $this->DB->get_Sql($sql)[0];

        return $res;
    }
}
?>
