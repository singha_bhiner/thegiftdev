/* ************************************************************************ */
/* Author : BTL
/* Prerequisite : jquery 1.7 或以上
/* Desc   : This js bundle is single page handler js, it detect class phref and its corresponding href
/*          该js包是单页处理程序js，它检测phref class类及其对应的href
/* Last Modify : David 
/* Last Update : 21 Jul 2020
/* Update log  : - add support for state url update of form post
/* ************************************************************************ */

window.yaxis = [];

function stateHandling(currenthref, spTitle, yaxis) {
    //debug('stateHandling:'+currenthref);
    if (window.stateReady == 'Y') {
        var lastDataPage = new Object();
        if (spTitle) {
            document.title = spTitle;
        }
        lastDataPage.title = document.title;
        lastDataPage.content = document.getElementById("container").innerHTML;
        history.pushState({ "html": lastDataPage.content, "pageTitle": lastDataPage.title }, null, currenthref);
    } else {
        var lastDataPage = new Object();
        if (spTitle) {
            document.title = spTitle;
        }
        lastDataPage.title = document.title;
        lastDataPage.content = document.getElementById("container").innerHTML;
        history.replaceState({ "html": lastDataPage.content, "pageTitle": lastDataPage.title, "yaxis": yaxis }, null, window.location.href);
        window.stateReady = 'Y';
    }
}

function pageHandler(page, skipSate = 'N', title = '', dataContainer = null, element = null, slientReplace = null, scrollController = null, extraHeader = null) {
    if (!window.pwaOn) {
        window.location.href = page;
        return;
    }
    var url = page;
    if (slientReplace == 'Y') {
        // slient replace for specific container
    } else {
        window.stateReady = 'N';
        stateHandling(null, null, window.pageYOffset);
        if (dataContainer) {
            dataContainer = '#' + dataContainer;
        } else {
            dataContainer = '#container';
        }
        if (scrollController == 'N') {
            // no full screen loader for scrollController = N
        } else {
            fullScreenLoader('Y', window.jstxt.jstxt0001 + '...', '')
        }

    }
    var headers = { "pwapage": "Y", };
    if (extraHeader) {
        headers = Object.assign(extraHeader, headers);
    }
    $.bajax({
        url: url,
        debug: 'N',
        pwapage: 'Y',
        headers: headers,
        timeout: 20000,
        pureString: 'Y',
        callback: function(data) {
            // try parse for the case need member login
            try {
                var returnPwaTest = data;
                if (returnPwaTest.result.code == '-99') {
                    // need login
                    window.location.href = returnPwaTest.result.redirect_url;
                    return;
                }
            } catch (e) {
                //debug(e);
            }
            //debug(data);
            try {
                var result = data.split('||xx||success||xx||');
                var resultHtml = data.split('||xx||success||xx||').slice(1).join('||xx||success||xx||');
                if (result[0] == '0' || result[0] == '1') {
                    if (scrollController) {
                        if (scrollController == 'N') {
                            //debug('hihi');
                            // no scroll
                        } else if (scrollController == 'Y') {
                            $([document.documentElement, document.body]).animate({
                                scrollTop: $('body').offset().top
                            }, 0);
                        } else {
                            $([document.documentElement, document.body]).animate({
                                scrollTop: $(scrollController).offset().top
                            }, 0);
                        }
                    } else {
                        if (element) {
                            if (element.attr('data-scroll-to-top') == 'N') {
                                // no scroll
                            } else if (element.attr('data-scroll-to-top') == 'C') {
                                $([document.documentElement, document.body]).animate({
                                    scrollTop: $(container).offset().top
                                }, 0);
                            } else {
                                $([document.documentElement, document.body]).animate({
                                    scrollTop: $('body').offset().top
                                }, 0);
                            }
                        }
                    }

                    if (slientReplace == 'Y') {
                        $(dataContainer).replaceWith(resultHtml);
                    } else {
                        outterContainerUIChanges(result[0]);
                        $(dataContainer).animate({
                            opacity: 0,
                        }, 50, function() {
                            $(dataContainer).html(resultHtml);
                            fullScreenLoader('N', '', '')
                            $(dataContainer).animate({
                                opacity: 1,
                            }, 50, function() {
                                read_blt_page_setup_json();
                            });
                        });
                        if (skipSate == 'Y' || skipSate == 'O') {
                            // Y skip only , O update url too
                            if (skipSate == 'O') {
                                window.history.replaceState('page_load', title, url);
                            }
                        } else {
                            //debug('here');
                            stateHandling(url, title, yaxis);
                        }
                    }

                    //do js tracking for GA & facebook
                    //google
                    /*try{
                        gtag('config', 'UA-173432880-1', {
                            'page_title': $('title').html(),
                            'page_path': window.location.pathname + window.location.search + window.location.hash
                        });
                    } catch(e){

                    }*/
                } else {
                    debug('error in pwa 1');
                }
            } catch (e) {
                debug('1');
                debug(e);
                debug(data);
                if (data.result.code == '-99') {
                    fullScreenLoader('N', '', '')
                    window.errorCallback = $.bmodal({
                        type: 'alert',
                        title: 'Error',
                        elementId: 'errorCallback',
                        headerCloseBtn: true,
                        content: data.result.msg,
                    });
                }
            }
        },
        errorCallback: function(jqXHR, textStatus, errorThrown) {
            var httpCode = jqXHR.status;
            if (httpCode == 403) {
                // do not eval
            } else {
                try {
                    var err = eval("(" + jqXHR.responseText + ")");
                    debug(httpCode + ":" + err.Message);
                } catch (e) {
                    debug('catch-' + httpCode + ":" + jqXHR.responseText);
                }
            }

            fullScreenLoader('N');
            errorShown(window.jstxt.jstxt0002, window.jstxt.jstxt0003 + '(' + window.jstxt.jstxt0004 + httpCode + ')', window.jstxt.jstxt0005);
        },
    });
}

window.addEventListener('pushState', function(e) {
    //debug('pushState');
});

window.addEventListener('replaceState', function(e) {
    //debug('replaceState');
});

window.addEventListener('popstate', function(e) {
    if (e.state.html && e.state.html != '') {
        outterContainerUIChanges(null);
        $('#container').html(e.state.html);
        document.title = e.state.pageTitle;
        if (e.state.yaxis) {
            //debug(e.state.yaxis);
            setTimeout(function() {
                document.documentElement.scrollTop = e.state.yaxis;
            }, 10);
        }

    } else {
        // get current url
        var url = window.location.href.split('/');
        var page = url[3]; // the fourth element
        pageHandler(page, 'Y');
    }
});

$(document).ready(function() {
    $(document).off('click', '.phref');
    $(document).on('click', '.phref', function(e) {
        e.preventDefault();
        var page = $(this).attr('href');
        var pageTitle = $(this).attr('page-title'); // provide page title
        if (!pageTitle) {
            pageTitle = $(this).html().trim().replace(/(<([^>]+)>)/ig, ''); // page title not provided, use html content
            if (!pageTitle || pageTitle == '') {
                pageTitle = null;
            }
        }
        var dataContainer = $(this).attr('data-container');
        pageHandler(page, 'N', pageTitle, dataContainer, $(this));
    });

    $(document).off('click', '.phref_cb');
    $(document).on('click', '.phref_cb', function(e) {
        console.log('1');
        // 這個功應可以在轉版前先調用指定url 作準備，在 spahandler 不允許 在其他頁面更改history state
        e.preventDefault();
        var page = $(this).attr('href'); // 先調用的api
        var cpage = $(this).attr('chref'); // 要轉版的目的地
        var pageTitle = $(this).attr('pageTitle');
        var dataContainer = $(this).attr('data-container');

        if (dataContainer) {
            dataContainer = '#' + dataContainer;
        } else {
            dataContainer = '#container';
        }

        var headers = { "pwapage": "Y", };
        //$(dataContainer).html('');
        //$.bloader({ 'loader_type': dataContainer });
        //debug(page);
        $.bajax({
            url: page,
            debug: 'N',
            headers: headers,
            timeout: 20000,
            callback: function(data) {
                debug(data);
                try {
                    if (data.result.code == '0') {
                        pageHandler(cpage, 'N', pageTitle, dataContainer.substr(1), $(this));
                    } else {
                        window.location.href = data.result.redirect_url;
                    }
                } catch (e) {
                    debug(e);
                    debug(data);
                }
            },
            errorCallback: function(jqXHR, textStatus, errorThrown) {
                debug(jqXHR);
                showjqXHRerrorReturn(jqXHR, page);
            },
        });
    });

    $(document).off('submit', '.phref-form');
    $(document).on('submit', '.phref-form', function(e) {
        e.preventDefault();
        var xurl = $(this).attr('action');
        var xid = $(this).attr('id');
        var title = $(this).attr('phref-page-title');
        var containerTarget = $(this).attr('data-target-container');
        var method = ($(this).attr('method') ? $(this).attr('method') : 'get');
        var func_before_submit = $(this).attr('func-before-submit');
        var skipSateOverride = $(this).attr('skip-state');
        var element = $(this);
        var skipSate = 'N';
        if (func_before_submit) {
            eval(func_before_submit);
        }

        window.stateReady = 'N';
        if (skipSateOverride) {
            skipSate = skipSateOverride;
        }

        stateHandling(null, null, window.pageYOffset);
        //$('#container').html('');
        var dataContainer = "#container";
        if (containerTarget && containerTarget != 'undefined') {
            dataContainer = '#' + containerTarget;
        }
        //$.bloader({ 'loader_type': dataContainer });
        fullScreenLoader('Y', '', '')
        $.bajax({
            url: xurl,
            debug: 'N',
            formElement: 'Y',
            formElementID: xid,
            method: (method ? method.toUpperCase() : "POST"),
            pwapage: 'Y',
            headers: {
                "pwapage": "Y",
            },
            pureString: 'Y',
            callback: function(data, geturl) {
                fullScreenLoader('N', '', '')
                try {
                    if (geturl) {
                        xurl = xurl + "?" + geturl;
                    }
                    var result = data.split('||xx||success||xx||');
                    var resultHtml = data.split('||xx||success||xx||').slice(1).join('||xx||success||xx||');
                    if (result[0] == '0' || result[0] == '1') {
                        if (element) {
                            if (element.attr('data-scroll-to-top') == 'N') {
                                // no scroll
                            } else if (element.attr('data-scroll-to-top') == 'C') {
                                $([document.documentElement, document.body]).animate({
                                    scrollTop: $(container).offset().top
                                }, 0);
                            } else {
                                $([document.documentElement, document.body]).animate({
                                    scrollTop: $('body').offset().top
                                }, 0);
                            }
                        }
                        outterContainerUIChanges(result[0]);
                        $(dataContainer).html(resultHtml);

                        if (method.toUpperCase() != 'POST' || !method) {
                            var parms = element.find('input').serialize();
                        }

                        if (skipSate == 'Y' || skipSate == 'O') {
                            // Y skip only , O update url too
                            if (skipSate == 'O') {
                                window.history.replaceState('page_load', title, xurl);
                            }
                        } else {
                            debug('here');
                            debug(xurl);
                            stateHandling(xurl, title, yaxis);
                        }
                    } else {
                        debug('error in pwa 2');
                        debug(data);
                    }
                } catch (e) {
                    debug(e);
                    debug(data);
                    fullScreenLoader('N', '', '')
                    window.errorCallback = $.bmodal({
                        type: 'alert',
                        title: 'Error',
                        elementId: 'errorCallback',
                        headerCloseBtn: true,
                        content: data.result.msg,
                    });
                }
            },
            errorCallback: function(jqXHR, textStatus, errorThrown) {
                //debug('Error in Reading');
                debug(jqXHR);
            },
        });

        return false;
    });

    stateHandling();
});

function showjqXHRerrorReturn(jqXHR, url) {
    var httpCode = jqXHR.status;
    if (httpCode == 403) {
        // do not eval
    } else {
        try {
            var err = eval("(" + jqXHR.responseText + ")");
            debug(httpCode + ":" + err.Message);
        } catch (e) {
            debug(httpCode + ":" + jqXHR.responseText);
        }
    }
    //fullScreenLoader('N');
    //errorMessageShow('不明錯誤','請聯絡技術支援','好');
    debug('Error in calling : ' + url);
}

function read_blt_page_setup_json() {
    var blt_json = $('[type="blt/page_setup_json"]').html();
    try {
        if (blt_json) {
            blt_json = JSON.parse(blt_json);
            if (blt_json[0]['page_title'][window.page_lang]) {
                // language setting
                document.title = blt_json[0]['page_title'][window.page_lang];
                $('[name="currentUrl"]').attr('content', window.location.href);
                $('[name="desciption"]').attr('content', blt_json[0]['page_description'][window.page_lang]);
                $('[name="keywords"]').attr('content', blt_json[0]['keywords'][window.page_lang]);
            } else if (blt_json[0]['page_title']) {
                document.title = blt_json[0]['page_title'];
                $('[name="currentUrl"]').attr('content', window.location.href);
                $('[name="desciption"]').attr('content', blt_json[0]['page_description']);
                $('[name="keywords"]').attr('content', blt_json[0]['keywords']);
            }
            
        }
    } catch (e) {

    }
}

function outterContainerUIChanges(status) {
    // spa mode update header apperance on page changes
    // Per website setting ===== START ===== 
    if (status || status == '0') {

    } else {
        // handle refreshOrderListing

        $.bmodal({
            close: 'Y',
            elementId: 'placeOrderForm',
        });

    }
    $('.grecaptcha-badge').closest('div').remove();
    $('.datepickers-container').remove()
        // Per website setting ===== END ===== 

    // common repear js
    $(function() {
        //$('.lazy').Lazy({
        //    scrollDirection: 'vertical',
        //   effect: "fadeIn"
        //});
    });
}

var getParams = function(url) {
    var params = {};
    var parser = document.createElement('a');
    parser.href = url;
    var query = parser.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        params[pair[0]] = decodeURIComponent(pair[1]);
    }
    return params;
};