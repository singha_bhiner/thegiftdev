<?php
/* 
    Objective: coupon code related 
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_item_tag


        
*/
require_once dirname(__FILE__). '/abstract.class.php';
class itemTag extends baseClass{

    function editItemTag($data){
        if ($data['tag_id'] == 0){
            $id = $this->DB->insert_db('tbl_item_tag', $data);
        } else {
            $id = $this->DB->update_Sql("tbl_item_tag", "tag_id", $data);
        }
    
        return $this->getItemTagDtl($id);
    }

    function getItemTagList($filter = null, $limiter = null, $sorting = null){
        $fromShort = "it";
		$selectSql = "SELECT $fromShort.* ";
		$fromSql = " FROM `tbl_item_tag` as $fromShort";
		$whereSql = " WHERE $fromShort.`activate` = 'Y'";
        $groupSql = " GROUP BY $fromShort.`tag_id`";
		if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
			$whereSql = " WHERE 1=1";

		if ($sorting)
			$sortSql = " ORDER BY ". $sorting['sort'] . " " . $sorting['order'];
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
				if ($i){
					switch ($k){
                        case 'tag_id':
                            $whereSql .= " AND ($fromShort.`tag_id` = '$i')";
                            break;
                        case 'item_id':
                            $fromSql .= " LEFT JOIN `tmap_item_tag` AS m ON m.`tag_id` = $fromShort.`tag_id` AND m.`activate` = 'Y'";
                            $whereSql .= " AND (m.`item_id` = '$i')";
                            break;
					}
				}
			}
		}
		
		$sql = $selectSql . $fromSql . $whereSql . $sortSql . $groupSql . $limitSql;
		$res = $this->DB->get_Sql($sql);

		return $this->dispatchItemTag($res);
    }

    function getItemTagDtl($tag_id){
        if ($tag_id)
            return $this->getItemTagList(["tag_id" => $tag_id], $this->DB->getUnique)[0];
        else 
            return null;
    }

    function dispatchItemTag($tagList){
        // print_r($tagList);
        $res = [];
        // generate the parent_level
        foreach ($tagList as $k => $tag){
            if (!$tag['parent_tag_id']){
                // print_r($tag);
                array_push($res, $tag);
            }
        }

        // generate the child_level
        foreach ($tagList as $k => $tag){
            if ($tag['parent_tag_id']){
                $parentIdx = array_keys(array_filter($res, fn($par) => $par['tag_id'] == $tag['parent_tag_id']))[0];
                
                if ($parentIdx === null){
                    $itemTagDtl = $this->getItemTagDtl($tag['parent_tag_id']);
                    if ($itemTagDtl){
                        array_push($res, $itemTagDtl);
                        $parentIdx = array_keys(array_filter($res, fn($par) => $par['tag_id'] == $tag['parent_tag_id']))[0];
                    }
                }

                if ($parentIdx || $parentIdx == '0'){
                    if (!isset($res[$parentIdx]['child_list'])){
                        $res[$parentIdx]['child_list'] = [];
                    }

                    array_push($res[$parentIdx]['child_list'], $tag);
                }
            }
        }

        return $res;
    }
}
?>