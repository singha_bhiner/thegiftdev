<?php
/* 
    Objective: Navigation bar
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_nav
        tmap_cate_nav
        tmap_nav_page
        
*/


require_once dirname(__FILE__). '/abstract.class.php';
class nav extends baseClass{
    function init() {
        $this->multiLangFieldArr = ['name'];
	}

    function sortNav($shop_id, $nav_id_arr){
        // sorting the navigation bar
        // $shop_id: shop in use
        // $nav_id_arr: ascending sequence base on the nav_id_arr

        $this->DB->sort("tbl_nav", "nav_id", $nav_id_arr);
        return $this->getNavList($shop_id);
    }

    function sortNavPage($nav_id, $page_id_arr){
        // sorting the page content
        // $shop_id: shop in use
        // $page_id_arr: ascending sequence base on the page_id_arr

        $this->DB->sortMap("tmap_nav_page", "nav_id", $nav_id, "page_id", $page_id_arr);
        return null;
    }

    function sortNavCate($nav_id, $cate_id_arr){
        // sorting the navigation bar category
        // $shop_id: shop in use
        // $cate_id_arr: ascending sequence base on the cate_id_arr

        $this->DB->sortMap("tmap_cate_nav", "nav_id", $nav_id, "cate_id", $cate_id_arr);
        return null;
    }

    function getNavList($shop_id){
        // get the list
        // $shop_id: shop in use

        $sql = "SELECT * ";
        foreach($this->multiLangFieldArr AS $field){
            $sql .= ", $field" . "_" . $this->lang . " AS $field ";
        }
        $sql .= " FROM `tbl_nav` WHERE activate = 'Y' && shop_id = '$shop_id' ORDER BY `sort` ASC";
        $res = $this->DB->get_Sql($sql);

        foreach($res as $k => $row){
            if (!$row['link']){
                $nav_id = $row['nav_id'];
                // if no link it should be cate list or page list
                $sql = "SELECT * 
                    FROM `tmap_cate_nav` AS m
                    LEFT JOIN `tbl_cate` AS c ON c.cate_id = m.cate_id AND c.activate ='Y' 
                    WHERE m.activate ='Y' AND m.nav_id = $nav_id AND c.cate_id IS NOT NULL
                    ORDER BY m.sort ";
                $tres = $this->DB->get_Sql($sql);
                if ($tres)
                    $res[$k]['cateList'] = $tres;

                if (!$tres){
                    // if no cate list then should be page list
                    $sql = "SELECT *, name_" . $this->lang. " AS name
                            FROM `tmap_nav_page` AS m
                            LEFT JOIN `tbl_page` AS p ON p.page_id = m.page_id AND p.activate ='Y'
                            WHERE m.activate ='Y' AND m.nav_id = $nav_id AND p.page_id IS NOT NULL
                            ORDER BY m.sort ";
                    $tres = $this->DB->get_Sql($sql);
                    $res[$k]['pageList'] = $tres;
                }
            }
        }

        return $res;
    }

    function getNavDtl($nav_id){
        // get the navigation bar Detail
        // $nav_id: PK

        $sql = "SELECT n.*
                FROM `tbl_nav` AS n
                WHERE n.activate ='Y' && n.nav_id = '$nav_id'
                GROUP BY n.nav_id";
        $res = $this->DB->get_Sql($sql)[0];

        $sql = "SELECT cate_id FROM `tmap_cate_nav` AS m WHERE activate='Y' AND nav_id = '$nav_id'";
        $res['cate'] = $this->DB->get_Sql($sql);
        if($res['cate']){
            $t = [];
            foreach($res['cate'] as $page){
                array_push($t, $page['cate_id']);
            }
            $res['cate_id_arr'] = implode(",", $t);
        }
        
        $sql = "SELECT page_id FROM `tmap_nav_page` AS m WHERE activate='Y' AND nav_id = '$nav_id'";
        $res['page'] = $this->DB->get_Sql($sql);
        if ($res['page']){
            $t = [];
            foreach($res['page'] as $page){
                array_push($t, $page['page_id']);
            }
            $res['page_id_arr'] = implode(",", $t);
        }
        
        return $res;
    }

    function editNav($nav_id, $data){
        // edit navigation
        // $nav_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($nav_id == 0 )
            $nav_id = $this->DB->insert_db('tbl_nav', null);

        $this->DB->updateMap("tmap_cate_nav", "nav_id", $nav_id, "cate_id", $data['cate_id_arr']);
        unset($data['cate_id_arr']);

        $this->DB->updateMap("tmap_nav_page", "nav_id", $nav_id, "page_id", $data['page_id_arr']);
        unset($data['page_id_arr']);

        $sql = $this->DB->buildSql_update("tbl_nav", "nav_id", $nav_id, $data);
        $this->DB->update($sql);

        return $this->getNavDtl($nav_id);
    }

    function editNavList($shop_id, $nav_list){
        $sql = "UPDATE `tbl_nav` 
                SET activate = 'N'
                WHERE shop_id = $shop_id";
        $this->DB->update($sql);
        
        foreach($nav_list as $k => $data){
            $sql = "SELECT *
                    FROM `tbl_nav`
                    WHERE shop_id = $shop_id AND activate = 'N'";
            $row = $this->DB->get_Sql($sql)[0];
            
            $data['activate'] = 'Y';
            $data['sort'] = $k+1;
            
            if ($row){
                // row existing
                $data['nav_id'] = $row['nav_id'];
                $nav_id = $this->DB->update_Sql("tbl_nav", "nav_id", $data);
            } else {
                $nav_id = $this->DB->insert_db("tbl_nav", $data);
            }

            
            // clear the existing mapping row
            $sql = "UPDATE `tmap_cate_nav` 
                    SET activate = 'N'
                    WHERE nav_id = $nav_id";
            $this->DB->update($sql);

            $sql = "UPDATE `tmap_nav_page` 
                    SET activate = 'N'
                    WHERE nav_id = $nav_id";
            $this->DB->update($sql);

            $this->DB->updateMap("tmap_cate_nav", "nav_id", $nav_id, "cate_id", $data['cate_id_arr']);
            $this->DB->updateMap("tmap_nav_page", "nav_id", $nav_id, "page_id", $data['page_id_arr']);
        }
        return true;
    }
}
?>