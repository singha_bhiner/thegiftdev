/* ************************************************************************ */
/* Author : BTL
/* Prerequisite : jquery 1.7 或以上
/* Desc   : This js bundle is a structurize version for various ajax function with jquery
/*          该js捆绑包是带有jQuery的各种ajax函数的结构化版本   
/* Last Modify : David 
/* Last Update : 30 Nov 2020
/* Log         : - Add supprt for error thrown on plain text return
/* ************************************************************************ */

(function($) {
    $.bajax = function(settings) {
        settings = jQuery.extend({
            url: "", // 目標網址
            debug: "N", // 調試模式
            thirdParty: "N", // Y 為跨網調用 , YP 為跨網jsonp
            formElement: "N", // Y 為提交 form 
            formElementID: "", // DOM 中 form 的 id
            pureString: "N", // N : 回調 json  , Y : 回調為純文本
            timeout: 15000,
            method: "POST", // 支持 GET 或 POST
            headers: {}, // 提文的header 數據
            pwapage: 'N', // 是否 pwapage
            data: null,
            callback: function(obj) {}, // 回調的 callback
            errorCallback: function(obj) {} // 錯誤回調的 callback
        }, settings);

        if (settings.debug == 'Y') {
            console.log('Bajax : started');
            console.log('Bajax url : ' + settings.url);
        }

        if (!settings.url) {
            // 沒有url 的話 停止
            if (settings.debug == 'Y') {
                console.log('Bajax : missing url');
            }
            return;
        }
        if (settings.pwapage == 'Y') {
            // pwa 模式
            if (settings.formElement == 'Y') {
                var formElement = document.getElementById(settings.formElementID);
                var formData = new FormData(formElement);

                if (settings.method == 'POST') {
                    $.ajax({
                        url: settings.url,
                        headers: settings.headers,
                        type: settings.method,
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        timeout: settings.timeout,
                        success: function(data) {
                            if (settings.pureString == 'Y') {
                                if (settings.debug == 'Y') {
                                    console.log(data);
                                }
                                run(settings.callback(data));
                            } else {
                                var result_string = JSON.stringify(data, null, 0);
                                var obj = $.parseJSON(result_string);
                                if (settings.debug == 'Y') {
                                    console.log(obj);
                                }
                                if (obj.error) {
                                    console.log(obj.error);
                                    return;
                                }

                                //console.log(obj);
                                run(settings.callback(obj));
                                if (settings.debug == 'Y') {
                                    console.log('Bajax : ended');
                                }
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            //console.log('Error in Reading');
                            if (settings.errorCallback) {
                                run(settings.errorCallback(jqXHR, textStatus,errorThrown));
                            }
                        },
                        complete: function() {}
                    });
                }

                if (settings.method == 'GET') {
                    $.ajax({
                        url: settings.url,
                        headers: settings.headers,
                        type: settings.method,
                        data: $('#' + settings.formElementID).serialize(),
                        timeout: settings.timeout,
                        success: function(data) {
                            if (settings.pureString == 'Y') {
                                if (settings.debug == 'Y') {
                                    console.log(data);
                                }
                                run(settings.callback(data, $('#' + settings.formElementID).serialize()));
                            } else {
                                var result_string = JSON.stringify(data, null, 0);
                                var obj = $.parseJSON(result_string);
                                if (settings.debug == 'Y') {
                                    console.log(obj);
                                }
                                if (obj.error) {
                                    console.log(obj.error);
                                    return;
                                }

                                //console.log(obj);
                                run(settings.callback(obj, $('#' + settings.formElementID).serialize()));
                                if (settings.debug == 'Y') {
                                    console.log('Bajax : ended');
                                }
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            //console.log('Error in Reading');
                            if (settings.errorCallback) {
                                run(settings.errorCallback(jqXHR, textStatus,errorThrown));
                            }
                        },
                        complete: function() {}
                    });
                }

                return;
            }

            // PWA 非form模式
            $.ajax({
                type: settings.method,
                data: (settings.data ? settings.data : {}),
                url: settings.url,
                headers: settings.headers,
                beforeSend: settings.beforeSend,
                crossDomain: true,
                cache: false,
                timeout: settings.timeout,
                success: function(data) {
                    if (settings.pureString == 'Y') {
                        run(settings.callback(data));
                    } else {
                        var result_string = JSON.stringify(data, null, 0);
                        var obj = $.parseJSON(result_string);
                        if (settings.debug == 'Y') {
                            console.log(obj);
                        }
                        if (obj.error) {
                            console.log(obj.error);
                            return;
                        }

                        //console.log(obj);
                        run(settings.callback(obj));
                        if (settings.debug == 'Y') {
                            console.log('Bajax : ended');
                        }
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //console.log('Error in Reading');
                    if (settings.errorCallback) {
                        run(settings.errorCallback(jqXHR, textStatus,errorThrown));
                    }
                },
                complete: function() {}
            });

            return;
        }

        // 以下為 非pwa 模式
        if (settings.formElement == 'Y') {
            var formElement = document.getElementById(settings.formElementID);
            var formData = new FormData(formElement);

            var dataType = 'json';
            if (settings.pureString == 'Y') {
                dataType = 'text';
            }

            if (settings.method == 'POST') {
                $.ajax({
                    url: settings.url,
                    headers: settings.headers,
                    type: settings.method,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: dataType,
                    timeout: settings.timeout,
                    success: function(data) {
                        if (settings.debug == 'Y') {
                            console.log(data);
                        }
                        if (settings.pureString == 'Y') {
                            run(settings.callback(data));
                        } else {
                            var result_string = JSON.stringify(data, null, 0);
                            var obj = $.parseJSON(result_string);
                            if (settings.debug == 'Y') {
                                console.log(obj);
                            }
                            if (obj.error) {
                                console.log(obj.error);
                                return;
                            }

                            //console.log(obj);
                            run(settings.callback(obj));
                            if (settings.debug == 'Y') {
                                console.log('Bajax : ended');
                            }
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        if (settings.debug == 'Y') {
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            console.log('Error in Reading');
                        }
                        if (settings.errorCallback) {
                            run(settings.errorCallback(jqXHR, textStatus,errorThrown));
                        }
                    },
                    complete: function() {}
                });
            }

            if (settings.method == 'GET') {
                $.ajax({
                    url: settings.url,
                    headers: settings.headers,
                    type: settings.method,
                    data: $('#' + settings.formElementID).serialize(),
                    dataType: dataType,
                    timeout: settings.timeout,
                    success: function(data) {
                        if (settings.pureString == 'Y') {
                            run(settings.callback(data));
                        } else {
                            var result_string = JSON.stringify(data, null, 0);
                            var obj = $.parseJSON(result_string);
                            if (settings.debug == 'Y') {
                                console.log(obj);
                            }
                            if (obj.error) {
                                console.log(obj.error);
                                return;
                            }

                            //console.log(obj);
                            run(settings.callback(obj));
                            if (settings.debug == 'Y') {
                                console.log('Bajax : ended');
                            }
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        if (settings.debug == 'Y') {
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            console.log('Error in Reading');
                        }
                        if (settings.errorCallback) {
                            run(settings.errorCallback(jqXHR, textStatus,errorThrown));
                        }
                    },
                    complete: function() {}
                });
            }

            return;
        }

        if (settings.thirdParty == 'YP') {
            // 跨網模式 jsonp
            $.ajax({
                type: settings.method,
                url: settings.url,
                headers: settings.headers,
                beforeSend: settings.beforeSend,
                data: {
                    todo: "jsonp"
                },
                dataType: "jsonp",
                crossDomain: true,
                cache: false,
                timeout: settings.timeout,
                success: function(data) {
                    var result_string = JSON.stringify(data, null, 0);
                    var obj = $.parseJSON(result_string);
                    run(settings.callback(obj));
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log('Error in Reading');
                    if (settings.errorCallback) {
                        run(settings.errorCallback(jqXHR, textStatus,errorThrown));
                    }
                },
                complete: function() {}
            });
            return;
        }

        if (settings.thirdParty == 'Y') {
            // 跨網模式 json
            $.ajax({
                type: settings.method,
                url: settings.url,
                headers: settings.headers,
                beforeSend: settings.beforeSend,
                data: {
                    todo: "json"
                },
                dataType: "json",
                crossDomain: true,
                cache: false,
                timeout: settings.timeout,
                success: function(data) {
                    //console.log(data);
                    run(settings.callback(data));
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                    console.log('Error in Reading');
                    if (settings.errorCallback) {
                        run(settings.errorCallback(jqXHR, textStatus,errorThrown));
                    }
                },
                complete: function() {}
            });
            return;
        }

        if (settings.pureString == 'Y') {
            if (settings.data) {
                $.ajax({
                    url: settings.url,
                    headers: settings.headers,
                    beforeSend: settings.beforeSend,
                    type: settings.method,
                    data: (settings.data ? settings.data : {}),
                    cache: false,
                    contentType: false,
                    processData: false,
                    timeout: settings.timeout,
                    success: function(data) {
                        run(settings.callback(data));
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        if (settings.debug == 'Y') {
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            console.log('Error in Reading');
                        }
                        if (settings.errorCallback) {
                            run(settings.errorCallback(jqXHR, textStatus,errorThrown));
                        }
                    },
                    complete: function() {}
                });
            } else {
                $.ajax({
                    url: settings.url,
                    headers: settings.headers,
                    beforeSend: settings.beforeSend,
                    type: settings.method,
                    data: (settings.data ? settings.data : {}),
                    cache: false,
                    contentType: false,
                    processData: false,
                    timeout: settings.timeout,
                    success: function(data) {
                        run(settings.callback(data));
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        if (settings.debug == 'Y') {
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            console.log('Error in Reading');
                        }
                        if (settings.errorCallback) {
                            run(settings.errorCallback(jqXHR, textStatus,errorThrown));
                        }
                    },
                    complete: function() {}
                });
            }


            return;
        }

        // 基本 request
        //console.log('iam here');
        $.ajax({
            type: settings.method,
            url: settings.url,
            headers: settings.headers,
            beforeSend: settings.beforeSend,
            data: (settings.data ? Object.assign({}, { todo: "json" }, settings.data) : { todo: "json" }),
            dataType: "json",
            timeout: settings.timeout,
            success: function(data) {
                var result_string = JSON.stringify(data, null, 0);
                var obj = $.parseJSON(result_string);
                if (settings.debug == 'Y') {
                    console.log(obj);
                }
                if (obj.error) {
                    console.log(obj.error);
                    return;
                }

                //console.log(obj);
                run(settings.callback(obj));
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //console.log('1');
                if (settings.debug == 'Y') {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                    console.log('Error in Reading');
                }
                if (settings.errorCallback) {
                    run(settings.errorCallback(jqXHR, textStatus,errorThrown));
                }
            },
            complete: function() {
                //console.log('2');
            }
        });

        function run(funcToExecute) {
            //load some async content
            //funcToExecute(obj); // NOTE: parentheses
        }
    }
}(jQuery));