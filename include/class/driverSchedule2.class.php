<?php
require_once dirname(__FILE__). '/abstract.class.php';
class driverschedule extends baseClass{

    function databaseConstruct(){
        $sql = "
            DROP TABLE IF EXISTS `tbl_driver_schedule`;
            CREATE TABLE IF NOT EXISTS `tbl_driver_schedule` (
                `driver_schedule_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `driver_admin_id` INT(11) DEFAULT NULL,
                `start` datetime DEFAULT NULL,
                `end` datetime DEFAULT NULL,
                `text` LONGTEXT DEFAULT NULL,
                `create_time` DATETIME DEFAULT NULL,
                `create_by` INT(11) DEFAULT NULL,
                `last_update_time` DATETIME DEFAULT NULL,
                `last_update_by` INT(11) DEFAULT NULL,
                `activate` VARCHAR(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y'
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ";
        $this->DB->update($sql);
    }

//////////////////////////////////////////// DRIVER SCHEDULE START ////////////////////////////////////////////

    function getDriverList(){
        $sql = "SELECT ad.* 
                FROM `tbl_driver_schedule` AS sc
                LEFT JOIN `tbl_admin` AS ad ON ad.admin_id = sc.driver_admin_id
                WHERE sc.activate = 'Y'
                GROUP BY ad.admin_id ";
        return $this->DB->get_Sql($sql);
    }

    function getDriverScheduleList($filter = null, $sorting = null, $limiter = null, $dispatch = true){
		$fromShort = "driverschedule";
		$selectSql = "SELECT $fromShort.driver_schedule_id as id, start, end, text AS title ";
		$fromSql = "FROM `tbl_driver_schedule` AS $fromShort";
        $whereSql = " WHERE $fromShort.`activate` = 'Y'";
		$groupSql = " GROUP BY $fromShort.driver_schedule_id";
		if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
			$whereSql = " WHERE 1=1";

		if ($sorting)
			$sortSql = " ORDER BY $sorting";
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
                switch ($k){
                    case 'driver_schedule_id':
                        $whereSql .= " AND ($fromShort.`driver_schedule_id` = '$i')";
                        break;
                    case 'admin_id':
                        $whereSql .= " AND ($fromShort.`driver_admin_id` = '$i')";
                        break;

                } 
			}
		}
		
		$sql = $selectSql . $fromSql . $whereSql . $groupSql . $sortSql  . $limitSql;
		$res = $this->DB->get_Sql($sql);

        if ($dispatch){
            foreach($res as $k => $i){
                $res[$k] = $this->dispatchDriverSchedule($i);
            }
        }

		return $res;
	}

    function getDriverSchedule($driver_schedule_id){
        $filter = [
            "driver_schedule_id" => $driver_schedule_id
        ];

        return $this->getDriverScheduleList($filter, null, $this->DB->getUnique)[0];
    }

    function dispatchDriverSchedule($driverSchedule){
        return $driverSchedule;
    }

    function editDriverSchedule($data){
        if (isset($data['driver_schedule_id']) && $data['driver_schedule_id'] != 0) {
			$id = $this->DB->update_Sql("tbl_driver_schedule", "driver_schedule_id", $data);
		} else {
			$id = $this->DB->insert_db("tbl_driver_schedule", $data);
		} 

        return $id;
    }

//////////////////////////////////////////// DRIVER SCHEDULE END   ////////////////////////////////////////////
}
?>