<?php
/* 
    Objective: coupon code related 
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_coupon
        tmap_cate_coupon: which category can use this coupon, null DEFAULT ava
        tmap_cate_coupon_productBlack: product black list
        tmap_cate_coupon_productWhite: product white list, null DEFAULT ava for all


        
*/
require_once dirname(__FILE__). '/abstract.class.php';
class coupon extends baseClass{
   

    function executeCoupon($coupon_code){
        // execute the coupon 
        // $coupon_code: tbl_coupon.code

        // no event when coupon execute, action done in the orderData already
    }

    function getCouponDtlByCode($coupon_code, $final_price, $cate_arr = null, $product_arr = null){
        // get the coupon base on the tbl_coupon.code
        // $coupon_code: tbl_coupon.code
        // $final_price: the final price for the order 
        //               ie. limitation for some coupon have minimum final price (tbl_coupon.minimu_order_amt)
        // $cate_arr: the category which the order have
        // $product_arr: the product which the order have

        $sql = "SELECT c.*
                FROM `tbl_coupon` AS c
                WHERE c.`activate` = 'Y' AND c.`code` = '$coupon_code'
                    AND c.minimum_order_amt < '$final_price' 
                    AND (c.expiry_date < NOW() OR c.expiry_date IS NULL)";
        $res = $this->DB->get_Sql($sql)[0];
        
        if ($res){
            // check used limit
            if ($res['usage_limit']){
                $sql = "SELECT * 
                        FROM `tbl_order` AS o
                        WHERE o.activate = 'Y'";
                $orderList = $this->DB->get_Sql($sql);

                if (sizeof($orderList) > $res['usage_limit'])
                    new apiDataBuilder(11, null, "Coupon is over the usage limit");
            }
            
			if ($res['type'] == 'P'){
				$res['act_discount'] = round($final_price * $res['discount'] / 100);
			} elseif ($res['couponDtl']['type'] == 'A'){
				$res['act_discount'] = $res['discount'];
			}

            $coupon_id = $res['coupon_id'];

            if ($product_arr){
                // check white list
                $sql = "SELECT m.product_id
                        FROM `tmap_coupon_productWhiteList` AS m
                        WHERE m.activate ='Y' AND m.coupon_id = '$coupon_id';";
                $whiteList = $this->DB->get_Sql($sql);
    
                if ($whiteList){
                    foreach ($whiteList as $k => $white){
                        if(in_array($white, $product_arr)){
                            return $res;
                        }
                    }
                } 
            }
 
            if ($product_arr){
                // check black list
                $sql = "SELECT m.product_id
                        FROM `tmap_coupon_productBlackList` AS m
                        WHERE m.activate ='Y' AND m.coupon_id = '$coupon_id';";
                $blackList = $this->DB->get_Sql($sql);
                
                if ($blackList){
                    foreach ($blackList as $k => $black){
                        if(in_array($black, $product_arr)){
                            new apiDataBuilder(11, null, "This coupon is not available in this product.");
                        }
                    }
                }
            }

            if ($cate_arr){
                // check category
                $sql = "SELECT m.cate_id
                        FROM `tmap_cate_coupon` AS m
                        WHERE m.activate ='Y' AND m.coupon_id = '$coupon_id';";
                $cateList = $this->DB->get_Sql($sql);
    
                if ($cateList){
                    $exist = false;
    
    
                    foreach ($cateList as $k => $cate){
                        if(in_array($cate, $cate_arr)){
                            $exist = true;
                        }
                    }
    
                    if (!$exist)
                        new apiDataBuilder(11, null, "This coupon is not available in this category.");
                }
            }


            return $res;

        } else {
            // coupon code is not existed
            new apiDataBuilder(11, null, "This coupon is not exist.");
        }
    }

    function editCoupon($coupon_id, $data){
        // edit coupon
        // $coupon_id: tbl_coupon PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($coupon_id == 0)
            $coupon_id = $this->DB->insert_db('tbl_coupon', null);
            
        $this->DB->updateMap("tmap_coupon_email", "coupon_id", $coupon_id, "email", $data['email_suffix']);
        unset($data['email_suffix']);
            
        $this->DB->updateMap("tmap_cate_coupon", "coupon_id", $coupon_id, "cate_id", $data['cate_id_arr']);
        unset($data['cate_id_arr']);
            
        $this->DB->updateMap("tmap_coupon_productBlackList", "coupon_id", $coupon_id, "product_id", $data['blist_product_id_arr']);
        unset($data['blist_product_id_arr']);
            
        $this->DB->updateMap("tmap_coupon_productWhiteList", "coupon_id", $coupon_id, "product_id", $data['wlist_product_id_arr']);
        unset($data['wlist_product_id_arr']);
            
        $this->DB->updateMap("tmap_coupon_cateBlackList", "coupon_id", $coupon_id, "cate_id", $data['blist_cate_id_arr']);
        unset($data['blist_cate_id_arr']);
            
        $this->DB->updateMap("tmap_coupon_cateWhiteList", "coupon_id", $coupon_id, "cate_id", $data['wlist_cate_id_arr']);
        unset($data['wlist_cate_id_arr']);
        
        $sql = $this->DB->buildSql_update("tbl_coupon", "coupon_id", $coupon_id, $data);

        $this->DB->update($sql);

        return $this->getCouponDtl($coupon_id);
    }

    function getCouponList($shop_id, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: which shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by
        $sql = "SELECT * FROM `tbl_coupon` WHERE `shop_id` = '$shop_id' AND `activate` ='Y'";

        if ($filter){
            foreach($filter as $k => $i){
                if ($i || $i == '0'){
                    switch ($k){
                        case 'skey':
                            $sql .= " AND (coupon_id = '$i' OR code LIKE '%$i%' OR adm_name LIKE '%$i%')";
                            break;
                        
                        default: 
                            break;
                    }                    
                }
            }
        }

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        return $res;
    }

    function getCouponDtl($coupon_id){
        // get the coupon Detail
        // $coupon_id: PK

        $sql = "SELECT v.*, GROUP_CONCAT(ce.email) AS email_suffix
                FROM `tbl_coupon` AS v
                LEFT JOIN `tmap_coupon_email` AS ce ON ce.coupon_id = v.coupon_id AND ce.activate = 'Y'
                WHERE v.`activate` = 'Y' && v.`coupon_id` = '$coupon_id'";
        $res = $this->DB->get_Sql($sql)[0];

        if ($res){
            // get the category list
            $sql = "SELECT GROUP_CONCAT(m.cate_id) AS cate_id_arr
                    FROM `tmap_cate_coupon` AS m 
                    INNER JOIN `tbl_cate` AS c ON c.cate_id = m.cate_id
                    WHERE m.activate = 'Y' AND c.activate = 'Y' AND m.coupon_id = '$coupon_id' GROUP BY m.coupon_id";
            $t = $this->DB->get_Sql($sql)[0];
            $res['cate_id_arr'] = $t['cate_id_arr'];

            // get the black list product list
            $sql = "SELECT m.product_id
                    FROM `tmap_coupon_productBlackList` AS m 
                    WHERE m.activate = 'Y' AND m.coupon_id = '$coupon_id'";
            $t  = $this->DB->get_Sql($sql);
            $arr = [];
            foreach ($t as $i){
                array_push($arr, $i['product_id']);
            }
            $res['blist_product_id_arr'] = $arr;
            
            $sql = "SELECT m.cate_id
                    FROM `tmap_coupon_cateBlackList` AS m 
                    WHERE m.activate = 'Y' AND m.coupon_id = '$coupon_id'";
            $t  = $this->DB->get_Sql($sql);
            $arr = [];
            foreach ($t as $i){
                array_push($arr, $i['cate_id']);
            }
            $res['blist_cate_id_arr'] = $arr;
            

            // get the white list product list
            $sql = "SELECT m.product_id
                    FROM `tmap_coupon_productWhiteList` AS m 
                    WHERE m.activate = 'Y' AND m.coupon_id = '$coupon_id'";
            $t  = $this->DB->get_Sql($sql);
            $arr = [];
            foreach ($t as $i){
                array_push($arr, $i['product_id']);
            }
            $res['wlist_product_id_arr'] = $arr;

            $sql = "SELECT m.cate_id
                    FROM `tmap_coupon_cateWhiteList` AS m 
                    WHERE m.activate = 'Y' AND m.coupon_id = '$coupon_id'";
            $t  = $this->DB->get_Sql($sql);
            $arr = [];
            foreach ($t as $i){
                array_push($arr, $i['cate_id']);
            }
            $res['wlist_cate_id_arr'] = $arr;
        }

        return $res;
    }
}
?>