<?php
class pageNavigator {
	public $lang;
	public $nav;
	public $RM;
	
	public function __construct($lang,$RM) {
		$this->lang = $lang;
		$this->RM = $RM;
		$this->nav = $this->navigatorConstruct();
	}
	

	function navigatorConstruct(){
		// 生成頁面導航 

		// Sample

		$groupkey = 'userx';
		$navigator['admin'][$groupkey]['code'] = $groupkey;
		$navigator['admin'][$groupkey]['url'] = "/admin/$groupkey";
		$navigator['admin'][$groupkey]['router'] = "/admin/$groupkey";
		$navigator['admin'][$groupkey]['page_title'] = "管理帳戶";	

		$submodekey = 'myAccount';
		$navigator['admin'][$groupkey][$submodekey]['code'] = $submodekey;
		$navigator['admin'][$groupkey][$submodekey]['url'] = "/admin/$groupkey/$submodekey";
		$navigator['admin'][$groupkey][$submodekey]['router'] = "/admin/$groupkey/$submodekey";
		$navigator['admin'][$groupkey][$submodekey]['page_title'] = "我的帳戶";


		/* ============================================================================================ */


		return $navigator;
	}

	function navigatorConstructDynamic($page,$dynamicData,$pageTitle){
		if($page=='property'){
			$navigator[$page]['code'] = "$page";
			$navigator[$page]['url'] = "/$page/$dynamicData";
			$navigator[$page]['router'] = "/propertySearch/$page/$dynamicData";
			$navigator[$page]['page_title'] = $pageTitle;	
		}
		return $navigator;
	}

	function createNavMenuArray($url,$navObject=null){
		if(!$navObject){
			$navObject = $this->navigatorConstruct();	
		} else{
			// do nth
		}
		$final_nav_array = array();
		$nav_array = explode('/', $url);

		$last_key=array();
		foreach($nav_array as $k=>$i){
			if(!$i){
				$final_nav_array[$k]['key'] = "主頁";
				$final_nav_array[$k]['url'] = "/admin/";
			} else {
				if(@$navObject['admin'][$i]['code'] == $i){
					$final_nav_array[$k]['key'] = $navObject['admin'][$i]['page_title'];
					$final_nav_array[$k]['url'] = $navObject['admin'][$i]['url'];
				} else {
					if(@$navObject[$last_key[0]][$i]['page_title'] && $navObject[$last_key[0]][$i]['url']){
						//die('abbbb');
						$final_nav_array[$k]['key'] = $navObject[$last_key[0]][$i]['page_title'];
						$final_nav_array[$k]['url'] = $navObject[$last_key[0]][$i]['url'];	
					} elseif(@$navObject[$last_key[1]][$last_key[0]][$i]['page_title'] && $navObject[$last_key[1]][$last_key[0]][$i]['url']) {
						$final_nav_array[$k]['key'] = $navObject[$last_key[1]][$last_key[0]][$i]['page_title'];
						$final_nav_array[$k]['url'] = $navObject[$last_key[1]][$last_key[0]][$i]['url'];	
					} 
					
				}
				
			}
			if($i && trim($i)!=''){
				array_unshift($last_key,$i);	
			}
		}
		return $final_nav_array;
	}
}
?>