<?php
class uiConfig {
	public $lang;
	
	public function __construct($gthis,$lang) {
		$this->gthis = $gthis;
		$this->lang = $lang;
		$this->colorThemeSetting();
	}

	/* defined below in tailwind css standard */
	function colorThemeSetting(){
		$colorTheme = array();
		$colorTheme['mainBodyBackgroundColor'] = 'bg-gray-100';
		$colorTheme['menuBarBackgroundColor'] = 'bg-gray-700';
		$colorTheme['menuBarTextColor'] = 'text-white';


		$colorTheme['submenuBarBorderColor'] = 'border-gray-700';
		$colorTheme['submenuBarTextColor'] = 'text-gray-700';
		$colorTheme['submenuBarActiveBackgroundColor'] = 'bg-gray-700';
		$colorTheme['submenuBarActiveTextColor'] = 'text-white';

		$colorTheme['navBreadCrumbTextColor'] = '';
		$this->gthis->assign('CT',$colorTheme);
	}
}
?>