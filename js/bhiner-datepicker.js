function bhinerDatePickerInit(e) {



    e.preventDefault()
    $(e.target).blur()

    var _lang = $('html').attr('lang');
    moment.locale(_lang);

    var $elem = $(this)
    if ($elem.data().datepickerEnabled == true) {
        return false
    } else {
        $elem.data().datepickerEnabled = true
        debug('datepicker summoned')
    }
    _dateFormat = $elem.data().dateFormat == undefined ? 'YYYY/MM/DD' : $elem.data().dateFormat

    debug($elem.data())

    initParamater = {
        timestamp: new Date().getTime(),
        defaultMonthRange: $elem.data().monthRange == undefined ? null : $elem.data().monthRange,
        startFromToday : true,
        rangeStart: $elem.data().start == undefined ? null : moment($elem.data().start, _dateFormat), //
        rangeEnd: $elem.data().end == undefined ? null : moment($elem.data().end, _dateFormat),
        sundayAvailable: $elem.data().sundayAvailable == undefined ? true : $elem.data().sundayAvailable == false ? false : true,
        saturdayAvailable: $elem.data().saturdayAvailable == undefined ? true : $elem.data().saturdayAvailable == false ? false : true,
        currentSelectedDate: $elem.val().trim().length == 0 ? null : moment($elem.val(), _dateFormat).isValid() ? moment($elem.val(), _dateFormat) : moment(),
        blockedDate: null,
        ___: true,
    }

    if(initParamater.startFromToday == true){
        initParamater.rangeStart = moment(new Date()).subtract(1, 'days')
    }

    if ($elem.data().blockedDate) {
        var momentBlockedDateTemp = []
        $elem.data().blockedDate.split(',').forEach(function(i) {
            debug(moment(i, 'DD/MM/YYYY'))
            momentBlockedDateTemp.push(moment(i, 'DD/MM/YYYY'))
        })
        initParamater.blockedDate = momentBlockedDateTemp
    }


    debug(initParamater)

    //define body dom

    var $body = $('body'),
        _offset = {
            top: $elem.offset().top + $elem.outerHeight(),
            left: $elem.offset().left
        }

        var bodyPositionHacker = 0
        //hack for vk modal
        if($body.css('top') != 'auto'){
            bodyPositionHacker = parseFloat($body.css('top')) * -1
            _offset.top = _offset.top - bodyPositionHacker
        }

    function datePickerDomGenerator() {
        return `<div class="bhiner-date-picker-${initParamater.timestamp} absolute left-0 w-full" id="bhiner-date-picker" style="z-index:635535; top:${bodyPositionHacker}px;">
        ${innerBuilder()}
        </div>`

        function innerBuilder() {
            console.log('new off set top :' + _offset.top)
            if (mobileCheck()) {
                //mobile version of datepicker
                return `<div class="fixed top-0 left-0 h-screen w-screen pointer-events-none ease-out transition-all duration-150 opacity-0" style="background-color:rgba(0,0,0,.3)">
                <div class="month-graph-container fixed bottom-0 w-full bg-white py-4 px-4 pointer-events-auto  ease-out rounded-t-3xl transform transition-all duration-150 translate-y-full"
                style="min-height: 440px;box-shadow: 0 4px 20px 0 rgba(0,0,0,.3);">${monthGraphDomGenerator(initParamater.currentSelectedDate)}</div>
                </div>`
            } else {
                //desktop version
                return `<div class="absolute py-2 datepicker-act-js-dom" style="left:${_offset.left}px; top: ${_offset.top}px;" data-debug-top="${_offset.top}"> 
                    <div class="bg-white rounded-xl p-8 transition-all duration-150 ease-out opacity-0 translate-y-5 transform" style="box-shadow: 0 1px 3px 0 rgb(0 0 0 / 6%), 0 10px 14px 0 rgba(0,0,0,.1); ">
                        <div class="month-graph-container">${monthGraphDomGenerator(initParamater.currentSelectedDate)}</div>
                    </div>
                </div>`
            }
        }

    }

    //month dom generator
    function monthGraphDomGenerator(_initData) {
        var _initData = _initData;

        //calculate variables for months
        if (_initData == null || _initData == undefined) {
            _initData = moment(new Date())
        }

        //get total days in this month
        var totalWeekdayInMonth = _initData.daysInMonth()

        //get the first date and its weekday
        var weekdayOfInitialDate = moment(`${_initData.year()}/${_initData.month() + 1}/1`, 'YYYY/MM/DD').weekday()
        var weekdaysDom = ''

        //generate static weekday
        for (var weekday = 0; weekday < 7; weekday++) {
            var weekdayStringZh = ['日', '一', '二', '三', '四', '五', '六']
            var weekdayStringEn = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa']

            var finalWeekDayString;

            if (_lang.indexOf('zh') != -1) {
                finalWeekDayString = weekdayStringZh
            } else {
                finalWeekDayString = weekdayStringEn
            }

            weekdaysDom += `<div class="text-center font-semibold text-gray-400 mb-2">${finalWeekDayString[weekday]}</div>`
        }
        //generate dynamic weekday
        for (var _days = 0; _days < totalWeekdayInMonth + weekdayOfInitialDate; _days++) {
            if (_days < weekdayOfInitialDate) {
                weekdaysDom += `<div></div>`
            } else {

                //date availablility checker
                var domDate = moment(`${_initData.year()}-${_initData.month() + 1}-${_days - weekdayOfInitialDate + 1}`, 'YYYY-MM-DD')
                var dateVaild = (initParamater.rangeEnd == null ? true : domDate.isBefore(initParamater.rangeEnd)) && (initParamater.rangeStart == null ? true : domDate.isSameOrAfter(initParamater.rangeStart)) && ((initParamater.sundayAvailable == true ? true : domDate.weekday() != 0) && (initParamater.saturdayAvailable == true ? true : domDate.weekday() != 6))
                var currentPickedDate = domDate.isSame(initParamater.currentSelectedDate, 'day')

                //holiday or custome day blocker
                if (initParamater.blockedDate) {
                    initParamater.blockedDate.forEach(function(date) {
                        if (date.isSame(moment(`${_initData.year()}-${_initData.month() + 1}-${_days - weekdayOfInitialDate + 1}`, 'YYYY-MM-DD'))) {
                            dateVaild = false
                        }
                    })
                }

                //debug(`${dateVaild} : ${moment(`${_initData.year()}-${_initData.month() + 1}-${_days - weekdayOfInitialDate + 1}`, 'YYYY-MM-DD').format(_dateFormat)}`)

                //final DOM
                weekdaysDom += `<a href="#" class="act-date-picker text-center w-12 h-12 font-bold relative hover:bg-gray-50 rounded ${currentPickedDate == true ? 'border-2 border-theme-black ' : ''} ${!dateVaild ? 'opacity-20 pointer-events-none' : ''}"
                data-date="${moment(`${_initData.year()}-${_initData.month() + 1}-${_days - weekdayOfInitialDate + 1}`, 'YYYY-MM-DD').format(_dateFormat)}">
                <div class="flex items-center justify-center w-full h-full rounded-full text-theme-black ">${_days - weekdayOfInitialDate + 1}</div></a>`
            }
        }

 

        //DOM function binders
        //date pick act action
        function datepickListener(e){
             e.preventDefault()
             //change the value of the input
            $elem.val($(e.target).closest('.act-date-picker').attr('data-date'))
             //trigger click event
             $elem.trigger('change')
            //and close the date picker
            datePickerCloser()
           
        }


        $(document).off('click',`.bhiner-date-picker-${initParamater.timestamp} .act-date-picker`, datepickListener)
        .on('click',`.bhiner-date-picker-${initParamater.timestamp} .act-date-picker`, datepickListener)

        //final DOM return

        //calculate available month
        var previousMonthAvailable = false,
        nextMonthAvailable = false

        if(initParamater.rangeStart == null ? true : _initData.clone().subtract(1, 'months').isSameOrAfter(initParamater.rangeStart, 'month')){
            previousMonthAvailable = true
        }

        if(initParamater.rangeEnd == null ? true : _initData.clone().add(1, 'months').isSameOrBefore(initParamater.rangeEnd, 'month')){
            nextMonthAvailable = true
        }

        //bind functions for pre and next month
        $(document).off('click', '.previous-date-month, .next-date-month')
        .on('click', '.previous-date-month, .next-date-month', function(e){
       
            e.preventDefault()
  
            if($(e.target).is('.next-date-month')){
                //next month
                $(`.bhiner-date-picker-${initParamater.timestamp} .month-graph-container`).html(monthGraphDomGenerator(_initData.clone().add(1, 'months')))
            } else {
                //previous month
                $(`.bhiner-date-picker-${initParamater.timestamp} .month-graph-container`).html(monthGraphDomGenerator(_initData.clone().subtract(1, 'months')))
            }
        })

        return `
        <div class="dat-picker-navigator-bar flex flex-row mb-4">
            <a href="#" class="previous-date-month text-2xl px-4 text-gray-600 hover:bg-gray-50 rounded ${previousMonthAvailable == true ? '' : 'pointer-events-none opacity-20'}"><i class="ri-arrow-left-s-line pointer-events-none"></i></a>
            <div class="flex items-center font-bold justify-center text-center flex-1">${_lang.indexOf('zh') != -1 ? _initData.format('YYYY年MMM') : _initData.format('MMM YYYY')}</div>
            <a href="#" class="next-date-month text-2xl px-4 text-gray-600 hover:bg-gray-50 rounded ${nextMonthAvailable == true ? '' : 'pointer-events-none opacity-20'}"><i class="ri-arrow-right-s-line pointer-events-none"></i></a>
        </div>
        <div class="grid grid-cols-7 gap-0">
        ${weekdaysDom}
        </div>
        `
    }

    //append the whole DOM
    $body.append(datePickerDomGenerator())

    //and start the css animation
    setTimeout(function(){
        var $pop = $(`.bhiner-date-picker-${initParamater.timestamp}`);
        var $picker = $pop.find('.datepicker-act-js-dom')
        $pop.children('div').addClass('opacity-100').children('div').removeClass('opacity-0 translate-y-full translate-y-5')
        // if(!mobileCheck()){
        //     //not mobile and the bottom of screen cannot contain the popup :
        //     if(window.innerHeight < $elem.offset().top + $elem.outerHeight() + $picker.outerHeight() ){

        //         var offsetCal = $elem.offset().top - 10 - $picker.outerHeight()
        //         if(($elem.position().top == 0 ? $elem.offset().top : $elem.position().top) < ($picker.outerHeight() + 20)){
        //             offsetCal = $elem.offset().top + 10 + $elem.outerHeight()
        //         }

        //         $picker.css({
        //             //top : $elem.offset().top - 10 - $picker.outerHeight()
        //             top : offsetCal
        //         })
        //     }
            
        // }
   
    },1)
    //debug($(`bhiner-date-picker-${initParamater.timestamp}`).children('div'))

    //datepicker closer
    function datePickerCloser() {
        $('.bhiner-date-picker-' + initParamater.timestamp).remove()
        debug($elem.data())
        $elem.data().datepickerEnabled = false
    }

    $(document).off('mousedown', '#bhiner-date-picker')
    .on('mousedown', '#bhiner-date-picker', function(e){
       if($(e.target).is('#bhiner-date-picker')){
          
           datePickerCloser()
       } else {
           //debug('cannot close..')
       }
    })
 
    // $elem.off('blur', datePickerCloser)
    //     .on('blur', datePickerCloser)


    //screen resizer - to config the dom's responsiveness
    function windowResizeController() {
        $(`.bhiner-date-picker-${initParamater.timestamp}`).css({
            height: $(window).innerHeight()
        })
    }
    $(window).off('resize', windowResizeController)
        .on('resize', windowResizeController)

    //set the init height for the background
    windowResizeController()

}

$(document).off('focus', '.bhiner-datepicker', bhinerDatePickerInit)
    .on('focus', '.bhiner-datepicker', bhinerDatePickerInit)