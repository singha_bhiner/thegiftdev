<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;
		
		switch ($this->gthis->submode) {
			case 'popupList':
				$result_array = $this->popupList();
				break;
			case 'popupDtl':
				$result_array = $this->popupDtl();
				break;
			case 'style':
				$result_array = $this->style();
				break;
			case 'info':
				$result_array = $this->info();
				break;
			case 'nav':
				$result_array = $this->nav();
				break;
			case 'navList':
				$result_array = $this->navList();
				break;
			case 'navDtl':
				$result_array = $this->navDtl();
				break;
			case 'pageDtl':
				$result_array = $this->pageDtl();
				break;
			case 'clientList':
				$result_array = $this->clientList();
				break;
			default:
				array_push($this->gthis->breadcrumb, [
					"code" => $this->gthis->submode,
					"url" => "admin/page/" . $this->gthis->submode,
					"page_title" => $this->gthis->RMBC->getReturnString($this->gthis->submode)
				]);
				break;
		}

		$result_array['pageList'] = $this->pageList();
		@$this->gthis->assign('result_array',$result_array);
	}

	function clientList(){
		if (intval($_SESSION['right']['page']) < 30){
			return array("right"=>"Failure");
		}
		
		require_once ROOT.'/include/class/client.class.php';
		$clientCls = new client($this->DB, $this->lang, $this->isAdmin);

		$filter = $this->gthis->paramBuilder([
			["key" => "shop_id", "request" => "session"]
		]);

		$client_list = $clientCls->getClientList($filter);

		$res['client_list'] = $client_list;
		
		array_push($this->gthis->breadcrumb, [
			"code" => $this->gthis->submode,
			"url" => "admin/page/" . $this->gthis->submode,
			"page_title" => $this->gthis->RMBC->getReturnString($this->gthis->submode)
		]);
		return $res;
	}

	function popupList(){
		if (intval($_SESSION['right']['page']) < 30){
			return array("right"=>"Failure");
		}
		
		require_once ROOT.'/include/class/page.class.php';
		$pageCls = new page($this->DB, $this->lang, $this->isAdmin);
		
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');		

		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "popup_id",
			"order" => $order ? $order : "ASC"
		];

		$res['popupList'] = $pageCls->getPopupList($shop_id, $filter, $limiter, $sorting);

		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($pageCls->getPopupList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "popupList",
			"url" => "admin/product/popupList",
			"page_title" => $this->gthis->RMBC->getReturnString('popupList')
		]);
		return $res;
	}

	function popupDtl(){
		if (intval($_SESSION['right']['page']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/page.class.php';
		$page = new page($this->DB, $this->lang, $this->isAdmin);

		$popup_id = $this->gthis->parm_sql('request', 'popup_id', 'N');
		$result_array['popupDtl'] = $page->getPopupDtl($popup_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "popupList",
			"url" => "admin/page/popupList",
			"page_title" => $this->gthis->RMBC->getReturnString('popupList')
		],[
			"code" => "popupDtl",
			"url" => "admin/page/popupDtl?nav_id=" . ($popup_id ? $popup_id : "0"),
			"page_title" => $popup_id? $result_array['popupDtl']['adm_name'] : $this->gthis->RMBC->getReturnString('addPopup')
		]);
		return $result_array;
	}
	
	function pageList(){
		if (intval($_SESSION['right']['page']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/page.class.php';
		$page = new page($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		return $page->getPageList($shop_id);
	}

	function pageDtl(){
		if (intval($_SESSION['right']['page']) < 30){
			return array("right"=>"Failure");
		}

		$shop_id = $this->gthis->parm_in('session', 'shop_id');

		require_once ROOT.'/include/class/page.class.php';
		$page = new page($this->DB, $this->lang, $this->isAdmin);

		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$res['productList'] = $productCls->getProductList($shop_id);

		$page_id = $this->gthis->parm_sql('request', 'page_id', 'N');
		$res['page'] = $page->getPageDtl($page_id);


		array_push($this->gthis->breadcrumb, [
			"code" => "pageDtl",
			"url" => "admin/page/pageDtl?page_id=" . ($page_id ? $page_id : "0"),
			"page_title" => $page_id ? $res['page']['name_tc'] : $this->gthis->RMBC->getReturnString('addPage')
		]);
		return $res;
	}

	function navDtl(){
		if (intval($_SESSION['right']['page']) < 30){
			return array("right"=>"Failure");
		}
		$nav_id = $this->gthis->parm_sql('request', 'nav_id', 'N');
		$shop_id = $this->gthis->parm_in('session', 'shop_id', 'N');
		

		require_once ROOT.'/include/class/nav.class.php';
		$nav = new nav($this->DB, $this->lang, $this->isAdmin);

		$result_array['nav'] = $nav->getNavDtl($nav_id);


		require_once ROOT.'/include/class/page.class.php';
		$page = new page($this->DB, $this->lang, $this->isAdmin);
		$result_array['pageList'] = $page->getPageList($shop_id);

		require_once ROOT.'/include/class/cate.class.php';
		$cate = new cate($this->DB, $this->lang, $this->isAdmin);
		$result_array['cateList'] = $cate->getCateList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "navList",
			"url" => "admin/page/navList",
			"page_title" => $this->gthis->RMBC->getReturnString('navList')
		],[
			"code" => "navDtl",
			"url" => "admin/page/navDtl?nav_id=" . ($nav_id ? $nav_id : "0"),
			"page_title" => $nav_id? $result_array['nav']['name_tc'] : $this->gthis->RMBC->getReturnString('addNav')
		]);
		return $result_array;
	}

	function nav(){
		if (intval($_SESSION['right']['page']) < 30){
			return array("right"=>"Failure");
		}

		array_push($this->gthis->breadcrumb, [
			"code" => "navList",
			"url" => "admin/page/navList",
			"page_title" => $this->gthis->RMBC->getReturnString('navList')
		]);
		return null;
	}

	function navList(){
		if (intval($_SESSION['right']['page']) < 30){
			return array("right"=>"Failure");
		}
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		require_once ROOT.'/include/class/nav.class.php';
		$nav = new nav($this->DB, $this->lang, $this->isAdmin);

		$result_array['nav'] = $nav->getNavList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "navList",
			"url" => "admin/page/navList",
			"page_title" => $this->gthis->RMBC->getReturnString('navList')
		]);
		return $result_array;
	}

	function style(){
		if (intval($_SESSION['right']['page']) < 30){
			return array("right"=>"Failure");
		}
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
	
		require_once ROOT.'/include/class/shop.class.php';
		$shop = new shop($this->DB, $this->lang, $this->isAdmin);
		
		$result_array['shopDtl'] = $shop->getShopDtl($shop_id);

		
		array_push($this->gthis->breadcrumb, [
			"code" => "style",
			"url" => "admin/page/style",
			"page_title" => $this->gthis->RMBC->getReturnString('style')
		]);
		return $result_array;
	}

	function info(){
		if (intval($_SESSION['right']['shop']) < 30){
			return array("right"=>"Failure");
		}
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
	
		require_once ROOT.'/include/class/shop.class.php';
		$shop = new shop($this->DB, $this->lang, $this->isAdmin);
		
		$result_array['shopDtl'] = $shop->getShopDtl($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "info",
			"url" => "admin/page/info",
			"page_title" => $this->gthis->RMBC->getReturnString('info')
		]);
		return $result_array;
	}
}
?>