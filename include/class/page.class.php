<?php
/* 
    Objective: Page related
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_page
        tbl_page_content
        tbl_popup
        
*/


require_once dirname(__FILE__). '/abstract.class.php';
class page extends baseClass{
    function init() {
        $this->multiLangFieldArr = ['lr_img', 'lr_img2', 'html', 'html2', 'html3', 'mini_banner'];
	}

    function sortPageContent($page_id, $page_content_id_arr){
        // sorting function for the page content
        // $page_content_id_arr: ascending sequence base on the page_content_id_arr 

        $this->DB->sortMap("tbl_page_content", "page_id", $page_id, "content_id", $page_content_id_arr);
        return $this->getPageDtl($page_id);
    }

    function getPopupUri($uri){
        // get the popup box by URI
        // $uri: existing uri

        $sql = "SELECT *, content_" . $this->lang ." AS content
                FROM `tbl_popup` AS p
                LEFT JOIN `tmap_popup_url` AS m ON m.popup_id = p.popup_id AND m.activate='Y'
                WHERE p.`activate` = 'Y'";
        if ($uri) { 
            $sql .= "AND m.`url` LIKE '%$uri'";
        } else {
            $sql .= "AND (m.`url` LIKE '%" . $_SERVER['HTTP_HOST'] . "' OR m.`url` LIKE '%". $_SERVER['HTTP_HOST'] . "/')";
        }

        $res = $this->DB->get_Sql($sql);

        return $res;
    }

    function getPopupList($shop_id, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $sql = "SELECT p.* 
                FROM `tbl_popup` AS p 
                WHERE p.activate = 'Y' && p.shop_id = '$shop_id'";

        if ($filter){
            foreach($filter as $k => $i){
                if ($i || $i == '0'){
                    switch ($k){
                        case 'skey':
                            $sql .= " AND (p.adm_name LIKE '%$i%')";
                            break;
                        
                        default: 
                            break;
                    }                    
                }
            }
        }
        
        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }
        
        $res = $this->DB->get_Sql($sql);

        foreach ($res as $k => $val){
            $sql = "SELECT * FROM `tmap_popup_url` AS m WHERE m.popup_id ='".$val['popup_id']."' AND m.activate ='Y'";
            $res[$k]['urlList'] = $this->DB->get_Sql($sql);
        }


        return $res;
    }

    function getPopupDtl($popup_id){
        // get the popup box Detail
        // $popup_id: PK

        $sql = "SELECT * 
                FROM `tbl_popup` AS p 
                WHERE p.activate = 'Y' && p.popup_id = '$popup_id'";
        $res = $this->DB->get_Sql($sql)[0];

        if ($res){
            $sql = "SELECT * FROM `tmap_popup_url` AS m WHERE m.popup_id ='".$res['popup_id']."' AND m.activate ='Y'";
            $res['urlList'] = $this->DB->get_Sql($sql);
        }

        return $res;
    }

    function editPopup($popup_id, $data){
        // edit popup box
        // $popup_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($popup_id == 0)
            $popup_id = $this->DB->insert_db('tbl_popup', null);
        
        
        $this->DB->updateMap("tmap_popup_url", "popup_id", $popup_id, "url", $data['url_arr']);
        unset($data['url_arr']);

        $sql = $this->DB->buildSql_update("tbl_popup", "popup_id", $popup_id, $data);
        $this->DB->update($sql);


        return $this->getPopupDtl($popup_id);
    }
    
    function editPageContent($content_id, $data){
        // edit page content
        // $content_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($content_id == 0)
            $content_id = $this->DB->insert_db('tbl_page_content', null);

        $slider_arr  = $data['slider_arr'];
        unset($data['slider_arr']);
        $this->DB->updateMap("tmap_content_product", "content_id", $content_id, "product_id", $data['product_id_arr']);
        unset($data['product_id_arr']);

        if ($data['lr_img2_en']){
            $data['lr_img2_en'] = $this->DB->uploadImg($data['lr_img2_en'], "page_content", $content_id);
        }

        if ($data['lr_img2_tc']){
            $data['lr_img2_tc'] = $this->DB->uploadImg($data['lr_img2_tc'], "page_content", $content_id);
        }

        if ($data['lr_img_en']){
            $data['lr_img_en'] = $this->DB->uploadImg($data['lr_img_en'], "page_content", $content_id);
        }

        if ($data['lr_img_tc']){
            $data['lr_img_tc'] = $this->DB->uploadImg($data['lr_img_tc'], "page_content", $content_id);
        }

        if ($data['mini_banner_en']){
            $data['mini_banner_en'] = $this->DB->uploadImg($data['mini_banner_en'], "page_content", $content_id);
        }

        if ($data['mini_banner_tc']){
            $data['mini_banner_tc'] = $this->DB->uploadImg($data['mini_banner_tc'], "page_content", $content_id);
        }

        if(isset($slider_arr)){
            $this->updateSliderArr($slider_arr, $content_id);
        }

        $sql = $this->DB->buildSql_update("tbl_page_content", "content_id", $content_id, $data);
        $this->DB->update($sql);

        return $this->getPageDtl($data['page_id']);
    }

    function delSliderImg($slider_id, $page_id){
        // del slider img
        // $slider_id: PK
        // $page_id: for return use

        $sql = "UPDATE `tmap_content_slider` SET activate = 'N' WHERE slider_id = '$slider_id'";
        $this->DB->update($sql);

        return $this->getPageDtl($page_id);
    }

    function updateSliderArr($slider_arr, $content_id){
        // update slider img 
        // $slider_arr: img slider arr
        // $content_id: FK 

        $sql = "update `tmap_content_slider` set activate ='N' WHERE `content_id` = '$content_id'";
        $this->DB->update($sql);

        foreach($slider_arr as $slider){
            if($slider){
                $data = [
                    // 'slider_id' => $slider['slider_id'],
                    'content_id' => $content_id,
                    'sort' => $slider['sort'],
                    'updated_by_id' => $_SESSION['admin']['admin_id'],
                    'activate' => 'Y'
                ];

                if ($slider['img_tc']){
                    $data['img_tc'] = $this->DB->uploadImg($slider['img_tc'], "page_slider", $content_id);
                }

                if ($slider['img_en']){
                    $data['img_en'] = $this->DB->uploadImg($slider['img_en'], "page_slider", $content_id);
                }

                if ($slider['slider_id'] == 0){
                    // add slider
                    $this->DB->insert_db("tmap_content_slider", $data);
                } else {
                    // edit slider
                    $sql = $this->DB->buildSql_update("tmap_content_slider", "slider_id", $slider['slider_id'], $data);
                    $this->DB->update($sql);
                }
            }
        }
    }

    function editPage($page_id, $data){
        // edit page
        // $page_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($page_id == 0)
            $page_id = $this->DB->insert_db('tbl_page', $data);

        $sql = $this->DB->buildSql_update("tbl_page", "page_id", $page_id, $data);
        $this->DB->update($sql);

        return $this->getPageDtl($page_id);
    }

    function getPageList($shop_id, $mode = "lv"){
        // get the list
        // $shop_id: shop in use

        switch ($mode){
            case 'lv':
                $sql = "SELECT * ";
                $sql .= " FROM tbl_page WHERE activate = 'Y' && shop_id = '$shop_id'";
                $pageList = $this->DB->get_Sql($sql);
        
                $res['removeable_N'] = [];
                $res['removeable_Y'] = [];
        
                foreach($pageList as $page){
                    if ($page['removeable'] == 'Y')
                        array_push($res['removeable_Y'], $page);
                    else 
                        array_push($res['removeable_N'], $page);
                }
                break;
            case 'flat':
                $sql = "SELECT * ";
                $sql .= " FROM tbl_page WHERE activate = 'Y' && shop_id = '$shop_id'";
                $pageList = $this->DB->get_Sql($sql);
                $res = $pageList;
                break;
        }

        return $res;
    }

    function getPageDtlByUri($uri, $shop_id, $this_page_id = null, $show_in_web = null){
        // get the page Detail by URI
        // $uri: browsing uri

        $sql = "SELECT `page_id`
                FROM `tbl_page` AS p
                WHERE p.`activate` = 'Y' AND p.`url` = '" . str_replace("tc/","", str_replace("en/","",$uri)) ."' AND editable = 'Y' AND shop_id = '$shop_id'";
                
        if ($show_in_web){
            $sql .= " AND show_in_web = '$show_in_web'";
        }

        if ($this_page_id){
            $sql .= " AND page_id != $this_page_id";
        }

        $res = $this->DB->get_Sql($sql)[0];
        return $this->getPageDtl($res['page_id']);
    }

    function getPageDtl($page_id){
        // get the page Detail
        // $page_id: PK

        $sql = "SELECT * 
                FROM `tbl_page` AS p 
                WHERE p.activate = 'Y' && p.page_id = '$page_id'";
        $res = $this->DB->get_Sql($sql)[0];

        if ($res){
            $sql = "SELECT * ";
            foreach($this->multiLangFieldArr AS $field){
                $sql .= ", $field" . "_" . $this->lang . " AS $field ";
            }
            $sql .= "FROM `tbl_page_content` AS c
                    WHERE c.activate = 'Y' && c.page_id = '$page_id'
                    ORDER BY sort";
            $res['content'] = $this->DB->get_Sql($sql);
    
            foreach($res['content'] as $k => $i){
                $content_id = $i['content_id'];
                switch($i['type']){
                    case 'slider':
                        $sql = "SELECT *, img_".$this->lang." AS img FROM `tmap_content_slider` WHERE `content_id` = '$content_id' && activate = 'Y' ORDER BY sort";
                        $res['content'][$k]['slider'] = $this->DB->get_Sql($sql);
                        break;
                    case 'product_grp':
                    case 'l_product_r_img':
                    case 'l_img_r_product':
                    case 'product_grp':
                    case 'productSlider':
                        $sql = "SELECT * FROM `tmap_content_product` WHERE `content_id` = '$content_id' && activate = 'Y' ORDER BY sort";
                        $productList = $this->DB->get_Sql($sql);

                        
                        require_once ROOT.'/include/class/product.class.php';
                        $productCls = new product($this->DB, $this->lang, $this->isAdmin);

                        foreach($productList AS $kk => $productDtl){
                            $productList[$kk]['productDtl'] = $productCls->getProductDtl($res['shop_id'], $productDtl['product_id']);
                        }

                        $res['content'][$k]['productList'] = $productList;
                        break;
                }
            }
        }
        
        return $res;
    }

    function getNavList($shop_id){
        // get the list
        // $shop_id: shop in use

        $sql = "SELECT * FROM tbl_nav WHERE activate = 'Y' && shop_id = '$shop_id' ";
        return $this->DB->get_Sql($sql);
    }
}
?>  