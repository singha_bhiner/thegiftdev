<?php
require_once dirname(__FILE__). '/abstract.class.php';
class alert extends baseClass{

    function databaseConstruct(){
        $sql = "
            DROP TABLE IF EXISTS `tbl_alert`;
            CREATE TABLE IF NOT EXISTS `tbl_alert` (
                `alert_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `type` VARCHAR(128) DEFAULT NULL,
                `remarks` VARCHAR(512) DEFAULT NULL,
                `ref_id` LONGTEXT DEFAULT NULL,
                `alert_time` DATETIME DEFAULT CURRENT_TIMESTAMP(),
                `alert_status` VARCHAR(128) DEFAULT '10_reminding', 
                `create_time` DATETIME DEFAULT NULL,
                `create_by` INT(11) DEFAULT NULL,
                `last_update_time` DATETIME DEFAULT NULL,
                `last_update_by` INT(11) DEFAULT NULL,
                `activate` VARCHAR(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y'
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            DROP TABLE IF EXISTS `tbl_alert_admin`;
            CREATE TABLE IF NOT EXISTS `tbl_alert_admin` (
                `map_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `alert_id` INT(11) DEFAULT NULL,
                `admin_id` INT(11) DEFAULT NULL,
                `expire_time` DATETIME DEFAULT NULL,
                `create_time` DATETIME DEFAULT NULL,
                `create_by` INT(11) DEFAULT NULL,
                `last_update_time` DATETIME DEFAULT NULL,
                `last_update_by` INT(11) DEFAULT NULL,
                `activate` VARCHAR(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y'
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ";
        $this->DB->update($sql);
    }

//////////////////////////////////////////// ALERT START ////////////////////////////////////////////


    function getAlertList($filter = null, $sorting = null, $limiter = null, $dispatch = true){
		$fromShort = "alert";
		$selectSql = "SELECT $fromShort.*, DATE_ADD(DATE_FORMAT(alert_time, '%Y-%m-%d %H:00:00'),INTERVAL IF(MINUTE(alert_time) < 30, 0, 1) HOUR) AS rounded_alert_time ";
		$fromSql = "FROM `tbl_alert` AS $fromShort";
        $whereSql = " WHERE $fromShort.`activate` = 'Y'";
		$groupSql = " GROUP BY $fromShort.alert_id";
		if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
			$whereSql = " WHERE 1=1";

		if ($sorting)
			$sortSql = " ORDER BY $sorting";
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
                switch ($k){
                    case 'alert_id':
                        $whereSql .= " AND ($fromShort.`alert_id` = '$i')";
                        break;
                    case 'type':
                        $whereSql .= " AND ($fromShort.`type` = '$i' || $fromShort.`type` = '$i"."_".$filter['shop_id']."')";
                        break;
                    case 'alert_status':
                        $status_code = explode("_", $i)[0];
                        $whereSql .= " AND ($fromShort.`alert_status` LIKE '$status_code%')";
                        break;
                    case 'admin_id':
                        $selectSql .= ", aa.ignore_time ";
                        $fromSql .= " LEFT JOIN `tbl_alert_admin` AS aa ON $fromShort.alert_id = aa.alert_id AND aa.activate = 'Y' AND aa.admin_id = $i ";
                        $whereSql .= " AND (aa.ignore_time IS NULL OR aa.ignore_time <= CURRENT_TIMESTAMP())";
                        break;
                } 
			}
		}
		
		$sql = $selectSql . $fromSql . $whereSql . $groupSql . $sortSql  . $limitSql;
		$res = $this->DB->get_Sql($sql);

        if ($dispatch){
            foreach($res as $k => $i){
                $res[$k] = $this->dispatchAlert($i);
            }
        }

        // group date
        // $rounded_alert_time = array_unique(array_column($row, "rounded_alert_time"));

        // foreach($rounded_alert_time as $k => $i){
        //     $res[$k] = [
        //         "rounded_alert_time" => $i,
        //         "alert_list" => []
        //     ];
        // }


        // foreach($row as $k => $i){
        //     $idx = array_search($i['rounded_alert_time'], array_column($res, "rounded_alert_time"));
        //     array_push($res[$idx]['alert_list'], $i);
        // } 


		return $res;
	}

    function getAlertTimeSession(){
        $sql = "SELECT DATE_ADD(DATE_FORMAT(alert_time, '%Y-%m-%d %H:00:00'),INTERVAL IF(MINUTE(alert_time) < 30, 0, 1) HOUR) AS rounded_alert_time
                FROM `tbl_alert` WHERE `alert_status` LIKE '10%'
                GROUP BY rounded_alert_time
                ORDER BY alert_time DESC";
        return $this->DB->get_Sql($sql);
    }

    function getAlert($filter){
        return $this->getAlertList($filter, null, $this->DB->getUnique)[0];
    }

    function dispatchAlert($alert){
        $alert_id = $alert['alert_id'];

        switch ($alert['type']){
            case 'item_out_of_stock':
                require_once ROOT.'/include/class/item.class.php';
                $itemCls = new item($this->DB, $this->lang, $this->isAdmin);
                $item = $itemCls->getItemDtl($alert['ref_id']);
                $item['product_list'] = $itemCls->getItemProductList($item);
                $alert['item'] = $item;
                break;
            case 'product_out_of_stock':
                require_once ROOT.'/include/class/product.class.php';
                $productCls = new product($this->DB, $this->lang, $this->isAdmin);
                $product = $productCls->getProductDtl(1, $alert['ref_id']);
                $alert['product'] = $product;
                break;
            case 'publish_1':
                require_once ROOT.'/include/class/product.class.php';
                $productCls = new product($this->DB, $this->lang, $this->isAdmin);
                $product = $productCls->getProductDtl(1, $alert['ref_id']);
                $alert['product'] = $product;
                break;
            case 'publish_2':
                require_once ROOT.'/include/class/product.class.php';
                $productCls = new product($this->DB, $this->lang, $this->isAdmin);
                $product = $productCls->getProductDtl(2, $alert['ref_id']);
                $alert['product'] = $product;
                break;
            case 'publish_3':
                require_once ROOT.'/include/class/product.class.php';
                $productCls = new product($this->DB, $this->lang, $this->isAdmin);
                $product = $productCls->getProductDtl(3, $alert['ref_id']);
                $alert['product'] = $product;
                break;
            case 'deposit_slip_1':
                require_once ROOT.'/include/class/order.class.php';
                $orderCls = new order($this->DB, $this->lang, $this->isAdmin);
                $deposit_slip = $orderCls->getDepositSlipList(1, ["slip_id" => $alert['ref_id']])[0];
                $alert['deposit_slip'] = $deposit_slip;
                break;
            case 'deposit_slip_2':
                require_once ROOT.'/include/class/order.class.php';
                $orderCls = new order($this->DB, $this->lang, $this->isAdmin);
                $deposit_slip = $orderCls->getDepositSlipList(2, ["slip_id" => $alert['ref_id']])[0];
                $alert['deposit_slip'] = $deposit_slip;
                break;
            case 'deposit_slip_3':
                require_once ROOT.'/include/class/order.class.php';
                $orderCls = new order($this->DB, $this->lang, $this->isAdmin);
                $deposit_slip = $orderCls->getDepositSlipList(3, ["slip_id" => $alert['ref_id']])[0];
                $alert['deposit_slip'] = $deposit_slip;
                break;
            case 'new_order':
                require_once ROOT.'/include/class/order.class.php';
                $orderCls = new order($this->DB, $this->lang, $this->isAdmin);
                $order = $orderCls->getOrderDtl($alert['ref_id']);
                $alert['order'] = $order;
                break;

        }
        return $alert;
    }

    function editAlert($data){
        if (isset($data['alert_id']) && $data['alert_id'] != 0) {
			$id = $this->DB->update_Sql("tbl_alert", "alert_id", $data);
		} else {
			$id = $this->DB->insert_db("tbl_alert", $data);
		} 

        return $id;
    }

    function refreshAlertStatus($shop_id = 1, $type_list = ["item_out_of_stock", "product_out_of_stock","publish","deposit_slip"]){
        foreach($type_list as $type) {
            $alert_list = $this->getAlertList(["type" => $type, "alert_status" => "10_reminding", "shop_id" => $shop_id]);
            switch ($type){
                case 'item_out_of_stock':
                    require_once ROOT.'/include/class/item.class.php';
                    $itemCls = new item($this->DB, $this->lang, true);

                    $item_out_of_stock_list = $itemCls->getOutOfStockList();

                    foreach ($item_out_of_stock_list as $item){
                        if (($idx = array_search($item['item_id'], array_column($alert_list, "ref_id"))) !== false){
                            // alert already 
                            // remove it from the alert_list, the remaining alert will be mark as solved
                            // unset($alert_list[$idx]);
                            array_splice($alert_list, $idx, 1);
                        } else {
                            $this->DB->insert_db("tbl_alert", [
                                "type" => "item_out_of_stock",
                                "ref_id" => $item['item_id']
                            ]);
                        }
                    }
                    break;
                case 'product_out_of_stock':
                    require_once ROOT.'/include/class/product.class.php';
                    $productCls = new product($this->DB, $this->lang, true);
                    $product_out_of_stock_list = $productCls->getOutOfStockList();

                    foreach ($product_out_of_stock_list as $product){
                        if (($idx = array_search($product['product_id'], array_column($alert_list, "ref_id"))) !== false){
                            // alert already 
                            // remove it from the alert_list, the remaining alert will be mark as solved
                            // unset($alert_list[$idx]);
                            array_splice($alert_list, $idx, 1);
                        } else {
                            $this->DB->insert_db("tbl_alert", [
                                "type" => "product_out_of_stock",
                                "ref_id" => $product['product_id']
                            ]);
                        }
                    }

                    break;
                case 'publish':
                    require_once ROOT.'/include/class/product.class.php';
                    $productCls = new product($this->DB, $this->lang, true);
                    $product_list = $productCls->getProductList($shop_id, ["waitingPublish" => 'Y']);
                    foreach ($product_list as $product){
                        if (($idx = array_search($product['product_id'], array_column($alert_list, "ref_id"))) !== false){
                            // alert already 
                            // remove it from the alert_list, the remaining alert will be mark as solved
                            // unset($alert_list[$idx]);
                            array_splice($alert_list, $idx, 1);
                        } else {
                            $this->DB->insert_db("tbl_alert", [
                                "type" => "publish_$shop_id",
                                "ref_id" => $product['product_id']
                            ]);
                        }
                    }
                    break;
                case 'deposit_slip':
                    require_once ROOT.'/include/class/order.class.php';
                    $orderCls = new order($this->DB, $this->lang, $this->isAdmin);
                    $slipList = $orderCls->getDepositSlipList($shop_id, ["is_confirmed" => 'N']);
                    foreach ($slipList as $slip){
                        if (($idx = array_search($slip['slip_id'], array_column($alert_list, "ref_id"))) !== false){
                            // alert already 
                            // remove it from the alert_list, the remaining alert will be mark as solved
                            // unset($alert_list[$idx]);
                            array_splice($alert_list, $idx, 1);
                        } else {
                            $this->DB->insert_db("tbl_alert", [
                                "type" => "deposit_slip_$shop_id",
                                "ref_id" => $slip['slip_id']
                            ]);
                        }
                    }
                    break;
                case 'new_order':
                    require_once ROOT.'/include/class/order.class.php';
                    $orderCls = new order($this->DB, $this->lang, $this->isAdmin);
                    $orderList = $orderCls->getOrderList($shop_id, ["order_status" => 'paid']);
                    foreach ($orderList as $order){
                        if (($idx = array_search($order['order_id'], array_column($alert_list, "ref_id"))) !== false){
                            // alert already 
                            // remove it from the alert_list, the remaining alert will be mark as solved
                            // unset($alert_list[$idx]);
                            array_splice($alert_list, $idx, 1);
                        } else {
                            $this->DB->insert_db("tbl_alert", [
                                "type" => "new_order_$shop_id",
                                "ref_id" => $order['order_id']
                            ]);
                        }
                    }
                    break;

            }
            
            if ($alert_list){
                $sql = "UPDATE `tbl_alert` SET alert_status = '20_solved' WHERE alert_id IN (" . implode(",", array_column($alert_list, "alert_id")) .")";
                $this->DB->update($sql);
            }
        }
        return true;
    }

    function editIgnore($data){
        if (isset($data['map_id']) && $data['map_id'] != 0) {
			$id = $this->DB->update_Sql("tbl_alert_admin", "map_id", $data);
		} else {
			$id = $this->DB->insert_db("tbl_alert_admin", $data);
		} 

        return $id;
    }

//////////////////////////////////////////// ALERT END   ////////////////////////////////////////////

}
?>