<?php
require_once dirname(__FILE__). '/abstract.class.php';
class driverschedule extends baseClass{

    function databaseConstruct(){
        $sql = "
            DROP TABLE IF EXISTS `tbl_driver_schedule`;
            CREATE TABLE IF NOT EXISTS `tbl_driver_schedule` (
                `driver_schedule_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `driver_admin_id` INT(11) DEFAULT NULL,
                `start` time DEFAULT NULL,
                `end` time DEFAULT NULL,
                `text` LONGTEXT DEFAULT NULL,
                `create_time` DATETIME DEFAULT NULL,
                `create_by` INT(11) DEFAULT NULL,
                `last_update_time` DATETIME DEFAULT NULL,
                `last_update_by` INT(11) DEFAULT NULL,
                `activate` VARCHAR(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y'
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            DROP TABLE IF EXISTS `tbl_car`;
            CREATE TABLE IF NOT EXISTS `tbl_car` (
                `car_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `name` LONGTEXT DEFAULT NULL,
                `create_time` DATETIME DEFAULT NULL,
                `create_by` INT(11) DEFAULT NULL,
                `last_update_time` DATETIME DEFAULT NULL,
                `last_update_by` INT(11) DEFAULT NULL,
                `activate` VARCHAR(1) COLLATE utf8mb4_unicode_ci DEFAULT 'Y'
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ";
        $this->DB->update($sql);
    }

//////////////////////////////////////////// DRIVER SCHEDULE START ////////////////////////////////////////////

    function getDriverList(){
        $sql = "SELECT ad.* 
                FROM `tbl_driver_schedule` AS sc
                LEFT JOIN `tbl_admin` AS ad ON ad.admin_id = sc.driver_admin_id
                WHERE sc.activate = 'Y'
                GROUP BY ad.admin_id ";
        return $this->DB->get_Sql($sql);
    }

    function getDriverScheduleList($filter = null, $sorting = null, $limiter = null, $dispatch = true){
		$fromShort = "driverschedule";
		$selectSql = "SELECT $fromShort.driver_schedule_id, DATE_FORMAT(start, '%H:%m') AS start, DATE_FORMAT(end, '%H:%m') AS end, text ";
		$fromSql = "FROM `tbl_driver_schedule` AS $fromShort";
        $whereSql = " WHERE $fromShort.`activate` = 'Y'";
		$groupSql = " GROUP BY $fromShort.driver_schedule_id";
		if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
			$whereSql = " WHERE 1=1";

		if ($sorting)
			$sortSql = " ORDER BY $sorting";
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
                switch ($k){
                    case 'driver_schedule_id':
                        $whereSql .= " AND ($fromShort.`driver_schedule_id` = '$i')";
                        break;
                    case 'admin_id':
                        $whereSql .= " AND ($fromShort.`driver_admin_id` = '$i')";
                        break;

                } 
			}
		}
		
		$sql = $selectSql . $fromSql . $whereSql . $groupSql . $sortSql  . $limitSql;
		$res = $this->DB->get_Sql($sql);

        if ($dispatch){
            foreach($res as $k => $i){
                $res[$k] = $this->dispatchDriverSchedule($i);
            }
        }

		return $res;
	}

    function getDriverSchedule($driver_schedule_id){
        $filter = [
            "driver_schedule_id" => $driver_schedule_id
        ];

        return $this->getDriverScheduleList($filter, null, $this->DB->getUnique)[0];
    }

    function dispatchDriverSchedule($driverSchedule){
        return $driverSchedule;
    }

    function editDriverSchedule($data){
        if (isset($data['driver_schedule_id']) && $data['driver_schedule_id'] != 0) {
			$id = $this->DB->update_Sql("tbl_driver_schedule", "driver_schedule_id", $data);
		} else {
			$id = $this->DB->insert_db("tbl_driver_schedule", $data);
		} 

        return $id;
    }

//////////////////////////////////////////// DRIVER SCHEDULE END   ////////////////////////////////////////////

//////////////////////////////////////////// CAR START ////////////////////////////////////////////

    function getCarList($filter = null, $sorting = null, $limiter = null, $dispatch = true){
		$fromShort = "car";
		$selectSql = "SELECT * ";
		$fromSql = "FROM `tbl_car` AS $fromShort";
        $whereSql = " WHERE $fromShort.`activate` = 'Y'";
		$groupSql = " GROUP BY $fromShort.car_id";
		if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
			$whereSql = " WHERE 1=1";

		if ($sorting)
			$sortSql = " ORDER BY $sorting";
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
                switch ($k){
                    case 'car_id':
                        $whereSql .= " AND ($fromShort.`car_id` = '$i')";
                        break;
                } 
			}
		}
		
		$sql = $selectSql . $fromSql . $whereSql . $groupSql . $sortSql  . $limitSql;
		$res = $this->DB->get_Sql($sql);

        if ($dispatch){
            foreach($res as $k => $i){
                $res[$k] = $this->dispatchCar($i);
            }
        }

		return $res;
	}

    function getCar($car_id){
        $filter = [
            "car_id" => $car_id
        ];

        return $this->getCarList($filter, null, $this->DB->getUnique)[0];
    }

    function dispatchCar($car){
        return $car;
    }

    function editCar($data){
        if (isset($data['car_id']) && $data['car_id'] != 0) {
			$id = $this->DB->update_Sql("tbl_car", "car_id", $data);
		} else {
			$id = $this->DB->insert_db("tbl_car", $data);
		} 

        return $id;
    }

//////////////////////////////////////////// CAR END   ////////////////////////////////////////////
}
?>