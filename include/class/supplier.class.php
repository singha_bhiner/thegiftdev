<?php
/* 
    Objective: coupon code related 
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_supplier
        tbl_supplier_tag
        tmp_supplier_tag
        tbl_supplier_contact


        
*/
require_once dirname(__FILE__). '/abstract.class.php';
class supplier extends baseClass{

    function editSupplier($data){
        // edit supplier
        // $supplier_id: tbl_coupon PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($data['supplier_id'] == 0){
            $supplier_id = $this->DB->insert_db('tbl_supplier', $data);
        } else {
            $supplier_id = $this->DB->update_Sql("tbl_supplier", "supplier_id", $data);
        }
        
        $this->DB->updateMap("tmap_supplier_tag", "supplier_id", $supplier_id, "tag_id", $data['tag_id_arr']);
        $this->DB->updateForeignMap("tbl_supplier_contact", "supplier_id", $supplier_id, "contact_id", $data['contact_json']);
        
        return $this->getSupplierDtl($supplier_id);
    }

    function getSupplierList($filter = null, $limiter = null, $sorting = null){
        // get the list
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by
        $sql = "SELECT * FROM `tbl_supplier` WHERE `activate` ='Y'";

        if ($filter){
            foreach($filter as $k => $i){
                if ($i || $i == '0'){
                    switch ($k){
                        case 'skey':
                            $sql .= " AND (code LIKE '%$i%' OR adm_name = '%$i%')";
                            break;
                        case 'supplier_id':
                            $sql .= " AND (supplier_id = '$i')";
                            break;
                        default: 
                            break;
                    }                    
                }
            }
        }

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        foreach ($res as $k => $i){
            $res[$k] = $this->dispatchSupplier($i);
        }
        return $res;
    }

    function getSupplierDtl($supplier_id){
        if ($supplier_id)
            return $this->getSupplierList(["supplier_id" => $supplier_id], $this->DB->getUnique)[0];
        else
            return null;
    }

    function dispatchSupplier($supplier){
        $supplier_id = $supplier['supplier_id'];
        $supplier['next_item_id'] = $this->getNextItemId($supplier['supplier_id']);
        $supplier['contact_list'] = $this->getSupplierContactList(["supplier_id" => $supplier['supplier_id']]);
        $supplier['tag_list'] = $this->dispatchTag($this->getSupplierTagList(["supplier_id" => $supplier['supplier_id']]));
        
        // $sql = "SELECT *
        //         FROM `tbl_item` AS i
        //         WHERE i.activate = 'Y' AND supplier_id = '".$supplier['supplier_id']."'";
        $sql = "SELECT i.*, if(vi.vCount IS NULL, 'N', 'Y') AS is_parent
                FROM `tbl_item` AS i
                LEFT JOIN (
                    SELECT i.various_item_id, count(i.various_item_id) as vCount
                    FROM `tbl_item` AS i 
                    WHERE i.activate = 'Y' AND supplier_id  = '$supplier_id' AND various_item_id IS NOT NULL
                    GROUP BY i.various_item_id
                ) AS vi ON vi.various_item_id = i.item_id
                WHERE i.activate = 'Y' AND supplier_id = '$supplier_id' 
            ";
        $supplier['item_list'] = $this->DB->get_Sql($sql);

        return $supplier;
    }

    function getNextItemId($supplier_id){
        $sql = "SELECT item_id, sku
                FROM `tbl_item`
                WHERE various_item_id IS NULL AND supplier_id = '$supplier_id'
                ORDER BY item_id DESC";
        $res = $this->DB->get_Sql($sql)[0];
        $val = $res['sku'];

        try {
            $val = intval(explode("-", "$val")[2]);
        } catch (exception $ex){
            $val = 1;
        }

        $val ++;
        
        return str_pad($val,6, "0", STR_PAD_LEFT);
    }

    function getSupplierContactList($filter = null, $limiter = null, $sorting = null){
        // get the list
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by
        $sql = "SELECT * FROM `tbl_supplier_contact` WHERE `activate` ='Y'";

        if ($filter){
            foreach($filter as $k => $i){
                if ($i || $i == '0'){
                    switch ($k){
                        case 'skey':
                            $sql .= " AND (code LIKE '%$i%' OR adm_name = '%$i%')";
                            break;
                        case 'supplier_id':
                            $sql .= " AND (supplier_id = '$i')";
                            break;
                        default: 
                            break;
                    }                    
                }
            }
        }

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        return $res;
    }

    // function getTagList($filter = null, $limiter = null, $sorting = null){
    //     $fromShort = "mst";
	// 	$selectSql = "SELECT $fromShort.* ";
	// 	$fromSql = " FROM `tmap_supplier_tag` as $fromShort";
	// 	$whereSql = " WHERE $fromShort.`activate` = 'Y'";
	// 	if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
	// 		$whereSql = " WHERE 1=1";

	// 	if ($sorting)
	// 		$sortSql = " ORDER BY ". $sorting['sort'] . " " . $sorting['order'];
		
	// 	if ($limiter)
	// 		$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

	// 	if ($filter){
	// 		foreach ($filter as $k => $i){
	// 			if ($i){
	// 				switch ($k){
	// 					case 'supplier_id':
	// 						$whereSql .= " AND ($fromShort.`supplier_id` = '$i')";
	// 						break;
	// 				}
	// 			}
	// 		}
	// 	}
		
	// 	$sql = $selectSql . $fromSql . $whereSql . $sortSql . $limitSql;
	// 	$res = $this->DB->get_Sql($sql);

	// 	return $res;
    // }

    function dispatchTag($tagList){
        // print_r($tagList);
        $res = [];
        // generate the parent_level
        foreach ($tagList as $k => $tag){
            if (!$tag['parent_tag_id']){
                // print_r($tag);
                array_push($res, $tag);
            }
        }

        // generate the child_level
        foreach ($tagList as $k => $tag){
            if ($tag['parent_tag_id']){
                $parentIdx = array_keys(array_filter($res, fn($par) => $par['tag_id'] == $tag['parent_tag_id']))[0];
                
                if ($parentIdx === null){
                    $supplierTag = $this->getSupplierTagDtl($tag['parent_tag_id']);
                    if ($supplierTag){
                        array_push($res, $supplierTag);
                        $parentIdx = array_keys(array_filter($res, fn($par) => $par['tag_id'] == $tag['parent_tag_id']))[0];
                    }
                }

                if ($parentIdx || $parentIdx == '0'){
                    if (!isset($res[$parentIdx]['child_list'])){
                        $res[$parentIdx]['child_list'] = [];
                    }
    
                    array_push($res[$parentIdx]['child_list'], $tag);
                }
            }
        }

        return $res;
    }

    function editSupplierTag($data){
        if ($data['tag_id'] == 0){
            $id = $this->DB->insert_db('tbl_supplier_tag', $data);
        } else {
            $id = $this->DB->update_Sql("tbl_supplier_tag", "tag_id", $data);
        }
        
        return $this->getSupplierTagDtl($id);
    }

    function sortSupplierTag($tag_id_arr){
        $this->DB->sort("tbl_supplier_tag", "tag_id", $tag_id_arr);
        return null;
    }

    function getSupplierTagList($filter = null, $limiter = null, $sorting = null){
        $fromShort = "st";
		$selectSql = "SELECT $fromShort.* ";
		$fromSql = " FROM `tbl_supplier_tag` as $fromShort";
		$whereSql = " WHERE $fromShort.`activate` = 'Y'";
        $groupSql = " GROUP BY $fromShort.`tag_id`";
		if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
			$whereSql = " WHERE 1=1";

		if ($sorting)
			$sortSql = " ORDER BY ". $sorting['sort'] . " " . $sorting['order'];
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
				if ($i){
					switch ($k){
                        case 'supplier_id':
                            $fromSql .= " LEFT JOIN `tmap_supplier_tag` AS m ON m.tag_id = st.tag_id AND m.activate ='Y'";
                            $whereSql .= " AND (m.supplier_id = '$i')";
                            break;
                        case 'tag_id':
                            $whereSql .= " AND ($fromShort.tag_id = '$i')";
                            break;
					}
				}
			}
		}
		
		$sql = $selectSql . $fromSql . $whereSql . $groupSql. $sortSql  . $limitSql;
		$res = $this->DB->get_Sql($sql);

		return $this->dispatchTag($res);
    }

    function getSupplierTagDtl($tag_id){
        return $this->getSupplierTagList(["tag_id" => $tag_id], $this->DB->getUnique)[0];
    }
}
?>