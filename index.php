<?php
require_once './include/baseModel.class.php';
class index extends baseModel
{
	public $webpSupport;

	public function __construct()
	{
		parent::__construct('');
		$this->https_handling('Y'); // force https
		$this->headerCheck(); // handle request header
		$this->mobileCheck(); // check mobile setting
		$this->memberCheck(); // member check
		$this->webpSupport(); // check is support webp
		$this->headerMenuBuild(); // special handle for header menu
		$this->siteConfig(); // site special config 

		require_once 'include/langMap/returnMessage.php'; // load language file
		$this->RM = new returnMessage($this->lang);
		$this->RMBC = new returnMessage($this->lang,ROOT.'/include/langMap/returnCodeBreadcrumb.csv');

		require_once 'include/pageNavigator.php';	// load page navigator for breadcrumb
		$this->PN = new pageNavigator($this->lang,$this->RM);
		// reset password required
		$this->resetPasswordHandling();

		// create site defaut object
		$this->navObject = $this->navObjectDefault();

		// load Tailwind class
		$this->loadTailwindClass();		// load defined tailwind element class
		
		$this->init();
	}

	/* ***************************************************************************************** */
	/*	Author : David Chung
	/*	Date : 19 Jun 2021
	/*  LUD  : 19 Jun 2021
	 *  Desc : Default setup for page, description and keywords */
	/** 	   
	/* ***************************************************************************************** */

	function navObjectDefault(){
		$navObject['url'] = MASTER_HOME_FULL.$_SERVER['REQUEST_URI'];
		$navObject['page_title'] = $this->RM->getReturnString('page_title');
		$navObject['page_description'] = $this->RM->getReturnString('page_description');
		$navObject['keywords'] =  $this->RM->getReturnString('keywords');
		return $navObject;
	}

	public function init()
	{
		// mode multi-lang mapper
		$this->mode = $this->modeMapper($this->mode);
		$this->submode = $this->parm_in('request','submode','N');
		$this->submode = $this->subModeMapper($this->submode);
		$this->thirdmode = $this->parm_in('request','thirdmode','N');
		$this->thirdmode = $this->thirdModeMapper($this->thirdmode);
		// echo "mode: " . $this->mode ."<br/>";
		// echo "submode: " . $this->submode ."<br/>";
		// echo "thirdmode: " . $this->thirdmode ."<br/>";
		// echo "language_set: " . $this->lang;
		// echo "<hr/>";
		
		$this->assign("shop_id", $this->shop_id);
		$this->assign('mode', $this->mode);
		$this->assign('submode', $this->submode);
		$this->assign('thirdmode', $this->thirdmode);

		$this->breadcrumb = [
			["code"=>"code", "url"=>"home", "page_title"=> $this->RMBC->getReturnString('home')], 
		];

		$this->findPopupPage();
		
		
		if (!$this->findPage($_SERVER['REQUEST_URI'])){
			if ($this->submode == "api"){
				$extraFlag = $this->try_for_api_file($this->mode);
			} else {
				// echo "<script>console.log('admin.php ln:~94 || mode: " . $this->mode ."', ' || submode: " . $this->submode ."',' || thirdmode: " . $this->thirdmode ."');</script>";
				$extraFlag = $this->try_for_php_file($this->mode);
			}
			
			if($extraFlag){
				$this->page = "$this->mode/$this->mode";
				// 使用附加php
			} else {
				switch ($this->mode) {
					case 'contact-us':
						$this->page = 'contact-us';
						break;
					default:
						$this->assign('header_refresh', 'Y');
						$this->assign('style', $_REQUEST['style']);
						$this->page = 'template_modules/index';
						
				}
			}
		} 

		if($this->thirdmode){
			@$navObject = ($this->PN->nav[$this->mode][$this->submode][$this->thirdmode] ? $this->PN->nav[$this->mode][$this->submode][$this->thirdmode] : $this->PN->nav[$this->mode]);
		} elseif($this->submode && empty($this->thirdmode)){
			@$navObject = ($this->PN->nav[$this->mode][$this->submode] ? $this->PN->nav[$this->mode][$this->submode] : $this->PN->nav[$this->mode]);
		} else {
			@$navObject = ($this->mode ? $this->PN->nav[$this->mode] : $this->PN->nav['/']);
		}
		@$navObject['nav_array'] = $this->PN->createNavMenuArray($navObject['router']);
		$this->navObject['breadcrumb'] = $navObject['nav_array'];
		if(isset($navObject['page_title'])){
			$this->navObject['page_title'] = $navObject['page_title'];
		}

		if(isset($navObjectOverride)){
			if(isset($navObjectOverride['page_title'])){
				$this->navObject['page_title'] = $navObjectOverride['page_title'];
				$this->navObject['page_description'] = $navObjectOverride['page_description'];
				$this->navObject['keywords'] = $navObjectOverride['keywords'];
			}
		}

		if(isset($this->navObjectBreadcrumbArr)){
			foreach ($this->navObjectBreadcrumbArr as $navObjectBreadcrumb) {
				array_push($this->navObject['breadcrumb'],$navObjectBreadcrumb);
			}
		}

		$this->assign('cmode',$this->mode);
		$this->assign('submode',$this->submode);
		$this->assign('thirdmode',$this->thirdmode);
	}

	function findPopupPage(){
		$uri = $_SERVER['REQUEST_URI'];
		$uri = ltrim($uri, '/');
		$uri = str_replace("?error=Y", "", $uri);
		$uri = $this->escape_string($uri);
		require_once ROOT.'/include/class/page.class.php';
		$page = new page($this->DB, $this->lang);

		$popupList = $page->getPopupUri($uri);
		
		if ($popupList)
			$this->assign("popupList", $popupList);
	}

	function findPage($uri){
		$uri = ltrim($uri, '/');
		$uri = str_replace("?error=Y", "", $uri);
		$uri = $this->escape_string($uri);
		require_once ROOT.'/include/class/page.class.php';
		$page = new page($this->DB, $this->lang);

		if ($uri == '')
			$this->assign("sideMenu", 'Y');

		$pageDtl = $page->getPageDtlByUri($uri, $this->shop_id, null, 'Y');
		
		if ($pageDtl) {
			$result_array['pageDtl'] = $pageDtl;
			$this->page = "/page";
			$this->assign("result_array", $result_array);
			return true;
		} else {
			return false;
		}
	}

	function modeMapper($mode){
		$mapper['login'] = array('登入');

		
		foreach($mapper as $k=>$i){
			if (in_array($mode, $i)) {
			    return $k;
			}
		}

		return $mode;
	}

	function subModeMapper($submode){
		$mapper['personalInfo'] = array('個人資料');

		foreach($mapper as $k=>$i){
			if (in_array($submode, $i)) {
			    return $k;
			}
		}
		return $submode;
	}

	function thirdModeMapper($submode){
		$mapper['addAddress'] = array('增加地址');
		
		foreach($mapper as $k=>$i){
			if (in_array($submode, $i)) {
			    return $k;
			}
		}
		return $submode;
	}

	public function display(){
		$page_setup = file_get_contents(dirname(__FILE__).'/templates/'. $this->shopDtl['prefix'] ."/" .$this->page . '.html');
		preg_match_all('%<script type="blt/page_setup_json">(.*?)</script>%s', $page_setup, $page_setup_data); // 找出page setup 的數據		
		if(is_array($page_setup_data)){
			// 有找到 blt jsons
			if(sizeOf($page_setup_data[1])>0){
				// data found
				preg_match_all('%\[(.*?)\]%s', $page_setup_data[1][0], $page_setup_data_fin); // 找出page setup 的數據
				$page_setup_data_fin = json_decode($page_setup_data_fin[0][0],1);
			}	
		}
		if(isset($page_setup_data_fin)){
			if(is_array($page_setup_data_fin)){
				if(sizeOf($page_setup_data_fin)>0){
					if(isset($this->tpl->tpl_vars['navObject'])){
						// change original nav object
						foreach($page_setup_data_fin[0] as $k=>$i){
							if(isset($i)){
								if(trim($i)!='' || is_array($i)){
									$this->navObject[$k] = $i;		
								}
							}
						}
					} else {
						//print_r($page_setup_data_fin[0]);
						//die();
						foreach($page_setup_data_fin[0] as $k=>$i){
							if(isset($i)){
								if(is_array($i)){
									if(isset($i['en'])){
										if($k=='canonical'){
											$this->navObject[$k] = $i;	
										} else {
											$this->navObject[$k] = $i[$this->lang];			
										}
									}
								} else {
									if(trim(''.$i)!=''){
										$this->navObject[$k] = $i;		
									}
								}
							}
						}
					}
				}
			}
		}
		
		if(isset($this->navObject['lang'])){
			$nlang = array();
			foreach($this->navObject['lang'] as $k=>$i){
				$nlang[$k] = $i[$this->lang];
			}
			$this->assign('nlang',$nlang);
			unset($this->navObject['lang']);
		}

		$this->assign('navObject', $this->navObject);
		//lok testing case for FAQ and blog to handle meta tag
		if(isset($this->navObjectOverride)){
			$this->assign('navObject', $this->navObjectOverride);
		}
		$this->assign('RM', $this->array_merge_keep_key($this->RM->csv,$this->RM->csv_string));
		$this->assign('RMS', $this->RM->csv_string);
		$this->assign('RMJS', json_encode($this->RM->csv_js_string));
		$this->assign('mode', $this->mode);
		$this->assign('pageStatus', $this->page);
		$this->assign('page', $this->page . '.html');
		if ($this->pwapage == 'Y') {
			// BLT SPA mode return code setting
			if(isset($this->mode)){
				if(trim($this->mode)==''){
					echo  '1||xx||success||xx||';  // non home transparent header
				} else {
					echo  '0||xx||success||xx||';	// non home yellow header
				}
			} else {
				echo  '1||xx||success||xx||';  // non home transparent header
			}
			$this->displayPage('index.html');
		} else {
			$this->displayPage('index.html');
		}
	}

	function try_for_api_file($mode){
		$php_file = 'php/'.$mode.'/'.$mode.'_api.php';
		// echo $php_file;die();
		if(file_exists($php_file)){
			require_once $php_file;
			$this->extraPHP = new extraPHP($this,$this->DB);
			return true;
		} 
		return false;
	}

	function try_for_php_file($mode){
		$php_file = 'php/'.$mode.'/'.$mode.'.php';
		if(file_exists($php_file)){
			// echo $php_file;die();
			require_once $php_file;
			$this->extraPHP = new extraPHP($this,$this->DB);
			$this->localStringValueBuild($mode); // build local string file;
			$this->page = "/$mode/$mode";
			
			return true;
		} 
		return false;
	}

	function localStringValueBuild($groupClass,$path=null){
		// load local csv file
		if(isset($path)){
			if(file_exists($path)){
				$localRM = new returnMessageAdmin($this->lang,$path);	
			}
		} else {
			if(file_exists(ROOT.'/adminA/templates/'.$groupClass.'/'.$groupClass.'.csv')){
				$localRM = new returnMessageAdmin($this->lang,ROOT.'/adminA/templates/'.$groupClass.'/'.$groupClass.'.csv');
			}
		}

		if(isset($localRM)){
			foreach($this->RM as $k=>$i){
				if(is_array($this->RM->$k)){
					$this->RM->$k = array_replace($this->RM->$k, $localRM->$k);	
				}
			}
		}	
	}

	function home(){
		
	}

	function headerMenuBuild(){
		if ($this->pwapage == 'Y' && $this->mode != 'header_refresh') {
			// no need get data for header
		} else {
			require_once ROOT.'/include/class/nav.class.php';
			$nav = new nav($this->DB, $this->lang);
			require_once ROOT.'/include/class/cate.class.php';
			$cateCls = new cate($this->DB, $this->lang);

			$res = $nav->getNavList($this->shop_id);
			foreach($res as $k => $nav){
				if ($nav['cateList']){
					foreach ($nav['cateList'] as $kk => $cate){
						$res[$k]['cateList'][$kk] = $cateCls->getCateDtl($cate['cate_id']);
					}
				}
			}
			
			$this->assign('header_result_array',$res);
			
			require_once ROOT.'/include/class/client.class.php';
			$clientCls = new client($this->DB, $this->lang);

			$this->assign('client_array', $clientCls->getClientList(['shop_id' => $this->shop_id]));
		}
	}

	function siteConfig(){
		$this->assign('logo_path',LOGO);
	}

	function resetPasswordHandling(){
		if (isset($_SESSION['reset_password_required'])) {
			if ($_SESSION['reset_password_required'] == 'Y') {
				if($this->mode=='header_refresh'){
					// do nothing , allowed header refresh pass
					// echo 'cccc';
				} else {
					if ($this->pwapage == 'Y') {
						$this->mode = 'forceChangePassword';
					} else {
						if ($this->mode != 'forceChangePassword') {
							header("Location:/forceChangePassword");
							exit;
						}	
					}
				}
			}
		}
	}
}
$HOME = new index;
$HOME->display();
