<?php
/* 
    Objective: category related
    Author: Sing
    Last major update: 202108003

    Related file:
        include/class/blog.class.php - DAO
        admin/php/blog.php - field esacpe, routing
        php/blog.php - field esacpe, routing
        admin/templates/blog/* - UI 
        templates/blog/* - UI

	Table scheme: 
        CREATE TABLE `tbl_cate` (
            `cate_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
            `shop_id` int(11) NOT NULL, -- gift || ltp || zuri, in pattern: gift,ltp
            `parent_cate_id` int(11) DEFAULT NULL,
            `name_tc` varchar(255) DEFAULT NULL,
            `name_en` varchar(255) DEFAULT NULL,
            `order` int(11) DEFAULT NULL,
            `activate` varchar(1) DEFAULT 'Y',
            `updated_by_id` int(11) DEFAULT NULL,
            `last_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

*/

class category{
    public function __construct($DB, $lang) {
		$this->DB = $DB;
        $this->lang = $lang;
	}

    function update($cate_id, $data){
        $sql = "UPDATE `tbl_shop` SET ";

        $updateFieldArr = [];
        foreach($data as $key => $val){
            array_push($updateFieldArr, "`$key` = '$val' ");
        }
        $sql .= implode(',', $updateFieldArr);

        $sql .= "WHERE `tbl_cate` = '$cate_id'";
        $this->DB->update($sql);

        // return $this->getShopDtl($cate_id);
    }

    function getCate($shop_id){
        // get 1st lv
        $sql = "SELECT * FROM tbl_cate WHERE activate = 'Y' && parent_cate_id IS NULL";
        $cate_list = $this->DB->get_Sql($sql);
        
        for ($i = 0 ; $i < sizeof($cate_list) ; $i++){
            $lv_1st = $cate_list[$i];
            $sql = "SELECT * FROM tbl_cate WHERE activate = 'Y' && parent_cate_id ='". $lv_1st['cate_id'] ."'";
            $cate_list[$i]['subcate'] = $this->DB->get_Sql($sql);
        }



        return $cate_list;
    }

}
?>