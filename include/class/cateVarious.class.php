<?php
/* 
    Objective: item and SKU and item group related 
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
       tbl_item_cate_various_type
       tbl_item_cate_various_val

        
*/


require_once dirname(__FILE__). '/abstract.class.php';
class cateVarious extends baseClass{

    function getCateVariousTypeList($filter = null, $limiter = null, $sorting = null){
        $fromShort = "icvt";
		$selectSql = "SELECT $fromShort.* ";
		$fromSql = " FROM `tbl_item_cate_various_type` as $fromShort";
		$whereSql = " WHERE $fromShort.`activate` = 'Y'";
        $groupSql = " GROUP BY $fromShort.`type_id`";
		if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
			$whereSql = " WHERE 1=1";

		if ($sorting)
			$sortSql = " ORDER BY ". $sorting['sort'] . " " . $sorting['order'];
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
				if ($i){
					switch ($k){
                        case 'cate_id':
                            $whereSql .= " AND ($fromShort.`cate_id` = '$i')";
                            break;
                        case 'type_id':
                            $whereSql .= " AND ($fromShort.`type_id` = '$i')";
                            break;
					}
				}
			}
		}
		
		$sql = $selectSql . $fromSql . $whereSql . $sortSql . $groupSql . $limitSql;
		$res = $this->DB->get_Sql($sql);

        foreach ($res as $k => $i){
            $res[$k] = $this->dispatchVarious($i);
        }

		return $res;
    }

    function getCateVariousValList($filter = null, $limiter = null, $sorting = null){
        $fromShort = "icvv";
		$selectSql = "SELECT $fromShort.* ";
		$fromSql = " FROM `tbl_item_cate_various_val` as $fromShort";
		$whereSql = " WHERE $fromShort.`activate` = 'Y'";
        $groupSql = " GROUP BY $fromShort.`val_id`";
		if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
			$whereSql = " WHERE 1=1";
        if ($filter && $filter['isHide'] == 'all'){
            
        } else {
            $whereSql .= " AND ($fromShort.`isHide` = 'N')";
        }

		if ($sorting)
			$sortSql = " ORDER BY ". $sorting['sort'] . " " . $sorting['order'];
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
				if ($i){
					switch ($k){
                        case 'cate_id':
                            $whereSql .= " AND ($fromShort.`cate_id` = '$i')";
                            break;
                        case 'type_id':
                            $whereSql .= " AND ($fromShort.`type_id` = '$i')";
                            break;
                        case 'val_id':
                            $whereSql .= " AND ($fromShort.`val_id` = '$i')";
                            break;
					}
				}
			}
		}
		
		$sql = $selectSql . $fromSql . $whereSql . $sortSql . $groupSql . $limitSql;
		$res = $this->DB->get_Sql($sql);

		return $res;
    }
 
    function dispatchVarious($variousType){
        $variousType['val_list'] = $this->getCateVariousValList(["type_id" => $variousType['type_id']]);
        return $variousType;
    }

    function editCateVariousType($data){
        if ($data['type_id'] == 0){
            $id = $this->DB->insert_db('tbl_item_cate_various_type', $data);
        } else {
            $id = $this->DB->update_Sql("tbl_item_cate_various_type", "type_id", $data);
        }
    
        return $this->getCateVariousTypeList(["type_id" => $id]);
    }

    function editCateVariousVal($data){
        $pk = 'val_id';
        if ($data[$pk] == 0){
            $id = $this->DB->insert_db('tbl_item_cate_various_val', $data);
        } else {
            $id = $this->DB->update_Sql("tbl_item_cate_various_val", $pk, $data);
        }
    
        return $this->getCateVariousTypeList(["type_id" => $data['type_id']]);
    }

    function renderCodeMap($codeMap, $bdArr = []){
        // recursive function
        // render the code mapping
        // $codeMap: source codeMap
        // $bdArr: bought down array
        // src codeMap sample: [{"type_id":3,"val_list":[5,6]},{"type_id":4,"val_list":[4]},{"type_id":5,"val_list":[1,2]}]

        $valArr = [];


        if (sizeof($bdArr) == 0){ // check it is the first level
            foreach($codeMap[0]['val_list'] as $k => $val){ // create a 2lv array to store the val
                $valArr[$k]  = [$val];
            }

        } else {
            $key = 0;
            for ($i = 0 ; $i < sizeof($bdArr) ; $i++){
                foreach ($codeMap[0]['val_list'] as $kk => $val){
                    // multiple the bdArr and combine the val;
                    $valArr[$key] = $bdArr[$i];
                    array_push($valArr[$key], $val);
                    $key++;
                    
                }
            }
        }

        if (sizeof($codeMap) > 1){ // if there is more level
            array_shift($codeMap); // shift the codeMap
            return $this->renderCodeMap($codeMap, $valArr); // recursive run the func
        } else {
            return $valArr;
        }   
    }

    function getVariousValDtl($val_id){
        $sql = "SELECT *
                FROM `tbl_item_cate_various_val`
                WHERE val_id = '$val_id' LIMIT 1";
        return $this->DB->get_Sql($sql)[0];
    }







    // TPL version

    function editCateVariousTpl($data){
        if ($data['type_id'] == 0){
            $id = $this->DB->insert_db('tpl_item_cate_various_type', $data);
        } else {
            $id = $this->DB->update_Sql("tpl_item_cate_various_type", "type_id", $data);
        }
    
        $this->DB->updateForeignMap("tpl_item_cate_various_val", "type_id", $id, "val_id", $data['val_json']);
        
        return $this->getCateVariousTypeTplList();
    }

    function getCateVariousTypeTplList($filter = null, $limiter = null, $sorting = null){
        $fromShort = "icvt";
		$selectSql = "SELECT $fromShort.* ";
		$fromSql = " FROM `tpl_item_cate_various_type` as $fromShort";
		$whereSql = " WHERE $fromShort.`activate` = 'Y'";
        $groupSql = " GROUP BY $fromShort.`type_id`";
		if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
			$whereSql = " WHERE 1=1";

		if ($sorting)
			$sortSql = " ORDER BY ". $sorting['sort'] . " " . $sorting['order'];
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
				if ($i){
					switch ($k){
                        case 'cate_id':
                            $whereSql .= " AND ($fromShort.`cate_id` = '$i')";
                            break;
                        case 'type_id':
                            $whereSql .= " AND ($fromShort.`type_id` = '$i')";
                            break;
					}
				}
			}
		}
		
		$sql = $selectSql . $fromSql . $whereSql . $sortSql . $groupSql . $limitSql;
		$res = $this->DB->get_Sql($sql);

        foreach ($res as $k => $i){
            $res[$k] = $this->dispatchVariousTpl($i);
        }

		return $res;
    }

    function getCateVariousValTplList($filter = null, $limiter = null, $sorting = null){
        $fromShort = "icvv";
		$selectSql = "SELECT $fromShort.* ";
		$fromSql = " FROM `tpl_item_cate_various_val` as $fromShort";
		$whereSql = " WHERE $fromShort.`activate` = 'Y'";
        $groupSql = " GROUP BY $fromShort.`val_id`";
		if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
			$whereSql = " WHERE 1=1";
        if ($filter && $filter['isHide'] == 'all'){
            
        } else {
            $whereSql .= " AND ($fromShort.`isHide` = 'N')";
        }

		if ($sorting)
			$sortSql = " ORDER BY ". $sorting['sort'] . " " . $sorting['order'];
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
				if ($i){
					switch ($k){
                        case 'cate_id':
                            $whereSql .= " AND ($fromShort.`cate_id` = '$i')";
                            break;
                        case 'type_id':
                            $whereSql .= " AND ($fromShort.`type_id` = '$i')";
                            break;
                        case 'val_id':
                            $whereSql .= " AND ($fromShort.`val_id` = '$i')";
                            break;
					}
				}
			}
		}
		
		$sql = $selectSql . $fromSql . $whereSql . $sortSql . $groupSql . $limitSql;
		$res = $this->DB->get_Sql($sql);

		return $res;
    }
 
    function dispatchVariousTpl($variousType){
        $variousType['val_list'] = $this->getCateVariousValTplList(["type_id" => $variousType['type_id']]);
        return $variousType;
    }

    function editCateVariousTypeTpl($data){
        if ($data['type_id'] == 0){
            $id = $this->DB->insert_db('tpl_item_cate_various_type', $data);
        } else {
            $id = $this->DB->update_Sql("tpl_item_cate_various_type", "type_id", $data);
        }
    
        return $this->getCateVariousTypeTplList(["type_id" => $id]);
    }

    function editCateVariousValTpl($data){
        $pk = 'val_id';
        if ($data[$pk] == 0){
            $id = $this->DB->insert_db('tpl_item_cate_various_val', $data);
        } else {
            $id = $this->DB->update_Sql("tpl_item_cate_various_val", $pk, $data);
        }
    
        return $this->getCateVariousTypeTplList(["type_id" => $data['type_id']]);
    }

}
?>