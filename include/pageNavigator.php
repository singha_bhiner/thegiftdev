<?php
class pageNavigator {
	public $lang;
	public $nav;
	public $RM;
	
	public function __construct($lang,$RM) {
		$this->lang = $lang;
		$this->RM = $RM;
		$this->nav = $this->navigatorConstruct();
	}
	

	function navigatorConstruct(){
		// 生成頁面導航
		$groupkey = 'about';
		$navigator[$groupkey]['code'] = $groupkey;
		$navigator['about'][$groupkey]['url'] = "/$groupkey";
		$navigator['about'][$groupkey]['router'] = "/$groupkey";
		$navigator['about'][$groupkey]['page_title'] = $this->RM->getReturnString('aboutUs');

		$submodekey = 'privacy';
		$navigator[$groupkey][$submodekey]['code'] = $submodekey;
		$navigator[$groupkey][$submodekey]['url'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['router'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['page_title'] = $this->RM->getReturnString('privacyPolicy');

		$submodekey = 'tos';
		$navigator[$groupkey][$submodekey]['code'] = $submodekey;
		$navigator[$groupkey][$submodekey]['url'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['router'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['page_title'] = $this->RM->getReturnString('tos');;			


		/* ============================================================================================ */

		$groupkey = 'myAccount';
		$navigator[$groupkey]['code'] = $groupkey;
		$navigator[$groupkey]['url'] = "/myAccount";
		$navigator[$groupkey]['router'] = "/myAccount";
		$navigator[$groupkey]['page_title'] = $this->RM->getReturnString('myAccount');;	

		$submodekey = 'personalInfo';
		$navigator[$groupkey][$submodekey]['code'] = $submodekey;
		$navigator[$groupkey][$submodekey]['url'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['router'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['page_title'] = $this->RM->getReturnString('tos');			

		$submodekey = 'changeAddress';
		$navigator[$groupkey][$submodekey]['code'] = $submodekey;
		$navigator[$groupkey][$submodekey]['url'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['router'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['page_title'] = $this->RM->getReturnString('changeAddress');			

		$submodekey = 'supportCenter';
		$navigator[$groupkey][$submodekey]['code'] = $submodekey;
		$navigator[$groupkey][$submodekey]['url'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['router'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['page_title'] = $this->RM->getReturnString('supportCenter');;	

		$submodekey = 'supportTicketDtl';
		$navigator[$groupkey][$submodekey]['code'] = $submodekey;
		$navigator[$groupkey][$submodekey]['url'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['router'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['page_title'] = $this->RM->getReturnString('supportTicketDtl');			

		$thirdmodekey = 'editAddress';
		$navigator[$groupkey][$submodekey][$thirdmodekey]['code'] = $thirdmodekey;
		$navigator[$groupkey][$submodekey][$thirdmodekey]['url'] = "/$groupkey/$submodekey/$thirdmodekey";
		$navigator[$groupkey][$submodekey][$thirdmodekey]['router'] = "/$groupkey/$submodekey/$thirdmodekey";
		$navigator[$groupkey][$submodekey][$thirdmodekey]['page_title'] = $this->RM->getReturnString('editAddress');			

		$thirdmodekey = 'addAddress';
		$navigator[$groupkey][$submodekey][$thirdmodekey]['code'] = $thirdmodekey;
		$navigator[$groupkey][$submodekey][$thirdmodekey]['url'] = "/$groupkey/$submodekey/$thirdmodekey";
		$navigator[$groupkey][$submodekey][$thirdmodekey]['router'] = "/$groupkey/$submodekey/$thirdmodekey";
		$navigator[$groupkey][$submodekey][$thirdmodekey]['page_title'] = $this->RM->getReturnString('addAddress');			

		$submodekey = 'accountSecurity';
		$navigator[$groupkey][$submodekey]['code'] = $submodekey;
		$navigator[$groupkey][$submodekey]['url'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['router'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['page_title'] = $this->RM->getReturnString('accountSecurity');			

		$submodekey = 'accountReSetPassword';
		$navigator[$groupkey][$submodekey]['code'] = $submodekey;
		$navigator[$groupkey][$submodekey]['url'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['router'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['page_title'] = $this->RM->getReturnString('accountReSetPassword');			

		$submodekey = 'accountCoupon';
		$navigator[$groupkey][$submodekey]['code'] = $submodekey;
		$navigator[$groupkey][$submodekey]['url'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['router'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['page_title'] = $this->RM->getReturnString('accountCoupon');			

		$submodekey = 'accountShare';
		$navigator[$groupkey][$submodekey]['code'] = $submodekey;
		$navigator[$groupkey][$submodekey]['url'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['router'] = "/$groupkey/$submodekey";
		$navigator[$groupkey][$submodekey]['page_title'] = $this->RM->getReturnString('accountShare');			

		/* ============================================================================================ */

		$groupkey = 'contactus_online';
		$navigator[$groupkey]['code'] = $groupkey;
		$navigator[$groupkey]['url'] = "/$groupkey";
		$navigator[$groupkey]['router'] = "/$groupkey";
		$navigator[$groupkey]['page_title'] = $this->RM->getReturnString('contactus_online');


		/* ============================================================================================ */

		$groupkey = 'shippingCalculator';
		$navigator[$groupkey]['code'] = $groupkey;
		$navigator[$groupkey]['url'] = "/$groupkey";
		$navigator[$groupkey]['router'] = "/$groupkey";
		$navigator[$groupkey]['page_title'] = $this->RM->getReturnString('shippingCalculator');


		/* ============================================================================================ */


		$groupkey = 'helpCenter';
		$navigator[$groupkey]['code'] = $groupkey;
		$navigator[$groupkey]['url'] = "/helpCenter";
		$navigator[$groupkey]['router'] = "/helpCenter";
		$navigator[$groupkey]['page_title'] = $this->RM->getReturnString('helpCenter');


		/* ============================================================================================ */


		$groupkey = 'taxCalculator';
		$navigator[$groupkey]['code'] = $groupkey;
		$navigator[$groupkey]['url'] = "/$groupkey";
		$navigator[$groupkey]['router'] = "/$groupkey";
		$navigator[$groupkey]['page_title'] = $this->RM->getReturnString('taxCalculator');


		/* ============================================================================================ */


		$groupkey = 'taxInfo';
		$navigator[$groupkey]['code'] = $groupkey;
		$navigator[$groupkey]['url'] = "/". $this->RM->getReturnString('taxInfo');
		$navigator[$groupkey]['router'] = "/$groupkey";
		$navigator[$groupkey]['page_title'] = $this->RM->getReturnString('taxInfo');


		return $navigator;
	}

	function navigatorConstructDynamic($page,$dynamicData,$pageTitle){
		if($page=='property'){
			$navigator[$page]['code'] = "$page";
			$navigator[$page]['url'] = "/$page/$dynamicData";
			$navigator[$page]['router'] = "/propertySearch/$page/$dynamicData";
			$navigator[$page]['page_title'] = $pageTitle;	
		}
		return $navigator;
	}

	function createNavMenuArray($url,$navObject=null){
		if(!$navObject){
			$navObject = $this->navigatorConstruct();	
		} else{
			$navObject = array_merge($navObject,$this->navigatorConstruct());
		}

		$final_nav_array = array();
		$nav_array = explode('/', $url);
		$last_key='';
		$mode = '';
		$submode = '';
		$thirdmode = '';
		foreach($nav_array as $k=>$i){
			if(!$i){
				$final_nav_array[$k]['key'] = $this->RM->getReturnString('homePage');
				$final_nav_array[$k]['url'] = "/";
			} else {
				if($k==1){
					// mode level
					if($navObject[$i]['code'] == $i){
						$mode = $i;
						$final_nav_array[$k]['key'] = $navObject[$i]['page_title'];
						$final_nav_array[$k]['url'] = $navObject[$i]['url'];
					}
				}
				if($k==2){
					// submode level
					if($navObject[$mode][$i]['code'] == $i){
						$submode = $i;
						$final_nav_array[$k]['key'] = $navObject[$mode][$i]['page_title'];
						$final_nav_array[$k]['url'] = $navObject[$mode][$i]['url'];	
					}
				}

				if($k==3){
					// thirdmode level
					if($navObject[$mode][$submode][$i]['code'] == $i){
						$thirdmode = $i;
						$final_nav_array[$k]['key'] = $navObject[$mode][$submode][$i]['page_title'];
						$final_nav_array[$k]['url'] = $navObject[$mode][$submode][$i]['url'];	
					}
				}
				

				/*if($navObject[$i]['code'] == $i){
					$final_nav_array[$k]['key'] = $navObject[$i]['page_title'];
					$final_nav_array[$k]['url'] = $navObject[$i]['url'];
				} else {
					if($navObject[$last_key][$i]['page_title'] && $navObject[$last_key][$i]['url']){
						$final_nav_array[$k]['key'] = $navObject[$last_key][$i]['page_title'];
						$final_nav_array[$k]['url'] = $navObject[$last_key][$i]['url'];	
					}
					
				}*/
				
			}
			$last_key = $i;
		}
		return $final_nav_array;
	}
}
?>