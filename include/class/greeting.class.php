<?php
/* 
    Objective: category related
    Author: Sing
    Last major update: 03-11-2021

	Table scheme: 
        tbl_greeting*


*/

require_once dirname(__FILE__). '/abstract.class.php';
class greeting extends baseClass{
    
    function editGreetingCate($cate_id, $data){
        // edit greeting cate, EG: 祝賀句子, 生日祝賀句子
        // $cate_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($cate_id == 0 )
            $cate_id = $this->DB->insert_db('tbl_greeting_cate', null);

        $sql = $this->DB->buildSql_update("tbl_greeting_cate", "cate_id", $cate_id, $data);
        $this->DB->update($sql);

        return $this->getGreetingCateDtl($data['shop_id'], $cate_id);
    }

    function editGreetingSubcate($subcate_id, $data){
        // edit greeting subcate, EG: 賀結誼, 賀擴展業務
        // $subcate_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($subcate_id == 0 )
            $subcate_id = $this->DB->insert_db('tbl_greeting_subcate', null);

        $sql = $this->DB->buildSql_update("tbl_greeting_subcate", "subcate_id", $subcate_id, $data);
        $this->DB->update($sql);

        return $this->getGreetingSubcateDtl($data['shop_id'], $subcate_id);
    }

    function editGreeting($greeting_id, $data){
        // edit greeting
        // $greeting_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($greeting_id == 0 )
            $greeting_id = $this->DB->insert_db('tbl_greeting', null);

        $sql = $this->DB->buildSql_update("tbl_greeting", "greeting_id", $greeting_id, $data);
        $this->DB->update($sql);

        return $this->getGreetingDtl($greeting_id);
    }

    function getGreetingDtl($greeting_id){
        // get the greeting msg Detail
        // $greeting_id: PK

        $sql = "SELECT * FROM `tbl_greeting` WHERE activate = 'Y' && greeting_id='$greeting_id'";
        $res = $this->DB->get_Sql($sql)[0];

        return $res;
    }

    // function getGreetingCategoryMapping($cate_id){        
    //     $sql = "SELECT * 
    //             FROM `tmap_cate_greeting` AS m
    //             INNER JOIN `tbl_greeting_cate` AS c ON c.cate_id = m.greeting_cate_id
    //             WHERE m.activate = 'Y' && c.activate ='Y' && m.cate_id = '$cate_id'";
    //     return $this->DB->get_Sql($sql);
    // }

    function getGreetingSubcateDtl($shop_id, $subcate_id){
        // get the greeting subcate  Detail
        // $shop_id: shop in use
        // $subcate_id: PK

        $sql = "SELECT c.*
                FROM `tbl_greeting_subcate` as c
                WHERE c.activate = 'Y' && c.shop_id = '$shop_id' && c.subcate_id = '$subcate_id'";
        $res = $this->DB->get_Sql($sql)[0];
        
        $sql = "SELECT * FROM `tbl_greeting` WHERE activate = 'Y' && shop_id = '$shop_id' && subcate_id = '$subcate_id'";
        $res['greetingList'] = $this->DB->get_Sql($sql);

        return $res;
    }

    function getGreetingCateList($shop_id = null, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: which shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        // $sql = "SELECT c.* 
        //         FROM `tbl_greeting_cate` AS c
        //         LEFT JOIN `tmap_cate_greeting` AS m ON m.greeting_cate_id = c.cate_id AND m.activate = 'Y'
        //         WHERE c.activate = 'Y'";
        $sql = "SELECT c.* 
                FROM `tbl_greeting_cate` AS c
                LEFT JOIN `tbl_greeting_subcate` AS sc ON sc.parent_cate_id = c.cate_id AND sc.activate = 'Y'
                WHERE c.activate = 'Y'";

        if ($filter){
            foreach($filter as $k => $i){
                if ($i){
                    switch ($k){
                        case 'skey':
                            $sql .= " AND (c.name_tc LIKE '%$i%' OR c.name_en LIKE '%$i%' OR sc.name_tc LIKE '%$i%' OR sc.name_en LIKE '%$i%')";
                            break;
                        // case 'cate_id':
                        //     $sql .= " AND (m.`cate_id` = '$i')";
                        //     break;
                    }
                }
            }
        }

        $sql .= " GROUP BY cate_id";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        return $this->DB->get_Sql($sql);
    }

    function getGreetingCateDtl($shop_id, $cate_id){
        // get the companyDisocunt Detail
        // $shop_id: shop in use
        // $cate_id: PK

        $sql = "SELECT * FROM `tbl_greeting_cate` WHERE activate = 'Y' && shop_id = '$shop_id' && cate_id = '$cate_id'";
        $res = $this->DB->get_Sql($sql)[0];

        $sql = "SELECT * FROM `tbl_greeting_subcate` WHERE activate = 'Y' && shop_id = '$shop_id' && parent_cate_id = '$cate_id'";
        $res['subcateList'] = $this->DB->get_Sql($sql);

        return $res;
    }

    function editGreetingGroup($data){
        // edit gift Greeting
        // $Greeting_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        
        if ($data['greeting_group_id'] == 0){
            $greeting_group_id = $this->DB->insert_db('tbl_greeting_group', $data);
        } else {
            $greeting_group_id = $this->DB->update_Sql('tbl_greeting_group', 'greeting_group_id', $data);
        }

        $this->DB->updateMap("tmap_greeting_group", "greeting_group_id", $greeting_group_id, "cate_id", $data['cate_id_arr']);

        $sql = $this->DB->buildSql_update("tbl_greeting_group", "greeting_group_id", $greeting_group_id, $data);
        $this->DB->update($sql);

        return $this->getGreetingGroupDtl($greeting_group_id);
    }

    function getGreetingGroupList($filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: which shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $fromShort = "gg";
        $sql = "SELECT *
                FROM `tbl_greeting_group` AS $fromShort
                WHERE $fromShort.`activate` = 'Y'";

        foreach($filter AS $k => $i){
            if ($i){
                switch($k){
                    case 'skey':
                        $sql .= " AND ($fromShort.greeting_group_id LIKE '%$i$' OR $fromShort.name_tc LIKE '%$i%' OR $fromShort.name_en LIKE '%$i%') ";
                        break;
                    case 'greeting_group_id':
                        $sql .= " AND ($fromShort.greeting_group_id = '$i')";
                        break;
                }
            }
        }

        $sql .= "GROUP BY $fromShort.greeting_group_id";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit']; 
        }
        
        $res = $this->DB->get_Sql($sql);
        
        foreach ($res as $k => $i){
            $res[$k] = $this->dispatchGreetingGroup($i);
        }

        return $res;
    }

    function dispatchGreetingGroup($greeting_group){
        $greeting_group_id = $greeting_group['greeting_group_id'];

        $sql = "SELECT gc.*, m.map_id
                FROM `tmap_greeting_group` AS m 
                LEFT JOIN `tbl_greeting_cate` AS gc ON gc.cate_id = m.cate_id
                WHERE m.`greeting_group_id` = $greeting_group_id AND m.activate = 'Y'";
        $greeting_group['greeting_cate_list'] = $this->DB->get_Sql($sql);

        return $greeting_group;
    }

    function getGreetingGroupDtl($greeting_group_id){
        if ($greeting_group_id){
            return $this->getGreetingGroupList(["greeting_group_id" => $greeting_group_id], $this->DB->getUnique)[0];
        } else {
            return null;
        }
    }
}
?>