<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;

		require_once ROOT. '/include/apiDataBuilder.class.php';

		switch ($this->gthis->thirdmode) {
			case 'session':
				new apiDataBuilder(0, $_SESSION);
				break;
			case 'reset_session':
				$_SESSION['callPath'] = [];
				new apiDataBuilder(0, null, null, $this->lang);
				break;
			case 'getLocationDtl':
				$this->getLocationDtl();
				break;
			case 'validateBarcode';
				$this->validateBarcode();
				break;
			case 'editLocation':
				$this->editLocation();
				break;
			case 'editSku':
				$this->editSku();
				break;
			case 'voidSku':
				$this->voidSku();
				break;
			case 'editPurchase':
				$this->editPurchase();
				break;
			case 'getSupplierList':
				$this->getSupplierList();
				break;
			case 'getSupplierTagList':
				$this->getSupplierTagList();
				break;
			case 'editItem':
				$this->editItem();
				break;
			case 'delItem':
				$this->delItem();
				break;
			case 'getSkuList':
				$this->getSkuList();
				break;
			case 'getItemList':
				$this->getItemList();
				break;
			case 'getAllItemList':
				$this->getAllItemList();
				break;
			case 'getItemGroupList':
				$this->getItemGroupList();
				break;
			case 'getItemGroupDtl':
				$this->getItemGroupDtl();
				break;
			case 'delItemGroup':
				$this->delItemGroup();
				break;
			case 'editItemGrp':
				$this->editItemGrp();
				break;
			case 'getItemSkuList':
				$this->getItemSkuList();
				break;
			case 'getSkuCodeList':
				$this->getSkuCodeList();
				break;
			// case 'addSku':
			// 	$this->addSku();
			// 	break;
			case 'editSkuVal':
				$this->editSkuVal();
				break;
			case 'addSkuCode':
				$this->addSkuCode();
				break;
			case 'addLocationSku':
				$this->addLocationSku();
				break;
			case 'moveItem':
				$this->moveItem();
				break;
			case 'unloadItem':
				$this->unloadItem();
				break;
			case 'loadItem':
				$this->loadItem();
				break;
			case 'editSupplier':
				$this->editSupplier();
				break;
			case 'getSkuDtl':
				$this->getSkuDtl();
				break;
			case 'startStocktake':
				$this->startStocktake();
				break;
			case 'editStocktake':
				$this->editStocktake();
				break;
			case 'submitStocktake':
				$this->submitStocktake();
				break;
			case 'createItemGrpBySku':
				$this->createItemGrpBySku();
				break;
			case 'getItemTagList':
				$this->getItemTagList();
				break;
			case 'getSkuTagList':
				$this->getSkuTagList();
				break;
			case 'addItemTag':
				$this->addItemTag();
				break;
			case 'addSkuTag':
				$this->addSkuTag();
				break;
			case 'delItemTag':
				$this->delItemTag();
				break;
			case 'delSkuTag':
				$this->delSkuTag();
				break;

			case 'getItemDtl':
				$this->getItemDtl();
				break;

			case 'getCountryList':
				$this->getCountryList();
				break;
			case 'editCountry':
				$this->editCountry();
				break;
			
			case 'getBrandList':
				$this->getBrandList();
				break;
			case 'editBrand':
				$this->editBrand();
				break;
			
			case 'getItemCateList':
				$this->getItemCateList();
				break;
			case 'editItemCate':
				$this->editItemCate();
				break;
			
			case 'getItemTagList':
				$this->getItemTagList();
				break;
			case 'editItemTag':
				$this->editItemTag();
				break;

			case 'getItemCateVarious':
				$this->getItemCateVarious();
				break;
			case 'editItemCateVariousType':
				$this->editItemCateVariousType();
				break;
			case 'editItemCateVariousVal':
				$this->editItemCateVariousVal();
				break;


			case 'getCateVariousTpl':
				$this->getCateVariousTpl();
				break;
			case 'editItemCateVariousTpl':
				$this->editItemCateVariousTpl();
				break;

			case 'addItemVarious':
				$this->addItemVarious();
				break;

			case 'uploadItemImg':
				$this->uploadItemImg();
				break;

			case 'renderVariousCodeMap':
				$this->renderVariousCodeMap();
				break;


			case 'getItemCateDtl':
				$this->getItemCateDtl();
				break;
			case 'createItemGrp':
				$this->createItemGrp();
				break;

			case 'getStockInStockOutList':
				$this->getStockInStockOutList();
				break;

			case 'sortItemCate':
				$this->sortItemCate();
				break;

			case 'sortSupplierTag':
				$this->sortSupplierTag();
				break;

			case 'getLocationList':
				$this->getLocationList();
				break;
				
			default:
				new apiDataBuilder(-84, null, null, $this->lang);
				break;
		}

		$result_array['lang'] = $this->lang;
		@$this->gthis->assign('result_array',$result_array);
	}

	function getLocationList(){
		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);

		$location_list = $warehouseCls->getLocationList();

		$res['location_list'] = $location_list;

		new apiDataBuilder(0, $res);
	}

	function sortSupplierTag(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/supplier.class.php';
		$supplierCls = new supplier($this->DB, $this->lang, $this->isAdmin);

		$tag_id_arr = $this->DB->parseIntArr($this->gthis->parm_sql('request', 'tag_id_arr', 'Y'));

		$res = $supplierCls->sortSupplierTag($tag_id_arr);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function sortItemCate(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/itemCate.class.php';
		$itemCateCls = new itemCate($this->DB, $this->lang, $this->isAdmin);

		$cate_id_arr = $this->DB->parseIntArr($this->gthis->parm_sql('request', 'cate_id_arr', 'Y'));

		$res = $itemCateCls->sortItemCate($cate_id_arr);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function getItemCateDtl(){
		require_once ROOT.'/include/class/itemCate.class.php';
		$itemCateCls = new itemCate($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "cate_id", "type" => "int", "compulsory" => true, "allowBlank" => false]
		]);

		$res['item_cate'] = $itemCateCls->getItemCateDtl($data['cate_id']);

		new apiDataBuilder(0, $res);
	}

	function getCateVariousTpl(){
		require_once ROOT.'/include/class/cateVarious.class.php';
		$cateVariousCls = new cateVarious($this->DB, $this->lang, $this->isAdmin);

		$variousTplList = $cateVariousCls->getCateVariousTypeTplList();

		$res['variousTplList'] = $variousTplList;

		new apiDataBuilder(0, $res);
	}

	function editItemCateVariousTpl(){
		require_once ROOT.'/include/class/cateVarious.class.php';
		$cateVariousCls = new cateVarious($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "type_id", "type" => "int", "compulsory" => true, "allowBlank" => false],
			["key" => "name_tc"],
			["key" => "name_en"],
			["key" => "remarks"],
			["key" => "val_json", "type" => "json"],
			["key" => "activate", "type" => "NY"]
		]);

		$res['various_list'] = $cateVariousCls->editCateVariousTpl($data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function renderVariousCodeMap(){
		if (intval($_SESSION['right']['item']) < 30){
			new apiDataBuilder(-62);
		}

		$data = $this->gthis->paramBuilder([
			["key" => "various_json", "type" => "json"],
			["key" => "sku"]
		]);

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/cateVarious.class.php';
		$cateVariousCls = new cateVarious($this->DB, $this->lang, $this->isAdmin);

		$itemDtl = $itemCls->getItemDtlBySku($data['sku']);
		if ($itemDtl['various_item_id']){
			$itemDtl = $itemCls->getItemDtl($data['various_item_id']);
		}
		if ($itemDtl){
			$itemCls->getItemList(["various_item_id" => $itemDtl['item_id']]);
		}

		$codeMap = $cateVariousCls->renderCodeMap($data['various_json']);

		$skuList = [];

		foreach($codeMap as $code){
			$name = "";
			$sku = $data['sku'];
			$val_id_arr = [];

			foreach($code as $val_id){
				$val = $cateVariousCls->getVariousValDtl($val_id);
				if ($name == ""){
					$name .= $val['name_tc'];
				} else {
					$name .= "-".$val['name_tc'];
				}
				
				$sku .= "-".$val['val'];
				array_push($val_id_arr, $val['val_id']);
			}

			$skuDtl = $itemCls->getItemDtlBySku($sku);

			if (!$skuDtl){
				array_push($skuList, [
					"name" => $name,
					"sku" => $sku,
					"val_id_arr" => implode(",", $val_id_arr)
				]);
			}


		}

		$res['sku_list'] = $skuList;

		new apiDataBuilder(0, $res);
	}

	function uploadItemImg(){
		if (intval($_SESSION['right']['item']) < 30){
			new apiDataBuilder(-62);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "item_id", "type" => "int"]
		]);

		
		$res['img_path'] = $itemCls->uploadItemImg($_FILES['item_img'], $data['item_id']);
		

		new apiDataBuilder(0, $res);
	}

	function submitStocktake(){
		if (intval($_SESSION['admin']['role']['stocktake_approver_right'] != 'Y')){
			new apiDataBuilder(-62, $_SESSION, null, $this->lang);
		}

		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$stocktake_id = $this->gthis->parm_sql("request", "stocktake_id",'Y');

		$stocktakeDtl = $warehouseCls->getStocktakeDtl($stocktake_id);

		$inList = [];
		foreach ($stocktakeDtl['locationList'] as $k => $location){
			foreach($location['skuList'] as $kk => $sku){
				$diff = intval($sku['actual_qty']) - intval($sku['expected_qty']);
				if ($diff > 0){
					// stockin
					array_push($inList, [
						"qty" => $diff,
						"location_id" => $location['location_id'],
						"sku_id" => $sku['sku_id']	
					]);
				} else if ($diff < 0){
					// stockout
					$data = [
						"qty" => $diff * -1,
						"location_id" => $location['location_id'],
						"remarks" => "盤點",
						"sku_id" => $sku['sku_id']	
					];

					$itemCls->voidSku($data);
				}
			}
        }

		if (sizeof($inList) > 0){
			$data = [
				"inList" => $inList,
				"remarks" => "盤點"
			];
			$itemCls->editPurchase(0, $data);
		}		

		$warehouseCls->editStocktake($stocktake_id, ["status" => "20_completed"]);


		new apiDataBuilder(0);
	}

	function editStocktake(){
		if (intval($_SESSION['right']['warehouse']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);

		$data = [];
		$fieldArr = ["stocktake_id", "stocktakeJson"];
		
		foreach ($fieldArr as $field){
			if(isset($_REQUEST[$field])){
				$$field = $this->gthis->parm_sql('request', $field);
				switch ($field){
					case 'stocktake_id':
						if (!$$field){
							new apiDataBuilder(-99, null, "$field cannot be null", $this->lang);
						}
						$$field = $this->DB->parseInt($$field);
						break;
					case 'stocktakeJson':
						$$field  = json_decode(str_replace("\\", "", $$field), true);
						break;
	
				}
				$data[$field] = $$field;
			}
		}

		$isAllChecked = true;
		if ($stocktakeJson){
			foreach ($stocktakeJson as $stocktake){
				if ($isAllChecked && $stocktake['actual_qty'] == ''){
					$isAllChecked = false;
				}
				$warehouseCls->editStocktakeSku($stocktake);
			}
		}

		if ($isAllChecked)
			new apiDataBuilder(31, null, null, $this->lang);
		else 
			new apiDataBuilder(0, null, null, $this->lang);
	}

	function startStocktake(){
		if (intval($_SESSION['admin']['role']['stocktake_approver_right'] != 'Y')){
			new apiDataBuilder(-62, $_SESSION, null, $this->lang);
		}

		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);

		$stocktake_id = $warehouseCls->startStocktake();

		$res['stocktake_id'] = $stocktake_id;
		new apiDataBuilder(0, $res);
	}

	function editSupplier(){
		if (intval($_SESSION['right']['warehouse']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$supplier_id = $this->gthis->parm_in('request', 'supplier_id');

		$data = [];
		$fieldArr = ["supplier_name", "contact_name", "contact_phone", "contact_email", "remarks"];
		
		foreach ($fieldArr as $field){
			if(isset($_REQUEST[$field])){
				$$field = $this->gthis->parm_sql('request', $field);


				switch ($field){
					// case 'skuList':
					// 	$$field = json_decode(str_replace("\\", "", $$field), true);
					// 	break;
				}
				$data[$field] = $$field;
			}

			
		}

		$res = $itemCls->editSupplier($supplier_id, $data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function getLocationDtl(){
		if (intval($_SESSION['right']['warehouse']) < 50 && $_SESSION['admin']['mobile_right'] != 'Y'){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);
		
		$location_id = $this->DB->gthis->parm_sql('request', 'location_id', 'Y');
		$locationDtl = $warehouseCls->getLocationDtl($location_id);
		foreach ($locationDtl['skuList'] as $k => $i){
			$locationDtl['skuList'][$k]['skuDtl'] = $itemCls->getSkuDtl($i['sku_id']);
		}
		$res['locationDtl'] = $locationDtl;

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function getSkuDtl(){
		if (intval($_SESSION['right']['warehouse']) < 50 && $_SESSION['admin']['mobile_right'] != 'Y'){
			new apiDataBuilder(-62, $_SESSION, null, $this->lang);
		}

		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);
		
		$code = $this->DB->gthis->parm_sql('request', 'code', 'Y');
		$sku_id = $itemCls->codeToSku($code);
		$skuDtl = $itemCls->getSkuDtl($sku_id);
		$skuDtl['locationList'] = $warehouseCls->getLocationList(['sku_id'=> $sku_id]);
		
		$res['skuDtl'] = $skuDtl;

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function validateBarcode(){
		if (intval($_SESSION['right']['item']) < 50 && intval($_SESSION['right']['warehouse']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$sku_id = $this->DB->gthis->parm_sql('request', 'sku_id', 'Y');
		$barcode = $this->DB->gthis->parm_sql('request', 'barcode', 'Y');
		
		if (str_contains($barcode, 'SKUID:'))
			new apiDataBuilder(-35, null, null, $this->lang) ;

		if ($itemCls->isSkuBarcodeMatch($sku_id, $barcode)){
			new apiDataBuilder(0, null, null, $this->lang);
		} else {
			new apiDataBuilder(-30, null, null, $this->lang);
		}
	}
	
	function loadItem(){
		if (intval($_SESSION['right']['warehouse']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		
		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$location_id = $this->DB->gthis->parm_sql('request', 'location_id', 'Y');
		$code = $this->DB->gthis->parm_sql('request', 'code', 'Y'); // barcode || sku_id (SKUID:[0-9])
		$qty = $this->DB->gthis->parm_sql('request', 'qty');
		if (!$qty)
			$qty = 1;

		$item_id = $itemCls->codeToItem($code);
		$location = $warehouseCls->getLocationDtl($location_id);

		if (!$location)
			new apiDataBuilder(-32, null, null, $this->lang);
			
		if (!$item_id)
			new apiDataBuilder(-33, null, null, $this->lang);
			

		$location = $warehouseCls->loadItem($location['location_id'], $item_id, $qty);
		new apiDataBuilder(0, $location, null, $this->lang);
	}

	function unloaditem(){
		if (intval($_SESSION['right']['warehouse']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		
		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$location_id = $this->DB->gthis->parm_sql('request', 'location_id', 'Y');
		$code = $this->DB->gthis->parm_sql('request', 'code', 'Y'); // barcode || sku_id (SKUID:[0-9])
		$qty = $this->DB->gthis->parm_sql('request', 'qty');
		if (!$qty)
			$qty = 1;


		$item_id = $itemCls->codeToItem($code);
		$location = $warehouseCls->getLocationDtl($location_id);

		if (!$location)
			new apiDataBuilder(-32, null, null, $this->lang);
			
		if (!$item_id)
			new apiDataBuilder(-33, null, null, $this->lang);
			

		$location = $warehouseCls->unloadItem($location['location_id'], $item_id, $qty);
		new apiDataBuilder(0, $location, null, $this->lang);
	}

	function moveItem(){
		if (intval($_SESSION['right']['warehouse']) < 50 && $_SESSION['admin']['mobile_right'] != 'Y'){	
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		
		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$tar_location_id = $this->DB->gthis->parm_sql('request', 'tar_location_id', 'Y');
		$location_id = $this->DB->gthis->parm_sql('request', 'location_id', 'Y');
		$code = $this->DB->gthis->parm_sql('request', 'code', 'Y'); // barcode || sku_id (SKUID:[0-9])
		$qty = $this->DB->gthis->parm_sql('request', 'qty');
		if (!$qty)
			$qty = 1;

		$item_id = $itemCls->codeToItem($code);
		$location = $warehouseCls->getLocationDtl($location_id);
		$tar_location = $warehouseCls->getLocationDtl($tar_location_id);

		if (!$location || !$tar_location)
			new apiDataBuilder(-32, null, null, $this->lang);
			
		if (!$item_id)
			new apiDataBuilder(-33, null, null, $this->lang);

		$location = $warehouseCls->moveItem($location['location_id'], $tar_location['location_id'],$item_id, $qty);
		$tar_location = $warehouseCls->getLocationDtl($tar_location_id);
		new apiDataBuilder(0, $tar_location, null, $this->lang);
	}

	function addLocationSku(){
		if (intval($_SESSION['right']['warehouse']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);

		$data = [];
		$fieldArr = ["sku", "qty", "location_id"];
		
		foreach ($fieldArr as $field){
			if(isset($_REQUEST[$field])){
				$$field = $this->gthis->parm_sql('request', $field);
				$data[$field] = $$field;
			} else {
				new apiDataBuilder(-99, null, "$field cannot be null", $this->lang);
			}
		}

		$warehouseCls->addLocationSku($location_id, $sku, $qty);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editLocation(){
		if (intval($_SESSION['right']['warehouse']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);

		$location_id = $this->gthis->parm_in('request', 'location_id');

		$data = [];
		$fieldArr = ["adm_name", "remarks"];
		
		foreach ($fieldArr as $field){
			if(isset($_REQUEST[$field])){
				$$field = $this->gthis->parm_sql('request', $field);
				$data[$field] = $$field;
			}
		}

		$res = $warehouseCls->editLocation($location_id, $data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}
	
	function editSku(){
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = [];
		$fieldArr = ["sku_id", "alert_qty", "cost", "auto", "alias", "sAlias"];
		
		foreach ($fieldArr as $field){
			if(isset($_REQUEST[$field])){
				$$field = $this->gthis->parm_sql('request', $field);
				switch ($field){
					case 'alert_qty':
					case 'cost':
						$$field = $this->DB->parseInt($$field);
						break;
					case 'sku_id':
						if (!$$field){
							new apiDataBuilder(-99, null, "$field cannot be null", $this->lang);
						}
						$$field = $this->DB->parseInt($$field);
						break;
	
				}
				$data[$field] = $$field;
			}
		}

		$res = $itemCls->editSku($sku_id, $data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function voidSku(){
		if (intval($_SESSION['right']['item']) < 50 && $_SESSION['admin']['mobile_right'] != 'Y'){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = [];
		
		$fieldArr = ["qty", "remarks", "location_id"];
		
		foreach ($fieldArr as $field){
			if(isset($_REQUEST[$field])){
				$$field = $this->gthis->parm_sql('request', $field);
				switch ($field){
					case 'qty':
					case 'location_id':
						if (!$$field){
							new apiDataBuilder(-99, null, "$field cannot be null", $this->lang);
						}
						$$field = $this->DB->parseInt($$field);
						break;
	
				}
				$data[$field] = $$field;
			}
		}

		$code = $this->DB->gthis->parm_sql('request', 'code', 'Y');
		$item_id = $itemCls->codeToItem($code);
		$data['item_id'] = $item_id;
		
		$itemCls->voidSku($data);

		new apiDataBuilder(0, null, null, $this->lang);
	}

	function editPurchase(){
		if (intval($_SESSION['right']['warehouse']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);
		
		$po_id = $this->DB->parseInt($this->gthis->parm_sql('request', 'po_id'));

		$data = $this->gthis->paramBuilder([
			["key" => "po_id", "type" => "int"],
			["key" => "supplier_id", "type" => "int"],
			["key" => "ref_no"],
			["key" => "type"],
			["key" => "remarks"],
			["key" => "inList", "type" => "json"],
			["key" => "ref_file", "request" => "file", ],
		]);

		// $data = [];
		// if ($po_id == 0){
		// 	$fieldArr = ["supplier_id", "ref_no", "remarks", "inList"];
			
		// 	foreach ($fieldArr as $field){
		// 		$$field = $this->gthis->parm_sql('request', $field);
		// 		switch ($field){
		// 			case 'supplier_id':
		// 				$$field = $this->DB->parseInt($$field);
		// 				break;
		// 			case 'inList':
		// 				$$field = json_decode(str_replace("\\", "", $$field), true);
	
		// 		}
		// 		$$field? $data[$field] = $$field: null;
		// 	}

		// 	$ref_file = $this->gthis->parm_in('file', "ref_file");
		// 	if ($ref_file)
		// 		$data['ref_file'] = $ref_file;

		// 	// foreach ($inList as $in){
		// 	// 	// check whether the barcode exist or not
		// 	// 	$barcodeArr = explode(",", $in['barcode']);
		// 	// 	foreach($barcodeArr as $barcode){
		// 	// 		if ($barcode){
		// 	// 			if(!$itemCls->isSkuBarcodeMatch($in['sku_id'], $barcode))
		// 	// 				new apiDataBuilder(-30, null, null, $this->lang);
		// 	// 			else 
		// 	// 				$itemCls->addSkuBarcodeMatch($in['sku_id'], $barcode);
		// 	// 		}
		// 	// 	}
		// 	// }
		// } else {
		// 	$fieldArr = ["ref_no", "remarks"];
			
		// 	foreach ($fieldArr as $field){
		// 		$$field = $this->gthis->parm_sql('request', $field);
		// 		$$field? $data[$field] = $$field: null;
		// 	}
		// }
		$purchaseDtl = $itemCls->editPurchase($data['po_id'], $data);

		new apiDataBuilder(0, $purchaseDtl, null, $this->lang);

	}

	// function getSupplierTagList(){
	// 	require_once ROOT.'/include/class/supplier.class.php';
	// 	$supplierCls = new supplier($this->DB, $this->lang, $this->isAdmin);

	// 	$res['supplier_tag_list'] = $supplierCls->getSupplierTagList();

	// 	new apiDataBuilder(0, $res, null, $this->lang);
	// }

	function getSupplierList(){
		if (intval($_SESSION['right']['warehouse']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$skey = $this->gthis->parm_sql('request', 'skey');
		$page = $this->gthis->parm_sql('request', 'page');
		$limit = $this->gthis->parm_sql('request', 'limit');
		$sort = $this->gthis->parm_sql('request', 'sort');
		$order = $this->gthis->parm_sql('request', 'order');
        
		$filter = [
			"skey" => $skey ? $skey : null
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "name_tc",
			"order" => $order ? $order : "ASC"
		];
		
		$res = $itemCls->getSupplierList($filter, $limiter, $sorting);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function addSkuCode(){
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = [];
		$fieldArr = ["code"];
		
		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			if (!$$field)
				new apiDataBuilder(-99, null, null, $this->lang);

			$data[$field] = $$field;
		}

		if (sizeof($itemCls->getSkuCodeList(["code" => $data['code']])) == 0)
			$itemCls->addSkuCode(0, $data);

		$res['skuCodeList'] = $itemCls->getSkuCodeList();

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editSkuVal(){
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = [];
		$fieldArr = ["val_id", "code_id", "val", "lbl_tc", "lbl_en"];
		
		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($$field){
				case 'code_id':
				case 'val':
				case 'lbl_tc':
				case 'lbl_en':
					if (!$$field)
						new apiDataBuilder(-99, null, null, $this->lang);
					break;
			}

			$data[$field] = $$field;
		}

		$itemCls->editSkuVal($data['val_id'], $data);

		$res['skuCodeList'] = $itemCls->getSkuCodeList();

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function addSku(){
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key"=> "item_id", "compulsory" => true, "type" => "int"],
			["key"=> "codeMap", "compulsory" => true, "type" => "json"]
		]);

		$itemDtl = $itemCls->getItemDtl($data['item_id']);
		$itemDtl['skuList'] = $itemCls->getSkuList(['item_id' => $itemDtl['item_id']]);


		$sku_id_arr = $itemCls->addSku($itemDtl, $data['codeMap']);

		new apiDataBuilder(0, $sku_id_arr, null, $this->lang);
	}

	function getSkuCodeList(){
		if (intval($_SESSION['right']['item']) < 30){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$skey = $this->gthis->parm_sql('request', 'skey');

		$filter = [
			"skey" => $skey ? $skey : null
		];

		$res = $itemCls->getSkuCodeList($filter);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function getItemSkuList(){
		if (intval($_SESSION['right']['item']) < 30){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
        
		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$res = $itemCls->getItemSkuList($shop_id, $filter);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function createItemGrpBySku(){
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$sku_id = $this->gthis->parm_sql("request", "sku_id", "Y");
		$skuDtl = $itemCls->getSkuDtl($sku_id);

		$skuList = $itemCls->getSkuList(['item_id' => $skuDtl['item_id']], null, ["sort" => "s.sku_id=$sku_id DESC, sku_val"]);
		
		$item_grp_content_json = []; 
		foreach($skuList as $k => $sku){
			array_push($item_grp_content_json,[
				"content_id" => "0",
				"sku_id" => $sku['sku_id'],
				"name_tc" => $sku['name_tc'],
				"name_en" => $sku['name_en'],
				"qty" => "1",
				"additional_price" => "0",
				"remarks" => "",
				"client_optional" => "N",
				"is_active" => "Y"
			]);
		}
		$data = [
			"adm_name" => $skuDtl['name_tc'] . "物品組合",
			"item_grp_content_json_str" => $item_grp_content_json
		];

		$res['itemGrpDtl'] = $itemCls->editItemGrp(0, $data);
		new apiDataBuilder(0, $res);
	}

	function delItemGroup(){
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "grp_id", "type" => "int"],
			["key" => "activate", "request" => "self", "defaultValue" => "N"]
		]);

		$res = $itemCls->editItemGroup($data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editItemGrp(){
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "grp_id", "type" => "int"],
			["key" => "adm_name", "allowBlank" => false],
			["key" => "alias"],
			["key" => "inhouse_name_tc"],
			["key" => "inhouse_name_en"],
			["key" => "gift_name_tc"],
			["key" => "gift_name_en"],
			["key" => "zuri_name_tc"],
			["key" => "zuri_name_en"],
			["key" => "ltp_name_tc"],
			["key" => "ltp_name_en"],
			["key" => "item_json", "type" => "json"],
			["key" => "is_tpl", "type" => "NY"]
		]);
		
		$res = $itemCls->editItemGroup($data);		

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function getItemList(){
		if (intval($_SESSION['right']['item']) < 30){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "supplier_id"],
			["key" => "cate_id_arr", "type" => "intArr"],
			["key" => "skey"],
		]);
		
		$sort = $this->gthis->param(["key" => "sorting"]);

		$sortArr = explode("-", $sort);

		$sorting = [];
		if ($sortArr[0])
			$sorting["sort"] = $sortArr[0];
		
		if ($sortArr[1])
			$sorting["order"] = $sortArr[1];
		


		
		$data["parent_level_only"] = 'Y';

		$res = $itemCls->getItemList($data, null, $sorting);


		foreach ($res as $k => $item){
			$res[$k] = $this->dispatchItemDtl($item['item_id']);
		}

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function getAllItemList(){
		if (intval($_SESSION['right']['item']) < 30){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "supplier_id"],
			["key" => "cate_id_arr", "type" => "intArr"],
			["key" => "skey"]
		]);


		$res = $itemCls->getItemList($data);


		foreach ($res as $k => $item){
			$res[$k] = $this->dispatchItemDtl($item['item_id']);
		}

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function getItemGroupList(){
		if (intval($_SESSION['right']['item']) < 30){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$filter = $this->gthis->paramBuilder([
			["key" => "skey"],
			["key" => "is_tpl", "type" => "NY"],
			["key" => "cate_id_arr", "type" => "intArr"]
		]);

		$group_list = $itemCls->getItemGroupList($filter);

		$res['group_list'] = $group_list;

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function getItemGroupDtl(){
		if (intval($_SESSION['right']['item']) < 30){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "grp_id", "type" => "int"]
		]);

		$group = $itemCls->getItemGroupDtl($data['grp_id']);

		$res['group'] = $group;

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function getSkuList(){
		if (intval($_SESSION['right']['item']) < 30){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
		
        
		$filter = [
			"skey" => $skey ? $skey : null,
			"shop_id" => $shop_id ? $shop_id : null
		];
		
		$res = $itemCls->getSkuList($filter, null, ["sort" => "s.sku_val"]);

		new apiDataBuilder(0, $res, null, $this->lang);
	}
	
	function delItem(){
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "item_id", "type" => "int"]
		]);
		
		$data["activate"] = 'N';

		$itemCls->editItem($data);

		new apiDataBuilder(0, null, null, $this->lang);
	}

	function editItem(){
		// $this->gthis->showError();
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "item_id", "type" => "int", "compulsory" => true],
			["key" => "item_cate_id", "type" => "int", "compulsory" => true, "allowBlank" => false, "allowZero" => false],
			["key" => "supplier_id", "type" => "int", "compulsory" => true, "allowBlank" => false, "allowZero" => false],
			["key" => "brand_id", "type" => "int", "allowZero" => false, "defaultValue" => "NULL"],
			["key" => "country_tc"],
			["key" => "country_en"],
			["key" => "sku", "allowBlank" => false],
			["key" => "alias"],
			["key" => "supplier_name_tc"],
			["key" => "supplier_name_en"],
			["key" => "pattern"],
			["key" => "inhouse_name_tc"],
			["key" => "inhouse_name_en"],
			["key" => "gift_name_tc"],
			["key" => "gift_name_en"],
			["key" => "zuri_name_tc"],
			["key" => "zuri_name_en"],
			["key" => "ltp_name_tc"],
			["key" => "ltp_name_en"],
			["key" => "supplier_barcode"],
			["key" => "supplier_stock_code"],
			["key" => "unit"],
			["key" => "cost", "type" => "int"],
			["key" => "stockin_price", "type" => "int"],
			["key" => "suggested_price"],
			["key" => "alert_qty"],
			["key" => "is_unlimited"],
			["key" => "remarks"],
			["key" => "item_img"],
			["key" => "qty"],
			["key" => "ava_qty"],
			["key" => "reserved_qty"],
			["key" => "replace_sku_id", "type" => "int"],
			["key" => "is_unlimited_qty", "type" => "NY"],
			["key" => "is_product_page_show_qty", "type" => "NY"],
			["key" => "is_replace_period", "type" => "NY"],
			["key" => "replace_period_start", "type" => "parseDate"],
			["key" => "replace_period_end", "type" => "parseDate"],
			["key" => "is_replace", "type" => "NY"],
			["key" => "tag_id_arr", "type" => "intArr"],
			["key" => "various_json", "type" => "json"],
			["key" => "activate", "type" => "NY"]
		]);

		$data['sku'] ? $data['sku_prefix'] = $data['sku']: null;
		
		$itemDtl = $itemCls->editItem($data);
		foreach ($data['various_json'] as $k => $data){
			$cItemDtl = $itemCls->getItemDtlBySku($data['sku']);

			if ($cItemDtl){
				$cData = $data;
				$cData['item_id'] = $cItemDtl['item_id'];
			} else {
				$cData = $itemDtl;
				$cData['item_id'] = 0;
				$cData["item_img"] = $data['item_img'];
				$cData['qty'] = $data['qty'];
				$cData['val_list'] = $data['val_list'];
				$cData['sku'] = $data['sku'];
				$cData['is_unlimited_qty'] = $data['is_unlimited_qty'];
				$cData['supplier_stock_code'] = $data['supplier_stock_code'];
				$cData['supplier_barcode'] = $data['supplier_barcode'];
				$cData['various_item_id'] = $itemDtl['item_id'];
			}
			
			
			$itemCls->editItem($cData);
		}

		$res["itemDtl"] = $itemCls->dispatchItem($itemDtl);
		
		new apiDataBuilder(0, $res);
	}



	// function editItem(){
	// 	if (intval($_SESSION['right']['item']) < 50){
	// 		new apiDataBuilder(-62, null, null, $this->lang);
	// 	}

	// 	require_once ROOT.'/include/class/item.class.php';
	// 	$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

	// 	$item_id = $this->gthis->parm_sql('request', 'item_id');
	// 	$data = [];
	// 	$fieldArr = ["name_tc", "name_en", "shop_id_arr", "replace_sku_id", "is_replace_period", "replace", "sku_prefix", "replace_period_start", "replace_period_end"];
		
	// 	foreach ($fieldArr as $field){
	// 		$$field = $this->gthis->parm_sql('request', $field);
	// 		switch ($field){
	// 			case 'type':
	// 				if (!$$field){
	// 					new apiDataBuilder(-99, null, "$field cannot be null", $this->lang);
	// 				}
	// 				break;
	// 			case 'is_replace_period':
	// 			case 'replace':
	// 				$$field = $this->DB->parseNY($$field);
	// 				break;
	// 			case 'replace_period_start':
	// 			case 'replace_period_end':
	// 				$$field = $this->DB->parseDate($$field);
	// 				break;
	// 		}
	// 		$$field? $data[$field] = $$field: null;
	// 	}

	// 	if (!$item_id){
	// 		if (!$sku_prefix){
	// 			new apiDataBuilder(-99, null, "SKU prefix cannot be null", $this->lang);
	// 		} else {
	// 			$item = $itemCls->getItemList(null, ["sku_prefix" => $sku_prefix]);
	// 			if ($item){
	// 				new apiDataBuilder(-38, null, "SKU prefix cannot be null", $this->lang);
	// 			}
	// 		}
	// 	}

	// 	if(isset($replace_sku_id))
	// 		$data['replace_sku_id'] = $replace_sku_id;

	// 	$item_img = $this->gthis->parm_in('file', 'item_img');
	// 	$item_img? $data['item_img'] = $item_img: null;
		
	// 	if (strpos($item_id, ",") !== false){
	// 		$item_id_arr = explode(",", $item_id);

	// 		foreach($item_id_arr as $k => $id){
	// 			$res = $itemCls->editItem($id, $data);
	// 			if ($k == 0 && $data['item_img']){
	// 				//special handling for img 
	// 				$data['item_img'] = $res['item_img'];
	// 			}
	// 		}
	// 	} else {
	// 		$res = $itemCls->editItem($item_id, $data);
	// 	}

	// 	new apiDataBuilder(0, $res, null, $this->lang);
	// }

	// function getItemTagList(){
	// 	if (intval($_SESSION['right']['item']) < 50){
	// 		new apiDataBuilder(-62, null, null, $this->lang);
	// 	}

	// 	require_once ROOT.'/include/class/item.class.php';
	// 	$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

	// 	$tagList = $itemCls->getItemTagList();
	// 	$res['tagList'] = $tagList;
		
	// 	new apiDataBuilder(0, $res);
	// }

	function getSkuTagList(){
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$tagList = $itemCls->getSkuTagList();
		$res['tagList'] = $tagList;
		
		new apiDataBuilder(0, $res);
	}

	function addItemTag(){
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key"=> "item_id", "compulsory" => true, "type" => "int"],
			["key"=> "tag", "compulsory" => true],
		]);

		if (sizeof($itemCls->getItemTagList($data)) == 0)
			$itemCls->addItemTag($data);

		$res['itemDtl'] = $itemCls->dispatchItem($itemCls->getItemDtl($data['item_id']));
		
		new apiDataBuilder(0, $res);
	}

	function addSkuTag(){
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key"=> "sku_id", "compulsory" => true, "type" => "int"],
			["key"=> "tag", "compulsory" => true],
		]);

		if (sizeof($itemCls->getSkuTagList($data)) == 0)
			$itemCls->addSkuTag($data);

		$itemList = $itemCls->getItemList($data)[0];
		$itemDtl = $itemCls->getItemDtl($itemList['item_id']);

		$res['itemDtl'] = $itemCls->dispatchItem($itemDtl);
		
		new apiDataBuilder(0, $res);
	}

	function delItemTag(){
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key"=> "item_id", "compulsory" => true, "type" => "int"],
			["key"=> "tag", "compulsory" => true],
		]);

		$itemCls->delItemTag($data['item_id'], $data['tag']);

		$res['itemDtl'] = $itemCls->dispatchItem($itemCls->getItemDtl($data['item_id']));
		
		new apiDataBuilder(0, $res);
	}

	function delSkuTag(){
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key"=> "sku_id", "compulsory" => true, "type" => "int"],
			["key"=> "tag", "compulsory" => true],
		]);

		$itemCls->delSkuTag($data['sku_id'], $data['tag']);

		$itemList = $itemCls->getItemList($data)[0];
		$itemDtl = $itemCls->getItemDtl($itemList['item_id']);

		$res['itemDtl'] = $itemCls->dispatchItem($itemDtl);
		
		new apiDataBuilder(0, $res);
	}

	function getItemDtl(){
		if (intval($_SESSION['right']['item']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		$data = $this->gthis->paramBuilder([
			["key" => "item_id", "compulsory" => true, "type" => "int"],
		]);

		$res['item'] = $this->dispatchItemDtl($data['item_id']);

		new apiDataBuilder(0, $res);
	}

	function getCountryList(){
		require_once ROOT.'/include/class/itemCountry.class.php';
		$itemCountryCls = new itemCountry($this->DB, $this->lang, $this->isAdmin);

		$res['item_conutry_list'] = $itemCountryCls->getCountryList();


		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editCountry(){
		require_once ROOT.'/include/class/itemCountry.class.php';
		$itemCountryCls = new itemCountry($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "country_id", "compulsory" => true, "allowBlank" => false],
			["key" => "country_name_tc", "allowBlank" => false],
			["key" => "country_name_en", "allowBlank" => false],
			["key" => "activate", "type" => "NY"],
		]);

		$res['item_country'] = $itemCountryCls->editCountry($data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function getBrandList(){
		require_once ROOT.'/include/class/itemBrand.class.php';
		$itemBrandCls = new itemBrand($this->DB, $this->lang, $this->isAdmin);

		$filter = $this->gthis->paramBuilder([
			["key" => "skey"],
		]);
		
		$sort = $this->gthis->param(["key" => "sorting"]);

		$sortArr = explode("-", $sort);

		$sorting = [];
		if ($sortArr[0])
			$sorting["sort"] = $sortArr[0];
		
		if ($sortArr[1])
			$sorting["order"] = $sortArr[1];

		$res['item_brand_list'] = $itemBrandCls->getBrandList($filter, null, $sorting);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editBrand(){
		require_once ROOT.'/include/class/itemBrand.class.php';
		$itemBrandCls = new itemBrand($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "brand_id", "compulsory" => true, "allowBlank" => false],
			["key" => "brand_name_tc", "allowBlank" => false],
			["key" => "brand_name_en", "allowBlank" => false],
			["key" => "activate", "type" => "NY"],
			["key" => "skey"],
		]);

		$res['item_brand'] = $itemBrandCls->editBrand($data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function getItemCateList(){
		require_once ROOT.'/include/class/itemCate.class.php';
		$itemCateCls = new itemCate($this->DB, $this->lang, $this->isAdmin);

		$res['item_cate_list'] = $itemCateCls->getItemCateList(null, null, ["sort" => "sort"]);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editItemCate(){
		require_once ROOT.'/include/class/itemCate.class.php';
		$itemCateCls = new itemCate($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "cate_id", "type" => "int", "compulsory" => true, "allowBlank" => false],
			["key" => "parent_cate_id", "type" => "int", "allowBlank" => false, "allowZero" => false],
			["key" => "cate_name_tc"],
			["key" => "cate_name_en"],
			["key" => "activate", "type" => "NY"],
			["key" => "various_json", "type" => "json"] // [{"name_tc":"尺碼","name_en":"size","remarks":"衣服尺碼","val_json":[{"val":"S","name_tc":"細","name_en":"small","remarks":""},{"val":"M","name_tc":"中","name_en":"middle","remarks":""},{"val":"L","name_tc":"大","name_en":"large","remarks":""},{"val":"XL","name_tc":"加大","name_en":"x-large","remarks":""}]},{"name_tc":"顏色","name_en":"color","remarks":"衣服顏色（主色）","val_json":[{"val":"RED","name_tc":"紅色","name_en":"red","remarks":""},{"val":"BLUE","name_tc":"湖水藍色","name_en":"lake blue","remarks":"湖水藍"},{"val":"YELLOW","name_tc":"黃色","name_en":"yellow","remarks":""}]}]
		]);

		$res['item_cate'] = $itemCateCls->editItemCate($data);
		
		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function getItemTagList(){
		require_once ROOT.'/include/class/itemTag.class.php';
		$itemTagCls = new itemTag($this->DB, $this->lang, $this->isAdmin);

		$res['item_tag_list'] = $itemTagCls->getItemTagList();

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editItemTag(){
		require_once ROOT.'/include/class/itemTag.class.php';
		$itemTagCls = new itemTag($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "tag_id", "type" => "int", "compulsory" => true, "allowBlank" => false],
			["key" => "parent_tag_id", "type" => "int", "allowBlank" => false, "allowZero" => false],
			["key" => "tag_name_tc"],
			["key" => "tag_name_en"],
			["key" => "activate", "type" => "NY"],
		]);

		$res['item_tag'] = $itemTagCls->editItemTag($data);
		
		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function getItemCateVarious(){
		require_once ROOT.'/include/class/cateVarious.class.php';
		$cateVariousCls = new cateVarious($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "cate_id", "compulsory" => true]
		]);

		$res['various_list'] = $cateVariousCls->getCateVariousTypeList($data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}
	
	function editItemCateVariousType(){
		require_once ROOT.'/include/class/cateVarious.class.php';
		$cateVariousCls = new cateVarious($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "type_id", "type" => "int", "compulsory" => true, "allowBlank" => false],
			["key" => "cate_id", "type" => "int", "compulsory" => true, "allowBlank" => false, "allowZero" => false],
			["key" => "name_tc"],
			["key" => "name_en"],
			["key" => "remarks"],
			["key" => "activate", "type" => "NY"],
		]);

		$res['various_list'] = $cateVariousCls->editCateVariousType($data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}
	
	function editItemCateVariousVal(){
		require_once ROOT.'/include/class/cateVarious.class.php';
		$cateVariousCls = new cateVarious($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "val_id", "type" => "int", "compulsory" => true, "allowBlank" => false],
			["key" => "type_id", "type" => "int", "compulsory" => true, "allowBlank" => false, "allowZero" => false],
			["key" => "val", "allowBlank" => false],
			["key" => "name_tc"],
			["key" => "name_en"],
			["key" => "remarks"],
			["key" => "activate", "type" => "NY"],
			["key" => "isHide", "type" => "NY"],
		]);

		$res['various_list'] = $cateVariousCls->editCateVariousVal($data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function addItemVarious(){
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "various_json", "type" => "json", "compulsory" => true, "allowBlank" => false],
			["key" => "item_id", "type" => "int", "compulsory" => true, "allowBlank" => false, "allowZero" => false],
		]);

		// get the parent itemDtl
		$itemDtl = $itemCls->getItemDtl($data['item_id']);
	
		foreach ($data['various_json'] as $k => $v){

			if ($itemCls->getItemDtlBySku($v['sku'])){ 
				// existing 
			} else {
				// reconstructure the data
				$data = $itemDtl;
				$data["item_id"] = 0;
				$data['various_item_id'] = $itemDtl['item_id'];
				$data['sku'] = $v['sku'];
				$data['qty'] = $v['qty'];
				$data['item_img'] = $v['item_img'];
				$data['val_list'] = $v['val'];
				
				$itemCls->editItem($data);
			}

		}

		$res['item'] = $this->dispatchItemDtl($itemDtl['item_id']);
		
		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function delItemVarious(){
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "item_id", "compulsory" => true, "allowBlank" => false, "allowZero" => false],
			["key" => "various_json", "type" => "json"]
		]);






		$res = $this->dispatchItemDtl($item_id);

		new apiDataBuilder(0, $res);
	}

	function createItemGrp(){
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "item_id", "type" => "int"]
		]);

		$item = $itemCls->getItemDtl($data['item_id']);
		$various_item_list = $itemCls->getItemList(["various_item_id" => $data['item_id']]);		

		$itemJson = [];
		foreach($various_item_list as $various_item){
			array_push($itemJson, [
				"item_id" => $various_item['item_id'],
				"qty" => "1",
				"is_client_optional" => "N",
				"additional_price" => "0",
				"is_auto_replace" => "Y",
				"remarks" => "",
			]);
		}

		$data = [
			"grp_id" => 0,
			"adm_name" => $item['inhouse_name_tc'] . "物品組合",
			"alias" => "",
			"inhouse_name_tc" => $item['inhouse_name_tc'] . "組合",
			"inhouse_name_en" => $item['inhouse_name_en'] . " group",
			"gift_name_tc" => $item['gift_name_tc'] . "組合",
			"gift_name_en" => $item['gift_name_en'] . " group",
			"zuri_name_tc" => $item['zuri_name_tc'] . "組合",
			"zuri_name_en" => $item['zuri_name_en'] . " group",
			"ltp_name_tc" => $item['ltp_name_tc'] . "組合",
			"ltp_name_en" => $item['ltp_name_en'] . " group",
			"is_tpl" => "Y",
			"item_json" => $itemJson
		];

		$res['grp'] = $itemCls->editItemGroup($data);

		new apiDataBuilder(0, $res);
	}

	function getStockInStockOutList(){
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "item_id_arr", "type" => "intArr"],
			["key" => "sorting"],
			["key" => "type_arr", "type" => "arrV1"],
			["key" => "start_date", "type" => "date"],
			["key" => "end_date", "type" => "date"]
		]);
 
		$res['stockInStockOutList'] = $itemCls->getStockInStockOutList($data, null, $data['sorting']);
		$res['stockInStockOutSumUpList'] = $itemCls->stockInStockOutSumUpList($data);

		new apiDataBuilder(0, $res);
	}

	private function dispatchItemDtl($item_id){
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);
		
		$item = $itemCls->getItemDtl($item_id);
		if ($item['various_item_id']){
			$item['parent_item'] = $itemCls->getItemDtl($item['various_item_id']);
			$item['various_item_list'] = $itemCls->getItemList(["various_item_id" => $item['various_item_id']]);
		} else {
			$item['parent_item'] = null;
			$item['various_item_list'] = $itemCls->getItemList(["various_item_id" => $item['item_id']]);
		}

		return $item;
	}
}
