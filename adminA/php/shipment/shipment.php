<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;

		switch ($this->gthis->submode) {
			case 'quickOrder':
				$result_array = $this->quickOrder();
				break;
			case 'quickOrderPreview':
				$result_array = $this->quickOrderPreview();
				break;
            case 'deliList':
                $result_array = $this->deliList();
                break;
            case 'deliDtl':
                $result_array = $this->deliDtl();
                break;
			case 'orderList':
				$result_array = $this->orderList();
				break;
            case 'orderDtl':
                $result_array = $this->orderDtl();
                break;
			case 'deliverySchedule':
				$this->gthis->assign('page_only', 'Y');
				break;
			case 'depositSlipList':
				$result_array = $this->depositSlipList();
				break;
			case 'excelOrderList':
				$result_array = $this->excelOrderList();
				break;
			case 'cardList':
				$result_array = $this->cardList();
				break;
			case 'cardPrint':
				$result_array = $this->cardPrint();
				break;
			case 'deliPrint':
				$this->gthis->assign('page_only', 'Y');
				$result_array = $this->deliDtl();
				break;
			case 'packingPrint':
				$this->gthis->assign('page_only', 'Y');
				$result_array = $this->deliDtl();
				break;
			default:
				$result_array = null;
				break;
		}

		$this->gthis->assign('result_array',$result_array);
	}

	function cardPrint(){
		if (intval($_SESSION['right']['order']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/card.class.php';
        $cardCls = new card($this->DB, $this->lang, $this->isAdmin);

		$this->gthis->assign("page_only",'Y');

		$deli_id_arr = $this->gthis->parm_sql('request', 'deli_id');
		$deli_id_list = explode(",",$deli_id_arr);

		$deliList = [];
		foreach($deli_id_list as $deli_id){
			array_push($deliList, $orderCls->getDeliDtl($deli_id));
		}

		$res['deliList'] = $deliList;
		return $res;
	}

	function cardList(){
		if (intval($_SESSION['right']['order']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/card.class.php';
        $cardCls = new card($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql("session", 'shop_id','Y');
		$page = $this->gthis->parm_sql('request', 'page');
		$limit = $this->gthis->parm_sql('request', 'limit');
		$sort = $this->gthis->parm_sql('request', 'sort');
		$order = $this->gthis->parm_sql('request', 'order');
		
		$card_id_arr = $this->gthis->parm_sql('request', 'card_id_arr');

		$filter = [
			"card_id_arr" => $card_id_arr? $card_id_arr : null,
			"card_only" => 'Y' 
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "deli_id",
			"order" => $order ? $order : "ASC"
		];

		$res['cardList'] = $cardCls->getCardList();
		$res['deliList'] = $orderCls->getDeliList($shop_id, $filter, $limiter, $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "cardList",
			"url" => "admin/shipment/cardList",
			"page_title" => $this->gthis->RMBC->getReturnString('cardList')
		]);

		$res['page_config']['ttlCount'] = sizeof($orderCls->getDeliList($shop_id, $filter));
		$res['page_config'] = array_merge($res['page_config'], $filter);
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);

		return $res;
	}

	function excelOrderList(){
		if (intval($_SESSION['right']['order']) < 30){
			return array("right"=>"Failure");
		}

        require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->lang, $this->isAdmin);
		$shop_id = $this->gthis->parm_sql("session", 'shop_id','Y');

		$filter = [
			"is_confirmed" => 'N'
		];

		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "create_datetime",
			"order" => $order ? $order : "ASC"
		];

		$res['excelList'] = $orderCls->getExcelOrderList($shop_id, $filter, $limiter, $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "excelOrderList",
			"url" => "admin/shipment/excelOrderList",
			"page_title" => $this->gthis->RMBC->getReturnString('excelOrderList')
		]);

		$res['page_config']['ttlCount'] = sizeof($orderCls->getExcelOrderList($shop_id));
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
	}

	function depositSlipList(){
		if (intval($_SESSION['right']['order']) < 30){
			return array("right"=>"Failure");
		}

        require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->lang, $this->isAdmin);
		$shop_id = $this->gthis->parm_sql("session", 'shop_id','Y');

		$filter = [
			"is_confirmed" => 'N'
		];

		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "create_datetime",
			"order" => $order ? $order : "ASC"
		];

		$res['slipList'] = $orderCls->getDepositSlipList($shop_id, $filter, $limiter, $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "depositSlipList",
			"url" => "admin/shipment/depositSlipList",
			"page_title" => $this->gthis->RMBC->getReturnString('depositSlipList')
		]);

		$res['page_config']['ttlCount'] = sizeof($orderCls->getDepositSlipList($shop_id));
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
	}

	function quickOrderPreview(){
		if (intval($_SESSION['right']['quickOrder']) < 30){
			return array("right"=>"Failure");
		}

        require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->lang, $this->isAdmin);

        require_once ROOT.'/include/class/product.class.php';
        $productCls = new product($this->DB, $this->lang, $this->isAdmin);
        
        require_once ROOT.'/include/class/member.class.php';
        $memberCls = new member($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql("session", 'shop_id','Y');
		$member_id = $this->gthis->parm_sql("request", "member_id");
		$deli_arr = $this->gthis->parm_sql("request", "deli_arr");

		$deli = json_decode(str_replace("\\", "", $deli_arr), true);

		$res['deliDtl'] = $productCls->calcPriceByDeliveryList($deli, $shop_id);
		// $res['memberDtl'] = $memberCls->getMemberDtl($member_id);
		// $res['orderDtl'] = [
		// 	"sender_name" => $this->gthis->parm_sql("request", "sender_name"),
		// 	"sender_email" => $this->gthis->parm_sql("request", "sender_email"),
		// 	"sender_phone" => $this->gthis->parm_sql("request", "sender_phone"),
		// 	"sender_company" => $this->gthis->parm_sql("request", "sender_company"),
		// 	"remarks_from_client" => $this->gthis->parm_sql("request", "remarks_from_client"),
		// 	"remarks_internal" => $this->gthis->parm_sql("request", "remarks_internal"),
		// 	"remarks_external" => $this->gthis->parm_sql("request", "remarks_external")
		// ];

		$res['deli_arr'] = $deli_arr;

		array_push($this->gthis->breadcrumb, [
			"code" => "quickOrder",
			"url" => "admin/shipment/quickOrder",
			"page_title" => $this->gthis->RMBC->getReturnString('quickOrder')
		], [
			"code" => "quickOrderPreview",
			"url" => "admin/shipment/quickOrderPreview",
			"page_title" => $this->gthis->RMBC->getReturnString('quickOrderPreview')
		]);

		return $res;
	}

	function quickOrder(){
		if (intval($_SESSION['right']['quickOrder']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/member.class.php';
        $memberCls = new member($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/user.class.php';
        $userCls = new user($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/delivery.class.php'; 
        $delivery = new delivery($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/item.class.php'; 
        $item = new item($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/product.class.php';
        $product = new product($this->DB, $this->lang, $this->isAdmin);
        
		$shop_id = $this->gthis->parm_sql("session", 'shop_id','Y');
		$admin = $this->gthis->parm_in("session", 'admin');
		
		$roleDtl = $userCls->getAdminRoleDtl($admin['role_id']);

		if ($roleDtl['product_id_arr']){
			$filter = [
				"product_id_list" => $roleDtl['product_id_arr']
			];
		} else {
			$filter = [];
		}

		$res['memberList'] = $memberCls->getMemberList($shop_id);
		$res['timeslotList'] = $delivery->getTimeslotList($shop_id);
        $res['regionList'] = $delivery->getRegionList($shop_id);
        $res['itemFullList'] = $item->getItemList();
		$res['productList'] = $product->getProductList($shop_id,$filter);

		array_push($this->gthis->breadcrumb, [
			"code" => "quickOrder",
			"url" => "admin/shipment/quickOrder",
			"page_title" => $this->gthis->RMBC->getReturnString('quickOrder')
		]);

		return $res;
	}

	function deliList(){
        if (intval($_SESSION['right']['order']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->lang, $this->isAdmin);

        require_once ROOT.'/include/class/delivery.class.php';
        $deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);
        
		require_once ROOT.'/include/class/item.class.php';
        $itemCls = new item($this->DB, $this->lang, $this->isAdmin);
        
		require_once ROOT.'/include/class/driverSchedule.class.php';
        $driverCls = new driverschedule($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "shop_id", "request" => "session"]
		]);

		$shop_id = $data['shop_id'];


		$filter = $this->gthis->paramBuilder([
			["key" => "skey"],
			["key" => "order_date_start", "type" => "date"],
			["key" => "order_date_end", "type" => "date"],
			["key" => "deli_date_start", "type" => "date"],
			["key" => "deli_date_end", "type" => "date"],
			["key" => "order_id", "type" => "int"],
			["key" => "region_id_arr", "intArr"],
			["key" => "timeslot_id_arr", "intArr"],
			["key" => "item_id_arr", "intArr"],
			["key" => "deli_status", "defaultValue" => "paid,packing,packed,scheduled,delivering,cantDeliver,acknowledged,delivered,readyToPack"],
		]);

		
		$limiter = $this->gthis->paramBuilder([
			["key" => "page", "type" => "int", "defaultValue" => 1],
			["key" => "limit", "type" => "int", "defaultValue" => 50]
		]);

		$sorting = $this->gthis->paramBuilder([
			["key" => "sort", "defaultValue" => "deli_id"],
			["key" => "order", "defaultValue" => "ASC"],
		]);

        $res['deliList'] = $orderCls->getDeliList($shop_id, $filter, $limiter, $sorting);
		$res['regionList'] = $deliveryCls->getRegionList($shop_id);
		$res['timeslotList'] = $deliveryCls->getTimeslotList($shop_id);
		$res['itemList'] = $itemCls->getItemList();
		$res['carList'] = $driverCls->getCarList();

		array_push($this->gthis->breadcrumb, [
			"code" => "deliList",
			"url" => "admin/shipment/deliList",
			"page_title" => $this->gthis->RMBC->getReturnString('deliList')
		]);

		$res['page_config']['ttlCount'] = sizeof($orderCls->getDeliList($shop_id, $filter));
		$res['page_config'] = array_merge($res['page_config'], $filter);
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);

		return $res;
	}

    function deliDtl(){
        if (intval($_SESSION['right']['order']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/delivery.class.php'; 
        $delivery = new delivery($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/item.class.php'; 
        $item = new item($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/product.class.php';
        $product = new product($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/audit.class.php';
        $audit = new audit($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/card.class.php';
        $cardCls = new card($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/user.class.php';
        $userCls = new user($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'N');
		$deli_id = $this->gthis->parm_sql('request', 'deli_id', 'N');

		$res['timeslotList'] = $delivery->getTimeslotList($shop_id);
        $res['regionList'] = $delivery->getRegionList($shop_id);
        $res['itemFullList'] = $item->getItemList();
        $res['productList'] = $product->getProductList($shop_id);
		$res['driver_list'] = $userCls->getAdminList(["driver" => 'Y']);
		$res['composer_list'] = $userCls->getAdminList();
		$res['admin_list'] = $userCls->getAdminList();

		
		$deliDtl = $orderCls->getDeliDtl($deli_id);
		$deliDtl['deli_region_lbl'] = $res['regionList'][array_search($deliDtl['deli_region_id'], array_column($res['regionList'], 'region_id'))]['name_tc'];
		$deliDtl['timeslotDtl'] = $res['timeslotList'][array_search($deliDtl['deli_timeslot_id'], array_column($res['timeslotList'], 'slot_id'))];
		if ($deliDtl['card_id'])
			$deliDtl['cardDtl'] = $cardCls->getCardDtl($deliDtl['card_id']);

		// $deliDtl['deli_region_lbl'] = $res['regionList']
		switch(date('w', strtotime($deliDtl['deli_date']))){
			case 0: $deliDtl['deli_date_weekday'] = '星期日'; break;
			case 1: $deliDtl['deli_date_weekday'] = '星期一'; break;
			case 2: $deliDtl['deli_date_weekday'] = '星期二'; break;
			case 3: $deliDtl['deli_date_weekday'] = '星期三'; break;
			case 4: $deliDtl['deli_date_weekday'] = '星期四'; break;
			case 5: $deliDtl['deli_date_weekday'] = '星期五'; break;
			case 6: $deliDtl['deli_date_weekday'] = '星期六'; break;
		}

		$res['deliDtl'] = $deliDtl;		
		$res['deliDtl']['productDtl'] = $product->getProductDtl($shop_id, $res['deliDtl']['product_id']);
		$res['deliDtl']['auditLogList'] = $audit->getAuditList("tbl_order_deli", $res['deliDtl']['deli_id']);
		
		foreach ($res['deliDtl']['auditLogList'] as $idx => $row){
			if ($row['field'] == 'deli_status'){
				$res['deliDtl']['auditLogList'][$idx]['originalDisplay'] = $orderCls->deliStatusMapping[$row['original_value']];
				$res['deliDtl']['auditLogList'][$idx]['updatedDisplay'] = $orderCls->deliStatusMapping[$row['updated_value']];
			}
		}

		$res['orderDtl'] = $orderCls->getOrderDtl($res['deliDtl']['order_id']);

		array_push($this->gthis->breadcrumb, [
			"code" => "deliList",
			"url" => "admin/shipment/deliList",
			"page_title" => $this->gthis->RMBC->getReturnString('deliList')
		],[
			"code" => "deliDtl",
			"url" => "admin/shipment/orderDtl?order_id=" . $deliDtl['order_id'],
			"page_title" => "訂單"
		],[
			"code" => "deliDtl",
			"url" => "admin/shipment/deliDtl?deli_id=" . ($deli_id ? $deli_id : "0"),
			"page_title" => $deli_id ? "運單號：".$deliDtl['deli_no'] : $this->gthis->RMBC->getReturnString('addDeli')
		]);

		return $res;
    }

	function orderList(){
        if (intval($_SESSION['right']['order']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql("session", 'shop_id','Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sort = $this->gthis->parm_sql('request', 'sort', 'N');
		$order = $this->gthis->parm_sql('request', 'order', 'N');
        $order_status = $this->gthis->parm_sql('request', 'order_status', 'N');

		if(!$order_status)
			$order_status = "paid,deliProcess,waitingPayment,autoAccept";

		$filter = [
			"skey" => $skey ? $skey : null,
			"order_status" => $order_status ? $order_status : null
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "order_datetime",
			"order" => $order ? $order : "DESC"
		];
		
        $orderList = $orderCls->getOrderList($shop_id, $filter, $limiter, $sorting);
		foreach ($orderList as $k => $order){
			$orderList[$k]['deliList'] = $orderCls->getDeliList($shop_id, [
				"order_id" => $order['order_id']
			]);
		}
		$result_array['orderList'] = $orderList;

		$result_array['page_config'] = ["skey" => $skey];
		$result_array['page_config']['ttlCount'] = sizeof($orderCls->getOrderList($shop_id, $filter));
		$result_array['page_config'] = array_merge($result_array['page_config'], $filter);
		$result_array['page_config'] = array_merge($result_array['page_config'], $limiter);
		$result_array['page_config'] = array_merge($result_array['page_config'], $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "order",
			"url" => "admin/shipment/orderList",
			"page_title" => $this->gthis->RMBC->getReturnString('orderList')
		]);
		return $result_array;
	}

    function orderDtl(){
        if (intval($_SESSION['right']['order']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/order.class.php';
		$order = new order($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/audit.class.php';
        $audit = new audit($this->DB, $this->lang, $this->isAdmin);

		$order_id = $this->gthis->parm_sql('request', 'order_id', 'N');
		$orderDtl = $order->getOrderDtl($order_id);
		if ($orderDtl){
			$orderDtl['paymentList'] = $order->getOrderPaymentList($orderDtl['order_id']);
			// $orderDtl['auditLogList'] = $audit->getAuditList("tbl_order", $orderDtl['order_id']);
			$orderDtl['auditLogList'] = $audit->getOrderAuditList($orderDtl['order_id']);
		}
		$res['orderDtl'] = $orderDtl;

		foreach ($res['orderDtl']['auditLogList'] as $idx => $row){
			if ($row['field'] == 'order_status'){
				$res['orderDtl']['auditLogList'][$idx]['originalDisplay'] = $order->orderStatusMapping[$row['original_value']];
				$res['orderDtl']['auditLogList'][$idx]['updatedDisplay'] = $order->orderStatusMapping[$row['updated_value']];
			}
		}
		
		require_once ROOT.'/include/class/user.class.php';
        $userCls = new user($this->DB, $this->lang, $this->isAdmin);
		$res['salesList'] = $userCls->getAdminList();

		array_push($this->gthis->breadcrumb, [
			"code" => "orderList",
			"url" => "admin/shipment/orderList",
			"page_title" => $this->gthis->RMBC->getReturnString('orderList')
		],[
			"code" => "orderDtl",
			"url" => "admin/shipment/orderDtl?deli_id=" . ($deli_id ? $deli_id : "0"),
			"page_title" => $order_id ? $res['orderDtl']['order_no'] : $this->gthis->RMBC->getReturnString('addOrder')
		]);

		return $res;
    }


}
?>

