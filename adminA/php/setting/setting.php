<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;
		
		switch ($this->gthis->submode) {
			case 'emailList':
				$result_array = $this->emailList();
				break;
			case 'emailDtl':
				$result_array = $this->emailDtl();
				break;
			case 'emailTplDtl':
				$result_array = $this->emailTplDtl();
				break;
			case 'greetingCateList':
				$result_array = $this->greetingCateList();
				break;
			case 'greetingCateDtl':
				$result_array = $this->greetingCateDtl();
				break;
			case 'greetingSubcateDtl':
				$result_array = $this->greetingSubcateDtl();
				break;
			case 'holidayList':
				$result_array = $this->holidayList();
				break;
			case 'holidayDtl':
				$result_array = $this->holidayDtl();
				break;
			case 'districtList':
				$result_array = $this->districtList();
				break;
			case 'districtDtl':
				$result_array = $this->districtDtl();
				break;
			case 'timeslotList':
				$result_array = $this->timeslotList();
				break;
			case 'timeslotDtl':
				$result_array = $this->timeslotDtl();
				break;
			case 'custDeliList':
				$result_array = $this->custDeliList();
				break;
			case 'custDeliDtl':
				$result_array = $this->custDeliDtl();
				break;
			case 'cardTplList':
				$result_array = $this->cardTplList();
				break;
			case 'apiDoc':
				
				$this->gthis->assign("page_only", 'Y');
				break;
			default:
				array_push($this->gthis->breadcrumb, [
					"code" => $this->gthis->submode,
					"url" => "admin/product/" . $this->gthis->submode,
					"page_title" => $this->gthis->RMBC->getReturnString($this->gthis->submode)
				]);
				break;
		}

		@$this->gthis->assign('result_array',$result_array);
	}

	function cardTplList(){
		if (!isset($_SESSION['right'])){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/card.class.php';
		$cardCls = new card($this->DB, $this->lang, $this->isAdmin);

		return $cardCls->getCardList();
	}

	function emailTplDtl(){
		if (!isset($_SESSION['right'])){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/email.class.php';
		$emailCls = new email($this->DB, $this->lang, $this->isAdmin);

		$tpl_id = $this->gthis->parm_in('request', 'tpl_id');
		$res = $emailCls->getEmailTplDtl($tpl_id);

		$this->gthis->assign('page_only', 'Y');
		$this->gthis->page = '../../templates/' .$this->gthis->shopDtl['prefix']. '/email/'. $res['adm_name'];
	}

	function emailDtl(){
		if (!isset($_SESSION['right'])){
			return array("right"=>"Failure");
		}



	}

	function emailList(){
		if (intval($_SESSION['right']['shop']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/email.class.php';
		$emailCls = new email($this->DB, $this->lang, $this->isAdmin);

		$res['emailTplList'] = $emailCls->getEmailTplList();

		return $res;
	}

	function custDeliDtl(){
		if (intval($_SESSION['right']['shop']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->lang, $this->isAdmin);

		$grp_id = $this->gthis->parm_sql('request', 'grp_id', 'N');
		$shop_id = $this->gthis->parm_in('session', 'shop_id', 'N');

		$res['grpDtl'] = $delivery->getCustDeliGrpDtl($grp_id);
		
		require_once ROOT.'/include/class/product.class.php';
        $product = new product($this->DB, $this->lang, $this->isAdmin);
		$res['productList'] = $product->getProductList($shop_id, null, ["page"=>1,"limit"=>9999], null);

		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->lang, $this->isAdmin);
		$res['regionList'] = $delivery->getRegionList($shop_id);
		$res['timeslotList'] = $delivery->getTimeslotList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "custDeliList",
			"url" => "admin/setting/custDeliList",
			"page_title" => $this->gthis->RMBC->getReturnString('custDeliList')
		],[
			"code" => "custDeliDtl",
			"url" => "admin/setting/custDeliDtl?grp_id=" . ($grp_id ? $grp_id : "0"),
			"page_title" => $grp_id ? $res['grpDtl']['adm_name'] : $this->gthis->RMBC->getReturnString('addCustDeli')
		]);
		return $res;
	}

	function custDeliList(){
		if (intval($_SESSION['right']['shop']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->lang, $this->isAdmin);
		$shop_id = $this->gthis->parm_sql('session', 'shop_id');
		$skey = $this->gthis->parm_sql('request', 'skey');
		$page = $this->gthis->parm_sql('request', 'page');
		$limit = $this->gthis->parm_sql('request', 'limit');
		$sort = $this->gthis->parm_sql('request', 'sort');
		$order = $this->gthis->parm_sql('request', 'order');

		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "grp_id",
			"order" => $order ? $order : "ASC"
		];

		$res['grpList'] = $delivery->getCustDeliGrpList($shop_id, $filter, $limiter, $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "custDeliList",
			"url" => "admin/setting/custDeliList",
			"page_title" => $this->gthis->RMBC->getReturnString('custDeliList')
		]);

		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($delivery->getCustDeliGrpList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
	}

	function greetingSubcateDtl(){
		if (intval($_SESSION['right']['shop']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/greeting.class.php';
		$greeting = new greeting($this->DB, $this->lang, $this->isAdmin);
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$cate_id = $this->gthis->parm_in('request', 'cate_id');
		$subcate_id = $this->gthis->parm_in('request', 'subcate_id');

		$res['parentCateDtl'] = $greeting->getGreetingCateDtl($shop_id, $cate_id);
		$res['subcateDtl'] = $greeting->getGreetingSubcateDtl($shop_id, $subcate_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "greetingCateList",
			"url" => "admin/setting/greetingCateList",
			"page_title" => $this->gthis->RMBC->getReturnString('greetingCateList')
		], [
			"code" => "greetingCateDtl",
			"url" => "admin/setting/greetingCateDtl?cate_id=" . ($cate_id ? $cate_id : "0"),
			"page_title" => $res['parentCateDtl']['name_tc']
		], [
			"code" => "greetingSubcateDtl",
			"url" => "admin/setting/greetingSubcateDtl?cate_id=" . ($cate_id ? $cate_id : "0"),
			"page_title" => $subcate_id ? $res['subcateDtl']['name_tc'] : $this->gthis->RMBC->getReturnString('addGreetingSubcate')
		]);
		return $res;
	}

	function greetingCateList(){
		if (intval($_SESSION['right']['shop']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/greeting.class.php';
		$greeting = new greeting($this->DB, $this->lang, $this->isAdmin);
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sort = $this->gthis->parm_sql('request', 'sort', 'N');
		$order = $this->gthis->parm_sql('request', 'order', 'N');
        
		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "cate_id",
			"order" => $order ? $order : "ASC"
		];

		$res['greetingCateList'] = $greeting->getGreetingCateList($shop_id, $filter, $limiter, $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "greetingCateList",
			"url" => "admin/setting/greetingList",
			"page_title" => $this->gthis->RMBC->getReturnString('greetingCateList')
		]);
		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($greeting->getGreetingCateList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
	}

	function greetingCateDtl(){
		if (intval($_SESSION['right']['shop']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/greeting.class.php';
		$greeting = new greeting($this->DB, $this->lang, $this->isAdmin);
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$cate_id = $this->gthis->parm_in('request', 'cate_id');

		$res['greetingCateDtl'] = $greeting->getGreetingCateDtl($shop_id, $cate_id);
		array_push($this->gthis->breadcrumb, [
			"code" => "greetingCateList",
			"url" => "admin/setting/greetingCateList",
			"page_title" => $this->gthis->RMBC->getReturnString('greetingCateList')
		], [
			"code" => "greetingCateDtl",
			"url" => "admin/setting/greetingCateDtl?cate_id=" . ($cate_id ? $cate_id : "0"),
			"page_title" => $cate_id ? $res['greetingCateDtl']['name_tc'] : $this->gthis->RMBC->getReturnString('addGreetingCate')
		]);
		return $res;
	}

	function timeslotList(){
		if (intval($_SESSION['right']['shop']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sort = $this->gthis->parm_sql('request', 'sort', 'N');
		$order = $this->gthis->parm_sql('request', 'order', 'N');
        
		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "slot_id",
			"order" => $order ? $order : "ASC"
		];

		$res['timeslotList'] = $delivery->getTimeslotList($shop_id, $filter, $limiter, $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "timeslotList",
			"url" => "admin/setting/timeslotList",
			"page_title" => $this->gthis->RMBC->getReturnString('timeslotList')
		]);
		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($delivery->getTimeslotList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
	}

	function timeslotDtl(){
		if (intval($_SESSION['right']['shop']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->lang, $this->isAdmin);

		$slot_id = $this->gthis->parm_sql('request', 'slot_id');

		$result_array['timeslotDtl'] = $delivery->getTimeslotDtl($slot_id);
		
		array_push($this->gthis->breadcrumb, [
			"code" => "timeslotList",
			"url" => "admin/setting/timeslotList",
			"page_title" => $this->gthis->RMBC->getReturnString('timeslotList')
		],[
			"code" => "timeslotDtl",
			"url" => "admin/setting/timeslotDtl?slot_id=" . ($slot_id ? $slot_id : "0"),
			"page_title" => $slot_id ? $result_array['timeslotDtl']['adm_name'] : $this->gthis->RMBC->getReturnString('addTimeslot')
		]);
		return $result_array;
	}

	function districtList(){
		if (intval($_SESSION['right']['shop']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->lang, $this->isAdmin);
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sort = $this->gthis->parm_sql('request', 'sort', 'N');
		$order = $this->gthis->parm_sql('request', 'order', 'N');
        
		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "d.district_id",
			"order" => $order ? $order : "ASC"
		];

		$res['districtList'] = $delivery->getDistrictList($shop_id, $filter, $limiter, $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "districtList",
			"url" => "admin/setting/districtList",
			"page_title" => $this->gthis->RMBC->getReturnString('districtList')
		]);
		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($delivery->getDistrictList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
	}

	function districtDtl(){
		if (intval($_SESSION['right']['shop']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->lang, $this->isAdmin);
		$district_id = $this->gthis->parm_sql('request', 'district_id');

		$res['districtDtl'] = $delivery->getDistrictDtl($district_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "districtList",
			"url" => "admin/setting/districtList",
			"page_title" => $this->gthis->RMBC->getReturnString('districtList')
		], [
			"code" => "districtDtl",
			"url" => "admin/setting/districtDtl?holiday_id=" . ($holiday_id ? $holiday_id : "0"),
			"page_title" => $district_id ? $res['districtDtl']['name_tc'] : $this->gthis->RMBC->getReturnString('addDistrict')
		]);
		return $res;
	}

	function holidayList(){
		if (intval($_SESSION['right']['shop']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/holiday.class.php';
		$holiday = new holiday($this->DB, $this->lang, $this->isAdmin);
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sort = $this->gthis->parm_sql('request', 'sort', 'N');
		$order = $this->gthis->parm_sql('request', 'order', 'N');
        
		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "c.card_id",
			"order" => $order ? $order : "ASC"
		];

		$res['holidayList'] = $holiday->getHolidayList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "holidayList",
			"url" => "admin/setting/holidayList",
			"page_title" => $this->gthis->RMBC->getReturnString('holidayList')
		]);
		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($holiday->getHolidayList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
	}

	function holidayDtl(){
		if (intval($_SESSION['right']['shop']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/holiday.class.php';
		$holiday = new holiday($this->DB, $this->lang, $this->isAdmin);
		$holiday_id = $this->gthis->parm_sql('request', 'holiday_id');
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$res['holidayDtl'] = $holiday->getHolidayDtl($holiday_id);

		require_once ROOT.'/include/class/product.class.php';
		$product = new product($this->DB, $this->lang, $this->isAdmin);
		$res['productList'] = $product->getProductList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "holidayList",
			"url" => "admin/setting/holidayList",
			"page_title" => $this->gthis->RMBC->getReturnString('holidayList')
		], [
			"code" => "holidayDtl",
			"url" => "admin/setting/holidayDtl?holiday_id=" . ($holiday_id ? $holiday_id : "0"),
			"page_title" => $holiday_id ? $res['holidayDtl']['adm_name'] : $this->gthis->RMBC->getReturnString('addholiday')
		]);
		return $res;
	}
	
}