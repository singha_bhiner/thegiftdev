<?php
/* 
    Objective: order related
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_order
        tbl_order_deli
        tbl_order_deli_addon
        tbl_order_deli_item
        tbl_order_deposit_slip
        tbl_order_excel
        tbl_order_payment
        
*/

require_once dirname(__FILE__). '/abstract.class.php';
class order extends baseClass{
    function init() {
	    $this->orderStatusMapping = [
            "incart" => "購物車中",
            "waitingPayment" => "等待收款",
            "autoAccept" => "系統自動認可入數紙",
            "paid" => "已付款",
            "confirmed" => "已核實",
            "deliProcess" => "已進入包裝及運輸流程",
            "completed" => "完成",
            "cancelled" => "取消",
            "refunded" => "退款",
            "delay" => "延遲",
            "onHold" => "暫停"
        ];

        $this->deliStatusMapping = [
            "incart" => "購物車中",
            "waitingPayment" => "等待收款",
            "autoAccept" => "系統自動認可入數紙",
            "paid" => "已付款",
            "confirmed" => "已核實",
            "packing" => "執貨中",
            "readyToPack" => "已完成執貨並等待包裝",
            "packed" => "包裝完成",
            "scheduled" => "已安排運輸",
            "delivering" => "運送中",
            "cantDeliver" => "無發送達",
            "acknowledged" => "客戶已確認",
            "delivered" => "已送達",
            "completed" => "已核實送達"
        ];
	}

    function editOrderPayment($payment_id, $data){
        // edit order payment record
        // $payment_id: PK
        // $data: the data of it 

        $sql = $this->DB->buildSql_update("tbl_order_payment", "payment_id", $payment_id, $data);
        $this->DB->update($sql);

        return null;
    }

    function createAePayment($order_id, $payment_value){
        $data = [
            "payment_status" => "waitingPayment",
            "payment_method" => "ae",
            "order_id" => $order_id,
            "expected_payment_value" => $payment_value
        ];

        $payment_id = $this->DB->insert_db("tbl_order_payment", $data);

        return $payment_id; 
    }
    
    function createPaypalPayment($order_id, $payment_value){
        // create paypal payment, default it is waitingPayment
        // $order_id: which order create the payment
        // $payment_value: how much it pay

        $data = [
            "payment_status" => "waitingPayment",
            "payment_method" => "paypal",
            "order_id" => $order_id,
            "expected_payment_value" => $payment_value
        ];

        $payment_id = $this->DB->insert_db("tbl_order_payment", $data);

        return $payment_id;
    }

    function createOfflinePayment($order_id, $payment_value){
        // create paypal payment, default it is waitingPayment
        // $order_id: which order create the payment
        // $payment_value: how much it pay

        $data = [
            "payment_status" => "waitingPayment",
            "payment_method" => "offline",
            "order_id" => $order_id,
            "expected_payment_value" => $payment_value
        ];

        $payment_id = $this->DB->insert_db("tbl_order_payment", $data);

        return $payment_id;
    }

    function getOrderPaymentList($order_id){
        // get the list
        // $order_id: which order is finding

        $sql = "SELECT * FROM `tbl_order_payment` WHERE `activate` = 'Y' AND `order_id` = '$order_id'";
        $res = $this->DB->get_Sql($sql);
        return $res;
    }

    function getOrderPaymentDtl($payment_id){
        // get the order payment Detail
        // $payment_id: PK
        
        if ($this->isAdmin)
            $selectSql = "SELECT * ";
        else
            $selectSql = "SELECT payment_id, payment_status, expected_payment_value ";

        $sql = $selectSql . "FROM `tbl_order_payment` WHERE `activate` = 'Y' AND `payment_id` = '$payment_id' ORDER BY payment_id DESC";
        $res = $this->DB->get_Sql($sql)[0];
        return $res;
    }

    function getDeliAddonList($deli_id){
        // get the addon list in the deli record 
        // $deli_id: PK
        
        $sql = "SELECT * FROM `tbl_order_deli_addon` AS a WHERE `activate` = 'Y' AND `deli_id` = '$deli_id'";
        $res = $this->DB->get_Sql($sql);
        return $res;
    }

    function getOrderDtlByMemberId($shop_id, $member_id){
        // get the order by member_id
        // $shop_id: shop in use
        // $member_id: FK 

        $sql = "SELECT order_id
                FROM `tbl_order`
                WHERE `activate` = 'Y' AND `shop_id` = '$shop_id' AND `member_id` = '$member_id' AND `order_status` = 'incart'";
        $order_id = $this->DB->get_One($sql);
        $res = $this->getOrderDtl($order_id);
        return $res;
    }

    function getExcelOrderList($shop_id, $filter = null, $limiter = null, $sorting = null){
        // get the list by excel upload
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $sql = "SELECT * 
                FROM `tbl_order_excel` 
                WHERE activate = 'Y' AND shop_id = '$shop_id'";

        if ($filter){
            foreach($filter as $k => $i){
                if ($i){
                    switch ($k){
                        case 'is_confirmed':
                            $sql .= " AND `is_confirmed` = '$i'";
                            break;
                    }
                }
            }
        }

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }
        
        $res = $this->DB->get_Sql($sql);

        return $res;
    }

    function getDepositSlipList($shop_id, $filter = null, $limiter = null, $sorting = null){
        // get the list by upload deposit slip
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $sql = "SELECT * 
                FROM `tbl_order_deposit_slip` 
                WHERE activate = 'Y' AND shop_id = '$shop_id'";

        if ($filter){
            foreach($filter as $k => $i){
                if ($i){
                    switch ($k){
                        case 'is_confirmed':
                            $sql .= "AND `is_confirmed` = '$i'";
                            break;
                        case 'slip_id':
                            $sql .= " AND `slip_id` = '$i' ";
                            break;
                    }
                }
            }
        }

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        return $res;
    }

    function editExcel($excel_id, $data){
        // edit uploaded excel (batch upload)
        // $excel_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($excel_id == 0)
            $excel_id = $this->DB->insert_db("tbl_order_excel", null);

        if ($data['excel_path'])
            $data['excel_path'] = $this->DB->uploadExcel($data['excel_path'], "excel", $excel_id);
            
        $sql = $this->DB->buildSql_update("tbl_order_excel", "excel_id", $excel_id, $data);
        $this->DB->update($sql);

        return $excel_id;
    }

    function editDepositSlip($slip_id, $data){
        // edit uploaded deposit slip
        // $slip_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($slip_id == 0)
            $slip_id = $this->DB->insert_db("tbl_order_deposit_slip", null);

        if ($data['deposit_slip'])
            $data['deposit_slip'] = $this->DB->uploadImg($data['deposit_slip'], "deposit_slip", $slip_id);
            
        $sql = $this->DB->buildSql_update("tbl_order_deposit_slip", "slip_id", $slip_id, $data);
        $this->DB->update($sql);

        return $slip_id;
    }

    function getCart($shop_id, $member_id){
        // get the user shopping cart, and create the cart when missing
        // $shop_id: shop in use
        // $member_id: FK
        $res = $this->getOrderDtlByMemberId($shop_id, $member_id);
        
        if (!$res){
            $data = [
                "shop_id" => $shop_id,
                "order_status" => "incart",
                "member_id" => $member_id
            ];
            
            $res = $this->editOrder(0, $data);
        }
        
        $this->updateOrderDeliPrice($res, $member_id); // re-calculate the order price 
        $res = $this->getOrderDtlByMemberId($shop_id, $member_id);
        return $res;
    }

    function checkOut($shop_id, $member_id, $data){
        // unused
        // checkout procedure
        // $shop_id: shop in use
        // $member_id: FK
        // $data: the data

        $sql = "SELECT order_id
                FROM `tbl_order`
                WHERE `activate` = 'Y' AND `shop_id` = '$shop_id' AND `member_id` = '$member_id' AND `order_status` = 'incart'";
        $order_id = $this->DB->get_One($sql);

        $sql = $this->DB->buildSql_update("tbl_order", "order_id", $order_id, $data);
        $this->DB->update($sql);
    }

    function createOrder($orderData, $deliData){
        // create the order by create the order and delivery record
        // $orderData: [obj] the order data
        // $deliData: [obj] the delivery record

        $deli_arr = $deliData['deli_arr'];
        unset($data['deli_arr']);
        
        $orderRes = $this->editOrder(0, $orderData);
        $order_id = $orderRes['order_id'];

        foreach($deli_arr as $k => $deli){
            $deli['order_id'] = $order_id;
            $this->editDeli(0, $deli, $orderRes['order_no']);
        }

        $res = $this->getOrderDtl($order_id);

        return $res;
    }

    function getDeliItemList($filter = null, $limiter = null, $sorting = null){
        $fromShort = "odi";
		$selectSql = "SELECT $fromShort.* ";
		$fromSql = "FROM `tbl_order_deli_item` AS $fromShort";
        $whereSql = " WHERE $fromShort.`activate` = 'Y'";
		$groupSql = " GROUP BY $fromShort.map_id";
		if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
			$whereSql = " WHERE 1=1";

		if ($sorting)
			$sortSql = " ORDER BY $sorting";
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
                switch ($k){
                    case 'map_id':
                        $whereSql .= " AND ($fromShort.`map_id` = '$i')";
                        break;
                    case 'deli_id':
                        $whereSql .= " AND ($fromShort.`deli_id` = '$i')";
                        break;
                    case 'item_id':
                        $whereSql .= " AND ($fromShort.`item_id` = '$i')";
                        break;
                    
                } 
			}
		}
		
		$sql = $selectSql . $fromSql . $whereSql . $groupSql . $sortSql  . $limitSql;
		$res = $this->DB->get_Sql($sql);

		// foreach($res as $k => $i){
		// 	$res[$k] = $this->dispatchBlog($i);
		// }

		return $res;
    }

    function editDeliItemChangeItemId($map_id_arr, $to_item_id, $qty = null){
        $sql = "UPDATE `tbl_order_deli_item` set item_id = $to_item_id, remarks = CONCAT(remarks, ' |此項目於 " . date("Y-m-d H:i:s"). " 受到批訂修改|'), packed_qty = 0 ";
        if ($qty){
            $sql .= ", qty = $qty";
        }
        $sql .= " WHERE map_id in (" . implode(",", $map_id_arr) . ")";
        $this->DB->update($sql);

        return true;
    }

    function editDeliItem($map_id, $data){
        // edit the delivery item list content
        // $map_id: PK
        // $data: the data of it 

        if ($map_id == 0)
            $map_id = $this->DB->insert_db("tbl_order_deli_item", null);

        if ($data['qty'] || $data['item_id']){
            $data['packed'] = 0;
        }
            
        $sql = $this->DB->buildSql_update("tbl_order_deli_item", "map_id", $map_id, $data);
        $this->DB->update($sql);

        return $map_id;
    }

    function editdeli($deli_id, $data, $order_no = null){
        // edit CompanyDiscount
        // $discount_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($deli_id == 0)
            $deli_id = $this->DB->insert_db('tbl_order_deli', null);    
        $existingDeliDtl = $this->getDeliDtl($deli_id);

        $itemList = $data['itemList'];

        // update deli_no when changing the deli_date
        if ($order_no && $data['deli_date']){
            if ($existingDeliDtl['deli_status'] != "incart") { // incart deli dun have the deli_no
                if (!$existingDeliDtl['deli_no'] || $existingDeliDtl['deli_date'] != $data['deli_date'])
                    $data['deli_no'] = $this->genDeliNo($order_no, $data['deli_date']);
            }
        } 
        
        $addonList = $data['addon_list'];
        if ($addonList){
            require_once ROOT.'/include/class/product.class.php'; 
		    $productCls = new product($this->DB, $this->lang, $this->isAdmin);

            foreach($addonList as $k => $addon){
                $aData = $addon;
                $aData['deli_id'] = $deli_id;
                $remarks = $aData['remarks'];
                $isAddon = $aData['isAddon'];
                $this->editOrderAddon(0, $aData);
                $product = $productCls->getProductDtl($data['shop_id'], $addon['product_id']);
                foreach($product['item_group_list'] as $item_group){
                    $item = $item_group['item_list'][0];
                    array_push($itemList, [
                        "item_id" => $item['item_id'],
                        "qty" => $item['consume'],
                        "isAddon" => $isAddon,
                        "remarks" => $remarks
                    ]);
                }
            }
        }
        if ($itemList){
            foreach ($itemList as $k => $item){
                $itemList[$k]['deli_id'] = $deli_id;
            }
            $this->DB->updateDataMap("tbl_order_deli_item", "deli_id", $deli_id, "map_id", $itemList);
        }

        if ($data['deli_before_photo_path']){
            $data['deli_before_photo_path'] = $this->DB->uploadImg($data['deli_before_photo_path'], "delivery", $deli_id);
        }

        if ($data['deli_after_photo_path']){
            $data['deli_after_photo_path'] = $this->DB->uploadImg($data['deli_after_photo_path'], "delivery", $deli_id);
        }
        
        $data['deli_id'] = $deli_id;
        $this->DB->update_Sql("tbl_order_deli", "deli_id", $data);

        return $this->getDeliDtl($deli_id);
    }

    function editOrderAddon($map_id, $data){
        // edit order addon
        // $map_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($map_id == 0)
            $map_id = $this->DB->insert_db('tbl_order_deli_addon', null);    

        $sql = $this->DB->buildSql_update("tbl_order_deli_addon", "map_id", $map_id, $data);
        $this->DB->update($sql);

        return true;
    }

    function getComposeFilter($filter){
        $fromSql = "FROM `tbl_order_deli` AS od
                    LEFT JOIN `tbl_product` AS p            ON od.product_id = p.product_id AND od.activate ='Y'
                    LEFT JOIN `tbl_order` AS o              ON o.order_id = od.order_id
                    LEFT JOIN `tmap_cate_product` AS mcp    ON mcp.product_id = p.product_id AND mcp.activate = 'Y'
                    LEFT JOIN `tbl_member` AS m             ON m.member_id = o.member_id
                    LEFT JOIN `tbl_cate` AS c               ON mcp.cate_id = c.cate_id
                    ";
        $whereSql = "WHERE od.activate = 'Y' ";

        if ($filter){
            foreach($filter as $k => $i){
                if ($i){
                    switch ($k){
                        case 'skey':
                            $whereSql .= " AND (od.deli_no LIKE '%$i%' || o.order_no LIKE '%$i%' 
                                            || p.gift_product_name_tc LIKE '%$i%' || p.gift_product_name_en LIKE '%$i%' || p.gift_product_code LIKE '%$i%'
                                            || p.zuri_product_name_tc LIKE '%$i%' || p.zuri_product_name_en LIKE '%$i%' || p.zuri_product_code LIKE '%$i%'
                                            || p.ltp_product_name_tc LIKE '%$i%' || p.ltp_product_name_en LIKE '%$i%' || p.ltp_product_code LIKE '%$i%') ";
                            break;
                        case 'deli_status':
                            $i = "'" . implode("','", explode(",", $i)   ) . "'";
                            $whereSql .= " AND (od.deli_status in ($i)) ";
                            break;
                        case 'deli_date_start':
                            if ($i == 'CURDATE()')
                                $whereSql .= " AND (od.deli_date >= $i)";
                            else 
                                $whereSql .= " AND (od.deli_date >= '$i')";
                            break;
                        case 'deli_date_end':
                            if ($i == 'CURDATE()')
                                $whereSql .= " AND (od.deli_date <= $i)";
                            else 
                                $whereSql .= " AND (od.deli_date <= '$i')";
                            break;
                    }
                }
            }
        }

        $selectSql = "SELECT p.product_id, IFNULL(p.gift_product_name_tc, IFNULL(p.zuri_product_name_tc, p.ltp_product_name_tc)) AS product_name, IFNULL(p.gift_product_code, IFNULL(p.zuri_product_code, p.ltp_product_code)) AS product_code ";
        $groupSql = " GROUP BY od.deli_id";
        $sql = "SELECT *, COUNT(*) AS counta FROM ( $selectSql $fromSql $whereSql $groupSql) AS src GROUP BY product_id";
        $res['product_list'] = $this->DB->get_Sql($sql);
        
        $selectSql = "SELECT IFNULL(c.name_tc, '沒有產品分類') AS name_tc, c.cate_id ";
        $groupSql = " ";
        $sql = "SELECT *, COUNT(*) AS counta FROM ( $selectSql $fromSql $whereSql $groupSql) AS src GROUP BY cate_id";
        $res['product_cate_list'] = $this->DB->get_Sql($sql);

        $selectSql = "SELECT m.member_no, m.name, m.member_id";
        $groupSql = " GROUP BY od.deli_id";
        $sql = "SELECT *, COUNT(*) AS counta FROM ( $selectSql $fromSql $whereSql $groupSql) AS src GROUP BY member_id";
        $res['member_list'] = $this->DB->get_Sql($sql);

        $selectSql = "SELECT od.deli_region_id, dr.name_tc AS region_name";
        $groupSql = " GROUP BY od.deli_id";
        $additionFromSql = "LEFT JOIN `tbl_deli_region` AS dr ON dr.region_id = od.deli_region_id";
        $sql = "SELECT *, COUNT(*) AS counta FROM ( $selectSql $fromSql $additionFromSql $whereSql $groupSql) AS src GROUP BY deli_region_id";
        $res['region_list'] = $this->DB->get_Sql($sql);

        $selectSql = "SELECT od.driver_admin_id AS admin_id, IFNULL(a.username, '未有分派司機') AS driver_name";
        $groupSql = " GROUP BY od.deli_id";
        $additionFromSql = "LEFT JOIN `tbl_admin` AS a ON a.admin_id = od.driver_admin_id";
        $sql = "SELECT *, COUNT(*) AS counta FROM ( $selectSql $fromSql $additionFromSql $whereSql $groupSql) AS src GROUP BY admin_id";
        $res['driver_list'] = $this->DB->get_Sql($sql);


        return $res;
    }

    function getComposeList($filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $selectSql = "SELECT od.deli_id, od.deli_no, p.product_id, IFNULL(p.gift_product_name_tc, IFNULL(p.zuri_product_name_tc, p.ltp_product_name_tc)) AS product_name, IFNULL(p.gift_product_code, IFNULL(p.zuri_product_code, p.ltp_product_code)) AS product_code, m.member_id, m.member_no, m.name, m.company_name, od.deli_status, pi.img AS product_img, remarks_packing ";
        $fromSql = "FROM `tbl_order_deli` AS od
                    LEFT JOIN `tbl_product` AS p ON od.product_id = p.product_id AND od.activate ='Y'
                    LEFT JOIN `tbl_order` AS o ON o.order_id = od.order_id
                    LEFT JOIN `tbl_product_img` AS pi ON pi.product_id = p.product_id
                    LEFT JOIN `tmap_cate_product` AS mcp ON mcp.product_id = p.product_id AND mcp.activate = 'Y'
                    LEFT JOIN `tbl_member` AS m ON m.member_id = o.member_id
                    ";
        $whereSql = "WHERE p.`activate` ='Y'";
        
  
        if ($filter){
            foreach($filter as $k => $i){
                if ($i){
                    switch ($k){
                        case 'skey':
                            $whereSql .= " AND (od.deli_no LIKE '%$i%' || o.order_no LIKE '%$i%' 
                                            || p.gift_product_name_tc LIKE '%$i%' || p.gift_product_name_en LIKE '%$i%' || p.gift_product_code LIKE '%$i%'
                                            || p.zuri_product_name_tc LIKE '%$i%' || p.zuri_product_name_en LIKE '%$i%' || p.zuri_product_code LIKE '%$i%'
                                            || p.ltp_product_name_tc LIKE '%$i%' || p.ltp_product_name_en LIKE '%$i%' || p.ltp_product_code LIKE '%$i%') ";
                            break;
                        case 'deli_status':
                            $i = "'" . implode("','", explode(",", $i)   ) . "'";
                            $whereSql .= " AND (od.deli_status in ($i)) ";
                            break;
                        case 'deli_date_start':
                            if ($i == 'CURDATE()')
                                $whereSql .= " AND (od.deli_date >= $i)";
                            else 
                                $whereSql .= " AND (od.deli_date >= '$i')";
                            break;
                        case 'deli_date_end':
                            if ($i == 'CURDATE()')
                                $whereSql .= " AND (od.deli_date <= $i)";
                            else 
                                $whereSql .= " AND (od.deli_date <= '$i')";
                            break;
                        case 'member_id':
                            $whereSql .= " AND (o.member_id = $i)";
                            break;
                        case 'cate_id':
                            $whereSql .= " AND (mcp.cate_id = $i)";
                            break;
                        case 'product_id':
                            $whereSql .= " AND (p.product_id = $i)";
                            break;
                    }
                }
            }
        }

        $sql = $selectSql . $fromSql . $whereSql;

        $sql .= " GROUP BY od.deli_id";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        $res = $this->dispatchCompose($res);

        return $res;
    }

    private function dispatchCompose($deliList){
        $deliGroupList = [];

        foreach($deliList AS $k => $deli){
            $sql = "SELECT item_id, qty, remarks
                    FROM `tbl_order_deli_item`
                    WHERE activate = 'Y' AND deli_id = " .$deli['deli_id'] . "
                    ORDER BY item_id";
            $product_content = $this->DB->get_Sql($sql);

            $isFound = false;

            for($i = 0 ; $i < sizeof($deliGroupList) && !$isFound ; $i++){
                $deliGroup = $deliGroupList[$i];
                
                if ($deliGroup['product_id'] == $deli['product_id']){
                    // check is same product

                    $isSame = true;
                    if ($deli['remarks_packing'] != $deliGroup['remarks_packing']){
                        $isSame = false;
                    }

                    for ($j = 0 ; $j < sizeof($product_content) && $isSame ; $j++){
                        if ($product_content[$j]['item_id'] != $deliGroup['product_content'][$j]['item_id'] ||
                            $product_content[$j]['qty'] != $deliGroup['product_content'][$j]['qty'] ||
                            $product_content[$j]['remarks'] != $deliGroup['product_content'][$j]['remarks']){
                            $isSame = false;
                        }
                    }

                    if ($isSame){
                        $isFound = true;
                        $deliGroupList[$i]['qty']++; 
                        $tpl = [
                            "deli_id" => $deli['deli_id'], 
                            "deli_no" => $deli['deli_no'],
                            "member_id" => $deli['member_id'],
                            "member_no" => $deli['member_no'],
                            "name" => $deli['name'],
                            "company_name" => $deli['company_name'],
                            "deli_status" => $deli['deli_status'],
                        ];
                        array_push($deliGroupList[$i]['deliList'], $tpl);
                    }
                } 
            }

            if (!$isFound){
                $tpl = [
                    "deli_id" => $deli['deli_id'], 
                    "deli_no" => $deli['deli_no'],
                    "member_id" => $deli['member_id'],
                    "member_no" => $deli['member_no'],
                    "name" => $deli['name'],
                    "company_name" => $deli['company_name'],
                    "deli_status" => $deli['deli_status'],
                ];
                array_push($deliGroupList, [
                    "qty" => 1,
                    "product_id" => $deli['product_id'],
                    "product_name" => $deli['product_name'],
                    "product_code" => $deli['product_code'],
                    "product_img" => $deli['product_img'],
                    "remarks_packing" => $deli['remarks_packing'],
                    "product_content" => $product_content,
                    "deliList" => [$tpl]
                ]);
            }
        }

        

        return $deliGroupList;
    }

    function getDeliProduct($deli_id_arr){
        $sql = "SELECT *, count(*) as counta 
                FROM (
                    SELECT p.product_id, IFNULL(p.gift_product_name_tc, IFNULL(p.zuri_product_name_tc, p.ltp_product_name_tc)) AS product_name, IFNULL(p.gift_product_code, IFNULL(p.zuri_product_code, p.ltp_product_code)) AS product_code, od.deli_status, pi.img AS product_img 
                    FROM `tbl_order_deli` AS od
                    LEFT JOIN `tbl_product` AS p ON od.product_id = p.product_id AND od.activate ='Y'
                    LEFT JOIN `tbl_product_img` AS pi ON pi.product_id = p.product_id
                    WHERE od.deli_id IN (" . implode(",",$deli_id_arr) . ")
                    GROUP BY od.deli_id
                ) as src
                GROUP BY product_id
                ";

        return $this->DB->get_Sql($sql)[0];
    }

    function getDeliList($shop_id = "1,2,3", $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $prefix = $this->getPrefix($shop_id);
        $fromShort = "d";
        $selectSql = "SELECT $fromShort.*, c.name_" . $this->lang . " AS `card_name`, c.size, c.img_path, o.order_datetime, RIGHT(deli_no,4) AS deli_no_short ";
        $fromSql = "FROM `tbl_order_deli` AS $fromShort 
                    LEFT JOIN `tbl_product` AS p ON p.product_id = $fromShort.product_id 
                    LEFT JOIN `tmap_cate_product` AS mcp ON mcp.product_id = $fromShort.product_id
                    LEFT JOIN `tbl_order` AS o ON o.order_id = $fromShort.order_id 
                    LEFT JOIN `tbl_product_card` AS c ON c.card_id = $fromShort.card_id
                    LEFT JOIN `tbl_order_deli_item` AS odi ON odi.deli_id = $fromShort.deli_id AND odi.activate = 'Y' 
                    ";
        $whereSql = "WHERE $fromShort.`activate` ='Y'";
        
        if ($shop_id){
            $whereSql .= "&& $fromShort.`shop_id` IN ($shop_id) ";
        }
        
        if ($filter){
            foreach($filter as $k => $i){
                if ($i){
                    switch ($k){
                        case 'card_only':
                            $whereSql .= " AND (card_json IS NOT NULL AND card_activate = 'Y' AND card_completed = 'N') ";
                            break;
                        case 'card_id_arr':
                            $i = "'" . implode("','", explode(",", $i)   ) . "'";
                            $whereSql .= " AND ($fromShort.card_id in ($i))";
                            break;
                        case 'skey':
                            $whereSql .= " AND ($fromShort.deli_no LIKE '%$i%' || o.order_no LIKE '%$i%' || p.$prefix"."_product_name_tc LIKE '%$i%' || p.$prefix"."_product_name_en LIKE '%$i%') ";
                            break;
                        case 'region_id_arr':
                            $i = "'" . implode("','", explode(",", $i)   ) . "'";
                            $whereSql .= " AND (deli_region_id in ($i)) ";
                            break;
                        case 'deli_status':
                            $i = "'" . implode("','", explode(",", $i)   ) . "'";
                            $whereSql .= " AND (deli_status in ($i)) ";
                            break;
                        case 'timeslot_id_arr':
                            $i = "'" . implode("','", explode(",", $i)   ) . "'";
                            $whereSql .= " AND (deli_timeslot_id in ($i)) ";
                            break;
                        case 'order_id':
                            $whereSql .= " AND ($fromShort.order_id = '$i') ";
                            break;
                        case 'deli_date_start':
                            if ($i == 'CURDATE()')
                                $whereSql .= " AND ($fromShort.deli_date >= $i)";
                            else 
                                $whereSql .= " AND ($fromShort.deli_date >= '$i')";
                            break;
                        case 'deli_date_end':
                            if ($i == 'CURDATE()')
                                $whereSql .= " AND ($fromShort.deli_date <= $i)";
                            else 
                                $whereSql .= " AND ($fromShort.deli_date <= '$i')";
                            break;
                        case 'order_date_start':
                            $whereSql .= " AND (o.order_datetime >= '$i')";
                            break;
                        case 'order_date_end':
                            $whereSql .= " AND (o.order_datetime <= '$i')";
                            break;
                        case 'item_id_arr':
                            $whereSql .= " AND (odi.item_id IN ($i))";
                            break;
                        case 'member_id':
                            $whereSql .= " AND (o.member_id = $i)";
                            break;
                        case 'cate_id':
                            $whereSql .= " AND (mcp.cate_id = $i)";
                            break;
                        case 'driver_admin_id':
                            $whereSql .= " AND ($fromShort.driver_admin_id = $i)";
                            break;
                    }
                }
            }
        }

        $sql = $selectSql . $fromSql . $whereSql;

        $sql .= " GROUP BY d.deli_id";

        if ($sorting){
            if ($sorting['sort'] == "product_code")
                $sorting['sort'] = $prefix . "_product_code";
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        foreach($res AS $k => $deli){
            $res[$k] = $this->dispatchDeli($deli);
        }
        return $res;
    }

    function dispatchDeli($deli){
        require_once ROOT.'/include/class/product.class.php'; 
		$product = new product($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/delivery.class.php'; 
		$delivery = new delivery($this->DB, $this->lang, $this->isAdmin);
        
        
        $deli['productDtl'] = $product->getProductDtl($deli['shop_id'], $deli['product_id']);
        $deli['timeslotDtl'] = $delivery->getTimeslotDtl($deli['deli_timeslot_id']);
        $deli['regionDtl'] = $delivery->getRegionDtl($deli['deli_region_id']);
        $deli['deli_statusDisplay'] = $this->deliStatusMapping[$deli['deli_status']];
        $deli['deliDtl'] = $this->getDeliDtl($deli['deli_id']);;
        $sql = "SELECT * FROM `tbl_order_deli_addon` AS a WHERE `activate` = 'Y' AND `deli_id` = '".$deli['deli_id']."'";
        $addonList = $this->DB->get_Sql($sql);
        foreach ($addonList as $kk => $addon){
            $addonList[$kk]['productDtl'] = $product->getProductDtl($deli['shop_id'], $addon['product_id']);
        }
        $deli['addonList'] = $addonList;

        return $deli;
    }

    function getDeliDtl($deli_id){
        // get the companyDisocunt Detail
        // $deli_id: PK

        require_once ROOT.'/include/class/product.class.php'; 
		$product = new product($this->DB, $this->lang, $this->isAdmin);

        $sql = "SELECT h.*, c.name_" . $this->lang . " AS `card_name`, c.size, c.img_path, a.username AS driver_name, ia.username AS incharge_name
                FROM `tbl_order_deli` AS h
                LEFT JOIN `tbl_admin` AS a ON a.admin_id = h.driver_admin_id
                LEFT JOIN `tbl_admin` AS ia ON ia.admin_id = h.incharge_admin_id
                LEFT JOIN `tbl_product_card` AS c ON c.card_id = h.card_id 
                WHERE h.`activate` = 'Y' && h.`deli_id` = '$deli_id'";

        $res = $this->DB->get_Sql($sql)[0];

        if ($res){
            $sql = "SELECT d.*, i.`inhouse_name_tc` AS `name`, i.`alias` AS `alias`
                    FROM `tbl_order_deli_item` AS d
                    LEFT JOIN `tbl_item` AS i ON i.`item_id` = d.`item_id`
                    WHERE d.`activate` = 'Y' AND d.deli_id = '$deli_id';
            ";

            $res['itemList'] = $this->DB->get_Sql($sql);

            // $sql = "SELECT d.*, i.`inhouse_name_tc` AS `name`, s.sku_val, i.item_id, s.auto, i.item_img, s.auto, s.alias
            //         FROM `tbl_order_deli_item` AS d
            //         LEFT JOIN `tbl_sku` AS s ON s.sku_id = d.sku_id AND s.activate = 'Y'
            //         LEFT JOIN `tbl_item` AS i ON i.item_id = s.item_id AND i.activate = 'Y'
            //         LEFT JOIN `tbl_product_item_content` AS pic ON pic.product_id = '".$res['product_id']."' AND pic.type = 'grp_item' 
            //         LEFT JOIN `tbl_item_grp_content` AS igc ON pic.`ref_id`= igc.grp_id AND d.`sku_id` = igc.`sku_id`
            //         WHERE d.activate = 'Y' AND d.deli_id = ". $res['deli_id'] ."
            //         GROUP BY map_id";
            
            // $res['itemList'] = $this->DB->get_Sql($sql);
            
            $res['deli_statusDisplay'] = $this->deliStatusMapping[$res['deli_status']];
    
            $sql = "SELECT * FROM `tbl_order_deli_addon` AS a WHERE `activate` = 'Y' AND `deli_id` = '".$res['deli_id']."'";
            $addonList = $this->DB->get_Sql($sql);
            foreach ($addonList as $k => $addon){
                $addonList[$k]['productDtl'] = $product->getProductDtl($res['shop_id'],$addon['product_id']);
            }
            $res['addonList'] = $addonList;
        }

        return $res;
    }

    function editOrder($order_id, $data){
        // edit order
        // $order_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($order_id == 0){
            $order_id = $this->DB->insert_db('tbl_order', null);
        }

        $sql = $this->DB->buildSql_update("tbl_order", "order_id", $order_id, $data);
        $this->DB->update($sql);
        return $this->getOrderDtl($order_id);
    }

    function genOrderNo($shop_id){
        // generate order no
        // $shop_id: shop in use

        // YYMMDD-[公司代碼][4位隨機字元] 
        switch($shop_id){
            case 1:
                $key = "G";
                break;
            case 2:
                $key = "Z";
                break;
            case 3:
                $key = "L";
                break;
            default:
                $key = "";
                break;
        }        
        
        $order_no = date("ymd"). "-$key" . $this->random_str(4);

        // check if the order_no existed, if existed re-gen
        $sql = "SELECT * 
                FROM `tbl_order` AS o 
                WHERE o.`order_no` = '$order_no'";
        $res = $this->DB->get_One($sql);

        if ($res)
            $order_no = $this->genOrderNo($shop_id);

        return $order_no;
    }

    
    function genDeliNo($order_no, $deli_date){
        // generate order delivery no
        // $order_no: which order_no
        // $deli_date: the delivery date

        if (!$deli_date){
           return null; 
        }
        //[該訂單編號]-YYMM-[4位數字]
        $sql = "SELECT max(deli_no) AS `max` FROM `tbl_order_deli` WHERE MONTH(deli_date) = MONTH('$deli_date') AND YEAR(deli_date) = YEAR('$deli_date')";
        $max = $this->DB->get_One($sql);
        if ($max){
            preg_match('/(.*)-(.*)-(.*)-(.*)/', $max, $output_array);
            $count = ltrim($output_array[4], '0');
            $count++;
        } else {
            $count = 1;
        }

        $thatDate = strtotime($deli_date);

        $deli_no = $order_no . "-" . date("ym", $thatDate) . "-" . str_pad($count, 4, "0", STR_PAD_LEFT);
        
        return $deli_no;
    }

    function getOrderList($shop_id, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $sql = "SELECT * 
                FROM `tbl_order` 
                WHERE `activate` ='Y' && `shop_id` IN ($shop_id)";

        if ($filter){
            foreach($filter as $k => $i){
                if ($i){
                    switch($k){
                        case 'member_id':
                            $sql .= " && (member_id = '$i' && order_status NOT IN ('incart'))";
                            break;
                        case 'order_no':
                            $sql .= " && (order_no = '$i')";
                            break;
                        case 'skey':
                            $sql .= " && (sender_name LIKE '%$i%' || sender_phone LIKE '%$i%' || order_no LIKE '%$i%')";
                            break;
                        case 'order_status':
                            $i = "'" . implode("','", explode(",", $i)   ) . "'";
                            $sql .= " && (order_status in ($i))";
                            break;
                        case 'top30':
                            $sql = "SELECT * FROM (
                                SELECT p.product_id, p.gift_product_name_tc as product_name_tc, count(p.product_id) AS counter 
                                FROM `tbl_order` AS o 
                                LEFT JOIN `tbl_order_deli` AS d ON d.order_id = o.order_id AND d.`activate` = 'Y' 
                                LEFT JOIN `tbl_product` AS p ON d.product_id = p.product_id AND p.`activate` = 'Y' 
                                WHERE o.`activate` = 'Y' AND o.`shop_id` IN ($shop_id) AND p.product_id IS NOT NULL AND o.create_datetime > CURDATE() - INTERVAL 30 DAY AND o.order_status IN ('paid','deliProcess')
                                GROUP BY p.product_id
                                ) AS src ";
                            break;
                    }
                }
            }
        }

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        foreach ($res AS $k => $row){
            $res[$k]['order_statusDisplay'] = $this->orderStatusMapping[$row['order_status']];
        }

        return $res;
    }

    function getOrderDtl($order_id){
        // get the order Detail
        // $discount_id: PK
        $sql = "SELECT h.*, m.name, m.member_no, a.username as handle_admin_username, m.is_monthly_payment
                FROM `tbl_order` AS h
                LEFT JOIN `tbl_member` AS m ON h.member_id = m.member_id
                LEFT JOIN `tbl_admin` AS a ON a.admin_id = h.handle_admin_id
                WHERE h.`activate` = 'Y' && h.`order_id` = '$order_id'";
                
        $res = $this->DB->get_Sql($sql)[0];
        if ($res){
            $res['order_statusDisplay'] = $this->orderStatusMapping[$res['order_status']];
            $res['deliList'] = $this->getDeliList($res['shop_id'], ["order_id"=>$order_id]);

            $sql = "SELECT * 
                    FROM `tbl_order_deposit_slip` 
                    WHERE `order_id` = '$order_id' AND activate ='Y'";
            $res['slipList'] = $this->DB->get_Sql($sql);
        }
        return $res;
    }

    function getOrderDtlByOrderNo($shop_id, $order_no){
        if ($shop_id && $order_no){
            $order = $this->getOrderList($shop_id, ["order_no" => $order_no],$this->DB->getUnique,null)[0];
            return $this->getOrderDtl($order['order_id']);
        } else {
            return null;
        }
    }

    function getOrderStatByDay($shop_id, $day){
        // get the order record by period (day)
        // $shop_id: shop in use
        // $day: [int] day

        $sql = "SELECT DATE_FORMAT(order_datetime, '%Y-%m-%d') AS date, count(order_datetime) AS count
                FROM `tbl_order` AS o
                WHERE o.activate = 'Y' AND order_datetime > DATE_ADD(CURRENT_DATE, INTERVAL -$day DAY) AND shop_id = '$shop_id'
                GROUP BY DATE_FORMAT(order_datetime, '%Y-%m-%d')";
        $orderRes = $this->DB->get_Sql($sql);
        $orderArr = [];
        // restructure
        foreach($orderRes as $order){
            $orderArr[$order['date']] = $order['count'];
        }
        
        $lblArr = [];
        $valArr = [];
        for($i = 0; $i < $day; $i++){
            $date = date('Y-m-d', strtotime("-" . ($day - $i-1) ."days"));
            array_push($lblArr, $date);
            array_push($valArr, ($orderArr[$date] ? $orderArr[$date] : 0));
            
        }

        $res = [
            "lbl" => $lblArr,
            "val" => $valArr
        ];

        return $res;
    }

    function getOrderStatByMonth($shop_id){
        // get the order record by 1 month
        // $shop_id: shop in use

        $sql = "SELECT DATE_FORMAT(order_datetime, '%Y-%m-%d') AS date, count(order_datetime) AS count
                FROM `tbl_order` AS o
                WHERE o.activate = 'Y' AND MONTH(order_datetime) = MONTH(CURDATE()) AND shop_id = '$shop_id'
                GROUP BY DATE_FORMAT(order_datetime, '%Y-%m-%d')";
        $orderRes = $this->DB->get_Sql($sql);

        $orderArr = [];
        // restructure
        foreach($orderRes as $order){
            $orderArr[$order['date']] = $order['count'];
        }

        $lblArr = [];
        $valArr = [];

        for($i = 1; $i <= 31; $i++){
            $date =  date('Y-m-d', strtotime(date('Y-m-'.$i)));
            if ($i == 1){
                $cMonth = date('m', strtotime($date));
            } 

            if ($cMonth == date('m', strtotime($date))){
                array_push($lblArr, $date);
                array_push($valArr, ($orderArr[$date] ? $orderArr[$date] : 0));
            }

        }

        $res = [
            "lbl" => $lblArr,
            "val" => $valArr
        ];
        
        return $res;
    }

    function updateOrderDeliPrice($order, $member_id = null, $coupon_code = null, $member_voucher_id = null, $cash_dollar = null){
        // update the delivery price
        // $order: [obj] order detail
        // $member_id: FK
        // $coupon_code: tbl_coupon.code
        // $member_voucher_id: FK
        // $cash_dollar: [int] cash dollar used
        
        require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/shop.class.php';
		$shopCls = new shop($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->lang, $this->isAdmin);
        $memberDtl = $memberCls->getMemberDtl($member_id);

        $shop_id = $order['shop_id'];
        $shopDtl = $shopCls->getShopDtl($shop_id);
        
		$cate_arr = [];
		$product_arr = [];
		$product_price = 0;
		$shipping_fee = 0;
		$timeslot_fee = 0;
		$final_price = 0;
        $membership_discount = 0;
		
		$orderData = [];
		$deliDataArr = [];
		foreach($order['deliList'] as $k => $deli){

			$deliData = [
				"shop_id" => $shop_id
			];
			$deli_price = 0;

            // update the delivery records and re-calculate the latest price			
			$productDtl = $productCls->getProductDtl($shop_id, $deli['product_id']);
			
            $regionDtl = $deliveryCls->getCustDeliGrpRegionDtl($deli['productDtl']['deli_id'], $deli['deli_region_id']);
            !$regionDtl ? $regionDtl = $deliveryCls->getRegionDtl($deli['deli_region_id']) : null;

            $timeslotDtl = $deliveryCls->getCustDeliGrpTimeslotDtl($deli['productDtl']['deli_id'], $deli['deli_region_id'], $deli['deli_timeslot_id']);
            !$timeslotDtl ? $timeslotDtl = $deliveryCls->getTimeslotDtl($deli['deli_timeslot_id']) : null;
            
			$dp_price = $productDtl['sp_price'] ? $productDtl['sp_price']: $productDtl['price'];
			$dp_shipping_fee = $regionDtl['shipping_fee'] ? $regionDtl['shipping_fee'] : 0;
			$dp_timeslot_fee = $timeslotDtl['deli_fee'] ? $timeslotDtl['deli_fee'] : 0;

            // check same day
            if ($deli['deli_date'] == date("Y-m-d")){
                if ($timeslotDtl['same_day_deli_fee'])
                    $dp_timeslot_fee += $timeslotDtl['same_day_deli_fee'];
            }

			$itemList = $deli['deliDtl']['itemList'];

            // check if it is additional price for the selected sku (upgrade product)
            foreach ($productDtl['item_group_list'] as $k => $item_group){
                $sel = $itemList[$k]['item_id'];
                if ($item_group['item_list'] && $sel){
                    foreach($item_group['item_list'] as $item){
                        if ($item['item_id'] == $sel){
                            $dp_price += $item['additional_price'];
                        }
                    }
                }
            }

			
			// add additional product price in the product price
			$addon_list = $this->getDeliAddonList($deli['deli_id']);
			$dp_addon_price = 0;
			if ($addon_list){
				foreach ($addon_list as $addon){
					$productDtl = $productCls->getProductDtl($shop_id, $addon['product_id']);
					$dp_addon_price += ($productDtl['sp_price'] ? $productDtl['sp_price'] : $productDtl['price']) * $addon['qty'];
				}
			}

			$deliData['product_price'] = $dp_price;
			$deliData['shipping_fee'] = $dp_shipping_fee;
			$deliData['timeslot_fee'] = $dp_timeslot_fee;
			$deliData['addon_price'] = $dp_addon_price;

			$deli_price = $dp_price + $dp_shipping_fee + $dp_timeslot_fee + $dp_addon_price;
			$deliData['deli_price'] = $deli_price;
			
			
			$final_price += $deli_price;
			$product_price += $dp_price + $dp_addon_price;
			$shipping_fee += $dp_shipping_fee;
			$timeslot_fee += $dp_timeslot_fee;

			array_push($product_arr, $deli['product_id']);
			foreach($productDtl['cate'] as $cate){
				foreach($cate['subcate'] as $subcate){
					array_push($cate_arr, $subcate['cate_id']);
				}
			}

			array_push($deliDataArr, [
				"deli_id" => $deli['deli_id'],
				"deliData" => $deliData
			]);
		}
		
		if ($coupon_code){
			require_once ROOT.'/include/class/coupon.class.php';
			$couponCls = new coupon($this->DB, $this->gthis->lang, $this->isAdmin);
			$couponDtl = $couponCls->getCouponDtlByCode($coupon_code, $product_price, $cate_arr, $product_arr);
			if ($couponDtl['sales_admin_id'])
				$orderData['sales_admin_id'] = $couponDtl['sales_admin_id'];
                
			$orderData['coupon_code_id'] = $couponDtl['coupon_id'];
			$coupon_discount = $couponDtl['act_discount'];
			$orderData['coupon_discount'] = $coupon_discount;
			$final_price -= $couponDtl['act_discount'];
		}

		if (!$coupon_code || ($couponDtl && $couponDtl['allow_voucher'] == 'Y' && ($couponDtl['member_only'] != 'Y' || $member_id)) ){
			// case 1: without coupon code
			// case 2: coupon && coupon allow voucher && not member only
			// case 3: coupon && coupon allow voucher && member only && member
			
			if ($member_voucher_id){
				require_once ROOT.'/include/class/voucher.class.php';
				$voucherCls = new voucher($this->DB, $this->gthis->lang, $this->isAdmin);
				$voucherDtl = $voucherCls->getVoucherDtlByMemberVoucher($member_voucher_id, $product_price, $cate_arr);
				$voucher_discount = $voucherDtl['act_discount'];
				$orderData['voucher_discount'] = $voucher_discount;
				$final_price -= $voucherDtl['act_discount'];

			}
		}

		if ($final_price < 0)
			$final_price = 0;

        if ($memberDtl){
            if ($memberDtl['membership_discount'] > 0){
                $membership_discount = round($product_price * $memberDtl['membership_discount'] / 100);
                $final_price -= $membership_discount;
            }
        }

		if ($cash_dollar && $memberDtl){
			if ($cash_dollar > $memberDtl['cash_dollar'])
				$cash_dollar = $memberDtl['cash_dollar'];

			if ($case_dollar > $final_price)
				$cash_dollar = $final_price;

			$orderData['cash_dollar_used'] = $cash_dollar;
			$final_price -= $cash_dollar;
		}

		// $order_no = $this->genOrderNo($shop_id);

		$orderData = [
			// "order_status" => "waitingPayment",
			// "order_no" => $order_no,
			"product_price" => $product_price,
			"shipping_fee" => $shipping_fee,
			"timeslot_fee" => $timeslot_fee,
			"coupon_discount" => $coupon_discount,
			"voucher_discount" => $voucher_discount,
			"membership_discount" => $membership_discount,
			"cash_dollar_used" => $cash_dollar,
            "cash_dollar_gain" => ($product_price && $shopDtl['point_ratio']) ? round($product_price / $shopDtl['point_ratio']) : 0,
            "point_gain" => $product_price,
            "exp_gain" => $product_price,
			"final_price" => $final_price,
			// "order_datetime" => date("Y-m-d H:i:s")
		];

		$this->editOrder($order['order_id'], $orderData);
		foreach($deliDataArr as $deli){
			$deliDtl = $this->getDeliDtl($deli['deli_id']);
			$deliData = $deli['deliData'];
			
			$this->editDeli($deli['deli_id'], $deliData);
		}
    }
}
?>
