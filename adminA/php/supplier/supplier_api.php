<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;
		
		require_once ROOT. '/include/apiDataBuilder.class.php';

		switch ($this->gthis->thirdmode) {
			case 'getSupplierList':
                $this->getSupplierList();
                break;
            case 'getSupplierDtl':
                $this->getSupplierDtl();
                break;
            case 'editSupplier':
                $this->editSupplier();
                break; 

            case 'editSupplierTag':
                $this->editSupplierTag();
                break;
            case 'getSupplierTagList':
                $this->getSupplierTagList();
                break;
			default:
				new apiDataBuilder(-84);
				break;
		}

		$result_array['lang'] = $this->lang;
		$this->gthis->assign('result_array',$result_array);
	}

    function getSupplierList() {
        require_once ROOT. '/include/class/supplier.class.php';
        $supplierCls = new supplier($this->DB, $this->lang, $this->isAdmin);
        
        $res['supplier_list'] = $supplierCls->getSupplierList(null, null, ["sort" => "name_tc ASC"]);
        
        new apiDataBuilder(0, $res);
    }

    function getSupplierDtl(){
        require_once ROOT. '/include/class/supplier.class.php';
        $supplierCls = new supplier($this->DB, $this->lang, $this->isAdmin);

        $data = $this->gthis->paramBuilder([
            ["key" => "supplier_id", "type" => "int", "compulsory" => true, "allowBlank" => false],
        ]);

        $res['supplier'] = $supplierCls->getSupplierDtl($data['supplier_id']);

        new apiDataBuilder(0, $res);
    }

    function editSupplier(){
        require_once ROOT. '/include/class/supplier.class.php';
        $supplierCls = new supplier($this->DB, $this->lang, $this->isAdmin);

        $data = $this->gthis->paramBuilder([
            ["key" => "supplier_id", "type" => "int", "compulsory" => true, "allowBlank" => false],
            ["key" => "name_tc"],
            ["key" => "name_en"],
            ["key" => "short_form"],
            ["key" => "address"],
            ["key" => "phone"],
            ["key" => "email"],
            ["key" => "remarks"],
            ["key" => "MOQ"],
            ["key" => "fax"],
            ["key" => "payment_method"],
            ["key" => "payment_detail"],
            ["key" => "contact_json", "type" => "json"],
            ["key" => "tag_id_arr", "type" => "intArr"],
            ["key" => "activate", "type" => "NY"]
        ]);

        

        $res['supplier'] = $supplierCls->editSupplier($data);

        new apiDataBuilder(0, $res);
    }

    function editSupplierTag(){
        require_once ROOT. '/include/class/supplier.class.php';
        $supplierCls = new supplier($this->DB, $this->lang, $this->isAdmin);

        $data = $this->gthis->paramBuilder([
            ["key" => "tag_id", "type" => "int", "compulsory" => true, "allowBlank" => false],
            ["key" => "parent_tag_id", "type" => "int", "allowBlank" => false, "allowZero" => false],
            ["key" => "name_tc"],
            ["key" => "name_en"],
            ["key" => "activate", "type" => "NY"]
        ]);

        $res['supplier_tag'] = $supplierCls->editSupplierTag($data);

        new apiDataBuilder(0, $res);
    }

    function getSupplierTagList(){
        require_once ROOT. '/include/class/supplier.class.php';
        $supplierCls = new supplier($this->DB, $this->lang, $this->isAdmin);

        $res['supplier_tag_list'] = $supplierCls->getSupplierTagList(null, null, ["sort" => "sort"]);

        new apiDataBuilder(0, $res);
    }
}
?> 