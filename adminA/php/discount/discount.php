<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;

		switch ($this->gthis->submode) {
            case 'voucherList':
                $result_array = $this->voucherList();
                break;
			case 'voucherDtl':
				$result_array = $this->voucherDtl();
				break;
			case 'couponList':
				$result_array = $this->couponList();
				break;
			case 'couponDtl':
				$result_array = $this->couponDtl();
				break;
			case 'pointRatio':
				$result_array = $this->pointRatio();
				break;
			case 'companyDiscountList':
				$result_array = $this->companyDiscountList();
				break;
			case 'companyDiscountDtl':
				$result_array = $this->companyDiscountDtl();
				break;
            case 'giftList':
                $result_array = $this->giftList();
                break;
			case 'giftDeliList':
				$result_array = $this->giftDeliList();
				break;
            case 'giftDtl':
                $result_array = $this->giftDtl();
                break;
			case 'giftCardList':
				$result_array = $this->giftCardList();
				break;
			case 'giftCardDtl':
				$result_array = $this->giftCardDtl();
				break;
			case 'newMember':
				$result_array = $this->newMember();
				break;
			default:
				$result_array = null;
				break;
		}

		$this->gthis->assign('result_array',$result_array);
	}

	function giftDeliList(){
		if (intval($_SESSION['right']['discount']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/gift.class.php';
        $gift = new gift($this->DB, $this->lang, $this->isAdmin);
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sort = $this->gthis->parm_sql('request', 'sort', 'N');
		$deli_status = $this->gthis->parm_sql('request', 'deli_status', 'N');

		if(!$deli_status)
			$deli_status = "requested";
        
		$filter = [
			"skey" => $skey ? $skey : null,
			"deli_status" => $deli_status ? $deli_status : null
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "deli_id",
			"order" => $order ? $order : "DESC"
		];
		
		$res['skey'] = $filter['skey'];
		$res['page'] = $limiter['page'];
		$res['limit'] = $limiter['limit'];
		$res['giftDeliList'] = $gift->getGiftDeliList($filter, $limiter, $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "giftDeliList",
			"url" => "admin/discount/giftDeliList",
			"page_title" => $this->gthis->RMBC->getReturnString('giftDeliList')
		]);

		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($gift->getGiftDeliList($filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $filter);
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
	}

	function newMember(){
        if (intval($_SESSION['right']['discount']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/shop.class.php';
        $shop = new shop($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/voucher.class.php';
        $voucher = new voucher($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql("session", 'shop_id','Y');

		$result_array['shopDtl'] = $shop->getShopDtl($shop_id);
		$result_array['voucherList'] = $voucher->getVoucherList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "newMember",
			"url" => "admin/discount/newMember",
			"page_title" => $this->gthis->RMBC->getReturnString('newMember')
		]);
		return $result_array;
	}

    function giftCardList(){
        if (intval($_SESSION['right']['discount']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/giftCard.class.php';
        $gift = new giftCard($this->DB, $this->lang, $this->isAdmin);
        $shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sort = $this->gthis->parm_sql('request', 'sort', 'N');
		$order = $this->gthis->parm_sql('request', 'order', 'N');
        
		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "c.card_id",
			"order" => $order ? $order : "ASC"
		];
		
		$res['skey'] = $filter['skey'];
		$res['page'] = $limiter['page'];
		$res['limit'] = $limiter['limit'];
		$res['cardList'] = $gift->getGiftCardList($shop_id, $filter, $limiter, $sorting);


		array_push($this->gthis->breadcrumb, [
			"code" => "giftCardList",
			"url" => "admin/discount/giftCardList",
			"page_title" => $this->gthis->RMBC->getReturnString('giftCardList')
		]);

		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($gift->getGiftCardList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
    }

    function giftCardDtl(){
        if (intval($_SESSION['right']['discount']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/giftCard.class.php';
        $gift = new giftCard($this->DB, $this->lang, $this->isAdmin);

		$card_id = $this->gthis->parm_sql('request', 'card_id', 'N');

		$res['cardDtl'] = $gift->getGiftCardDtl($card_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "giftCardList",
			"url" => "admin/discount/giftCardList",
			"page_title" => $this->gthis->RMBC->getReturnString('giftCardList')
		],[
			"code" => "giftCardDtl",
			"url" => "admin/discount/giftCardDtl?card_id=" . ($card_id ? $card_id : "0"),
			"page_title" => $card_id ? $res['cardDtl']['name_tc'] : $this->gthis->RMBC->getReturnString('addGiftCard')
		]);
		return $res;
    }

    function giftList(){
        if (intval($_SESSION['right']['discount']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/gift.class.php';
        $gift = new gift($this->DB, $this->lang, $this->isAdmin);
        $shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sort = $this->gthis->parm_sql('request', 'sort', 'N');
		$order = $this->gthis->parm_sql('request', 'order', 'N');
        
		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "gift_id",
			"order" => $order ? $order : "ASC"
		];
		
		$res['skey'] = $filter['skey'];
		$res['page'] = $limiter['page'];
		$res['limit'] = $limiter['limit'];
		$res['giftList'] = $gift->getGiftList($shop_id, $filter, $limiter, $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "giftList",
			"url" => "admin/discount/giftList",
			"page_title" => $this->gthis->RMBC->getReturnString('giftList')
		]);

		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($gift->getGiftList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
    }

    function giftDtl(){
        if (intval($_SESSION['right']['discount']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/gift.class.php';
        $gift = new gift($this->DB, $this->lang, $this->isAdmin);

		$gift_id = $this->gthis->parm_sql('request', 'gift_id', 'N');

		$res['giftDtl'] = $gift->getGiftDtl($gift_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "giftList",
			"url" => "admin/discount/giftList",
			"page_title" => $this->gthis->RMBC->getReturnString('giftList')
		],[
			"code" => "giftDtl",
			"url" => "admin/discount/giftDtl?gift_id=" . ($card_id ? $card_id : "0"),
			"page_title" => $gift_id ? $res['giftDtl']['name_tc'] : $this->gthis->RMBC->getReturnString('addGift')
		]);
		return $res;
    }

	function companyDiscountList(){
		if (intval($_SESSION['right']['discount']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/companyDiscount.class.php';
        $discount = new companyDiscount($this->DB, $this->lang, $this->isAdmin);
        $shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sort = $this->gthis->parm_sql('request', 'sort', 'N');
		$order = $this->gthis->parm_sql('request', 'order', 'N');
        
		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "discount_id",
			"order" => $order ? $order : "ASC"
		];
		
		$res['skey'] = $filter['skey'];
		$res['page'] = $limiter['page'];
		$res['limit'] = $limiter['limit'];
		$res['companyDiscountList'] = $discount->getCompanyDiscountList($shop_id, $filter, $limiter, $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "companyDiscountList",
			"url" => "admin/discount/companyDiscountList",
			"page_title" => $this->gthis->RMBC->getReturnString('companyDiscountList')
		]);

		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($discount->getCompanyDiscountList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
	}

	function companyDiscountDtl(){
		if (intval($_SESSION['right']['discount']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/companyDiscount.class.php';
        $discount = new companyDiscount($this->DB, $this->lang, $this->isAdmin);
		

		$discount_id = $this->gthis->parm_sql('request', 'discount_id', 'N');

		$res['companyDiscountDtl'] = $discount->getCompanyDiscountDtl($discount_id);
		$applicantList = $discount->getCompanyDiscountApplicantList($discount_id);
		if ($applicantList){
			require_once ROOT.'/include/class/member.class.php';
        	$memberCls = new member($this->DB, $this->lang, $this->isAdmin);
			require_once ROOT.'/include/class/voucher.class.php';
			foreach ($applicantList as $k => $applicant){
				$applicantList[$k]['memberDtl'] = $memberCls->getMemberDtl($applicant['member_id']);
			}

			$res['companyDiscountDtl']['applicantList'] = $applicantList;
		}



		array_push($this->gthis->breadcrumb, [
			"code" => "companyDiscountList",
			"url" => "admin/discount/companyDiscountList",
			"page_title" => $this->gthis->RMBC->getReturnString('companyDiscountList')
		],[
			"code" => "companyDiscountDtl",
			"url" => "admin/discount/companyDiscountDtl?discount_id=" . ($discount_id ? $discount_id : "0"),
			"page_title" => $discount_id ? $res['companyDiscountDtl']['company_name'] : $this->gthis->RMBC->getReturnString('addCompanyDiscount')
		]);
		return $res;
	}

	function pointRatio(){
		if (intval($_SESSION['right']['discount']) < 30){
			return array("right"=>"Failure");
		}
		
		require_once ROOT.'/include/class/shop.class.php';
		$shop = new shop($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'N');

		$res['shopDtl'] = $shop->getShopDtl($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "pointRatio",
			"url" => "admin/discount/pointRatio",
			"page_title" => $this->gthis->RMBC->getReturnString('pointRatio')
		]);
		return $res;
	}

	function couponList(){
		if (intval($_SESSION['right']['discount']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/coupon.class.php';
        $coupon = new coupon($this->DB, $this->lang, $this->isAdmin);
        $shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sort = $this->gthis->parm_sql('request', 'sort', 'N');
		$order = $this->gthis->parm_sql('request', 'order', 'N');
        
		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "coupon_id",
			"order" => $order ? $order : "ASC"
		];

		
		$res['skey'] = $filter['skey'];
		$res['page'] = $limiter['page'];
		$res['limit'] = $limiter['limit'];
		$res['couponList'] = $coupon->getCouponList($shop_id, $filter, $limiter, $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "couponList",
			"url" => "admin/discount/couponList",
			"page_title" => $this->gthis->RMBC->getReturnString('couponList')
		]);

		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($coupon->getCouponList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
	}

	function couponDtl(){
		if (intval($_SESSION['right']['discount']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/coupon.class.php';
        $coupon = new coupon($this->DB, $this->lang, $this->isAdmin);

		$coupon_id = $this->gthis->parm_sql('request', 'coupon_id', 'N');
		$shop_id = $this->gthis->parm_in('session', 'shop_id');

		$res['couponDtl'] = $coupon->getCouponDtl($coupon_id);

		require_once ROOT.'/include/class/user.class.php';
        $userCls = new user($this->DB, $this->lang, $this->isAdmin);
		$res['salesList'] = $userCls->getAdminList();

		require_once ROOT.'/include/class/product.class.php';
        $product = new product($this->DB, $this->lang, $this->isAdmin);
		$res['productList'] = $product->getProductList($shop_id);
		
		require_once ROOT.'/include/class/cate.class.php';
        $cateCls = new cate($this->DB, $this->lang, $this->isAdmin);
		$res['cateList'] = $cateCls->getCateList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "couponList",
			"url" => "admin/discount/couponList",
			"page_title" => $this->gthis->RMBC->getReturnString('couponList')
		],[
			"code" => "couponDtl",
			"url" => "admin/discount/couponDtl?coupon_id=" . ($coupon_id ? $coupon_id : "0"),
			"page_title" => $coupon_id ? $res['couponDtl']['adm_name'] : $this->gthis->RMBC->getReturnString('addCoupon')
		]);
		return $res;
	} 
	
	function voucherList(){
		if (intval($_SESSION['right']['discount']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/voucher.class.php';
        $voucher = new voucher($this->DB, $this->lang, $this->isAdmin);

        $shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sort = $this->gthis->parm_sql('request', 'sort', 'N');
		$order = $this->gthis->parm_sql('request', 'order', 'N');
        
		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "voucher_id",
			"order" => $order ? $order : "ASC"
		];

		$res['voucherList'] = $voucher->getVoucherList($shop_id, $filter, $limiter);
		array_push($this->gthis->breadcrumb, [
			"code" => "voucherList",
			"url" => "admin/discount/voucherList",
			"page_title" => $this->gthis->RMBC->getReturnString('voucherList')
		]);

		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($voucher->getVoucherList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
    }

	function voucherDtl(){
		if (intval($_SESSION['right']['discount']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/voucher.class.php';
        $voucher = new voucher($this->DB, $this->lang, $this->isAdmin);

		$voucher_id = $this->gthis->parm_sql('request', 'voucher_id', 'N');
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$res['voucherDtl'] = $voucher->getVoucherDtl($voucher_id);
		
		require_once ROOT.'/include/class/user.class.php';
        $userCls = new user($this->DB, $this->lang, $this->isAdmin);
		$res['salesList'] = $userCls->getAdminList();
		
		require_once ROOT.'/include/class/product.class.php';
        $product = new product($this->DB, $this->lang, $this->isAdmin);
		$res['productList'] = $product->getProductList($shop_id);

		require_once ROOT.'/include/class/cate.class.php';
        $cateCls = new cate($this->DB, $this->lang, $this->isAdmin);
		$res['cateList'] = $cateCls->getCateList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "voucherList",
			"url" => "admin/discount/voucherList",
			"page_title" => $this->gthis->RMBC->getReturnString('voucherList')
		],[
			"code" => "voucherDtl",
			"url" => "admin/discount/voucherDtl?voucher_id=" . ($voucher_id ? $voucher_id : "0"),
			"page_title" => $voucher_id ? $res['voucherDtl']['name_tc'] : $this->gthis->RMBC->getReturnString('addCoupon')
		]);
		return $res;
	}
}
?>