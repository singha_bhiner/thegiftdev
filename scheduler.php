<?php	
if (isset($_SERVER['HTTP_ORIGIN'])){
	$http_origin = $_SERVER['HTTP_ORIGIN'];
	if(strpos($http_origin, 'whatsapp')>-1){
		header('Access-Control-Allow-Origin:'.$http_origin);
		header('Access-Control-Allow-Headers: Accept');
		header("Access-Control-Allow-Credentials: true");
	}
}

require_once 'include/baseModel.class.php';

class scheduler extends baseModel{

	public function __construct(){
		parent::__construct('');
		$this->init();
	}

	public function init(){
		switch($this->mode){
			case 'schedulerStart':
				$this->schedulerStart();
				break;
			case 'dedicatedTask':
				$this->dedicatedTask();
				break;
			case 'activePoller':
				$this->activePoller();
				break;
			default:
				$this->schedulerStart();
				break;
		}
	}

	function dedicatedTask(){
		$task_name = $this->parm_in('request','task_name','N');
		$query = "select * from `tbl_scheduler` where `task_name` = '".$this->escape_string($task_name)."';";
		$tasks = $this->DB->get_Sql($query);

		foreach($tasks as $k=>$i){
			$query = "update `tbl_scheduler` set `last_check_on` = '".$this->get_current_time()."' where `scheduler_id` = '".$i['scheduler_id']."';";
			$this->DB->update($query);
			$i['running'] = 'N';
			if(method_exists(scheduler::class,$i['task_name'])){
				call_user_func('scheduler'.'::'.$i['task_name'],$i);
			}
		}

		die('Scheduler Done');
	}

	/* ******************************************************************** */
	/* ** Author        : David Chung                                       */
	/* ** Last Updat On : 30 Mar 2020                                       */
	/* ** Desciption    : 檢查要執行的排程任務                                */
	/* ******************************************************************** */

	function schedulerStart(){
		// 這邊檢查數據庫中已登記的定時任務
		$query = "select * from `tbl_scheduler` where `activate` = 'Y';";
		$tasks = $this->DB->get_Sql($query);

		// 	檢查每一個已登記的定時任務
		foreach($tasks as $k=>$i){
			// 下邊sql 更新已登記的定時任務 的檢查時期，作為除錯檢查及記錄時間
			$query = "update `tbl_scheduler` set `last_check_on` = '".$this->get_current_time()."' where `scheduler_id` = '".$i['scheduler_id']."';";
			$this->DB->update($query);

			// 下邊代碼為如果在本php script 中有找到 對應的function 名稱，則自動執行 function 名稱應對應 tbl_scheduler 中登記的task_name, 留意大少寫必須一致
			if(@method_exists(scheduler::class,$i['task_name'])){ 
				call_user_func('scheduler'.'::'.$i['task_name'],$i);
			}
		}

		die('Scheduler Done');
	}

	/* ******************************************************************** */
	/* ** Author        : David Chung                                       */
	/* ** Last Updat On : 30 Mar 2020                                       */
	/* ** Desciption    : 檢查要發送的電郵                                   */
	/* ******************************************************************** */

	function email_sender($task){
		if($task['running']=='Y'){
			// 正在執行 , 離開
			return;
		}
		$query = "select * from `tbl_email_sender` where `status` = 'O';";
		$email_to_send = $this->DB->get_Sql($query);
		if($email_to_send){
			// 執行
			$this->schedulerTaskFire($task);

			// 發送確認電郵
			require_once dirname(__FILE__).'/include/email.class.php';
			$emailClass = new email($this,$this->DB);

			// 程序執行
			foreach($email_to_send as $k=>$i){
				$address = $i['to_email'];
				$title = $i['title'];
				$emailContent = $i['content'];
				$attachments = $i['attachments'];
				if(PERSONAL_DATA_ENCRYPT){
					$address = $this->dataDecrypt($address);
				}
				if(ENV=='DEV'){
					$title = '[DEV] '.$title;
				}
				$returnCode = $emailClass->sendEmailGoZoho($address,$title,$emailContent,'N',$attachments);
				if(is_array($returnCode)){
					// update send status
					$error_log = $returnCode['error_info'];
					$return_code = $returnCode['code'];
					$query = "update `tbl_email_sender` set `status` = 'D' , `sent_on` = '".$this->get_current_time()."' , `return_code` = '$return_code' , `error_log` = '".$this->escape_string($error_log)."' where `email_id` = '".$i['email_id']."';";
					$this->DB->update($query);
				} else {
					// update send status
					$query = "update `tbl_email_sender` set `status` = 'D' , `sent_on` = '".$this->get_current_time()."' , `return_code` = '$returnCode' where `email_id` = '".$i['email_id']."';";
					$this->DB->update($query);	
				}
			}
		
			// 完結
			$this->schedulerTaskCompleted($task);
		}
	}


	function schedulerTaskFire($task){
		$query = "update `tbl_scheduler` set `last_fire_on` = '".$this->get_current_time()."' , `running` = 'Y' where `scheduler_id` = '".$task['scheduler_id']."';";
		$this->DB->update($query);
	}

	function schedulerTaskCompleted	($task){
		$query = "update `tbl_scheduler` set `last_completed_on` = '".$this->get_current_time()."' , `running` = 'N' where `scheduler_id` = '".$task['scheduler_id']."';";
		$this->DB->update($query);
	}

	function clear_login_register_token($task){
		if($task['running']=='Y'){
			// 正在執行 , 離開
			return;
		}
		$query = "select * from `tbl_register_prepare` where now()> `valid_till`;";
		$register_token_expired = $this->DB->get_Sql($query);

		$query = "select * from `tbl_login_prepare` where now()> `valid_till`;";
		$login_token_expired = $this->DB->get_Sql($query);
		if(sizeOf($register_token_expired)>0 || sizeOf($login_token_expired)>0){
			// 執行
			$this->schedulerTaskFire($task);

			if(($register_token_expired)>0){
				$query = "delete from `tbl_register_prepare` where now()> `valid_till`;";
				$this->DB->update($query);	
			}
			if(($login_token_expired)>0){
				$query = "delete from `tbl_login_prepare` where now()> `valid_till`;";
				$this->DB->update($query);	
			}
			
			// 完結
			$this->schedulerTaskCompleted($task);
		}
	}

	/* ******************************************************************** */
	/* ** Author        : David Chung                                       */
	/* ** Last Updat On : 03 Feb 2021                                       */
	/* ** Desciption    : 每小時檢查tracking 狀態                             */
	/* ******************************************************************** */

	function tracking_status($task){
		if($task['running']=='Y'){
			// 正在執行 , 離開
			return;
		}

		$now = $this->get_current_time();
		$hr = substr($now, -8,5);

		// 於下列時間執行檢本跟蹤號的狀態 API
		if($hr=='09:00' || $hr=='10:00' || $hr=='11:00' || $hr=='12:00' || $hr=='13:00' || $hr=='14:00' || $hr=='15:00' || $hr=='16:00' || $hr=='17:00' || $hr=='18:00' || $hr=='21:00'){
			// 執行
			$this->schedulerTaskFire($task);
            //$this->commentRefresh(); //不應放在這邊，因為commentRefresh 是直接die() 了整個 ，會令整個scheulder 停下來，其他的任務會執行不了
			// start on every 0 minute
			$query = "SELECT *  FROM `app_tbl_orders` WHERE `order_status` >= 2 and `order_status` not in (6,41,42,99) and `tracking_number` is not null;";
			$order_to_check = $this->DB->get_Sql($query);

			$aramex_tracking = array();
			$ups_tracking = array();
			$dhl_tracking = array();
			$fedex_tracking = array();

			foreach($order_to_check as $k=>$i){
				$shipping_provider = $i['shipping_provider'];

				if(strpos($shipping_provider, 'aramex')>-1){
					array_push($aramex_tracking, $i['tracking_number']);
				}

				if(strpos($shipping_provider, 'ups')>-1){
					array_push($ups_tracking, $i);
				}
			}

			if(sizeOf($ups_tracking)>0){
				foreach($ups_tracking as $k=>$i){
					// check aramex checking api
					$postdata = http_build_query(
					    array(
					        'tracking_number' => json_encode($i['tracking_number']),
					    )
					);
					$opts = array('http' =>
					    array(
					        'method'  => 'POST',
					        'header'  => 'Content-Type: application/x-www-form-urlencoded',
					        'content' => $postdata
					    )
					);
					$context = stream_context_create($opts);
					// get price result
					$tracking_result = file_get_contents($this->baseURL."/apis/upsTrackingService.php", false, $context);
					$tracking_result = json_decode($tracking_result,1);
					$order_id = $i['order_id'];
					foreach($tracking_result as $a=>$b){
						if(trim($b['Status']['StatusType']['Description'])=='Delivered'){
							// 已送達
							$query1 = "update `app_tbl_orders` set `order_status` = '6' , `delivery_date` = '".$this->get_current_time()."' where `order_id` = '$order_id' and `order_status` in (3,4,5);";
							$this->DB->update($query1);

							$query = "select * from `app_tbl_orders` where `order_status` = '6' and `order_id` = '$order_id' and `delivered_email` = 'N';";
							$order = $this->DB->get_Sql($query);

							if($order){
								$order_id = $order[0]['order_id'];
								
								require_once dirname(__FILE__).'/include/member.class.php';
								$memberObject = new member($this,$this->DB);
								$member = $memberObject->getMember($order[0]['member_id']);

								require_once dirname(__FILE__).'/include/email.class.php';
								$emailClass = new email($this, $this->DB);
								$email_id = $emailClass->sendFinalDeliveryConfirmation($member, $order_id);

								// update shipout email record
								$query = "update `app_tbl_orders` set `delivered_email` = 'Y' where `order_id` = '$order_id';";
								$this->DB->update($query);
							}			
						}

						// aramex : Held in Customs - Pending Clearance
						// Your package was released by the customs agency.

						if(trim($b['Status']['StatusType']['Description'])=='Pickup Scan'){
							// 已取件
							$query = "update `app_tbl_orders` set `order_status` = '4' where `order_id` = '$order_id' and `order_status` in (2,3);";
							$this->DB->update($query);
						}

						if(trim($b['Status']['StatusType']['Description'])=='Export Scan'){
							// 已到達物流中心
							$query1 = "update `app_tbl_orders` set `order_status` = '5' where `order_id` = '$order_id' and `order_status` in (4);";
							$this->DB->update($query1);

							$query = "select * from `app_tbl_orders` where `order_status` = '5' and `order_id` = '$order_id' and `ship_out_email` = 'N';";
							$order = $this->DB->get_Sql($query);

							if($order){
								$order_id = $order[0]['order_id'];
								
								require_once dirname(__FILE__).'/include/member.class.php';
								$memberObject = new member($this,$this->DB);
								$member = $memberObject->getMember($order[0]['member_id']);

								require_once dirname(__FILE__).'/include/email.class.php';
								$emailClass = new email($this, $this->DB);
								$email_id = $emailClass->sendOrderShipoutConfirmation($member, $order_id);

								// update shipout email record
								$query = "update `app_tbl_orders` set `ship_out_email` = 'Y' where `order_id` = '$order_id';";
								$this->DB->update($query);
							}									
						}
					}
				}
			}

			if(sizeOf($aramex_tracking)>0){
				// check aramex checking api
				$postdata = http_build_query(
				    array(
				        'tracking_number' => json_encode($aramex_tracking),
				    )
				);
				$opts = array('http' =>
				    array(
				        'method'  => 'POST',
				        'header'  => 'Content-Type: application/x-www-form-urlencoded',
				        'content' => $postdata
				    )
				);


				$context = stream_context_create($opts);
				// get price result
				$tracking_result = file_get_contents($this->baseURL."/apis/aramexTrackingService.php", false, $context);
				$tracking_result = json_decode($tracking_result,1);
				// loop the return
				foreach($tracking_result['TrackingResults'] as $a=>$b){
					$first_value = array_values($b)[0];
					if(isset($first_value['Key'])){
						// multi mode
						foreach($b as $m=>$n){
							$key = $n['Key'];
							$value = $n['Value'];
							foreach($value['TrackingResult'] as $k=>$i){
								if(trim($i['UpdateDescription'])=='Delivered'){
									// 已送達
									$query1 = "update `app_tbl_orders` set `order_status` = '6' , `delivery_date` = '".$this->get_current_time()."' where `tracking_number` = '$key' and `order_status` in (5);";
									$this->DB->update($query1);

									$query = "select * from `app_tbl_orders` where `order_status` = '6' and `tracking_number` = '$key' and `delivered_email` = 'N';";
									$order = $this->DB->get_Sql($query);

									if($order){
										$order_id = $order[0]['order_id'];
										
										require_once dirname(__FILE__).'/include/member.class.php';
										$memberObject = new member($this,$this->DB);
										$member = $memberObject->getMember($order[0]['member_id']);

										require_once dirname(__FILE__).'/include/email.class.php';
										$emailClass = new email($this, $this->DB);
										$email_id = $emailClass->sendFinalDeliveryConfirmation($member, $order_id);

										// update shipout email record
										$query = "update `app_tbl_orders` set `delivered_email` = 'Y' where `order_id` = '$order_id';";
										$this->DB->update($query);
									}			
								}

								if(trim($i['UpdateDescription'])=='Picked Up From Shipper'){
									// 已取件
									$query = "update `app_tbl_orders` set `order_status` = '4' where `tracking_number` = '$key' and `order_status` in (2,3);";
									$this->DB->update($query);
								}

								if(trim($i['UpdateDescription'])=='Received at Origin Facility'){
									// 已到達物流中心
									$query1 = "update `app_tbl_orders` set `order_status` = '5' where `tracking_number` = '$key' and `order_status` in (4);";
									$this->DB->update($query1);

									$query = "select * from `app_tbl_orders` where `order_status` = '5' and `tracking_number` = '$key' and `ship_out_email` = 'N';";
									$order = $this->DB->get_Sql($query);

									if($order){
										$order_id = $order[0]['order_id'];
										
										require_once dirname(__FILE__).'/include/member.class.php';
										$memberObject = new member($this,$this->DB);
										$member = $memberObject->getMember($order[0]['member_id']);

										require_once dirname(__FILE__).'/include/email.class.php';
										$emailClass = new email($this, $this->DB);
										$email_id = $emailClass->sendOrderShipoutConfirmation($member, $order_id);

										// update shipout email record
										$query = "update `app_tbl_orders` set `ship_out_email` = 'Y' where `order_id` = '$order_id';";
										$this->DB->update($query);
									}									
								}
							}
						}
					} else {
						// single mode
						$key = $b['Key'];
						$value = $b['Value'];
						foreach($value['TrackingResult'] as $k=>$i){
							if(trim($i['UpdateDescription'])=='Delivered'){
								// 已送達
								$query1 = "update `app_tbl_orders` set `order_status` = '6' where `tracking_number` = '$key' and `order_status` in (5);";
								$this->DB->update($query1);

								$query = "select * from `app_tbl_orders` where `order_status` = '6' and `tracking_number` = '$key' and `delivered_email` = 'N';";
								$order = $this->DB->get_Sql($query);

								if($order){
									$order_id = $order[0]['order_id'];
									
									require_once dirname(__FILE__).'/include/member.class.php';
									$memberObject = new member($this,$this->DB);
									$member = $memberObject->getMember($order[0]['member_id']);

									require_once dirname(__FILE__).'/include/email.class.php';
									$emailClass = new email($this, $this->DB);
									$email_id = $emailClass->sendFinalDeliveryConfirmation($member, $order_id);

									// update shipout email record
									$query = "update `app_tbl_orders` set `delivered_email` = 'Y' where `order_id` = '$order_id';";
									$this->DB->update($query);
								}			
							}

							if(trim($i['UpdateDescription'])=='Picked Up From Shipper'){
								// 已取件
								$query = "update `app_tbl_orders` set `order_status` = '4' where `tracking_number` = '$key' and `order_status` in (2,3);";
								$this->DB->update($query);
							}

							if(trim($i['UpdateDescription'])=='Received at Origin Facility'){
								// 已到達物流中心
								$query1 = "update `app_tbl_orders` set `order_status` = '5' where `tracking_number` = '$key' and `order_status` in (4);";
								$this->DB->update($query1);

								$query = "select * from `app_tbl_orders` where `order_status` = '5' and `tracking_number` = '$key' and `ship_out_email` = 'N';";
								$order = $this->DB->get_Sql($query);

								if($order){
									$order_id = $order[0]['order_id'];
									
									require_once dirname(__FILE__).'/include/member.class.php';
									$memberObject = new member($this,$this->DB);
									$member = $memberObject->getMember($order[0]['member_id']);

									require_once dirname(__FILE__).'/include/email.class.php';
									$emailClass = new email($this, $this->DB);
									$email_id = $emailClass->sendOrderShipoutConfirmation($member, $order_id);

									// update shipout email record
									$query = "update `app_tbl_orders` set `ship_out_email` = 'Y' where `order_id` = '$order_id';";
									$this->DB->update($query);
								}									
							}
						}
					}
				}
			}

			// 完結
			$this->schedulerTaskCompleted($task);
		}

		/*$date1 = new DateTime($now);
		$date2 = new DateTime($last_completed_on);	
		$interval1 = $date1->diff($date2);		
		if($interval1->s>0 && $interval1->invert>0){
			// update course status for deadline reached
			$query = "update `tbl_cpd_course` set `status` = 'C' where `course_id` = '".$i['course_id']."';";
			$this->DB->update($query);

			// send attendance list to staging
			$this->sendAttendanceListToStagingForREW($i['course_id']);
		}*/

	}

	/* ******************************************************************** */
	/* ** Author        : Jerry                                       		*/
	/* ** Last Update On : 23 Jun 2021                                      */
	/* ** Desciption    : 15天自動評價 */
	/** 		
	 * 	@param $task
	 *  @return null	    
	/* ******************************************************************** */
    function commentRefresh($task){ //15天自動評價
    	if($task['running']=='Y'){
			// 如果正在執行 , 則離開不重覆提交 , scheduler 是會一分鐘由corn job 執行一次，這段免一些執行時間超過一分鐘的任務未成成前被重複執行
 			return;
		}

		$now = $this->get_current_time();
		$hr = substr($now, -8,5);

		// 只在以下時間執行，每天只執行一次便可以
		if($hr=='12:00'){
			// 執行
			$this->schedulerTaskFire($task); // 必須:這邊是更新任務實際執行時間的記錄 ，同時將定時任務設為running = Y

            require_once dirname(__FILE__) . '/include/order.app.class.php';
            $orderClass = new order($this, $this->DB); // 這邊不會用 $this->pthis, 留意是沒有 $pthis class 的，直接用 $this,

	        $query = "select `order_id`,`pickup_date` from `app_tbl_orders` where `activate` = 'Y' and `order_status` = '6' and `comment_status` = 'N' and datediff(now(),`delivery_date`) >= '15'";
	        $order_ids = $this->DB->get_sql($query);

	        if(@$order_ids){
	        	$rows = [];
		        foreach($order_ids as $v){
		            $order_list = $orderClass->getOrderList(null, null,'order_id','desc',0,1,null,$v['order_id']);
		            $rows[] = $order_list["orders"][0];
		        }
		        if(count($rows)>0) {
		            foreach ($rows as $val) {
		                $com_sql = $this->DB->insert_db('app_tbl_comment_all', array(
		                    'member_id' => $val['member_id'],
		                    'order_id' => $val['order_id'],
		                    'shipping_id' => $val['shipping_provider_array']['id'],
		                    'Overall_score' => '5',
		                    'sender_country' => $val['sender_country'],
		                    'country' => $val['country'],
		                    'total_pre_act_weight' => $val['total_pre_act_weight'],
		                    'com_texts' => '',
		                    'com_images' => '',
		                    'comment_time' => $this->get_current_time(), 
		                    'com_status' => '1',
		                    'auto_evaluate' => 'Y'
		                ));
		                $query = "update `app_tbl_orders` set `comment_status` = 'Y' where `order_id` = '" . $val['order_id'] . "';";
		                $this->DB->update($query);
		            }
		        }
	        }
	        // 完結
			$this->schedulerTaskCompleted($task);
		}
		return;
    }


    /* ******************************************************************** */
	/* ** Author        : David Chung                                       */
	/* ** Last Updat On : 23 Fev 2021                                       */
	/* ** Desciption    : 自動api 下單		                                */
	/* ******************************************************************** */

	function auto_api_order($task){
		if($task['running']=='Y'){
			// 正在執行 , 離開
			return;
		}

		if(ENV=='PROD'){
			$query = "select * from `app_tbl_orders` where `order_status` = '2' and `shipping_provider` in ('aramexhk','aramexhk_ecom_usa','upshk_expedited','upshk_export_saver') and `api_auto` = 'Y';";
			$waiting_process_order = $this->DB->get_Sql($query);
			if($waiting_process_order){
				// 執行
				$this->schedulerTaskFire($task);

				$opts = [
				    "http" => [
				        "method" => "GET",
				        "header" => "Accept-language: en\r\n"
				    ]
				];
				$context = stream_context_create($opts);

				require_once dirname(__FILE__).'/include/email.class.php';
				$emailClass = new email($this,$this->DB);
				
				foreach($waiting_process_order as $k=>$i){
					// aramex
					if($i['shipping_provider'] == 'aramexhk' || $i['shipping_provider'] == 'aramexhk_ecom_usa'){
						$order_id = $i['order_id'];
						$result = file_get_contents($this->baseURL."/adminAjax/aramexApiPlaceOrderGo?order_id=$order_id",false,$context);	
						$result = json_decode($result,1);
						if($result['result']['code']!=0){
							$emailClass->sendInternalEmail('',"[失敗] $order_id API自動下單",json_encode($result));
							$query = "update `app_tbl_orders` set `api_auto` = 'N' where `order_id` = '$order_id';";
							$this->DB->update($query);
						} else {
							$emailClass->sendInternalEmail('',"[成功] $order_id API自動下單",json_encode($result));
							$query = "update `app_tbl_orders` set `api_auto` = 'N' where `order_id` = '$order_id';";
							$this->DB->update($query);

						}
					}
					
					// ups
					if($i['shipping_provider'] == 'upshk_expedited' || $i['shipping_provider'] == 'upshk_export_saver'){
						$order_id = $i['order_id'];
						$result = file_get_contents($this->baseURL."/adminAjax/upsApiPlaceOrderGo?order_id=$order_id",false,$context);	
						$result = json_decode($result,1);
						if($result['result']['code']!=0 || empty($result)){
							$emailClass->sendInternalEmail('',"[失敗] $order_id API自動下單",json_encode($result));
							$query = "update `app_tbl_orders` set `api_auto` = 'N' where `order_id` = '$order_id';";
							$this->DB->update($query);
						} else {
							$emailClass->sendInternalEmail('',"[成功] $order_id API自動下單",json_encode($result));
							$query = "update `app_tbl_orders` set `api_auto` = 'N' where `order_id` = '$order_id';";
							$this->DB->update($query);

						}
					}
				}
				// 完結
				$this->schedulerTaskCompleted($task);
			}
		}
	}

	function activePoller(){
		$task_name = $this->parm_in('request','task_name','N');
		switch ($task_name) {
			case 'whatsappSender':
				require_once dirname(__FILE__).'/include/im.class.php';
				$imClass = new im($this, $this->DB);
				$im_id = $this->parm_in('request','im_id','N');
				if($im_id){
					$imMessage = $imClass->querySetGoingToSend($im_id);
				} else {
					$imMessage = $imClass->queryToBeSend('whatsapp');
					$result['im_message'] = $imMessage;
				}
				break;
			
			default:
				# code...
				break;
		}

		$result['code'] = '0';
		$result['msg'] = 'Poller Done';
		$this->json_return('result', $result, 'Y');
	}

	public function display(){
		// display not required
	}

}
$HOME = new scheduler;
$HOME->display();

?>