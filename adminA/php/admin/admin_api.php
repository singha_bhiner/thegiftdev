<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->DB = $g_DB;
		
		require_once ROOT. '/include/apiDataBuilder.class.php';
		
		switch ($this->gthis->thirdmode) {
			case 'menubarStatus':
				$this->menubarStatus();
				break;
			
			default:
				new apiDataBuilder(-84, null, null, $this->lang);
				break;
		}

		$result_array['lang'] = $this->lang;
		@$this->gthis->assign('result_array',$result_array);
	}

	function menuBarStatus(){
		$open = $this->gthis->parm_in('request','open','Y');
		$_SESSION['menu_bar_open'] = $open;
		$result['code'] = '0';
		$result['msg'] = 'Done';
		$result['menu_bar_open'] = $_SESSION['menu_bar_open'];
		new apiDataBuilder(0, $result, null, $this->lang);
	}

}
?>