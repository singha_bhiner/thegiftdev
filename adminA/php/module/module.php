<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;


        $this->gthis->assign('page_only', 'Y');
		switch ($this->gthis->submode) {
			case 'editItemGrp':
				$result_array = $this->editItemGrp();
				break;
			default:
				break;
		}

		$this->gthis->assign('result_array',$result_array);
	}

    function editItemGrp(){
        require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "grp_id", "type" => "int"],
			["key" => "copy", "type" => "NY"]
		]);

		$group = $itemCls->getItemGroupDtl($data['grp_id']);
		$group['target_grp_id'] = $group['grp_id'];
		if ($data['copy'] == 'Y'){
			$group['grp_id'] = 0;
			$res['copy'] = 'Y';
		}
        $res['group'] = $group;

        return $res;
    }
}
?>