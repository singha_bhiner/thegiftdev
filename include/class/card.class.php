<?php
/* 
    Objective: greeting card option
    Author: Sing
    Last major update: 03-11-2021

	Table scheme: 
        tbl_product_card

*/


require_once dirname(__FILE__). '/abstract.class.php';
class card extends baseClass{

    function init(){
        $this->multiLangFieldArr = ['name'];
    }
    
    function getCardList(){
        $sql = "SELECT * ";
        foreach($this->multiLangFieldArr AS $field){
            $sql .= ", $field" . "_" . $this->lang . " AS $field ";
        }
        $sql .= " FROM tbl_product_card WHERE `activate` = 'Y'";
        $cardList = $this->DB->get_Sql($sql);

        return $cardList;
    }

    function getCardDtl($card_id){
        $sql = "SELECT * ";
        foreach($this->multiLangFieldArr AS $field){
            $sql .= ", $field" . "_" . $this->lang . " AS $field ";
        }
        $sql .= " FROM tbl_product_card WHERE `activate` = 'Y' AND card_id = $card_id";
        $cardDtl = $this->DB->get_Sql($sql)[0];

        return $cardDtl;
    }

    
    function editCardGroup($data){
        // edit gift card
        // $card_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        
        if ($data['card_group_id'] == 0){
            $card_group_id = $this->DB->insert_db('tbl_product_card_group', $data);
        } else {
            $card_group_id = $this->DB->update_Sql('tbl_product_card_group', 'card_group_id', $data);
        }

        $this->DB->updateMap("tmap_product_card_group", "card_group_id", $card_group_id, "card_id", $data['card_id_arr']);
        unset($data['product_id_arr']);

        $sql = $this->DB->buildSql_update("tbl_product_card_group", "card_group_id", $card_group_id, $data);
        $this->DB->update($sql);

        return $this->getCardDtl($card_group_id);
    }

    function getCardGroupList($filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: which shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $sql = "SELECT pcg.*
                FROM `tbl_product_card_group` AS pcg
                WHERE pcg.`activate` = 'Y'";

        foreach($filter AS $k => $i){
            if ($i){
                switch($k){
                    case 'skey':
                        $sql .= " AND (pcg.card_group_id LIKE '%$i$' OR pcg.name_tc LIKE '%$i%' OR pcg.name_en LIKE '%$i%') ";
                        break;
                    case 'card_group_id':
                        $sql .= " AND (pcg.card_group_id = '$i')";
                        break;
                }
            }
        }

        $sql .= "GROUP BY pcg.card_group_id";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit']; 
        }
        
        $res = $this->DB->get_Sql($sql);
        
        foreach ($res as $k => $i){
            $res[$k] = $this->dispatchCardGroup($i);
        }

        return $res;
    }

    function dispatchCardGroup($card_group){
        $card_group_id = $card_group['card_group_id'];

        $sql = "SELECT pc.*, m.map_id
                FROM `tmap_product_card_group` AS m 
                LEFT JOIN `tbl_product_card` AS pc ON pc.card_id = m.card_id
                WHERE m.`card_group_id` = $card_group_id AND m.activate = 'Y'";
        $card_group['card_list'] = $this->DB->get_Sql($sql);

        return $card_group;
    }

    function getCardGroupDtl($card_group_id){
        if ($card_group_id){
            return $this->getCardGroupList(["card_group_id" => $card_group_id], $this->DB->getUnique)[0];
        } else {
            return null;
        }
    }

}
?>