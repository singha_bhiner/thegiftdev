<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;
		
		require_once ROOT. '/include/apiDataBuilder.class.php';
		
		switch ($this->gthis->thirdmode) {
			case 'menuBarStatus':
				$this->menuBarStatus();
				break;

			case 'scanCode':
				$this->scanCode();
				break;
			default:
				new apiDataBuilder(-84);
				break;
		}

		$result_array['lang'] = $this->lang;
		@$this->gthis->assign('result_array',$result_array);
	}

	function scanCode(){
		$code = $this->gthis->param(["key" => "code"]);
		
		// is ID (ITEMID: || LOCATIONID: || DELIID: || ORDERID:)
		if (substr($code,0,7) == 'ITEMID:'){
			// if it is item_id
			require_once ROOT.'/include/class/item.class.php';
			$itemCls = new item($this->DB, $this->lang, $this->isAdmin);
			$res['item'] = $itemCls->getItemDtl(substr($code, 7));
		} else if (substr($code,0,8) == 'ORDERID:') {
			// if it is order_id
			require_once ROOT.'/include/class/order.class.php';
			$orderCls = new order($this->DB, $this->lang, $this->isAdmin);
			$res['order'] = $orderCls->getOrderDtl(substr($code, 8));
		} else if (substr($code,0,7) == 'DELIID:') {
			// if it is deli_id
			require_once ROOT.'/include/class/order.class.php';
			$orderCls = new order($this->DB, $this->lang, $this->isAdmin);
			$res['deli'] = $orderCls->getDeliDtl(substr($code, 7));
		} else if (substr($code,0,11) == 'LOCATIONID:') {
			// if it is location_id
			require_once ROOT.'/include/class/warehouse.class.php';
			$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);
			$res['location'] = $warehouseCls->getLocationDtl(substr($code, 11));
		} else {
			// it is barcode
			require_once ROOT.'/include/class/item.class.php';
			$itemCls = new item($this->DB, $this->lang, $this->isAdmin);
			$res['item'] = $itemCls->getItemDtl($itemCls->codeToItem($code));
		}


		new apiDataBuilder(0, $res);
	}

	function menuBarStatus(){
		$open = $this->gthis->parm_in('request','open','Y');
		$_SESSION['menu_bar_open'] = $open;
		$result['code'] = '0';
		$result['msg'] = 'Done';
		$result['menu_bar_open'] = $_SESSION['menu_bar_open'];
		new apiDataBuilder(0, $result);
	}

}
?>