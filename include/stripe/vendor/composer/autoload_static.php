<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit2be5a0f69edb71288b1130f79570c39d
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Stripe\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Stripe\\' => 
        array (
            0 => __DIR__ . '/..' . '/stripe/stripe-php/lib',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit2be5a0f69edb71288b1130f79570c39d::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit2be5a0f69edb71288b1130f79570c39d::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
