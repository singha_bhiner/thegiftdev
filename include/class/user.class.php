<?php
/* 
    Objective: Admin related
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
		tbl_admin
		tbl_admin_role


        
*/


require_once dirname(__FILE__). '/abstract.class.php';
class user extends baseClass{
	function init(){
		$this->uid = 'admin_id';
	}

	function getSales($sales_key){
		// get the sales detail base on the sales key
		// $sales_key: skey 

		$sql = "SELECT admin_id
				FROM `tbl_admin` 
				WHERE activate = 'Y' AND (admin_id = '$sales_key' || username = '$sales_key')";
		$admin_id = $this->DB->get_One($sql);

		return $admin_id;
	}

	function getAdminRoleList($filter = null, $limiter = null, $sorting = null){
		// get the list
		// $shop_id: shop in use
		// $filter: [obj]
		// $limiter: [obj] - page, limit
		// $sorting: [obj] - sort, order, 
		//          order: ASC/ DESC
		//          sort: field sort by

		$sql = "SELECT * 
				FROM `tbl_admin_role` AS r 
				WHERE r.`activate` = 'Y'";
        
        foreach($filter as $k => $i){
            if ($i || $i == '0'){
                switch ($k){
                    default: 
                        break;
                }                    
            }
        }

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }
        
        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

		return $this->DB->get_Sql($sql);
	}

	function getAdminRoleDtl($role_id){
		// get the admin role Detail
		// $role_id: PK

		$sql = "SELECT * 
				FROM `tbl_admin_role` AS r
				WHERE r.`activate` = 'Y' AND `role_id` = '$role_id'";
		$res = $this->DB->get_Sql($sql)[0];
		$res['gift_right'] = json_decode($res['gift_right'], true);
		$res['zuri_right'] = json_decode($res['zuri_right'], true);
		$res['ltp_right'] = json_decode($res['ltp_right'], true);



		if ($res){
			$sql = "SELECT GROUP_CONCAT(m.product_id) AS product_id_arr
                FROM `tbl_admin_role` AS r 
                LEFT JOIN `tmap_role_product` AS m ON r.role_id = m.role_id AND m.activate = 'Y'
                WHERE r.activate ='Y' && r.role_id = '$role_id'";
			$tList = $this->DB->get_Sql($sql)[0];
			$res['product_id_arr'] = $tList['product_id_arr'];
		}

		return $res;
	}

	function editAdminRole($role_id, $data){
		// edit admin role
		// $role_id: PK, which edit. Inserting a new row when 0
		// $data: the data of it 

		if ($role_id == 0)
			$role_id = $this->DB->insert_db('tbl_admin_role', null);

		$this->DB->updateMap("tmap_role_product", "role_id", $role_id, "product_id", $data['product_id_arr']);
		unset($data['product_id_arr']);

		$sql = $this->DB->buildSql_update("tbl_admin_role", "role_id", $role_id, $data);
		$this->DB->update($sql);

		return $this->getAdminRoleDtl($role_id);
	}

	function getAdminList($filter = null, $limiter = null, $sorting = null){
		// get the list
		// $shop_id: shop in use
		// $filter: [obj]
		// $limiter: [obj] - page, limit
		// $sorting: [obj] - sort, order, 
		//          order: ASC/ DESC
		//          sort: field sort by

		$sql = "SELECT * 
				FROM `tbl_admin` AS a 
				INNER JOIN `tbl_admin_role` AS r ON a.role_id = r.role_id
				WHERE a.`activate` = 'Y'";
        
        foreach($filter as $k => $i){
            if ($i || $i == '0'){
                switch ($k){
                    case 'skey':
                        $sql .= "AND (admin_id LIKE '%$i%' || username LIKE '%$i%')";
                        break;
					case 'driver':
						$sql .= "AND  (r.driver_right = '$i')";
						break;
                    default: 
                        break;
                }                    
            }
        }

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }
        
        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

		return $this->DB->get_Sql($sql);
	}

	function getAdminDtl($admin_id){
		// get the admin Detail
		// $admin_id: PK

		$sql = "SELECT * 
				FROM `tbl_admin` AS a 
				INNER JOIN `tbl_admin_role` AS r ON a.role_id = r.role_id
				WHERE a.`activate` = 'Y' AND `admin_id` = '$admin_id'";
		$res = $this->DB->get_Sql($sql)[0];

		return $res;
	}

	function editAdmin($admin_id, $data){
		// edit admin
		// $admin_id: PK, which edit. Inserting a new row when 0
		// $data: the data of it 

		if ($admin_id == 0)
			$admin_id = $this->DB->insert_db('tbl_admin', null);

		$sql = $this->DB->buildSql_update("tbl_admin", "admin_id", $admin_id, $data);
		$this->DB->update($sql);

		return $this->getAdminDtl($admin_id);
	}

    /* ***************************************************************************************** */
	/*	Author : David Chung
	/*	Date : 19 Jun 2021
	/*  LUD  : 19 Jun 2021
	 *  Desc : create an admin record */
	/** 	   
	 * 	@param N/A			   
	 *  @return $admin_id (int)	    
	/* ***************************************************************************************** */

	function createAdminUser(){
		$admin_id = $this->DB->insert_db($this->table, array(
  				$this->uid => '0',
  		));
  		return $admin_id;
	}

	/* ***************************************************************************************** */
	/*	Author : David Chung
	/*	Date : 19 Jun 2021
	/*  LUD  : 19 Jun 2021
	 *  Desc : get admin from admin username */
	/** 	   
	 * 	@param $username			   
	 *  @return $files (object)	    
	/* ***************************************************************************************** */

	function getAdminFromUsername($username,$activate='Y'){
		// build stand alone file obect
		$query = "select * from `tbl_admin` AS a INNER JOIN `tbl_admin_role` AS r ON a.role_id = r.role_id where `username` = '".$username."' and a.`activate` = '$activate';";
		
		$admin = $this->DB->get_Sql($query);
		if($admin){
			return $admin[0];
		}
		return null;
	}

	/* ***************************************************************************************** */
	/*	Author : David Chung
	/*	Date : 19 Jun 2021
	/*  LUD  : 19 Jun 2021
	 *  Desc : get admin from admin admin_id and password */
	/** 	   
	 * 	@param $admin_id			   
	 * 	@param $password			   
	 *  @return $files (object)	    
	/* ***************************************************************************************** */

	function getAdminFromAdminIdPassword($admin_id,$password){
		// build stand alone file obect
		$query = "select * from `tbl_admin` where `admin_id` = '".$admin_id."' and `password` = '".$this->gthis->escape_string($password)."';";
		$admin = $this->DB->get_Sql($query);
		if($admin){
			return $admin[0];
		}
		return null;
	}

	/* ***************************************************************************************** */
	/*	Author : David Chung
	/*	Date : 19 Jun 2021
	/*  LUD  : 19 Jun 2021
	 *  Desc : get admin from admin admin_id  */
	/** 	   
	 * 	@param $admin_id			   
	 * 	@param $password			   
	 *  @return $files (object)	    
	/* ***************************************************************************************** */

	function getAdminFromAdminId($admin_id){
		// build stand alone file obect
		$query = "select * from `tbl_admin` where `admin_id` = '".$this->gthis->escape_string($admin_id)."';";
		$admin = $this->DB->get_Sql($query);
		if($admin){
			return $admin[0];
		}
		return null;
	}

	/* ***************************************************************************************** */
	/*	Author : David Chung
	/*	Date : 19 Jun 2021
	/*  LUD  : 19 Jun 2021
	 *  Desc : update admin user */
	/** 	   
	 * 	@param $admin_id
	 * 	@param $input (object)
	 *  @return $user (object)
	/* ***************************************************************************************** */

	function updateUser($admin_id,$input){
		$update_string = "";
		foreach($input as $k=>$i){
			if($i=='null'){
				$update_string = $update_string."`$k` = null ,";
			} elseif($i=='' || $i==null){
				// update nothing
			}	else {
				$update_string = $update_string."`$k` = '".$this->gthis->escape_string($i)."' ,";
			}
		}
		$update_string = substr($update_string, 0, -1);
		// update query process
		$query = "update `tbl_admin` 
					 set  $update_string
				   where `".$this->uid ."` = '".$this->gthis->escape_string($admin_id)."';";
		$this->DB->update($query);

		$user = $this->getAdminFromAdminId($admin_id);
		return $user;
	}

	/* ***************************************************************************************** */
	/*	Author : David Chung
	/*	Date : 19 Jun 2021
	/*  LUD  : 19 Jun 2021
	 *  Desc : validate admin user password */
	/** 	   
	 * 	@param $password
	 *  @return $pass (bool)
	/* ***************************************************************************************** */

	function validatePasswordFormat($password){
		$pass = true;
		$password_length = strlen($password);
		if($password_length>=7 && $password_length<=15){
			// pass
		} else {
			$pass = false;
		}
		if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $password)){
		    $pass = false;
		}
		return $pass;
	}


	/* ***************************************************************************************** */
	/*	Author : David Chung
	/*	Date : 19 Jun 2021
	/*  LUD  : 19 Jun 2021
	 *  Desc : get all admin user */
	/** 	   
	 * 	@param N/A
	 *  @return $admin_list (list)
	/* ***************************************************************************************** */

	function getAllAdminUsers($activate='Y'){
		$query = "select * from `tbl_admin` where `activate` = '".$this->gthis->escape_string($activate)."';";
		$admin_list = $this->DB->get_Sql($query);
		if($admin_list){
			return $admin_list;
		}
		return null;
	}

  
}
?>