<?php
/* 
    Objective: gift card for redeem the card and earn the case dollar
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_gift_card
        tbl_gift_card_key

        
*/

require_once dirname(__FILE__). '/abstract.class.php';
class giftCard extends baseClass{

    function checkGiftCard($code){
        $sql = "SELECT * FROM `tbl_gift_card_key` WHERE `code` = '$code' AND use_datetime IS NULL LIMIT 1";
        $key = $this->DB->get_Sql($sql)[0];
        if ($key){
            return $key;
        } else {
            new apiDataBuilder(14);
        }
    }

    function executeGiftCard($key, $member_id){
        $sql = $this->DB->buildSql_update("tbl_gift_card_key", "map_id", $key['map_id'], [
            "used_by_id" => $member_id,
            "use_datetime" => "CURRENT_TIMESTAMP()"
        ]);
        $this->DB->update($sql);

        $sql = "SELECT * FROM `tbl_gift_card` WHERE card_id = '" . $key['card_id'] . "' LIMIT 1"; 
        $card = $this->DB->get_Sql($sql)[0];

        $this->DB->adjustFloatVal("tbl_member", "member_id", $member_id, "cash_dollar", $card['credit']);

        return true;
    }

    function editGiftCard($card_id, $data){
        // edit gift card
        // $card_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($card_id == 0)
            $card_id = $this->DB->insert_db('tbl_gift_card', null);

        // generate new gift card
        if ($data['quota']){
            for ($i = 0 ; $i < $data['quota'] ; $i++){
                $cardData = [
                    "card_id" => $card_id,
                    "code" => $this->random_str(10),
                    "updated_by_id" => $_SESSION['admin']['admin_id']
                ];
                $this->DB->insert_db('tbl_gift_card_key', $cardData);
            }

            unset($data['quota']);
        }

        $sql = $this->DB->buildSql_update("tbl_gift_card", "card_id", $card_id, $data);
        $this->DB->update($sql);

        return $this->getGiftCardDtl($card_id);
    }

    function getGiftCardList($shop_id = null, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: which shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $sql = "SELECT *, COUNT(c.card_id) AS card_ava
                FROM `tbl_gift_card` AS c
                LEFT JOIN `tbl_gift_card_key` AS k ON k.card_id = c.card_id
                WHERE c.`activate` ='Y' AND k.`activate` = 'Y' AND k.use_datetime IS NULL ";

        foreach($filter AS $k => $i){
            if ($i){
                switch($k){
                    case 'skey':
                        $sql .= " AND (c.card_id LIKE '%$i$' OR name_tc LIKE '%$i%' OR name_en LIKE '%$i%' OR k.`code` = '$i') ";
                        break;
                }
            }
        }

        $sql .= "GROUP BY c.card_id";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit']; 
        }
        $res = $this->DB->get_Sql($sql);

        return $res;
    }

    function getGiftCardDtl($card_id){
        // get the gift card Detail
        // $card_id: PK

        $sql = "SELECT *
        FROM `tbl_gift_card` AS c
        LEFT JOIN `tbl_gift_card_key` AS k ON k.card_id = c.card_id
        WHERE c.`activate` ='Y' AND k.`activate` = 'Y' AND c.card_id = '". $card_id ."'";
        $tmp = $this->DB->get_Sql($sql);

        $res = $tmp[0];
        $res['keyList'] = $tmp;

        return $res;
    }

}
?>