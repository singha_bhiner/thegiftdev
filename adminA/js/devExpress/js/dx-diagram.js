/*!
 * DevExpress Diagram (dx-diagram)
 * Version: 2.1.25
 * Build date: Tue Jul 27 2021
 * 
 * Copyright (c) 2012 - 2021 Developer Express Inc. ALL RIGHTS RESERVED
 * Read about DevExpress licensing here: https://www.devexpress.com/Support/EULAs
 */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["diagram"] = factory();
	else
		root["DevExpress"] = root["DevExpress"] || {}, root["DevExpress"]["diagram"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 108);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.ShapeType = exports.ShapeTypes = exports.ShapeCategories = void 0;
var ShapeCategories = /** @class */ (function () {
    function ShapeCategories() {
    }
    ShapeCategories.General = "general";
    ShapeCategories.Flowchart = "flowchart";
    ShapeCategories.OrgChart = "orgChart";
    ShapeCategories.Containers = "containers";
    ShapeCategories.Custom = "custom";
    return ShapeCategories;
}());
exports.ShapeCategories = ShapeCategories;
var ShapeTypes = /** @class */ (function () {
    function ShapeTypes() {
    }
    // Standard
    ShapeTypes.Text = "text";
    ShapeTypes.Rectangle = "rectangle";
    ShapeTypes.Ellipse = "ellipse";
    ShapeTypes.Cross = "cross";
    ShapeTypes.Triangle = "triangle";
    ShapeTypes.Diamond = "diamond";
    ShapeTypes.Heart = "heart";
    ShapeTypes.Pentagon = "pentagon";
    ShapeTypes.Hexagon = "hexagon";
    ShapeTypes.Octagon = "octagon";
    ShapeTypes.Star = "star";
    ShapeTypes.ArrowLeft = "arrowLeft";
    ShapeTypes.ArrowUp = "arrowTop";
    ShapeTypes.ArrowRight = "arrowRight";
    ShapeTypes.ArrowDown = "arrowBottom";
    ShapeTypes.ArrowUpDown = "arrowNorthSouth";
    ShapeTypes.ArrowLeftRight = "arrowEastWest";
    // Flowchart
    ShapeTypes.Process = "process";
    ShapeTypes.Decision = "decision";
    ShapeTypes.Terminator = "terminator";
    ShapeTypes.PredefinedProcess = "predefinedProcess";
    ShapeTypes.Document = "document";
    ShapeTypes.MultipleDocuments = "multipleDocuments";
    ShapeTypes.ManualInput = "manualInput";
    ShapeTypes.Preparation = "preparation";
    ShapeTypes.Data = "data";
    ShapeTypes.Database = "database";
    ShapeTypes.HardDisk = "hardDisk";
    ShapeTypes.InternalStorage = "internalStorage";
    ShapeTypes.PaperTape = "paperTape";
    ShapeTypes.ManualOperation = "manualOperation";
    ShapeTypes.Delay = "delay";
    ShapeTypes.StoredData = "storedData";
    ShapeTypes.Display = "display";
    ShapeTypes.Merge = "merge";
    ShapeTypes.Connector = "connector";
    ShapeTypes.Or = "or";
    ShapeTypes.SummingJunction = "summingJunction";
    // Containers
    ShapeTypes.Container = "container"; // abstract
    ShapeTypes.VerticalContainer = "verticalContainer";
    ShapeTypes.HorizontalContainer = "horizontalContainer";
    // Shapes with images
    ShapeTypes.Card = "card"; // abstract
    ShapeTypes.CardWithImageOnLeft = "cardWithImageOnLeft";
    ShapeTypes.CardWithImageOnTop = "cardWithImageOnTop";
    ShapeTypes.CardWithImageOnRight = "cardWithImageOnRight";
    return ShapeTypes;
}());
exports.ShapeTypes = ShapeTypes;
var ShapeType;
(function (ShapeType) {
    ShapeType[ShapeType["text"] = 0] = "text";
    ShapeType[ShapeType["rectangle"] = 1] = "rectangle";
    ShapeType[ShapeType["ellipse"] = 2] = "ellipse";
    ShapeType[ShapeType["cross"] = 3] = "cross";
    ShapeType[ShapeType["triangle"] = 4] = "triangle";
    ShapeType[ShapeType["diamond"] = 5] = "diamond";
    ShapeType[ShapeType["heart"] = 6] = "heart";
    ShapeType[ShapeType["pentagon"] = 7] = "pentagon";
    ShapeType[ShapeType["hexagon"] = 8] = "hexagon";
    ShapeType[ShapeType["octagon"] = 9] = "octagon";
    ShapeType[ShapeType["star"] = 10] = "star";
    ShapeType[ShapeType["arrowLeft"] = 11] = "arrowLeft";
    ShapeType[ShapeType["arrowTop"] = 12] = "arrowTop";
    ShapeType[ShapeType["arrowRight"] = 13] = "arrowRight";
    ShapeType[ShapeType["arrowBottom"] = 14] = "arrowBottom";
    ShapeType[ShapeType["arrowNorthSouth"] = 15] = "arrowNorthSouth";
    ShapeType[ShapeType["arrowEastWest"] = 16] = "arrowEastWest";
    // Flowchart
    ShapeType[ShapeType["process"] = 17] = "process";
    ShapeType[ShapeType["decision"] = 18] = "decision";
    ShapeType[ShapeType["terminator"] = 19] = "terminator";
    ShapeType[ShapeType["predefinedProcess"] = 20] = "predefinedProcess";
    ShapeType[ShapeType["document"] = 21] = "document";
    ShapeType[ShapeType["multipleDocuments"] = 22] = "multipleDocuments";
    ShapeType[ShapeType["manualInput"] = 23] = "manualInput";
    ShapeType[ShapeType["preparation"] = 24] = "preparation";
    ShapeType[ShapeType["data"] = 25] = "data";
    ShapeType[ShapeType["database"] = 26] = "database";
    ShapeType[ShapeType["hardDisk"] = 27] = "hardDisk";
    ShapeType[ShapeType["internalStorage"] = 28] = "internalStorage";
    ShapeType[ShapeType["paperTape"] = 29] = "paperTape";
    ShapeType[ShapeType["manualOperation"] = 30] = "manualOperation";
    ShapeType[ShapeType["delay"] = 31] = "delay";
    ShapeType[ShapeType["storedData"] = 32] = "storedData";
    ShapeType[ShapeType["display"] = 33] = "display";
    ShapeType[ShapeType["merge"] = 34] = "merge";
    ShapeType[ShapeType["connector"] = 35] = "connector";
    ShapeType[ShapeType["or"] = 36] = "or";
    ShapeType[ShapeType["summingJunction"] = 37] = "summingJunction";
    // Containers
    ShapeType[ShapeType["verticalContainer"] = 38] = "verticalContainer";
    ShapeType[ShapeType["horizontalContainer"] = 39] = "horizontalContainer";
    // Shapes with images
    ShapeType[ShapeType["cardWithImageOnLeft"] = 40] = "cardWithImageOnLeft";
    ShapeType[ShapeType["cardWithImageOnTop"] = 41] = "cardWithImageOnTop";
    ShapeType[ShapeType["cardWithImageOnRight"] = 42] = "cardWithImageOnRight";
})(ShapeType = exports.ShapeType || (exports.ShapeType = {}));


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Point = (function () {
    function Point(x, y) {
        this.x = x;
        this.y = y;
    }
    Point.zero = function () {
        return new Point(0, 0);
    };
    Point.fromNumber = function (num) {
        return new Point(num, num);
    };
    Point.prototype.isZero = function () {
        return this.x === 0 && this.y === 0;
    };
    Point.prototype.toString = function () {
        return JSON.stringify(this);
    };
    Point.prototype.copyFrom = function (obj) {
        this.x = obj.x;
        this.y = obj.y;
    };
    Point.prototype.clone = function () {
        return new Point(this.x, this.y);
    };
    Point.prototype.equals = function (obj) {
        return this.x === obj.x && this.y === obj.y;
    };
    Point.prototype.offset = function (offsetX, offsetY) {
        this.x += offsetX;
        this.y += offsetY;
        return this;
    };
    Point.prototype.offsetByPoint = function (offset) {
        this.x += offset.x;
        this.y += offset.y;
        return this;
    };
    Point.prototype.multiply = function (multiplierX, multiplierY) {
        this.x *= multiplierX;
        this.y *= multiplierY;
        return this;
    };
    Point.prototype.negative = function () {
        this.x *= -1;
        this.y *= -1;
        return this;
    };
    Point.prototype.applyConverter = function (converter) {
        this.x = converter(this.x);
        this.y = converter(this.y);
        return this;
    };
    Point.plus = function (a, b) {
        return new Point(a.x + b.x, a.y + b.y);
    };
    Point.minus = function (a, b) {
        return new Point(a.x - b.x, a.y - b.y);
    };
    Point.xComparer = function (a, b) {
        return a.x - b.x;
    };
    Point.yComparer = function (a, b) {
        return a.y - b.y;
    };
    Point.equals = function (a, b) {
        return a.x === b.x && a.y === b.y;
    };
    return Point;
}());
exports.Point = Point;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Size = (function () {
    function Size(width, height) {
        this.width = width;
        this.height = height;
    }
    Size.empty = function () {
        return new Size(0, 0);
    };
    Size.fromNumber = function (num) {
        return new Size(num, num);
    };
    Size.initByCommonAction = function (action) {
        var widthAdp = function (s) { return s.width; };
        var heightAdp = function (s) { return s.height; };
        return new Size(action(widthAdp, heightAdp), action(heightAdp, widthAdp));
    };
    Size.prototype.isEmpty = function () {
        return this.width === 0 && this.height === 0;
    };
    Size.prototype.toString = function () {
        return JSON.stringify(this);
    };
    Size.prototype.nonNegativeSize = function () {
        if (this.width < 0)
            this.width = 0;
        if (this.height < 0)
            this.height = 0;
        return this;
    };
    Size.prototype.offset = function (offsetWidth, offsetHeight) {
        this.width = this.width + offsetWidth;
        this.height = this.height + offsetHeight;
        return this;
    };
    Size.prototype.multiply = function (multiplierW, multiplierH) {
        this.width *= multiplierW;
        this.height *= multiplierH;
        return this;
    };
    Size.prototype.equals = function (obj) {
        return this.width === obj.width && this.height === obj.height;
    };
    Size.prototype.clone = function () {
        return new Size(this.width, this.height);
    };
    Size.prototype.copyFrom = function (obj) {
        this.width = obj.width;
        this.height = obj.height;
    };
    Size.prototype.applyConverter = function (conv) {
        this.width = conv(this.width);
        this.height = conv(this.height);
        return this;
    };
    Size.equals = function (a, b) {
        return a.width === b.width && a.height === b.height;
    };
    return Size;
}());
exports.Size = Size;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.ObjectUtils = exports.GeometryUtils = exports.Utils = exports.EventDispatcher = exports.Range = exports.LineEquation = void 0;
__webpack_require__(175);
var size_1 = __webpack_require__(2);
var point_1 = __webpack_require__(1);
var rectangle_1 = __webpack_require__(12);
var search_1 = __webpack_require__(41);
var metrics_1 = __webpack_require__(35);
var math_1 = __webpack_require__(32);
var vector_1 = __webpack_require__(58);
var segment_1 = __webpack_require__(33);
var Style_1 = __webpack_require__(27);
var LineEquation = /** @class */ (function () {
    function LineEquation(aParam, bParam, cParam) {
        this.aParam = aParam;
        this.bParam = bParam;
        this.cParam = cParam;
    }
    LineEquation.fromPoints = function (pointA, pointB, accuracy) {
        if (accuracy === void 0) { accuracy = 0.00001; }
        return !GeometryUtils.areDuplicatedPoints(pointA, pointB, accuracy) ?
            new LineEquation(pointB.y - pointA.y, pointA.x - pointB.x, pointB.x * pointA.y - pointA.x * pointB.y) : undefined;
    };
    LineEquation.prototype.getPointIntersection = function (other, accuracy) {
        if (accuracy === void 0) { accuracy = 0.00001; }
        var A1 = this.aParam;
        var B1 = this.bParam;
        var C1 = this.cParam;
        var A2 = other.aParam;
        var B2 = other.bParam;
        var C2 = other.cParam;
        var v = A2 * B1 - A1 * B2;
        if (math_1.MathUtils.numberCloseTo(v, 0, accuracy))
            return null;
        if (A1 === 0) {
            var x = (B2 * C1 - C2 * B1) / (B1 * A2);
            return this.createPoint(x, -C1 / B1);
        }
        var y = (C2 * A1 - C1 * A2) / v;
        return this.createPoint((-B1 * y - C1) / A1, y);
    };
    LineEquation.prototype.containsPoint = function (point, accuracy) {
        if (accuracy === void 0) { accuracy = 0.00001; }
        return math_1.MathUtils.numberCloseTo(this.aParam * point.x + this.bParam * point.y + this.cParam, 0, accuracy);
    };
    LineEquation.prototype.createPoint = function (x, y, accuracy) {
        if (accuracy === void 0) { accuracy = 0.00001; }
        return new point_1.Point(math_1.MathUtils.numberCloseTo(x, 0, accuracy) ? 0 : x, math_1.MathUtils.numberCloseTo(y, 0, accuracy) ? 0 : y);
    };
    return LineEquation;
}());
exports.LineEquation = LineEquation;
var Range = /** @class */ (function () {
    function Range(from, to) {
        this.from = from;
        this.to = to !== undefined ? to : from;
    }
    Object.defineProperty(Range.prototype, "length", {
        get: function () {
            return Math.abs(this.to - this.from);
        },
        enumerable: false,
        configurable: true
    });
    Range.prototype.extend = function (range) {
        this.from = Math.min(range.from, this.from);
        this.to = Math.max(range.to, this.to);
    };
    Range.prototype.includes = function (value) {
        return value >= this.from && value <= this.to;
    };
    Range.fromLength = function (from, length) {
        return new Range(from, from + length);
    };
    return Range;
}());
exports.Range = Range;
var EventDispatcher = /** @class */ (function () {
    function EventDispatcher() {
        this.listeners = [];
    }
    EventDispatcher.prototype.add = function (listener) {
        if (!listener)
            throw new Error("Not Implemented");
        if (!this.hasEventListener(listener))
            this.listeners.push(listener);
    };
    EventDispatcher.prototype.remove = function (listener) {
        for (var i = 0, currentListener = void 0; currentListener = this.listeners[i]; i++)
            if (currentListener === listener) {
                this.listeners.splice(i, 1);
                break;
            }
    };
    EventDispatcher.prototype.raise = function (funcName) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        for (var i = 0, listener = void 0; listener = this.listeners[i]; i++) {
            var func = listener[funcName];
            func && func.apply(listener, args);
        }
    };
    EventDispatcher.prototype.raise1 = function (action) {
        for (var i = 0, listener = void 0; listener = this.listeners[i]; i++)
            action(listener);
    };
    EventDispatcher.prototype.hasEventListener = function (listener) {
        for (var i = 0, l = this.listeners.length; i < l; i++)
            if (this.listeners[i] === listener)
                return true;
        return false;
    };
    return EventDispatcher;
}());
exports.EventDispatcher = EventDispatcher;
var Utils = /** @class */ (function () {
    function Utils() {
    }
    Utils.flatten = function (arr) {
        return [].concat.apply([], arr);
    };
    return Utils;
}());
exports.Utils = Utils;
var GeometryUtils = /** @class */ (function () {
    function GeometryUtils() {
    }
    GeometryUtils.getCommonRectangle = function (rects) {
        if (!rects.length)
            return new rectangle_1.Rectangle(0, 0, 0, 0);
        var minX = Number.MAX_VALUE;
        var maxX = -Number.MAX_VALUE;
        var minY = Number.MAX_VALUE;
        var maxY = -Number.MAX_VALUE;
        rects.forEach(function (rect) {
            minX = Math.min(minX, rect.x);
            maxX = Math.max(maxX, rect.right);
            minY = Math.min(minY, rect.y);
            maxY = Math.max(maxY, rect.bottom);
        });
        return new rectangle_1.Rectangle(minX, minY, maxX - minX, maxY - minY);
    };
    GeometryUtils.findFreeSpace = function (rects, size, exact, targetRect) {
        var xs = [targetRect ? targetRect.x : 0];
        var ys = [targetRect ? targetRect.y : 0];
        rects.forEach(function (r) {
            xs.push(r.x);
            xs.push(r.right);
            ys.push(r.y);
            ys.push(r.bottom);
        });
        xs = xs.sort(function (a, b) { return a - b; }).reduce(function (acc, v, index) { return (xs[index - 1] !== v && acc.push(v) && acc) || acc; }, []); // magic for distinct
        ys = ys.sort(function (a, b) { return a - b; }).reduce(function (acc, v, index) { return (ys[index - 1] !== v && acc.push(v) && acc) || acc; }, []);
        var matrix = ys.map(function (y) { return xs.map(function (x, i) { return xs[i + 1] - x; }); });
        var _loop_1 = function (i, rect) {
            var xi0 = search_1.SearchUtils.binaryIndexOf(xs, function (a) { return a - rect.x; });
            var xi1 = search_1.SearchUtils.binaryIndexOf(xs, function (a) { return a - rect.right; });
            var yi0 = search_1.SearchUtils.binaryIndexOf(ys, function (a) { return a - rect.y; });
            var yi1 = search_1.SearchUtils.binaryIndexOf(ys, function (a) { return a - rect.bottom; });
            for (var y = yi0; y < yi1; y++)
                for (var x = xi0; x < xi1; x++)
                    matrix[y][x] *= -1;
        };
        for (var i = 0, rect = void 0; rect = rects[i]; i++) {
            _loop_1(i, rect);
        }
        for (var yi = 0; yi < ys.length; yi++)
            for (var xi = 0; xi < xs.length - 1; xi++) {
                var checkResult = this.checkRect(matrix, ys, xs, yi, xi, size, exact);
                if (checkResult > 0)
                    xi = checkResult;
                else if (checkResult === 0)
                    return new point_1.Point(xs[xi], ys[yi]);
            }
        return null;
    };
    // wtf
    GeometryUtils.checkRect = function (matrix, ys, xs, yimin, ximin, size, exact) {
        var height = 0;
        var width = 0;
        var ximax = xs.length - 2;
        for (var yi = yimin; yi < ys.length; yi++) {
            height = ys[yi + 1] - ys[yimin];
            for (var xi = ximin; xi <= ximax; xi++) {
                if (matrix[yi][xi] < 0)
                    return xi === 0 ? -1 : xi; // move left?
                width = xs[xi + 1] - xs[ximin];
                if (size.width <= width || (!exact && xi === xs.length - 2 && size.width / 2 <= width)) {
                    if (size.height <= height || (!exact && yi === ys.length - 2 && size.height / 2 <= height))
                        return 0;
                    ximax = xi;
                }
            }
        }
    };
    GeometryUtils.getArrowPoints = function (point, directionPoint, arrowHeight, arrowWidth) {
        if (point.x === directionPoint.x && point.y === directionPoint.y)
            return { point1: point.clone(), point2: point.clone(), point3: point.clone() };
        var catX = directionPoint.x - point.x;
        var catY = directionPoint.y - point.y;
        var hypotenuse = Math.sqrt(Math.pow(catX, 2) + Math.pow(catY, 2));
        var cos = catX / hypotenuse;
        var sin = catY / hypotenuse;
        var x1 = point.x + arrowHeight * cos + arrowWidth * sin;
        var y1 = point.y + arrowHeight * sin - arrowWidth * cos;
        var x2 = point.x + arrowHeight * cos - arrowWidth * sin;
        var y2 = point.y + arrowHeight * sin + arrowWidth * cos;
        var x3 = point.x + arrowHeight * cos;
        var y3 = point.y + arrowHeight * sin;
        return { point1: new point_1.Point(x1, y1), point2: new point_1.Point(x2, y2), point3: new point_1.Point(x3, y3) };
    };
    GeometryUtils.createSegments = function (points) {
        var result = [];
        for (var i = 1; i < points.length; i++)
            result.push(new segment_1.Segment(points[i - 1], points[i]));
        return result;
    };
    GeometryUtils.createRectagle = function (points) {
        var xarr = points.map(function (p) { return p.x; });
        var yarr = points.map(function (p) { return p.y; });
        var minX = xarr.reduce(function (prev, cur) { return Math.min(prev, cur); }, Number.MAX_VALUE);
        var maxX = xarr.reduce(function (prev, cur) { return Math.max(prev, cur); }, -Number.MAX_VALUE);
        var minY = yarr.reduce(function (prev, cur) { return Math.min(prev, cur); }, Number.MAX_VALUE);
        var maxY = yarr.reduce(function (prev, cur) { return Math.max(prev, cur); }, -Number.MAX_VALUE);
        return new rectangle_1.Rectangle(minX, minY, maxX - minX, maxY - minY);
    };
    GeometryUtils.createSegmentsFromRectangle = function (rect) {
        var result = [];
        var topLeft = new point_1.Point(rect.x, rect.y);
        var topRight = new point_1.Point(rect.right, rect.y);
        var bottomRight = new point_1.Point(rect.right, rect.bottom);
        var bottomLeft = new point_1.Point(rect.x, rect.bottom);
        result.push(new segment_1.Segment(topLeft, topRight));
        result.push(new segment_1.Segment(topRight, bottomRight));
        result.push(new segment_1.Segment(bottomRight, bottomLeft));
        result.push(new segment_1.Segment(bottomLeft, topLeft));
        return result;
    };
    GeometryUtils.areSegmentsCutRectangle = function (segments, rect) {
        if (!rect)
            return false;
        var rectanlePolygonalChain = GeometryUtils.createSegmentsFromRectangle(rect);
        var hasSegmentIn = false;
        var hasSegmentOut = false;
        var _loop_2 = function (i) {
            if (hasSegmentIn && hasSegmentOut)
                return { value: true };
            var segment = segments[i];
            if (segment.isIntersectedByRect(rect)) {
                var startPoint_1 = segment.startPoint;
                var endPoint_1 = segment.endPoint;
                var currentContainsStart = rect.containsPoint(startPoint_1);
                var currentContainsEnd = rect.containsPoint(endPoint_1);
                if (!currentContainsStart && !currentContainsEnd)
                    return { value: true };
                if (currentContainsStart && !currentContainsEnd) {
                    var rectLinesContainsStart_1 = rectanlePolygonalChain.filter(function (s) { return s.containsPoint(startPoint_1); });
                    if (rectLinesContainsStart_1.length > 0) {
                        var otherRectSegments = rectanlePolygonalChain.filter(function (s) {
                            if (rectLinesContainsStart_1.length === 1)
                                return !s.containsPoint(rectLinesContainsStart_1[0].startPoint) && !s.containsPoint(rectLinesContainsStart_1[0].endPoint);
                            return s !== rectLinesContainsStart_1[0] && s !== rectLinesContainsStart_1[1];
                        });
                        if (otherRectSegments.some(function (s) { return segment.isIntersected(s); }) && !hasSegmentIn)
                            hasSegmentIn = true;
                    }
                    if (!hasSegmentOut)
                        hasSegmentOut = true;
                    return "continue";
                }
                if (!currentContainsStart && currentContainsEnd) {
                    if (!hasSegmentIn) {
                        hasSegmentIn = true;
                        if (hasSegmentOut)
                            hasSegmentOut = false;
                    }
                    var rectLinesContainsEnd_1 = rectanlePolygonalChain.filter(function (s) { return s.containsPoint(endPoint_1); });
                    if (rectLinesContainsEnd_1.length > 0) {
                        var otherRectSegments = rectanlePolygonalChain.filter(function (s) {
                            if (rectLinesContainsEnd_1.length === 1)
                                return !s.containsPoint(rectLinesContainsEnd_1[0].startPoint) && !s.containsPoint(rectLinesContainsEnd_1[0].endPoint);
                            return s !== rectLinesContainsEnd_1[0] && s !== rectLinesContainsEnd_1[1];
                        });
                        if (otherRectSegments.some(function (s) { return segment.isIntersected(s); }) && !hasSegmentOut)
                            hasSegmentOut = true;
                    }
                    return "continue";
                }
                var rectLinesContainsStart = rectanlePolygonalChain.filter(function (s) { return s.containsPoint(startPoint_1); });
                var rectLinesContainsEnd = rectanlePolygonalChain.filter(function (s) { return s.containsPoint(endPoint_1); });
                if (rectLinesContainsStart.length === 2 && rectLinesContainsEnd.length === 2)
                    return { value: true };
                if (rectLinesContainsStart.length === 1 && rectLinesContainsEnd.length === 1 &&
                    rectLinesContainsStart[0] !== rectLinesContainsEnd[0])
                    return { value: true };
                if (!hasSegmentOut && rectLinesContainsEnd.length === 1 && !rectLinesContainsStart.length)
                    hasSegmentOut = true;
                if (!hasSegmentIn && rectLinesContainsStart.length === 1 && !rectLinesContainsEnd.length) {
                    hasSegmentIn = true;
                    if (hasSegmentOut)
                        hasSegmentOut = false;
                }
            }
        };
        for (var i = 0; i < segments.length; i++) {
            var state_1 = _loop_2(i);
            if (typeof state_1 === "object")
                return state_1.value;
        }
        return hasSegmentIn && hasSegmentOut;
    };
    GeometryUtils.areIntersectedSegments = function (segments, otherSegments) {
        if (!otherSegments)
            return false;
        var segmentIndex = 0;
        var segment;
        while (segment = segments[segmentIndex]) {
            var otherSegmentIndex = 0;
            var otherSegment = void 0;
            while (otherSegment = otherSegments[otherSegmentIndex]) {
                if (otherSegment.isIntersected(segment))
                    return true;
                otherSegmentIndex++;
            }
            segmentIndex++;
        }
        return false;
    };
    GeometryUtils.isLineIntersected = function (beginLinePoint, endLinePoint, segment, excludeBeginPoint, excludeEndPoint) {
        var line = LineEquation.fromPoints(beginLinePoint, endLinePoint);
        var segmentStartPoint = segment.startPoint;
        var segmentEndPoint = segment.endPoint;
        if (line.containsPoint(segmentStartPoint) && line.containsPoint(segmentEndPoint))
            return !excludeBeginPoint && !excludeEndPoint;
        var segmentLine = LineEquation.fromPoints(segmentStartPoint, segmentEndPoint);
        var intersection = segmentLine.getPointIntersection(line);
        if (!intersection || !segment.containsPoint(intersection))
            return false;
        if (excludeBeginPoint)
            return !GeometryUtils.areDuplicatedPoints(segmentStartPoint, intersection);
        if (excludeEndPoint)
            return !GeometryUtils.areDuplicatedPoints(segmentEndPoint, intersection);
        return true;
    };
    GeometryUtils.removeUnnecessaryPoints = function (points, removeCallback, checkCallback, accuracy) {
        if (checkCallback === void 0) { checkCallback = function (p) { return p !== undefined; }; }
        if (accuracy === void 0) { accuracy = 0.00001; }
        this.removeUnnecessaryPointsCore(points, removeCallback, checkCallback, accuracy);
        this.removeBackwardPoints(points, removeCallback, checkCallback, accuracy);
        this.removeUnnecessaryPointsCore(points, removeCallback, checkCallback, accuracy);
    };
    GeometryUtils.removeUnnecessaryRightAnglePoints = function (points, removeCallback, checkCallback, accuracy) {
        if (checkCallback === void 0) { checkCallback = function (p) { return p !== undefined; }; }
        if (accuracy === void 0) { accuracy = 0.00001; }
        this.removeUnnecessaryPointsCore(points, removeCallback, checkCallback, accuracy);
        this.removeBackwardPoints(points, removeCallback, checkCallback, accuracy);
        this.removeNotRightAnglePoints(points, removeCallback, checkCallback, accuracy);
        this.removeUnnecessaryPointsCore(points, removeCallback, checkCallback, accuracy);
    };
    GeometryUtils.removeUnnecessaryPointsCore = function (points, removeCallback, checkCallback, accuracy) {
        if (checkCallback === void 0) { checkCallback = function (p) { return p !== undefined; }; }
        if (accuracy === void 0) { accuracy = 0.00001; }
        this.removeDuplicatedPoints(points, removeCallback, checkCallback, accuracy);
        this.removeNotCornersPoints(points, removeCallback, checkCallback, accuracy);
    };
    GeometryUtils.removeNotRightAnglePoints = function (points, removeCallback, checkCallback, accuracy) {
        if (checkCallback === void 0) { checkCallback = function (p) { return p !== undefined; }; }
        if (accuracy === void 0) { accuracy = 0.00001; }
        var index = 0;
        var point;
        while ((point = points[index]) && points.length > 2) {
            var nextPoint = this.getNextPoint(points, index, 1, checkCallback);
            var prevPoint = this.getNextPoint(points, index, -1, checkCallback);
            if (!prevPoint || !nextPoint ||
                GeometryUtils.isRightAngleCorner(prevPoint, point, nextPoint, accuracy) ||
                !removeCallback(point, index))
                index++;
        }
    };
    GeometryUtils.removeDuplicatedPoints = function (points, removeCallback, checkCallback, accuracy) {
        if (checkCallback === void 0) { checkCallback = function (p) { return p !== undefined; }; }
        if (accuracy === void 0) { accuracy = 0.00001; }
        var index = 0;
        var point;
        while ((point = points[index]) && points.length > 2) {
            var nextPoint = this.getNextPoint(points, index, 1, checkCallback);
            if (nextPoint && GeometryUtils.areDuplicatedPoints(point, nextPoint, accuracy)) {
                var actualIndex = index === points.length - 2 ? index : index + 1;
                if (removeCallback(points[actualIndex], actualIndex))
                    continue;
            }
            index++;
        }
    };
    GeometryUtils.removeNotCornersPoints = function (points, removeCallback, checkCallback, accuracy) {
        if (checkCallback === void 0) { checkCallback = function (p) { return p !== undefined; }; }
        if (accuracy === void 0) { accuracy = 0.00001; }
        var index = 0;
        var point;
        while ((point = points[index]) && points.length > 2) {
            var nextPoint = this.getNextPoint(points, index, 1, checkCallback);
            var prevPoint = this.getNextPoint(points, index, -1, checkCallback);
            if (!prevPoint || !nextPoint ||
                GeometryUtils.isCorner(prevPoint, point, nextPoint, accuracy) ||
                !removeCallback(point, index))
                index++;
        }
    };
    GeometryUtils.removeBackwardPoints = function (points, removeCallback, checkCallback, accuracy) {
        if (checkCallback === void 0) { checkCallback = function (p) { return p !== undefined; }; }
        if (accuracy === void 0) { accuracy = 0.00001; }
        var index = 0;
        var point;
        while ((point = points[index]) && points.length > 2) {
            var nextPoint = this.getNextPoint(points, index, 1, checkCallback);
            var prevPoint = this.getNextPoint(points, index, -1, checkCallback);
            if (!prevPoint || !nextPoint ||
                !GeometryUtils.isBackwardPoint(prevPoint, point, nextPoint, accuracy) ||
                !removeCallback(point, index))
                index++;
        }
    };
    GeometryUtils.isRightAngleCorner = function (prev, current, next, accuracy) {
        if (accuracy === void 0) { accuracy = 0.00001; }
        return math_1.MathUtils.numberCloseTo(GeometryUtils.createAngle(prev, current, next), Math.PI / 2.0, accuracy) ||
            math_1.MathUtils.numberCloseTo(GeometryUtils.createAngle(prev, current, next), Math.PI, accuracy) ||
            math_1.MathUtils.numberCloseTo(GeometryUtils.createAngle(prev, current, next), 3.0 * Math.PI / 2.0, accuracy);
    };
    GeometryUtils.isCorner = function (prev, current, next, accuracy) {
        if (accuracy === void 0) { accuracy = 0.00001; }
        return !math_1.MathUtils.numberCloseTo(GeometryUtils.createAngle(prev, current, next), 0, accuracy);
    };
    GeometryUtils.areDuplicatedPoints = function (current, next, accuracy) {
        if (accuracy === void 0) { accuracy = 0.00001; }
        return (math_1.MathUtils.numberCloseTo(current.x, next.x, accuracy) && math_1.MathUtils.numberCloseTo(current.y, next.y, accuracy));
    };
    GeometryUtils.isBackwardPoint = function (prev, current, next, accuracy) {
        if (accuracy === void 0) { accuracy = 0.00001; }
        return math_1.MathUtils.numberCloseTo(GeometryUtils.createAngle(prev, current, next), Math.PI, accuracy);
    };
    GeometryUtils.createAngle = function (prev, current, next) {
        var vector1 = vector_1.Vector.fromPoints(current, next);
        var vector2 = vector_1.Vector.fromPoints(prev, current);
        var vector1X = vector1.x;
        var vector1Y = vector1.y;
        var vector2X = vector2.x;
        var vector2Y = vector2.y;
        var atan = Math.atan2(vector1X * vector2Y - vector2X * vector1Y, vector1X * vector2X + vector1Y * vector2Y);
        return atan < 0 ? 2 * Math.PI + atan : atan;
    };
    GeometryUtils.getNextPoint = function (points, index, step, checkCallback) {
        var result;
        var newIndex = index + step;
        while (result = points[newIndex]) {
            if (checkCallback(result))
                return result;
            newIndex = newIndex + step;
        }
    };
    GeometryUtils.addSelectedLinesTo = function (prevPt, pt, nextPt, offsetX, offsetY, offsetXNegative, offsetYNegative, nextOffsetX, nextOffsetY, nextOffsetXNegative, nextOffsetYNegative, addSelectedLine, addSelectedLineWB, accuracy) {
        if (accuracy === void 0) { accuracy = 0.00001; }
        var a1 = pt.y - prevPt.y;
        var a2 = nextPt.y - pt.y;
        var b1 = prevPt.x - pt.x;
        var b2 = pt.x - nextPt.x;
        var det = a1 * b2 - a2 * b1;
        if (!math_1.MathUtils.numberCloseTo(det, 0, accuracy)) {
            var c1 = a1 * (prevPt.x + offsetX) + b1 * (prevPt.y + offsetY);
            var c2 = a2 * (pt.x + nextOffsetX) + b2 * (pt.y + nextOffsetY);
            addSelectedLine((b2 * c1 - b1 * c2) / det, (a1 * c2 - a2 * c1) / det);
            var c1WB = a1 * (prevPt.x + offsetXNegative) + b1 * (prevPt.y + offsetYNegative);
            var c2WB = a2 * (pt.x + nextOffsetXNegative) + b2 * (pt.y + nextOffsetYNegative);
            addSelectedLineWB((b2 * c1WB - b1 * c2WB) / det, (a1 * c2WB - a2 * c1WB) / det);
        }
    };
    GeometryUtils.getSelectionOffsetPoint = function (prev, current, distance) {
        return new point_1.Point((prev.y - current.y) / distance, (current.x - prev.x) / distance);
    };
    GeometryUtils.getSelectionTextStartEndPoints = function (prev, current, distance, center, size, align) {
        var cos = (current.x - prev.x) / distance;
        var sin = (current.y - prev.y) / distance;
        var width = size.width * cos + size.height * sin;
        switch (align) {
            case Style_1.TextAlignment.Left:
                return [center, new point_1.Point(center.x + cos * width, center.y + sin * width)];
            case Style_1.TextAlignment.Right:
                return [new point_1.Point(center.x - cos * width, center.y - sin * width), center];
            default:
                return [
                    new point_1.Point(center.x - 0.5 * cos * width, center.y - 0.5 * sin * width),
                    new point_1.Point(center.x + 0.5 * cos * width, center.y + 0.5 * sin * width)
                ];
        }
    };
    GeometryUtils.getPathLength = function (points) {
        var length = 0;
        var prevPt;
        points.forEach(function (pt) {
            if (prevPt !== undefined)
                length += metrics_1.Metrics.euclideanDistance(pt, prevPt);
            prevPt = pt;
        });
        return length;
    };
    GeometryUtils.getPathPointByPosition = function (points, relativePosition) {
        if (!points.length)
            throw new Error("Invalid points");
        if (0 > relativePosition || relativePosition > 1)
            throw new Error("Invalid relative position");
        var length = this.getPathLength(points);
        if (points.length <= 2 && length === 0 || relativePosition === 0)
            return [points[0], 0];
        var targetLength = length * relativePosition;
        var currentLength = 0;
        for (var i = 1; i < points.length; i++) {
            var lineLength = metrics_1.Metrics.euclideanDistance(points[i], points[i - 1]);
            if (currentLength + lineLength >= targetLength) {
                var delta = targetLength - currentLength;
                var cos = (points[i].x - points[i - 1].x) / lineLength;
                var sin = (points[i].y - points[i - 1].y) / lineLength;
                return [new point_1.Point(points[i - 1].x + cos * delta, points[i - 1].y + sin * delta), i];
            }
            currentLength += lineLength;
        }
        return [points[points.length - 1], points.length - 1];
    };
    GeometryUtils.getLineAngle = function (beginPoint, endPoint) {
        return Math.atan2(endPoint.y - beginPoint.y, endPoint.x - beginPoint.x);
    };
    GeometryUtils.getTriangleBeginAngle = function (beginPoint, endPoint, point) {
        var lineAngle = this.getLineAngle(beginPoint, endPoint);
        var beginPointAngle = this.getLineAngle(beginPoint, point);
        return Math.abs(beginPointAngle - lineAngle);
    };
    GeometryUtils.getTriangleEndAngle = function (beginPoint, endPoint, point) {
        var lineAngle = this.getLineAngle(beginPoint, endPoint);
        var endPointAngle = this.getLineAngle(point, endPoint);
        return Math.abs(lineAngle - endPointAngle);
    };
    GeometryUtils.getPathPointByPoint = function (points, point) {
        if (!points.length)
            throw new Error("Invalid points");
        if (points.length === 1)
            return points[0];
        var distance = Number.MAX_VALUE;
        var result;
        for (var i = 1; i < points.length; i++) {
            var beginPoint = points[i - 1];
            var endPoint = points[i];
            if (point.equals(beginPoint)) {
                result = beginPoint.clone();
                break;
            }
            if (point.equals(endPoint)) {
                result = endPoint.clone();
                break;
            }
            var beginAngle = this.getTriangleBeginAngle(beginPoint, endPoint, point);
            var endAngle = this.getTriangleEndAngle(beginPoint, endPoint, point);
            var beginDistance = metrics_1.Metrics.euclideanDistance(point, beginPoint);
            var endDistance = metrics_1.Metrics.euclideanDistance(point, endPoint);
            var orthOffset = beginDistance * Math.sin(beginAngle);
            var currentDistance = void 0;
            if (Math.PI / 2 <= beginAngle && beginAngle <= Math.PI * 3 / 2)
                currentDistance = beginDistance;
            else if (Math.PI / 2 <= endAngle && endAngle <= Math.PI * 3 / 2)
                currentDistance = endDistance;
            else
                currentDistance = Math.abs(orthOffset);
            if (currentDistance < distance) {
                distance = currentDistance;
                if (Math.PI / 2 <= beginAngle && beginAngle <= Math.PI * 3 / 2)
                    result = beginPoint.clone();
                else if (Math.PI / 2 <= endAngle && endAngle <= Math.PI * 3 / 2)
                    result = endPoint.clone();
                else {
                    var round = Math.fround || Math.round;
                    var lineAngle = this.getLineAngle(beginPoint, endPoint);
                    var offsetX = round(Math.abs(orthOffset * Math.sin(lineAngle)));
                    var offsetY = round(Math.abs(orthOffset * Math.cos(lineAngle)));
                    var isAbove = point.y - beginPoint.y < round((point.x - beginPoint.x) * Math.tan(lineAngle));
                    if (0 <= lineAngle && lineAngle <= Math.PI / 2) {
                        offsetX *= isAbove ? -1 : 1;
                        offsetY *= isAbove ? 1 : -1;
                    }
                    else if (Math.PI / 2 <= lineAngle && lineAngle <= Math.PI) {
                        offsetX *= isAbove ? 1 : -1;
                        offsetY *= isAbove ? 1 : -1;
                    }
                    else if (0 >= lineAngle && lineAngle >= -Math.PI / 2) {
                        offsetX *= isAbove ? 1 : -1;
                        offsetY *= isAbove ? 1 : -1;
                    }
                    else if (-Math.PI / 2 >= lineAngle && lineAngle >= -Math.PI) {
                        offsetX *= isAbove ? -1 : 1;
                        offsetY *= isAbove ? 1 : -1;
                    }
                    result = point.clone().offset(offsetX, offsetY);
                }
            }
        }
        return result;
    };
    GeometryUtils.getPathPositionByPoint = function (points, point, maxPositionCount) {
        if (maxPositionCount === void 0) { maxPositionCount = 100; }
        point = this.getPathPointByPoint(points, point);
        var length = this.getPathLength(points);
        var currentLength = 0;
        for (var i = 1; i < points.length; i++) {
            var beginPoint = points[i - 1];
            var endPoint = points[i];
            var lineLength = metrics_1.Metrics.euclideanDistance(endPoint, beginPoint);
            var angle = Math.atan((endPoint.y - beginPoint.y) / (endPoint.x - beginPoint.x));
            var round = Math.fround || Math.round;
            if ((point.x === endPoint.x && point.x === beginPoint.x) || (point.y === endPoint.y && point.y === beginPoint.y) ||
                round(point.y - beginPoint.y) === round((point.x - beginPoint.x) * Math.tan(angle))) {
                if (Math.sin(angle) !== 0)
                    currentLength += Math.abs((point.y - beginPoint.y) / Math.sin(angle));
                else
                    currentLength += Math.abs(point.x - beginPoint.x);
                return Math.round(currentLength * maxPositionCount / length) / maxPositionCount;
            }
            currentLength += lineLength;
        }
        return 1;
    };
    GeometryUtils.arePointsEqual = function (points1, points2) {
        var count1 = points1.length;
        var count2 = points2.length;
        if (count1 !== count2)
            return false;
        for (var i = 0; i < count1; i++)
            if (!points1[i].equals(points2[i]))
                return false;
        return true;
    };
    GeometryUtils.getMaxRectangleEnscribedInEllipse = function (ellipseSize) {
        var dx = ellipseSize.width * Math.sqrt(2) / 2;
        var dy = ellipseSize.height * Math.sqrt(2) / 2;
        return new size_1.Size(dx, dy);
    };
    GeometryUtils.getEllipseByEnscribedRectangle = function (rectSize) {
        return new size_1.Size(2 * rectSize.width / Math.sqrt(2), 2 * rectSize.height / Math.sqrt(2));
    };
    return GeometryUtils;
}());
exports.GeometryUtils = GeometryUtils;
var ObjectUtils = /** @class */ (function () {
    function ObjectUtils() {
    }
    ObjectUtils.cloneObject = function (source) {
        return source && Object.assign({}, source);
    };
    ObjectUtils.compareObjects = function (obj1, obj2) {
        if (obj1 === obj2)
            return true;
        if (typeof obj1 === "object" && typeof obj2 === "object")
            return this.isDeepEqual(obj1, obj2);
        return false;
    };
    ObjectUtils.isDeepEqual = function (obj1, obj2) {
        var props1 = obj1 ? Object.getOwnPropertyNames(obj1) : [];
        var props2 = obj2 ? Object.getOwnPropertyNames(obj2) : [];
        if (props1.length !== props2.length)
            return false;
        for (var i = 0; i < props1.length; i++) {
            var property = props1[i];
            switch (typeof obj1[property]) {
                case "object": {
                    if (!this.isDeepEqual(obj1[property], obj2[property]))
                        return false;
                    break;
                }
                case "number": {
                    if (!isNaN(obj1[property]) || !isNaN(obj2[property]))
                        if (obj1[property] !== obj2[property])
                            return false;
                    break;
                }
                default: {
                    if (obj1[property] !== obj2[property])
                        return false;
                }
            }
        }
        return true;
    };
    return ObjectUtils;
}());
exports.ObjectUtils = ObjectUtils;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.ModelUtils = void 0;
var unit_converter_1 = __webpack_require__(11);
var metrics_1 = __webpack_require__(35);
var point_1 = __webpack_require__(1);
var rectangle_1 = __webpack_require__(12);
var size_1 = __webpack_require__(2);
var vector_1 = __webpack_require__(58);
var math_1 = __webpack_require__(32);
var Enums_1 = __webpack_require__(49);
var AddConnectionHistoryItem_1 = __webpack_require__(50);
var AddConnectorHistoryItem_1 = __webpack_require__(64);
var AddConnectorPointHistoryItem_1 = __webpack_require__(188);
var AddShapeHistoryItem_1 = __webpack_require__(53);
var ChangeConnectorPointsHistoryItem_1 = __webpack_require__(86);
var ChangeShapeParametersHistoryItem_1 = __webpack_require__(115);
var DeleteConnectionHistoryItem_1 = __webpack_require__(65);
var DeleteConnectorHistoryItem_1 = __webpack_require__(116);
var DeleteShapeHistoryItem_1 = __webpack_require__(117);
var InsertToContainerHistoryItem_1 = __webpack_require__(189);
var MoveConnectorPointHistoryItem_1 = __webpack_require__(190);
var MoveShapeHistoryItem_1 = __webpack_require__(191);
var RemoveFromContainerHistoryItem_1 = __webpack_require__(192);
var ResizeShapeHistoryItem_1 = __webpack_require__(118);
var SetSelectionHistoryItem_1 = __webpack_require__(66);
var ModelResizeHistoryItem_1 = __webpack_require__(193);
var UpdatePositionsOnPageResizeHistoryItem_1 = __webpack_require__(194);
var ChangeConnectorPropertyHistoryItem_1 = __webpack_require__(88);
var ChangeConnectorTextHistoryItem_1 = __webpack_require__(67);
var ChangeLockedHistoryItem_1 = __webpack_require__(119);
var ChangeStyleHistoryItem_1 = __webpack_require__(89);
var ChangeStyleTextHistoryItem_1 = __webpack_require__(68);
var Graph_1 = __webpack_require__(69);
var GraphInfo_1 = __webpack_require__(195);
var Structures_1 = __webpack_require__(38);
var Utils_1 = __webpack_require__(3);
var Connector_1 = __webpack_require__(6);
var ConnectorProperties_1 = __webpack_require__(30);
var ConnectorRenderPoint_1 = __webpack_require__(29);
var ConnectorRenderPointsContext_1 = __webpack_require__(85);
var Shape_1 = __webpack_require__(14);
var ModelUtils = /** @class */ (function () {
    function ModelUtils() {
    }
    ModelUtils.setShapePosition = function (history, model, shape, newPosition) {
        if (!shape.position.equals(newPosition))
            history.addAndRedo(new MoveShapeHistoryItem_1.MoveShapeHistoryItem(shape.key, newPosition));
    };
    ModelUtils.setShapeSize = function (history, model, shape, newPosition, newSize) {
        if (!shape.size.equals(newSize) || !shape.position.equals(newPosition))
            history.addAndRedo(new ResizeShapeHistoryItem_1.ResizeShapeHistoryItem(shape.key, newPosition, newSize));
    };
    ModelUtils.addConnectorPoint = function (history, connectorKey, pointIndex, position) {
        history.addAndRedo(new AddConnectorPointHistoryItem_1.AddConnectorPointHistoryItem(connectorKey, pointIndex, position));
    };
    ModelUtils.deleteConnectorCustomPoints = function (history, connector) {
        if (connector.points.length > 2) {
            var oldContext = connector.createRenderPointsContext();
            if (connector.properties.lineOption === ConnectorProperties_1.ConnectorLineOption.Straight || !oldContext)
                history.addAndRedo(new ChangeConnectorPointsHistoryItem_1.ReplaceConnectorPointsHistoryItem(connector.key, [
                    connector.points[0].clone(),
                    connector.points[connector.points.length - 1].clone()
                ]));
            else {
                var beginPoint = connector.points[0].clone();
                var lastPoint = connector.points[connector.points.length - 1].clone();
                history.addAndRedo(new ChangeConnectorPointsHistoryItem_1.ChangeConnectorPointsHistoryItem(connector.key, [beginPoint, lastPoint], new ConnectorRenderPointsContext_1.ConnectorRenderPointsContext([
                    new ConnectorRenderPoint_1.ConnectorRenderPoint(beginPoint.x, beginPoint.y, 0),
                    new ConnectorRenderPoint_1.ConnectorRenderPoint(lastPoint.x, lastPoint.y, 1)
                ], false, oldContext.actualRoutingMode)));
            }
        }
    };
    ModelUtils.deleteConnectorUnnecessaryPoints = function (history, connector) {
        var oldRenderPoints = connector.getRenderPoints(true).map(function (p) { return p.clone(); });
        if (connector.properties.lineOption === ConnectorProperties_1.ConnectorLineOption.Straight) {
            var unnecessaryPoints = ModelUtils.createUnnecessaryRenderPoints(oldRenderPoints.filter(function (p) { return !p.skipped; }).map(function (p) { return p.clone(); }), connector.skippedRenderPoints, function (removedPoint) { return ModelUtils.findFirstPointIndex(oldRenderPoints, function (p) { return p.equals(removedPoint); }); });
            if (Object.keys(unnecessaryPoints).length)
                ModelUtils.replaceConnectorPoints(history, connector, unnecessaryPoints);
        }
        else {
            var newRenderPoints = oldRenderPoints.filter(function (p) { return !p.skipped; }).map(function (p) { return p.clone(); });
            var unnecessaryPoints = ModelUtils.createUnnecessaryRightAngleRenderPoints(newRenderPoints, connector.skippedRenderPoints, function (removedPoint) { return ModelUtils.findFirstPointIndex(oldRenderPoints, function (p) { return p.equals(removedPoint); }); });
            if (Object.keys(unnecessaryPoints).length) {
                var oldContext = connector.createRenderPointsContext();
                if (oldContext) {
                    var newPoints = ModelUtils.createNecessaryPoints(connector.points.map(function (p) { return p.clone(); }), unnecessaryPoints);
                    var newRenderContext = new ConnectorRenderPointsContext_1.ConnectorRenderPointsContext(ModelUtils.validateRenderPointIndexes(newPoints, newRenderPoints, 0), oldContext.lockCreateRenderPoints, oldContext.actualRoutingMode);
                    history.addAndRedo(new ChangeConnectorPointsHistoryItem_1.ChangeConnectorPointsHistoryItem(connector.key, newPoints, newRenderContext));
                }
                else
                    ModelUtils.replaceConnectorPoints(history, connector, unnecessaryPoints);
            }
        }
    };
    ModelUtils.replaceConnectorPoints = function (history, connector, unnecessaryPoints) {
        history.addAndRedo(new ChangeConnectorPointsHistoryItem_1.ReplaceConnectorPointsHistoryItem(connector.key, ModelUtils.createNecessaryPoints(connector.points.map(function (p) { return p.clone(); }), unnecessaryPoints)));
    };
    ModelUtils.skipUnnecessaryRenderPoints = function (points) {
        var clonedPoints = points.map(function (p) { return p.clone(); });
        ModelUtils.removeUnnecessaryRenderPoints(clonedPoints);
        points.forEach(function (p) { return p.skipped = clonedPoints.some(function (cp) { return cp.skipped && cp.equals(p); }); });
        points[0].skipped = false;
        points[points.length - 1].skipped = false;
    };
    ModelUtils.skipUnnecessaryRightAngleRenderPoints = function (points) {
        var clonedPoints = points.map(function (p) { return p.clone(); });
        ModelUtils.removeUnnecessaryRightAngleRenderPoints(clonedPoints);
        points.forEach(function (p) { return p.skipped = clonedPoints.some(function (cp) { return cp.skipped && cp.equals(p); }); });
        points[0].skipped = false;
        points[points.length - 1].skipped = false;
    };
    ModelUtils.removeUnnecessaryRenderPoints = function (points) {
        Utils_1.GeometryUtils.removeUnnecessaryPoints(points, function (pt, index) { return ModelUtils.removeUnnecessaryPoint(points, pt, index); }, function (pt) { return pt !== undefined && !pt.skipped; });
        points[0].skipped = false;
        points[points.length - 1].skipped = false;
    };
    ModelUtils.removeUnnecessaryRightAngleRenderPoints = function (points) {
        Utils_1.GeometryUtils.removeUnnecessaryRightAnglePoints(points, function (p, index) { return ModelUtils.removeUnnecessaryPoint(points, p, index); }, function (p) { return p !== undefined && !p.skipped; });
        points[0].skipped = false;
        points[points.length - 1].skipped = false;
    };
    ModelUtils.createUnnecessaryRenderPoints = function (renderPointsWithoutSkipped, skippedRenderPoints, getPosition, predicate) {
        if (predicate === void 0) { predicate = function (_) { return true; }; }
        var result = {};
        Utils_1.GeometryUtils.removeUnnecessaryPoints(renderPointsWithoutSkipped, function (removedPoint, removedIndex) {
            return ModelUtils.collectNotSkippedRenderPoints(result, renderPointsWithoutSkipped, removedPoint, removedIndex, getPosition, predicate);
        });
        ModelUtils.collectSkippedRenderPoints(result, skippedRenderPoints, getPosition, predicate);
        return result;
    };
    ModelUtils.createUnnecessaryRightAngleRenderPoints = function (renderPointsWithoutSkipped, skippedRenderPoints, getPosition, predicate) {
        if (predicate === void 0) { predicate = function (_) { return true; }; }
        var result = {};
        Utils_1.GeometryUtils.removeUnnecessaryRightAnglePoints(renderPointsWithoutSkipped, function (removedPoint, removedIndex) {
            return ModelUtils.collectNotSkippedRenderPoints(result, renderPointsWithoutSkipped, removedPoint, removedIndex, getPosition, predicate);
        });
        ModelUtils.collectSkippedRenderPoints(result, skippedRenderPoints, getPosition, predicate);
        return result;
    };
    ModelUtils.createNecessaryPoints = function (points, unnecessaryPoints) {
        var _this = this;
        var result = [];
        var lastPointIndex = points.length - 1;
        points.forEach(function (p, index) {
            if (index === 0 || index === lastPointIndex || _this.isNecessaryPoint(p, index, unnecessaryPoints))
                result.push(p.clone());
        });
        return result;
    };
    ModelUtils.isNecessaryPoint = function (point, pointIndex, unnecessaryPoints) {
        return !Object.keys(unnecessaryPoints).some(function (key) {
            var unnecessaryPoint = unnecessaryPoints[key];
            return unnecessaryPoint.pointIndex === pointIndex && Utils_1.GeometryUtils.areDuplicatedPoints(point, unnecessaryPoint);
        });
    };
    ModelUtils.collectSkippedRenderPoints = function (targetRenderPoints, skippedRenderPoints, getPosition, predicate) {
        if (predicate === void 0) { predicate = function (_) { return true; }; }
        skippedRenderPoints && skippedRenderPoints.forEach(function (skippedPoint) {
            if (predicate(skippedPoint)) {
                var positionIndex = getPosition(skippedPoint);
                if (targetRenderPoints[positionIndex] === undefined)
                    targetRenderPoints[positionIndex] = skippedPoint;
            }
        });
    };
    ModelUtils.collectNotSkippedRenderPoints = function (targetRenderPoints, sourceRenderPoints, removedPoint, removedIndex, getPosition, predicate) {
        if (predicate === void 0) { predicate = function (_) { return true; }; }
        if (!predicate(removedPoint))
            return false;
        var positionIndex = getPosition(removedPoint);
        if (targetRenderPoints[positionIndex] === undefined) {
            targetRenderPoints[positionIndex] = removedPoint;
            removedPoint.skipped = true;
            sourceRenderPoints.splice(removedIndex, 1);
        }
        return true;
    };
    ModelUtils.removeUnnecessaryPoint = function (points, point, removedIndex) {
        if (point.pointIndex === -1) {
            points.splice(removedIndex, 1);
            return true;
        }
        point.skipped = true;
        return false;
    };
    ModelUtils.validateRenderPointIndexes = function (points, renderPoints, startIndex) {
        var _this = this;
        var result = renderPoints.map(function (rp, i) { return new ConnectorRenderPoint_1.ConnectorRenderPoint(rp.x, rp.y, i >= startIndex && rp.pointIndex >= 0 ? _this.findFirstPointIndex(points, function (p) { return p.equals(rp); }) : rp.pointIndex, rp.skipped); });
        result[0].skipped = false;
        result[result.length - 1].skipped = false;
        return result;
    };
    ModelUtils.findFirstPointIndex = function (points, predicate) {
        if (!points || !predicate)
            return -1;
        for (var i = 0; i < points.length; i++)
            if (predicate(points[i]))
                return i;
        return -1;
    };
    ModelUtils.moveConnectorRightAnglePoints = function (history, connector, firstPoint, firstPointIndex, lastPoint, lastPointIndex) {
        if (!Utils_1.GeometryUtils.areDuplicatedPoints(connector.points[firstPointIndex], firstPoint) ||
            !Utils_1.GeometryUtils.areDuplicatedPoints(connector.points[lastPointIndex], lastPoint))
            history.addAndRedo(new MoveConnectorPointHistoryItem_1.MoveConnectorRightAnglePointsHistoryItem(connector.key, firstPointIndex, firstPoint, lastPointIndex, lastPoint));
    };
    ModelUtils.moveConnectorPoint = function (history, connector, pointIndex, newPosition) {
        if (!connector.points[pointIndex].equals(newPosition))
            history.addAndRedo(new MoveConnectorPointHistoryItem_1.MoveConnectorPointHistoryItem(connector.key, pointIndex, newPosition));
    };
    ModelUtils.updateConnectorAttachedPoints = function (history, model, connector) {
        history.beginTransaction();
        var beginContainer = connector.beginItem && model.findItemCollapsedContainer(connector.beginItem);
        var beginAttachedToContainer = beginContainer && (!connector.endItem || !model.isContainerItem(beginContainer, connector.endItem));
        var endContainer = connector.endItem && model.findItemCollapsedContainer(connector.endItem);
        var endAttachedToContainer = endContainer && (!connector.beginItem || !model.isContainerItem(endContainer, connector.beginItem));
        if (beginAttachedToContainer)
            this.updateConnectorBeginPoint(history, connector, beginContainer, (endAttachedToContainer && endContainer) || connector.endItem, function (index) { return beginContainer.getConnectionPointIndexForItem(connector.beginItem, index); });
        else
            this.updateConnectorBeginPoint(history, connector, connector.beginItem, (endAttachedToContainer && endContainer) || connector.endItem);
        if (endAttachedToContainer)
            this.updateConnectorEndPoint(history, connector, endContainer, function (index) { return endContainer.getConnectionPointIndexForItem(connector.beginItem, index); });
        else
            this.updateConnectorEndPoint(history, connector, connector.endItem);
        history.endTransaction();
    };
    ModelUtils.updateConnectorBeginPoint = function (history, connector, beginItem, endItem, getConnectionPointIndex) {
        if (beginItem) {
            var connectionPointIndex = getConnectionPointIndex !== undefined ?
                getConnectionPointIndex(connector.beginConnectionPointIndex) : connector.beginConnectionPointIndex;
            var targetPoint = connector.points[1];
            if (endItem && connector.points.length === 2)
                if (connector.endConnectionPointIndex !== -1)
                    targetPoint = endItem.getConnectionPointPosition(connector.endConnectionPointIndex, point_1.Point.zero());
                else
                    targetPoint = endItem.rectangle.center;
            var newPoint = beginItem.getConnectionPointPosition(connectionPointIndex, targetPoint);
            this.moveConnectorPoint(histo