<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;
		
		require_once ROOT. '/include/apiDataBuilder.class.php';
		
		switch ($this->gthis->thirdmode) {
			case 'getDriverSchedule':
				$this->getDriverSchedule();
				break;
			case 'getDriverSchedule2':
				$this->getDriverSchedule2();
				break;
			case 'getPackingDeliDtl':
				$this->getPackingDeliDtl();
				break;
			case 'cardComplete':
				$this->cardComplete();
				break;
			case 'editDeliItem':
				$this->editDeliItem();
				break;
            case 'editDeli':
                $this->editDeli();
                break;
            case 'batchEditDeli':
                $this->batchEditDeli();
                break;
            case 'editOrder':
                $this->editOrder();
                break;
			case 'getDeliList':
				$this->getDeliList();
				break;
			case 'voucherApply':
				$this->voucherApply();
				break;
			case 'couponApply';
				$this->couponApply();
				break;
			case 'orderCreate':
				$this->orderCreate();
				break;
			case 'getProductDtl':
				$this->getProductDtl();
				break;
			case 'checkHolidayAva':
				$this->checkHolidayAva();
				break;
			case 'getOrderList':
				$this->getOrderList();
				break;
			case 'editDepositSlip':
				$this->editDepositSlip();
				break;
			case 'approveDepositSlip':
				$this->approveDepositSlip();
				break;
			case 'editExcel':
				$this->editExcel();
				break;
			case 'packProduct':
				$this->packProduct();
				break;
			case 'handleOrder':
				$this->handleOrder();
				break;
			case 'acceptOrder':
				$this->acceptOrder();
				break;
			case 'packingDone':
				$this->packingDone();
				break;
			case 'getDistrictList':
				$this->getDistrictList();
				break;
			case 'getTimeslotList':
				$this->getTimeslotList();
				break;

			case 'getPackingList': // BFF
				$this->getPackingList();
				break;

			case 'getComposeList': // BFF
				$this->getComposeList();
				break;
			case 'compose':
				$this->compose();
				break;
			case 'cancelCompose':
				$this->cancelCompose();
				break;
			case 'completeCompose':
				$this->completeCompose();
				break;
			case 'completePack':
				$this->completePack();
				break;

			default:
				new apiDataBuilder(-84);
				break;
		}

		$result_array['lang'] = $this->lang;
		@$this->gthis->assign('result_array',$result_array);
	}

	function getDriverSchedule2(){
        require_once ROOT.'/include/class/driverSchedule2.class.php';
        $dScCls = new driverSchedule($this->DB, $this->lang, $this->isAdmin);

		$schedule_list = $dScCls->getDriverScheduleList();

		$res['schedule_list'] = $schedule_list;

		new apiDataBuilder(0, $res);
	}

	function getDriverSchedule(){
        require_once ROOT.'/include/class/driverSchedule.class.php';
        $dScCls = new driverSchedule($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/user.class.php';
        $userCls = new user($this->DB, $this->lang, $this->isAdmin);

		$driver_list = $userCls->getAdminList(["driver" => 'Y']);

		foreach($driver_list as $k => $driver) {
			$driver_list[$k]['schedule_list'] = $dScCls->getDriverScheduleList(["admin_id" => $driver['admin_id']]);
		}

		$res['driver_list'] = $driver_list;

		new apiDataBuilder(0, $res);
	}

	function completePack(){
        require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->lang, $this->isAdmin);

		$deli_id = $this->gthis->param(["key" => "deli_id"]);

		$orderCls->editDeli($deli_id, [
			"deli_status" => "packed"
		]);

		new apiDataBuilder(0, $res);
	}

	function completeCompose(){
        require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);

		$deli_id_arr = $this->gthis->param(["key" => "deli_id_arr", "type" => "intArrV1"]);

		foreach ($deli_id_arr as $deli_id){
			$orderCls->editDeli($deli_id, [
				"deli_status" => "readyToPack"
			]);

			$deli = $orderCls->getDeliDtl($deli_id);
			$item_list = $deli['itemList'];


			foreach($item_list as $item){
				$warehouseCls->packProduct($item['map_id'], $item['item_id'], $item['qty'] - $item['packed_qty']);
			}
		}

		new apiDataBuilder(0);
	}

	function cancelCompose(){
        require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->lang, $this->isAdmin);

		$deli_id_arr = $this->gthis->param(["key" => "deli_id_arr", "type" => "intArrV1"]);

		foreach ($deli_id_arr as $deli_id){
			$orderCls->editDeli($deli_id, [
				"composer_admin_id" => "NULL",
				"deli_status" => "confirmed"
			]);
		}

		new apiDataBuilder(0, $res);
	}

	function compose(){
        require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/item.class.php';
        $itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$admin = $this->gthis->param(["key" => "admin", "request" => "session"]);

		$deli_id_arr = $this->gthis->param(["key" => "deli_id_arr", "type" => "intArrV1"]);

		$item_list = [];
		$product = $orderCls->getDeliProduct($deli_id_arr);

		foreach ($deli_id_arr as $deli_id){
			$deli = $orderCls->getDeliDtl($deli_id);
			foreach($deli['itemList'] as $item){
				$itemSrc = $itemCls->getItemDtl($item['item_id']);

				if (($idx = array_search($item['item_id'], array_column($item_list, 'item_id'))) !== false){
					$item_list[$idx]['total_qty'] = strval( intval($item_list[$idx]['total_qty']) + $item['qty'] );

				} else {
					array_push($item_list, [
						"item_id" => $item['item_id'],
						"qty" => $item['qty'],
						"total_qty" => $item['qty'],
						"name" => $item['name'],
						"location" => ["TMP", "架A", "架B"],
						"barcode" => $itemSrc['barcode'],
					]);
				}
			}

			$orderCls->editDeli($deli_id, [
				"composer_admin_id" => $admin['admin_id'],
				"deli_status" => "packing"
			]);
		}

		$res['item_list'] = $item_list;
		$res['product'] = $product;

		new apiDataBuilder(0, $res);

	}

	function getPackingList(){
        require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->lang, $this->isAdmin);

		$filter = $this->gthis->paramBuilder([
            ["key" => "member_id"],
            ["key" => "deli_status", "defaultValue" => "readyToPack"],
            ["key" => "deli_date_start", "type" => "date"],
            ["key" => "deli_date_end", "type" => "date"],
            ["key" => "cate_id"],
            ["key" => "product_id"],
			["key" => "skey"]
        ]);
		
        $deliList = $orderCls->getComposeList($filter);
		$res['deliList'] = $deliList;

		$filter = $orderCls->getComposeFilter($filter);
		$res['filter'] = $filter;

		new apiDataBuilder(0, $res);
	}

	function getComposeList(){
        require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->lang, $this->isAdmin);

		$filter = $this->gthis->paramBuilder([
            ["key" => "member_id"],
            ["key" => "deli_status", "defaultValue" => "confirmed"],
            ["key" => "deli_date_start", "type" => "date"],
            ["key" => "deli_date_end", "type" => "date"],
            ["key" => "cate_id"],
			["key" => "skey"]
        ]);
		
        $deliList = $orderCls->getComposeList($filter);
		$res['deliList'] = $deliList;

		$filter = $orderCls->getComposeFilter($filter);
		$res['filter'] = $filter;

		new apiDataBuilder(0, $res);
	}

	function getDistrictList(){
		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->gthis->lang);

		$res['district_list'] = $deliveryCls->getDistrictList();

		new apiDataBuilder(0, $res);
	}

	function packingDone(){
		$deli_id = $this->gthis->param(["key" => "deli_id"]);
		
		new apiDataBuilder(0, $deli_id);
	}

	function acceptOrder(){
		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->lang, $this->isAdmin);

		$order_id = $this->gthis->param(["key" => "order_id", "type" => "int"]);

		$orderCls->editOrder($order_id, [
			"order_status" => 'confirmed'
		]);

		$order = $orderCls->getOrderDtl($order_id);

		foreach($order['deliList'] as $k => $deli){
			$deliData = [
				"deli_status" => "confirmed",
			];
			$orderCls->editDeli($deli['deli_id'], $deliData);
		}

		$member_id = $order['member_id'];

		if ($order['cash_dollar_gain'] && $member_id){
			require_once ROOT.'/include/class/member.class.php';
			$memberCls = new member($this->DB, $this->lang, $this->isAdmin);
			$member = $memberCls->getMemberDtl($member_id);
			if ($member['is_charge_cash_dollar'] == 'Y'){
				$memberCls->cashDollarGain($order['cash_dollar_gain'], $member_id);
			}
		}
		if ($order['point_gain'] && $member_id)
			$memberCls->pointGain($order['point_gain'], $member_id);
		if ($order['exp_gain'] && $member_id)
			$memberCls->expGain($order['exp_gain'], $member_id);

		new apiDataBuilder();
	}

	function handleOrder(){
		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->lang, $this->isAdmin);

		$order_id = $this->gthis->param(["key" => "order_id", "type" => "int"]);

		$orderCls->editOrder($order_id, [
			"handle_time" => "CURRENT_TIMESTAMP()",
			"handle_admin_id" => $_SESSION['admin']['admin_id']
		]);

		$res['order_id'] = $order_id;

		new apiDataBuilder(0, $res);
	}

	function getPackingDeliDtl(){
		if (intval($_SESSION['right']['order']) < 50 && $_SESSION['admin']['mobile_right'] != 'Y'){
			new apiDataBuilder(-62);
		}

		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/delivery.class.php'; 
        $delivery = new delivery($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/item.class.php'; 
        $item = new item($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/product.class.php';
        $product = new product($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/audit.class.php';
        $audit = new audit($this->DB, $this->lang, $this->isAdmin);

		
		$deli_id = $this->gthis->parm_sql('request', 'deli_id', 'N');
		$deliDtl = $orderCls->getDeliDtl($deli_id);

		// foreach ($deliDtl['itemList'] as $k => $item){
		// 	if ($item['auto'] == 'Y'){
		// 		unset($deliDtl['itemList'][$k]);
		// 	}
		// }

		$res['deliDtl'] = $deliDtl;
		

		new apiDataBuilder(0, $res);
	}

	function cardComplete(){
		if (intval($_SESSION['right']['order']) < 50){
			new apiDataBuilder(-62);
		}

		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->lang, $this->isAdmin);

		$deli_id = $this->gthis->parm_sql('request', 'deli_id', 'Y');
		
		$deliDtl = $orderCls->editDeli($deli_id, ["card_completed" => "Y"]);
		new apiDataBuilder(0, $deliDtl);
	}
	
	function packProduct(){
		require_once ROOT.'/include/class/warehouse.class.php';
		$warehouseCls = new warehouse($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->lang, $this->isAdmin);

		$location_id = $this->gthis->parm_sql('request', 'location_id', 'Y');
		$code = $this->gthis->parm_sql('request', 'code', 'Y'); // barcode || sku_id (ITEMID:[0-9])
		$deli_id = $this->gthis->parm_sql('request', 'deli_id', 'Y');
		$qty = $this->DB->gthis->parm_sql('request', 'qty');
		if (!$qty)
			$qty = 1;

		$item_id = $itemCls->codeToItemId($code);
		$location = $warehouseCls->getLocationDtl($location_id);
		$deli = $orderCls->getDeliDtl($deli_id);
		$deliItemArr = [];
		$requiredQty = 0;

		// finding those deliItem which is about this sku and not packed completely 
		foreach ($deli['itemList'] as $item){
			if ($item['item_id'] == $item_id && $item['packed_qty'] < $item['qty']){
				$requiredQty += $item['qty'] - $item['packed_qty'];
				array_push($deliItemArr, $item);
			}
		}


		if ($deliItemArr && $location['location_id'] && $item_id && $qty){
			if ($requiredQty >= $qty){
				foreach ($deliItemArr as $deliItem){
					if ($qty > $deliItem['qty'] - $deliItem['packed_qty']){
						$packQty = $deliItem['qty'] - $deliItem['packed_qty'];
					} else {
						$packQty = $qty;
					}
					$warehouseCls->packProduct($deliItem, $location['location_id'], $item_id, $packQty);
					$qty -= $packQty;
				}

				$deli = $orderCls->getDeliDtl($deli_id); // get the updated productDeliRecord
				// update the status
				// if any item is not packed yet? return 0 : return 32 as deli_status packed (completed)
				foreach ($deli['itemList'] as $item){
					if ($item['auto'] == "N" && $item['qty'] > $item['packed_qty']){
						$orderCls->editDeli($deli_id, ["deli_status" => "packing"]);
						$orderCls->editOrder($deli['order_id'], ["order_status" => "deliProcess"]);
						$res['deliDtl'] = $orderCls->getDeliDtl($deli_id);
						new apiDataBuilder(0, $res);
					}
				}

				$orderCls->editDeli($deli_id, ["deli_status" => "readyToPack"]);
				$res['deliDtl'] = $orderCls->getDeliDtl($deli_id);
				new apiDataBuilder(32, $res);
			} else {
				new apiDataBuilder(-37);
				// new apiDataBuilder(32, $orderCls->getDeliDtl);
			}
		} else {
			new apiDataBuilder(-36);
		}
		
		new apiDataBuilder(-99);
	}

	function editExcel(){
		if (intval($_SESSION['right']['order']) < 50){
			new apiDataBuilder(-62);
		}
		require_once ROOT.'/include/class/order.class.php';
		$order = new order($this->DB, $this->lang, $this->isAdmin);
		
		$excel_id = $this->gthis->parm_sql('request', 'excel_id');
		$data = [];
		$fieldArr = ["is_confirmed", "activate"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
			}
			$$field? $data[$field] = $$field: null;
		}

		$res = $order->editExcel($excel_id, $data);
		
		new apiDataBuilder(0, $res);
	}

	function editDepositSlip(){
		if (intval($_SESSION['right']['order']) < 50){
			new apiDataBuilder(-62);
		}
		require_once ROOT.'/include/class/order.class.php';
		$order = new order($this->DB, $this->lang, $this->isAdmin);
		
		$slip_id = $this->gthis->parm_sql('request', 'slip_id');
		$data = [];
		$fieldArr = ["order_no", "is_confirmed", "order_id", "activate", "submitter_name"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
			}
			$$field? $data[$field] = $$field: null;
		}

		$res = $order->editDepositSlip($slip_id, $data);
		
		new apiDataBuilder(0, $res);
	}

	function approveDepositSlip(){
		if (intval($_SESSION['right']['order']) < 50){
			new apiDataBuilder(-62);
		}
		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->lang, $this->isAdmin);
		
		$slip_id = $this->gthis->parm_sql('request', 'slip_id');
		$data = [];
		$fieldArr = ["order_no", "is_confirmed", "order_id", "activate", "submitter_name"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
			}
			$$field? $data[$field] = $$field: null;
		}

		$res = $orderCls->editDepositSlip($slip_id, $data);
		
		$orderCls->editOrder($order_id, ["order_status" => "paid"]);
		$order = $orderCls->getOrderDtl($order_id);

		foreach($order['deliList'] as $deli){
			$orderCls->editDeli($deli['deli_id'], ["deli_status" => "paid"]);
		}


		new apiDataBuilder(0, $res);
	}

	function getOrderList(){
		if (intval($_SESSION['right']['order']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql("session", 'shop_id','Y');

		$filter = $this->gthis->paramBuilder([
			["key" => "skey"],
			["key" => "order_no"],
		]);
		
        $res['orderList'] = $orderCls->getOrderList($shop_id, $filter, $limiter, $sorting);

		new apiDataBuilder(0, $res);
	}

	function checkHolidayAva(){
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		require_once ROOT.'/include/class/holiday.class.php';
		$holidayCls = new holiday($this->DB, $this->lang, $this->isAdmin);

		$holidayList = $holidayCls->getHolidayList($shop_id, ["isCurrent" => 'Y']);
		

		$deli_arr = $this->gthis->parm_sql('request', "deli");
		
		$deliList = json_decode(str_replace("\\", "", $deli_arr), true);
		foreach ($deliList as $k => $deli){
			$deli_date = intval(strtotime($deli['deli_date']));
			
			foreach ($holidayList as $kk => $holi){
				if (!$holi['product_id_arr'] || in_array($deli['product_id'], $holi['product_id_arr'])){
					// check is the product including in this holiday rule

					if (intval(strtotime($holi['start_date'])) <= $deli_date && intval(strtotime($holi['end_date'])) >= $deli_date) {
						// check it is within the time of holiday rule

						$assumeConsumed = $holi['assumeConsumed']? $holi['assumeConsumed'] + 1: $holi['consumed'] + 1;
						if ($holi['isDayoff'] == 'Y'){
							$errorMsg = "產品(".$deli['product_name'].")不能在假期制限(".$holi['adm_name'].")下單";
							new apiDataBuilder(13, $holi, $errorMsg);
						} else if ($assumeConsumed > $holi['quota_everyday']){
							$errorMsg = "產品(".$deli['product_name'].")已超過假期制限(".$holi['adm_name'].")每天可以下單數量<br/>每天可下單數量: ". $holi['quota_everyday'] . "<br/>今天已下單數量: ". $holi['consumed'];
							new apiDataBuilder(12, null, $errorMsg);
						} else {
							$holidayList[$kk]['assumeConsumed'] = $assumeConsumed;
						}
					} else {
						// echo "outsided<br/>";
						
						
						// if (strtotime($holi['start_date']) <= $deli_date ){
							
						// 	echo "star: ". strtotime($holi['start_date']) ."<br/>";
						// 	echo "date: ". $deli_date . "<BR/>";
						// }
						// if (strtotime($holi['end_date'] >= $deli_date)){
						// 	echo "B";
						// 	echo "date: ". $deli_date . "<BR/>";
						// 	echo "ende: ". strtotime($holi['end_date']) ."<br/>";
							
						// }
					}

					
				}
			}
		}

		new apiDataBuilder(0);
	}

	function getProductDtl(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/product.class.php';
		$product = new product($this->DB, $this->lang, $this->isAdmin);

		$product_id = $this->gthis->parm_sql('request', 'product_id', 'Y');
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$productDtl = $product->getProductDtl($shop_id, $product_id);

		$res = $productDtl;
		new apiDataBuilder(0, $res);
	}

	function orderCreate(){
		if (intval($_SESSION['right']['quickOrder']) < 50){
			new apiDataBuilder(-62);
		}

		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/order.class.php';
		$order = new order($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

		$memberDtl = $memberCls->getMemberDtl($member_id);
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$order_no = $order->genOrderNo($shop_id);
		$orderData = [
			"shop_id" => $shop_id,
			"order_status" => "waitingPayment",
			"order_datetime" => "NOW()",
			"order_no" => $order_no
		];
		if ($memberDtl['sales_admin_id'])
			$orderData['sales_admin_id'] = $memberDtl['sales_admin_id'];
		$orderFieldArr = ["member_id", "sender_name", "sender_email", "sender_phone", "remarks_from_client", "remarks_internal", "remarks_external", "coupon_code", "member_voucher_id", "cash_dollar"];
		$deliFieldArr = ["deli_arr"];

		foreach ($orderFieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			$$field? $orderData[$field] = $$field: null;
		}

		foreach ($deliFieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
				case "deli_arr":
					$$field = json_decode(str_replace("\\", "", $$field), true);
					break;
			}
			$$field? $deliData[$field] = $$field: null;
		}		
		
		$cate_arr = [];
		$product_arr = [];
		$product_price = 0;
		$shipping_fee = 0;
		$timeslot_fee = 0;

		foreach($deliData['deli_arr'] as $deliIdx => $deli){
			unset($deliData['deli_arr'][$deliIdx]['product_code']);
			unset($deliData['deli_arr'][$deliIdx]['product_name']);
			$deliData['deli_arr'][$deliIdx]['deli_no'] = $order->genDeliNo($order_no, $deli['deli_date']);
			$productDtl = $productCls->getProductDtl($shop_id, $deli['product_id']);
			$regionDtl = $deliveryCls->getRegionDtl($deli['deli_region_id']);
			$timeslotDtl = $deliveryCls->getTimeslotDtl($deli['deli_timeslot_id']);

			$deliData['deli_arr'][$deliIdx]['shop_id'] = $shop_id;
			$deliData['deli_arr'][$deliIdx]['deli_status'] = 'waitingPayment';


			$dp_price = $productDtl['act_price'];
			$itemList = $deli['itemList'];
			// check if it is additional price for the selected item (upgrade product)
			foreach ($productDtl['item_group_list'] as $k => $item_group){
				$sel = $itemList[$k]['item_id'];
				if ($item_group['item_list'] && $sel){
					foreach($item_group['item_list'] as $item){
						if ($item['item_id'] == $sel){
							$dp_price += $item['additional_price'];
						}
					}
				}
			}

			$dp_shipping_fee = $regionDtl['shipping_fee'] ? $regionDtl['shipping_fee'] : 0;
			$dp_timeslot_fee = $timeslotDtl['deli_fee'] ? $timeslotDtl['deli_fee'] : 0;
			
			$dp_addon_price = 0;
			foreach($deli['addon_list'] as $addon){
				$dp_addon_price += $addon['qty'] * $addon['product_price'];
			}
			
			$deliData['deli_arr'][$deliIdx]['product_price'] = $dp_price;
			$deliData['deli_arr'][$deliIdx]['shipping_fee'] = $dp_shipping_fee;
			$deliData['deli_arr'][$deliIdx]['timeslot_fee'] = $dp_timeslot_fee;
			$deliData['deli_arr'][$deliIdx]['addon_price'] = $dp_addon_price;
			$deliData['deli_arr'][$deliIdx]['deli_price'] = $dp_price + $dp_shipping_fee + $dp_timeslot_fee + $dp_addon_price;

			$product_price += $dp_price + $dp_addon_price;
			$shipping_fee += $dp_shipping_fee;
			$timeslot_fee += $dp_timeslot_fee;

			array_push($product_arr, $deli['product_id']);
			foreach($productDtl['cate'] as $cate){
				foreach($cate['subcate'] as $subcate){
					array_push($cate_arr, $subcate['cate_id']);
				}
			}
		}

		$final_price = $product_price + $shipping_fee + $timeslot_fee; 
		$orderData['product_price'] = $product_price;
		$orderData['shipping_fee'] = $shipping_fee;
		$orderData['timeslot_fee'] = $timeslot_fee;

		if ($coupon_code){
			require_once ROOT.'/include/class/coupon.class.php';
			$couponCls = new coupon($this->DB, $this->lang, $this->isAdmin);
			$couponDtl = $couponCls->getCouponDtlByCode($coupon_code, $final_price, $cate_arr, $product_arr);

			if ($couponDtl['sales_admin_id'])
				$orderData['sales_admin_id'] = $couponDtl['sales_admin_id'];
			$orderData['coupon_code_id'] = $couponDtl['coupon_id'];
			$orderData['coupon_discount'] = $couponDtl['act_discount'];
			$final_price -= $couponDtl['act_discount'];
		}

		if (!$coupon_code || ($couponDtl && $couponDtl['allow_voucher'] == 'Y' && ($couponDtl['member_only'] != 'Y' || $member_id)) ){
			// case 1: without coupon code
			// case 2: coupon && coupon allow voucher && not member only
			// case 3: coupon && coupon allow voucher && member only && member
			
			if ($member_voucher_id){
				require_once ROOT.'/include/class/voucher.class.php';
				$voucherCls = new voucher($this->DB, $this->lang, $this->isAdmin);
				$voucherDtl = $voucherCls->getVoucherDtlByMemberVoucher($member_voucher_id, $final_price, $cate_arr);
	
				$orderData['voucher_discount'] = $voucherDtl['act_discount'];
				$final_price -= $voucherDtl['act_discount'];

			}
		}

		if ($final_price < 0)
			$final_price = 0;

		require_once ROOT.'/include/class/shop.class.php';
		$shopCls = new shop($this->DB, $this->lang, $this->isAdmin);
		$shopDtl = $shopCls->getShopDtl($shop_id);
		$pointRatio = $shopDtl['point_ratio'];
		
		if ($product_price && $pointRatio)
			$orderData['cash_dollar_gain'] = round($product_price / $pointRatio);

		if ($cash_dollar && $member_id){
			if ($cash_dollar > $memberDtl['cash_dollar'])
				$cash_dollar = $memberDtl['cash_dollar'];

			if ($case_dollar > $final_price)
				$cash_dollar = $final_price;

			$orderData['cash_dollar_used'] = $cash_dollar;
			$final_price -= $cash_dollar;
		}

		if ($member_id){
			$orderData['point_gain'] = $orderData['product_price'];
			$orderData['exp_gain'] = $orderData['product_price'];
		}
		$orderData['final_price'] = $final_price;
		unset($orderData['cash_dollar']);

		$res = $order->createOrder($orderData, $deliData);
		if ($coupon_code)
			$couponCls->executeCoupon($coupon_code);
		if ($member_voucher_id)
			$voucherCls->executeVoucher($member_voucher_id);
		if ($orderData['cash_dollar_used'] && $member_id )
			$memberCls->cashDollarUsed($orderData['cash_dollar_used'], $member_id);
		if ($orderData['cash_dollar_gain'] && $member_id)
			$memberCls->cashDollarGain($orderData['cash_dollar_gain'], $member_id);
		if ($orderData['point_gain'] && $member_id)
			$memberCls->pointGain($orderData['point_gain'], $member_id);
		if ($orderData['exp_gain'] && $member_id)
			$memberCls->expGain($orderData['exp_gain'], $member_id);
		
		new apiDataBuilder(0, $res);
	}
	
	function voucherApply(){
        $member_voucher_id = $this->gthis->parm_sql("request", "member_voucher_id", 'Y');
		$final_price = $this->gthis->parm_sql("request", "final_price", 'Y');
		$deli_arr = $this->gthis->parm_sql("request", "deli_arr", 'Y');
		$shop_id = $this->gthis->parm_sql("session", "shop_id", 'Y');

        require_once ROOT.'/include/class/voucher.class.php';
        $voucherCls = new voucher($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/product.class.php';
        $productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$deliList = json_decode(str_replace("\\", "", $deli_arr), true);
		
		// get the productList related category
		$cate_arr = [];
		foreach($deliList AS $k => $deli){
			$product = $productCls->getProductDtl($deli['product_id'], $shop_id);
			foreach($product['cate'] as $kk => $cate){
				foreach($cate['subcate'] as $kkk => $subcate){
					array_push($cate_arr, $subcate['cate_id']);
				}
			}
		}

		$res['voucherDtl'] = $voucherCls->getVoucherDtlByMemberVoucher($member_voucher_id, $final_price, $cate_arr);
		
		if ($res['voucherDtl']){
			new apiDataBuilder(0, $res);
		} else {
			new apiDataBuilder(10);
		}
	}

	function couponApply(){
		$coupon_code = $this->gthis->parm_sql("request", "coupon_code", 'Y');
		$final_price = $this->gthis->parm_sql("request", "final_price", 'Y');
		$deli_arr = $this->gthis->parm_sql("request", "deli_arr", 'Y');
		$shop_id = $this->gthis->parm_sql("session", "shop_id", 'Y');

        require_once ROOT.'/include/class/coupon.class.php';
        $couponCls = new coupon($this->DB, $this->lang, $this->isAdmin);
        require_once ROOT.'/include/class/product.class.php';
        $productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$deliList = json_decode(str_replace("\\", "", $deli_arr), true);
		
		// get the productList related category and productList
		$cate_arr = [];
		$product_arr = [];
		foreach($deliList AS $k => $deli){
			array_push($product_arr, $deli['product_id']);
			$product = $productCls->getProductDtl($deli['product_id'], $shop_id);
			foreach($product['cate'] as $kk => $cate){
				foreach($cate['subcate'] as $kkk => $subcate){
					array_push($cate_arr, $subcate['cate_id']);
				}
			}
		}

		$res['couponDtl'] = $couponCls->getCouponDtlByCode($coupon_code, $final_price, $cate_arr, $product_arr);

		if ($res['couponDtl']){
			new apiDataBuilder(0, $res);
		} else {
			new apiDataBuilder(11);
		}
	}

	function getDeliList(){
        require_once ROOT.'/include/class/order.class.php';
        $orderCls = new order($this->DB, $this->lang, $this->isAdmin);

		$filter = $this->gthis->paramBuilder([
            ["key" => "member", "request" => "session"],
            ["key" => "deli_status"],
            ["key" => "deli_date_start", "type" => "date"],
            ["key" => "deli_date_end", "type" => "date"],
			["key" => "skey"],
            ["key" => "cate_id"],
			["key" => "member_id"],
			["key" => "driver_admin_id"]
        ]);

        $deliList = $orderCls->getDeliList("1,2,3", $filter);
		$res['deliList'] = $deliList;

		new apiDataBuilder(0, $res);
	}

    function editOrder(){
        if (intval($_SESSION['right']['order']) < 50){
			new apiDataBuilder(-62);
		}
		require_once ROOT.'/include/class/order.class.php';
		$order = new order($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "order_id", "type" => "int"],
			["key" => "sales_admin_id", "type" => "int"],
			["key" => "order_status"],
			["key" => "sender_name"],
			["key" => "sender_email"],
			["key" => "sender_phone"],
			["key" => "sender_company"],
			["key" => "remarks_from_client"],
			["key" => "remarks_internal"],
			["key" => "remarks_external"],
			["key" => "activate", "type" => "NY"],
		]);


		$res = $order->editOrder($data['order_id'], $data);
		
		new apiDataBuilder(0, $res);
    }

    function editDeli(){
        if (intval($_SESSION['right']['order']) < 50){
			new apiDataBuilder(-62);
		}

        require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->lang, $this->isAdmin);
		

		$data = $this->gthis->paramBuilder([
			["key" => "deli_id", "type" => "int"],
			["key" => "shop_id", "type" => "int"],
			["key" => "deli_timeslot_id", "type" => "int"],
			["key" => "deli_region_id", "type" => "int"],
			["key" => "order_id", "type" => "int"],
			["key" => "product_id", "type" => "int"],

			["key" => "driver_admin_id", "type" => "int"],
			["key" => "composer_admin_id", "type" => "int"],
			["key" => "incharge_admin_id", "type" => "int"],
			
			["key" => "itemList", "type" => "json"],

			["key" => "deli_status"],
			["key" => "recipient_name"],
			["key" => "recipient_phone"],
			["key" => "recipient_company"],
			["key" => "card_head"],
			["key" => "card_body"],
			["key" => "card_sign"],
			["key" => "remarks_internal"],
			["key" => "remarks_external"],
			["key" => "remarks_packing"],
			["key" => "remarks_driver"],
			["key" => "remarks_external"],
			["key" => "deli_address"],

			["key" => "deli_date", "type" => "date"],

			["key" => "deli_before_photo_path", "request" => "file"],
			["key" => "deli_after_photo_path", "request" => "file"],
			["key" => "activate", "type" => "NY"],
			
			["key" => "is_greeting_card_printed", "type" => "NY"],
			["key" => "is_compose_note_printed", "type" => "NY"],
			["key" => "is_receive_note_printed", "type" => "NY"],
			["key" => "is_blank_card_provide", "type" => "NY"],
			["key" => "is_client_card", "type" => "NY"],

		]);


		if ($data['order_id']){
			$order = $orderCls->getOrderDtl($data['order_id']);
			$data['order_no'] = $order['order_no'];
		}

		
		$res = $orderCls->editDeli($data['deli_id'], $data, $data['order_no']);
		
		new apiDataBuilder(0, $res);
    }

    function batchEditDeli(){
        if (intval($_SESSION['right']['order']) < 50){
			new apiDataBuilder(-62);
		}

        require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->lang, $this->isAdmin);
		
		$srcData = $this->gthis->paramBuilder([
			["key" => "action"],
		]);

		switch ($srcData['action']){
			case 'item_update':
				$data = $this->gthis->paramBuilder([
					["key" => "map_id_arr", "type" => "intArrV1"],
					["key" => "to_item_id"],				
					["key" => "qty"]					
				]);
				$orderCls->editDeliItemChangeItemId($data['map_id_arr'], $data['to_item_id'], $data['qty']); 		
				break;
		}
		
		new apiDataBuilder(0, $res);
    }

	function editDeliItem(){
		if (intval($_SESSION['right']['order']) < 50){
			new apiDataBuilder(-62);
		}
		require_once ROOT.'/include/class/order.class.php';
		$order = new order($this->DB, $this->lang, $this->isAdmin);
		
		$map_id = $this->gthis->parm_sql('request', 'map_id');
		$data = [];
		$fieldArr = ["deli_id", "sku_id", "qty", "remarks"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
				case 'map_id':
				case 'deli_id':
				case 'sku_id':
				case 'qty':
					$$field = $this->DB->parseInt($$field);
					break;
			}
			$$field? $data[$field] = $$field: null;
		}

		$res = $order->editDeliItem($map_id, $data);
		
		new apiDataBuilder(0, $res);
	}
}
?>