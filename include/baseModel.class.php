<?php
require_once dirname(__FILE__).'/config.php';
require_once dirname(__FILE__).'/commonHeader.php';
abstract class baseModel{
	public $tpl;
	public $DB;
	public $DBC;
	public $session;
	public $connectionIp;
	public $mode;
	public $home;
	public $lang;
	public $pwapage = 'N';
	public $isMobile = 'N';
	public $rm = 'N';
	public $navObject = array();
	public $refinedHeaders = array();
	public $whiteListIpArray = array();

	public function __construct($templates){
		if(isset($_REQUEST['mode']))
            $this->mode = $_REQUEST['mode'];

        // 檢查有否惡意的注入代碼
        $result = $this->prevent_injection($_REQUEST);
		if($result == 'N'){
			$xresult['code'] = '-100';
			$xresult['msg'] = 'X5 Security issue, action aborted';
			$this->json_return('result', $xresult, 'Y');
		}

		// 檢查是否排錯模式
		$this->error_display();

		// smarty 導入
		require_once LIB.'./templates_engine/Smarty.class.php';
		$this->tpl = new Smarty();
		$this->tpl->setTemplateDir('.'.$templates.'/templates');
		$this->tpl->setCompileDir('.'.$templates.'/templates_cache');
		$this->tpl->setForceCompile(true); // 是否每次都制作新的smarty compiled html
		// 數據庫準備
		require_once ROOT.'/include/DB.class.php';
		$this->DB = new DB($this);

		// SESSION
		require_once ROOT.'/include/Session.class.php';
		$this->session = new Session();
		$this->referer = REFERER;
		$this->home=HOST;
		$this->user_type='';
		$this->business_name= BUSINESS_NAME;
		$this->connectionIp= $this->get_connection_ip();;
		$this->whiteListIpArray = array('202.126.213.158','139.162.109.91','47.56.185.204',"2400:8902::f03c:91ff:fea4:e93d","119.247.179.230","2400:8901::f03c:91ff:fe59:8b9a");
		$this->assign('business_name',$this->business_name);
		$this->assign('homepage',$this->home);
		$this->assign('connection_scheme',$this->get_connection_scheme());
		$this->assign('timestamp',time() . "000"); // unix timestamp to 1970 format	
		$this->assign('pageOnly','N');
		$this->baseURL = $this->get_connection_scheme()."://".$this->home;
		$this->assign('baseURL',$this->baseURL);
		$this->assign('ENV',ENV);
		$this->assign('paypalId', PAYPAL_ID);
		$this->dev_mode_check();
		$this->countryCheck();
		// 檢查是否開發模式
		$this->assign('devMode',empty($_SESSION['devMode'])? null : $_SESSION['devMode']);
		$this->assign('currentVersion',CURRENT_VERSION);
		$this->assign('year',date('Y'));
		if(ENV=='DEV'){
			$this->assign('srcVersion',"?v=".time());
		} else {
			// PROD
			$this->assign('srcVersion',"?v=".CURRENT_VERSION);
		}

		// 語言準備
		$this->language_setup();
		$callPath = $_SESSION['callPath'];
		if (!$callPath)
			$callPath = [];

			// echo $this->baseURL . " || " . $_SERVER['HTTP_HOST'] . " || " . $_SERVER['REQUEST_URI'];
		// shop_id 準備, 等待最後domain 落實
		// if (!str_contains($_SERVER['REQUEST_URI'], 'admin')
		// 	&& !str_contains($_SERVER['REQUEST_URI'], 'css') 
		// 	&& !str_contains($_SERVER['REQUEST_URI'], 'js')
		// 	&& !str_contains($_SERVER['REQUEST_URI'], 'null')
		// 	&& $_SERVER['REQUEST_URI'] != ''){
			
		if ($_SERVER['REQUEST_URI'] == "/" && !str_contains($_SERVER['REQUEST_URI'], 'null')){
			if (str_contains($this->baseURL, 'gift')){
				$shop_id= 1;
			} else if (str_contains($this->baseURL, 'zuri')){
				$shop_id = 2;
			} else if (str_contains($this->baseURL, 'lotaipo')){
				$shop_id = 3; 
			} else {
				new apiDataBuilder(-999);
			}

			array_push($callPath, "|| $shop_id || " .$_SERVER['REQUEST_URI']);
		} else {
			$shop_id = $_SESSION['shop_id'];
		}

		// $shop_id = $_SESSION['shop_id'];

		if (!$shop_id){
			$shop_id = 1;
			array_push($callPath, "||" .'blank');
		}

		$_SESSION['callPath'] = $callPath;
		$_SESSION['shop_id'] = $shop_id;

		require_once ROOT.'/include/class/shop.class.php';

		$shop = new shop($this->DB, $this->lang);
		$shopDtl = $shop->getShopDtl($shop_id);
        $this->shopDtl = $shopDtl;
		$this->assign("shop", $shopDtl);
		$this->assign('smartyTemplateRoot', $shopDtl['prefix']."/");
		$this->assign("domain", $this->get_connection_scheme()."://" . $shopDtl['domain']);
		
		$this->shop_id = $_SESSION['shop_id'];
	}
	    
	protected function displayPage($tplPath){
		$this->tpl->display($tplPath);
	}
	
	public function assign($para, $val){
		$this->tpl->assign($para , $val);
	}

	function DB(){
		$this->DBC_avalibility();
		return $this->DBC;
	}

	function DBC_avalibility(){
		if(empty($this->DBC)){
			// 數據庫
			require_once ROOT.'/include/SQLDBi.class.php';
			$this->DBC = new SQLDBi(O_DB,O_DBUSER,O_DBPWD,O_HOST);
		}
	}

	function language_setup(){
		$language_set = empty($_REQUEST['language_set'])? null : $_REQUEST['language_set'];

		if($language_set && $language_set != ""){
			if (in_array($language_set, LANG_SUPPORT, true)){
				if ($language_set == "zh")
					$language_set = "tc";
				$_SESSION['lang'] = $language_set;
			}
		}
		$this->lang = empty($_SESSION['lang'])? DEFAULT_LANG : $_SESSION['lang'];
		$_SESSION['lang'] = $this->lang;
		$this->assign('lang',$this->lang);
	}

	function error_display(){
		if(isset($_REQUEST['error'])){
			if(@$_REQUEST['error']=='Y'){
				error_reporting(E_ALL);
				ini_set('display_errors', 1);		
			}
			if(@$_REQUEST['error']=='A'){
				error_reporting(E_ERROR);
				ini_set('display_errors', 1);		
			}
		} else {
			error_reporting(0);
			ini_set('display_errors', 0);		
		}
	}

	function showError(){
		error_reporting(E_ALL);
		ini_set('display_errors', 1);	
	}

	function get_connection_scheme(){
		if($_SERVER['HTTP_X_FORWARDED_PROTO']){
			return $_SERVER['HTTP_X_FORWARDED_PROTO'];
		} else if ($_SERVER['REQUEST_SCHEME']){
			return $_SERVER['REQUEST_SCHEME'];	
		} else {
			$protocol=$_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
			return $protocol;
		} 
	}

	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function generateRandomInt($length = 6) {
	    $characters = '0123456789';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function json_return($key_name,$data,$is_array){
		$debug = empty($_REQUEST['debug'])? null : $_REQUEST['debug'];
		if($debug == 'Y'){
			print_r($data);
			die();
		}
		if($is_array == 'Y'){
			$final_result_array[$key_name] = $data;
		} else{
			$result_json = array();
			array_push($result_json,$data);
			$final_result_array[$key_name] = $result_json;
		}

		if($debug  == 'Y'){
			print_r($final_result_array);
			die();
		}
		header('Content-Type: application/json; charset=UTF-8');
		$json = json_encode($final_result_array,JSON_UNESCAPED_UNICODE);
		echo "".isset($_GET['callback'])
		? "{$_GET['callback']}($json)"
		: $json."";
		exit;
	}

	function get_current_time(){
		return date('Y-m-d H:i:s');
	}

	function get_current_time_string(){
		return date('YmdHis');
	}

	function get_future_day($day){
		$date = date("Y-m-d H:i:s");// current date
		$date = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s", strtotime($date)) . " +".$day." day"));
		return $date;
	}

	function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' ){
	    $datetime1 = date_create($date_1);
	    $datetime2 = date_create($date_2);
	    $interval = date_diff($datetime1, $datetime2);
	    if($differenceFormat=='min'){
	    	return $interval;
	    }
	    if($interval->invert>0){
	    	return $interval->format($differenceFormat);	// date_1 小於 date_2
	    } else {
	    	return -1*$interval->format($differenceFormat); // date_1 大於 date_2
	    }
	    
	}

	function set_array_key_value($array,$key){
		$new_array = null;
		foreach($array as $k=>$i){
			$new_array[$i[$key]] = $i;
		}
		return $new_array;
	}

	function prevent_injection(){
		$pass = 'Y';
		$string = "";
		foreach($_REQUEST as $k=>$i){
			if(is_array($i)){
			// do nothing
			} else {
				$i = preg_replace('/\s+/', '', $i);
				$string = $string.$i;
			}
		}
		
		$pos = strpos(strtolower('x'.$string), 'deletefrom');
		if ($pos === false) {
			// did nth
		} else {
		    $pass = 'N';
		}
		
		$pos = strpos(strtolower('x'.$string), 'select*from');
		if ($pos === false) {
			// did nth
		} else {
		    $pass = 'N';
		}
		
		$pos = strpos(strtolower('x'.$string), 'truncatetable');
		if ($pos === false) {
			// did nth
		} else {
		    $pass = 'N';
		}
		/*foreach($_REQUEST as $k=>$i){
			$_REQUEST[$k] = htmlspecialchars($i, ENT_QUOTES, 'UTF-8');
		}*/
		$pos = strpos(strtolower('x'.$string), '<script>');
		if ($pos === false) {
			// did nth
		} else {
		    $pass = 'N';
		}
		$pos = strpos(strtolower('x'.$string), 'onerrormprompt');
		if ($pos === false) {
			// did nth
		} else {
		    $pass = 'N';
		}
		$pos = strpos(strtolower('x'.$string), 'document.write');
		if ($pos === false) {
			// did nth
		} else {
		    $pass = 'N';
		}
		$pos = strpos(strtolower('x'.$string), 'document.');
		if ($pos === false) {
			// did nth
		} else {
		    $pass = 'N';
		}
		$pos = strpos(strtolower('x'.$string), 'xmlhttpequest');
		if ($pos === false) {
			// did nth
		} else {
		    $pass = 'N';
		}
		
		$pos = strpos(strtolower('x'.$string), 'http.o');
		if ($pos === false) {
			// did nth
		} else {
		    $pass = 'N';
		}
		
		$pos = strpos(strtolower('x'.$string), 'http.s');
		if ($pos === false) {
			// did nth
		} else {
		    $pass = 'N';
		}
		$pos = strpos(strtolower('x'.$string), '<?php');
		if ($pos === false) {
			// did nth
		} else {
		    $pass = 'N';
		}
		$pos = strpos(strtolower('x'.$string), '?>');
		if ($pos === false) {
			// did nth
		} else {
		    $pass = 'N';
		}
		/*$pos = strpos(strtolower('x'.$string), '<!--');
		if ($pos === false) {
			// did nth
		} else {
		    $pass = 'N';
		}
		$pos = strpos(strtolower('x'.$string), '-->');
		if ($pos === false) {
			// did nth
		} else {
		    $pass = 'N';
		}*/
		$pos = strpos(strtolower('x'.$string), '?>');
		if ($pos === false) {
			// did nth
		} else {
		    $pass = 'N';
		}
		$pos = strpos(strtolower('x'.$string), '1=1');
		if ($pos === false) {
			// did nth
		} else {
		    $pass = 'N';
		}
		return $pass;
	}

	function get_connection_ip(){
		$ip = $_SERVER['REMOTE_ADDR'];
		if($_SERVER['HTTP_X_FORWARDED_FOR']){
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		if($_SERVER['HTTP_CF_CONNECTING_IP']){
			$ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
		}
		return $ip;
	}

	function dev_mode_check(){
		$_REQUEST['devMode'] = empty($_SESSION['devMode'])? null : $_SESSION['devMode'];
		if($_REQUEST['devMode']){
			$_SESSION['devMode'] = $_REQUEST['devMode'];
		}
	}

	function countryCheck(){
		if(isset($_SERVER['HTTP_CF_IPCOUNTRY'])){
			$countyCode = $_SERVER['HTTP_CF_IPCOUNTRY'];	
			//$countyCode = 'CN';
		} else {
			$countyCode = 'CN';
		}
		$this->assign('countyCode',$countyCode);
	}

	function getDelimitedStrings($array,$key,$delimiter){
		$final_string = '';
		foreach($array as $k=>$i){
			$final_string = $final_string.$delimiter.$i[$key];
		}
		return substr($final_string, 1);
	}

	function get_datetime_to($modify){
		$date = new DateTime($this->get_current_time());
		$date->modify($modify);
		return $date->format("Y-m-d H:i:s");
	}

	function parm_sql($type,$key,$required='N'){     //获取单个值，並sql escape
		return $this->escape_string($this->parm_in($type, $key, $required));
	}

	function parm_in($type,$key,$required='N'){     //获取单个值
		// echo "$type || $key || $required";
		if($type=='post'){
		    if($key==='ALL') {
                $value = $_POST;
            }else{
                $value = (isset($_POST[$key]) ? $_POST[$key] : null);
            }
			$ins = $_POST;
		}
		if($type=='get'){
		    if($key==='ALL'){
		        $value = $_GET;
            }else {
                $value = (isset($_GET[$key]) ? $_GET[$key] : null);
            }
			$ins = $_GET;
		}
		if($type=='request'){
			$value =(isset($_REQUEST[$key])?$_REQUEST[$key]:null);
			$ins = $_REQUEST;
		}
		if($type=='session'){
			$value =(isset($_SESSION[$key])?$_SESSION[$key]:null);
			$ins = $_SESSION;
		}
		if($type=='header'){
			$value =(isset($this->refinedHeaders[$key])?$this->refinedHeaders[$key]:null);
			$ins = $this->refinedHeaders;
		}
		if($type=='file'){
			$value =(isset($_FILES[$key])?$_FILES[$key]:null);
			if ($value['tmp_name'] == "")
				$value = null;
			$ins = $_FILES;
		}
		if($required=='Y' && !$value){
			require_once 'langMap/returnMessage.php';
			$RMG = new returnMessage($this->lang);
			
			require_once 'apiDataBuilder.class.php';
			new apiDataBuilder(-99, null, $this->dataBuilder('missingRequiredParameters',$key));
			// $this->json_return('result',$result,'Y');
		}
		return $value;
	}

	function escape_string($data){
		$this->DBC_avalibility();
		return mysqli_real_escape_string($this->DBC->Link_ID,$data);
	}

	function dataEncrypt($string){
		if(trim($string)==''){
			return '';
		}
	    $encrypt_method = "AES-128-CBC";
	    $secret_key = SECURITY_PASSPHARSE;
	    $secret_iv = SECURITY_PASSPHARSE;
	    $ciphertext = openssl_encrypt($string, $encrypt_method, $secret_key, $options=0, $secret_iv);
	    return $ciphertext;
	}
	
	function dataDecrypt($string){
		if(trim($string)==''){
			return '';
		}
	    $output = false;
	    $encrypt_method = "AES-128-CBC";
	    $secret_key = SECURITY_PASSPHARSE;
	    $secret_iv = SECURITY_PASSPHARSE;
	    // hash
	    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $secret_key, OPENSSL_RAW_DATA, $secret_iv);
	    if(trim($output)==''){
	        $output = $string;
	    }
	    return $output;
	}

	function memberCheck(){
		// accessToken override admin session
		if ($_REQUEST['accessToken']){			
			require_once ROOT.'/include/class/user.class.php';
			$userCls = new user($this->DB, $this->lang); 

			$accessToken = $this->dataDecrypt($_REQUEST['accessToken']);
			$accessTokenArr = explode("||", $accessToken);
			foreach($accessTokenArr as $token){
				$key = explode(":", $token)[0];
				$val = explode(":", $token)[1];
				switch ($key){
					case 'ADMINID':
						$admin_id = $val;
						$admin = $userCls->getAdminDtl($admin_id);
						$_SESSION['admin'] = $admin;
						break;
					case 'TIMESESSION':
						$_SESSION['login_time'] = $val;
						break;
				}
			}

		}



		if(isset($_SESSION['member'])){
			require_once ROOT.'/include/class/member.class.php';
			$memberCls = new member($this->DB,$this->lang);


			$member_id = $_SESSION['member']['member_id'];
			// 獲取會員資料
			$member = $memberCls->getMemberDtl($member_id);
			// $membershipDtl = $memberCls->getMembershipDtl($member['membership_id']);
			// $member['membership_discount'] = $membershipDtl['discount'];
			$_SESSION['member'] = $member;
			// if ($_SESSION['member']['member_id'] == '7') {
			// 	print_r($_SESSION);
			// }
			$this->assign('member',$_SESSION['member']);
		}
		if(isset($_SESSION['admin'])){
			// pass
		} else {
			if(isset($_REQUEST['adminRequest'])){

			} else {
				//die('Close');	
			}
		}
	}

	function adminCheck(){
		if(isset($_SESSION['admin'])){
			$sql = "SELECT * FROM `tbl_admin` WHERE `admin_id` = '".$_SESSION['admin']['admin_id']."'";
			$admin = $this->DB->get_Sql($sql)[0];

			$prefix = "";
			switch($this->shop_id){
				case 1:
					$prefix = "gift";
					break;
				case 2:
					$prefix = "zuri";
					break;
				case 3:
					$prefix = "ltp";
					break;
				default:
					$prefix = "";
					break;
			}

			$sql = "SELECT *, $prefix"."_right AS `right` FROM `tbl_admin_role` WHERE `role_id` = ". $admin['role_id'];
			$role = $this->DB->get_Sql($sql)[0];
			$role['right'] = json_decode($role['right'],1);

			$admin['role'] = $role;
			$_SESSION['admin'] = $admin;
			
			$this->assign('admin',$_SESSION['admin']);
		} else {

			// check for the mobile user
			$admin_id_e = $this->DB->gthis->parm_sql('request', 'admin_id');
			try {
				$admin_id = base64_decode($admin_id_e);
			} catch (\Exception $e) {
				$admin_id = $admin_id_e;
			}
			require_once ROOT.'/include/class/user.class.php';
			$userCls = new user($this->DB, $this->lang, $this->isAdmin);
			$admin = $userCls->getAdminDtl($admin_id);
			$_SESSION['admin'] = $admin;
			$this->assign('admin',$_SESSION['admin']);


			if($this->pwapage == 'Y'){
				$xresult['code'] = '-99';
				$xresult['msg'] = 'admin session expired';
				$xresult['redirect_url'] = '/admin/';
				$this->json_return('result', $xresult, 'Y');
			}
		}
	}

	/* ***************************************************************************************** */
	/*	Author : David Chung
	/*	Date : 19 Jun 2021
	/*  LUD  : 19 Jun 2021
	 *  Desc : global check up for request header and handling */
	/** 	   
	/* ***************************************************************************************** */

	function headerCheck(){
		$headers = apache_request_headers();
		foreach($headers as $k=>$i){
			if(strtolower($k)=='pwapage'){
				if($i=='Y'){
					// is pwa calling
					$this->pwapage = 'Y';
				}
			}
		}

		if(isset($_REQUEST['pwapage_test'])){
			if($_REQUEST['pwapage_test']=='Y'){
				$this->pwapage = 'Y';
			}
		}

		if(isset($_REQUEST['show_page'])){
			if($_REQUEST['show_page']=='Y'){
				$this->pwapage = 'Y';
			}
		}

		// this flag for SPA handle
		if($this->pwapage=='Y'){
			$this->assign('page_only','Y');
		}

	    foreach($headers as $k=>$i){
	        $this->refinedHeaders[strtolower($k)]=$i;
	    }

	    // if header provide user language preference, handle it
	    if(isset($this->refinedHeaders['user-lang'])){
	    	$_REQUEST['language_set'] = $this->refinedHeaders['user-lang'];
	    	$this->language_setup();
	    }
	}

	/* ***************************************************************************************** */
	/*	Author : David Chung
	/*	Date : 19 Jun 2021
	/*  LUD  : 19 Jun 2021
	 *  Desc : check is browser in mobile mode */
	/** 	   
	/* ***************************************************************************************** */

	function mobileCheck(){
		require_once ROOT."/include/MobileDetect/Mobile_Detect.php";
		$detect = new Mobile_Detect;
		// Any mobile device (phones or tablets).
		if ($detect->isMobile()) {
		 	$this->isMobile = 'Y';
		}
		$this->assign('isMobile',$this->isMobile);
	}

	/* ***************************************************************************************** */
	/*	Author : David Chung
	/*	Date : 19 Jun 2021
	/*  LUD  : 19 Jun 2021
	 *  Desc : if redirect is on, while income scheme is http, redirect to https HTTP_X_FORWARDED_PROTO work only under cloudflare CDN */
	/** 	   
	/* ***************************************************************************************** */

	function https_handling($https_redirect_on='N'){
		// clould flare CDN paramter
		if($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'http'){
			$script_url = $_SERVER['REQUEST_URI'];
			$http_host = $_SERVER['HTTP_HOST'];
			$script_url = 'https://'.$http_host.$script_url;
			header("Location:".$script_url);
		} else {
			if(isset($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'http' && !$_SERVER['HTTP_X_FORWARDED_PROTO']){
				$script_url = $_SERVER['REQUEST_URI'];
				$http_host = $_SERVER['HTTP_HOST'];
				$script_url = 'https://'.$http_host.$script_url;
				header("Location:".$script_url);
			}
		}
	}

	function memberRequired($needVerify='N'){
		if($needVerify=='Y'){
			if($_SESSION['member'] && ($_SESSION['member']['email_validated'] == 'Y' || $_SESSION['member']['mobile_validated']=='Y')){
				return true;
			} else {
				return false;
			}
		}
		if(!$_SESSION['member']){
			return false;
		}
		return true;
	}

	function memberRequiredRedirect($needVerify='N'){
		if($needVerify=='Y'){
			if($_SESSION['member'] && ($_SESSION['member']['email_validated'] == 'Y' || $_SESSION['member']['mobile_validated']=='Y')){
				// pass
			} else {
				header("Location:/");
			}
		}
		if(!$_SESSION['member']){
			header("Location:/");
		}
		// pass
	}

	function memberNeedVerifyEmailRedirect(){
		if(isset($_SESSION)){
			if($_SESSION['member'] && $_SESSION['member']['email_validated'] == 'Y'){
				// pass
			} else {
				header("Location:/validateEmail");
				exit();
			}
		}
	}

	function get_page_from_count($total_size,$page,$per_page,$page_base_uri){
		if($page == 0){
			$page = '1';
		}
		$page_per_page = $per_page;
		$page = $page - 1;
		$page_start = $page * $page_per_page;
		$page_end = ($page+1) * $page_per_page;
		foreach($result_array as $k=>$i){
			if($k >= $page_start && $k<$page_end){
				$final_array[$counter] = $i;
				$counter = $counter +1;
			}
		}
		$current_page = ($page+1);
		$page_offset = '5';
		$page_size_array;
		$xcount=0;
		$max_offset = $current_page + $page_offset;
		$i = $current_page - $page_offset;
		if(is_array($total_size)){
			$x_total_size = 0;
			$b = 0;
			foreach($total_size as $k=>$i){
				$i_ori_page = $i['ori_page'];
				$i_per_page = $i['per_page'];
				$x_total_size = $i_ori_page+$x_total_size;
				for($b;$b<=$x_total_size;$b++){
					if($b>0 && $b<$max_offset){
						$page_size_array[$xcount] = $b;
						$xcount = $xcount + 1;
					}
				}
			}
		} else {
			$total_size = ceil($total_size / $page_per_page);
			for($i;$i<=$total_size;$i++){
				if($i>0 && $i<$max_offset){
					$page_size_array[$xcount] = $i;
					$xcount = $xcount + 1;
				}
			}
		}

		// check conjuntor
		if(strpos($page_base_uri, '?')>-1){
			$conjuntor = "&";
		} else {
			$conjuntor = "?";
		}
		$final_array['page_size_array'] = $page_size_array;
		$final_array['current_page'] = $current_page;
		$final_array['next_page'] = $current_page+1;
		$final_array['previous_page'] = $current_page-1;
		$final_array['page_per_page'] = $page_per_page;
		$final_array['max_page'] = $total_size;
		$final_array['page_base_uri'] = $page_base_uri;
		$final_array['conjuntor'] = $conjuntor;
		return $final_array;
	}

	function urlAddParam($url,$key,$value){
		$pos = strpos($url, '?');
		if($pos>-1){
			$url = $url."&".$key."=".$value;
		} else {
			$url = $url."?".$key."=".$value;
		}
		return $url;
	}

	function loadTailwindClass($subfolder='/'){
		$tailwindClass = scandir(dirname(__FILE__).'//../'.$subfolder.'templates/tailwindDefine/', 1);

		foreach($tailwindClass as $k=>$i){
			if($i == '.' || $i=='..'){
				// do nothing
			} else {
				$file_type = explode('.', $i);
				$object_type = $file_type[1];
				if($object_type=='class'){
					$tailwind_class_content = file_get_contents(dirname(__FILE__).'//../'.$subfolder.'templates/tailwindDefine/'.$i);
					preg_match_all('%class="(.*?)"%s', $tailwind_class_content, $tailwind_class_content_data); // 找出class的數據
					if(empty($tailwind_class_content_data[1][0])){
						preg_match_all("%class='(.*?)'%s", $tailwind_class_content, $tailwind_class_content_data); // 找出class的數據
					}
					$this->assign(str_replace('.','_',str_replace('.html', '', $i)),$tailwind_class_content_data[1][0]);
				} elseif($object_type=='element') {
					$tailwind_class_content = file_get_contents(dirname(__FILE__).'//../'.$subfolder.'templates/tailwindDefine/'.$i);
					$this->assign(str_replace('.','_',str_replace('.html', '', $i)),$tailwind_class_content);
				}
				
			}
		}
	}

	function array_sort($arr, $keys, $type = 'asc') {
		$keysvalue = $new_array = array();
		foreach ($arr as $k => $v) {
			$keysvalue[$k] = $v[$keys];
		}
		if ($type == 'asc') {
			asort($keysvalue);
		} else {
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k => $v) {
			$new_array[$k] = $arr[$k];
		}
		return $new_array;
	}

	function smartyPageOnly(){
		$this->assign('page_only', 'Y');
	}

	function globalFileGetContentsContext(){
		$lang = $this->lang;
		$opts = [
			"http" => [
				"method" => "GET",
				"header" => "user-lang: $lang\r\n"
					
			]
		];
		$context = stream_context_create($opts);					
		return $context;
	}

	function array_merge_keep_key($array1,$array2){
		foreach($array2 as $k=>$i){
			$array1[$k] = $i;
		}
		return $array1;
	}

	function dataBuilder($msg_code='00000', $key){
		$display_msg = $this->RM->getReturnCode($msg_code);
		if(!$display_msg){
			$display_msg = $this->RM->getReturnString($msg_code);	
		}
		$msg = "$display_msg: $key";
		return $msg;
	}

	function webpSupport(){
		if( strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' ) == true || strpos( $_SERVER['HTTP_USER_AGENT'], ' Chrome/' ) == true ) {
		    $this->assign('webpSupport','Y');
		    $this->webpSupport='Y';
		} else {
			$this->assign('webpSupport','N');
			$this->webpSupport='N';
		}
	}
	
	abstract function init();
	
	abstract function display();
	
	function param($paramList){
		return $this->paramBuilder([$paramList])[$paramList["key"]];
	}

	function paramBuilder($paramList){
		/***********
		 * paramList {
		 * key: [string] [compulsory] the request key
		 * compulsory: [boolean] [default: false] is the field compulsory
		 * type: [string] [default: string] the data type
		 * allowZero: [boolean] [default: true] is the field allow Zero as an return (int, float)
		 * allowNull: [boolean] [default: true] is the field allow null as an return
		 * allowBlank: [boolean] [default: true] is the field allow blank as an return
		 * defaultValue: [any] default value while any rule break
		 * request: [string] [default: request] identify the request method: POST GET REQUEST FILE SESSION
		 * }
		 * 
		 * sample: 
		 * $data = $this->gthis->parmaBuilder([
			["key" => "patient_id", "compulsory" => true, "type" => "int", "allowZero" => false, "allowNull" => false],
			["key" => "medical_history", "type" => "longtext"],
			["key" => "drug_history", "type" => "longtext"],
		]);
		 * 
		 * 
		 * *****************/


		$res = [];
		foreach ($paramList as $param){
			
			$key = $param['key'];
			$compulsory = isset($param['compulsory']) ? $param['compulsory'] : false;
			$type = isset($param['type']) ? $param['type'] : 'string';
			$allowZero = isset($param['allowZero']) ? $param['allowZero'] : true;
			$allowNull = isset($param['allowNull']) ? $param['allowNull'] : true;
			$allowBlank = isset($param['allowBlank']) ? $param['allowBlank'] : true;
			$request = isset($param['request']) ? $param['request'] : 'request';
			$defaultValue = isset($param['defaultValue']) ? $param['defaultValue'] : null;
			unset($val);

			switch ($request){
				case 'post':
					if (isset($_POST[$key])){
						$val = $_POST[$key];
					} else if ($defaultValue){
						$val = $defaultValue;
					} else if ($compulsory){
						new apiDataBuilder(-99, $key);
					}
					break;
				case 'get':
					if (isset($_GET[$key])){
						$val = $_GET[$key];
					} else if ($defaultValue){
						$val = $defaultValue;
					} else if ($compulsory){
						new apiDataBuilder(-99, $key);
					}
					break;
				case 'session':
					if (isset($_SESSION[$key])){
						$val = $_SESSION[$key];
					} else if ($defaultValue){
						$val = $defaultValue;
					} else if ($compulsory){
						new apiDataBuilder(-99, $key);
					}
					break;
				case 'file':
					if (isset($_FILES[$key])){
						$val = $_FILES[$key];
					} else if ($defaultValue){
						$val = $defaultValue;
					} else if ($compulsory){
						new apiDataBuilder(-99, $key);
					}
					break;
				case 'server':
					if (isset($_SERVER[$key])){
						$val = $_SERVER[$key];
					} else if ($defaultValue){
						$val = $defaultValue;
					} else if ($compulsory){
						new apiDataBuilder(-99, $key);
					}
					break;
				case 'self':
					$val = $defaultValue;
					break;
				case 'request':
				default:
					if (isset($_REQUEST[$key])){
						$val = $_REQUEST[$key];
					} else if ($defaultValue){
						$val = $defaultValue;
					} else if ($compulsory){
						new apiDataBuilder(-99, $key);
					}
					break;
			}

			$errorJson = [
				"key" => $key,
				"type" => $type,
				"val" => $val,
			];

			if (isset($val)){
				if (!$allowZero && $val == '0'){
					new apiDataBuilder(-991, $errorJson);
				}
	
				if (!$allowBlank && $val === ''){
					new apiDataBuilder(-993, $errorJson);
				} 
				
				switch ($param['type']){
					case 'int':
						$val = $this->DB->parseInt($val);
						break;
					case 'timestamp':
						$val = $this->DB->parseTimestamp($val);
						break;
					case 'date':
						$val = $this->DB->parseDate($val);
						break;
					case 'time':
						$val = $this->DB->parseTime($val);
						break;
					case 'NY':
						$val = $this->DB->parseNY($val);
						break;
					case 'intArr':
						$val = $this->DB->parseIntArr($val);
						break;
					case 'intArrV1':
						$val = $this->DB->parseIntArrV1($val);
						break;
					case 'arr':
						$val = $this->DB->parseArr($val);
						break;
					case 'json':
						$val = $this->DB->parseJson($val);
						$val = json_decode($val, true);
						break;
					case 'email':
						$val = $this->DB->parseEmail($val);
						break;
					case 'float':
						$val = $this->DB->parseFloat($val);
						break;
					case 'string':
						$val = $val;
						break;
	
					case 'salesStatus':
						if ($val != "10_to_be_confirm" && $val != "20_not_for_sale" && $val != "25_schdeuled_sale" && $val != "30_on_sale" && $val != "40_out_of_stock")
							$val = null;
						break;
					case 'sku':
						$val = str_replace(" ", "_", $val);
						$val = str_replace("-", "_", $val);
						$val = strtolower($val);
						$val = preg_replace("/[^A-Za-z0-9_]/", '', $val);
						break;
	
					
				}
	
				$res[$key] = $val;
			}
		}

		return $res;
	}

}

?>