window.__vkmodalList = []


async function vkmodal(param) {


    var $body = $('body')

    if (!param) {
        debug('Undefined or null paramater object for Vk Modal.')
    } else {
        var timeStampSeed = new Date().getTime()
        var initialObject = $.extend({
            extraClass: '',
            timestamp: timeStampSeed,
            summonDomElement: null,
            backdropOpacity: 20,
            modalMode: 'blank', //loading, ajax, blank
            modalBackgroundBoolean: true,
            tapBackgroundToClose: false,
            shadow: true,
            area: '',
            fullScreen: false,
            ajaxPath: null,
            _zIndex: 99 + window.__vkmodalList.length,
            setAjaxPath: function(url) {
                this.ajaxPath = url
                    //debug('TODO : should render the DOM again here.')
            },
            setContent: function(injectContent) {
                this.ajaxPath = null
                this.content = injectContent
                    //debug('TODO : should render the DOM again here.')
            },
            content: '',
            beforeMount: function() {
                //debug('beforeMount : init empty function in vk modal')
            },
            mounted: function() {
                //debug('mounted : init empty function in vk modal')
            },
            _destory: function() {
                //debug('Going to kill the following element : ')
                //debug(this.summonDomElement)

                //remove the actual object in window global variable
                window.__vkmodalList.splice(window.__vkmodalList.indexOf(this), 1)

                //and kill the dom
                this.summonDomElement.remove()

                //unlock the screen
                __vkmodalBodyLocker(false)

                //$('#vkmodal-' + timeStampSeed).remove()
            }

        }, param)

        //debug('Initial object for Vk Modal :')
        //debug(initialObject)

        //start to bind functions for beforeMount
        initialObject.beforeMount($('#vkmodal-' + initialObject.timestamp))

        //start to render popup
        $body.append(__vkmodalContentGenerator(initialObject))

        //lock the screen
        //console.log(param.bodyLocker);
        if(param.bodyLocker==false){
            __vkmodalBodyLocker(false)    
        } else {
            __vkmodalBodyLocker(true)    
        }

        //push relative object to global list
        initialObject.summonDomElement = $('#vkmodal-' + initialObject.timestamp)

        //bind init object to DOM
        //debug(initialObject)
        initialObject.summonDomElement.data(initialObject)

        //loading ajax content
        await ajaxContentGenerator(initialObject)
            //mounted functions
        initialObject.mounted($('#vkmodal-' + initialObject.timestamp))


        //push object to window
        window.__vkmodalList.push(initialObject)

        //debug(initialObject)

        return initialObject

    }
}

async function ajaxContentGenerator(initialObject) {

    if (initialObject.ajaxPath != null) {
        var ajaxContent = new Promise(function(reslove) {
            $.bajax({
                url: initialObject.ajaxPath,
                debug: 'N',
                pureString: 'Y',
                callback: function(data) {
                    reslove(data)
                },
            });
        })

        var ajaxContentResult = await ajaxContent;
        //debug(ajaxContentResult)

        //parse html content
        var $outerMainContainer = $('#vkmodal-' + initialObject.timestamp)
        $outerMainContainer.find('.vk-modal-dom-container').html(ajaxContentResult)
            //enable background closer
        if (initialObject.tapBackgroundToClose != true) {
            $outerMainContainer.find('.vk-modal-background').addClass('vk-close-disabled')
        } else {
            $outerMainContainer.find('.vk-modal-background').removeClass('vk-close-disabled')
        }
    }


}

function __vkmodalBodyLocker(param) {

    var $w = $(window)
    var $b = $('body')

    //lock screen
    if (param == true) {
        //debug('vk body locker enabled')

        $b.data().cachedScrollTop = $w.scrollTop()
        $b.css({
            'overflow': 'hidden',
            'top': $b.data().cachedScrollTop * -1
        })
    }

    //unlock screen
    if (param == false && window.__vkmodalList.length == 0) {
        $b.css({
            'position': '',
            'overflow': ''
        })
        $w.scrollTop($b.data().cachedScrollTop)
    }
}

function __vkmodalContentGenerator(initialObject) {
    $(document).off('click', '.vk-modal-background')
    .on('click', '.vk-modal-background', function() {
        var $this = $(this)
        var ts = $(this).closest('.vk-modal-dom').data().vkmodalId

        if ($this.hasClass('vk-close-disabled') == false) {
            window.__vkmodalList.forEach(function(_) {
                if (_.timestamp == ts) {
                    if (_.tapBackgroundToClose == true)
                        _._destory()
                }
            })
        }

    })
    
    var loadingModalGate = false
    if (initialObject.modalMode == 'loading' || initialObject.ajaxPath != null) {
        //define variable for loading modal...for locking background click etc.
        loadingModalGate = true
    }

    function innerBuilder() {
        if (initialObject.modalMode == 'loading' || initialObject.ajaxPath != null) {
            return `<div class="flex items-center justify-center"><div class="w-32 h-32 bg-white rounded-lg"><svg class="animate-spin mt-6 mx-auto h-8 w-8 text-gray-300" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
      <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
      <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
    </svg><div class="text-center my-4 text-sm text-gray-600 px-6">` + window.jstxt.jstxt0001 + `...</div></div></div>`
        } else if (initialObject.modalMode == 'blank') {
            //blank mode
            return initialObject.content
        }
    }

    //sperate ajax method
    if (initialObject.ajaxPath) {
        //debug(initialObject.ajaxPath)
    } else {
        //debug('No AjaxPath was found.')
    }

    return `
    <div id="vkmodal-${initialObject.timestamp}" data-vkmodal-id="${initialObject.timestamp}"
    class="${initialObject.extraClass} vk-modal-dom fixed top-0 left-0 h-screen w-screen"
    style="max-height:-webkit-fill-available; z-index:${initialObject._zIndex};"
    >
    <div class="h-full w-full opacity-${initialObject.backdropOpacity} bg-black
    absolute top-0 left-0 vk-modal-background ${loadingModalGate == true ? 'vk-close-disabled' : ''} z-10"></div>
        <div class="flex items-center justify-center h-full relative z-20 pointer-events-none
        ${initialObject.fullScreen == true ? '' : 'py-8'}
        ">
            <div class="max-h-full rounded max-w-full overflow-auto
            pointer-events-auto vk-modal-dom-container
            ${initialObject.shadow == true ? 'shadow-lg' : ''}
            ${initialObject.modalBackgroundBoolean == true ? 'bg-white' : ''}
            w-full
            ${initialObject.fullScreen == true ? 'w-full' : 'mx-8 sm:w-auto'}
            ${initialObject.area}
            ">
            ${innerBuilder()}
            </div>
        </div>
    </div>
    `
}

function killVkmodal(modalTimeStamp) {
    try {
        if (modalTimeStamp.attr('data-vkmodal-id')) {
            window.__vkmodalList.forEach(function(_) {
                if (_.timestamp == modalTimeStamp.attr('data-vkmodal-id')) {
                    _._destory()
                }
            })
        }
    } catch (e) {

    }

    window.__vkmodalList.forEach(function(_) {
        if (_.timestamp == modalTimeStamp) {
            _._destory()
        }
    })
}


//DOM binders

//app background-modal-tap
$(document).off('click', '.vk-modal-background')
    .on('click', '.vk-modal-background', function() {
        var $this = $(this)
        var ts = $(this).closest('.vk-modal-dom').data().vkmodalId

        if ($this.hasClass('vk-close-disabled') == false) {
            window.__vkmodalList.forEach(function(_) {
                if (_.timestamp == ts) {
                    if (_.tapBackgroundToClose == true)
                        _._destory()
                }
            })
        }

    })

$(document).off('click', '.vk-modal-close')
    .on('click', '.vk-modal-close', function() {
        var $this = $(this)
        var ts = $(this).closest('.vk-modal-dom').data().vkmodalId
            //console.log(ts);
        if ($this.hasClass('vk-close-disabled') == false) {
            window.__vkmodalList.forEach(function(_) {
                if (_.timestamp == ts) {
                    _._destory()
                }
            })
        }

    })

$vkmodal = function(param) {
    var $this = $(this)
    vkmodal(param)
}

function vkfullScreenLoader(action) {
    if (!window.vlfullScreenLoaders) {
        window.vlfullScreenLoaders = [];
    }

    if (action == 'show') {
        vkmodal({
            modalMode: 'loading',
            mounted: function(elem) {
                window.vlfullScreenLoaders.push(elem);
            }
        });
    }
    if (action == 'close') {
        try{
            var lastItem = window.vlfullScreenLoaders[window.vlfullScreenLoaders.length - 1];
            killVkmodal(lastItem.attr('data-vkmodal-id'));
            window.vlfullScreenLoaders.pop();    
        } catch(e){
            //
        }
    }
}

//preset pop ups
function vkAlertDefault(param){
    var init = $.extend({
        cancelButton: false,
    }, param)

    vkAlert(init);
}

function vkConfirmDefault(param){
    var init = $.extend({}, param)
    vkAlert(init);
}


function vkAlert(param) {
    var init = $.extend({
        title: 'UNDEFINED TITLE',
        content: 'UNDEFINED CONTENT',
        outerClass: 'p-4',
        confirmButton: true,
        confirmButtonHtml: 'confirm_missing',
        confirmButtonHtmlClass: '',
        confirmButtonFunction: function($obj) {
            debug('confirm button pressed')
            debug($obj)
        },
        cancelButton: true,
        cancelButtonHtml: 'cancel_missing',
        cancelButtonHtmlClass: '',
        cancelButtonFunction: function($obj) {
            debug('cancel button pressed')
            debug($obj)
        },
        closeOnConfirm: true,
        closeOnCancel: true,
        tapBackgroundToClose: false,
    }, param)

    //title,
    //content,
    //confirmButton
    //confirmButtonText
    //confirmButtonFn
    //cancelButton
    //cancelButtonText
    //cancelBUttonFn
    //closeOnConfirm
    //closeOnCancel

    $vkmodal({
                content: `<div class="${init.outerClass} max-w-screen-80 sm:min-w-84">
            <div class="font-bold text-2xl mb-2">${init.title}</div>
            <div class="text-gray-800 mb-2">${init.content}</div>
            <div class="vk-alert-operation-panel overflow-y-auto">
                ${init.confirmButton ? `<div class="flex flex-col md:flex-row-reverse items-center mt-2">
                    ${init.confirmButton ? `<a href="#" class="md:ml-2 mb-2 md:mb-0 vk-modal-alert-confirm rounded w-full md:w-auto font-bold py-2 text-center md:text-left hover:opacity-75 px-4 ${init.confirmButtonHtmlClass}">${init.confirmButtonHtml}</a>` : ''}
                    ${init.cancelButton ? `<a href="#" class="vk-modal-alert-cancel w-full md:w-auto rounded bg-gray-200 text-gray-600 font-bold py-2 text-center md:text-left hover:opacity-75 px-4 ${init.cancelButtonHtmlClass}">${init.cancelButtonHtml}</a>` : '' }
                </div>` : ''}
            </div>
        </div>`,
        tapBackgroundToClose: init.tapBackgroundToClose,
        mounted: function($obj) {
            if (param.mounted)
                param.mounted();
            $obj.find('.vk-modal-alert-confirm').click(function(e) {
                e.preventDefault()
                init.confirmButtonFunction($obj)
                if (init.closeOnConfirm == true) {
                    $obj.data()._destory()
                }

            })
            $obj.find('.vk-modal-alert-cancel').click(function(e) {
                e.preventDefault()
                init.cancelButtonFunction($obj)
                if (init.closeOnConfirm == true) {
                    $obj.data()._destory()
                }

            })
        }
    })
}

//preset Toast Msg by David
function vkToast(param) {
    var init = $.extend({
        content : 'UNDEFINED CONTENT',
        outerClass: 'p-4',
        confirmButton: false,
        cancelButton: false,
        tapBackgroundToClose: false,
        bodyLocker: true,
        backdropOpacity: 20,
        modalBackgroundBoolean :false,
    }, param);

    $vkmodal({
            content: `<div class="${init.outerClass} max-w-screen-80 sm:min-w-84 text-center bg-black bg-opacity-55">
            <div class="text-white font-bold">${init.content}</div>
        </div>`,
        tapBackgroundToClose: init.tapBackgroundToClose,
        bodyLocker: init.bodyLocker,
        backdropOpacity: init.backdropOpacity,
        modalBackgroundBoolean: init.modalBackgroundBoolean,
        mounted: function($obj) {
            setTimeout(function(){
                $obj.fadeOut(1500,function() {
                    $obj.data()._destory()
                })
            }, 1500)
        }
    })
}