<?php
/* 
    Objective: Abstract class
    Author: Sing
    Last major update: 03-11-2021

*/
require_once ROOT. '/include/apiDataBuilder.class.php';
abstract class baseClass{

    public function __construct($DB, $lang, $isAdmin = false) {
		$this->DB = $DB;
        $this->lang = $lang;
        $this->isAdmin = $isAdmin;
        $this->init();
        $this->shop_id = $DB->gthis->shop_id;
        $this->shopList = [
            ["id" => "1", "prefix" => "gift"],
            ["id" => "2", "prefix" => "zuri"],
            ["id" => "3", "prefix" => "ltp"],
        ];
        $this->callback = explode(',',$_REQUEST['callback']);
	}

    function init(){}
    

    function getPrefix($shop_id){
        // get the shop sql prefix
        $shop_id = strval($shop_id)[0];
        switch($shop_id){
            case 1:
                return "gift";
                break;
            case 2:
                return "zuri";
                break;
            case 3:
                return "ltp";
                break;
            default:
                return null;
                break;
        }
    }


    function sql_escape($src)    {
        // sql escape 
        // $src: str before escape

        $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
        $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");

        return str_replace($search, $replace, $src);
    }

    function random_str($len) {
        // generate random string
        // $len: the random string length
        
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < $len; $i++) {
            $randstring .= $characters[rand(0, strlen($characters))];
        }
        
        return $randstring;
    }

    function parse_percent($val){
        return number_format((float)$val*100, 2, '.', '');;
    }

    function dataEncrypt($string){
		if(trim($string)==''){
			return '';
		}
	    $encrypt_method = "AES-128-CBC";
	    $secret_key = SECURITY_PASSPHARSE;
	    $secret_iv = SECURITY_PASSPHARSE;
	    $ciphertext = openssl_encrypt($string, $encrypt_method, $secret_key, $options=0, $secret_iv);
	    return $ciphertext;
	}
	
	function dataDecrypt($string){
		if(trim($string)==''){
			return '';
		}
	    $output = false;
	    $encrypt_method = "AES-128-CBC";
	    $secret_key = SECURITY_PASSPHARSE;
	    $secret_iv = SECURITY_PASSPHARSE;
	    // hash
	    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $secret_key, OPENSSL_RAW_DATA, $secret_iv);
	    if(trim($output)==''){
	        $output = $string;
	    }
	    return $output;
	}
}