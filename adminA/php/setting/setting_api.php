<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;
		
		require_once ROOT. '/include/apiDataBuilder.class.php';

		switch ($this->gthis->thirdmode) {
			case 'changeShop':
				$this->changeShop();
				break;
			case 'editHoliday':
				$this->editHoliday();
				break;
			case 'editDistrict':
				$this->editDistrict();
				break;
			case 'editRegion':
				$this->editRegion();
				break;
			case 'editGreeting':
				$this->editGreeting();
				break;
			case 'editGreetingCate':
				$this->editGreetingCate();
				break;
			case 'editGreetingSubcate':
				$this->editGreetingSubcate();
				break;
			case 'getGreetingCateList':
				$this->getGreetingCateList();
				break;
			case 'editTimeslot':
				$this->editTimeslot();
				break;
			case 'editCustDeliGrp':
				$this->editCustDeliGrp();
				break;
			case 'editCustDeliRegion':
				$this->editCustDeliRegion();
				break;
			case 'editCustDeliTimeslot':
				$this->editCustDeliTimeslot();
				break;

			case 'getDistrictList':
				$this->getDistrictList();
				break;
			case 'sortDistrict':
				$this->sortDistrict();
				break;

			case 'getTimeslotList':
				$this->getTimeslotList();
				break;
			case 'sortTimeslot':
				$this->sortTimeslot();
				break;
			

			

			default:
				new apiDataBuilder(-84);
				break;
		}

		$result_array['lang'] = $this->lang;
		$this->gthis->assign('result_array',$result_array);
	}

	function editCustDeliTimeslot(){
		if (intval($_SESSION['right']['shop']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);

        $cust_id = $this->gthis->parm_sql('request', 'cust_id');
		$data = [];
		$fieldArr = ["grp_id", "slot_id", "ava_b4_day", "online_pay_same_day", "deli_fee", "same_day_deli_fee", "ava_b4_cutoff", "activate", "region_id", "show_in_web"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			$$field? $data[$field] = $$field: null;
		}

		$res = $deliveryCls->editCustDeliTimeslot($cust_id, $data);
		
		new apiDataBuilder(0, $res);
	}

	function editCustDeliRegion(){
		if (intval($_SESSION['right']['shop']) < 50){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);

        $cust_id = $this->gthis->parm_sql('request', 'cust_id');
		$data = [];
		$fieldArr = ["grp_id", "region_id", "shipping_fee", "address_required", "show_in_web", "activate"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			$$field? $data[$field] = $$field: null;
		}

		$res = $deliveryCls->editCustDeliRegion($cust_id, $data);
		
		new apiDataBuilder(0, $res);
	}

	function editCustDeliGrp(){
		if (intval($_SESSION['right']['shop']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);

        $grp_id = $this->gthis->parm_sql('request', 'grp_id', 'N');
		$data = [];
		$fieldArr = ["adm_name", "shop_id", "activate"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			$$field? $data[$field] = $$field: null;
		}

		$res = $deliveryCls->editCustDeliGrp($grp_id, $data);
		
		new apiDataBuilder(0, $res);
	}

	function changeShop(){
		$_SESSION['shop_id'] = $this->gthis->parm_in("request", "shop_id",'Y');
		new apiDataBuilder(0);
	}

	function getGreetingCateList(){
		if (intval($_SESSION['right']['shop']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/greeting.class.php';
		$greeting = new greeting($this->DB, $this->lang, $this->isAdmin);

        $shop_id = $this->gthis->parm_sql('session', 'shop_id');

		$res = $greeting->getGreetingCateList($shop_id);
		
		new apiDataBuilder(0, $res);
	}

	function editGreeting(){
		if (intval($_SESSION['right']['shop']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/greeting.class.php';
		$greeting = new greeting($this->DB, $this->lang, $this->isAdmin);

        $greeting_id = $this->gthis->parm_sql('request', 'greeting_id',  'N');
		$data = [];
		$fieldArr = ["msg", "subcate_id", 'sort', "shop_id", "activate"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
				case 'shop_id':
					if (!$$field){
						new apiDataBuilder(-99, null, "$field cannot be null");
					}
					break;
			}
			$$field? $data[$field] = $$field: null;
		}

		$res = $greeting->editGreeting($greeting_id, $data);
		
		new apiDataBuilder(0, $res);
	}

	function editGreetingCate(){
		if (intval($_SESSION['right']['shop']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/greeting.class.php';
		$greeting = new greeting($this->DB, $this->lang, $this->isAdmin);

        $cate_id = $this->gthis->parm_sql('request', 'cate_id',  'N');
		$data = [];
		$fieldArr = ["name_tc", "name_en", 'sort', "activate", "shop_id"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
				case 'shop_id':
					if (!$$field){
						new apiDataBuilder(-99, null, "$field cannot be null");
					}
					break;
			}
			$$field? $data[$field] = $$field: null;
		}

		$res = $greeting->editGreetingCate($cate_id, $data);
		
		new apiDataBuilder(0, $res);
	}

	function editGreetingSubcate(){
		if (intval($_SESSION['right']['shop']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/greeting.class.php';
		$greeting = new greeting($this->DB, $this->lang, $this->isAdmin);

        $subcate_id = $this->gthis->parm_sql('request', 'subcate_id',  'N');
		$data = [];
		$fieldArr = ["name_tc", "name_en", "parent_cate_id", 'sort', "shop_id", "activate"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
				case 'shop_id':
					if (!$$field){
						new apiDataBuilder(-99, null, "$field cannot be null");
					}
					break;
			}
			$$field? $data[$field] = $$field: null;
		}

		$res = $greeting->editGreetingSubcate($subcate_id, $data);
		
		new apiDataBuilder(0, $res);
	}

	function editRegion(){ 
		if (intval($_SESSION['right']['shop']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->lang, $this->isAdmin);

		$region_id = $this->gthis->parm_sql('request', 'region_id',  'N');
		$data = [];
		$fieldArr = ["district_id", "name_tc", "name_en", "show_in_web", "shipping_fee", "address_required", "activate"];

		foreach ($fieldArr as $field){
			if(isset($_REQUEST[$field])){
				$$field = $this->gthis->parm_sql('request', $field);
				switch ($field){
					case 'shipping_fee':
						$$field = $this->DB->parseInt($$field);
						break;
					case 'address_required':
					case 'active':
						$$field = $this->DB->parseNY($$field);
						break;
				}
				$data[$field] = $$field;
			}
		}

		$res = $delivery->editRegion($region_id, $data);

		new apiDataBuilder(0, $res);
	}

	function editDistrict(){
		if (intval($_SESSION['right']['shop']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);

		$district_id = $this->gthis->parm_sql('request', 'district_id',  'N');
		$data = [];
		$fieldArr = ["shop_id", "name_tc", "name_en", "show_in_web", "sort", "activate"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
				case 'show_in_web':
					$$field = $this->DB->parseNY($$field);
					break;
			}
			$$field? $data[$field] = $$field: null;
		}

		$res = $deliveryCls->editDistrict($district_id, $data);
		
		$shipping_fee = $this->gthis->param(["key"=>"shipping_fee", "type"=>"int"]);
        if (isset($shipping_fee)){
            $region_list = $deliveryCls->getRegionList($res['shop_id'], ["district_id"=> $res['district_id']]);
			foreach ($region_list as $k => $region){
				$deliveryCls->editRegion($region['region_id'], [
					"shipping_fee" => $shipping_fee	
				]);
			}
        }

		


		new apiDataBuilder(0, $res);
	}

	function editHoliday(){
		if (intval($_SESSION['right']['shop']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/holiday.class.php';
		$holiday = new holiday($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" =>"holiday_id", "type" => "int"],
			["key" =>"shop_id", "type" => "int"],
			["key" =>"quota_everyday", "type" => "int"],
			["key" =>"start_date", "type" => "date"],
			["key" =>"end_date", "type" => "date"],
			["key" =>"isDayOff", "type" => "NY"],
			["key" =>"activate", "type" => "NY"],
			["key" =>"adm_name"],
			["key" =>"remark_tc"],
			["key" =>"remark_en"],
			["key" =>"product_id_arr", "type" => "intArr"],
		]);

		$res = $holiday->editHoliday($data['holiday_id'], $data);
		
		new apiDataBuilder(0, $res);
	}

	function editTimeslot(){
		if (intval($_SESSION['right']['shop']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->lang, $this->isAdmin);

        $slot_id = $this->gthis->parm_sql('request', 'slot_id',  'N');
		$data = [];
		$fieldArr = ["shop_id", "adm_name", "start_time", "end_time", "ava_b4", "deli_fee", "same_day_deli_fee", "default", "online_pay_same_day", "show_in_web", "activate", "sort"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
				case 'start_time':
				case 'end_time':
					$$field = $this->DB->parseTime($$field);
					break;
				case 'default':
				case 'online_pay_same_day':
					$$field = $this->DB->parseNY($$field);
					break;
				case 'deli_fee':
				case 'same_day_deli_fee':
					// allow zero
					$data[$field] = $$field;
					break;
			}
			$$field? $data[$field] = $$field: null;
		}

		$res = $delivery->editTimeslot($slot_id, $data);
		
		new apiDataBuilder(0, $res);
	}

	function getDistrictList(){
		if (intval($_SESSION['right']['shop']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);
	
		$res['district_list'] = $deliveryCls->getDistrictList();
	
		new apiDataBuilder(0, $res);
	}
	
	function sortDistrict(){
		if (intval($_SESSION['right']['shop']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);
	
		$data = $this->gthis->paramBuilder([
			["key" => "district_id_arr", "type" => "intArr"]
		]);
	
		$deliveryCls->sortDistrict($data['district_id_arr']);
	
		new apiDataBuilder(0, true);
	}

	function getTimeslotList(){
		if (intval($_SESSION['right']['shop']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);
	
		$res['timeslot_list'] = $deliveryCls->getTimeslotList();
	
		new apiDataBuilder(0, $res);
	}
	
	function sortTimeslot(){
		if (intval($_SESSION['right']['shop']) < 50){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);
	
		$data = $this->gthis->paramBuilder([
			["key" => "slot_id_arr", "type" => "intArr"]
		]);
	
		$deliveryCls->sortTimeslot($data['slot_id_arr']);
	
		new apiDataBuilder(0, true);
	}
}
?> 