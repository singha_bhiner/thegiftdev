<?php	
require_once '../include/baseModel.class.php';
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

class admin extends baseModel{
	public $RM;
	public $twoFactorOn = false;
	public $submode;

	public function __construct(){
		parent::__construct('./adminA'); // 定義 html 檔案路徑
		require_once 'include/langMap/returnMessage.php';
		$this->RM = new returnMessageAdmin($this->lang,ROOT.'/adminA/include/langMap/returnCodeMap.csv');
		$this->RMBC = new returnMessageAdmin($this->lang,ROOT.'/adminA/include/langMap/returnCodeBreadcrumb.csv');
		require_once ROOT.'/include/langMap/returnMessage.php';
		$this->RMG = new returnMessage($this->lang);
		require_once 'include/class/uiConfig.php';
		$this->uiConfig = new uiConfig($this,$this->lang);

		
		$this->https_handling('Y');
		$this->headerCheck();
		$this->mobileCheck();
		$this->adminCheck();
		$this->membercheck();

		// load Tailwind class
		$this->loadTailwindClass('/adminA/');

		$this->navObject = $this->navObjectDefault();

		require_once 'include/pageNavigator.php';
		$this->PN = new pageNavigator($this->lang,$this->RM);

		// get submode param
		$this->mode = $this->parm_in('request','mode','N');
		$this->submode = $this->parm_in('request','submode','N');
		$this->thirdmode = $this->parm_in('request','thirdmode','N');
		$this->is_popup = $this->parm_in('request', 'is_popup'); 
		if ($this->is_popup == 'Y'){
			$this->assign("no_header", 'Y');
			$this->assign("is_popup", 'Y');
		}

		$this->breadcrumb = [
			["code"=>"code", "url"=>"admin", "page_title"=> $this->RMBC->getReturnString('admin')], 
		];
		
		if(!isset($_SESSION['admin'])){
			if ($this->mode != "user"){
				header('Location: admin/user/login');
			}
			// $this->assign('page_only','Y');
			// $this->page = 'login';	
		}

		$this->init();
	}

	function navObjectDefault(){
		$navObject['url'] = $this->baseURL.$_SERVER['REQUEST_URI'];
		$navObject['page_title'] = $this->RM->getReturnString('adminPage');	
		$navObject['page_description'] = '';
		$navObject['keywords'] = '';
		return $navObject;
	}

	function localStringValueBuild($groupClass,$path=null){
		// load local csv file
		if(isset($path)){
			if(file_exists($path)){
				$localRM = new returnMessageAdmin($this->lang,$path);	
			}
		} else {
			if(file_exists(ROOT.'/adminA/templates/'.$groupClass.'/'.$groupClass.'.csv')){
				$localRM = new returnMessageAdmin($this->lang,ROOT.'/adminA/templates/'.$groupClass.'/'.$groupClass.'.csv');
			}
		}

		if(isset($localRM)){
			foreach($this->RM as $k=>$i){
				if(is_array($this->RM->$k)){
					$this->RM->$k = array_replace($this->RM->$k, $localRM->$k);	
				}
			}
		}	
	}

	public function init(){
		
		if(isset($_SESSION['menu_bar_open'])){
			$this->assign('menu_bar_open',$_SESSION['menu_bar_open']);
		}
		switch($this->mode){
			default:
				if ($this->submode == "api"){
					$extraFlag = $this->try_for_api_file($this->mode);
				} else {
					// echo "<script>console.log('admin.php ln:~94 || mode: " . $this->mode ."', ' || submode: " . $this->submode ."',' || thirdmode: " . $this->thirdmode ."');</script>";
					$this->page = "$this->mode/$this->mode";
					$extraFlag = $this->try_for_php_file($this->mode);
				}
				if($extraFlag){
					// 使用附加php
				} else {
					$this->home();
					$this->page = 'home';	
				}
				break;
		}

		@$navObject = ($this->submode ? $this->PN->nav['admin'][$this->mode][$this->submode] : $this->PN->nav['admin'][$this->mode]);
		// $navObject['nav_array'] = $this->PN->createNavMenuArray($navObject['router'],$this->PN->nav);
		$navObject['nav_array'] = $this->PN->nav;
		// $this->navObject['breadcrumb'] = $this->PN->nav;
		// print_r($this->navObject['breadcrumb']);
		$this->assign("admin", $_SESSION['admin']);
		$this->assign("breadcrumb", $this->breadcrumb);
		$this->assign("shop_id", $_SESSION['shop_id']);
		$this->assign('cmode',$this->mode);
		$this->assign('mode',$this->mode);
		$this->assign('submode',$this->submode);
	}

	function try_for_api_file($mode){
		$php_file = 'php/'.$mode.'/'.$mode.'_api.php';
		//echo $php_file;die();
		if(file_exists($php_file)){
			require_once $php_file;
			$this->extraPHP = new extraPHP($this,$this->DB);
			
			return true;
		} 
		return false;
	}

	function try_for_php_file($mode){
		$php_file = 'php/'.$mode.'/'.$mode.'.php';
		if(file_exists($php_file)){
			require_once $php_file;
			$this->extraPHP = new extraPHP($this,$this->DB);
			$this->localStringValueBuild($mode); // build local string file
			return true;
		} 
		return false;
	}

	function default(){
		$result['code'] = '0';
		$result['msg'] = 'Default Done';
		$this->json_return('result',$result,'Y');
	}

	function home(){
		// Dashboard
		$shop_id = $this->parm_sql('session', 'shop_id', 'Y');
		$period = $this->parm_sql('request', 'period');
		if (!$period)
			$period = "7d";

		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->lang);
		$result_array['orderList'] = null;
		$result_array['record'] = null;

		require_once ROOT.'/include/class/member.class.php';
		$member = new member($this->DB, $this->lang);

		$limiter = [
			"page" => 1,
			"limit" => 5
		];

		$sorting = [
			"sort" => "create_datetime",
			"order" => "DESC"
		];

		if ($period == "7d")
			$result_array['orderStat'] = $orderCls->getOrderStatByDay($shop_id, 7);
		else if ($period == "m")
			$result_array['orderStat'] = $orderCls->getOrderStatByMonth($shop_id);

		$result_array['memberList'] = $member->getMemberList($shop_id, ["member_no" => 'Y'], $limiter, $sorting);
		$result_array['orderList'] = $orderCls->getOrderList($shop_id, ['order_status' => 'paid,deliProcess,completed'], $limiter, $sorting);
		$result_array['productList'] = $orderCls->getOrderList($shop_id, ["top30" => 'Y'], $limiter, ["sort" => "counter", "order" => "DESC"]);
		$result_array['period'] = $period;		

		$this->assign("result_array", $result_array);
	}

	function display(){
		$page_setup = file_get_contents(dirname(__FILE__).'/templates/'."/" .$this->page . '.html');
		preg_match_all('%<script type="blt/page_setup_json">(.*?)</script>%s', $page_setup, $page_setup_data); // 找出page setup 的數據
		if(is_array($page_setup_data)){
			if(sizeOf($page_setup_data[1])>0){
				// data found
				preg_match_all('%\[(.*?)\]%s', $page_setup_data[1][0], $page_setup_data_fin); // 找出page setup 的數據
				$page_setup_data_fin = json_decode($page_setup_data_fin[0][0],1);
			}
		}
		if(@is_array($page_setup_data_fin)){
			if(sizeOf($page_setup_data_fin)>0){
				if(isset($this->tpl->tpl_vars['navObject'])){
					// change original nav object
					foreach($page_setup_data_fin[0] as $k=>$i){
						if(isset($i)){
							if(trim($i)!='' || is_array($i)){
								$this->navObject[$k] = $i;		
							}
						}
					}
				} else {

				}
			}
		}
		if(isset($this->navObject['lang'])){
			$nlang = array();
			foreach($this->navObject['lang'] as $k=>$i){
				$nlang[$k] = $i[$this->lang];
			}
			$this->assign('nlang',$nlang);
			unset($this->navObject['lang']);
		}

		$this->assign('navObject', $this->navObject);
		$this->assign('RM', $this->array_merge_keep_key($this->RM->csv,$this->RM->csv_string));
		$this->assign('RMS', $this->RM->csv_string);
		$this->assign('RMJS', json_encode($this->RM->csv_js_string));
		$this->assign('RMG', $this->RMG->csv);
		$this->assign('RMGS', $this->RMG->csv_string);
		$this->assign('RMHJS', json_encode($this->RMG->csv_js_string));
		$this->assign('mode', $this->mode);
		$this->assign('pageStatus', $this->page);
		$this->assign('page', $this->page . '.html');
		
		
		$this->displayPage('index.html');
	}

	function login(){
		
	}

	function capchaImgReg(){
		if($_SESSION['onetimecci']){
			unset($_SESSION['onetimecci']);
		}
		$_SESSION['onetimecci'] = strtolower($this->generateRandomString(4));
		$im = imagecreate(50, 20);
		// White background and blue text
		if($_REQUEST['color']=='white'){
			$bg = imagecolorallocatealpha($im, 255, 255, 255, 0);
		} else {
			$bg = imagecolorallocatealpha($im, 247, 247, 247, 0);
		}
		$textcolor = imagecolorallocate($im, 26, 32, 44);
		// Write the string at the top left
		imagestring($im, 5, 10, 5, $_SESSION['onetimecci'] , $textcolor);
		// Output the image
		header('Content-type: image/png');
		imagepng($im);
		imagedestroy($im);
		exit;
	}

}
$HOME = new admin;
$HOME->display();

?>