<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->DB = $g_DB;

		switch ($this->gthis->submode) {
			case 'resetPassword':
				$result_array = $this->resetPassword();
				break;
			case 'myCart':
				$result_array = $this->myCart();
				break;
			case 'fbLogin':
				$result_array = $this->fbLogin();
				break;
			case 'validate':
				$this->validate();
				break;
			case 'account':
			case 'myVoucher':
				$result_array = $this->account();
				break;
			case 'register':
				$this->register(); 
				break;
			case 'login':
				$result_array = $this->login();
				break;
			case 'logout':
				$this->logout();
				break;
			case 'downloadInvoice':
				$this->downloadInvoice();
				break;
			default:
				break;
		}

		@$this->gthis->assign('result_array',$result_array);
	}

	function downloadInvoice(){
		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang);
		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);



		$html = 'HELLO WORLD';
		require 'include/mpdf/vendor/autoload.php';
		$mpdf = new \Mpdf\Mpdf();
		$mpdf->autoLangToFont = true;
		$mpdf->autoScriptToLang = true;
		$mpdf->WriteHTML($html);
		
		$mpdf->Output('MyPDF.pdf', 'D');
	}

	function resetPassword(){
		$code = $this->gthis->parm_sql("request", "code", 'Y');
		$code = base64_decode($code);

		$code = json_decode($code, true);
		

		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang);
		$member = $memberCls->getMemberDtl($code['member_id']);

		if ($member && $member['pw_valid_code'] == $code['pw_valid_code']){
			$res['memberDtl'] = $member;
			return $res;
		} else {
			$res = [
				"status" => "N"
			];
			return $res;
		}
	}

	function validate(){
		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang);

		$valid_code = $this->gthis->parm_sql("request", "valid_code", 'Y');

		$member = $memberCls->getMemberDtlByValidCode($valid_code);

		$data = [
			"verify" => "Y",
			"valid_code" => ""
		];

		$memberCls->editMember($member['member_id'], $data);
	}

	function login(){
		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang);
		$fbLoginUrl = $memberCls->facebookInit(true); // true for return loginLink
		$res['fbLoginUrl'] = $fbLoginUrl;
		return $res;
	}

	function fbLogin(){
		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang);
		if($memberCls->facebookInit(false)){ // false for return Boolean
			$shop_id = $this->gthis->parm_in('session', 'shop_id');
			$facebook_id = $this->gthis->parm_in("session", "fb_id");
			$email = $this->gthis->parm_in("session", "fb_email");
			$guest_member = $_SESSION['member'];
			$memberCls->loginMember($email, null, $shop_id, $facebook_id, $guest_member['member_id']); // login / create the account when not existed

			$member = $_SESSION['member'];
			    
			if ($member['verify'] == 'N'){
				$memberDtl = $memberCls->generateValidCode($member);
	
				require_once ROOT.'/include/class/email.class.php';
				$emailCls = new email($this->DB, $this->lang);
				$email_content = file_get_contents($this->gthis->baseURL.'/email/register?valid_code=' . $memberDtl['valid_code']);
				$emailCls->sendEmailGo($memberDtl['email'], "The Gift 禮譽帳戶認證", $email_content, $memberDtl['member_id'], "tbl_member");
				
			}
			header('Location: /myAccount/account');
		} else {
			header('Location: /myAccount/register');
		}
	}

	function register(){
		$member = $this->gthis->parm_in('session', 'member');

		if ($member && $member['member_no'])
			header('Location: /myAccount/account');
		else {
			require_once ROOT.'/include/class/member.class.php';
			$memberCls = new member($this->DB, $this->gthis->lang);
			$fbLoginUrl = $memberCls->facebookInit(true); // true for return loginLink
			$this->gthis->assign("fbLoginUrl", $fbLoginUrl);
		}
	}

	function logout(){
		unset($_SESSION['member']);
		unset($_SESSION['facebook_access_token']);

		header('Location: /myAccount/login');
	}

	function account(){
		$ac_page = $this->gthis->parm_in('request', 'ac_page');
		@$this->gthis->assign('ac_page', $ac_page);

		$member = $this->gthis->parm_in('session', 'member');
		$shop_id = $this->gthis->parm_in('session', 'shop_id');

		if (!$member || ($member && !$member['member_no'])){
			header('Location: /myAccount/login');
			die();
		}

		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang);
		$member = $memberCls->getMemberDtl($member['member_id']);

		if ($member['verify'] == 'N'){
			header('Location: /myAccount/verify');
			die();
		} else {
			if ($member['sales_admin_id']){
				require_once ROOT.'/include/class/user.class.php';
				$userCls = new user($this->DB, $this->gthis->lang);
				$sales = $userCls->getAdminDtl($member['sales_admin_id']);
				$member['sales'] = $sales['username'];
			}
	
			$res['memberDtl'] = $member;
			switch($ac_page){
				case 'order':
					require_once ROOT.'/include/class/order.class.php';
					$orderCls = new order($this->DB, $this->gthis->lang);
					
					$orderList = $orderCls->getOrderList($shop_id, ["member_id" => $member['member_id']],null, ["sort" => "create_datetime", "order" => "DESC"]);
	
					foreach($orderList as $k => $order){
						$orderList[$k] = $orderCls->getOrderDtl($order['order_id']);
					}
	
					$res['orderList'] = $orderList;
					break;
				case 'wishlist':
					require_once ROOT.'/include/class/product.class.php';
					$productCls = new product($this->DB, $this->gthis->lang);
	
					$favList = $memberCls->getFavList($member['member_id']);
					foreach ($favList as $k => $fav){
						$favList[$k]['productDtl'] = $productCls->getProductDtl($shop_id, $fav['product_id']);
					}
	
					$res['favList'] = $favList;
					break;
				case 'coupon':
					require_once ROOT.'/include/class/gift.class.php';
					$giftCls = new gift($this->DB, $this->gthis->lang);

					$giftList = $giftCls->getGiftList($shop_id);
					
					$res['giftList'] = $giftList;
					break;
			}
		}




		return $res;
	}

	function myCart(){
		$member_id = $_SESSION['member']['member_id'];
		$shop_id = $this->gthis->parm_in('session', 'shop_id', 'Y');

		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);
		
		$cartDtl = $orderCls->getCart($shop_id, $member_id);
		
		$res['cartDtl'] = $cartDtl;
		
		return $res;
	}
}
?>