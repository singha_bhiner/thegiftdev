/* ************************************************************************ */
/* Author : BTL
/* Prerequisite : jquery 1.7 或以上
/* Desc   : Fullscreen or by element loading cover
/*          全屏或按元素加載封面
/* Last Modify : David 
/* Last Update : 25 May 2020
/* ************************************************************************ */

(function($) {
    $.bloader = function(settings) {
        settings = jQuery.extend({
            loader_type: "body",
            timeout: 99999999,
            close: 'N',
            backgroundColor: '',
            callback: function(obj) {}
        }, settings);

        if (settings.close == 'Y') {
            $('.bloaderHolder').remove();
            return;
        }

        if (settings.loader_type == 'body') {
            // 全屏loader
            var cssInject = "<style>.bloaderPadderParent{position:relative;height:100vh;}.bloaderPadderChild{overflow:hidden;position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);}.bloaderHolder{width: 100%;height: 100%;position: fixed;top: 0;left: 0;background-color: #0000004f;z-index:100000;}</style>";
            cssInject = cssInject + `<style>.bloader,
								.bloader:after {
								  border-radius: 50%;
								  width: 10em;
								  height: 10em;
								}
								.bloader {
								  margin: 60px auto;
								  font-size: 10px;
								  position: relative;
								  text-indent: -9999em;
								  border-top: 1.1em solid rgba(255, 255, 255, 0.2);
								  border-right: 1.1em solid rgba(255, 255, 255, 0.2);
								  border-bottom: 1.1em solid rgba(255, 255, 255, 0.2);
								  border-left: 1.1em solid #ffffff;
								  -webkit-transform: translateZ(0);
								  -ms-transform: translateZ(0);
								  transform: translateZ(0);
								  -webkit-animation: load8 1.1s infinite linear;
								  animation: load8 1.1s infinite linear;
								}
								@-webkit-keyframes load8 {
								  0% {
								    -webkit-transform: rotate(0deg);
								    transform: rotate(0deg);
								  }
								  100% {
								    -webkit-transform: rotate(360deg);
								    transform: rotate(360deg);
								  }
								}
								@keyframes load8 {
								  0% {
								    -webkit-transform: rotate(0deg);
								    transform: rotate(0deg);
								  }
								  100% {
								    -webkit-transform: rotate(360deg);
								    transform: rotate(360deg);
								  }
								}</style>`;


            var loader = "<div class='bloaderHolder'>" + cssInject + "<div class='bloaderPadderParent'><div class='bloaderPadderChild'><div class='bloader'>Loading...</div></div></div></div>";
            $('body').append(loader);
            $('body').css('position', 'relative');
        } else {
            var cssInject = "<style></style>";
            cssInject = cssInject + `<style></style>`;

            var loader = `<div class='bloaderHolder absolute w-full h-full flex justify-center items-start top-0 left-0 rounded-lg' style="background-color:${settings.backgroundColor ? settings.backgroundColor : 'transparent'};">` +
                // cssInject + `
                // 			<div class="bloderInner mx-auto p-24">
                // 				<div class="animate-spin flex text-4xl text-white text-primary">
                // 					<i class="ri-loader-4-line"></i>
                // 				</div>
                // 			</div>
                // 		 </div>`;
                cssInject + `
        					<div class="bloderInner mx-auto p-24">
        						<img src="../img/design/character/box_character.svg" class="w-12 animate-bounce" "/>
        					</div>
        				 </div>`;
            $(settings.loader_type).append(loader);
            $(settings.loader_type).css('position', 'relative');
        }

        function run(funcToExecute) {
            //load some async content
            //funcToExecute(obj); // NOTE: parentheses
        }
    }
}(jQuery));

(function($) {

    var vkLoaderList = []

    $.fn.vkLoader = function(param) {

        var loadingHtmlTemplate = '<div class="flex flex-col h-full w-full items-center pt-20">'+
                        '<div class="bg-white rounded-xl shadow-lg p-12 py-8">'+
                            '<img src="../img/loading.gif" class="mx-auto block w-16 h-16" />'+
                            '<div class="text-center text-gray-600 mt-4 font-semibold ">請稍候...</div>'+
                        '</div></div>';

        if (param == false) {
            if ($(this).lenght == 0) {
                //full body loader
                $('#vk-loader-univerisal').remove()
            } else {
                //loader by covering DOM
                return this.each(function() {
                    var $this = $(this)
                    if ($this.data('_vkLoader') != undefined) {
                        $this.html($this.data('_vkLoader').htmlCache)
                    }
                })
            }

        } else if (!param) {
            if ($(this).lenght == 0) {
                console.log('123')
                    //full body loader
                if ($('#vk-loader-univerisal').length == 0) {
                    $('body').append(`
                            <div id="vk-loader-univerisal" class="fixed top-0 left-0 h-screen w-screen" style="background-color:rgba(0,0,0,.2)">
                                <div class="flex flex-col h-full w-full items-center justify-center">
                                    <div class="bg-white rounded-xl shadow-lg p-12 py-8">
                                        <img src="../img/design/character/box_character.svg" class="mx-auto block w-16 h-16 animate-bounce" />
                                        <div class="text-center text-gray-600 mt-4 font-semibold ">努力加載中...</div>
                                    </div>
                                </div>
                            </div>
                            `)
                }

            } else {
                //console.log('456')
                var settings = $.extend({
                    opacity: 0.5,
                    loadingHtml: loadingHtmlTemplate
                }, param)

                return this.each(function() {



                    var $this = $(this)
                    $this.data('_vkLoader', {
                        id: new Date().getTime(),
                        cssStorage: $this.attr('class'),
                        htmlCache: $this.html()
                    })

                    //start to implement html 
                    $this.css({
                        position: $this.hasClass('absolute') || $this.hasClass('relative') || $this.hasClass('fixed') ? '' : 'relative'
                    }).html(`
                            <div class="opacity-appender" style="opacity : ${settings.opacity}">${$this.html()}</div>
                        `).append(`
                            <div class="vk-loader absolute top-0 left-0 w-full h-full select-none" style="top: 34%; position: fixed; left: 10%;">${settings.loadingHtml}</div>
                        `)


                })
            }
        }

    }

}(jQuery));