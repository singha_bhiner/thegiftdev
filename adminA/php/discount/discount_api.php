<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;
		
		require_once ROOT. '/include/apiDataBuilder.class.php';

		switch ($this->gthis->thirdmode) {
			case 'editVoucher':
				$this->editVoucher();
				break;
			case 'editPtRatio':
				$this->editPtRatio();
				break;
			case 'editCoupon':
				$this->editCoupon();
				break;
			case 'editCompanyDiscount':
				$this->editCompanyDiscount();
				break;
			case 'editGift':
				$this->editGift();
				break;
			case 'editGiftCard':
				$this->editGiftCard();
				break;
			case 'getVoucherList':
				$this->getVoucherList();
				break;
			case 'editGiftDeli':
				$this->editGiftDeli();
				break;
			case 'getVoucherMember':
				$this->getVoucherMember();
				break;
			case 'downloadVoucherMember':
				$this->downloadVoucherMember();
				break;
			case 'downloadGiftCard':
				$this->downloadGiftCard();
				break;
			default:
				new apiDataBuilder(-84, null, null, $this->lang);
				break;
		}

		$result_array['lang'] = $this->lang;
		@$this->gthis->assign('result_array',$result_array);
	}

	function downloadVoucherMember(){
		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/voucher.class.php';
		$voucherCls = new voucher($this->DB, $this->lang, $this->isAdmin);
		
		$data = $this->gthis->paramBuilder([
			["key" => "shop_id", "type" => "int"],
			["key" => "voucher_id", "type" => "int"],
			["key" => "voucher_get_start_date", "type" => "date"],
			["key" => "voucher_get_end_date", "type" => "date"]
		]);

		$member_list = $memberCls->getMemberList($data['shop_id'], $data);

		$res['member_list'] = $member_list;

		$voucher = $voucherCls->getVoucherDtl($data['voucher_id']);

		$file = "voucher - " . $voucher['name_en'] . ".csv";
		$txt = fopen($file, "w") or die("Unable to open file!");
		$csv = "";
		foreach($member_list as $member){
			$csv .= $member['member_no'] . "," . $member['name'] . "," . $member['mobile'] . "," . $member['email']; 
			
			if ($member['voucher_use_date']){
				$csv .= "," . $member['voucher_use_date'] ."(" . $member['order_no'] . ")";
			} else {
				$csv .= ",unused"; 
			}

			$csv .= "," . $member['voucher_get_date'];			
			$csv .= "\n";
		}

		fwrite($txt, $csv);
		fclose($txt);
		ob_clean();
		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename='.basename($file));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		header("Content-Type: text/plain");
		readfile($file);

		die();
	}

	function getVoucherMember(){
		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->lang, $this->isAdmin);
		
		$data = $this->gthis->paramBuilder([
			["key" => "shop_id", "type" => "int"],
			["key" => "voucher_id", "type" => "int"],
			["key" => "voucher_get_start_date", "type" => "date"],
			["key" => "voucher_get_end_date", "type" => "date"]
		]);

		$member_list = $memberCls->getMemberList($data['shop_id'], $data);

		$res['member_list'] = $member_list;

		new apiDataBuilder(0, $res);
	}

	function downloadGiftCard(){
		if (intval($_SESSION['right']['discount']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/giftCard.class.php';
		$giftCard = new giftCard($this->DB, $this->lang, $this->isAdmin);
		
		$data = $this->gthis->paramBuilder([
			["key" =>"card_id", "type" => "int"],
		]);

		$giftCard = $giftCard->getGiftCardDtl($data['card_id']);

		$file = $giftCard['name_en'] . ".csv";
		$txt = fopen($file, "w") or die("Unable to open file!");
		$csv = "";
		foreach($giftCard['keyList'] as $key){
			if (!$key['used_by_id']){
				$csv .= $key['code'] . "," . $key['credit'] . "," . $key['expiry_date'] . "\n";
			}
		}

		fwrite($txt, $csv);
		fclose($txt);
		ob_clean();
		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename='.basename($file));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		header("Content-Type: text/plain");
		readfile($file);

		die();
		new apiDataBuilder(0, null, null, $this->lang);
	}

	function editGiftDeli(){
		if (intval($_SESSION['right']['discount']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		
		require_once ROOT.'/include/class/gift.class.php';
		$gift = new gift($this->DB, $this->lang, $this->isAdmin);

		$deli_id = $this->gthis->parm_sql('request', 'deli_id');
		$data = [];
		$fieldArr = ["remarks", "deli_status"];

		foreach ($fieldArr as $field){
			if (isset($_REQUEST[$field])){
				$$field = $this->gthis->parm_sql('request', $field);
				$data[$field] = $$field;
			}
		}

		$res = $gift->editGiftDeli($deli_id, $data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function getVoucherList(){
		if (intval($_SESSION['right']['discount']) < 30){
			return array("right"=>"Failure");
		}
        require_once ROOT.'/include/class/voucher.class.php';
        $voucher = new voucher($this->DB, $this->lang, $this->isAdmin);

        $shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_sql('request', 'skey', 'N');
        
		$filter = [
			"skey" => $skey ? $skey : null,
		];

		$res = $voucher->getVoucherList($shop_id, $filter);
		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editGift(){
		if (intval($_SESSION['right']['discount']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		
		require_once ROOT.'/include/class/gift.class.php';
		$gift = new gift($this->DB, $this->lang, $this->isAdmin);


		$data = $this->gthis->paramBuilder([
			["key" => "gift_id", "type" => "int"],
			["key" => "shop_id", "request" => "session", "type" => "int"],
			["key" => "name_tc"],
			["key" => "name_en"],
			["key" => "required_point", "type" => "int"],
			["key" => "quota", "type" => "int"],
			["key" => "activate", "type" => "NY"],
		]);

		$gift_img = $this->gthis->parm_in('file', 'gift_img', 'N');
		$gift_img['tmp_name']? $data['gift_img'] = $gift_img : null;

		$res = $gift->editGift($data['gift_id'], $data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editGiftCard(){
		if (intval($_SESSION['right']['discount']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/giftCard.class.php';
		$giftCard = new giftCard($this->DB, $this->lang, $this->isAdmin);
		
		$data = $this->gthis->paramBuilder([
			["key" =>"card_id", "type" => "int"],
			["key" =>"shop_id", "type" => "int"],
			["key" =>"credit", "type" => "int"],
			["key" =>"quota", "type" => "int"],
			["key" =>"name_tc"],
			["key" =>"name_en"],
			["key" =>"expiry_date", "type" => "date"],
		]);

		$res = $giftCard->editGiftCard($data['card_id'], $data);
		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editCompanyDiscount(){
		if (intval($_SESSION['right']['discount']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		
		require_once ROOT.'/include/class/companyDiscount.class.php';
		$discount = new companyDiscount($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "discount_id", "compulsory" => true, "type" => "int", "allowBlank" => false],
			["key" => "shop_id", "type" => "int"],
			["key" => "company_name"],
			["key" => "email_suffix", "type" => "arrV1"],
			["key" => "credit", "type" => "int"],
			["key" => "quota", "type" => "int"],
			["key" => "expiry_date", "type" => "date"],
			["key" => "activate", "type" => "NY"],	
		]);
		
		$res = $discount->editCompanyDiscount($data);
		
		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editPtRatio(){
		if (intval($_SESSION['right']['discount']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		
		require_once ROOT.'/include/class/shop.class.php';
		$shop = new shop($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('request', 'shop_id', 'Y');
		$data = [];
		$fieldArr = ["point_ratio"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
				case 'shop_id':
					$$field = $this->DB->parseInt($$field);
					if (!$$field){
						new apiDataBuilder(-99, null, "$field cannot be null", $this->lang);
					}
					break;
				case 'point_ratio':
					if (!isset($$field)){
						new apiDataBuilder(-99, null, "$field cannot be null", $this->lang);
					}
			}

			isset($$field)? $data[$field] = $$field: null;
		}

		$res = $shop->editShop($shop_id, $data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editCoupon(){
		if (intval($_SESSION['right']['discount']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		
		require_once ROOT.'/include/class/coupon.class.php';
		$coupon = new coupon($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "coupon_id", "type" => "int"],
			["key" => "sales_admin_id", "type" => "int"],
			["key" => "shop_id", "type" => "int"],
			["key" => "discount", "type" => "int"],
			["key" => "usage_limit", "type" => "int"],
			["key" => "minimum_order_amt", "type" => "int"],
			["key" => "wlist_product_id_arr", "type" => "intArr"],
			["key" => "blist_product_id_arr", "type" => "intArr"],
			["key" => "wlist_cate_id_arr", "type" => "intArr"],
			["key" => "blist_cate_id_arr", "type" => "intArr"],
			["key" => "cate_id_arr", "type" => "intArr"],
			["key" => "adm_name"],
			["key" => "type"],
			["key" => "code"],
			["key" => "allow_voucher", "type" => "NY"],
			["key" => "member_only", "type" => "NY"],
			["key" => "activate", "type" => "NY"],
			["key" => "start_date", "type" => "date"],
			["key" => "end_date", "type" => "date"],
			["key" => "email_suffix", "type" => "arrV1"]
		]);

		$res = $coupon->editCoupon($data['coupon_id'], $data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editVoucher(){
		if (intval($_SESSION['right']['discount']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		
		require_once ROOT.'/include/class/voucher.class.php';
		$voucher = new voucher($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "voucher_id", "type" => "int"],
			["key" => "remarks_tc"],
			["key" => "remarks_en"],
			["key" => "name_tc"],
			["key" => "name_en"],
			["key" => "sales_admin_id", "type" => "int"],
			["key" => "shop_id", "type" => "int"],
			["key" => "discount", "type" => "int"],
			["key" => "type"],
			["key" => "minimum_order_amt", "type" => "int"],
			["key" => "ava_day", "type" => "int"],
			["key" => "expiry_date", "type" => "date"],
			["key" => "redeem"],
			["key" => "is_ava_discount_product", "type" => "NY"],
			["key" => "activate", "type" => "NY"],
			["key" => "cate_id_arr", "intArr"],
		]);

		if (array_key_exists('expiry_date', $data) && !$data['expiry_date']){
			$data['expiry_date'] = "NULL";
		} 

		$res = $voucher->editVoucher($data['voucher_id'], $data);
		new apiDataBuilder(0, $res, null, $this->lang);
	}
}
?>