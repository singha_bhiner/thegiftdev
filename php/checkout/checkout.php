<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->DB = $g_DB;

		switch ($this->gthis->submode) {
			case 'checkoutConfirmation':
                $result_array = $this->checkoutConfirmation();
                break;
            case 'checkoutConfirmAddress':
                $result_array = $this->checkoutConfirmAddress();
                break;
			case 'transactionProcessing':
				$result_array = $this->transactionProcessing();
				break;
			case 'offlineCheckoutConfirm':
				$result_array = $this->offlineCheckoutConfirm();
				break;
			default:
				break;
		}

		@$this->gthis->assign('result_array',$result_array);
	}

	function offlineCheckoutConfirm(){
		$encryptedCode = $this->gthis->parm_in('request', 'order');

		$order = json_decode(base64_decode($encryptedCode), true);

		$res['order'] = $order;

		return $res;
	}

	function transactionProcessing(){
		$member = $_SESSION['member'];
		$member_id = $member['member_id'];
		$shop_id = $member['shop_id'];
		$encryptedCode = $this->gthis->parm_in('request', 'payment_id');

		$waiting_code = json_decode(base64_decode($encryptedCode), true);

		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);
		
		$order = $orderCls->getOrderDtlByMemberid($shop_id, $member_id); // get the updated order

		if ($order['order_status'] == 'incart'){
			// if the order_status is incart which has not execute the payment
			$final_price = $order['product_price'] + $order['shipping_fee'] + $order['timeslot_fee']; 
	
			// $order_no = $orderCls->genOrderNo($shop_id);
			$orderData = [	
				"order_status" => "waitingPayment",
				// "order_no" => $order_no,
				"order_datetime" => date("Y-m-d H:i:s"),
			];
	
			if ($waiting_code['coupon_code']){
				require_once ROOT.'/include/class/coupon.class.php';
				$couponCls = new coupon($this->DB, $this->gthis->lang);
				
				$couponDtl = $couponCls->getCouponDtlByCode($waiting_code['coupon_code'], $final_price);
				if ($couponDtl['sales_admin_id'])
					$orderData['sales_admin_id'] = $couponDtl['sales_admin_id'];
				$orderData['coupon_code_id'] = $couponDtl['coupon_id'];
				$orderData['coupon_discount'] = $couponDtl['act_discount'];
				$orderData['coupon_code'] = $couponDtl['code'];
			}
	
			if ($waiting_code['member_voucher_id']){
				require_once ROOT.'/include/class/voucher.class.php';
				$voucherCls = new voucher($this->DB, $this->gthis->lang);
				$voucherDtl = $voucherCls->getVoucherDtlByMemberVoucher($waiting_code['member_voucher_id'], $final_price);
				$orderData['voucher_discount'] = $voucherDtl['act_discount'];
			}
			
			$orderCls->editOrder($order['order_id'], $orderData);
	
			foreach($order['deliList'] as $deli){
				$deliData = [
					"shop_id" => $shop_id,
					"deli_status" => "waitingPayment",
					// "deli_no" => $orderCls->genDeliNo($order_no, $deli['deli_date'])
				];
				$orderCls->editDeli($deli['deli_id'], $deliData, $order_no);


				// // reserve the item
				// $deliDtl = $orderCls->getDeliDtl($deli['deli_id']); // get the itemList
				// $itemList = $deliDtl['itemList'];

				// foreach ($itemList as $item){
				// 	$itemCls->stock_reserve($item['sku_id'], $item['qty']);
				// }
			}
		}


		$res['orderDtl'] = $order;
		$res['payment_id'] = base64_encode($waiting_code['payment_id']);
		
		return $res;
	}

    function checkoutConfirmAddress(){
        $member = $_SESSION['member'];
		$member_id = $member['member_id'];
		$shop_id = $this->gthis->parm_in('session', 'shop_id', 'Y');

		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);
		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->gthis->lang);
		
		$order = $orderCls->getCart($shop_id, $member_id);
		$product_id_arr = [];
		$deli_group_id_arr = [];
		foreach ($order['deliList'] as $deli){
			array_push($product_id_arr, $deli['product_id']);
			array_push($deli_group_id_arr, $deli['productDtl']['deli_group_id']);
		}
		// $deli_group_id_arr = array_unique($deli_group_id_arr);
		
		$res['cartDtl'] = $order;
		$districtList = $deliveryCls->getDistrictList($shop_id);
		foreach($districtList as $kk => $district){
		// 	// $regionList = $deliveryCls->getRegionList($shop_id, ["district_id" => $district['district_id'], "cust_deli_id_arr" => implode(",", $cust_deli_id_arr)]);
			$regionList = $deliveryCls->getRegionListClient($district['district_id'], $deli_group_id_arr, $shop_id);
			// $districtList[$kk]['regionList'] = $regionList;
			$districtList[$kk]['region_list'] = $regionList;
		}
		// $timeslotList = $deliveryCls->getTimeslotList($shop_id);
		// print_r(json_encode($districtList));
		// die();
		
		$res['districtList'] = $districtList;
		$res['timeslotList'] = $timeslotList;

		
		require_once ROOT.'/include/class/holiday.class.php';
		$holidayCls = new holiday($this->DB, $this->gthis->lang);
		$filter = [
			"isCurrent" => 'Y',
			"product_id_list" => implode(",", $product_id_arr)
		];

		$holidayList = $holidayCls->getHolidayList($shop_id, $filter);
		$res['holidayList'] = $holidayList;

		// start / renew to reserve
		$orderCls->editOrder($order['order_id'], [
			"reserve_time" => ((new DateTime(date('Y-m-d H:i:s')))->modify('+' . RESERVE_TIME . ' minutes'))->format('Y-m-d H:i:s')
		]);

		return $res;
    }

	function checkoutConfirmation(){
		$member = $_SESSION['member'];
		$member_id = $member['member_id'];
		$shop_id = $this->gthis->parm_in('session', 'shop_id', 'Y');

		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);
        require_once ROOT.'/include/class/voucher.class.php';
        $voucherCls = new voucher($this->DB, $this->gthis->lang);
        require_once ROOT.'/include/class/product.class.php';
        $productCls = new product($this->DB, $this->gthis->lang, true);

		$order = $orderCls->getOrderDtlByMemberid($shop_id, $member_id); 
		$orderCls->updateOrderDeliPrice($order, $member_id); // re-calculate the price
		$order = $orderCls->getOrderDtlByMemberid($shop_id, $member_id); // get the updated order
		$deliList = $order['deliList'];
		$final_price = $order['product_price'] + $order['addon_price'];
		
		// get the productList related category
		$cate_arr = [];
		foreach($deliList AS $deli){
			$product = $productCls->getProductDtl($shop_id, $deli['product_id']);
			foreach($product['cate'] as $cate){
				foreach($cate['subcate'] as $subcate){
					array_push($cate_arr, $subcate['cate_id']);
				}
			}
		}

		foreach($member['voucherList'] as $k => $voucher){
			if($voucherCls->isVoucherAva($voucher['member_voucher_id'], $final_price, $cate_arr)){
				$member['voucherList'][$k]["ava"] = 'Y';
			} else {
				$member['voucherList'][$k]["ava"] = 'N';
			}
		}

		$res['voucherList'] = $member['voucherList'];
		$res['cartDtl'] = $order;		




		////////////////////// AE CHECKOUT START //////////////////////

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://gateway-japa.americanexpress.com/api/nvp/version/63");
		curl_setopt($ch, CURLOPT_POST, 1);


		$field_list = [
			"apiOperation=INITIATE_CHECKOUT",
			"apiPassword=94be89601306afad0a476dc4f2808d49",
			"apiUsername=merchant.TEST9811921469",
			"merchant=TEST9811921469",
			"interaction.operation=PURCHASE",
			"order.id=" . $order['order_id'],
			"order.amount=100.00",
			"order.currency=HKD"
		];

		$postField = join("&", $field_list);
		// echo $postField;

		curl_setopt($ch, CURLOPT_POSTFIELDS, $postField);

		// In real life you should use something like:
		// curl_setopt($ch, CURLOPT_POSTFIELDS, 
		//          http_build_query(array('postvar1' => 'value1')));

		// Receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec($ch);
		
		$ae_list = [];
		$return_list = explode("&", $server_output);

		foreach ($return_list as $return){
			$tmp = explode("=", $return );

			if (str_contains($tmp[0], ".")){
				$t2 = explode(".", $tmp[0]);
				$ae_list[$t2[0]][$t2[1]] = $tmp[1];
			} else {
				$ae_list[$tmp[0]] = $tmp[1];
			}
		}

		curl_close ($ch);
		
		$res["ae"] = $ae_list;
		////////////////////// AE CHECKOUT END   //////////////////////



		return $res;
	}
}
?>