<?php
/* 
    Objective: coupon code related 
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_item_cate


        
*/
require_once dirname(__FILE__). '/abstract.class.php';
class itemCate extends baseClass{

    function editItemCate($data){
        if ($data['cate_id'] == 0){
            $cate_id = $this->DB->insert_db('tbl_item_cate', $data);
        } else {
            $cate_id = $this->DB->update_Sql("tbl_item_cate", "cate_id", $data);
        }

        if (isset($data['various_json'])){
            $various_type_id_arr = $this->DB->updateForeignMap("tbl_item_cate_various_type", "cate_id", $cate_id, "type_id", $data['various_json']);

            foreach($various_type_id_arr as $k => $type_id){
                $this->DB->updateForeignMap("tbl_item_cate_various_val", "type_id", $type_id, "val_id", $data['various_json'][$k]['val_json']);
            }
        }
    
        return $this->getItemCateDtl($cate_id);
    }

    function getItemCateList($filter = null, $limiter = null, $sorting = null){
        $fromShort = "st";
		$selectSql = "SELECT $fromShort.* ";
		$fromSql = " FROM `tbl_item_cate` as $fromShort";
		$whereSql = " WHERE $fromShort.`activate` = 'Y'";
        $groupSql = " GROUP BY $fromShort.`cate_id`";
		if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
			$whereSql = " WHERE 1=1";

		if ($sorting)
			$sortSql = " ORDER BY ". $sorting['sort'] . " " . $sorting['order'];
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
				if ($i){
					switch ($k){
                        case 'cate_id':
                            $whereSql .= " AND ($fromShort.`cate_id` = '$i')";
                            break;
					}
				}
			}
		}
		
		$sql = $selectSql . $fromSql . $whereSql . $groupSql . $sortSql . $limitSql;
		$res = $this->DB->get_Sql($sql);

		return $this->dispatchItemCate($res);
    }

    function getItemCateDtl($cate_id){
        if ($cate_id)
            return $this->getItemCateList(["cate_id" => $cate_id], $this->DB->getUnique)[0];
        else 
            return null;
    }

    function dispatchItemCate($cateList){
        require_once ROOT.'/include/class/cateVarious.class.php';
        $cateVariousCls = new cateVarious($this->DB, $this->lang, $this->isAdmin);

        $res = [];
        // generate the parent_level
        foreach ($cateList as $k => $cate){
            if (!$cate['parent_cate_id']){
                // print_r($tag);
                array_push($res, $cate);
            }
        }

        // generate the child_level
        foreach ($cateList as $k => $cate){
            if ($cate['parent_cate_id']){
                $parentIdx = array_keys(array_filter($res, fn($par) => $par['cate_id'] == $cate['parent_cate_id']))[0];
                
                if ($parentIdx === null){
                    $itemCateDtl = $this->getItemCateDtl($cate['parent_cate_id']);
                    if ($itemCateDtl){
                        array_push($res, $itemCateDtl);
                        $parentIdx = array_keys(array_filter($res, fn($par) => $par['cate_id'] == $cate['parent_cate_id']))[0];
                    }
                }

                if ($parentIdx || $parentIdx == '0'){
                    if (!isset($res[$parentIdx]['child_list'])){
                        $res[$parentIdx]['child_list'] = [];
                    }
    
                    // cate get its various option

                    $cate['various_list'] = $cateVariousCls->getCateVariousTypeList(["cate_id" => $cate['cate_id']]);

                    array_push($res[$parentIdx]['child_list'], $cate);
                }
            }
        }

        return $res;
    }

    function sortItemCate($cate_id_arr){
        $this->DB->sort("tbl_item_cate", "cate_id", $cate_id_arr);
        return null;
    }
}
?>