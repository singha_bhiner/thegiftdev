<?php
require_once ROOT. '/include/class/abstract.class.php';
class extraPHP extends baseClass{
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->DB = $g_DB;
        $this->res = null;

		require_once ROOT. '/include/apiDataBuilder.class.php';
		
		switch ($this->gthis->thirdmode) {
			case 'getSummary':
				$this->getSummary();
				break;
			default:
				new apiDataBuilder(-84);
				break;
		}

		new apiDataBuilder(0, $this->res);
	}
    
    function getSummary(){
		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->lang, $this->isAdmin);

        $shop_id = $this->gthis->param(["key" => "shop_id", "type" => "int"]),
        $data = $this->gthis->paramBuilder([
            ["key" => "member", "request" => "session"],
            ["key" => "deli_date_start", "type" => "date"],
            ["key" => "deli_date_end", "type" => "date"]
        ]);

        $orderCls->getDeliList($shop_id, $data);

    }

    function editBlogCategory(){
		require_once ROOT.'/include/class/blog.class.php';
		$blogCls = new blog($this->DB, $this->lang, $this->isAdmin);

        $data = $this->gthis->paramBuilder([
            ["key" => "blog_category_id", "type" => "int"],
            ["key" => "name"],
            ["key" => "description"],
        ]);

        $blog_category_id = $blogCls->editBlogCategory($data);

        $res['blog_category'] = $blogCls->getBlogCategory($blog_category_id);

        $this->res = $res;

    }

    function delBlogCategory(){
		require_once ROOT.'/include/class/blog.class.php';
		$blogCls = new blog($this->DB, $this->lang, $this->isAdmin);

        $data = $this->gthis->paramBuilder([
            ["key" => "blog_category_id", "type" => "int"],
            ["key" => "activate", "request" => "self", "defaultValue" => 'N']
        ]);

		$blogCategory = $blogCls->getblogCategory($data['blog_category_id']);
		if ($blogCategory){
			$data['blog_category_id'] = $blogCategory['blog_category_id'];
			$blogCls->editBlogCategory($data);

            new apiDataBuilder(0, $data);
		} else {
			new apiDataBuilder(-900, null, $this->gthis->lang);
		}
    }

    function getBlogCategoryList(){
        require_once ROOT.'/include/class/blog.class.php';
		$blogCls = new blog($this->DB, $this->lang, $this->isAdmin);
        
        $blog_category_list = $blogCls->getBlogCategoryList($filter, $sorting);

        $res['blog_category_list'] = $blog_category_list;

        $this->res = $res;
    }
    
    function uploadBlogImg(){
        $tmp_img = $this->gthis->parm_in('file', 'file');
		$img_path = $this->DB->uploadImg($tmp_img, "blog", 0);
		$res['img_path'] = $this->gthis->baseURL."/api/readfile/readFileCode?code=".urlencode(base64_encode($this->dataEncrypt($img_path)));
		$res['img_path_raw'] = $img_path;
		new apiDataBuilder(0, $res);
    }

    function getBlogList(){
		require_once ROOT.'/include/class/blog.class.php';
		$blogCls = new blog($this->DB, $this->lang, $this->isAdmin);

        $filter = $this->gthis->paramBuilder([
            ["key" => "skey"],
            ["key" => "cate_id_arr"],
        ]);

        $data = $this->gthis->paramBuilder([
            ["key" => "sort"],
            ["key" => "order"],
        ]);

        if ($data['sort'] && $data['order']){
            $sorting = $data['sort'] . " " . $data['order'];
        }
        
        $blog_list = $blogCls->getBlogList($filter, $sorting);

        $res['blog_list'] = $blog_list;

        $this->res = $res;
    }

    function editBlog(){
		require_once ROOT.'/include/class/blog.class.php';
		$blogCls = new blog($this->DB, $this->lang, $this->isAdmin);

        $data = $this->gthis->paramBuilder([
            ["key" => "blog_id", "type" => "int"],
            ["key" => "title"],
            ["key" => "blog_category_id_arr", "type" => "intArr"],
            ["key" => "blog_tag_arr", "type" => "arr"],
            ["key" => "content"],
            ["key" => "url_slug"],
            ["key" => "youtube_url"],
            ["key" => "structure_data_json"],
            ["key" => "faq_json"],
            ["key" => "blog_author"],
            ["key" => "hash_tag", "allowNull" => false],
            ["key" => "cover_image_path"],
            ["key" => "is_publish", "type" => "NY"],
        ]);

        // create blog author
        if (isset($data['blog_author'])){
            if ($data['blog_author']){
                $blog_tag = $blogCls->getBlogAuthorByName($data['blog_author']);
    
                if (!$blog_tag){
                    $data['blog_author_id'] = $blogCls->editBlogAuthor([
                        "name" => $data['blog_author']
                    ]);
                } else {
                    $data['blog_author_id'] = $blog_tag['blog_author_id'];
                }
            } else {
                $data['blog_author_id'] = null;
            }
        }

        // create blog tag
        if (isset($data['blog_tag_arr'])){
            $blog_tag_id_arr= [];
            foreach($data['blog_tag_arr'] as $k => $blog_tag_name){
                if ($blog_tag_name){
                    $blog_tag = $blogCls->getBlogTagByName($blog_tag_name);
        
                    if (!$blog_tag){
                        array_push($blog_tag_id_arr, $blogCls->editBlogTag([
                            "name" => $blog_tag_name
                        ]));
                    } else {
                        array_push ($blog_tag_id_arr, $blog_tag['blog_tag_id']);
                    }
                }
            }
            $data['blog_tag_id_arr'] = $blog_tag_id_arr;
        }

        $blog_id = $blogCls->editBlog($data);

        $res['blog'] = $blogCls->getBlog($blog_id);

        $this->res = $res;
    }

    function delBlog(){
		require_once ROOT.'/include/class/blog.class.php';
		$blogCls = new blog($this->DB, $this->lang, $this->isAdmin);

        $data = $this->gthis->paramBuilder([
            ["key" => "blog_id", "type" => "int"],
            ["key" => "activate", "request" => "self", "defaultValue" => 'N']
        ]);

		$blog = $blogCls->getBlog($data['blog_id']);
		if ($blog){
			$data['blog_id'] = $blog['blog_id'];
			$blogCls->editBlog($data);

            new apiDataBuilder(0, $data);
		} else {
			new apiDataBuilder(-900, null, $this->gthis->lang);
		}
    }

    function getBlog(){
		require_once ROOT.'/include/class/blog.class.php';
		$blogCls = new blog($this->DB, $this->lang, $this->isAdmin);

        $data = $this->gthis->paramBuilder([
            ["key" => "blog_id", "type" => "int"],
        ]);

        $res['blog'] = $blogCls->getBlog($data['blog_id']);

        $this->res = $res;
    }

    function editBlogTag(){
		require_once ROOT.'/include/class/blog.class.php';
		$blogCls = new blog($this->DB, $this->lang, $this->isAdmin);

        $data = $this->gthis->paramBuilder([
            ["key" => "blog_tag_id", "type" => "int"],
            ["key" => "name"],
        ]);

        $blog_id = $blogCls->editBlogTag($data);

        $res['blog_tag'] = $blogCls->getBlogTag($blog_id);

        $this->res = $res;
    }

    function delBlogTag(){
		require_once ROOT.'/include/class/blog.class.php';
		$blogCls = new blog($this->DB, $this->lang, $this->isAdmin);

        $data = $this->gthis->paramBuilder([
            ["key" => "blog_tag_id", "type" => "int"],
            ["key" => "activate", "request" => "self", "defaultValue" => 'N']
        ]);

		$blog = $blogCls->getBlogTag($data['blog_tag_id']);
		if ($blog){
			$blogCls->editBlogTag($data);

            new apiDataBuilder(0, $data);
		} else {
			new apiDataBuilder(-900, null, $this->gthis->lang);
		}
    }

    function getBlogTagList(){
		require_once ROOT.'/include/class/blog.class.php';
		$blogCls = new blog($this->DB, $this->lang, $this->isAdmin);

        $res['blog_tag_list'] = $blogCls->getBlogTagList();

        $this->res = $res;
    }

    function getBlogAuthorList(){
		require_once ROOT.'/include/class/blog.class.php';
		$blogCls = new blog($this->DB, $this->lang, $this->isAdmin);

        $res['blog_author_list'] = $blogCls->getBlogAuthorList();

        $this->res = $res;
    }
}
?>