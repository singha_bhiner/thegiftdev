<?php
/* 
    Objective: logger
    Author: Sing
    Last major update: 03-11-2021

	Related table: 
        log_*
        tbl_sql_audit_log
        

*/

class pushNotification {
    function createPushNotification($url, $content){
        $hashes_array = array();
        $fields = array(
            'app_id' => oneSignalPublicKey,
            'included_segments' => array(
                'Subscribed Users'
            ),
            'data' => array(
                "foo" => "bar"
            ),
            'url' => $url,
            'contents' => array(
                "en" => $content
            ),
            'web_buttons' => $hashes_array
        );
        
        $fields = json_encode($fields);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic '.oneSignalPrivateKey
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
        $response = curl_exec($ch);
        curl_close($ch);
    }
}
?>