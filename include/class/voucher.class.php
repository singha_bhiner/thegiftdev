<?php
/* 
    Objective: Voucher related
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_voucher
        tmap_new_member_voucher

        
*/


require_once dirname(__FILE__). '/abstract.class.php';
class voucher extends baseClass{

    function isVoucherAva($member_voucher_id, $final_price, $cate_arr = null){
        // check whether the voucher is ava in this price and category
        // $member_voucher_id: PK
        // $final_price: checking the voucher is available in this voucher
        // $cate_arr: category_id array in this order
        $sql = "SELECT v.* 
                FROM `tbl_member_voucher` AS v
                WHERE v.activate = 'Y' AND v.redeemed = 'N' AND v.member_voucher_id = '$member_voucher_id' 
                    AND v.minimum_order_amt < '$final_price' 
                    AND (v.expiry_date > NOW() OR v.expiry_date IS NULL) 
                    AND (v.create_datetime + INTERVAL ava_day DAY) > NOW()
                    ";
                    
        $res = $this->DB->get_Sql($sql)[0];

        if ($res){
            if ($res['type'] == 'P'){
                $res['act_discount'] = round($final_price * $res['discount'] / 100);
            } elseif ($res['type'] == 'A'){
                $res['act_discount'] = $res['discount'];
            }

            if ($cate_arr){
                $sql = "SELECT * FROM `tmap_cate_voucher` AS m WHERE m.activate ='Y' AND voucher_id = '".$res['voucher_id']."'";
                $voucherCateList = $this->DB->get_Sql($sql);
                
                if ($voucherCateList){
                    foreach($voucherCateList as $k => $cate){
                        if (in_array($cate['cate_id'], $cate_arr)){
                            // black list existed and one of the category is in the black list
                            return false;
                        }
                    }

                    // black list is existed but the category are not in the white list
                    return true;
                } else {
                    // Assume no black list, which mean all the category allow
                    return true;
                }
            } else {
                // without category checking
                return true;
            }
        } else {
            // cannot find voucher
            return false;
        }


    }

    function getVoucherDtlByMemberVoucher($member_voucher_id, $final_price, $cate_arr = null){
        // get and validate voucher detail 
        // $member_voucher_id: PK
        // $final_price: checking the voucher is available in this voucher
        // $cate_arr: category_id array in this order

        $sql = "SELECT v.* 
                FROM `tbl_member_voucher` AS v
                WHERE v.activate = 'Y' AND v.redeemed = 'N' AND v.member_voucher_id = '$member_voucher_id' 
                    AND v.minimum_order_amt < '$final_price' 
                    AND (v.expiry_date > NOW() OR v.expiry_date IS NULL) 
                    AND (v.create_datetime + INTERVAL ava_day DAY) > NOW()
                    ";
        $res = $this->DB->get_Sql($sql)[0];

        if ($res){
            if ($res['type'] == 'P'){
                $res['act_discount'] = round($final_price * $res['discount'] / 100);
            } elseif ($res['type'] == 'A'){
                $res['act_discount'] = $res['discount'];
            }

            if ($cate_arr){
                $sql = "SELECT * FROM `tmap_cate_voucher` AS m WHERE m.activate ='Y' AND voucher_id = '".$res['voucher_id']."'";
                $voucherCateList = $this->DB->get_Sql($sql);
    
                if ($voucherCateList){
                    foreach($voucherCateList as $k => $cate){
                        if (in_array($cate['cate_id'], $cate_arr)){
                            // black list existed and one of the category is in the black list
                            new apiDataBuilder(10, null, "Voucher is not in the blackList");
                        }
                    }
    
                    // black list is existed but the products are not in the white list
                    return $res;
                } else {
                    // Assume no white list, which mean all the products allow
                    return $res;
                }
            } else {
                // without category checking
                return $res;
            }
        } else {
            // cannot find voucher
            new apiDataBuilder(10, null, "Cannot find the voucher");
        }
    }

    function executeVoucher($member_voucher_id){
        // execute the voucher
        // $member_voucher_id: PK

        $sql = $this->DB->buildSql_update("tbl_member_voucher", "member_voucher_id", $member_voucher_id, 
            [
                "redeemed" => 'Y'
            ]
        );
        $this->DB->update($sql);

        return true;
    }

    function editVoucher($voucher_id, $data){
        // edit voucher
        // $voucher_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($voucher_id == 0){
            $voucher_id = $this->DB->insert_db('tbl_voucher', $data);
        } else {
            $voucher_id = $this->DB->update_sql("tbl_voucher", "voucher_id", $data);
        }

        $this->DB->updateMap("tmap_cate_voucher", "voucher_id", $voucher_id, "cate_id", $data['cate_id_arr']);
        unset($data['cate_id_arr']);

        return $this->getVoucherDtl($voucher_id);
    }

    function getNewMemberVoucherList($shop_id){
        // get if any voucher get when new member registration
        // $shop_id: shop in use

        $sql = "SELECT * FROM `tmap_new_member_voucher` WHERE `shop_id` = '$shop_id' AND `activate` = 'Y'";
        
        $res = $this->DB->get_Sql($sql);
        return $res;
    }

    function getVoucherList($shop_id, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $sql = "SELECT * FROM `tbl_voucher` WHERE `shop_id` = '$shop_id' AND `activate` ='Y'";

        foreach($filter AS $k => $i){
            if ($i){
                switch($k){
                    case 'skey':
                        $sql .= " AND (name_tc LIKE '%$i%' OR name_en LIKE '%$i%') ";
                        break;
                }
            }
        }

        $sql .= " GROUP BY voucher_id";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        return $res;
    }

    function getVoucherDtl($voucher_id){
        // get the voucher Detail
        // $voucher_id: PK

        $sql = "SELECT v.*, GROUP_CONCAT(s.cate_id) AS cate_id_arr
                FROM `tbl_voucher` AS v
                LEFT JOIN `tmap_cate_voucher` AS s ON s.voucher_id = v.voucher_id AND s.activate ='Y'
                WHERE v.`activate` = 'Y' && v.`voucher_id` = '$voucher_id'";
        $res = $this->DB->get_Sql($sql)[0];

        if ($res){
            $sql = "SELECT * 
                    FROM `tmap_cate_voucher` AS s 
                    INNER JOIN `tbl_cate` AS c ON c.cate_id = s.cate_id
                    WHERE s.activate = 'Y' AND c.activate = 'Y' AND s.voucher_id = '$voucher_id'";
            $res['cate'] = $this->DB->get_Sql($sql);
            $arr = [];
            foreach($res['cate'] as $cate){
                array_push($arr, $cate['cate_id']);
            }
            $res['cate_id_arr'] = $arr;

            $sql = "SELECT * 
                    FROM `tbl_order` AS o
                    WHERE o.`activate` ='Y' AND o.`member_voucher_id` = '$voucher_id'";
            $res['orderList'] = $this->DB->get_Sql($sql);

            // $sql = "SELECT m.*, mv.redeemed, o.create_datetime AS redeem_datetime
            //         FROM `tbl_member_voucher` AS mv
            //         LEFT JOIN `tbl_order` AS o ON o.member_voucher_id = mv.member_voucher_id AND o.activate ='Y'
            //         LEFT JOIN `tbl_member` AS m ON m.member_id = mv.member_id AND m.activate = 'Y'
            //         WHERE mv.activate = 'Y' AND mv.voucher_id = '$voucher_id'
            //         ORDER BY mv.member_voucher_id DESC
            //         LIMIT 0, 50;";
            // $res['memberList'] = $this->DB->get_Sql($sql);

            $sql = "SELECT * 
                    FROM `tbl_admin`
                    WHERE admin_id = '".$res['sales_admin_id']."'";
            $res['sales'] = $this->DB->get_Sql($sql);
        }

        return $res;
    }
}
?>
