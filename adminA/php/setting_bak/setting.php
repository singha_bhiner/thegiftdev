<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->DB = $g_DB;
		
		switch ($this->gthis->submode) {
			case 'greetingCateList':
				$result_array = $this->greetingCateList();
				break;
			case 'greetingCateDtl':
				$result_array = $this->greetingCateDtl();
				break;
			case 'greetingSubcateDtl':
				$result_array = $this->greetingSubcateDtl();
				break;
			case 'holidayList':
				$result_array = $this->holidayList();
				break;
			case 'holidayDtl':
				$result_array = $this->holidayDtl();
				break;
			case 'timeslotList':
				$result_array = $this->timeslotList();
				break;
			case 'timeslotDtl':
				$result_array = $this->timeslotDtl();
				break;
			case 'regionList':
				$result_array = $this->regionList();
				break;
			case 'regionDtl':
				$result_array = $this->regionDtl();
				break;
			case 'custDeliList':
				$result_array = $this->custDeliList();
				break;
			case 'custDeliDtl':
				$result_array = $this->custDeliDtl();
				break;
			default:
				break;
		}

		@$this->gthis->assign('result_array',$result_array);
	}

	function custDeliList(){
		if (intval($_SESSION['right']['deli']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->gthis->lang);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$result_array['grpList'] = $delivery->getCustDeliGrpList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "custDeliList",
			"url" => "admin/setting/custDeliList",
			"page_title" => $this->gthis->RMBC->getReturnString('custDeliList')
		]);
		return $result_array;
	}

	function custDeliDtl(){
		if (intval($_SESSION['right']['deli']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->gthis->lang);

		$grp_id = $this->gthis->parm_sql('request', 'grp_id', 'N');
		$shop_id = $this->gthis->parm_in('session', 'shop_id', 'N');

		$res['grpDtl'] = $delivery->getCustDeliGrpDtl($grp_id);
		
		require_once ROOT.'/include/class/product.class.php';
        $product = new product($this->DB, $this->gthis->lang, true);
		$res['productList'] = $product->getProductList($shop_id, null, ["page"=>1,"limit"=>9999], null);

		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->gthis->lang);
		$res['regionList'] = $delivery->getRegionList($shop_id);
		$res['timeslotList'] = $delivery->getTimeslotList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "custDeliList",
			"url" => "admin/setting/custDeliList",
			"page_title" => $this->gthis->RMBC->getReturnString('custDeliList')
		],[
			"code" => "custDeliDtl",
			"url" => "admin/setting/custDeliDtl?grp_id=" . ($grp_id ? $grp_id : "0"),
			"page_title" => $grp_id ? $res['regionDtl']['name_tc'] : $this->gthis->RMBC->getReturnString('addCustDeli')
		]);
		return $res;
	}

	function regionList(){
		if (intval($_SESSION['right']['deli']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->gthis->lang);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$result_array['regionList'] = $delivery->getRegionList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "regionList",
			"url" => "admin/setting/regionList",
			"page_title" => $this->gthis->RMBC->getReturnString('regionList')
		]);
		return $result_array;
	}

	function regionDtl(){
		if (intval($_SESSION['right']['deli']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->gthis->lang);

		$region_id = $this->gthis->parm_sql('request', 'region_id');

		$result_array['regionDtl'] = $delivery->getRegionDtl($region_id);
		
		array_push($this->gthis->breadcrumb, [
			"code" => "regionList",
			"url" => "admin/setting/regionList",
			"page_title" => $this->gthis->RMBC->getReturnString('regionList')
		],[
			"code" => "regionDtl",
			"url" => "admin/setting/regionDtl?region_id=" . ($region_id ? $region_id : "0"),
			"page_title" => $region_id ? $result_array['regionDtl']['name_tc'] : $this->gthis->RMBC->getReturnString('addTimeslot')
		]);
		return $result_array;
	}

	function timeslotList(){
		if (intval($_SESSION['right']['deli']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->gthis->lang);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$result_array['timeslotList'] = $delivery->getTimeslotList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "timeslotList",
			"url" => "admin/setting/timeslotList",
			"page_title" => $this->gthis->RMBC->getReturnString('timeslotList')
		]);
		return $result_array;
	}

	function timeslotDtl(){
		if (intval($_SESSION['right']['deli']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->gthis->lang);

		$slot_id = $this->gthis->parm_sql('request', 'slot_id');

		$result_array['timeslotDtl'] = $delivery->getTimeslotDtl($slot_id);
		
		array_push($this->gthis->breadcrumb, [
			"code" => "timeslotList",
			"url" => "admin/setting/timeslotList",
			"page_title" => $this->gthis->RMBC->getReturnString('timeslotList')
		],[
			"code" => "timeslotDtl",
			"url" => "admin/setting/timeslotDtl?slot_id=" . ($slot_id ? $slot_id : "0"),
			"page_title" => $slot_id ? $result_array['timeslotDtl']['adm_name'] : $this->gthis->RMBC->getReturnString('addTimeslot')
		]);
		return $result_array;
	}

	function holidayList(){
		if (intval($_SESSION['right']['deli']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->gthis->lang);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$result_array['holidayList'] = $delivery->getHolidayList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "holidayList",
			"url" => "admin/setting/holidayList",
			"page_title" => $this->gthis->RMBC->getReturnString('holidayList')
		]);
		return $result_array;
	}

	function holidayDtl(){
		if (intval($_SESSION['right']['deli']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/delivery.class.php';
		$delivery = new delivery($this->DB, $this->gthis->lang);

		$holiday_id = $this->gthis->parm_sql('request', 'holiday_id');

		$result_array['holidayDtl'] = $delivery->getHolidayDtl($holiday_id);
		
		array_push($this->gthis->breadcrumb, [
			"code" => "holidayList",
			"url" => "admin/setting/holidayList",
			"page_title" => $this->gthis->RMBC->getReturnString('holidayList')
		],[
			"code" => "holidayDtl",
			"url" => "admin/setting/holidayDtl?holiday_id=" . ($holiday_id ? $holiday_id : "0"),
			"page_title" => $holiday_id ? $result_array['holidayDtl']['adm_name'] : $this->gthis->RMBC->getReturnString('addHoliday')
		]);
		return $result_array;
	}

    // function greetingDtl(){
	// 	if (intval($_SESSION['right']['greeting']) < 30){
	// 		return array("right"=>"Failure");
	// 	}
	// 	require_once ROOT.'/include/class/greeting.class.php';
	// 	$greeting = new greeting($this->DB, $this->gthis->lang);

    //     $greeting_id = $this->gthis->parm_sql('request', 'greeting_id', 'Y');

	// 	$result_array['greetingDtl'] = $greeting->getGreetingDtl($greeting_id);
		
	// 	array_push($this->gthis->breadcrumb, [
	// 		"code" => "greetingList",
	// 		"url" => "admin/setting/greetingList",
	// 		"page_title" => $this->gthis->RMBC->getReturnString('greetingList')
	// 	],[
	// 		"code" => "greetingDtl",
	// 		"url" => "admin/setting/greetingDtl?greeting_id=" . ($greeting_id ? $greeting_id : "0"),
	// 		"page_title" => $greeting_id ? $result_array['greetingDtl']['name_tc'] : $this->gthis->RMBC->getReturnString('addGreeting')
	// 	]);
	// 	return $result_array;
    // }

	function greetingSubcateDtl(){
		if (intval($_SESSION['right']['greeting']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/greeting.class.php';
		$greeting = new greeting($this->DB, $this->gthis->lang);
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$cate_id = $this->gthis->parm_in('request', 'cate_id');
		$subcate_id = $this->gthis->parm_in('request', 'subcate_id');

		$res['subcateDtl'] = $greeting->getGreetingSubcateDtl($shop_id, $subcate_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "greetingCateList",
			"url" => "admin/setting/greetingCateList",
			"page_title" => $this->gthis->RMBC->getReturnString('greetingCateList')
		], [
			"code" => "greetingCateDtl",
			"url" => "admin/setting/greetingCateDtl?cate_id=" . ($cate_id ? $cate_id : "0"),
			"page_title" => $res['subcateDtl']['parent_name_tc']
		], [
			"code" => "greetingSubcateDtl",
			"url" => "admin/setting/greetingSubcateDtl?cate_id=" . ($cate_id ? $cate_id : "0"),
			"page_title" => $subcate_id ? $res['subcateDtl']['name_tc'] : $this->gthis->RMBC->getReturnString('addGreetingCate')
		]);
		return $res;
	}

	function greetingCateList(){
		if (intval($_SESSION['right']['greeting']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/greeting.class.php';
		$greeting = new greeting($this->DB, $this->gthis->lang);
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$result_array['greetingCateList'] = $greeting->getGreetingCateList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "greetingCateList",
			"url" => "admin/setting/greetingList",
			"page_title" => $this->gthis->RMBC->getReturnString('greetingCateList')
		]);
		return $result_array;
	}

	function greetingCateDtl(){
		if (intval($_SESSION['right']['greeting']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/greeting.class.php';
		$greeting = new greeting($this->DB, $this->gthis->lang);
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$cate_id = $this->gthis->parm_in('request', 'cate_id');

		$res['greetingCateDtl'] = $greeting->getGreetingCateDtl($shop_id, $cate_id);
		array_push($this->gthis->breadcrumb, [
			"code" => "greetingCateList",
			"url" => "admin/setting/greetingCateList",
			"page_title" => $this->gthis->RMBC->getReturnString('greetingCateList')
		], [
			"code" => "greetingCateDtl",
			"url" => "admin/setting/greetingCateDtl?cate_id=" . ($cate_id ? $cate_id : "0"),
			"page_title" => $cate_id ? $res['greetingCateDtl']['name_tc'] : $this->gthis->RMBC->getReturnString('addGreetingCate')
		]);
		return $res;
	}
	
}
?>