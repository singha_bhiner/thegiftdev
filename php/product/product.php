<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->DB = $g_DB;

		switch ($this->gthis->submode) {
			case 'ae':
				$this->gthis->assign("page_only" , "Y");
				$result_array = $this->ae();
				break;
			case 'productList':
				$result_array = $this->productList();
				break;
			case 'productDtl':
				$result_array = $this->productDtl();
				break;
			case 'comboProductList':
				$result_array = $this->comboProductList();
				break;
			default:
				break;
		}

		$this->gthis->assign('result_array',$result_array);
	}	

	function ae(){

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://gateway-japa.americanexpress.com/api/nvp/version/63");
		curl_setopt($ch, CURLOPT_POST, 1);


		$field_list = [
			"apiOperation=INITIATE_CHECKOUT",
			"apiPassword=94be89601306afad0a476dc4f2808d49",
			"apiUsername=merchant.TEST9811921469",
			"merchant=TEST9811921469",
			"interaction.operation=PURCHASE",
			"order.id=100001",
			"order.amount=100.00",
			"order.currency=HKD"
		];

		$postField = join("&", $field_list);
		// echo $postField;

		curl_setopt($ch, CURLOPT_POSTFIELDS, $postField);

		// In real life you should use something like:
		// curl_setopt($ch, CURLOPT_POSTFIELDS, 
		//          http_build_query(array('postvar1' => 'value1')));

		// Receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec($ch);
		
		$ae_list = [];
		$return_list = explode("&", $server_output);

		foreach ($return_list as $return){
			$tmp = explode("=", $return );

			if (str_contains($tmp[0], ".")){
				$t2 = explode(".", $tmp[0]);
				$ae_list[$t2[0]][$t2[1]] = $tmp[1];
			} else {
				$ae_list[$tmp[0]] = $tmp[1];
			}
		}

		curl_close ($ch);
		
		$res["ae"] = $ae_list;
		return $res;
	}

	function comboProductList(){
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->gthis->lang);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sortMode = $this->gthis->parm_sql('request', 'sortMode', 'N');
		if (!$sortMode)
			$sortMode = 1; // default sortmode

		switch ($sortMode){
			case '1':
				// 由定價低至高 for front sorting mode
				$sorting = [
					"sort" => $sort ? $sort : "IFNULL(`sp_price`, `price`)",
					"order" => $order ? $order : "ASC"
				];
				break;
			case '2':
				// 由定價高至低 for front sorting mode
				$sorting = [
					"sort" => $sort ? $sort : "IFNULL(`sp_price`, `price`)",
					"order" => $order ? $order : "DESC"
				];
				break;
			case '3':
				// 由新貨排序 for front sorting mode
				$sorting = [
					"sort" => $sort ? $sort : "create_datetime",
					"order" => $order ? $order : "ASC"
				];
				break;
		}

		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 5
		];

		$filter = [
			"combo_product" => "Y"
		];

		$productList = $productCls->getProductList($shop_id, $filter, $limiter, $sorting);

		foreach ($productList as $k => $product){
			$src_product_id_arr = explode(",", $product['src_product_id_arr']);
			foreach ($src_product_id_arr as $kk => $pid){
				$productList[$k]['productDtl']['srcProductDtl'][$kk] = $productCls->getProductDtl($pid, $shop_id);
			}
			// $productList[$k]['productDtl']  = $productCls->getProductDtl($product['product_id'], $shop_id);
		}


		$res['productList'] = $productList;


		array_push($this->gthis->breadcrumb, [
			"code" => "comboProductList",
			"url" => "product/comboProductList",
			"page_title" => $this->gthis->RMBC->getReturnString('comboProductList')
		]);
		return $res;

	}

    function productList(){
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->gthis->lang);

		require_once ROOT.'/include/class/cate.class.php';
		$cateCls = new cate($this->DB, $this->gthis->lang);

		$member = $this->gthis->parm_in('session', 'member');
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$page = $this->gthis->parm_sql('request', 'page', 'N');
		$limit = $this->gthis->parm_sql('request', 'limit', 'N');
		$sortMode = $this->gthis->parm_sql('request', 'sortMode', 'N');
		if (!$sortMode)
			$sortMode = 1; // default sortmode
		
		$res['cateList'] = $cateCls->getCateList($shop_id);
		$cateName = $this->gthis->thirdmode;
		$res['cateDtl'] = $cateCls->getCateDtl(null, $cateName);

		if ($res['cateDtl']){
			if ($res['cateDtl']['subcateList']){
				$filter = ["cate_id" => $res['cateDtl']['cate_id']];
			} else {
				$filter = ["subcate_id" => $res['cateDtl']['cate_id']];
			}
		} else {
			$filter = ["allCate" => 'Y'];
		}

		$filter['frontend_show'] == 'Y';

		if($member['product_id_arr']){
			$filter['product_id_arr'] = $member['product_id_arr'];
		}

		

		switch ($sortMode){
			case '1':
				// 由定價低至高 for front sorting mode
				$sorting = [
					"sort" => $sort ? $sort : "IFNULL(`sp_price`, `price`)",
					"order" => $order ? $order : "ASC"
				];
				break;
			case '2':
				// 由定價高至低 for front sorting mode
				$sorting = [
					"sort" => $sort ? $sort : "IFNULL(`sp_price`, `price`)",
					"order" => $order ? $order : "DESC"
				];
				break;
			case '3':
				// 由新貨排序 for front sorting mode
				$sorting = [
					"sort" => $sort ? $sort : "create_datetime",
					"order" => $order ? $order : "ASC"
				];
				break;
		}

		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 25
		];

		$res['productList'] = $productCls->getProductList($shop_id, $filter, $limiter, $sorting);
		
		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($productCls->getProductList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config']['sortMode'] = $sortMode; // front is using sortMode instead
		
		array_push($this->gthis->breadcrumb, [
			"code" => "productList",
			"url" => "product/productList",
			"page_title" => $this->gthis->RMBC->getReturnString('productList')
		]);
		return $res;
	}

	function productDtl(){
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->gthis->lang);

	
		$product_code = $this->gthis->thirdmode;
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$productDtl = $productCls->getProductDtlByCode($product_code, $shop_id);
		if (!$productDtl){
			header('Location: /');
		}
		$res['productDtl'] = $productDtl;
		
		$product_id = $res['productDtl']['product_id'];
		$res['productTplDtl'] = $productCls->getProductTplDtl($productDtl['product_tpl_id'], $shop_id);
		
		$cateArr = [];
		foreach ($productDtl['cate'] as $cate){
			foreach ($cate['subcate'] as $subcate){
				array_push($cateArr, $subcate['cate_id']);
			}
		}
		
		$filter = ["subcate_id" => implode(",", $cateArr)];
		$limiter = ["limit" => 4, "page" => 1];
		$sorting = ["sort" => "rand()"];

		// $res['relatedProductList'] = $product->getProductList($shop_id, $filter, $limiter, $sorting);

		$res['relatedProductList'] = $productCls->getRelatedProductList($shop_id, $product_id);
		
		array_push($this->gthis->breadcrumb, [
			"code" => "productList",
			"url" => "product/productList",
			"page_title" => $this->gthis->RMBC->getReturnString('productList')
		],[
			"code" => "productDtl",
			"url" => "product/productDtl?product_id=" . ($product_id ? $product_id : "0"),
			"page_title" => $product_id ? $res['productDtl']['product_name_tc'] : $this->gthis->RMBC->getReturnString('addProduct')
		]);
		return $res;
	}
}
?>