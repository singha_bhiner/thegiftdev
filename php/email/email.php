<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->DB = $g_DB;

        $this->gthis->assign("page_only", 'Y');
		switch ($this->gthis->submode) {
			case 'register':
				$result_array = $this->register();
				break;
			case 'resetPassword':
				$result_array = $this->resetPassword();
				break;
			default:
				break;
		}

		@$this->gthis->assign('result_array',$result_array);
	}

	function resetPassword(){
		$code = $this->gthis->parm_sql("request", "code", "Y");
		$res['code'] = $code;
		return $res;
	}

    function register(){
        $valid_code = $this->gthis->parm_sql("request", "valid_code", "Y");
        $res['valid_code'] = $valid_code; 
        return $res;
    }
	
}
?>