<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;
		require_once ROOT.'/include/class/user.class.php';
		$this->userClass = new user($this->DB, $this->lang, $this->isAdmin);

		require_once ROOT. '/include/apiDataBuilder.class.php';
		
		switch ($this->gthis->thirdmode) {
            case 'loginGo':
                $this->loginGo();
				break;
            case 'capchaImgReg':
                $this->capchaImgReg();
				break;
			case 'logout':
				$this->logout();
				break;
			case 'editAdmin':
				$this->editAdmin();
				break;
			case 'editAdminRole':
				$this->editAdminRole();
				break;
			case 'getAdminInfo':
				$this->getAdminInfo();
				break;
			default:
				break;
		}

		$result_array['lang'] = $this->lang;
		@$this->gthis->assign('result_array',$result_array);
	}

	function getAdminInfo(){
		$res['admin'] = $_SESSION['admin'];
		new apiDataBuilder(0, $res);
	}

	function editAdmin(){
		$admin_id = $this->gthis->parm_sql('request', 'admin_id', 'N');
		if (intval($_SESSION['right']['admin']) < 50 && $admin_id != $_SESSION['admin']['admin_id']){
			new apiDataBuilder(-62);
		}

		require_once ROOT.'/include/class/user.class.php';
		$admin = new user($this->DB, $this->lang, $this->isAdmin);

		$role_id = $this->DB->parseInt($this->gthis->parm_sql('request', 'role_id', 'N'));
		
		$username = $this->gthis->parm_sql('request', 'username', 'N');

		$data = [];
		$role_id? $data['role_id'] = $role_id: null;

		$username? $data['username'] = $username: null;
		if($admin_id == 0){
			$password = $this->gthis->parm_sql('request', 'password');
			if (!$admin->validatePasswordFormat($password)){
				new apiDataBuilder(-92, null, "password format invalid");
			}
			$password = md5($password);
			$password? $data['password'] = $password: null;
			$res = $admin->editAdmin(0, $data);
		} else {
			$res = $admin->editAdmin($admin_id, $data);
		}
		
		new apiDataBuilder(0, $res);
	}

	function editAdminRole(){
		if (intval($_SESSION['right']['admin']) < 50){
			new apiDataBuilder(-62);
		}
		require_once ROOT.'/include/class/user.class.php';
		$admin = new user($this->DB, $this->lang, $this->isAdmin);
		$adm_name = $this->gthis->parm_sql('request', 'adm_name', 'N');
		$role_id = $this->DB->parseInt($this->gthis->parm_sql('request', 'role_id', 'N'));

		$shopArr = ['gift', 'zuri', 'ltp'];		
		$roleArr = ['item','shop','warehouse','order','quickOrder','product','page','member','discount','admin'];
		$data = [];
		$adm_name ? $data['adm_name'] = $adm_name: null;

		foreach ($shopArr as $shop){
			$tmpArr = [];
			foreach($roleArr as $role){
				$tmpArr[$role] = $this->DB->parseInt($this->gthis->parm_sql('request', $shop."_".$role));
			}
			$data[$shop."_right"] = json_encode($tmpArr);
		}


		$data['product_id_arr'] = $this->DB->parseIntArr($this->gthis->parm_sql('request', 'product_id_arr'));

		$res = $admin->editAdminRole($role_id, $data);
		
		new apiDataBuilder(0, $res);
	}

	function logout(){
		unset($_SESSION['admin']);
		new apiDataBuilder(0);
	}

    function loginGo(){
		$forceLogin = $this->gthis->parm_sql('request','forceLogin');
		$appLogin  = $this->gthis->parm_sql('request', 'appLogin');

		if ($forceLogin == ""){
			$admin = $this->userClass->getAdminFromUsername('admin');
	
			$_SESSION['admin'] = $admin;
			$shop_id = $admin['default_shop_id'];
			$_SESSION['shop_id'] = $shop_id;
		
			switch ($shop_id){
				case 2:
					$right = $admin['zuri_right'];
					break;
				case 3:
					$right = $admin['ltp_right'];
					break;
				case 1:
				default:
					$right = $admin['gift_right'];
					break;
			}
	
			$_SESSION['right'] = json_decode($right, true);
	
			$result['code'] = '0';
			$result['msg'] = 'Done';
			$result['data'] = $this->gthis->dataBuilder(null,'success');
			//$result['to_url'] = $_REQUES
		} else {
			$username = $this->gthis->parm_sql('request','username','Y');
			$password = $this->gthis->parm_sql('request','password','Y');
			$captcha = $this->gthis->parm_sql('request','captcha','N');
			
			if(isset($_SESSION['fail_count'])){
				if($_SESSION['fail_count']>3){
					if($_SESSION['onetimecci'] == $captcha){
						// pass 
					} else {
						$result['code'] = '-2';
						$result['msg'] = 'Done';
						if($captcha){
							$result['data'] = $this->gthis->dataBuilder(array(),'invalidToken');
						} else {
							$result['data'] = $this->gthis->dataBuilder(array(),'tooManyFail');
						}
						$_SESSION['fail_count'] = $_SESSION['fail_count'] + 1;
						$this->gthis->json_return('result',$result,'Y');	
					}
				} 
			} else {
				$_SESSION['fail_count'] = 0;
			}
			
			if ($appLogin != 'Y'){
				$otlk = $this->gthis->parm_sql('request','otlk','Y');
				if($otlk != $_SESSION['otlk']){
					$result['code'] = '-1';
					$result['msg'] = 'Done';
					$result['data'] = $this->gthis->dataBuilder(null,'invalidToken') . " || $otlk || " . $_SESSION['otlk'];
					$_SESSION['fail_count'] = $_SESSION['fail_count'] + 1;
					$this->gthis->json_return('result',$result,'Y');
				}
			}
	
			$admin = $this->userClass->getAdminFromUsername($username);
	
			if(!$admin){
				$result['code'] = '-3';
				$result['msg'] = 'Done';
				$result['data'] = $this->gthis->dataBuilder(null,'invalidLogin');
				$_SESSION['fail_count'] = $_SESSION['fail_count'] + 1;
				$this->gthis->json_return('result',$result,'Y');
			}

			if($password != $admin['password']){
				$result['code'] = '-4';
				// $result['msg'] = 'Done';
				$result['msg'] = $admin['password'];
				$result['msg2'] = $password;
				$result['data'] = $this->gthis->dataBuilder(null,'invalidLogin');
				$_SESSION['fail_count'] = $_SESSION['fail_count'] + 1;
				$this->gthis->json_return('result',$result,'Y');
			}
	
			$_SESSION['admin'] = $admin;
			$shop_id = $admin['default_shop_id'];
			$_SESSION['shop_id'] = $shop_id;
		
			switch ($shop_id){
				case 2:
					$right = $admin['zuri_right'];
					break;
				case 3:
					$right = $admin['ltp_right'];
					break;
				case 1:
				default:
					$right = $admin['gift_right'];
					break;
			}
	
			$_SESSION['right'] = json_decode($right, true);
	
			if ($appLogin =='Y'){
				$accessToken = "ADMINID:" . $admin['admin_id'] . "||TIMESESSION:". (new DateTime())->getTimestamp();
				$accessToken = $this->gthis->dataEncrypt($accessToken);
				$res['accessToken'] = $accessToken;
				$res['admin'] = $admin;
				new apiDataBuilder(0, $res, "Done");
			}

			$result['code'] = '0';
			$result['msg'] = 'Done';
			$result['data'] = $this->gthis->dataBuilder(null,'success');
			//$result['to_url'] = $_REQUEST;
		}

		$this->gthis->json_return('result',$result,'Y');
    }

    function capchaImgReg(){
		if($_SESSION['onetimecci']){
			unset($_SESSION['onetimecci']);
		}
		$_SESSION['onetimecci'] = strtolower($this->gthis->generateRandomString(4));
		$im = imagecreate(50, 20);
		// White background and blue text
		if($_REQUEST['color']=='white'){
			$bg = imagecolorallocatealpha($im, 255, 255, 255, 0);
		} else {
			$bg = imagecolorallocatealpha($im, 247, 247, 247, 0);
		}
		$textcolor = imagecolorallocate($im, 26, 32, 44);
		// Write the string at the top left
		imagestring($im, 5, 10, 5, $_SESSION['onetimecci'] , $textcolor);
		// Output the image
		header('Content-type: image/png');
		imagepng($im);
		imagedestroy($im);
		exit;
	}
}
?>