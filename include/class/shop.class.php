<?php
/* 
    Objective: shop related 
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_shop
        tmap_item_shop
        
*/


require_once dirname(__FILE__). '/abstract.class.php';
class shop extends baseClass{

    function editShop($shop_id, $data){
        // edit shop
        // $shop_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($data['logo']){
            $data['logo'] = $this->DB->uploadImg($data['logo'], "logo", $shop_id);
        }
        if ($data['product_list_img_1_path']){
            $data['product_list_img_1_path'] = $this->DB->uploadImg($data['product_list_img_1_path'], "logo", $shop_id);
        }
        if ($data['product_list_img_2_path']){
            $data['product_list_img_2_path'] = $this->DB->uploadImg($data['product_list_img_2_path'], "logo", $shop_id);
        }
        if ($data['product_list_img_3_path']){
            $data['product_list_img_3_path'] = $this->DB->uploadImg($data['product_list_img_3_path'], "logo", $shop_id);
        }

        $this->DB->updateMap("tmap_new_member_voucher", "shop_id", $shop_id, "voucher_id", $data['voucher_id_arr']);
        unset($data['voucher_id_arr']);

        $sql = $this->DB->buildSql_update('tbl_shop', 'shop_id', $shop_id, $data);
        $this->DB->update($sql);

        return $this->getShopDtl($shop_id);
    }

    function getShopDtlByIdArr($shop_id_arr){
        // get shop detail by id array
        // $shop_id_arr: shop_id array

        $sql = "SELECT * FROM tbl_shop WHERE activate = 'Y' && shop_id in ($shop_id_arr)";
        return $this->DB->get_Sql($sql);
    }

    function getShopDtl($shop_id){
        // get the shop Detail
        // $shop_id: PK

        $sql = "SELECT s.*, address_".$this->lang." AS address
                FROM `tbl_shop` AS s
                WHERE s.activate = 'Y' && s.shop_id = '$shop_id'
                GROUP BY s.shop_id";
        $res = $this->DB->get_Sql($sql)[0];

        $sql = "SELECT GROUP_CONCAT(m.voucher_id) AS voucher_id_arr 
                FROM `tmap_new_member_voucher` AS m 
                WHERE m.activate ='Y' && m.shop_id = '$shop_id'
                GROUP BY shop_id;";
        $t = $this->DB->get_Sql($sql)[0];
        $res['voucher_id_arr'] = $t['voucher_id_arr'];

        return $res;
    }
}
?>