<?php
/* 
    Objective: logger
    Author: Sing
    Last major update: 03-11-2021

	Related table: 
        log_*
        tbl_sql_audit_log
        

*/

require_once dirname(__FILE__). '/abstract.class.php';
class audit extends baseClass{
    
    function init(){

        $this->mapping = [
            "general" => [
                "action" => [
                    "update" => '修改',
                    "add" => '新增',
                    "del" => '刪除'
                ]
            ],
            "tbl_item" => [
                "brand_id" => "品牌",
                "country_id" => "國家",
                "pattern" => "款式",
                "supplier_name_tc" => "中文名稱(供應商)",
                "supplier_name_en" => "英文名稱(供應商)",
                "alias" => "產品別稱",
                "inhouse_name_tc" => "內部提示中文名稱",
                "inhouse_name_en" => "內部提示英文名稱",
                "gift_name_tc" => "The Gift 中文名稱",
                "gift_name_en" => "The Gift 英文名稱",
                "zuri_name_tc" => "Little Zuri 中文名稱",
                "zuri_name_en" => "Little Zuri 英文名稱",
                "ltp_name_tc" => "LoTaiPo 中文名稱",
                "ltp_name_en" => "LoTaiPo 英文名稱",
                "supplier_stock_code" => "貨品編號(供應商)",
                "supplier_barcode" => "商品條碼",
                "unit" => "單位",
                "cost" => "批發價",
                "stockin_price" => "購入價",
                "suggested_price" => "建議零售價",
                "remarks" => "物品備註",
                "qty" => "存量",
                "alert_qty" => "庫存量提醒"
            ],
            "log_product_item_content" => [
                "name_tc" => "中文顯示名稱",
                "name_en" => "英文顯示名稱",
                "gift_name_tc" => "中文顯示名稱",
                "zuri_name_tc" => "中文顯示名稱",
                "ltp_name_tc" => "中文顯示名稱",
                "gift_name_en" => "英文顯示名稱",
                "zuri_name_en" => "英文顯示名稱",
                "ltp_name_en" => "英文顯示名稱",
                "show_in_web" => "顯示在網站",
                "sort" => "排序",
                "qty" => "數量",
            ],
            "tbl_order" => [
                "order_status" => "訂單狀態",
                "order_no" => "訂單編號",
                "sender_name" => "客戶姓名",
                "sender_email" => "客戶電郵",
                "sender_phone" => "客戶電話",
                "product_price" => "貨物總價格",
                "shipping_fee" => "運輸總額外收費",
                "timeslot_fee" => "時段總附加費",
                // "point_used" => "積分使用",
                // "coupon_discount" => "優惠碼折扣",
                // "coupon_code" => "優惠碼",
                // "coupon_code_id" => "coupon_code_id",
                // "voucher_discount" => "優惠卷",
                // "voucher_id" => "voucher_id",
                // "final_price" => "總價格",
                // "point_gain" => "積分賺取",
                "remarks_from_client" => "由客戶提供的備註",
                "remarks_internal" => "內部備註",
                "remarks_external" => "對客戶備註",
                "excel_path" => "Excel下單檔案",
                "order_datetime" => "下單日期",
                "deli_status" => "運單狀態",
                "deli_no" => "訂單編號",
                "recipient_name" => "收件人姓名",
                "recipient_phone" => "收件人電話",
                "recipient_company" => "收件人公司",
                "card_type" => "禮物卡類型",
                "card_head" => "禮物卡上款",
                "card_body" => "禮物卡內害",
                "card_sign" => "禮物卡下款",
                "card_bg_img_path" => "禮物卡背景",
                "card_img_path" => "禮物卡圖片",
                "deli_region_id" => "運送區域",
                "deli_address" => "運送地址",
                "deli_date" => "運送日子",
                "deli_timeslot_id" => "運送時段",
                "deli_before_photo_path" => "運送前相片",
                "deli_after_photo_path" => "運送後相片",
                "acknowledgement" => "確認",
                "product_price" => "貨物價格",
                "addon_price" => "附加物品價格",
                "shipping_fee" => "運輸額外收費",
                "timeslot_fee" => "時段附加費",
                "remarks_internal" => "內部備註",
                "remarks_external" => "對客戶備註",
                "deli_price" => "運單總價格",
            ]
        ];
	}

    function logProductItemGroup($product_id, $item_json){
        $sql = "SELECT * 
                FROM tbl_product_item_group
                WHERE activate = 'Y' AND product_id = $product_id";
        $orgData = $this->DB->get_Sql($sql);

        
        foreach($orgData as $org){
            $org_json = [];
            $new_json = [];
            $item = $item_json[0];

            if ($item) {
                if ($item['grp_id'] != $org['grp_id']){
                    $org_json['grp_id'] = $org['grp_id'];
                    $new_json['grp_id'] = $item['grp_id'];
    
                    $logData = [
                        "product_id" => $product_id,
                        "action" => "update",
                        "original_json" => json_encode($org_json, JSON_UNESCAPED_UNICODE),
                        "updated_json" => json_encode($new_json, JSON_UNESCAPED_UNICODE),
                        "updated_by_id" => $_SESSION['admin']['admin_id']
                    ];
            
                    $this->DB->insert_db("log_product_item_content", $logData);
                } else {
                    // remain unchange
                }
            } else {
                // item removed
                
                $org_json['grp_id'] = $org['grp_id'];
                $logData = [
                    "product_id" => $product_id,
                    "action" => "del",
                    "original_json" => json_encode($org_json, JSON_UNESCAPED_UNICODE),
                    "updated_json" => json_encode($new_json, JSON_UNESCAPED_UNICODE),
                    "updated_by_id" => $_SESSION['admin']['admin_id']
                ];
        
                $this->DB->insert_db("log_product_item_content", $logData);   

            }

            array_shift($item_json);
        }

        foreach($item_json as $item){
            $org_json = [];
            $new_json = [];

            
            $new_json['grp_id'] = $item['grp_id'];
            $logData = [
                "product_id" => $product_id,
                "action" => "add",
                "original_json" => json_encode($org_json, JSON_UNESCAPED_UNICODE),
                "updated_json" => json_encode($new_json, JSON_UNESCAPED_UNICODE),
                "updated_by_id" => $_SESSION['admin']['admin_id']
            ];
    
            $this->DB->insert_db("log_product_item_content", $logData);   
        }
    }

    function getProductItemContentLog($product_id){
        $sql = "SELECT l.*, a.username 
                FROM `log_product_item_content` AS l
                LEFT JOIN `tbl_admin` AS a ON a.admin_id = l.updated_by_id
                WHERE product_id = $product_id
                ORDER BY log_id DESC";
        $log_list = $this->DB->get_Sql($sql);

        foreach ($log_list as $k => $log){
            $log_list[$k]['action_display'] = $this->mapping['general']['action'][$log['action']];
            $org_list = json_decode($log['original_json'], true);
            $new_list = json_decode($log['updated_json'], true);
            $org_grp_id = $org_list['grp_id'];
            $new_grp_id = $new_list['grp_id'];

            if ($org_grp_id){
                $sql = "SELECT * 
                        FROM tbl_item_grp 
                        WHERE grp_id = '$org_grp_id'";
                $org = $this->DB->get_Sql($sql)[0];
                if (!$org['inhouse_name_tc']){
                    $sql = "SELECT i.inhouse_name_tc
                            FROM tbl_item_grp_content AS igc
                            LEFT JOIN tbl_item AS i ON i.item_id = igc.item_id
                            WHERE grp_id = '$org_grp_id'";
                    $org = $this->DB->get_Sql($sql)[0];
                }
            }

            if ($new_grp_id){
                $sql = "SELECT * 
                        FROM tbl_item_grp 
                        WHERE grp_id = '$new_grp_id'";
                $new = $this->DB->get_Sql($sql)[0];
                if (!$new['inhouse_name_tc']){
                    $sql = "SELECT i.inhouse_name_tc
                            FROM tbl_item_grp_content AS igc
                            LEFT JOIN tbl_item AS i ON i.item_id = igc.item_id
                            WHERE grp_id = '$new_grp_id'";
                    $new = $this->DB->get_Sql($sql)[0];
                }
            }
            


            $log_list[$k]["original"] = $org["inhouse_name_tc"];
            $log_list[$k]["updated"] = $new["inhouse_name_tc"];

            // $item_list = [];
            // foreach($org_list as $key => $org){
            //     array_push($item_list, [
            //         "key" => $key,
            //         "key_display" => "物品",
            //         "original" => $org,
            //         "updated" => $new_list[$key]
            //     ]);
            // }
            // $log_list[$k]["item_list"] = $item_list;


            unset($log_list[$k]["original_json"]);
            unset($log_list[$k]["updated_json"]);
        }

        return $log_list;
    }

    function insertLog($tbl, $act, $newData, $primaryKey = null){
        // print_r($newData);
        $list = $this->mapping[$tbl];

        if ($act == "update"){
            // get the original data
            $sql = "SELECT * FROM $tbl WHERE $primaryKey = '".$newData[$primaryKey]."' LIMIT 0,1";
            $orgData = $this->DB->get_Sql($sql)[0];
        }
        
        $original_json = [];
        $updated_json = [];

        foreach($list as $key => $cell){
            if ($orgData[$key] != $newData[$key]){
                // echo $key . " ~ " . $orgData[$key] . " ~ " . $newData->$key . " || " ;
                $original_json[$key] = $orgData[$key];
                $updated_json[$key] = $newData[$key];
            }
        }

        $logData = [
            $primaryKey => $newData[$primaryKey],
            "action" => $act,
            "original_json" => json_encode($original_json, JSON_UNESCAPED_UNICODE),
            "updated_json" => json_encode($updated_json, JSON_UNESCAPED_UNICODE),
            "updated_by_id" => $_SESSION['admin']['admin_id']
        ];

        // die("END");
        $this->DB->insert_db("log". substr($tbl, 3), $logData);
    }

    function getAuditList($tbl, $row_id, $len =null){
        // get the Audit log base on tbl_sql_audit_log
        // $tbl: the table which related
        // $row_id: the primary key of the $tbl
        // $len: number of row to get

        $sql = "SELECT l.*, a.username 
                FROM `tbl_sql_audit_log` AS l
                LEFT JOIN `tbl_admin` AS a ON a.admin_id = l.updated_by_id
                WHERE `table` = '$tbl' AND `row_id` in ('$row_id') AND `field` != ''
                ORDER BY log_id DESC ";
        if ($len)
            $sql .= " LIMIT 0, $len";
            
        $res = $this->DB->get_Sql($sql);

        foreach ($res as $idx => $row){
            if ($this->mapping[$tbl][$row['field']]){
                $res[$idx]['fieldDisplay'] = $this->mapping[$tbl][$row['field']];
                $res[$idx]['create_datetime'] = date('d-m-Y H:i', strtotime($row['create_datetime']));
            } else {
                // 沒有mapping 的 視為不用顯示
                unset($res[$idx]);
            }

            switch($row['field']){
                case 'deli_timeslot_id':
                    if ($row['original_value']){
                        $sql = "SELECT * 
                                FROM `tbl_deli_timeslot`
                                WHERE slot_id = '" . $row['original_value'] . "' 
                                LIMIT 1";
                        $t = $this->DB->get_Sql($sql)[0];
                        $res[$idx]['original_value'] = $t['adm_name'] . "(" .$t['start_time'] . " - " . $t['end_time']. ")";
                    }
                    if ($row['updated_value']){
                        $sql = "SELECT * 
                                FROM `tbl_deli_timeslot`
                                WHERE slot_id = '" . $row['updated_value'] . "' 
                                LIMIT 1";
                        $t = $this->DB->get_Sql($sql)[0];
                        $res[$idx]['updated_value'] = $t['adm_name'] . "(" .$t['start_time'] . " - " . $t['end_time']. ")";
                    }
                    break;
                case 'deli_region_id':
                    if ($row['original_value']){
                        $sql = "SELECT * 
                                FROM `tbl_deli_region`
                                WHERE region_id = '" . $row['original_value'] . "' 
                                LIMIT 1";
                        $t = $this->DB->get_Sql($sql)[0];
                        $res[$idx]['original_value'] = $t['name_tc'];
                    }
                    if ($row['updated_value']){
                        $sql = "SELECT * 
                                FROM `tbl_deli_region`
                                WHERE region_id = '" . $row['updated_value'] . "' 
                                LIMIT 1";
                        $t = $this->DB->get_Sql($sql)[0];
                        $res[$idx]['updated_value'] = $t['name_tc'];
                    }
                    break;
                
            }
        }

        return $res;
    }

    function getItemLog($item_id){
        $sql = "SELECT l.*, a.username
                FROM `log_item` AS l
                LEFT JOIN `tbl_admin` AS a ON a.admin_id = l.updated_by_id
                WHERE `item_id` = $item_id
                GROUP by l.log_id
                ORDER BY log_id DESC";
        $log_list = $this->DB->get_Sql($sql);

        foreach ($log_list as $k => $log){
            $log_list[$k]['action_display'] = $this->mapping['general']['action'][$log['action']];
            $org_list = json_decode($log['original_json'], true);
            $new_list = json_decode($log['updated_json'], true);

            $item_list = [];
            foreach($org_list as $key => $org){
                switch ($key){
                    case 'brand_id':
                        if ($org != 'NULL' && $org != 'null' && $org){
                            $sql = "SELECT `brand_name_tc` FROM `tbl_item_brand` WHERE `brand_id` = $org";
                            $org = $this->DB->get_Sql($sql)[0]; 
                            $org = $org['brand_name_tc'];
                        } else {
                            $org = "";
                        }

                        if ($new_list[$key] != 'NULL' && $new_list[$key] != 'null' && $new_list[$key]){
                            $sql = "SELECT `brand_name_tc` FROM `tbl_item_brand` WHERE `brand_id` = $new_list[$key]";
                            $new = $this->DB->get_Sql($sql)[0]; 
                            $new = $new['brand_name_tc'];
                        } else {
                            $new = "";
                        }
                        break;
                    case 'country_id':
                        if ($org != 'NULL' && $org != 'null' && $org){
                            $sql = "SELECT `country_name_tc` FROM `tbl_item_country` WHERE `country_id` = $org";
                            $org = $this->DB->get_Sql($sql)[0]; 
                            $org = $org['country_name_tc'];
                        } else {
                            $org = "";
                        }
                        
                        if ($new_list[$key] != 'NULL' && $new_list[$key] != 'null' && $new_list[$key]){
                            $sql = "SELECT `country_name_tc` FROM `tbl_item_country` WHERE `country_id` = $new_list[$key]";
                            $new = $this->DB->get_Sql($sql)[0]; 
                            $new = $new['country_name_tc'];
                        } else {
                            $new = "";
                        }
                        break;
                    default:
                        $new = $new_list[$key];
                        break;

                }
                array_push($item_list, [
                    "key" => $key,
                    "key_display" => $this->mapping['tbl_item'][$key],
                    "original" => $org,
                    "updated" => $new
                ]);

            }
            $log_list[$k]["item_list"] = $item_list;


            unset($log_list[$k]["original_json"]);
            unset($log_list[$k]["updated_json"]);
        }

        return $log_list;
    }

    function getOrderAuditList($order_id){
        $sql = "SELECT l.*, o.order_id, od.deli_id, odi.map_id, od.deli_no
        FROM `tbl_order` AS o 
        LEFT JOIN `tbl_order_deli` AS od ON o.order_id = od.order_id
        LEFT JOIN `tbl_order_deli_item` AS odi ON od.deli_id = odi.deli_id
        INNER JOIN `tbl_sql_audit_log` AS l ON (o.order_id = l.row_id AND l.`table` = 'tbl_order') OR (od.deli_id = l.row_id AND l.`table` = 'tbl_order_deli') OR (odi.map_id = l.row_id AND l.`table` = 'tbl_order_deli_item') 
        WHERE o.order_id = $order_id
        GROUP BY log_id
        ORDER BY log_id DESC";
                
        $res = $this->DB->get_Sql($sql);

        foreach ($res as $idx => $row){
            if ($this->mapping['tbl_order'][$row['field']]){
                $fieldDisplay = $this->mapping['tbl_order'][$row['field']];

                if ($row['table'] == 'tbl_order_deli' || $row['table'] == 'tbl_order_deli_item') {
                    $fieldDisplay .= "(" . $row['deli_no'] .")";
                }
                
                $res[$idx]['fieldDisplay'] = $fieldDisplay;
                $res[$idx]['create_datetime'] = date('d-m-Y H:i', strtotime($row['create_datetime']));
            } else {
                // 沒有mapping 的 視為不用顯示
                unset($res[$idx]);
            }

            switch($row['field']){
                case 'deli_timeslot_id':
                    if ($row['original_value']){
                        $sql = "SELECT * 
                                FROM `tbl_deli_timeslot`
                                WHERE slot_id = '" . $row['original_value'] . "' 
                                LIMIT 1";
                        $t = $this->DB->get_Sql($sql)[0];
                        $res[$idx]['original_value'] = $t['adm_name'] . "(" .$t['start_time'] . " - " . $t['end_time']. ")";
                    }
                    if ($row['updated_value']){
                        $sql = "SELECT * 
                                FROM `tbl_deli_timeslot`
                                WHERE slot_id = '" . $row['updated_value'] . "' 
                                LIMIT 1";
                        $t = $this->DB->get_Sql($sql)[0];
                        $res[$idx]['updated_value'] = $t['adm_name'] . "(" .$t['start_time'] . " - " . $t['end_time']. ")";
                    }
                    break;
                case 'deli_region_id':
                    if ($row['original_value']){
                        $sql = "SELECT * 
                                FROM `tbl_deli_region`
                                WHERE region_id = '" . $row['original_value'] . "' 
                                LIMIT 1";
                        $t = $this->DB->get_Sql($sql)[0];
                        $res[$idx]['original_value'] = $t['name_tc'];
                    }
                    if ($row['updated_value']){
                        $sql = "SELECT * 
                                FROM `tbl_deli_region`
                                WHERE region_id = '" . $row['updated_value'] . "' 
                                LIMIT 1";
                        $t = $this->DB->get_Sql($sql)[0];
                        $res[$idx]['updated_value'] = $t['name_tc'];
                    }
                    break;
                
            }
        }

        return $res;
    }

}
?>