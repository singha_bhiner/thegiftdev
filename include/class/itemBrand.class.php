<?php
/* 
    Objective: coupon code related 
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_item_brand


        
*/
require_once dirname(__FILE__). '/abstract.class.php';
class itemBrand extends baseClass{

    function editBrand($data){
        if ($data['brand_id'] == 0){
            $brand_id = $this->DB->insert_db('tbl_item_brand', $data);
        } else {
            $brand_id = $this->DB->update_Sql("tbl_item_brand", "brand_id", $data);
        }
        
        return $this->getBrandDtl($brand_id);
    }

    function getBrandList($filter = null, $limiter = null, $sorting = null){
        $sql = "SELECT * FROM `tbl_item_brand` WHERE `activate` ='Y'";

        if ($filter){
            foreach($filter as $k => $i){
                if ($i || $i == '0'){
                    switch ($k){
                        case 'brand_id':
                            $sql .= " AND (brand_id = '$i')";
                            break;
                        case 'skey':
                            $sql .= " AND (brand_name_en LIKE '%$i%' OR brand_name_tc LIKE '%$i%')";
                            break;
                        
                        default: 
                            break;
                    }                    
                }
            }
        }

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        return $res;
    }

    function getBrandDtl($brand_id){ 
        if ($brand_id)
            return $this->dispatchBrand($this->getBrandList(['brand_id' => $brand_id])[0]);
        else
            return null;
    }

    function dispatchBrand($brand){
        return $brand;
    }
}
?>