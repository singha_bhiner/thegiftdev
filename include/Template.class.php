<?php
require_once LIB.'./templates_engine/Smarty.class.php';

class Template extends Smarty{
	
	var $redirSec;
	function __construct(){	
		$this->force_compile=true;
		$this->template_dir = 'templates';
		$this->compile_dir = 'templates_cache';

	}
}

?>