<?php
require_once dirname(__FILE__). '/abstract.class.php';
class client extends baseClass{

    function databaseConstruct(){
        $sql = "
            DROP TABLE IF EXISTS `tbl_client`;
            CREATE TABLE `tbl_client` (
                `client_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `shop_id` INT(11) DEFAULT NULL,
                `image_path` VARCHAR(64) DEFAULT NULL,
                `activate` VARCHAR(1) DEFAULT 'Y',
                `updated_by_id` INT(11) DEFAULT NULL,
                `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `create_datetime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ";
        $this->DB->update($sql);
    }

//////////////////////////////////////////// CLIENT START ////////////////////////////////////////////
    function getClientList($filter = null, $sorting = null, $limiter = null){
		$fromShort = "c";
		$selectSql = "SELECT $fromShort.* ";
		$fromSql = "FROM `tbl_client` AS $fromShort";
		$whereSql = " WHERE $fromShort.`activate` = 'Y'";
		$groupSql = " GROUP BY $fromShort.client_id";
		if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
			$whereSql = " WHERE 1=1";

		if ($sorting)
			$sortSql = " ORDER BY $sorting";
        else 
            $sortSql = " ORDER BY sort";
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
                if ($i){
                    switch ($k){
                        case 'client_id':
                            $whereSql .= " AND ($fromShort.`client_id` = '$i')";
                            break;
                        case 'shop_id':
                            $whereSql .= " AND ($fromShort.`shop_id` = '$i')";
                            break;
                    } 
                }
			}
		}
		
		$sql = $selectSql . $fromSql . $whereSql . $groupSql . $sortSql  . $limitSql;
		$res = $this->DB->get_Sql($sql);

		foreach($res as $k => $i){
			$res[$k] = $this->dispatchClient($i);
		}

		return $res;
	}

    function getClient($client_id){
        if ($client_id)
            return $this->getClientList(["client_id" => $client_id], null, $this->DB->getUnique)[0];
        else 
            return null;
    }

    function dispatchClient($client){
        return $client;
    }

    function editClient($data){
        if (isset($data['client_id']) && $data['client_id'] != 0) {
			$id = $this->DB->update_Sql("tbl_client", "client_id", $data);
		} else {
			$id = $this->DB->insert_db("tbl_client", $data);
		} 
        return $id;
    }

    
    function sortClient($shop_id, $client_arr){
        
        $this->DB->sortMap("tbl_client", "shop_id", $shop_id, "client_id", $client_arr);

        return true;
    }

//////////////////////////////////////////// CLIENT END   ////////////////////////////////////////////

}
?>