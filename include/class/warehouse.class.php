<?php
/* 
    Objective: Warehouse related 
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_location
        tmap_location_sku
        log_location_sku

        
*/


require_once dirname(__FILE__). '/abstract.class.php';
class warehouse extends baseClass{

    function editStocktakeSku($data){
        $map_id = $data['map_id'];
        unset($data['map_id']);
        $sql = $this->DB->buildSql_update("tbl_stocktake_sku", "map_id", $map_id, $data);
        $this->DB->update($sql);

        return true;
    }

    function editStocktake($stocktake_id,$data){
        $sql = $this->DB->buildSql_update("tbl_stocktake", "stocktake_id", $stocktake_id, $data);
        $this->DB->update($sql);

        return true;
    }

    function getStocktakeDtl($stocktake_id){
        $sql = "SELECT * 
                FROM tbl_stocktake 
                WHERE stocktake_id = $stocktake_id";
        $stocktake = $this->DB->get_Sql($sql)[0];

        $sql = "SELECT ss.location_id, l.*
                FROM `tbl_stocktake_sku` AS ss
                LEFT JOIN `tbl_location` AS l ON l.location_id = ss.location_id 
                WHERE stocktake_id = '$stocktake_id'
                GROUP BY ss.location_id";
        $locationList = $this->DB->get_Sql($sql);

        foreach ($locationList as $k => $location){
            $sql = "SELECT ls.*, s.sku_val, i.name_tc, a.username
                    FROM `tbl_stocktake_sku` AS ls
                    LEFT JOIN `tbl_sku` AS s ON s.sku_id = ls.sku_id
                    LEFT JOIN `tbl_item` AS i ON i.item_id = s.item_id
                    LEFT JOIN `tbl_admin` as a ON a.admin_id = ls.updated_by_id
                    WHERE ls.location_id = '".$location['location_id']."' AND ls.stocktake_id = '$stocktake_id'";

            $locationList[$k]['skuList'] = $this->DB->get_Sql($sql);
        }
        $stocktake['locationList'] = $locationList;

        return $stocktake;
    }

    function startStocktake(){
        $stocktake_id = $this->DB->insert_db("tbl_stocktake", null);
        $locationList = $this->getLocationList();

        foreach ($locationList as $k => $location){
            $containingList = $this->getLocationContainingList($location['location_id']);

            foreach ($containingList as $sku){
                if ($sku['qty'] != 0){
                    $data = [
                        "location_id" => $location['location_id'],
                        "sku_id" => $sku['sku_id'],
                        "expected_qty" => $sku['qty'],
                        "stocktake_id" => $stocktake_id
                    ];
    
                    $this->DB->insert_db('tbl_stocktake_sku', $data);
                }
            }
        }

        return $stocktake_id;
    }

    function getStocktakeList($filter = null, $limiter = null, $sorting = null){
        $sql = "SELECT * 
                FROM `tbl_stocktake` AS s";

        foreach($filter AS $k => $i){
            if ($i){
                switch($k){ 
                }
            }
        }

        $sql .= " GROUP BY s.stocktake_id";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        return $this->dispatchStocktake($res);
    }

    function dispatchStocktake($stocktakeList){
        foreach($stocktakeList as $k => $stocktake){
            switch ($stocktake['status']){
                case '10_incomplete':
                    $stocktakeList[$k]['status_lbl'] = "未完成";
                    break;
                case '20_completed':
                    $stocktakeList[$k]['status_lbl'] = "完成";
                    break;
            }
            
        }

        return $stocktakeList;
    }

    function getBarcodeList($sku_id){
        $sql = "SELECT * 
                FROM `tmap_sku_barcode`
                WHERE sku_id = '$sku_id'";
        return $this->DB->get_Sql($sql);
    }

    function packProduct($map_id, $item_id, $qty){
        $this->DB->adjustFloatVal("tbl_order_deli_item", "map_id", $map_id, "packed_qty", $qty);
        $this->DB->adjustFloatVal("tbl_item", "item_id", $item_id, "qty", $qty * -1);
    }

    // function packProduct($deliItem, $location_id, $item_id, $qty){
    //     $sql = "SELECT * 
    //             FROM `tmap_location_item`
    //             WHERE `item_id` = '$item_id' AND `location_id` = '$location_id'";
    //     $map = $this->DB->get_Sql($sql)[0];

    //     if (!$map){
    //         new apiDataBuilder(-34, ["location_id" => $location_id, "item_id" => $item_id]);
    //     } 

    //     $this->DB->adjustFloatVal("tmap_location_item", "map_id", $map['map_id'], "qty", $qty * -1);
    //     $this->DB->adjustFloatVal("tbl_order_deli_item", "map_id", $deliItem['map_id'], "packed_qty", $qty);
    //     $this->DB->adjustFloatVal("tbl_item", "item_id", $deliItem['item_id'], "qty", $qty * -1);

    //     $this->DB->insert_db("log_location_item", [
    //         "location_id" => $location_id, 
    //         "tar_location_id" => $deliItem['deli_id'],
    //         "item_id" => $item_id, 
    //         "qty" => $qty,
    //         "action" => "pack",
    //         "updated_by_id" => $_SESSION['admin']['admin_id']
    //     ]);

    //     return true;
    // }

    function moveItem($location_id, $tar_location_id, $item_id, $qty){

        // unload
        $sql = "SELECT * 
                FROM `tmap_location_item`
                WHERE `item_id` = '$item_id' AND `location_id` = '$location_id'";
        $map = $this->DB->get_Sql($sql)[0];

        if (!$map){
            new apiDataBuilder(-34, ["location_id" => $location_id, "item_id" => $item_id]);
        } 

        $this->DB->adjustFloatVal("tmap_location_item", "map_id", $map['map_id'], "qty", $qty * -1);

        // load
        $sql = "SELECT * 
                FROM `tmap_location_item`
                WHERE `item_id` = '$item_id' AND `location_id` = '$tar_location_id'";
        $map = $this->DB->get_Sql($sql)[0];

        if (!$map){
            $map_id = $this->DB->insert_db("tmap_location_item", [
                "location_id" => $tar_location_id, 
                "item_id" => $item_id
            ]);
        } else {
            $map_id = $map['map_id'];
        }

        $this->DB->adjustFloatVal("tmap_location_item", "map_id", $map_id, "qty", $qty);

        $this->DB->insert_db("log_location_item", [
            "location_id" => $location_id, 
            "tar_location_id" => $tar_location_id, 
            "item_id" => $item_id, 
            "qty" => $qty,
            "action" => "move",
            "updated_by_id" => $_SESSION['admin']['admin_id']
        ]);
        
        return $this->getLocationDtl($tar_location_id);
    }

    function unloadItem($location_id, $item_id, $qty){
        $sql = "SELECT * 
                FROM `tmap_location_item`
                WHERE `item_id` = '$item_id' AND `location_id` = '$location_id'";
        $map = $this->DB->get_Sql($sql)[0];

        if (!$map){
            new apiDataBuilder(-34, ["location_id" => $location_id, "sku_id" => $item_id]);
        } 

        
        require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

        $item = $itemCls->getItemDtl($item_id);

        if ($item['various_item_id']){
            $this->DB->adjustFloatVal("tbl_item", "item_id", $item['various_item_id'], 'qty', $qty * -1); 
        }

        // stock out in tbl_item
        $this->DB->adjustFloatVal("tbl_item", "item_id", $item_id, 'qty', $qty * -1); 
        // $this->DB->adjustFloatVal("tbl_item", "item_id", $item_id, 'ava_qty', $qty * -1); 

        $this->DB->adjustFloatVal("tmap_location_item", "map_id", $map['map_id'], "qty", $qty * -1);

        $this->DB->insert_db("log_location_item", [
            "tar_location_id" => $location_id, 
            "item_id" => $item_id, 
            "qty" => $qty,
            "action" => "unload",
            "updated_by_id" => $_SESSION['admin']['admin_id']
        ]);

        return $this->getLocationDtl($location_id);
    }

    function loadItem($location_id, $item_id, $qty){
        $sql = "SELECT * 
                FROM `tmap_location_item`
                WHERE `item_id` = '$item_id' AND `location_id` = '$location_id'";
        $map = $this->DB->get_Sql($sql)[0];

        if (!$map){
            $map_id = $this->DB->insert_db("tmap_location_item", [
                "location_id" => $location_id, 
                "item_id" => $item_id
            ]);
        } else {
            $map_id = $map['map_id'];
        }


        // // stock in in tbl_sku
        // $this->DB->adjustFloatVal("tbl_sku", "sku_id", $sku_id, 'qty', $qty); 
        // $this->DB->adjustFloatVal("tbl_sku", "sku_id", $sku_id, 'ava_qty', $qty); 

        // adjust parent item
        $sql = "SELECT * 
                FROM `tbl_item`
                WHERE item_id = (
                    SELECT various_item_id
                    FROM `tbl_item` 
                    WHERE item_id = $item_id
                )";
        $item = $this->DB->get_Sql($sql)[0];

        if ($item){
            $this->DB->adjustFloatVal("tbl_item", "item_id", $item['item_id'], 'qty', $qty); 
            $this->DB->adjustFloatVal("tbl_item", "item_id", $item['item_id'], 'ava_qty', $qty); 
        }
        
        // stock in in tbl_item
        $this->DB->adjustFloatVal("tbl_item", "item_id", $item_id, 'qty', $qty); 
        $this->DB->adjustFloatVal("tbl_item", "item_id", $item_id, 'ava_qty', $qty); 

        $this->DB->adjustFloatVal("tmap_location_item", "map_id", $map_id, 'qty', $qty);

        $this->DB->insert_db("log_location_item", [
            "tar_location_id" => $location_id, 
            "item_id" => $item_id, 
            "qty" => $qty,
            "action" => "load",
            "updated_by_id" => $_SESSION['admin']['admin_id']
        ]);

        return $this->getLocationDtl($location_id);
    }

    function addLocationSku($location_id, $sku_id, $qty){
        $sql = "SELECT * 
                FROM `tmap_location_sku` AS m
                WHERE `sku_id` = '$sku_id' AND `location_id` = '$location_id'";
        $map_id = $this->DB->get_Sql($sql)[0];

        if (!$map_id){
            $map_id = $this->DB->insert("tmap_location_sku", [
                "location_id" => $location_id,
                "sku_id" => $sku_id,
                "qty" => $qty
            ]);
        } else {
            $this->DB->adjustFloatVal("tmap_location_sku", "map_id", $map_id, $qty, $qty);
        }

        return $this->getLocationDtl($location_id);
    }

    function getLocationContainingList($location_id){
        $sql = "SELECT ls.*, i.inhouse_name_tc
                FROM `tmap_location_item` AS ls
                LEFT JOIN `tbl_item` AS i ON i.item_id = ls.item_id
                WHERE ls.location_id = '$location_id' AND ls.qty != 0;";
        $containingList = $this->DB->get_Sql($sql);

        return $containingList;
    }

    function editLocation($location_id, $data){
        // edit location
        // $location_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($location_id == 0)
            $location_id = $this->DB->insert_db('tbl_location', null);

        $sql = $this->DB->buildSql_update("tbl_location", "location_id", $location_id, $data);
        $this->DB->update($sql);

        return $this->getLocationDtl($location_id);
    }

    function getLocationList($filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $selectSql = "SELECT l.* ";
        $fromSql = "FROM `tbl_location` AS l
                    LEFT JOIN `tmap_location_item` AS m ON l.location_id = m.location_id AND m.activate ='Y'
                    LEFT JOIN `tbl_item` AS i ON i.item_id = m.item_id ";
        $whereSql = "WHERE l.`activate` = 'Y' ";


        foreach($filter AS $k => $i){
            if ($i){
                switch($k){ 
                    case 'skey':
                        $whereSql .= " AND (adm_name LIKE '%$i%' OR remarks LIKE '%$i%' 
                                            OR i.`inhouse_name_tc` LIKE '%$i%' OR i.`inhouse_name_en` LIKE '%$i%'
                                            OR i.`gift_name_tc` LIKE '%$i%' OR i.`gift_name_en` LIKE '%$i%'
                                            OR i.`zuri_name_tc` LIKE '%$i%' OR i.`zuri_name_en` LIKE '%$i%'
                                            OR i.`ltp_name_tc` LIKE '%$i%' OR i.`ltp_name_en` LIKE '%$i%'
                                            )";
                        break;
                    case 'item_id':
                        $selectSql .= ", m.qty AS item_qty ";
                        $whereSql .= " AND (m.item_id = '$i')";
                        break;
                }
            }
        }

        $sql = $selectSql . $fromSql . $whereSql;
        
        $sql .= " GROUP BY l.location_id";

        if ($sorting){
            $sql .= " ORDER BY l.location_id = 1 DESC," . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        foreach($res as $k => $location){
            $res[$k] = $this->dispatchLocation($location);
        }

        return $res;
    }

    function dispatchLocation($location){
        $location_id = $location['location_id'];
        $sql = "SELECT m.*, i.inhouse_name_tc, i.item_id, i.item_img
                FROM `tmap_location_item` as m 
                LEFT JOIN `tbl_item` as i ON i.item_id = m.item_id
                WHERE m.location_id = $location_id";
        
        $location['item_list'] = $this->DB->get_Sql($sql);
        return $location;
    }

    function getLocationDtl($location_id){
        // get the location Detail
        // $location_id: PK

        $sql = "SELECT * 
                FROM `tbl_location`
                WHERE location_id = $location_id";
        $res = $this->DB->get_Sql($sql)[0];

        if ($res){
            $sql = "SELECT m.*, i.".$this->getPrefix($this->shop_id)."_name_tc AS display_name
                    FROM `tmap_location_item` as m 
                    LEFT JOIN `tbl_item` as i ON i.item_id = m.item_id
                    WHERE m.location_id = $location_id AND m.qty != 0;";
            $res['itemList'] = $this->DB->get_Sql($sql);
        }

        return $res;
    }
}
?>