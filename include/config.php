<?php
/* BLT Smarty Web System */
/*
	PHP Version Required : php74+
	SQL Supported : mySQL / mariaDB
	Smarty Version : 3.1.35
	Suggested Server : Apache / nginx / Tomcat
*/

define('ROOT', substr(dirname(__FILE__), 0, -8));
define('LIB', ROOT.'/include/');
define('HOST',$_SERVER['HTTP_HOST']);
define('LANG',$_SERVER["HTTP_ACCEPT_LANGUAGE"]);
define('LOGO','/img/logo.png');

if(isset($_SERVER['HTTP_REFERER'])){
    define('REFERER',$_SERVER['HTTP_REFERER']);
} else {
	define('REFERER','');
}

ini_set('default_socket_timeout', 20);
define('BUSINESS_NAME', '');		// Business Display Name e.g. Email 
define('BUSINESS_EMAIL', '');		// Business Display Name e.g. Email 

$server_address = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '';
$server_name = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '';
define('MASTER_DOMAIN', 'devthegift.wwdrfive.com');		// Domain
define('SESSION_DOMAIN', 'wwdrfive.com');		// Domain
define('MASTER_HOME', 'https://dev.'.MASTER_DOMAIN);		// Domain
define('MASTER_HOME_DEV', 'https://dev.'.MASTER_DOMAIN);		// Dev Domain
define('CURRENT_VERSION', '1');		// database user name
define('SECURITY_PASSPHARSE', 'vuzXuS-zijqe8-ff');		// Secureity passphaRSE 16 bit please
define('PERSONAL_DATA_ENCRYPT', false);		// Should encrypt personal data
define('GOOGLE_RECAPTCHA_PKEY', '');		// Should encrypt personal data
define('GOOGLE_RECAPTCHA_SKEY', '');		// Should encrypt personal data

define('STRIPE_DEV_KEY', 'sk_test_51H2pXGIVgm7DECLYQ6Y0bKMKW02joAhe1nFXw6V9GpsuTOl6ddFNKWRCfLFFGRIFpN0cWHNPb2WqjjlP1J2jUHWc00KfjXNQsD');       // PRIVATE API KEY for stripe service
define('STRIPE_PROD_KEY', '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');       // PRIVATE API KEY for stripe service
define('STRIPE_DEV_PKEY', 'pk_test_51H2pXGIVgm7DECLY7PaNt99DCALZCRDV5Flm3aJjPrBGmCrxyvX5EWwzaCNfZ4RKieQ6adtEtwkHZKEq7h1tMlBk003NS0u4KK');       // PRIVATE API KEY for stripe service
define('STRIPE_PROD_PKEY', 'pk_live_51H2pXGIVgm7DECLY7woqUAw8Vc4Q6EqRthRzgMHuQ2xSVekppW7EJWbXWG4T7I6se7BODCTsnwoHVCqyG5BVTfg600MykjLXkE');       // PRIVATE API KEY for stripe service

define('RESERVE_TIME', '35');
define('DAILY_CUTOFF_TIME', '18:00');
define('DEFAULT_LANG', 'tc');
define('LANG_SUPPORT', ['zh', 'tc', 'en']);
define('oneSignalPublicKey', 'faf4b523-0fa2-4a2f-8e4f-bdcfd801af1e');
define('oneSignalPrivateKey', 'NGFhODJhOWMtYzFkNy00NWFkLTk1ZmYtZWVjNzIzOWE3Yjhh');

define('AE_API_PASSWORD', '94be89601306afad0a476dc4f2808d49');
define('AE_API_USERNAME', 'merchant.TEST9811921469');
define('AE_MERCHANT', 'TEST9811921469');

switch ($server_address) {
	default: // development server
		define('MASTER_HOME_FULL', MASTER_HOME_DEV);		// Copy Domian name
		define('MASTER_DOMAIN', MASTER_HOME_DEV);		// Copy Domian name
		define('ENV', "DEV");		// enviroment
		define('O_DBUSER', '');		// database user name
		define('O_DB', '');			// database name
		define('O_DBPWD', '');   // dba password
		define('O_HOST', '');       // db host should be either ip
		define('GOOGLE_API_KEY', '');       // API KEY for google service
		define('PAYPAL_AC', 'sb-jac5x7957585@business.example.com');       // paypal payment to account
		// define('PAYPAL_AC', 'sb-vdopx3314175@business.example.com');       // paypal payment to account
		define('PAYPAL_ID', 'ATtQmV76w4NhTR_PNdmxi1Jqq8ZPtdTQ9S5HY-rv3J0ZJsXFRoq7y57WPA70d7hyKfzrfgqEBTb8jBwf'); // paypal account id
		define('SMTP_USERNAME', ''); // SMTP username @ email.class.php
		define('SMTP_PASSWORD', ''); // SMTP password @ email.class.php
		define('SMTP_HOST', ''); // SMTP hosting @ email.class.php
		define('SMTP_SECURE', ''); // SMTP secure @ email.class.php
		define('SMTP_PORT', ''); // SMTP port @ email.class.php


		break;
}
?>
