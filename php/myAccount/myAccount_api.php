<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->DB = $g_DB;

		require_once ROOT. '/include/apiDataBuilder.class.php';
		
		switch ($this->gthis->thirdmode) {
			case 'resendVerificationEmail':
				$this->resendVerificationEmail();
				break;
			case 'resetPasswordRequest':
				$this->resetPasswordRequest();
				break;
			case 'resetPassword':
				$this->resetPassword();
				break;
			case 'login':
				$this->login();
				break;
			case 'addCart':
				$this->addCart();
				break;
			case 'register':
				$this->register();
				break;
			case 'updateProfile':
				$this->updateProfile();
				break;
			case 'addFav':
				$this->addFav();
				break;
			case 'myCart':
				$this->myCart();
				break;
			case 'removeCartDeliItem':
				$this->removeCartDeliItem();
				break;
			case 'redeemGiftCard':
				$this->redeemGiftCard();
				break;
			case 'redeemGift':
				$this->redeemGift();
				break;
			default:
				new apiDataBuilder(-84);
				break;
		}

		$result_array['lang'] = $this->lang;
		@$this->gthis->assign('result_array',$result_array);
	}

	function redeemGift(){
		$member = $_SESSION['member'];
		$member_id = $member['member_id'];

		if (!$member)
			new apiDataBuilder(-40);
			
		$gift_id = $this->gthis->parm_sql('request', 'gift_id', 'Y');

		require_once ROOT.'/include/class/gift.class.php';
		$giftCls = new gift($this->DB, $this->gthis->lang);
		
		$giftCls->executeGift($gift_id, $member_id);
		
		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang);

		new apiDataBuilder(0, $memberCls->getMemberDtl($member_id));
	
	}

	function redeemGiftCard(){
		$member = $_SESSION['member'];
		$member_id = $member['member_id'];

		if (!$member)
			new apiDataBuilder(-40);
			
		$redeem_code = $this->gthis->parm_sql('request', 'redeem_code', 'Y');

		require_once ROOT.'/include/class/giftCard.class.php';
		$giftCardCls = new giftCard($this->DB, $this->gthis->lang);

		$key = $giftCardCls->checkGiftCard($redeem_code);
		
		if ($key){
			$giftCardCls->executeGiftCard($key, $member_id);
			
			require_once ROOT.'/include/class/member.class.php';
			$memberCls = new member($this->DB, $this->gthis->lang);

			new apiDataBuilder(0, $memberCls->getMemberDtl($member_id));
		} else {
			// code is not valid
			new apiDataBuilder(14);
		}
	}

	function resendVerificationEmail(){
		$member = $_SESSION['member'];
		$member_id = $member['member_id'];
		$email = $this->gthis->parm_sql('request', 'email', 'Y');

		$data = [
			"email" => $email
		];

		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang);
		if ($email != $member['email']){
			if (!$memberCls->emailValidation($member['shop_id'], $email))
				new apiDataBuilder(-95);
		}
		
		$memberCls->editMember($member_id, $data);

		$res = $memberCls->generateValidCode($member);

		$email_content = file_get_contents($this->gthis->baseURL.'/email/register?valid_code=' . $res['valid_code']);
		
		require_once ROOT.'/include/class/email.class.php';
		$emailCls = new email($this->DB, $this->gthis->lang);
		$emailCls->sendEmailGo($res['email'], "The Gift 禮譽帳戶認證", $email_content, $res['member_id'], "tbl_member");

		new apiDataBuilder(0);
	}

	function resetPassword(){
		$password = $this->gthis->parm_sql('request', 'password', 'Y');
		$member_id = $this->gthis->parm_sql('request', 'member_id', 'Y');

		$data = [
			"password" => $this->DB->parsePassword($password)
		];

		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang);
		$memberCls->editMember($member_id, $data);

		new apiDataBuilder(0);
	}

	function resetPasswordRequest(){
		$shop_id = $this->gthis->parm_sql('request', 'shop_id', 'Y');
		$email = $this->gthis->parm_sql('request', 'email', 'Y');

		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang);

		$member = $memberCls->resetPasswordRequest($shop_id, $email);

		require_once ROOT.'/include/class/email.class.php';
		$emailCls = new email($this->DB, $this->gthis->lang);

		$codeJson = [
			"member_id" => $member['member_id'],
			"pw_valid_code" => $member['pw_valid_code']
		];
		$email_content = file_get_contents($this->gthis->baseURL.'/email/resetPassword?code=' . base64_encode(json_encode($codeJson)));
		$emailCls->sendEmailGo($member['email'], "The Gift 禮譽帳戶密碼重設", $email_content, $member['member_id'], "tbl_member");

		new apiDataBuilder(0);
	}

	function addFav(){
		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang);
		
		$member_id = $_SESSION['member']['member_id'];
		$product_id = $this->gthis->parm_sql("request", "product_id");

		$data = [
			"member_id" => $member_id,
			"product_id" => $product_id
		];

		$res = $memberCls->editFav(0, $data);

		new apiDataBuilder(0, $res);
	}

	function updateProfile(){
		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang);
		$data = [];
		$member_id = $_SESSION['member']['member_id'];

		$fieldArr = ["name", "mobile", "email", "password", "language"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
				case 'sales_key':
					$field = "sales_admin_id";
					$sales_admin_id = $userCls->getSales($sales_key);
					break;
				case 'mobile':
					$mobile = $this->DB->parseInt($mobile);
					break;
				case 'password':
					if ($password)
						$password = $this->DB->parsePassword($password);
					break;
				case 'email':
					if ($email){
						$email = $this->DB->parseEmail($email);
						// check if the email exist
						if (!$memberCls->emailValidation($member['shop_id'], $email))
							new apiDataBuilder(-95);
					}
					break;

			}
			$$field? $data[$field] = $$field: null;
		}

		$res = $memberCls->editMember($member_id, $data);

		new apiDataBuilder(0, $res);
	}

	function register(){
		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang);
		require_once ROOT.'/include/class/user.class.php';
		$userCls = new user($this->DB, $this->gthis->lang);

		$data = [
			"method" => "email",
			"membership_id" => 6 // 白 會員
		];
		$shop_id = $this->gthis->parm_sql('request', 'shop_id', 'Y');

		$fieldArr = ["name", "mobile", "email", "password", "language", "sales_key", "shop_id", "member_id", "company_name"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
				case 'sales_key':
					$field = "sales_admin_id";
					$sales_admin_id = $userCls->getSales($sales_key);
					break;
				case 'mobile':
					$mobile = $this->DB->parseInt($mobile);
					break;
				case 'password': 
					$password = $this->DB->parsePassword($password);
					break;
				case 'email':
					$email = $this->DB->parseEmail($email);
					// check if the email exist
					if (!$memberCls->emailValidation($shop_id, $email))
						new apiDataBuilder(-95);
					break;

			}
			$$field? $data[$field] = $$field: null;
		}

		$res = $memberCls->register($data);
		$res = $memberCls->generateValidCode($res);

		require_once ROOT.'/include/class/email.class.php';
		$emailCls = new email($this->DB, $this->gthis->lang);
		$email_content = file_get_contents($this->gthis->baseURL.'/email/register?valid_code=' . $res['valid_code']);
		$emailCls->sendEmailGo($res['email'], "The Gift 禮譽帳戶認證", $email_content, $res['member_id'], "tbl_member");

		new apiDataBuilder(0, $res);
	}

	function removeCartDeliItem(){
		$member_id = $this->gthis->parm_sql('request', 'member_id');
		if (!$member_id){
			$member_id = session_id();
		}

		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);

		$shop_id = $this->gthis->parm_sql('request', 'shop_id');
		$deli_id = $this->gthis->parm_sql('request', 'deli_id');
		$data = [
			"activate" => 'N',
			"shop_id" => $shop_id
		];

		$orderCls->editdeli($deli_id, $data);
		$order = $orderCls->getOrderDtlByMemberid($shop_id, $member_id);
		$orderCls->updateOrderDeliPrice($order, $member_id); // re-calculate the price
		$order = $orderCls->getOrderDtlByMemberid($shop_id, $member_id); // get the updated order
		
		new apiDataBuilder(0, $order);
	}

	function myCart(){
		$member = $this->gthis->parm_in('session', 'member', 'Y');
		$shop_id = $this->gthis->parm_in('session', 'shop_id', 'Y');

		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);

		$cartDtl = $orderCls->getCart($shop_id, $member['member_id']);
		
		$res['cartDtl'] = $cartDtl;

		new apiDataBuilder(0, $res);
	}

	function addCart(){
		require_once ROOT.'/include/class/order.class.php';
		$orderCls = new order($this->DB, $this->gthis->lang);

		$deli_id = 0;

		$fieldArr = ["shop_id"];
		$addon_list = json_decode(str_replace("\\", "", $this->gthis->parm_sql('request', "addon_list")), true);  
		$deli_arr = json_decode(str_replace("\\", "", $this->gthis->parm_sql('request', "deli_arr")), true);  
		$qty = $this->gthis->parm_sql('request', "qty");
		if (!$qty)
			$qty = 1;

		$data = $deli_arr[0];
		$data["deli_status"] = "incart";
		$data['addon_list'] = $addon_list;

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
			}
			$$field? $data[$field] = $$field: null;
		}


		$member = $this->gthis->parm_in('session', 'member');
		if (!$member){
			$session_id = session_id();
			// member checkout
			
			require_once ROOT.'/include/class/member.class.php';
			$memberCls = new member($this->DB, $this->gthis->lang);

			$member = $memberCls->createGuestMember($session_id, $shop_id);
			$_SESSION['member'] = $member;
		}
		
		$member_id = $member['member_id'];

		$orderDtl = $orderCls->getCart($shop_id, $member_id);

		$data['order_id'] = $orderDtl['order_id'];
		
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->gthis->lang);
		
		$product = $productCls->getProductDtl($shop_id, $data['product_id']);
		$product_price = $product['act_price'];
		$itemList = $deli_arr[0]['itemList'];
		// check if it is additional price for the selected item (upgrade product)
		foreach ($product['item_group_list'] as $k => $item_group){
			$sel = $itemList[$k]['item_id'];
			if ($item_group['item_list'] && $sel){
				foreach($item_group['item_list'] as $item){
					if ($item['item_id'] == $sel){
						$product_price += $item['additional_price'];
					}
				}
			}
		}
		// add additional product price in the product price
		$addon_price = 0;
		if ($addon_list){
			foreach ($addon_list as $addon){
				$product = $productCls->getProductDtl($shop_id, $addon['product_id']);
				// echo $product['act_price'] . " || " . $addon['qty'];
				$addon_price += $product['act_price'] * $addon['qty'];
			}
		}
		
		$data['product_price'] = $product_price;
		$data['addon_price'] = $addon_price;
		$data['deli_price'] = $product_price + $addon_price;
		
		for ($i = 0; $i < $qty; $i++){
			$res = $orderCls->editdeli($deli_id, $data);
		}
		
		new apiDataBuilder(0, $res);
	}

    function login(){
        $email = $this->gthis->parm_sql('request', 'email', 'Y');
        $password = $this->gthis->parm_sql('request', 'password', 'Y');
        $shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		require_once ROOT.'/include/class/member.class.php';
		$memberCls = new member($this->DB, $this->gthis->lang, 'Y');
		
        $memberCls->loginMember($email, $password, $shop_id);
    }
}
?>