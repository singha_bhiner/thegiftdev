//mobile checker

function emailValidation(inputText){
    if (inputText.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/))
        return true;
    else 
        return false;
}

function infoPopup(title, content, type = null){
    contentHtml = `
    <div aria-live="assertive" class="fixed inset-0 flex items-end px-4 py-6 pointer-events-none sm:p-6 sm:items-start z-50">
        <div class="w-full flex flex-col items-center space-y-4 sm:items-end">
            <div class="max-w-sm w-full bg-white shadow-lg rounded-lg pointer-events-auto ring-1 ring-black ring-opacity-5 overflow-hidden">
                <div class="p-4">
                    <div class="flex items-start">
                        <div class="flex-shrink-0">
                            <svg class="h-6 w-6 text-green-400" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                        </div>
                        <div class="ml-3 w-0 flex-1 pt-0.5">
                            <p class="text-sm font-medium text-gray-900">
                                `+ title +`
                            </p>
                            <p class="mt-1 text-sm text-gray-500">
                                `+ content +`
                            </p>
                        </div>
                        <div class="ml-4 flex-shrink-0 flex">
                            <button class="bg-white rounded-md inline-flex text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                <span class="sr-only">Close</span>
                                <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>`;

    $div = $("<div/>").html(contentHtml).addClass("popup_modal");
    $div.find("button").on("click", function(){
        $(this).parents(".popup_modal").remove();
    });

    setInterval(function(){
        $div.remove();
    }, 2000);
    $("body").append($div);
}

function _bodyScrollLock(param) {
    var $body = $('body')
    if (param == false) {
        $('html').css({
            'overflow-y': ''
        })
        $body.css({
                transform: '',
                position: '',
                'overflow-y': ''
            })
            //and scroll window
        $(window).scrollTop($body.data().cachedScrollYPosition)
    } else {
        var scrollY = $(window).scrollTop()
        $('html').css({
            'overflow-y': 'hidden'
        })
        $body.css({
            transform: 'translate3d(0,-' + scrollY + 'px, 0)',
            position: 'fixed',
            'overflow-y': 'hidden'
        }).data().cachedScrollYPosition = scrollY
    }
}

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

window.mobileCheck = function() {
    let check = false;
    (function(a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};

//lazy load
$(function() {
    $('.lazy').Lazy({
        scrollDirection: 'vertical',
        effect: "fadeIn"
    });
});


$(document).ready(function() {
    $(document).off('click', 'a');
    $(document).on("click", 'a', function(e) {
        if ($(this).attr('href') == '#') {
            e.preventDefault();
        }
    });

    window.db = new PouchDB('default_banner');
    window.memberId = '';

    db.get('member').catch(function(err) {
        if (err.name === 'not_found') {
            // 沒有找到member
            window.memberdb = {
                "_id": "member",
                "autoLogin": "N",
                "session": "N",
            };
            db.put(window.memberdb);
        } else { // hm, some other error
            debug(err);
            throw err;
        }
    }).then(function(configDoc) {
        // 有找到member
        if (window.memberId) {
            // do nth for already login
            window.memberdb = configDoc;
        } else {
            window.memberdb = configDoc;
            if (window.memberdb.autoLogin == 'Y') {
                var url = '/api/member/login_token?login_token=' + window.memberdb['session'];
                $.bajax({
                    url: url,
                    debug: 'N',
                    method: "GET",
                    callback: function(data) {
                        var url = '/refreshHeaderMenu?' + "currentUrl=" + encodeURIComponent(window.location.href);
                        $.bajax({
                            url: url,
                            debug: 'N',
                            pureString: 'Y',
                            callback: function(data) {
                                //
                                $('header').replaceWith(data);
                            },
                        });
                    },
                });
            }
        }
    }).catch(function(err) {
        // handle any errors
        debug(err);
    });

    $(document).off('click', '.logoutGo');
    $(document).on("click", '.logoutGo', function(e) {
        var url = '/api/member/logout';
        $.bajax({
            url: url,
            debug: 'N',
            callback: function(data) { // save to pouchdb
                db.get('member').then(function(doc) {
                    window.memberdb['_rev'] = doc['_rev'];
                    window.memberdb['autoLogin'] = 'N';
                    window.memberdb['session'] = 'N';
                    return db.put(window.memberdb);
                }).then(function(result) {
                    refreshHeader();
                    pageHandler('/');
                }).catch(function(err) {
                    debug('err');
                    debug(err);
                    refreshHeader();
                    pageHandler('/');
                });
            },
        });
    });

    // faq passage modal shower helper
    $(document).off('click', '.modalHelpCenter');
    $(document).on("click", '.modalHelpCenter', function(e) {
        var passageId = $(this).attr('data-id');
        var url = '/modalHelpCenter/' + passageId;

        $vkmodal({
            extraClass: "_modalHelpCenter",
            fullScreen: false,
            tapBackgroundToClose: true,
            ajaxPath: url,
            //content: $('#country_picker_holder').html(),
            mounted: function(elem) {
                // var element = $(
                //     '[name=country_real]:last');
                //element.focus();
                //element.trigger('keyup');
                elem.find('.modal-close-vk').click(function() {
                    console.log(elem.data())
                    killVkmodal(elem.data().timestamp)
                })
            }
        });
        /*window.modalHelpCenter = $.bmodal({
            type: 'modalClean',
            title: window.jstxt.jstxt0011+"...",
            elementId: 'modalHelpCenter',
            ajaxPath: url,
            closeOnBackgroundClick: true,
            ajaxDelayLoadTime: 900,
            callback: function(data) {
                //debug('yo');
            },
        });*/
    });

    // dropdown menu on button bar
    $(document).off('click', '.dropdown-trigger');
    $(document).on("click", ".dropdown-trigger", function(e) {
        // lok changed
        //$(this).closest('.dropdown').find('.dropdown-menu').toggleClass('opacity-0')
        //$(this).closest('.dropdown').find('.dropdown-menu').toggleClass('pointer-events-none')
        //$(this).closest('.dropdown').find('.dropdown-menu').toggleClass('scale-95')

        if ($(this).hasClass('open')) {
            $(this).closest('.dropdown').find('.dropdown-menu').addClass('hidden');
            $(this).closest('.dropdown').find('i').removeClass('ri-arrow-up-s-line');
            $(this).closest('.dropdown').find('i').addClass('ri-arrow-down-s-line');
            $(this).removeClass('open');
        } else {
            $(this).closest('.dropdown').find('.dropdown-menu').removeClass('hidden');
            $(this).closest('.dropdown').find('i').removeClass('ri-arrow-down-s-line');
            $(this).closest('.dropdown').find('i').addClass('ri-arrow-up-s-line');
            $(this).addClass('open');
            $(document).off('click', '*');
            $(document).on("click", '*', function(e) {
                if ($(e.target).closest(".dropdown-menu").length === 0) {
                    $('.dropdown-menu').addClass('hidden');
                    $('.dropdown-menu').closest('.dropdown').find('.dropdown-trigger').removeClass('open');
                    $('.dropdown-menu').closest('.dropdown').find('i').removeClass('ri-arrow-up-s-line');
                    $('.dropdown-menu').closest('.dropdown').find('i').addClass('ri-arrow-down-s-line');
                    $(document).off('click', '*');
                    $(document).off('click', '.dropdown-menu  *');
                }
            });
            if ($(this).closest('.dropdown').attr('data-close-child') == 'Y') {
                $(document).off('click', '.dropdown-menu  *');
                $(document).on("click", '.dropdown-menu  *', function(e) {
                    $('.dropdown-menu').addClass('hidden');
                    $('.dropdown-menu').closest('.dropdown').find('.dropdown-trigger').removeClass('open');
                    $('.dropdown-menu').closest('.dropdown').find('i').removeClass('ri-arrow-up-s-line');
                    $('.dropdown-menu').closest('.dropdown').find('i').addClass('ri-arrow-down-s-line');
                    $(document).off('click', '.dropdown-menu  *');
                    $(document).off('click', '*');
                });

            }
        }

    });

    // spa 模式下點擊自動關閉
    $(document).off('click', '.mobile-phref');
    $(document).on("click", ".mobile-phref", function(e) {
        if (window.pwaOn == true) {
            $('.mobile-menu-close:first').trigger('click');
        }
    });

    $(document).off('click', '.goBack');
    $(document).on("click", ".goBack", function(e) {
        window.history.back();
    });


    $(document).off('click', '#mobile-menu-trigger');
    $(document).on("click", '#mobile-menu-trigger', function(e) {
        $('#mobile-menu').toggle();
        $('html , body').css('overflow', 'hidden');
    });

    $(document).off('click', '.mobile-menu-close');
    $(document).on("click", '.mobile-menu-close', function(e) {
        $('#mobile-menu').toggle();
        $('html , body').css('overflow', 'auto');
    });

    $(document).off("click", ".set-lang").on("click", ".set-lang", function(){
        pathname = location.pathname;
        search = location.search; 
        langCode = $(this).data("lang");

        // remove language code
        if (pathname.substring(0,4) == "/en/" || pathname.substring(0,4) == '/tc/'){
            pathname = pathname.substring(3);
        } 

        location.href = `/${langCode}${pathname}${search}`;
    });
})

$.urlParam = function(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    }
    return decodeURI(results[1]) || 0;
}

function refreshCurrentPage() {
    if (window.pwaOn == true) {
        pageHandler(window.location.href, 'Y', '', null, null, 'N', 'N');
    } else{
        window.location.reload();
    }
    
}

function unknowErrorShown(errorCode) {
    window.wrongProcess = $.bmodal({
        type: 'alert',
        title: window.jstxt.jstxt0007,
        elementId: 'wrongProcess',
        content: window.jstxt.jstxt0002 + "(" + window.jstxt.jstxt0004 + ":" + errorCode + ")",
        trueBtnName: window.jstxt.jstxt0005,
        callback: function(data) {
            //debug('yo');
        },
    });
}

function errorShown(title, content, tTrueBtnName = window.jstxt.jstxt0005) {
    window.wrongProcess = $.bmodal({
        type: 'alert',
        title: title,
        elementId: 'wrongProcess',
        content: content,
        trueBtnName: tTrueBtnName,
        callback: function(data) {
            //debug('yo');
        },
    });
}

function debug(msg) {
    try {
        if (window.env == 'DEV') {
            console.log(msg);
        }
    } catch (e) {

    }

}

function validateFormInput(formElement) {
    var pass = true;
    $.each(formElement.find('.required'), function(key, value) {
        if ($(this).is("select")) {
            if (!$(this).children('option:selected').val() || $(this).children('option:selected').val() == 'NA') {
                $(this).addClass('bg-red-100');
                pass = false;
            }
        } else {
            if (!$(this).val()) {
                $(this).addClass('bg-red-100');
                pass = false;
            }
        }
    });
    return pass;
}

function fullScreenLoader(onOff, title = "Loading", content = "Loading") {
    if (onOff == 'Y') {
        /*window.modalLoader = $.bmodal({
            type: 'modalloader',
            title: title,
            elementId: 'modalLoader',
            content: content,
        });*/
        vkfullScreenLoader('show')
    } else {
        /*window.modalLoader = $.bmodal({
            close: 'Y',
            elementId: 'modalLoader',
        });*/
        vkfullScreenLoader('close')
    }
}

function encodeQueryData(data) {
    const ret = [];
    for (let d in data)
        ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
    return ret.join('&');
}

function refreshHeader() {
    pageHandler('/header_refresh', 'Y', '', 'header', null, 'Y');
}

function goBack() {
    window.history.back();
}

function jqueryBindEvent(event,element,functionCallback){
    $(document).off(event, element);
    $(document).on(event ,element,functionCallback);
}

Object.size = function(obj) {
    var size = 0,
        key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function globalErrorReturn(data){
    vkAlertDefault({
        title: "錯誤",
        content: data.code + ": " +data.msg,
        confirmButtonHtml: "確認"
    });
}

async function globalLoader(dom = null){
    if (dom){
        killVkmodal(dom.timestamp)
    } else {
        return  await vkmodal({
            modalMode : 'loading',
        })
    }
}

function thousand_separator(x, zeroPadding = true) {
    x = x.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(x))
        x = x.replace(pattern, "$1,$2");

    if (zeroPadding)
        x += ".00";
    return x;
}
