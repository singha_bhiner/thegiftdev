<?php
class SQLDBi {
	public $dbname		= "";		# mysql database name (Schema)
	public $dbuser		= "";		# username
	public $dbpw		= '';		# password
	public $Errno		= 0;					# error nunber report
	public $Error		= "";					# error message report
	public $NoNeedLog	= true;				    # control sqllog
	public $tracingError	= true;				# tracing all of the sql process
	public $tracingName	= null;					# tracing all of the sql process by object
	public $sqlCache	= false;				# cache sql query result to file
	public $Cachelog	= false;				# caching log file
	public $CacheLifeTime	= 900;				# cache files lifetime
	private $dbhost		= "";			# mysql host
	private $pconnect	= false;				# Use the pconnect
	public $Link_ID	= 0;						# connection id
	private $Query_ID	= 0;					# query id
	private $Row		= 0;					# List the # line of rows
	private $charset	= "utf8mb4";			# default character encoding
	private $dbcharset	= "utf8mb4_unicode_ci";	# default sql character collection set
	private $cacheArray	= array();				# sql query result cache
	private $selectables = array();
	private $table;
	private $whereClause;
	private $limit;
	private $order;
	private $querynum;
	public function __construct($odbname,$odbuser,$odbpw,$odbhost) {
		// constructor assign
		if($odbname){
			$this->dbname = $odbname;
		}
		if($odbuser){
			$this->dbuser = $odbuser;
		}
		if($odbpw){
			$this->dbpw = $odbpw;
		}
		if($odbhost){
			$this->dbhost = $odbhost;
		}

		if($this->Link_ID==0) {
			if($this->pconnect) {
				if(!$this->Link_ID = @mysqli_pconnect($this->dbhost, $this->dbuser, $this->dbpw)) {
					$this->halt('Can not connect to mysql server pconnect');
				};
			} else {
				if(!$this->Link_ID = @mysqli_connect($this->dbhost, $this->dbuser, $this->dbpw)) {
					$this->halt('Can not connect to mysql server 1');
				};
			};
			if($this->dbcharset) {
				$this->Link_ID->set_charset($this->charset);
				mysqli_query($this->Link_ID,"SET NAMES ".$this->dbcharset);
				mysqli_query($this->Link_ID,"SET SESSION collation_connection=\'utf8_general_ci\'");
				mysqli_query($this->Link_ID,"SET character_set_connection=".$this->dbcharset.", character_set_results=".$this->dbcharset.", character_set_client=binary");
			};

			if($this->version() > '5.0.1') {
				mysqli_query($this->Link_ID,"SET sql_mode=''");
			};
			
			if($this->dbname) {
				mysqli_select_db($this->Link_ID,$this->dbname);
			};
			
		};
	}

	# Get one row by id, retrun array.
	public function get_Id($table,$uid,$id)
	{
		if(empty($id)) return false;
		$sql="SELECT * FROM `$table` where $uid='$id' LIMIT 1";
		$this->query($sql);
		$result = $this->fetch_array();
		$this->free_result();
		return $result;
	}


	public function all_Sql($sql) 
	{
		if(empty($sql)) return false;
		$this->query($sql);
		$result = $this->fetch_array();
		$this->free_result();
		return $result;
	}

	public function go_Sql($sql) 
	{
		if(empty($sql)) return false;
		$this->query($sql);
		$result = $this->fetch_array();
		$this->free_result();
		return $result[0];
	}

	# Get single data(1 row, 1 column) by SQL conditions.
	public function get_One($sql) 
	{
		if(empty($sql)) return false;
		if(!preg_match('/^LIMIT/i', $sql)) $sql .= " LIMIT 1";
		$this->query($sql);
		$result = $this->fetch_row();
		$this->free_result();
		return $result[0];
	}
	
	# Get one more data by SQL conditions.
	# type = 0, n row limit by LIMIT conditions.
	# type = 1, 1 row.
	# type = 2, only execute SQL conditions, manual free_result.
	public function get_Sql($sql,$type=0) 
	{
		if(empty($sql)) return false;
		if ($type==1) {
			$toLimit = " LIMIT 1";
			if(preg_match("/^insert/i", $sql)) $toLimit = "";
			if(!preg_match('/^LIMIT/i', $sql)) $sql = $sql.$toLimit;
			if($this->sqlCache==true && preg_match("/^select/i", $sql)) {
				$result = $this->cacheSQL($sql, array(), false, $type);
			} else {
				
				$this->query($sql);
				$result = $this->fetch_array();
			};
		} elseif ($type==0) {
			$result = array();
			if($this->sqlCache==true && preg_match("/^select/i", $sql)) {
				$result = $this->cacheSQL($sql, array(), false, $type);
			} else {
				$this->query($sql);
				while($line = $this->fetch_array()) {
					$result[] = $line;
				};
			};
		} elseif ($type==2) {
			$result = $this->query($sql);
		};
		if(($this->sqlCache) && ($type==0 || $type==1)) $this->free_result();
		return $result;
	}

	# Query loops, using when query-type=0 or 2
	public function next_record($type=0) {
		if($this->Query_ID==0) return false;
		if($type==0) {
			$this->Record = $this->fetch_array();
		} elseif($type==1) {
			$this->Record = $this->fetch_row();
		};
		$this->Row += 1;
		$this->Errno = mysqli_errno();
		$this->Error = mysqli_error();
		$stat = is_array($this->Record);
		if(!$stat)
		{
			$this->free_result();
		};
		return $this->Record;
	}

	# Insert data to SQL by array.
	public function insert_db($table,$inputData)
	{
		$insertSQL = "INSERT INTO `$table` ( ";
		$_fields = array();
		$_values = array();
		foreach($inputData as $fields => $values) {
			$_fields[] = "`".$fields."`";

			if ($values == 'NOW()' || $values == 'CURRENT_TIMESTAMP()' || $values == 'NULL'){
				$_values[] = $values;
			} else{
				// if the values escaped already 
				if (strpos($values, "\'") === false)
					$_values[] = "'".$this->sql_addslashes($values)."'";
				else
					$_values[] = "'".$values."'";
			}
		};

		$insertSQL .= implode("," , $_fields)." ) VALUES ( ".implode("," , $_values)." )";
		//echo $insertSQL;
		# Debug: below to check sql commond
		//die($insertSQL);
		$this->query($insertSQL);
		if (!$this->Query_ID) {
			return 0;
		} else {
			return $this->insert_id();
		};
		$this->free_result();
	}

	# Update SQL by custom conditions.(like PRIMARY KEY or INDEX)
	public function update_db($table,$uid,$id,$str,$increment_use=false)
	{
		$updateSQL = "UPDATE `$table` SET ";
		$_fields = array();
		if (!$increment_use)
			$quote="'";
		else
			$quote='';
		foreach($str as $fields => $values) {
		   $_fields[] = "`".$fields."`=$quote".$this->sql_addslashes($values)."$quote";
		};
		$updateSQL .= implode("," , $_fields). " WHERE `$uid` = '$id' LIMIT 1";
		//echo $updateSQL.'<br/>';
		$this->query($updateSQL);
		return $this->affected_rows();
	}
	
	# Update SQL by custom conditions.(like PRIMARY KEY or INDEX)
	public function update($sql)
	{
		$this->query($sql);
	}
	
	public function excute($sql)
	{
		$this->query($sql);
	}

	# Delete one data by id conditions.
	public function del_Id($table,$uid,$id)
	{
		if(empty($id)) return false;
		$sql="DELETE FROM `$table` where $uid='$id' LIMIT 1";
		$this->query($sql);
		return $this->affected_rows();
		$this->free_result();
	}

	# Delete one more data by custom conditions.
	public function del_Sql($table,$delstr)
	{
		if($this->Link_ID==0) return false;
		if(empty($delstr)) return false;
		$sql="DELETE FROM `$table` where ".$this->sql_addslashes($delstr);
		$this->query($sql);
		return $this->affected_rows();
		$this->free_result();
	}

	# sql add slashes, html escape, tags escape
	public function sql_addslashes($theValue)
	{
		return addslashes($theValue);
	}
	public function sql_html($str)
	{
		return htmlspecialchars($str);
	}
	public function sql_strip($str)
	{
		return strip($str);
	}

	# hash the form, refuse the _POST from other site.
	public function post() 
	{
		$servername = $HTTP_SERVER_privateS['SERVER_NAME'];
		$sub_from = $HTTP_SERVER_privateS["HTTP_REFERER"];
		$sub_len = mb_strlen($servername);
		$checkfrom = mb_substr($sub_from,7,$sub_len);
		if($checkfrom != $servername)
		{
			header ("content='text/html; charset=utf-8'");
			echo '<script language="javascript" type="text/javascript">alert("unknow source");</script>';
			echo '<script language="javascript" type="text/javascript">javascript:history.back();</script>';
			exit;
		};
	}
	public function free_result() {
		if(isset($this->Query_ID)){
			$var = $this->Query_ID;
			$var instanceof MySQLi;
			if(is_object($var) && get_class($var) == 'mysqli_result'){
				@mysqli_free_result($this->Query_ID);
			} else {
				$this->Query_ID = 0;		
			}
		}  else {;
			$this->Query_ID = 0;
		}
	}
	public function close() {
		if($this->Link_ID!=0) @mysqli_close($this->Link_ID);
		$this->Link_ID = 0;
	}
	public function select_db() {
		return @mysqli_select_db($this->dbname, $this->Link_ID);
	}
	public function fetch_array($result_type = MYSQLI_ASSOC) {
		return mysqli_fetch_array($this->Query_ID, $result_type);
	}
	public function affected_rows() {
		return @mysqli_affected_rows();
	}
	public function error() {
		return intval(($this->Link_ID) ? @mysqli_error($this->Link_ID) : @mysqli_error());
	}
	public function errno() {
		return intval(($this->Link_ID) ? @mysqli_errno($this->Link_ID) : @mysqli_errno());
	}
	public function result() {
		return @mysqli_result($this->Query_ID, $this->Row);
	}
	public function num_rows() {
		return @mysqli_num_rows($this->Query_ID);
	}
	public function fetch_row() {
		return @mysqli_fetch_row($this->Query_ID);
	}
	public function num_fields() {
		return @mysqli_num_fields($this->Query_ID);
	}
	public function insert_id() {
		return @mysqli_insert_id($this->Link_ID);
	}
	public function fetch_fields() {
		return @mysqli_fetch_field($this->Query_ID);
	}
	public function version() {
		return @mysqli_get_server_info($this->Link_ID);
	}

	# Query function. Auto log the UPDATE, INSERT, SELECT.
	private function query($sql, $type = '') {
        $to="";
		if(preg_match("/^update/i", $sql)) $to = "update";
		if(preg_match("/^insert/i", $sql)) $to = "insert";
		if(preg_match("/^delete/i", $sql)) $to = "delete";
		if($to!="") $this->SQLlog($sql, $to);

		if(!$this->Link_ID) {
			if($this->tracingError==true) $this->SQLlog("Connection Error;".$sql, "mysql");
			$this->halt('mysql Connection Error', $sql);			
		};

		$func = $type == 'UNBUFFERED' && @function_exists('mysqli_unbuffered_query') ?
			'mysqli_unbuffered_query' : 'mysqli_query';
		$_tracingName = ($this->tracingName==null || empty($this->tracingName)) ? "mysqli" : $this->tracingName;
		if(!($this->Query_ID = $func($this->Link_ID,$sql)) && $type != 'SILENT') {
			if((isset($_REQUEST['dev'])?$_REQUEST['dev']:'') != 'devuserpassphase'){
				if($this->tracingError==true) $this->SQLlog("Query Error;".$sql, $_tracingName);
				$this->halt('mysql Query Error', $sql);	
			} else {
				die("Opps!! Sorry we having a short problem , our code monkey will fix them soon.
				 <br/>
				 <br/>
				 For any urgent helps, please contact site admin for support.
				 <br/>
				 <br/>
				 Return to home page <a href=''/>Home</a>");
			}			
		} else {
			if($this->tracingError==true) $this->SQLlog("Query OK;".$sql, $_tracingName);
		};
			
		$this->querynum++;
		$this->Errno = mysqli_errno($this->Link_ID);
		$this->Error = mysqli_error($this->Link_ID);
		$this->Row = 0;
		$to = "";
	}
	
	
	public function select()
	{
		$args=func_get_args();
		$this->selectables=join(",",$args);
		return $this;
	}
	public function from()
	{
		$args=func_get_args();
		$this->table = join(",",$args);
		return $this;
	}
	public function where($clause)
	{
		$this->whereClause = $clause;
		return $this;
	}
	public function limit($limit)
	{
		$this->limit = $limit;
		return $this;
	}
	public function order($order){
		$this->order = ' ORDER BY '.$order;
		return $this;
	}
	
	public function getQuery(){
		$query = "SELECT {$this->selectables} FROM {$this->table}";
		if (!empty($this->whereClause))
		$query .= " WHERE {$this->whereClause}";
		if (!empty($this->order))
		$query .= " {$this->order}";
		if (!empty($this->limit))
		$query .= " LIMIT {$this->limit}";
		return $query;
	}
	public function commitSelect()
	{
		$query = $this->getQuery();
		$this->query($query);
		while($line = $this->fetch_array()){
			$result[] = $line;
		};
		//echo $query;
		$this->selectables='';
		$this->table='';
		$this->whereClause='';
		$this->order='';
		$this->limit='';
		return $result;
	}
	public function paging($pagesize=10,$length=5,$ext='')
	{
		$query = "SELECT count(*) as totalrecord FROM {$this->table}";
		if (!empty($this->whereClause))
		$query .= " WHERE {$this->whereClause}";
		
		$this->query($query);
		//echo $query;
		$res=$this->fetch_array();
		$result["totalrecord$ext"]= $res['totalrecord'];
		$totalpage = ceil($res['totalrecord']/$pagesize);
		$result["totalpage$ext"]= $totalpage;
		//echo $totalpage;
		(!$_REQUEST["page$ext"])?$currentpage=1:$currentpage=$_REQUEST["page$ext"];
		$result["currentpage$ext"]=$currentpage;
		 if ($totalpage <=$length){
		   $start=1;
		   $end=$totalpage;
		}else{
		  if ($currentpage<=3){
			$start=1;
			$end=$start+$length-1;
		  }else if (($currentpage+$length-3) >= $totalpage){
					  $start=$currentpage-($length-($totalpage-$currentpage)-1);
					  $end=$totalpage;
				}else{
					  $start=$currentpage-2;
					  $end=$currentpage+($length-3);
				}   
		}
		if ($currentpage<$totalpage)
			$result["nextpage$ext"]= $currentpage+1;

		if ($currentpage>1)
			$result["previouspage$ext"]= $currentpage-1;

		if ($start >1)
			$result["firstpage$ext"]= 1;

		if ($end<$totalpage)
			$result["lastpage$ext"]= $totalpage;
		for($x=$start;$x<=$end;$x++)
		  $pages[]=$x;
		  $result["pages$ext"]= $pages;
		  //$result[]= array(ext=>$ext);
		//$res = array_slice($res, ($currentpage-1)*$pagesize, $pagesize);
		$query = "SELECT {$this->selectables} FROM {$this->table}";
		if (!empty($this->whereClause)){
			$from_index=($currentpage-1)*$pagesize;
			$query .= " WHERE {$this->whereClause} {$this->order} limit {$from_index},{$pagesize} ";
		}
		$this->query($query);
		while($line = $this->fetch_array()){
			$ress[] = $line;
		};
		$this->table='';
		$this->selectables='';
		$this->whereClause='';
		$this->limit='';
		$this->order='';
		$result["record$ext"]= $ress;
		unset($ress);
		//print_r($result);
		//echo $query;
		return $result;
	
	}

	
	# Caching Query
	private function cacheSQL($sql, $sqlArray=array(), $caching=false, $type=0) {
		$md5Cache = md5($sql);
		$subdir = substr($md5Cache, 0, 2);
		$touchtime = 0;
		$_tracingName = ($this->tracingName==null || empty($this->tracingName)) ? "tracing" : $this->tracingName;
		if(!is_dir($this->CacheDir.$subdir)) @mkdir($this->CacheDir.$subdir, 0755);
		$cacheFullPathFile = $this->CacheDir.$subdir."/mysqlcache_".$md5Cache.".cache";
		$cacheTempFile =  $this->CacheDir.$subdir."/tmp".getmypid()."_mysqlcache_".$md5Cache.".cache";
		if(@is_file($cacheFullPathFile)) $touchtime = filectime($cacheFullPathFile);

		if(!@fopen($cacheFullPathFile, "r") || (time() - $touchtime > $this->CacheLifeTime)) {
			if($this->tracingError==true) $this->SQLlog("Cache file handing to read error or over chching time, try to rebuild.", $_tracingName);
			$sqlArray = $this->rebuild_Sql($sql, $type);
			$cacheArray = $sqlArray;
			$caching = true;
		} else {
			$fp = @fopen($cacheFullPathFile, "r");
			$__cache = @fread($fp, @filesize($cacheFullPathFile));
			@fclose($fp);
			$cacheArray = unserialize($__cache);
			if(!is_array($cacheArray) || count($cacheArray)==0) {
				if($this->tracingError==true) $this->SQLlog("Recordset had unexpected EOF (in serialized recordset), try to rebuild.", $_tracingName);
				$sqlArray = $this->rebuild_Sql($sql, $type);
				$cacheArray = $sqlArray;
				$caching = true;
			} else {
				$caching = false;
			};
		};

		if($caching==true && $this->sqlCache==true && is_array($sqlArray) && count($sqlArray)>0) {
			if($this->Cachelog==true) $this->SQLlog($sql, "cache");
			$__cache = serialize($sqlArray);
			$fp = @fopen($cacheTempFile, 'a');
			if (!is_file($cacheTempFile)) {
				if($this->tracingError==true) $this->SQLlog("Cache temp file opening error", $_tracingName);
			} else {
				if(!flock($fp, LOCK_EX) && !ftruncate($fp, 0)) {
					if($this->tracingError==true) $this->SQLlog("Cache temp file handing to write error", $_tracingName);
				} else {
					if(!fwrite($fp, $__cache)) {
						if($this->tracingError==true) $this->SQLlog("Cache temp file writting error", $_tracingName);
						@flock($fp, LOCK_UN);
						@fclose($fp);
						@unlink($cacheTempFile);
					} else {
						@flock($fp, LOCK_UN);
						@fclose($fp);
						if(is_file($cacheFullPathFile)) {
							if(!@unlink($cacheFullPathFile)) {
								if($this->tracingError==true) $this->SQLlog("Cache file delete error", $_tracingName);
							};
						};
						if(!@rename($cacheTempFile, $cacheFullPathFile)) {
							if($this->tracingError==true) $this->SQLlog("Cache file rename error", $_tracingName);
						};
					};
				};
			};
			$cacheArray = $sqlArray;
		};

		return $cacheArray;
	}

	# Rebuild Sql Query
	private function rebuild_Sql($sql, $type=0) {
		$result = array();
		if(preg_match("/^select/i", $sql)) {
			if ($type==1) {
				$toLimit = " LIMIT 1";
				if(!eregi('LIMIT', $sql)) $sql = $sql.$toLimit;
				$this->query($sql);
				$result = $this->fetch_array();
			} else {
				$this->query($sql);
				while($line = $this->fetch_array()) {
					$result[] = $line;
				};
			};
		};

		return $result;
	}

	# Reporting
	private function halt($message = '', $sql = '') {
		if(isset($_REQUEST['dbainfo'])|| 1==1){
			$this->SQLlog($message.";".$sql, "error");
			$dberror = $this->error();
			$dberrno = $this->errno();
			$errmsg = "<b>Database Error:</b> $message<br /><br />";
			$errmsg .= "<b>Time:</b> ".date("Y-n-j g:ia", time())."<br />";
			$errmsg .= "<b>Script:</b> ".$_SERVER['PHP_SELF']."<br /><br />";
			if($sql) $errmsg .= "<b>SQL:</b> ".$this->sql_html($sql)."<br />";
			$errmsg .= "<b>Error:</b>  $dberror<br />";
			$errmsg .= "<b>Errno:</b>  $dberrno<br /></div>";
			echo $errmsg; // Sing: Front end error
			die("Session Halted.");	
			$this->free_result();
			$this->close();
		} else {
			$this->SQLlog($message.";".$sql, "error");
			$dberror = $this->error();
			$dberrno = $this->errno();
			$errmsg = "<b>Database Error:</b> $message<br /><br />";
			$errmsg .= "<b>Time:</b> ".date("Y-n-j g:ia", time())."<br />";
			$errmsg .= "<b>Script:</b> ".$_SERVER['PHP_SELF']."<br /><br />";
			if($sql) $errmsg .= "<b>SQL:</b> ".$this->sql_html($sql)."<br />";
			$errmsg .= "<b>Error:</b>  $dberror<br />";
			$errmsg .= "<b>Errno:</b>  $dberrno<br /></div>";
			$this->free_result();
			$this->close();

			$opts = array('http' =>
			    array(
			        'method'  => 'POST',
			        "header" => "Content-Type: application/x-www-form-urlencoded\r\n" ."email-token: 123456\r\n",
			        'content' => http_build_query(
					    array(
					        'site' => $_SERVER['HTTP_HOST'],
					        'content' => $errmsg,
					    )
					)
			    )
			);
			$context = stream_context_create($opts);
			$email_result = file_get_contents("", false, $context);

			if(isset($_GET['callback'])){
				$final_result_array['code'] = '-98';
				header('Content-Type: application/json; charset=UTF-8');
				$json = json_encode($final_result_array,JSON_UNESCAPED_UNICODE);
				echo "".isset($_GET['callback'])
				? "{$_GET['callback']}($json)"
				: $json."";
				die();
			}
			echo '<div><img src="/img/logo.png" style="width:200px;margin-bottom:12px;"></div><div>There currently have an error on DB connection, we are trying hard to resume it, please try again later.</div>';
			die();
		}
	}

	private function SQLlog($SQLstr='', $file='') {
		if($this->NoNeedLog==false) {
			$today = date("Ymd", time());
			$fp = fopen($this->Log.$today."_".$this->dbname."_".$file.".log", "a");
			//echo $this->Log.$today."_".$this->dbname."_".$file.".log";
			@fwrite($fp, date("Y/m/d H:i:s", time()).";Query:".$SQLstr.";Script:".$_SERVER["PHP_SELF"].";IP:".$_SERVER["REMOTE_ADDR"]."\n");
			$fp = null;
		}
		return "";
	}
}
?>