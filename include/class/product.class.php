<?php
/* 
    Objective: product related
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_product
        tbl_product_addon_cate
        tbl_product_cutoff_tpl
        tbl_product_img
        tbl_product_item_content
        tbl_product_tpl
        tmap_addonCate_product
        tmap_addon_product
        tmap_cate_product
        tmap_combo_product
        tmap_content_product
        tmap_grp_product
        
        
*/

require_once dirname(__FILE__). '/abstract.class.php';
class product extends baseClass{
    function init(){
        $this->multiShopFieldArr = ['is_new', 'product_tag_en', 'bottom_product_tag_en', 'bottom_product_tag_2_en', 'product_tag_tc', 'bottom_product_tag_tc', 'bottom_product_tag_2_tc', 'delivery_start_date', 'delivery_end_date', 'publish_date', 'force_checkout_end_date', 'force_checkout_start_date', 'is_force_checkout', 'show_in_web', 'deli_start_date', 'cutoff_tpl_id', 'product_tpl_id', 'ava_b4_day', 'ava_b4_cutoff', 'product_code', 'product_name_tc', 'product_name_en', 'cust_deli_act', 'cust_deli_tc', 'cust_deli_en', 'cust_info_act', 'cust_info_tc', 'cust_info_en', 'cust_addinfo_act', 'cust_addinfo_tc', 'cust_addinfo_en', 'price', 'sp_price', 'sales_status', 'display_in_cate', 'deli_id'];
        $this->multiLangFieldArr = ['product_tag', 'bottom_product_tag', 'bottom_product_tag_2', 'product_name', 'cust_deli', 'cust_info', 'cust_addinfo'];
    }

    function duplicateProduct($src_product_id, $shop_id){
        // duplicate the product 
        // $src_product_id: FK, product which copy from 
        // $shop_id: shop in use

        $product_id = $this->DB->insert_db('tbl_product', null);

        // duplicate tbl_product
        $sql = "UPDATE `tbl_product` AS p,
        (
        SELECT ";

        foreach($this->multiShopFieldArr as $k => $field){
            if ($k > 0)
                $sql .= ", ";

            $sql .= "`gift_$field`";
            $sql .= ", `zuri_$field`";
            $sql .= ", `ltp_$field`";
        }
        
        $sql .= "FROM `tbl_product` WHERE product_id = $src_product_id) AS s
        SET ";
        
        foreach($this->multiShopFieldArr as $k => $field){
            if ($k > 0)
                $sql .= ", ";

            $sql .= "p.`gift_$field` = s.`gift_$field`";
            $sql .= ", p.`zuri_$field` = s.`zuri_$field`";
            $sql .= ", p.`ltp_$field` = s.`ltp_$field`";
        }
        
        $sql .= "WHERE p.product_id = $product_id";

        $this->DB->update($sql);

        // duplicate tbl_product_addon_cate 附加產品
        $sql = "SELECT * FROM `tbl_product_addon_cate` WHERE `activate` = 'Y' AND `product_id` = $src_product_id";
        $addonList = $this->DB->get_Sql($sql);
        if ($addonList){
            foreach($addonList as $k => $addonCate){
                unset($addonList[$k]['addon_cate_id']);
            }

            $this->updateAddonCateMap($product_id, $addonList, $shop_id);
        }

        // duplicate tbl_product_img 產品圖片
        $sql = "SELECT * FROM `tbl_product_img` WHERE `activate` = 'Y' AND `product_id` = $src_product_id";
        $imgList = $this->DB->get_Sql($sql);
        if ($imgList){
            $path_arr = [];

            foreach($imgList as $k => $img){
                array_push($path_arr, $img['img']);
            }

            $this->updateImgPathMap($product_id, $path_arr, $shop_id);
        }

        // duplicate tbl_product_item_content 
        $sql = "SELECT * FROM `tbl_product_item_content` WHERE `activate` = 'Y' AND `product_id` = $src_product_id";
        $itemList = $this->DB->get_Sql($sql);
        if ($itemList){
            $fieldArr= ['gift_name_tc', 'gift_name_en', 'zuri_name_tc', 'zuri_name_en', 'ltp_name_tc', 'ltp_name_en', 'qty'];
            foreach($itemList as $k => $item){
                $src_content_id = $item['content_id'];
                $content_id = $this->DB->insert_db('tbl_product_item_content', null);
                $sql = "UPDATE `tbl_product_item_content` AS p, (
                        SELECT 
                    ";
                foreach($fieldArr as $kk => $field){
                    if ($kk > 0)
                        $sql .= ", ";
        
                    $sql .= "`$field`";
                }

                $sql .= "FROM `tbl_product_item_content` WHERE content_id = $src_content_id) AS s
                    SET p.`product_id` = '$product_id'";
                
                foreach($fieldArr as $field){
                    $sql .= ", p.`$field` = s.`$field`";
                }
                
                $sql .= " WHERE p.content_id = $content_id";

                $this->DB->update($sql);
            }
        }

        return $this->getProductDtl($product_id, $shop_id);
    }

    function editCutoffTpl($cutoff_tpl_id, $data){
        // edit cutoff template
        // $cutoff_tpl_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($cutoff_tpl_id == 0)
            $cutoff_tpl_id = $this->DB->insert_db('tbl_product_cutoff_tpl', null);

        $sql = $this->DB->buildSql_update("tbl_product_cutoff_tpl", "cutoff_tpl_id", $cutoff_tpl_id, $data);
        $this->DB->update($sql);

        return $this->getCutoffTplDtl($cutoff_tpl_id);
    }

    function getCutoffTplDtl($cutoff_tpl_id, $shop_id = 1 ){
        // get the cutoff template Detail
        // $cutoff_tpl_id: PK

        $prefix = $this->getPrefix($shop_id);

        $sql = "SELECT t.*, GROUP_CONCAT(p.product_id) AS product_id_arr
                FROM `tbl_product_cutoff_tpl` AS t
                LEFT JOIN `tbl_product` AS p ON $prefix"."_cutoff_tpl_id = t.cutoff_tpl_id AND p.activate = 'Y'
                WHERE t.activate = 'Y' AND t.cutoff_tpl_id = $cutoff_tpl_id";
        $res = $this->DB->get_Sql($sql)[0];

        return $res;
    }

    function getCutoffTplList($shop_id, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $selectSql = "SELECT * ";
        $fromSql = "FROM `tbl_product_cutoff_tpl` AS t ";
        $whereSql = "WHERE t.`activate` = 'Y' && t.`shop_id` = '$shop_id'";

        if ($filter){
            foreach($filter as $k => $i){
                if ($i || $i == '0'){
                    switch ($k){
                        case 'skey':
                            $whereSql .= " AND (t.cutoff_tpl_id LIKE '%$i%' OR t.adm_name = '%$i%')";
                            break;
                        
                        default: 
                            break;
                    }                    
                }
            }
        }
        $sql = $selectSql . $fromSql . $whereSql . " GROUP BY t.cutoff_tpl_id";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        
        return $res;
    }

    function editProductTpl($tpl_id, $data){
        // edit product template
        // $tpl_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($tpl_id == 0)
            $tpl_id = $this->DB->insert_db('tbl_product_tpl', $data);
        
        $sql = $this->DB->buildSql_update("tbl_product_tpl", "tpl_id", $tpl_id, $data);
        $this->DB->update($sql);

        return $this->getProductTplDtl($tpl_id);
    }

    function getProductTplDtl($tpl_id){
        // get the product template Detail
        // $tpl_id: PK

        $selectSql = "SELECT * ";
        foreach(['deli_msg', 'info_msg'] AS $field){
            $selectSql .= ",$field" . "_" . $this->lang . " AS $field ";
        }

        $sql = $selectSql . "
                FROM `tbl_product_tpl` AS t
                WHERE t.activate = 'Y' AND t.tpl_id = '$tpl_id'";
        $res = $this->DB->get_Sql($sql)[0];

        return $res;
    }

    function getProductTplList($shop_id, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $selectSql = "SELECT * ";
        $fromSql = "FROM `tbl_product_tpl` AS t ";
        $whereSql = "WHERE t.activate = 'Y' && t.shop_id = '$shop_id' ";


        if ($filter){
            foreach($filter as $k => $i){
                if ($i || $i == '0'){
                    switch ($k){
                        case 'skey':
                            $whereSql .= " AND (t.tpl_id = '$i' OR t.adm_name LIKE '%$i%')";
                            break;
                        default: 
                            break;
                    }                    
                }
            }
        }
        $sql = $selectSql . $fromSql . $whereSql . " GROUP BY t.tpl_id";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);
        
        return $res;
    }

    function delProductImg($map_id){
        $sql = $this->DB->buildSql_update("tbl_product_img", "map_id", $map_id, [
            "activate" => 'N'
        ]);
        $this->DB->update($sql);
        return null;
    }

    function sortProductImg($map_id_arr){
        // sorting function for product image
        // $map_id_arr: ascending sequence base on the map_id_arr

        $this->DB->sort("tbl_product_img", "map_id", $map_id_arr);
        return null;
    }

    function uploadProductImgs($files){
        return $this->DB->uploadImgs($files, "product");
    }

    function calcPriceByDeliveryList($deliList, $shop_id){
        // recalculate the final price base on delivery record
        // $deliList: deliData array
        // $shop_id: shop in use

		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);

        $product_price = 0;
		$shipping_fee = 0;
		$timeslot_fee = 0;
		$final_price = 0;
		$ttl_deli_price = 0;

        $res = [];
        $res['deli_arr'] = [];
    
        foreach($deliList as $k => $deli){
			$deli_price = 0;

            // update the delivery records and re-calculate the latest price			
			$productDtl = $this->getProductDtl($shop_id, $deli['product_id']);
			$regionDtl = $deliveryCls->getRegionDtl($deli['deli_region_id']);
			$timeslotDtl = $deliveryCls->getTimeslotDtl($deli['deli_timeslot_id']);
			
			$dp_price = $productDtl['sp_price'] ? $productDtl['sp_price']: $productDtl['price'];
			$dp_shipping_fee = $regionDtl['shipping_fee'] ? $regionDtl['shipping_fee'] : 0;
			$dp_timeslot_fee = $timeslotDtl['deli_fee'] ? $timeslotDtl['deli_fee'] : 0;

			$itemList = $deli['deliDtl']['itemList'];

            // check if it is additional price for the selected sku (upgrade product)
            foreach ($productDtl['itemList'] as $k => $item){
                $sel = $itemList[$k]['sku_id'];
                if ($item['itemGrpDtl']['item_grp_content'] && $sel){
                    foreach ($item['itemGrpDtl']['item_grp_content'] as $sku){
                        if ($sku['sku_id'] == $sel){
                            if ($sku['additional_price'] > 0){
                                // print_r($sku);
                                $dp_price += $sku['additional_price'];
                            }
                        }
                    }
                }
            }

			
			// add additional product price in the product price
			$addon_list = $deli['addon_list'];
			$dp_addon_price = 0;
			if ($addon_list){
				foreach ($addon_list as $addon){
					// $productDtl = $this->getProductDtl($addon['product_id'], $shop_id);
					$dp_addon_price += ($addon['product_price'] * $addon['qty']);
				}
			}

			$deliData['product_price'] = $dp_price;
			$deliData['shipping_fee'] = $dp_shipping_fee;
			$deliData['timeslot_fee'] = $dp_timeslot_fee;
			$deliData['addon_price'] = $dp_addon_price;

			$deli_price = $dp_price + $dp_shipping_fee + $dp_timeslot_fee + $dp_addon_price;
			$deliData['deli_price'] = $deli_price;
			
			
			$final_price += $deli_price;
			$ttl_deli_price += $deli_price;
			$product_price += $dp_price + $dp_addon_price;
			$shipping_fee += $dp_shipping_fee;
			$timeslot_fee += $dp_timeslot_fee;

			array_push($res['deli_arr'], $deliData);
		}


        $res['ttl_deli_price'] = $ttl_deli_price;
        return $res;
    }

    function editAddonGroup($data){
        // edit addon 
        // $discount_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($data['addon_group_id'] == 0){
            $addon_group_id = $this->DB->insert_db('tbl_addon_group', $data);
        } else {
            $addon_group_id = $this->DB->update_Sql('tbl_addon_group', 'addon_group_id', $data);
        }
        $this->DB->updateMap("tmap_addon_group", "addon_group_id", $addon_group_id, "addon_id", $data['addon_id_arr']);
        unset($data['product_id_arr']);
        
        return $this->getAddonGroupDtl(null, $addon_group_id);
    }

    function editAddon($addon_id, $data){
        // edit addon 
        // $discount_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 
        
        if ($addon_id == 0)
            $addon_id = $this->DB->insert_db('tbl_addon', $data);
        
        $this->DB->updateMap("tmap_addon_product", "addon_id", $addon_id, "product_id", $data['product_id_arr']);
        unset($data['product_id_arr']);

        $sql = $this->DB->buildSql_update("tbl_addon", "addon_id", $addon_id, $data);
        $this->DB->update($sql);
        
        return $this->getAddonDtl(null, $addon_id);
    }
    
    function editProduct($data){
        require_once ROOT.'/include/class/audit.class.php';
		$auditCls = new audit($this->DB, $this->lang, $this->isAdmin);

        $shop_id = $data['shop_id'];
        $prefix = $this->getPrefix($shop_id);
        
        // re-sturcture $data
        foreach ($this->multiShopFieldArr as $k => $multiShopField){
            if (isset($data[$multiShopField])){
                $data[$prefix. "_" . $multiShopField] = $data[$multiShopField];
                unset($data[$multiShopField]);
            }
        }

        if ($data['product_id'] == 0){
            $product_id = $this->DB->insert_db('tbl_product', $data);
        } else {
            $product_id = $this->DB->update_Sql("tbl_product", "product_id", $data);
        }

        if ($data['item_json']){
            $auditCls->logProductItemGroup($product_id, $data['item_json']);
            $product_item_id_arr = $this->DB->updateForeignMap("tbl_product_item_group", "product_id", $product_id, "product_item_id", $data['item_json']);
            // Clone version 
            // foreach ($product_item_id_arr as $k => $product_item_id){
            //     $this->DB->updateForeignMap("tbl_product_item_content", "product_item_id", $product_item_id, "content_id", $data['item_json'][$k]['item_list']);
            // }
        }
        
        if (isset($data['product_img_path_arr'])){
            $this->updateImgPathMap($product_id, $data['product_img_path_arr'], $shop_id);
        }
        
        if (isset($data['src_product_id_arr'])){
            $this->DB->updateMap("tmap_combo_product", "product_id", $product_id, "src_product_id", $data['src_product_id_arr']);
        }
        
        if (isset($data['card_id_arr'])){
            $this->DB->updateMap("tmap_product_card", "product_id", $product_id, "card_id", $data['card_id_arr']);
        }
        if (isset($data['cate_id_arr'])){
            $cateList = [];
            
            foreach ($data['cate_id_arr'] as $id){
                if ($id){
                    array_push($cateList, [
                        "product_id" => $product_id,
                        "cate_id" => $id,
                        "shop_id" => $shop_id
                    ]);
                }
            }

            $this->DB->updateDataMapByRefId("tmap_cate_product", "product_id", $product_id, "cate_id", $cateList);
        }
        
        if (isset($data['greeting_id_arr'])){
            $this->DB->updateMap("tmap_greeting_product", "product_id", $product_id, "greeting_id", $data['greeting_id_arr']);
        }
        
        if ($data['addon_id_arr']){
            $this->DB->updateMap("tmap_product_addon", "product_id", $product_id, "addon_id", $data['addon_id_arr']);

            // $this->updateAddonCateMap($product_id, $data['addonList'], $shop_id);
        }

        return $this->getProductDtl($shop_id, $product_id);
    }

    private function updateItemList($product_id, $itemList, $shop_id){
        // private function for edit the itemList
        // $product_id: FK
        // $itemList: item Array
        // $shop_id: shop in use

        $prefix = $this->getPrefix($shop_id);
        $content_id_arr = [];
        foreach($itemList as $k => $item){
            $logData = [];

            $content_id = $item['content_id'];
            if ($content_id == 0){
                $content_id = $this->DB->insert_db('tbl_product_item_content', null);
                $logData = [
                    'action' => "add",
                    'product_id' => $product_id,
                    'content_id' => $content_id,
                    'updated_by_id' => $_SESSION['admin']['admin_id']
                ];
            } else {
                $sql = "SELECT * FROM `tbl_product_item_content` AS pic WHERE pic.`content_id` = '$content_id'";
                $org = $this->DB->get_Sql($sql)[0];
                
                if ($item['name_tc'] != $org[$prefix. '_name_tc'] || 
                    $item['name_en'] != $org[$prefix. '_name_en'] || 
                    $item['qty'] != $org['qty'] || 
                    $item['type'] != $org['type'] || 
                    $item['show_in_web'] != $org['show_in_web'] || 
                    $org['sort'] != ($k + 1)
                    ){

                    $logData = [
                        'action' => 'update',
                        'product_id' => $product_id,
                        'content_id' => $content_id,
                        'original_json' => json_encode($org),
                        'updated_by_id' => $_SESSION['admin']['admin_id']
                    ];
                }


            }
            

            // construct the existing content_id for remove procedure
            array_push($content_id_arr, $content_id);
            
            $data = $item;
            $data['sort'] = $k +1;
            unset($data['content_id']);
            $data['activate'] = 'Y';
            $data['product_id'] = $product_id;
            $data[$prefix."_name_tc"] = $item['name_tc'];
            unset($data['name_tc']);
            $data[$prefix."_name_en"] = $item['name_en'];
            unset($data['name_en']);

            $sql = $this->DB->buildSql_update("tbl_product_item_content", "content_id", $content_id, $data);
            $this->DB->update($sql);

            if ($logData){
                $logData['updated_json'] = json_encode($data);
                $this->DB->insert_db('log_product_item_content', $logData);
            }

        }

        // remove the un-used item
        $sql = "SELECT i.*, ic.content_id
                FROM `tbl_product_item_content` AS ic 
                    LEFT JOIN `tbl_item_grp_content` AS igc ON igc.grp_id = ic.ref_id AND igc.`activate` ='Y' AND ic.type = 'grp_item'
                    LEFT JOIN `tbl_sku` AS s ON s.activate = 'Y' AND (ic.ref_id = s.sku_id AND ic.type = 'sku')
                    LEFT JOIN `tbl_item` AS i ON s.item_id = i.item_id AND i.activate = 'Y'
                    LEFT JOIN `tbl_sku` AS ss ON ss.sku_id = i.replace_sku_id
                WHERE ic.activate = 'Y' AND ic.content_id NOT IN (".implode(",", $content_id_arr).") AND ic.product_id = '$product_id' 
                GROUP BY ic.content_id";
        $removeList = $this->DB->get_Sql($sql);
        foreach ($removeList as $val){
            $this->DB->insert_db('log_product_item_content', [
                'action' => 'del',
                'product_id' => $product_id,
                'content_id' => $val['content_id'],
                'original_json' => json_encode([
                    "name_tc" => $val['name_tc']
                ]),
                'updated_by_id' => $_SESSION['admin']['admin_id']
            ]);
        }


        $sql = "UPDATE `tbl_product_item_content` SET `activate` ='N' WHERE product_id = '$product_id'";
        if ($content_id_arr && implode(',', $content_id_arr) != ""){
            $sql .= " AND content_id NOT IN (".implode(",", $content_id_arr).")";
        }
        $this->DB->update($sql);
    }

    function updateAddonCateMap($product_id, $addonList, $shop_id){
        // private function for edit the addon cate tailor made by product
        // $product_id: FK
        // $addonList: addon Array
        // $shop_id: shop in use

        $addon_cate_id_arr = []; // use for remove the un-used category
        foreach ($addonList as $k => $addon){
            $addon_cate_id = $addon['addon_cate_id'];

            if ($addon_cate_id == 0)
                $addon_cate_id = $this->DB->insert_db('tbl_product_addon_cate', null);

            array_push($addon_cate_id_arr, $addon_cate_id); // those id will not be in-activated


            // re-constructure the data for update the row
            $data = $addon;
            unset($data['addon_cate_id']);
            $data['activate'] = 'Y';
            $data['product_id'] = $product_id;
            $data['shop_id'] = $shop_id;

            $this->DB->updateMap("tmap_addonCate_product", "addon_cate_id", $addon_cate_id, "product_id", $data['product_id_arr']);
            unset($data['product_id_arr']);
            
            $sql = $this->DB->buildSql_update("tbl_product_addon_cate", "addon_cate_id", $addon_cate_id, $data);
            $this->DB->update($sql);
        }   

        $sql = "UPDATE `tbl_product_addon_cate` 
                SET `activate` = 'N' 
                WHERE product_id = $product_id AND shop_id = $shop_id";
        if ($addon_cate_id_arr && implode(',', $addon_cate_id_arr) != ""){
            $sql .= " AND addon_cate_id NOT IN (".implode(",", $addon_cate_id_arr).")";
        }
        $this->DB->update($sql);
    }

    function updateImgPathMap($product_id, $path_arr, $shop_id){
        // private function for edit the product images
        // $product_id: FK
        // $path_arr: path 
        // $shop_id: shop in use
        // $path_arr = explode(',', $path);
        $sql = "UPDATE `tbl_product_img` set activate = 'N' WHERE product_id = '$product_id' AND shop_id = '$shop_id'";
        $this->DB->update($sql);

        $sql = "SELECT * FROM `tbl_product_img` WHERE product_id = '$product_id' AND shop_id = '$shop_id' AND `activate`= 'Y' ORDER BY `sort`";
        $imgList = $this->DB->get_sql($sql);
        $sortKey = 1;
        foreach ($imgList as $k => $img){
            $path = $path_arr[$k];
            if ($path){
                // update path if the file is exist
                $sql = $this->DB->buildSql_update("tbl_product_img", "map_id", $img['map_id'], [
                    "product_id" => $product_id,
                    "shop_id" => $shop_id,
                    "img" => $path, 
                    "sort" => $sortKey,
                    "activate" => 'Y'
                ]);
                $sortKey++;
            } 
            $this->DB->update($sql);
        }

        if (sizeof($path_arr) > sizeof($imgList)){
            for ($i =  sizeof($imgList); $i < sizeof($path_arr) ; $i++){
                $path = $path_arr[$i];
                $data = [
                    'product_id' => $product_id,
                    'shop_id' => $shop_id,
                    'img' => $path,
                    "sort" => $sortKey,
                    'updated_by_id' => $_SESSION['admin']['admin_id']
                ];
                $sortKey++;
                $this->DB->insert_db('tbl_product_img', $data);
            }
        } 
    }

    function getAddonDtl($shop_id, $addon_id){
        if ($addon_id){
            return $this->getAddonList($shop_id, ["addon_id" => $addon_id], $this->DB->getUnique)[0];
        } else {
            return null;
        }
    }

    function getAddonList($shop_id, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $prefix = $this->getPrefix($shop_id);
        $selectSql = "SELECT a.*, GROUP_CONCAT(m.product_id) AS product_id_arr ";
        $fromSql = "FROM `tbl_addon` AS a LEFT JOIN `tmap_addon_product` AS m ON m.addon_id = a.addon_id ";
        // $whereSql = "WHERE a.activate = 'Y' && a.shop_id = '$shop_id'";
        $whereSql = "WHERE a.activate = 'Y'";


        if ($filter){
            foreach($filter as $k => $i){
                if ($i || $i == '0'){
                    switch ($k){
                        case 'skey':
                            // $whereSql .= " AND (a.`inhouse_name_tc` LIKE '%$i%' OR a.`inhouse_name_en` LIKE '%$i%' OR a.`$prefix"."_name_tc` LIKE '%$i%' OR a.`$prefix"."_name_en` LIKE '%$i%' OR a.alias LIKE '%$i%')";
                            $whereSql .= " AND (a.`inhouse_name_tc` LIKE '%$i%' OR a.`inhouse_name_en` LIKE '%$i%')";
                            break;
                        case 'addon_id':
                            $whereSql .= " AND (a.addon_id = '$i')";
                            break;
                        
                        default: 
                            break;
                    }                    
                }
            }
        }
        $sql = $selectSql . $fromSql . $whereSql . " GROUP BY a.addon_id";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        foreach($res as $k => $i){
            $res[$k] = $this->dispatchAddon($i);
        }

        
        return $res;
    }

    function getAddonGroupDtl($shop_id, $addon_group_id){
        if ($addon_group_id){
            return $this->getAddonGroupList($shop_id, ["addon_group_id" => $addon_group_id], $this->DB->getUnique)[0];
        } else {
            return null;
        }
    }

    function getAddonGroupList($shop_id, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $prefix = $this->getPrefix($shop_id);
        $selectSql = "SELECT ag.* ";
        $fromSql = "FROM `tbl_addon_group` AS ag ";
        $whereSql = "WHERE ag.activate = 'Y'";


        if ($filter){
            foreach($filter as $k => $i){
                if ($i || $i == '0'){
                    switch ($k){
                        case 'skey':
                            $whereSql .= " AND (ag.`inhouse_name_tc` LIKE '%$i%' OR ag.`inhouse_name_en` LIKE '%$i%' OR ag.`$prefix"."_name_tc` LIKE '%$i%' OR ag.`$prefix"."_name_en` LIKE '%$i%')";
                            break;
                        case 'addon_group_id':
                            $whereSql .= " AND (ag.addon_group_id = '$i')";
                            break;
                        
                        default: 
                            break;
                    }                    
                }
            }
        }
        $sql = $selectSql . $fromSql . $whereSql . " GROUP BY ag.addon_group_id";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        foreach($res as $k => $i){
            $res[$k] = $this->dispatchAddonGroup($shop_id, $i);
        }

        
        return $res;
    }

    function dispatchAddonGroup($shop_id, $addon_group){
        $addon_group_id = $addon_group['addon_group_id'];
        $sql = "SELECT * 
                FROM `tmap_addon_group` AS m
                WHERE m.activate = 'Y' AND m.addon_group_id = '$addon_group_id'";
        $addon_list = $this->DB->get_Sql($sql);

        foreach ($addon_list as $k => $addon){
            $addon_list[$k] = $this->getAddonDtl($shop_id, $addon['addon_id']);
        }
        $addon_group['addon_list'] = $addon_list;


        return $addon_group;
    }

    function dispatchAddon($addon){
        $addon_id = $addon['addon_id'];
        unset($addon['shop_id']);
        
        $addon['product_list'] = $this->getProductList(1,["addon_product_addon_id" => $addon_id]);

        // $sql = "SELECT * 
        //         FROM `tmap_addon_product` AS m 
        //         WHERE m.addon_id ='$addon_id' AND m.activate = 'Y'";
        // $product_list = $this->DB->get_Sql($sql);

        // foreach ($product_list as $k => $i){
        //     $product_list[$k] = $this->getProductDtl(1, $i['product_id']);
        // }
        // $addon['product_list'] = $product_list;

        return $addon;
    }

    function getProductList($shop_id = 1, $filter = null, $limiter = null, $sorting = null, $dispatch = true){
        // get the list
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $fromShort = "p";
        $prefix = $this->getPrefix($shop_id);
        
        $selectSql = "SELECT $fromShort.*, GROUP_CONCAT(c.src_product_id) AS src_product_id_arr, IFNULL($prefix"."_sp_price, $prefix"."_price) AS act_price, IF(gift_is_force_checkout = 'Y' AND (gift_force_checkout_start_date < CURDATE() OR gift_force_checkout_start_date IS NULL)
        AND (gift_force_checkout_end_date > CURDATE() OR gift_force_checkout_end_date IS NULL), 'Y', 'N') as is_force_checkout_valid ";
        
        foreach($this->multiShopFieldArr as $field){
            $selectSql .= ", " . $prefix . "_" . $field . " AS " . $field;
        }

        foreach($this->multiLangFieldArr AS $field){
            $selectSql .= ", $prefix" . "_$field" . "_" . $this->lang . " AS $field ";
        }
        
        $fromSql = " FROM tbl_product AS $fromShort 
                    LEFT JOIN `tmap_combo_product` AS c ON c.product_id = $fromShort.product_id ";
        $whereSql = "WHERE $fromShort.activate = 'Y'";
        $afterWhereSql = "";
        if ($filter){
            foreach($filter as $k => $i){
                if ($i){
                    switch ($k){
                        case 'addon_group_id':
                            $whereSql .= " AND ($fromShort.addon_group_id = '$i')";
                            break;
                        case 'addon_product_addon_id':
                            $selectSql .= " ,map.map_id";
                            $fromSql .= "   LEFT JOIN `tmap_addon_product` AS map ON map.product_id = $fromShort.product_id AND map.`activate` = 'Y'";
                            $whereSql .= "  AND (map.`addon_id` = '$i')";
                            $sorting = [
                                "sort" => "map_id",
                            ];
                            break;
                        case 'skey':
                            $fromSql .= "   LEFT JOIN `tbl_product_item_group` AS pig ON pig.product_id = $fromShort.product_id AND pig.`activate` ='Y'
                                            LEFT JOIN `tbl_item_grp_content` AS igc on igc.grp_id = pig.grp_id AND igc.activate ='Y' 
                                            LEFT JOIN `tbl_item` AS i ON i.`item_id` = igc.item_id
                                        ";
                            $whereSql .= "  AND (    
                                                $fromShort.`".$prefix."_product_name_tc` LIKE '%$i%' OR $fromShort.`".$prefix."_product_name_en` LIKE '%$i%' 
                                                OR $fromShort.`".$prefix."_product_code` LIKE '%$i%'
                                                OR i.sku = '$i' OR i.sku_prefix = '$i'
                                                OR i.`".$prefix."_name_tc` LIKE '%$i%' OR i.`".$prefix."_name_en` LIKE '%$i%' OR i.`inhouse_name_tc` LIKE '%$i%' OR i.`inhouse_name_en` LIKE '%$i%'
                                            )";
                            // $fromSql .= "   LEFT JOIN `tbl_product_item_group` AS pig ON pig.product_id = $fromShort.product_id AND pig.`activate` ='Y'
                            //                 LEFT JOIN `tbl_product_item_content` AS pic ON pic.`product_item_id` = pig.`product_item_id` AND pic.`activate` = 'Y'
                            //                 LEFT JOIN `tbl_item` AS i ON i.`item_id` = pic.item_id
                            //             ";
                            // $whereSql .= "  AND (    
                            //                     $fromShort.`".$prefix."_product_name_tc` LIKE '%$i%' OR $fromShort.`".$prefix."_product_name_en` LIKE '%$i%' 
                            //                     OR pic.`".$prefix."_name_tc` LIKE '%$i%' OR pic.`".$prefix."_name_en` LIKE '%$i%' OR pic.`inhouse_name_tc` LIKE '%$i%' OR pic.`inhouse_name_en` LIKE '%$i%'
                            //                     OR $fromShort.`".$prefix."_product_code` LIKE '%$i%'
                            //                     OR i.sku = '$i' OR i.sku_prefix = '$i'
                            //                     OR i.`".$prefix."_name_tc` LIKE '%$i%' OR i.`".$prefix."_name_en` LIKE '%$i%' OR i.`inhouse_name_tc` LIKE '%$i%' OR i.`inhouse_name_en` LIKE '%$i%'
                            //                 )";
                            // $whereSql .= " AND (  || s.sku_val = '$i' || s.`sAlias` LIKE '%$i%')";
                            // $whereSql .= " AND (".$prefix."_product_name_tc LIKE '%$i%' || ".$prefix."_product_name_en LIKE '%$i%' || ".$prefix."_product_code LIKE '%$i%' || pic.".$prefix."_name_tc LIKE '%$i%' || pic.".$prefix."_name_en LIKE '%$i%' || s.sku_val = '$i' || s.`sAlias` LIKE '%$i%')";
                            break;
                        case 'subcate_id':
                            $selectSql .= " ,cm.sort";
                            $fromSql .= " LEFT JOIN `tmap_cate_product` AS cm ON cm.product_id = $fromShort.product_id ";
                            $whereSql .= " AND cm.cate_id in ($i)"; 
                            break;
                        case 'product_id_arr':
                        case 'product_id_list':
                            $whereSql .= " AND $fromShort.product_id in ($i) ";
                            break;
                        case 'cate_id':
                            $fromSql .= " LEFT JOIN `tmap_cate_product` AS cm ON cm.product_id = $fromShort.product_id 
                                            LEFT JOIN `tbl_cate` AS cc ON cc.cate_id = cm.cate_id ";
                            $whereSql .= " AND cc.parent_cate_id = '$i'"; 
                            break;
                        case 'cate_id_arr':
                            $fromSql .= " LEFT JOIN `tmap_cate_product` AS cm ON cm.product_id = $fromShort.product_id 
                                            LEFT JOIN `tbl_cate` AS cc ON cc.cate_id = cm.cate_id ";
                            $whereSql .= " AND (cc.parent_cate_id in ('$i') OR cc.cate_id in ('$i'))"; 
                            break;
                        case 'combo_product':
                            $afterWhereSql .= " WHERE src_product_id_arr IS NOT NULL";
                            break;
                        case 'original_addon_id':
                            $fromSql .= "LEFT JOIN `tbl_product_addon_cate` AS a ON a.product_id = $fromShort.product_id AND a.activate='Y'";
                            $whereSql .= " AND (a.`original_addon_id` = '$i')";
                            break;
                        case 'allCate':
                            $whereSql .= " AND (".$prefix."_display_in_cate = '$i')";
                            break;
                        case 'product_id':
                            $whereSql .= " AND ($fromShort.`product_id` ='$i')";
                            break;
                        case 'greeting_group_id':
                            $whereSql .= " AND ($fromShort.`greeting_group_id` ='$i')";
                            break;
                        case 'grp_id':
                            $fromSql .= " LEFT JOIN `tbl_product_item_group` AS pig ON pig.`product_id` = $fromShort.`product_id` AND pig.`activate` = 'Y'";
                            $whereSql .= " AND (pig.`grp_id` = '$i')";
                            break;
                        case 'addon_id':
                            $fromSql .= "   LEFT JOIN `tmap_product_addon` AS pa ON pa.`product_id` = $fromShort.`product_id` AND pa.`activate` = 'Y'";
                            $whereSql .= " AND (pa.`addon_id` = '$i')";
                            break;
                        case 'waitingPublish':
                            $whereSql .= " AND ($fromShort.$prefix"."_publish_date > CURDATE() AND SUBSTRING_INDEX($fromShort.$prefix"."_sales_status, '_', 1) < 25)";
                            break;
                        case 'card_group_id':
                            $whereSql .= " AND ($fromShort.card_group_id = '$i')";
                            break;
                        case 'greeting_group_id':
                            $whereSql .= " AND ($fromShort.greeting_group_id = '$i')";
                            break;
                        case 'deli_group_id':
                            $whereSql .= " AND ($fromShort.deli_group_id = '$i')";
                            break;
                        case 'price_min':
                            $whereSql .= " AND (IFNULL($prefix"."_sp_price, $prefix"."_price) >= $i)";
                            break;
                        case 'price_max':
                            $whereSql .= " AND (IFNULL($prefix"."_sp_price, $prefix"."_price) <= $i)";
                            break;
                        default: 
                            break;
                    }
                }
            }
        }

        if (!$this->isAdmin){
            if (!$filter)
                $filter = [];
            array_push($filter, ["show_in_web" => 'Y']);
        //     $whereSql .= " AND ($prefix"."_show_in_web = 'Y')";
        }

        $sql = "SELECT * FROM (" . $selectSql . $fromSql . $whereSql . " GROUP BY p.product_id ) AS src " . $afterWhereSql;

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $row = $this->DB->get_Sql($sql);
        $res = [];
        foreach ($row as $k => $i){
            $product = $dispatch ? $this->dispatchProduct($shop_id, $i) : $i;

            // after dispatch filter
            if ($filter){
                foreach ($filter as $k => $i){
                    if ($i){
                        switch ($k){
                            case 'is_out_of_stock_alert':
                                if ($product['is_out_of_stock'] != $i){
                                    // echo "product_id: " .$product['product_id'] . " ||| is_out_of_stock: " . $product['is_out_of_stock'];
                                    $product = null;
                                }
                                break;
                            case 'frontend_show':
                                $status_code = explode("_", $product['frontend_status'])[0];
                                if ($status_code < 30){
                                    $product = null;
                                }
                                break;
                            case 'frontend_status':
                                $status_code = explode("_", $product['frontend_status'])[0];
                                if ($status_code != explode("_", $i)[0]){
                                    $product = null;  
                                }
                                break;
                            
                        }
                    }
                }
            } 

            if ($product)
                array_push($res, $product);
        }

		return $res;
    }
    
    function dispatchProduct($shop_id, $product){
        $prefix = $this->getPrefix($shop_id);
		require_once ROOT.'/include/class/item.class.php';
		$itemCls = new item($this->DB, $this->lang, $this->isAdmin);
        $product_id = $product['product_id'];

        $sql = "SELECT * FROM `tbl_product_img` as i WHERE `shop_id` = '$shop_id' && `product_id` = '$product_id' AND activate ='Y'";
        $product['img'] = $this->DB->get_Sql($sql);  

        //////////// Category START ////////////
        $sql = "SELECT m.*, cc.name_tc
                FROM `tmap_cate_product` AS m
                LEFT JOIN `tbl_cate` AS cc on cc.cate_id = m.cate_id
                WHERE m.activate = 'Y' AND m.product_id = '$product_id' AND cc.activate ='Y'";
        $cate = $this->DB->get_Sql($sql);

        $product['cate'] = $cate;
        //////////// Category END   ////////////

        //////////// Addon    START ////////////
        // Copy version 
        $sql = "SELECT a.*, $prefix"."_name_".$this->lang." AS name
                FROM `tbl_addon` AS a
                LEFT JOIN `tmap_product_addon` AS m ON m.addon_id = a.addon_id
                WHERE m.`activate` = 'Y' AND m.`product_id` = '$product_id'";
        $addonList = $this->DB->get_Sql($sql);

        foreach ($addonList as $k => $addon){
            $addon_id = $addon['addon_id'];
            $sql = "SELECT p.*, if($prefix"."_sp_price, $prefix"."_sp_price, $prefix"."_price) AS act_price, $prefix"."_product_name_".$this->lang." AS product_name, i.img, IFNULL(p.$prefix"."_sp_price, p.$prefix"."_price) as act_price 
                    FROM `tbl_product` AS p
                    LEFT JOIN `tmap_addon_product` AS map ON map.product_id = p.product_id AND map.activate = 'Y'
                    LEFT JOIN `tbl_product_img` AS i  ON p.product_id  = i.product_id AND i.shop_id = $shop_id 
                    WHERE map.addon_id = '$addon_id'
                    GROUP BY p.product_id";
                    
            $addonList[$k]['product_list'] = $this->DB->get_Sql($sql);
        }
        
        $product['addonList'] = $addonList;
        //////////// Addon    END   ////////////

        //////////// Greeting START ////////////
        $sql = "SELECT g.*
                FROM `tmap_greeting_product` AS m
                INNER JOIN `tbl_greeting_cate` AS g ON g.cate_id = m.greeting_id
                WHERE m.activate = 'Y' && g.activate = 'Y' && m.product_id = '$product_id'";
        $greetingCateList = $this->DB->get_Sql($sql);

        foreach ($greetingCateList as $k => $greetingCate){
            $sql = "SELECT * 
                    FROM `tbl_greeting_subcate` AS s
                    WHERE s.`activate` = 'Y' AND s.`parent_cate_id` = '".$greetingCate['cate_id']."'";
            $greetingSubCateList = $this->DB->get_Sql($sql);

            foreach($greetingSubCateList as $kk => $greetingSubCate){
                $sql = "SELECT * FROM `tbl_greeting` AS g WHERE g.`activate` = 'Y' AND g.`subcate_id` = '".$greetingSubCate['subcate_id']."'";
                $greetingSubCateList[$kk]['greetingList'] = $this->DB->get_Sql($sql);
            }

            $greetingCateList[$k]['subcateList'] = $greetingSubCateList;
        }

        $product['greetingCateList'] = $greetingCateList;
        //////////// Greeting END   ////////////

        //////////// Card START ////////////
        $sql = "SELECT *
                FROM `tmap_product_card` AS m
                LEFT JOIN `tbl_product_card` AS c ON m.card_id = c.card_id && c.`activate` = 'Y'
                WHERE m.activate = 'Y' AND m.product_id = '$product_id'";
        $cardList = $this->DB->get_Sql($sql);

        $product['cardList'] = $cardList;
        //////////// Card END   ////////////

        //////////// Item START   //////////
        // copy version
        $sql = "SELECT ig.*, pig.qty, pig.is_product_page_show_qty, pig.is_force_checkout, pig.product_item_id
                FROM `tbl_item_grp` AS ig
                LEFT JOIN  `tbl_product_item_group` AS pig ON pig.`grp_id` = ig.`grp_id` AND pig.`activate` = 'Y'
                WHERE pig.`product_id` = '$product_id'";
        $item_group_list = $this->DB->get_Sql($sql);
        $cost = 0;
        $boolProductAva = true;
        foreach ($item_group_list as $k => $grp){
            $storage_qty = 0;
            $ava_qty = 0;
            $grp_id = $grp['grp_id'];

            // echo "----------------- grp_id: $grp_id -----------------";
            $sql = "SELECT igc.*, i.inhouse_name_tc, i.inhouse_name_en, i.gift_name_tc, i.gift_name_en, i.zuri_name_tc, i.zuri_name_en, i.ltp_name_tc, i.ltp_name_en, i.item_img, i.sku, i.qty as storage_qty, i.various_item_id, igc.qty as consume, i.is_unlimited_qty, igc.remarks as item_remarks, IFNULL(i.remarks,'') AS remarks
                    FROM `tbl_item_grp_content` AS igc
                    LEFT JOIN `tbl_item` AS i ON i.item_id = igc.item_id
                    WHERE igc.activate = 'Y' AND igc.grp_id = '$grp_id'";  
            $item_list = $this->DB->get_Sql($sql);
            $bool_client_optional = 'N';

            $boolItemGrpAva = false;
            foreach($item_list as $kk => $item){
                if ($item['is_client_optional'] == 'Y'){
                    $bool_client_optional = 'Y';
                }

                $bool_parent_exist = false;    
                $itemDtl = $itemCls->getItemDtl($item['item_id']);
                
                $itemDtl['display_name'] = $itemDtl[$prefix . "_name_" . $this->lang];
                $item_list[$kk]['display_name'] = $itemDtl[$prefix . "_name_" . $this->lang];
                $item_list[$kk]['item'] = $itemDtl;
                $i_ava_qty = floor($itemDtl['ava_qty'] / $item['consume']);
                if ($i_ava_qty >0){
                    $ava_qty += $i_ava_qty;
                } else {
                    $i_ava_qty = 0;
                }
                $item_list[$kk]['i_ava_qty'] = $i_ava_qty;
              
                if ($item['various_item_id']){
                    foreach($item_list as $j){
                        if ($item['various_item_id'] == $j['item_id']) {
                            $bool_parent_exist = true;
                        }
                    }
                }
                if (!$bool_parent_exist){
                    $storage_qty += $item['storage_qty'];
                }
                if ($kk == 0){
                    $cost += $itemDtl['cost'] * $item['consume'];
                }

                ///// Check out of stock START /////

                if($item['is_unlimited_qty'] == 'Y' || $i_ava_qty >= $grp['qty']){
                    $boolItemGrpAva = true;
                }

                ///// Check out of stock END ///////
            }

            ///// Check out of stock START /////

            // echo $boolItemGrpAva;
            if(!$boolItemGrpAva){
                $item_group_list[$k]["group_out_of_stock"] = "Y";
                if ($grp['is_force_checkout'] == 'N'){

                    $boolProductAva = false;
                }
            } else {
                $item_group_list[$k]["group_out_of_stock"] = "N";
            }

            ///// Check out of stock END ///////


            $item_group_list[$k]['len'] = sizeof($item_list);
            if (sizeof($item_list) == 1 ){
                //single item 
                $item_group_list[$k]['display_name'] = $item_list[0][$prefix . "_name_" . $this->lang];
            } else {
                $item_group_list[$k]['display_name'] = $grp[$prefix . "_name_" . $this->lang];
            }

            
            $item_group_list[$k]['is_client_optional'] = $bool_client_optional;
            $item_group_list[$k]['storage_qty'] = $storage_qty;
            $item_group_list[$k]['item_list'] = $item_list;
            $item_group_list[$k]['ava_qty'] = $ava_qty;

            // echo "<hr>";
        }
        
        $product['cost'] = $cost;
        $product['item_group_list'] = $item_group_list;


        $act_price = $product['sp_price'] ? $product['sp_price'] : $product['price'];
        $product['act_price'] = $act_price;
        if ($act_price && $cost){
            $product['act_profit_ratio'] = $this->parse_percent(($act_price - $cost) / $cost) . '%';
        } else {
            $product['act_profit_ratio'] = "N/A";
        }

        if (!$boolProductAva || $product['sales_status'] == '40_out_of_stock'){
            $product['is_out_of_stock'] = "Y";
        } else {
            $product['is_out_of_stock'] = "N";
        }
        //////////// Item END     //////////
        
        
        //////////// FRONT END STATUS START //////////

        $status_code = explode("_", $product['sales_status'])[0] ;
        if ($status_code < 30){
            // status == 10_to_be_confirm || 20_not_for_sale ? dont show at the front end
            $frontend_status = "10_dont_show";
        } else {
            if ($product['sales_status'] == "40_out_of_stock"){
                $frontend_status = "40_out_of_stock"; 
            } else if ($product['is_force_checkout_valid'] == 'Y'){
                $frontend_status = "30_on_sales"; 
            } else if ($product['is_out_of_stock'] == 'Y'){
                $frontend_status = "40_out_of_stock";
            } else {
                $frontend_status = "30_on_sales"; 
            }
        }
        $product['frontend_status'] = $frontend_status;

        //////////// FRONT END STATUS END   //////////

        return $product;
    }

    function isProductInStock($product_id){
        // checking for product in stock or not
        // $product_id: PK

        // from sql: find out the sku_id which auto replace is on and existing item qty is not enough to consume for the product || replace period is on and within the period 
        $sql = "SELECT if(pic.consume>s.ava_qty, 'N', 'Y') AS isInStock, s.qty AS inStock, pic.*
                FROM (
                    SELECT ic.type, ic.content_id, ic.qty AS consume, ic.ref_id, s.sku_val, ic.show_in_web, 
                        IF(i.replace_sku_id IS NOT NULL AND i.replace_sku_id != ''
                                AND (
                                    (i.is_replace = 'Y' AND ic.qty > s.ava_qty)
                                    OR 
                                    (i.is_replace_period = 'Y' AND DATE_FORMAT(i.replace_period_start,  '%Y-%m-%d') <  DATE_FORMAT(CURRENT_TIMESTAMP, '%Y-%m-%d') AND DATE_FORMAT(i.replace_period_end,  '%Y-%m-%d') >  DATE_FORMAT(CURRENT_TIMESTAMP, '%Y-%m-%d'))
                                )
                            , ss.sku_id
                            , s.sku_id
                        ) AS sku_id
                    FROM `tbl_product_item_content` AS ic 
                    LEFT JOIN `tbl_item_grp_content` AS igc ON igc.grp_id = ic.ref_id AND igc.`activate` ='Y' AND ic.type = 'grp_item'
                    LEFT JOIN `tbl_sku` AS s ON s.activate = 'Y' AND ((ic.ref_id = s.sku_id AND ic.type = 'sku') OR (igc.sku_id = s.sku_id AND ic.type = 'grp_item'))
                    LEFT JOIN `tbl_item` AS i ON s.item_id = i.item_id AND i.activate = 'Y'
                    LEFT JOIN `tbl_sku` AS ss ON ss.sku_id = i.replace_sku_id
                    WHERE ic.`activate` = 'Y' AND ic.product_id = '$product_id'
                    GROUP BY ic.content_id
                ) AS pic 
                LEFT JOIN `tbl_sku` AS s ON s.sku_id = pic.sku_id
                LEFT JOIN `tbl_item` AS i ON i.item_id = s.item_id AND i.activate='Y' ";

        // find the group_item which contain at least one out of stock item.
        $itemList = $this->DB->get_Sql($sql);
                                
        foreach ($itemList as $item){
            if ($item['isInStock'] == 'N')
                return false;
        }
        return true;
    }

    function getAddonProductList($product_id, $shop_id){ // void?
        $prefix = $this->getPrefix($shop_id);
        // $sql = "SELECT pac.*, GROUP_CONCAT(m.product_id) AS product_id_arr, pac.name_".$this->lang." AS name
        //         FROM `tbl_product_addon_cate` AS pac
        //         LEFT JOIN `tmap_product_addon_cate` AS m ON pac.addon_cate_id = m.addon_cate_id AND m.activate = 'Y' 
        //         WHERE pac.activate ='Y' AND pac.product_id = '$product_id' AND pac.shop_id = '$shop_id'
        //         GROUP BY pac.addon_cate_id";
        // $cateList = $this->DB->get_Sql($sql);
        $sql = "SELECT m.*, GROUP_CONCAT(m.product_id) AS product_id_arr, pac.".$prefix."_name_".$this->lang." AS name 
                FROM `tmap_product_addon` AS m 
                LEFT JOIN `tbl_product_addon_cate` AS pac ON pac.addon_cate_id = m.addon_id 
                WHERE m.activate ='Y' AND m.product_id = '$product_id'"; 

                
        $addonList = $this->DB->get_Sql($sql);
        
        if ($addonList){
            foreach($addonList as $k => $addon){
                if ($addon['product_id_arr']){
                    $addonList[$k]['productList'] = $this->getProductList(1, ["product_id_list" => $addon['product_id_arr']]);
                }
            }
        }
    
        return $addonList;
    }

    function getProductDtlByCode($product_code, $shop_id){
        $prefix = $this->getPrefix($shop_id);
        $sql = "SELECT product_id FROM `tbl_product` WHERE `activate` = 'Y' AND $prefix" . "_product_code = '$product_code'";
        $product_id = $this->DB->get_One($sql);

        return $this->getProductDtl($shop_id, $product_id);
    }

    function getItemListPure($src){ //item list in sku_id and qty only
        $data = [];
        foreach($src as $v){
            array_push($data,[
                "sku_id" => $v['sku_id'],
                "qty" => $v['consume']
            ]);          
        }

        return $data;
    }

    function getItemList($product_id, $prefix){
        if ($this->isAdmin){

            if (!$product_id)
                return null;
            
            $sql = "SELECT * 
                FROM(
                    SELECT ic.content_id, ref_id, g.adm_name, s.`$prefix"."_name_tc` as name_tc, s.`$prefix"."_name_en` as name_en, ic.qty, gc.sku_id, ic.type, s.cost, i.item_img, ic.show_in_web, ic.sort
                    FROM `tbl_product_item_content` AS ic  
                    LEFT JOIN `tbl_item_grp` AS g ON g.grp_id = ic.ref_id AND g.`activate` = 'Y'
                    LEFT JOIN `tbl_item_grp_content` AS gc ON gc.grp_id = g.grp_id AND gc.`activate` = 'Y'
                    LEFT JOIN `tbl_sku` AS s ON s.sku_id = gc.sku_id  AND s.`activate` = 'Y'
                    INNER JOIN `tbl_item` AS i ON i.item_id = s.item_id
                    WHERE ic.activate ='Y' AND ic.product_id in (". $product_id .") AND ic.type = 'grp_item'
                    GROUP BY g.grp_id
                    UNION
                    SELECT ic.content_id, s.sku_id AS ref_id, s.sku_val AS adm_name, s.`$prefix"."_name_tc` as name_tc, s.`$prefix"."_name_en` as name_en, ic.qty, s.sku_id, ic.type, s.cost, i.item_img, ic.show_in_web, ic.sort
                    FROM `tbl_product_item_content` AS ic  
                    INNER JOIN `tbl_sku` AS s ON s.sku_id = ic.ref_id AND s.`activate` = 'Y'
                    INNER JOIN `tbl_item` AS i ON i.item_id = s.item_id AND i.activate = 'Y'
                    INNER JOIN `tmap_item_shop` AS m ON i.item_id = m.item_id AND m.activate = 'Y'
                    WHERE ic.activate ='Y' AND ic.product_id in (". $product_id .") AND ic.type = 'sku'
                    GROUP BY sku_id
                ) AS src 
                ORDER BY sort";
            $itemList = $this->DB->get_Sql($sql);

            require_once ROOT.'/include/class/item.class.php';
            $itemCls = new item($this->DB, $this->lang, $this->isAdmin);
            // get item group detail 
            foreach ($itemList as $k => $item){
                if ($item['type'] == 'grp_item'){
                    $itemList[$k]['itemGrpDtl'] = $itemCls->getItemGroupDtl($item['ref_id']);
                }
            }
            

        
        } else {

            $product_id_arr = explode(",", $product_id);
            
            $itemList = [];
            foreach ($product_id_arr as $pid){
                $productGrpArr = [];
                $sql = "SELECT if(pic.consume>s.ava_qty, 'N', 'Y') AS isInStock, s.qty AS inStock, pic.*
                        FROM (
                            SELECT ic.type, ic.content_id, ic.qty AS consume, ic.$prefix" . "_name_".$this->lang." AS `name`, ic.ref_id, s.sku_val, ic.show_in_web, 
                                IF(i.replace_sku_id IS NOT NULL AND i.replace_sku_id != ''
                                        AND (
                                            (i.is_replace = 'Y' AND ic.qty > s.ava_qty)
                                            OR 
                                            (i.is_replace_period = 'Y' AND DATE_FORMAT(i.replace_period_start,  '%Y-%m-%d') <  DATE_FORMAT(CURRENT_TIMESTAMP, '%Y-%m-%d') AND DATE_FORMAT(i.replace_period_end,  '%Y-%m-%d') >  DATE_FORMAT(CURRENT_TIMESTAMP, '%Y-%m-%d'))
                                        )
                                    , ss.sku_id
                                    , s.sku_id
                                ) AS sku_id
                            FROM `tbl_product_item_content` AS ic 
                            LEFT JOIN `tbl_item_grp_content` AS igc ON igc.grp_id = ic.ref_id AND igc.`activate` ='Y' AND ic.type = 'grp_item'
                            LEFT JOIN `tbl_sku` AS s ON s.activate = 'Y' AND ((ic.ref_id = s.sku_id AND ic.type = 'sku') OR (igc.sku_id = s.sku_id AND ic.type = 'grp_item') )
                            LEFT JOIN `tbl_item` AS i ON s.item_id = i.item_id AND i.activate = 'Y'
                            LEFT JOIN `tbl_sku` AS ss ON ss.sku_id = i.replace_sku_id
                            WHERE ic.`activate` = 'Y' AND ic.product_id = '$pid'
                            GROUP BY ic.content_id
                        ) AS pic 
                        LEFT JOIN `tbl_sku` AS s ON s.sku_id = pic.sku_id
                        LEFT JOIN `tbl_item` AS i ON i.item_id = s.item_id AND i.activate='Y'";

                $itemList = $this->DB->get_Sql($sql);

                require_once ROOT.'/include/class/item.class.php';
                $itemCls = new item($this->DB, $this->lang, $this->isAdmin);
                // get item group detail 
                foreach ($itemList as $k => $item){
                    if ($item['type'] == 'grp_item'){
                        $itemList[$k]['itemGrpDtl'] = $itemCls->getItemGroupDtl($item['ref_id']);
                    }
                }
            }
            
        }


        return $itemList;
    }

    function getRelatedProductList($shop_id, $product_id, $count = 4){
        // get src_product_id_arr
        $relatedProductList = [];


        $sql = "SELECT src_product_id FROM tmap_combo_product WHERE product_id = '$product_id' AND activate ='Y'";
        $tres = $this->DB->get_Sql($sql);

        if ($tres){
            foreach ($tres as $i){
                array_push($relatedProductList, $this->getProductDtl($shop_id, $i['src_product_id']));
                $count--;
            }
        }

        $product = $this->getProductDtl($shop_id, $product_id);
        $cateList = $product['cate'];
        
        //subcate_id
        $cate_id_arr = [];
        foreach ($cateList as $cate){
            foreach ($cate['subcate'] as $subcate){
                array_push($cate_id_arr, $subcate['cate_id']);
            }
        }


        if ($cate_id_arr)
            $filter['subcate_id'] = implode(",", $cate_id_arr);
        
        $limiter = ["limit" => $count, "page" => 1];
        $sorting = ["sort" => "rand()"];
        $relatedProductList = array_merge($relatedProductList, $this->getProductList($shop_id, $filter, $limiter, $sorting));
     

        
        return $relatedProductList;
    }

    function getProductDtl($shop_id, $product_id){
        if ($shop_id && $product_id){
            return $this->getProductList($shop_id, ["product_id" => $product_id], $this->DB->getUnique)[0];
        } else {
            return null;
        }
    }

    function getOutOfStockList($shop_id = 1){
        $prefix = $this->getPrefix($shop_id);
        $productList = $this->getProductList($shop_id, ["frontend_status" => "40_out_of_stock"]);
        return $productList;
    }

    function addProductCate($product_id_arr, $cate_id, $shop_id){
        foreach($product_id_arr as $product_id){
            $sql = "SELECT * 
                    FROM `tmap_cate_product`
                    WHERE product_id = $product_id AND cate_id = $cate_id AND shop_id = $shop_id";
            $map = $this->DB->get_Sql($sql)[0];

            if ($map){
                $sql = "UPDATE `tmap_cate_product` 
                        SET activate = 'Y' 
                        WHERE map_id = " . $map['map_id'];
                $this->DB->update($sql);
            } else {
                $this->DB->insert_db('tmap_cate_product', [
                    "product_id" => $product_id, 
                    "cate_id" => $cate_id,
                    "shop_id" => $shop_id
                ]);
            }
        }
    }

    function editProductItemGroup($data){
        if (isset($data['product_item_id']) && $data['product_item_id'] != 0) {
			$id = $this->DB->update_Sql("tbl_product_item_group", "product_item_id", $data);
		} else {
			$id = $this->DB->insert_db("tbl_product_item_group", $data);
		} 

        return $id;
    }
}
