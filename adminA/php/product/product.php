<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;
		
		switch ($this->gthis->submode) {
			case 'quotation':
				$result_array = $this->quotation();
				break;
			case 'subcateDtl':
				$result_array = $this->subcateDtl();
				break;
			case 'productList':
				$result_array = $this->productList();
				break;
			case 'productDtl':
				$result_array = $this->productDtl();
				break;
			case 'addonGroupList':
				$result_array = $this->addonGroupList();
				break;
			case 'addonList':
				$result_array = $this->addonList();
				break;
			case 'addonDtl':
				$result_array = $this->addonDtl();
				break;
			case 'productTplList':
				$result_array = $this->productTplList();
				break;
			case 'productTplDtl':
				$result_array = $this->productTplDtl();
				break;
			case 'cutoffTplList':
				$result_array = $this->cutoffTplList();
				break;
			case 'cutoffTplDtl':
				$result_array = $this->cutoffTplDtl();
				break;
			case 'itemGrpList':
				$result_array = $this->itemGrpList();
				break;
			default:
				array_push($this->gthis->breadcrumb, [
					"code" => $this->gthis->submode,
					"url" => "admin/product/" . $this->gthis->submode,
					"page_title" => $this->gthis->RMBC->getReturnString($this->gthis->submode)
				]);
				break;
		}

		$this->gthis->assign('result_array',$result_array);
	}

	function quotation(){
		require_once ROOT.'/include/class/shop.class.php';
		$shopCls = new shop($this->DB, $this->lang, $this->isAdmin);
		
		$shop_id = $this->gthis->param(["key" => "shop_id", "request" => "session", "type" => "int"],);

		$res['shop'] = $shopCls->getShopDtl($shop_id);

		return $res;
	}

	function itemGrpList(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/itemCate.class.php';
		$itemCateCls = new itemCate($this->DB, $this->lang, $this->isAdmin);
		$res['item_cate_list'] = $itemCateCls->getItemCateList();

		array_push($this->gthis->breadcrumb, [
			"code" => "itemGrpList",
			"url" => "admin/product/itemGrpList",
			"page_title" => $this->gthis->RMBC->getReturnString('itemGrpList')
		]);

		return $res;
	}

	function cutoffTplDtl(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/product.class.php';
		$product = new product($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/cate.class.php';
		$cateCls = new cate($this->DB, $this->lang, $this->isAdmin);

		$cutoff_tpl_id = $this->gthis->parm_sql('request', 'cutoff_tpl_id', 'N');
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$tplDtl = $product->getCutoffTplDtl($cutoff_tpl_id, $shop_id);
		
		$res['category_list'] = $cateCls->getCateList($shop_id);
		$res['product_list'] = $product->getProductList($shop_id);
		
		$res['cutoffTplDtl'] = $tplDtl;

		array_push($this->gthis->breadcrumb, [
			"code" => "cutoffTplList",
			"url" => "admin/product/cutoffTplList",
			"page_title" => $this->gthis->RMBC->getReturnString('cutoffTplList')
		],[
			"code" => "productTplDtl",
			"url" => "admin/product/productTplDtl?tpl_id=" . ($tpl_id ? $tpl_id : "0"),
			"page_title" => $cutoff_tpl_id ? $tplDtl['adm_name'] : $this->gthis->RMBC->getReturnString('addCutoffTpl')
		]);
		return $res;
	}

	function cutoffTplList(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/product.class.php';
		$product = new product($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_in('request', 'skey');
		$page = $this->gthis->parm_sql('request', 'page');
		$limit = $this->gthis->parm_sql('request', 'limit');
		$sort = $this->gthis->parm_sql('request', 'sort');
		$order = $this->gthis->parm_sql('request', 'order');

		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "cutoff_tpl_id",
			"order" => $order ? $order : "ASC"
		];

		$res['getCutoffTplList'] = $product->getCutoffTplList($shop_id, $filter, $limiter, $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "cutoffTplList",
			"url" => "admin/product/cutoffTplList",
			"page_title" => $this->gthis->RMBC->getReturnString('cutoffTplList')
		]);

		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($product->getCutoffTplList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
	}

	function productTplDtl(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/product.class.php';
		$product = new product($this->DB, $this->lang, $this->isAdmin);

		$tpl_id = $this->gthis->parm_sql('request', 'tpl_id', 'N');
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$tplDtl = $product->getProductTplDtl($tpl_id, $shop_id);
		
		$res['productTplDtl'] = $tplDtl;

		array_push($this->gthis->breadcrumb, [
			"code" => "productTplList",
			"url" => "admin/product/productTplList",
			"page_title" => $this->gthis->RMBC->getReturnString('productTplList')
		],[
			"code" => "productTplDtl",
			"url" => "admin/product/productTplDtl?tpl_id=" . ($tpl_id ? $tpl_id : "0"),
			"page_title" => $tpl_id ? $tplDtl['adm_name'] : $this->gthis->RMBC->getReturnString('addProductTpl')
		]);
		return $res;
	}

	function productTplList(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/product.class.php';
		$product = new product($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_in('request', 'skey');
		$page = $this->gthis->parm_sql('request', 'page');
		$limit = $this->gthis->parm_sql('request', 'limit');
		$sort = $this->gthis->parm_sql('request', 'sort');
		$order = $this->gthis->parm_sql('request', 'order');

		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "tpl_id",
			"order" => $order ? $order : "ASC"
		];

		$res['productTplList'] = $product->getProductTplList($shop_id, $filter, $limiter, $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "productTplList",
			"url" => "admin/product/productTplList",
			"page_title" => $this->gthis->RMBC->getReturnString('productTplList')
		]);

		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($product->getProductTplList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
	}

	function addonDtl(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/product.class.php';
		$product = new product($this->DB, $this->lang, $this->isAdmin);

		$addon_id = $this->gthis->parm_sql('request', 'addon_id', 'N');
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		$addonDtl = $product->getAddonDtl($shop_id, $addon_id);
		$addonDtl['productList'] = $product->getProductList($shop_id, ["original_addon_id" => $addonDtl['addon_id']]);

		$res['addonDtl'] = $addonDtl;

		$res['productList'] = $product->getProductList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "addonList",
			"url" => "admin/product/addonList",
			"page_title" => $this->gthis->RMBC->getReturnString('addonList')
		],[
			"code" => "addonDtl",
			"url" => "admin/product/addonDtl?addon_id=" . ($addon_id ? $addon_id : "0"),
			"page_title" => $addon_id ? $res['addonDtl']['name_tc'] : $this->gthis->RMBC->getReturnString('addAddon')
		]);
		return $res;
	}

	function addonList(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/product.class.php';
		$product = new product($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$skey = $this->gthis->parm_in('request', 'skey');
		$page = $this->gthis->parm_sql('request', 'page');
		$limit = $this->gthis->parm_sql('request', 'limit');
		$sort = $this->gthis->parm_sql('request', 'sort');
		$order = $this->gthis->parm_sql('request', 'order');

		$filter = [
			"skey" => $skey ? $skey : null,
		];
		
		$limiter = [
			"page" => $page ? $page : 1,
			"limit" => $limit ? $limit : 50
		];

		$sorting = [
			"sort" => $sort ? $sort : "addon_id",
			"order" => $order ? $order : "ASC"
		];

		$res['addonList'] = $product->getAddonList($shop_id, $filter, $limiter, $sorting);

		array_push($this->gthis->breadcrumb, [
			"code" => "addonList",
			"url" => "admin/product/addonList",
			"page_title" => $this->gthis->RMBC->getReturnString('addonList')
		]);

		$res['page_config'] = [
			"skey" => $skey,
			"ttlCount" => sizeof($product->getAddonList($shop_id, $filter))
		];
		$res['page_config'] = array_merge($res['page_config'], $limiter);
		$res['page_config'] = array_merge($res['page_config'], $sorting);
		return $res;
	}

	function addonGroupList(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		array_push($this->gthis->breadcrumb, [
			"code" => "addonGroupList",
			"url" => "admin/product/addonGroupList",
			"page_title" => $this->gthis->RMBC->getReturnString('addonGroupList')
		]);
		return null;
	}

	function productList(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		require_once ROOT.'/include/class/category.class.php';
		$categoryCls = new category($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/card.class.php';
		$cardCls = new card($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/greeting.class.php';
		$greetingCls = new greeting($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

		$res['product_tpl_list'] = $productCls->getProductTplList($shop_id);
		$res['category_list'] = $categoryCls->getCate($shop_id);
		$res['card_group_list'] = $cardCls->getCardGroupList();
		$res['greeting_group_list'] = $greetingCls->getGreetingGroupList($shop_id);
		$res['addon_group_list'] = $productCls->getAddonGroupList($shop_id);

		array_push($this->gthis->breadcrumb, [
			"code" => "productList",
			"url" => "admin/product/productList",
			"page_title" => $this->gthis->RMBC->getReturnString('productList')
		]);
		
		return $res;
	}

	function productDtl(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/delivery.class.php';
		$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/shop.class.php';
		$shopCls = new shop($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/audit.class.php';
		$auditCls = new audit($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/card.class.php';
		$cardCls = new card($this->DB, $this->lang, $this->isAdmin);
		require_once ROOT.'/include/class/greeting.class.php';
		$greetingCls = new greeting($this->DB, $this->lang, $this->isAdmin);
		
		
		$data = $this->gthis->paramBuilder([
			["key" => "shop_id", "request" => "session", "type" => "int"],
			["key" => "product_code"],
			["key" => "product_id", "type" => "int"],
			["key" => "copy", "type" => "NY"],
			["key" => "item_id", "type" => "int"],
		]);
		$shop_id = $data['shop_id'];
	
		if (!$data['product_id'] && $data['product_code']){
			$productDtl = $productCls->getProductDtlByCode($data['product_code'], $shop_id);
			$data['product_id'] = $res['productDtl']['product_id'];
		} else {
			$productDtl = $productCls->getProductDtl($shop_id, $data['product_id']);
		}
		
		
		$res['card_group_list'] = $cardCls->getCardGroupList();
		$res['greeting_group_list'] = $greetingCls->getGreetingGroupList($shop_id);
		$res['addon_group_list'] = $productCls->getAddonGroupList($shop_id);
		$res['productTplList'] = $productCls->getProductTplList($shop_id);
		$res['cutoffTplList'] = $productCls->getCutoffTplList($shop_id);
		$res['deliveryGroupList'] = $deliveryCls->getDeliveryGroupList();
		$res['shop'] = $shopCls->getShopDtl($shop_id);
		if ($data['product_id']){
			$res['log_list'] = $auditCls->getProductItemContentLog($data['product_id']);
		}

		if ($data['copy'] == 'Y'){
			$productDtl['product_id'] = 0;
		}

		if (!$productDtl && $data['item_id']){
			require_once ROOT.'/include/class/item.class.php';
			$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

			$prefix = $itemCls->getPrefix($data['shop_id']);

			$itemDtl = $itemCls->getItemDtl($data['item_id']);
			$productDtl['product_name_tc'] = $itemDtl[$prefix.'_name_tc'];
			$productDtl['product_name_en'] = $itemDtl[$prefix.'_name_en'];
			$productDtl['img'][0]['img'] = $itemDtl['item_img'];
		}


		$res['productDtl'] = $productDtl;

		array_push($this->gthis->breadcrumb, [
			"code" => "productList",
			"url" => "admin/product/productList",
			"page_title" => $this->gthis->RMBC->getReturnString('productList')
		],[
			"code" => "productDtl",
			"url" => "admin/product/productDtl?product_id=" . ($product_id ? $product_id : "0"),
			"page_title" => $data['product_id'] ? $res['productDtl']['product_name_tc'] : $this->gthis->RMBC->getReturnString('addProduct')
		]);
		
		return $res;
	}

	// function productDtl(){
	// 	if (intval($_SESSION['right']['product']) < 30){
	// 		return array("right"=>"Failure");
	// 	}
	// 	require_once ROOT.'/include/class/product.class.php';
	// 	$productCls = new product($this->DB, $this->lang, $this->isAdmin);
	// 	require_once ROOT.'/include/class/delivery.class.php';
	// 	$deliveryCls = new delivery($this->DB, $this->lang, $this->isAdmin);
	// 	require_once ROOT.'/include/class/shop.class.php';
	// 	$shopCls = new shop($this->DB, $this->lang, $this->isAdmin);
	// 	require_once ROOT.'/include/class/audit.class.php';
	// 	$auditCls = new audit($this->DB, $this->lang, $this->isAdmin);
	// 	require_once ROOT.'/include/class/item.class.php';
	// 	$itemCls = new item($this->DB, $this->lang, $this->isAdmin);

	// 	$product_id = $this->gthis->parm_sql('request', 'product_id', 'N');
	// 	$src_product_id = $this->gthis->parm_sql('request', 'src_product_id', 'N');
	// 	$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
	// 	$sku_id = $this->gthis->parm_sql('request', 'sku_id');

	// 	if (str_contains($product_id, ",")){
	// 		$res['batchEditMode'] = 'Y';
	// 		$res['productDtl']['product_id']= $product_id;
	// 	} else {
	// 		$res['productDtl'] = $productCls->getProductDtl($product_id, $shop_id, $src_product_id);
	// 	}

	// 	$res['productTplList'] = $productCls->getProductTplList($shop_id);
	// 	$res['cutoffTplList'] = $productCls->getCutoffTplList($shop_id);
	// 	$res['custDeliList'] = $deliveryCls->getCustDeliGrpList($shop_id);
	// 	$res['shop'] = $shopCls->getShopDtl($shop_id);
	// 	$res['skuDtl'] = $itemCls->getSkuDtl($sku_id);


	// 	array_push($this->gthis->breadcrumb, [
	// 		"code" => "productList",
	// 		"url" => "admin/product/productList",
	// 		"page_title" => $this->gthis->RMBC->getReturnString('productList')
	// 	],[
	// 		"code" => "productDtl",
	// 		"url" => "admin/product/productDtl?product_id=" . ($product_id ? $product_id : "0"),
	// 		"page_title" => $product_id ? $res['productDtl']['product_name_tc'] : $this->gthis->RMBC->getReturnString('addProduct')
	// 	]);
	// 	return $res;
	// }

	function subcateDtl(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/cate.class.php';
		$cate = new cate($this->DB, $this->lang, $this->isAdmin);

		
		$shop_id = $this->gthis->parm_sql('session', 'shop_id');
		$parent_cate_id = $this->gthis->parm_sql('request', 'parent_cate_id');
		$res['parent_cate_id'] = $parent_cate_id;
		$res['cateDtl'] = $cate->getCateDtl($parent_cate_id);
		
		$cate_id = $this->gthis->parm_sql('request', 'cate_id', 'N');
		$res['subcateDtl'] = $cate->getCateDtl($cate_id);
		
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);
		$res['subCateDtl']['productList'] = $productCls->getProductList($shop_id, ["subcate_id" => $res['subcateDtl']['cate_id']]);

		array_push($this->gthis->breadcrumb, [
			"code" => "cateList",
			"url" => "admin/product/cateList",
			"page_title" => $this->gthis->RMBC->getReturnString('cateList')
		],[
			"code" => "cateDtl",
			"url" => "admin/product/cateDtl?cate_id=$parent_cate_id",
			"page_title" => $res['cateDtl']['name_tc']
		],[
			"code" => "subcateDtl",
			"url" => "admin/product/subcateDtl?cate_id=" . ($cate_id ? $cate_id : "0"),
			"page_title" => $cate_id ? $res['subcateDtl']['name_tc'] : $this->gthis->RMBC->getReturnString('addSubcate')
		]);
		return $res;
	}
}
?>