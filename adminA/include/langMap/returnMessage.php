<?php
class returnMessageAdmin {
	public $csv;
	public $csv_string;
	public $csv_js_string;
	public $lang;
	public $string_file_path;
	
	public function __construct($lang,$stringFilePath=null) {
		$this->lang = $lang;
		$this->string_file_path = $stringFilePath;
		$this->prepare_csv($this->string_file_path);
	}
	
	function getReturnCode($code){
		$result = $this->csv[$code];
		//$result['code'] = $code;
		//$result['csv'] = $this->csv;
		return $result;
	}

	function getReturnString($string){
		$result = $this->csv_string[$string];
		//$result['code'] = $string;
		//$result['csv'] = $this->csv_string;
		return $result;
	}


	function prepare_csv($path=null){
		// 載入 csv 表格 並轉為直接調用格式
		if(isset($path)){
			$csv = array_map('str_getcsv', file($path));
		} else {
			$csv = array_map('str_getcsv', file(ROOT.'/adminA/include/langMap/returnCodeMap.csv'));
		}
		$this->csv = array();
		$key = array();
		$this->csv_string = array();
		$this->csv_js_string = array();
		$key_string = array();

		// by code
		foreach($csv as $k=>$i){
			if($k==0){
				foreach($i as $a=>$b){
					array_push($key, $b);
				}	
			}
			
			if($k>0){
				$ni = array();
				foreach($i as $c=>$d){
					if($this->lang == $key[$c]){
						$ni['lang'] = $this->lang;	
						$ni['msg'] = $d;	
					}
				}
				//$this->csv[$i[0]] = $ni;
				$this->csv[$i[0]] = $ni['msg'];
			}
		}

		// by string msg
		foreach($csv as $k=>$i){
			if($k==0){
				foreach($i as $a=>$b){
					array_push($key_string, $b);
				}	
			}
			
			if($k>0){
				$ni = array();
				foreach($i as $c=>$d){
					if($this->lang == $key_string[$c]){
						$ni['lang'] = $this->lang;	
						$ni['msg'] = $d;	
					}
				}
				//$this->csv[$i[1]] = $ni;
				$this->csv_string[$i[1]] = $ni['msg'];
			}
		}

		foreach($this->csv as $k=>$i){
			if(strpos($k, 'jstxt')>-1){
				$this->csv_js_string[$k] = $i;
			}
		}
	}
}
?>