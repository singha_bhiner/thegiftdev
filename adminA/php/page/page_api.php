<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->lang = $this->gthis->lang;
		$this->isAdmin = true;
		$this->DB = $g_DB;
		
		require_once ROOT.'/include/class/page.class.php';
		$this->page = new page($this->DB, $this->lang, $this->isAdmin);
		
		require_once ROOT. '/include/apiDataBuilder.class.php';
		
		switch ($this->gthis->thirdmode) {
			case 'sortPageContent':
				$this->sortPageContent();
				break;
			case 'uploadImg':
				$this->uploadImg();
				break;
			case 'editPopup':
				$this->editPopup();
				break;
			case 'sortNavCate':
				$this->sortNavCate();
				break;
			case 'sortNavPage':
				$this->sortNavPage();
				break;
			case 'sortNav':
				$this->sortNav();
				break;
			case 'addPage':
				$this->addPage();
				break;
            case 'editStyle':
                $this->editStyle();
                break;
            case 'editShop':
                $this->editShop();
                break;
			case 'editNav':	
				$this->editNav();
				break;
			case 'editPage':
				$this->editPage();
				break;
			case 'editPageContent':
				$this->editPageContent();
				break;
			case 'delSliderImg':
				$this->delSliderImg();
				break;

			case 'sortClient':
				$this->sortClient();
				break;
			case 'addClient':
				$this->addClient();
				break;
			case 'delClient':
				$this->delClient();
				break;
			
			case 'getNavList':
				$this->getNavList();
				break;
			case 'getPageList':
				$this->getPageList();
				break;
			
			default:
				new apiDataBuilder(-84, null, null, $this->lang);
				break;
		}

		$result_array['lang'] = $this->lang;
		@$this->gthis->assign('result_array',$result_array);
	}

	function getPageList(){
		if (intval($_SESSION['right']['page']) < 30){
			return array("right"=>"Failure");
		}

		require_once ROOT.'/include/class/page.class.php';
		$page = new page($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');
		$mode = $this->gthis->parm_sql('request', 'cb');

		$res['page_list'] = $page->getPageList($shop_id, $mode);
		
		new apiDataBuilder(0, $res);
	}

	function getNavList(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		$shop_id = $this->gthis->parm_sql('session', 'shop_id', 'Y');

		require_once ROOT.'/include/class/nav.class.php';
		$nav = new nav($this->DB, $this->lang, $this->isAdmin);

		$res['nav_list'] = $nav->getNavList($shop_id);

		new apiDataBuilder(0, $res);
	}

	function sortNavPage(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/nav.class.php';
		$navCls = new nav($this->DB, $this->lang, $this->isAdmin);

		$page_id_arr = $this->DB->parseIntArr($this->gthis->parm_sql('request', 'page_id_arr', 'Y'));
		$nav_id = $this->gthis->parm_sql('request', 'nav_id', 'Y');

		$res = $navCls->sortNavPage($nav_id, $page_id_arr);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function sortNavCate(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/nav.class.php';
		$navCls = new nav($this->DB, $this->lang, $this->isAdmin);

		$cate_id_arr = $this->DB->parseIntArr($this->gthis->parm_sql('request', 'cate_id_arr', 'Y'));
		$nav_id = $this->gthis->parm_sql('request', 'nav_id', 'Y');

		$res = $navCls->sortNavCate($nav_id, $cate_id_arr);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function sortPageContent(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/page.class.php';
		$pageCls = new page($this->DB, $this->lang, $this->isAdmin);

		$page_content_id_arr = $this->DB->parseIntArr($this->gthis->parm_sql('request', 'page_content_id_arr', 'Y'));
		$page_id = $this->gthis->parm_sql('request', 'page_id', 'Y');

		$res = $pageCls->sortPageContent($page_id, $page_content_id_arr);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function uploadImg(){
		$tmp_img = $this->gthis->parm_in('file', 'file');
		$res['img_path'] = $this->DB->uploadImg($tmp_img, "page_content", 0);
		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editPopup(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/page.class.php';
		$page = new page($this->DB, $this->lang, $this->isAdmin);

		$popup_id = $this->gthis->parm_sql('request', 'popup_id');
		$data = $this->gthis->paramBuilder([
			["key" => "shop_id"], 
			["key" => "location"], 
			["key" => "adm_name"], 
			["key" => "content_tc"], 
			["key" => "content_en"], 
			["key" => "expiry_datetime"], 
			["key" => "url_arr", "type" => "arr"], 
			["key" => "activate", "type" => "NY"], 
			["key" => "show_in_web", "type" => "NY"], 
			["key" => "bg_color"], 
		]);
		// $data = [];
		// $fieldArr = ["shop_id", "location", "adm_name", "content_tc", "content_en", "expiry_datetime", "url_arr", "activate", "show_in_web", "bg_color"];

		// foreach ($fieldArr as $field){
		// 	$$field = $this->gthis->parm_sql('request', $field);
		// 	switch ($field){
		// 		case 'shop_id':
		// 			if (!$$field){
		// 				new apiDataBuilder(-99, null, "$field cannot be null", $this->lang);
		// 			}
		// 			break;
		// 		case 'url_arr':
		// 			$tArr = [];

		// 			$json = json_decode(str_replace("\\", "", $$field), true);
					
		// 			foreach($json as $k => $url){

		// 				array_push($tArr, $url);
		// 			}
					
		// 			$url_arr = $tArr;
		// 			break;
		// 	}
		// 	$$field? $data[$field] = $$field: null;
		// }

		// new apiDataBuilder(0, $data);

		$res = $page->editPopup($popup_id, $data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function sortNav(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/nav.class.php';
		$nav = new nav($this->DB, $this->lang, $this->isAdmin);

		$nav_id_arr = $this->DB->parseIntArr($this->gthis->parm_sql('request', 'nav_id_arr', 'Y'));
		$shop_id = $this->gthis->parm_sql('request', 'shop_id', 'Y');

		$res = $nav->sortNav($shop_id, $nav_id_arr);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function delSliderImg(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		$slider_id = $this->gthis->parm_sql('request', 'slider_id', 'Y');
		$page_id = $this->gthis->parm_sql('request', 'page_id');
		
		$res = $this->page->delSliderImg($slider_id, $page_id);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editPageContent(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		// ini_set('display_errors', 1);
		// ini_set('display_startup_errors', 1);
		// error_reporting(E_ALL);

		$content_id = $this->gthis->parm_sql('request', 'content_id');
		// $data = [];
		// $fieldArr = ["html_tc", "html_en", "html_tc2", "html_en2","sort", "page_id", "product_id_arr", "type", "activate"];

		// foreach ($fieldArr as $field){
		// 	$$field = $this->gthis->parm_sql('request', $field);
		// 	switch ($field){
		// 	}
		// 	$$field? $data[$field] = $$field: null;
		// }

		
		$data = $this->gthis->paramBuilder([
			["key" => "html_tc"],
			["key" => "html_en"],
			["key" => "html2_tc"],
			["key" => "html2_en"],
			["key" => "sort"],
			["key" => "page_id"],
			["key" => "product_id_arr"],
			["key" => "type"],
			["key" => "activate", "type" => "NY"]
		]);


		switch($data['type']){
			case 'slider':
				$data['html_tc'] = null;
				$data['html_en'] = null;
				$data['lr_img_tc'] = null;
				$data['lr_img_en'] = null;
				$data['html_tc2'] = null;
				$data['html_en2'] = null;
				$data['lr_img_tc2'] = null;
				$data['lr_img_en2'] = null;
				$data['product_id_arr'] = null;

				$slider_arr = [];

				foreach (array_keys($_POST) as $key){
					if (strpos($key, 'slider_id') === 0){
						$idx = str_replace("slider_id_", "", $key);

						$sid = $this->gthis->parm_sql('request', 'slider_id_' .$idx, 'N');
						$cid = $this->gthis->parm_sql('request', 'content_id_' .$idx, 'N');
						$sort = $this->gthis->parm_sql('request', 'sort_' .$idx, 'N');
						$img_tc = $this->gthis->parm_in('file', 'img_tc_' .$idx, 'N');
						$img_en = $this->gthis->parm_in('file', 'img_en_' .$idx, 'N');

						array_push($slider_arr, [
							"slider_id" => $sid,
							"content_id" => $cid,
							"sort" => $sort,
							"img_tc" => $img_tc,
							"img_en" => $img_en
						]);
					}
				}
				
				$data['slider_arr'] = $slider_arr;
				break;
			case 'productSlider':
			case 'product_grp':
				$data['lr_img_tc'] = null;
				$data['lr_img_en'] = null;
				$data['slider_arr'] = null;
				$data['html_tc2'] = null;
				$data['html_en2'] = null;
				$data['lr_img_tc2'] = null;
				$data['lr_img_en2'] = null;
				break;
			case 'left_img':
			case 'right_img':
				$lr_img_tc = $this->gthis->parm_in('file', 'lr_img_tc', 'N');
				$lr_img_en = $this->gthis->parm_in('file', 'lr_img_en', 'N');
				$lr_img_tc? $data['lr_img_tc'] = $lr_img_tc : null;
				$lr_img_en? $data['lr_img_en'] = $lr_img_en : null;
				$data['html_tc2'] = null;
				$data['html_en2'] = null;
				$data['lr_img_tc2'] = null;
				$data['lr_img_en2'] = null;
				$data['slider_arr'] = null;
				$data['product_id_arr'] = null;
				break;
			case 'html':
			case 'marquee':
				$data['html_tc2'] = null;
				$data['html_en2'] = null;
				$data['lr_img_tc2'] = null;
				$data['lr_img_en2'] = null;
				$data['lr_img_tc'] = null;
				$data['lr_img_en'] = null;
				$data['product_id_arr'] = null;
				$data['slider_arr'] = null;
				break;
			case 'mini_banner':
				$data['html_tc2'] = null;
				$data['html_en2'] = null;
				$data['lr_img_tc2'] = null;
				$data['lr_img_en2'] = null;
				$data['lr_img_tc'] = null;
				$data['lr_img_en'] = null;
				$data['product_id_arr'] = null;
				$data['slider_arr'] = null;
				$mini_banner_tc = $this->gthis->parm_in('file', 'mini_banner_tc', 'N');
				$mini_banner_en= $this->gthis->parm_in('file', 'mini_banner_en', 'N');
				$mini_banner_tc ? $data['mini_banner_tc'] = $mini_banner_tc : null;
				$mini_banner_en ? $data['mini_banner_en'] = $mini_banner_en : null;
				break;
			case 'l_img_r_product':
			case 'l_product_r_img':
				$data['html_tc2'] = null;
				$data['html_en2'] = null;
				$data['lr_img_tc2'] = null;
				$data['lr_img_en2'] = null;
				$data['html_tc'] = null;
				$data['html_en'] = null;
				$data['slider_arr'] = null;
				$lr_img_tc = $this->gthis->parm_in('file', 'lr_img_tc', 'N');
				$lr_img_en = $this->gthis->parm_in('file', 'lr_img_en', 'N');
				$lr_img_tc? $data['lr_img_tc'] = $lr_img_tc : null;
				$lr_img_en? $data['lr_img_en'] = $lr_img_en : null;
				break;
			case 'double_left_img':
			case 'double_right_img':
				$lr_img_tc = $this->gthis->parm_in('file', 'lr_img_tc', 'N');
				$lr_img_en = $this->gthis->parm_in('file', 'lr_img_en', 'N');
				$lr_img_tc? $data['lr_img_tc'] = $lr_img_tc : null;
				$lr_img_en? $data['lr_img_en'] = $lr_img_en : null;
				$lr_img_tc2 = $this->gthis->parm_in('file', 'lr_img2_tc', 'N');
				$lr_img_en2 = $this->gthis->parm_in('file', 'lr_img2_en', 'N');
				$lr_img_tc2? $data['lr_img2_tc'] = $lr_img_tc2 : null;
				$lr_img_en2? $data['lr_img2_en'] = $lr_img_en2 : null;
				$data['slider_arr'] = null;
				$data['product_id_arr'] = null;
				break;
			case 'html_slider':
				$lr_img_tc = $this->gthis->parm_in('file', 'lr_img_tc', 'N');
				$lr_img_en = $this->gthis->parm_in('file', 'lr_img_en', 'N');
				$lr_img_tc? $data['lr_img_tc'] = $lr_img_tc : null;
				$lr_img_en? $data['lr_img_en'] = $lr_img_en : null;
				$lr_img_tc2 = $this->gthis->parm_in('file', 'lr_img2_tc', 'N');
				$lr_img_en2 = $this->gthis->parm_in('file', 'lr_img2_en', 'N');
				$lr_img_tc2? $data['lr_img2_tc'] = $lr_img_tc2 : null;
				$lr_img_en2? $data['lr_img2_en'] = $lr_img_en2 : null;
				$html3_tc = $this->gthis->parm_in('request', 'html3_tc', 'N');
				$html3_en = $this->gthis->parm_in('request', 'html3_en', 'N');
				$html3_tc? $data['html3_tc'] = $html3_tc : null;
				$html3_en? $data['html3_en'] = $html3_en : null;
				$data['slider_arr'] = null;
				$data['product_id_arr'] = null;
				break;
		}
	
		$res = $this->page->editPageContent($content_id, $data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}


	function addPage(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		$shop_id = $this->gthis->parm_sql('request', 'shop_id', 'Y');
		$res = $this->page->addPage($shop_id);
		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editPage(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		require_once ROOT.'/include/class/shop.class.php';
		$shopCls = new shop($this->DB, $this->lang, $this->isAdmin);

		$page_id = $this->gthis->parm_sql('request', 'page_id');
		$data = [];
		$fieldArr = ["shop_id", "name_tc", "name_en", "url", "see_through_nav", "show_in_web", "activate"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
				case 'shop_id':
					if (!$$field){
						new apiDataBuilder(-99, null, "$field cannot be null", $this->lang);
					}
					$shopDtl = $shopCls->getShopDtl($shop_id);
					$$field? $data[$field] = $$field: null;
					break;
				case 'url':
					$$field = str_replace("http://". $shopDtl['domain'], "", str_replace("https://" . $shopDtl['domain']."/", "", $$field));
					$data[$field] = $$field;
					break;
				case 'show_in_web':
					$$field = $this->DB->parseNY($$field);
					$data[$field] = $$field;
					break;
				default:
					$$field? $data[$field] = $$field: null;
					break;
			}
		}


		$page = $this->page->getPageDtlByUri($data['url'], $data['shop_id'], $page_id);
		if ($page){
			new apiDataBuilder(16,$page, null, $this->lang);
		}
		$res = $this->page->editPage($page_id, $data);

		new apiDataBuilder(0, $res, null, $this->lang);
	}

	function editNav(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
		require_once ROOT.'/include/class/nav.class.php';
		$navCls = new nav($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "shop_id", "request" => "session"],
			["key" => "nav_list", "type" => "json"]
		]);

		$navCls->editNavList($data['shop_id'], $data['nav_list']);

		new apiDataBuilder(0, $data);

	}

	// function editNav(){
	// 	if (intval($_SESSION['right']['page']) < 50){
	// 		new apiDataBuilder(-62, null, null, $this->lang);
	// 	}
	// 	require_once ROOT.'/include/class/nav.class.php';
	// 	$nav = new nav($this->DB, $this->lang, $this->isAdmin);

	// 	$nav_id = $this->gthis->parm_sql('request', 'nav_id', 'N');
	// 	$data = [];
	// 	$fieldArr = ["shop_id", "name_tc", "name_en", "cate_id_arr", "page_id_arr", "link", "activate"];

	// 	foreach ($fieldArr as $field){
	// 		$$field = $this->gthis->parm_sql('request', $field);
	// 		switch ($field){
	// 			case 'shop_id':
	// 				if (!$$field){
	// 					new apiDataBuilder(-99, null, "$field cannot be null", $this->lang);
	// 				}
	// 				break;
	// 			case "cate_id_arr":
	// 			case "page_id_arr": 
	// 			case "link":
	// 				$data[$field] = $$field;
	// 				break;
	// 		}
	// 		$$field? $data[$field] = $$field: null;
	// 	}

	// 	$res = $nav->editNav($nav_id, $data);

	// 	new apiDataBuilder(0, $res, null, $this->lang);
	// }

    function editStyle(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
        require_once ROOT.'/include/class/shop.class.php';
        $shop = new shop($this->DB, $this->lang, $this->isAdmin);

		$shop_id = $this->gthis->parm_sql('request', 'shop_id');
		$data = [];
		$fieldArr = ["nav_color", "product_list_img_1_link", "product_list_img_2_link", "product_list_img_3_link"];

		foreach ($fieldArr as $field){
			$$field = $this->gthis->parm_sql('request', $field);
			switch ($field){
				case 'shop_id':
					if (!$$field){
						new apiDataBuilder(-99, null, "$field cannot be null", $this->lang);
					}
					break;
				case 'nav_color':
					$$field = substr($$field, 0, -2);
					break;
			}
			$$field? $data[$field] = $$field: null;
		}

		$logo = $this->gthis->parm_in('file', 'logo', 'N');
		$logo ? $data['logo'] = $logo : null;
		$product_list_img_1_path = $this->gthis->parm_in('file', 'product_list_img_1_path', 'N');
		$product_list_img_1_path ? $data['product_list_img_1_path'] = $product_list_img_1_path : null;
		$product_list_img_2_path = $this->gthis->parm_in('file', 'product_list_img_2_path', 'N');
		$product_list_img_2_path ? $data['product_list_img_2_path'] = $product_list_img_2_path : null;
		$product_list_img_3_path = $this->gthis->parm_in('file', 'product_list_img_3_path', 'N');
		$product_list_img_3_path ? $data['product_list_img_3_path'] = $product_list_img_3_path : null;
        $res = $shop->editShop($shop_id, $data);

        new apiDataBuilder(0, $res, null, $this->lang);
    }

	function editShop(){
		if (intval($_SESSION['right']['shop']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}
        require_once ROOT.'/include/class/shop.class.php';
        $shop = new shop($this->DB, $this->lang, $this->isAdmin);
        
		$shop_id = $this->gthis->parm_sql('request', 'shop_id');
	
		$data = $this->gthis->paramBuilder([
			["key" => "email", "type" => "email"],
			["key" => "address_tc"],
			["key" => "address_en"],
			["key" => "whatsapp"],
			["key" => "whatsapp2"],
			["key" => "ig"],
			["key" => "twitter"],
			["key" => "fax"],
			["key" => "markup"],
			["key" => "new_member_credit"],
			["key" => "point_ratio", "type" => "int"],
			["key" => "google_x"],
			["key" => "google_y"],
			["key" => "phone"],
			["key" => "facebook"],
			["key" => "header_text"],
			["key" => "voucher_id_arr", "type" => "intArrV1"],
		]);

        $res = $shop->editShop($shop_id, $data);

        new apiDataBuilder(0, $res, null, $this->lang);
	}

	function sortClient(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/client.class.php';
		$clientCls = new client($this->DB, $this->lang, $this->isAdmin);
		
		$data = $this->gthis->paramBuilder([
			["key" => "client_arr", "type" => "intArr"],
			["key" => "shop_id", "type" => "int"]
		]);	

		$clientCls->sortClient($data['shop_id'], $data["client_arr"]);

		new apiDataBuilder(0);
	}	
	
	function addClient(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/client.class.php';
		$clientCls = new client($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "image", "request" => "file"],
			["key" => "shop_id", "type" => "int"]
		]);	

		$image_path = $this->DB->uploadImg($data['image'], "client", 0);

		$clientCls->editClient([
			"image_path" => $image_path,
			"shop_id" => $data['shop_id']
		]);

		new apiDataBuilder(0);
	}

	function delClient(){
		if (intval($_SESSION['right']['page']) < 50){
			new apiDataBuilder(-62, null, null, $this->lang);
		}

		require_once ROOT.'/include/class/client.class.php';
		$clientCls = new client($this->DB, $this->lang, $this->isAdmin);

		$data = $this->gthis->paramBuilder([
			["key" => "client_id", "type" => "int"],
			["key" => "shop_id", "type" => "int"],
			["key" => "activate", "request" => "self", "defaultValue" => "N"]
		]);	

		$clientCls->editClient($data);

		new apiDataBuilder(0);
	}	
}
?>