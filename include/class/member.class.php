<?php
/* 
    Objective: member related
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_member
        tbl_membership
        tbl_member_fav
        tbl_member_voucher
        tmap_member_product
        tmap_member_region
        tmap_member_timeslot
        tmap_new_member_voucher
        
*/

require_once dirname(__FILE__). '/abstract.class.php';
class member extends baseClass{

    function resetPasswordRequest($shop_id, $email){
        // generate a 6 digits random string for password valid code
        // $shop_id: shop in use
        // $email: email address for the request user

        $sql = "SELECT * 
                FROM `tbl_member` AS m
                WHERE `activate` = 'Y' AND `shop_id` = '$shop_id' AND `email` = '$email'";
        $res = $this->DB->get_Sql($sql)[0];
        
        if (!$res){
            new apiDataBuilder(-52);
        }
        $member_id = $res['member_id'];
        $data = [
            "shop_id" => $res['shop_id'],
            "pw_valid_code" => $this->random_str(6)
        ];

        $res = $this->editMember($member_id, $data);

        return $res;
    }

    function getMemberDtlByValidCode($valid_code){
        // verify the member account base on the valid code
        // $valid_code: valid code

        $sql = "SELECT * 
                FROM `tbl_member` AS m
                WHERE `activate` = 'Y' AND valid_code = '$valid_code' GROUP BY member_id";
        $res = $this->DB->get_Sql($sql)[0];
        return $res;
    }

    function facebookInit($linkReturn = false){
        // facebook login api 
        // $linkReturn: return the facebook login url or not


		//initialize facebook sdk
		require 'include/vendor/facebook/graph-sdk/src/Facebook/autoload.php';
		
		session_start();
		$fb = new Facebook\Facebook([
			'app_id' => '427609255595588',
			'app_secret' => '31271e0e95e2462f7bba5aee7c31be17',
			'default_graph_version' => 'v2.5',
		]);
		$helper = $fb->getRedirectLoginHelper();
		$permissions = ['email']; // optional
		try {
			if (isset($_SESSION['facebook_access_token'])) {
				$accessToken = $_SESSION['facebook_access_token'];
			} else {
				$accessToken = $helper->getAccessToken();
			}
		} catch(Facebook\Exceptions\facebookResponseException $e) {
			// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		if (isset($accessToken)) {

			if (isset($_SESSION['facebook_access_token'])) {
				$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
			} else {
				// getting short-lived access token
				$_SESSION['facebook_access_token'] = (string) $accessToken;
				// OAuth 2.0 client handler
				$oAuth2Client = $fb->getOAuth2Client();
				// Exchanges a short-lived access token for a long-lived one
				$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
				$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
				// setting default access token to be used in script
				$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
			}

			// // redirect the user to the profile page if it has "code" GET variable
			// if (isset($_GET['code'])) {
			// 	header('Location: /myAccount/account');
			// }
			// getting basic info about user
			try {
				$profile_request = $fb->get('/me?fields=name,first_name,last_name,email');
				$requestPicture = $fb->get('/me/picture?redirect=false&height=200'); //getting user picture
				$picture = $requestPicture->getGraphUser();
				$profile = $profile_request->getGraphUser();
				$fbid = $profile->getProperty('id');           // To Get Facebook ID
				$fbfullname = $profile->getProperty('name');   // To Get Facebook full name
				$fbemail = $profile->getProperty('email');    //  To Get Facebook email
				$fbpic = "<img src='".$picture['url']."' class='img-rounded'/>";
				# save the user nformation in session variable
				$_SESSION['fb_id'] = $fbid;
				$_SESSION['fb_name'] = $fbfullname;
				$_SESSION['fb_email'] = $fbemail;
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
				// When Graph returns an error
				echo 'Graph returned an error: ' . $e->getMessage();
				session_destroy();
				// redirecting user back to app login page
				header("Location: ./");
				exit;
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
				// When validation fails or other local issues
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}
            if ($linkReturn){
                return ""; // success
            } else {
                return true;
            }
		} else {
            if ($linkReturn){
                // replace your website URL same as added in the developers.Facebook.com/apps e.g. if you used http instead of https and you used            
                $loginUrl = $helper->getLoginUrl('https://' .MASTER_DOMAIN ."/myAccount/fbLogin", $permissions);
                return $loginUrl;
            } else {
                return false;
            }
		}
	}

    function getFavList($member_id){
        // get the member favourite list
        // $member_id: FK 

        $sql = "SELECT * 
                FROM `tbl_member_fav` AS f 
                WHERE activate = 'Y' AND member_id = '$member_id'"; 
        $res = $this->DB->get_Sql($sql);
        return $res;
    }

    function editFav($fav_id, $data){
        // edit member favourite list (item)
        // $fav_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($fav_id == 0){
            $sql = "SELECT fav_id FROM `tbl_member_fav` AS f WHERE activate ='Y' AND member_id = '".$data['member_id']."' AND product_id = '".$data['product_id']."'";
            $fav_id = $this->DB->get_One($sql);
            if (!$fav_id)
                $fav_id = $this->DB->insert_db('tbl_member_fav', null);
        }
            
        $sql = $this->DB->buildSql_update("tbl_member_fav", "fav_id", $fav_id, $data);
        $this->DB->update($sql);

        return $this->getMemberDtl($data['member_id']);
    }

    function generateValidCode($memberDtl){
        // generate the validation code
        // $memberDtl: [obj] the member Detail
        $data = [
            "valid_code" => $this->random_str(16),
        ];
        $this->editMember($memberDtl['member_id'], $data);
        return $this->getMemberDtl($memberDtl['member_id']);
    }

    function emailValidation($shop_id, $email){
        // validate the email is already use or not
        // $shop_id: shop in use
        // $email: which email to check

        $sql = "SELECT * FROM `tbl_member` WHERE email = '$email' && shop_id = '$shop_id'";
        $res = $this->DB->get_Sql($sql);
        if ($res)
            return false;
        else 
            return true;
    }

    function loginMember($email, $password = null, $shop_id, $fb_id = null, $guest_member_id = null){
        // member login
        // $email: email login
        // $password: password
        // $shop: shop in use
        // fb_id: facebook id


        $sql = "SELECT member_id FROM `tbl_member` WHERE email = '$email' && shop_id = '$shop_id' AND activate = 'Y'";
        $member_id = $this->DB->get_One($sql);

        if (!$member_id && !$fb_id){
            new apiDataBuilder(-52);
        } else if ($member_id && $fb_id){
            // login by fb_id and the email is already existed 
            // mapping the facebook account to existing account

            $data = [
                "facebook_access_token" => $_SESSION['facebook_access_token'],
                "facebook_id" => $fb_id,
            ];

            $memberDtl = $this->editMember($member_id, $data);
            $member_id = $memberDtl['member_id'];
            $_SESSION['member'] = $memberDtl;
        
        } else if ($fb_id){
            // login by fb_id and email cannot be found
            $sql = "SELECT member_id FROM `tbl_member` WHERE facebook_id = '$fb_id'";
            $member_id = $this->DB->get_One($sql);

            if (!$member_id){ // the account is not created yet
                $data = [
                    "shop_id" => $shop_id,
                    "email" => $email,
                    "name" => $_SESSION['fb_name'],
                    "method" => "facebook",
                    "membership_id" => 1, // 白 會員
                    "facebook_access_token" => $_SESSION['facebook_access_token'],
                    "facebook_id" => $fb_id,
                ];

                $memberDtl = $this->editMember($member_id, $data);
                $member_id = $memberDtl['member_id'];
                $_SESSION['member'] = $memberDtl;
            } else {
                $data = [
                    "shop_id" => $shop_id,
                    "facebook_access_token" => $_SESSION['facebook_access_token']
                ];
                // get the full memberDtl
                $memberDtl = $this->editMember($member_id, $data);
                $member_id = $memberDtl['member_id'];
                $_SESSION['member'] = $memberDtl;
            }
        } else if ($member_id){
            // login by email and email access
            $memberDtl = $this->getMemberDtl($member_id);
            $member_id = $memberDtl['member_id'];
            
            if (password_verify($password, $memberDtl['password'])){
                // login success
                $_SESSION['member'] = $memberDtl;
                new apiDataBuilder(0);
            } else {
                new apiDataBuilder(-50, null, "Password incorrect");
            }

        } else {
            new apiDataBuilder(-50);
        }

        
        if ($guest_member_id) {
            // move the guest cart to member account
            $sql = "UPDATE `tbl_order` SET member_id = '$member_id' WHERE member_id = '$guest_member_id'";
            $this->DB->update($sql);
        }
    }

    function expGain($exp, $member_id){
        // member exp increase
        // $exp: how much it get
        // $member_id: who get the exp

        $this->DB->adjustFloatVal("tbl_member", "member_id", $member_id, "exp", $exp);
        return $this->getMemberDtl($member_id);
    }

    function expUsed($exp, $member_id){
        // member exp decrease
        // $exp: how much it lose
        // $member_id: who lose the exp

        $this->DB->adjustFloatVal("tbl_member", "member_id", $member_id, "exp", $exp * -1);
        return $this->getMemberDtl($member_id);
    }

    function pointGain($point, $member_id){
        // member gift exchange point increase
        // $point: how much it get
        // $member_id: who get the point

        $this->DB->adjustFloatVal("tbl_member", "member_id", $member_id, "point", $point);
        return $this->getMemberDtl($member_id);
    }

    function pointUsed($point, $member_id){
        // member gift exchange point decrease
        // $point: how much it lose
        // $member_id: who lose the point

        $this->DB->adjustFloatVal("tbl_member", "member_id", $member_id, "point", $point * -1);
        return $this->getMemberDtl($member_id);
    }

    function cashDollarGain($cashDollar, $member_id){
        // member cash dollar increase
        // $cashDollar: how much it cash dollar
        // $member_id: who get the cash dollarc

        $this->DB->adjustFloatVal("tbl_member", "member_id", $member_id, "cash_dollar", $cashDollar);
        return $this->getMemberDtl($member_id);
    }

    function cashDollarUsed($cashDollar, $member_id){
        // member cash dollar decrease
        // $cashDollar: how much it cash dollar
        // $member_id: who get the cash dollar

        $this->DB->adjustFloatVal("tbl_member", "member_id", $member_id, "cash_dollar", $cashDollar * -1);
        return $this->getMemberDtl($member_id);
    }

    function delVoucher($member_voucher_id){
        // remove the member voucher
        // $member_voucher_id: which member voucher should be remove

        $data = [
            "member_voucher_id" => $member_voucher_id, 
            "activate" => 'N'
        ];

        $sql = $this->DB->buildSql_update("tbl_member_voucher", "member_voucher_id", $member_voucher_id, $data);
        $this->DB->update($sql);

        return null;
    }

    function addVoucher($voucher_id, $member_id){
        // member add voucher
        // $voucher_id: FK
        // $member_id: FK 

        $member_voucher_id = $this->DB->insert_db("tbl_member_voucher", null);
                
		require_once ROOT.'/include/class/voucher.class.php';
		$voucherCls = new voucher($this->DB, $this->lang);

        $voucher = $voucherCls->getVoucherDtl($voucher_id);

        $data = [
            "member_voucher_id" => $member_voucher_id,
            "voucher_id" => $voucher_id, 
            "member_id" => $member_id,
            "discount" => $voucher['discount'],
            "discount" => $voucher['discount'],
            "type" => $voucher['type'],
            "minimum_order_amt" => $voucher['minimum_order_amt'],
            "ava_day" => $voucher['ava_day'],
            "sales_admin_id" => $voucher['sales_admin_id']
        ];

        if  ($voucher['expiry_date'])
            $data["expiry_date"] = $voucher['expiry_date'];

        $this->DB->update_Sql("tbl_member_voucher", "member_voucher_id", $data);

        return $this->getMemberDtl($member_id);
    }

	function editMembership($data){
        // edit membership
        // $membership_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($data['membership_id'] == 0){
            $membership_id = $this->DB->insert_db('tbl_membership', $data);
        } else {
            $membership_id = $this->DB->update_Sql("tbl_membership", "membership_id", $data);
        } 

        return $this->getMembershipDtl($membership_id);

	}

	function getMembershipDtl($membership_id){
        // get the membership Detail
        // $membership_id: PK

		$sql = "SELECT * FROM tbl_membership WHERE activate ='Y' AND membership_id = '$membership_id'";
		return $this->DB->get_Sql($sql)[0];
	}

	function getMembershipList($shop_id){
        // get the list
        // $shop_id: shop in use

		// $sql = "SELECT * FROM tbl_membership WHERE activate ='Y' AND shop_id = '$shop_id'";
		$sql = "SELECT * FROM tbl_membership WHERE activate ='Y'";
		return $this->DB->get_Sql($sql);
	}

    function genMemberNo($shop_id){
        // generate the member_no
        // $shop_id: shop in use

        $sql = "SELECT max(member_no) AS member_no FROM `tbl_member` WHERE shop_id = '$shop_id' ";
        $member_no = $this->DB->get_One($sql);

        $pure_no = str_replace("P", "", $member_no);
        $n = intval($pure_no);
        $n++;

        return "P".str_pad($n, 6, "0", STR_PAD_LEFT);
    }

    function createGuestMember($session_id, $shop_id){
        $member_id = $this->DB->insert_db('tbl_member', [
            "session_id" => $session_id,
            "shop_id" => $shop_id
        ]);

        return $this->getMemberDtl($member_id);
    }

    function register($data){
        $member_id = $this->DB->insert_db('tbl_member', null);
        $data['session_id'] = '';
        $data['member_no'] = $this->genMemberNo($data['shop_id']);

        // new comer discount 
        // new member voucher
        require_once ROOT.'/include/class/voucher.class.php';
        $voucherCls = new voucher($this->DB, $this->lang, $this->isAdmin);

        $voucherList = $voucherCls->getNewMemberVoucherList($data['shop_id']);
        
        foreach ($voucherList as $voucher){
            $this->addVoucher($voucher['voucher_id'], $member_id);
        }

        // new member cash dollar
        require_once ROOT.'/include/class/shop.class.php';
        $shopCls = new shop($this->DB, $this->lang, $this->isAdmin);
        $shop = $shopCls->getShopDtl($data['shop_id']);
        $this->cashDollarGain($shop['new_member_credit'], $member_id);


        // company discount cash dollar 
        require_once ROOT.'/include/class/companyDiscount.class.php';
        $discountCls = new companyDiscount($this->DB, $this->lang, $this->isAdmin);
        preg_match('/.*@(.*)/', $data['email'], $output_array);
        $emailSuffix = $output_array[1];

        $filter = [
            "skey" => $emailSuffix,
        ];
        
        $companyDiscountList = $discountCls->getCompanyDiscountList($data['shop_id'], $filter);
        foreach ($companyDiscountList as $companyDiscount){
            $discountCls->applyCompanyDiscount($companyDiscount, $member_id);
        }
        
		$sql = $this->DB->buildSql_update("tbl_member", "member_id", $member_id, $data);
        $this->DB->update($sql);

        return $this->getMemberDtl($member_id);
    }
    
	function editMember($member_id, $data){
        // edit member
        // $member_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($member_id == 0){
            $member = $this->register($data);
            $member_id = $member['member_id'];
        }
            
        $this->DB->updateMap("tmap_member_product", "member_id", $member_id, "product_id", $data['product_id_arr']);
        unset($data['product_id_arr']);

        $this->DB->updateMap("tmap_member_timeslot", "member_id", $member_id, "slot_id", $data['slot_id_arr']);
        unset($data['slot_id_arr']);

        $this->DB->updateMap("tmap_member_region", "member_id", $member_id, "region_id", $data['region_id_arr']);
        unset($data['region_id_arr']);
            
		$sql = $this->DB->buildSql_update("tbl_member", "member_id", $member_id, $data);
        $this->DB->update($sql);


        return $this->getMemberDtl($member_id);
	}

	function getMemberDtl($member_id){
        // get the member Detail
        // $member_id: PK

        if ($this->isAdmin){
            $sql = "SELECT m.*
                    FROM `tbl_member` AS m 
                    WHERE m.activate = 'Y' && m.member_id = '$member_id'";
        } else {
            $sql = "SELECT m.`cash_dollar`, m.`company_name`, m.`create_datetime`, m.`email`, m.`exp`, m.`language`, m.`last_update`, m.`member_id`, m.`member_no`, m.`membership_id`, m.`method`, m.`mobile`, m.`name`, m.`point`, m.`sales_admin_id`, m.`shop_id`, m.`verify`, m.`valid_code`, m.`pw_valid_code`
                    FROM `tbl_member` AS m
                    WHERE m.activate = 'Y' && m.member_id = '$member_id'";
        }
        $res = $this->DB->get_Sql($sql)[0];

        if ($res){

            // get and update the membership
            $membershipDtl = $this->getMembershipDtl($res['membership_id']);
            if ($res['exp'] > $membershipDtl['upgrade_point'] && $membershipDtl['upgrade_to_membership_id'] != 0){
                $this->editMember($member_id, [
                    "membership_id" => $membershipDtl['upgrade_to_membership_id']
                ]);
            }
            $res['membership_discount'] = $membershipDtl['discount'];

            // get member restriction
            $sql = "SELECT GROUP_CONCAT(s.slot_id) AS slot_id_arr
                    FROM `tbl_member` AS m 
                    LEFT JOIN `tmap_member_timeslot` AS s ON s.member_id = m.member_id AND s.activate ='Y'
                    WHERE m.activate = 'Y' && m.member_id = '$member_id'";
            $tList = $this->DB->get_Sql($sql)[0];
            $res['slot_id_arr'] = $tList['slot_id_arr'];
    
            $sql = "SELECT GROUP_CONCAT(p.product_id) AS product_id_arr
                    FROM `tbl_member` AS m 
                    LEFT JOIN `tmap_member_product` AS p ON p.member_id = m.member_id AND p.activate ='Y'
                    WHERE m.activate = 'Y' && m.member_id = '$member_id'";
            $tList = $this->DB->get_Sql($sql)[0];
            $res['product_id_arr'] = $tList['product_id_arr'];
    
            $sql = "SELECT GROUP_CONCAT(r.region_id) AS region_id_arr
                    FROM `tbl_member` AS m 
                    LEFT JOIN `tmap_member_region` AS r ON r.member_id = m.member_id AND r.activate ='Y'
                    WHERE m.activate = 'Y' && m.member_id = '$member_id'";
            $tList = $this->DB->get_Sql($sql)[0];
            $res['region_id_arr'] = $tList['region_id_arr'];
        

            // get member voucher
            $sql = "SELECT mv.*, o.voucher_discount, o.order_id, o.order_no, o.create_datetime AS redeem_datetime, o.voucher_discount, v.name_".$this->lang." AS `name`, (mv.create_datetime + INTERVAL mv.ava_day DAY) AS ava_date
                    FROM `tbl_member_voucher` AS mv
                    LEFT JOIN `tbl_order` AS o ON o.member_voucher_id = mv.member_voucher_id
                    LEFT JOIN `tbl_voucher` AS v ON mv.voucher_id = v.voucher_id
                    WHERE mv.`activate` = 'Y' AND mv.`member_id` = '$member_id'
                        AND (mv.expiry_date > NOW() OR mv.expiry_date IS NULL) 
                        AND (mv.create_datetime + INTERVAL mv.ava_day DAY) > NOW()
                        AND mv.`redeemed` = 'N'
                    GROUP BY member_voucher_id ";

            $voucherList = $this->DB->get_Sql($sql);

            foreach($voucherList as $k => $voucher){
                $sql = "SELECT GROUP_CONCAT(m.cate_id) as cate_id_arr
                        FROM `tmap_cate_voucher` AS m
                        WHERE activate = 'Y' AND voucher_id = '".$voucher['voucher_id']."'";
                $voucherList[$k]['cate_id_arr'] = $this->DB->get_Sql($sql);
            }

            $res['voucherList'] = $voucherList;
        }

		return $res;
	}

	function getMemberList($shop_id, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $fromShort = "m";
		$selectSql = "SELECT $fromShort.*, ms.name_" . $this->lang ." AS membership_name";
        $fromSql = " FROM `tbl_member` AS $fromShort 
                    LEFT JOIN `tbl_membership` AS ms ON $fromShort.membership_id = ms.membership_id ";
        $whereSql = "WHERE $fromShort.activate = 'Y' AND $fromShort.shop_id = '$shop_id' ";
        
        foreach($filter as $k => $i){
            if ($i || $i == '0'){
                switch ($k){
                    case 'business':
                        if ($i == 'Y')
                            $whereSql .= " AND (company_name IS NOT NULL && company_name != '') ";
                        else
                            $whereSql .= " AND (company_name IS NULL || company_name = '') ";
                        break;
                    case 'sales':
                        $whereSql .= " AND (sales_admin_id in ($i))";
                        break;
                    case 'skey':
                        $whereSql .= " AND (email LIKE '%$i%' OR `name` LIKE '%$i%' OR mobile = '$i' OR member_no = '$i' OR member_id ='$i' OR sales_admin_id ='$i') ";
                        break;
                    case 'member_no':
                        if ($i == 'Y')
                            $whereSql .= " AND ($fromShort.member_no IS NOT NULL AND $fromShort.member_no != '')";
                        else 
                            $whereSql .= " AND ($fromShort.member_no IS NULL)";
                        break;
                    case 'voucher_get_start_date':
                        if (!str_contains($fromSql, "`tbl_member_voucher` AS mv")){
                            $selectSql .= " , mv.create_datetime AS voucher_get_date, o.order_datetime AS voucher_use_date, o.order_id, o.order_no ";
                            $fromSql .= "LEFT JOIN `tbl_member_voucher` AS mv ON mv.member_id = $fromShort.member_id 
                                        LEFT JOIN `tbl_order` AS o ON o.member_voucher_id = mv.member_voucher_id ";
                        }
                        $whereSql .= " AND (mv.create_datetime >= '$i')";
                        break;
                    case 'voucher_get_end_date':
                        if (!str_contains($fromSql, "`tbl_member_voucher` AS mv")){
                            $selectSql .= " , mv.create_datetime AS voucher_get_date, o.order_datetime AS voucher_use_date, o.order_id, o.order_no ";
                            $fromSql .= "LEFT JOIN `tbl_member_voucher` AS mv ON mv.member_id = $fromShort.member_id 
                                        LEFT JOIN `tbl_order` AS o ON o.member_voucher_id = mv.member_voucher_id ";
                        }
                        $whereSql .= " AND (mv.create_datetime <= '$i')";
                        break;
                    case 'voucher_id':
                        if (!str_contains($fromSql, "`tbl_member_voucher` AS mv")){
                            $selectSql .= " , mv.create_datetime AS voucher_get_date, o.order_datetime AS voucher_use_date, o.order_id, o.order_no ";
                            $fromSql .= "LEFT JOIN `tbl_member_voucher` AS mv ON mv.member_id = $fromShort.member_id 
                                        LEFT JOIN `tbl_order` AS o ON o.member_voucher_id = mv.member_voucher_id ";
                        }
                        $whereSql .= " AND (mv.voucher_id = '$i')";
                        break;
                    default: 
                        break;
                }                    
            }
        }
        $sql = $selectSql . $fromSql . $whereSql;

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }
        
        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }
        
        return $this->DB->get_Sql($sql);
	}
}
?>