<?php
/* 
    Objective: delivery related including: region, district, holiday, timeslot, customize region and timeslot
    Author: Sing
    Last major update: 03-11-2021

    Table Scheme: 
        tbl_deli_*
        tmap_grp_product

        
*/

require_once dirname(__FILE__). '/abstract.class.php';
class delivery extends baseClass{

    function sortTimeslot($slot_id_arr, $shop_id = 1){
        $this->DB->sortMap("tbl_deli_timeslot", "shop_id", $shop_id, "slot_id", $slot_id_arr);
        return true;
    }

    function sortDistrict($district_id_arr, $shop_id = 1){
        $this->DB->sortMap("tbl_deli_district", "shop_id", $shop_id, "district_id", $district_id_arr);
        return true;
    }

    function editDistrict($district_id, $data){
        // edit district
        // $discount_id: tbl_company_discount PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($district_id == 0)
            $district_id = $this->DB->insert_db("tbl_deli_district", null);

        $sql = $this->DB->buildSql_update("tbl_deli_district", "district_id", $district_id, $data);
        $this->DB->update($sql);

        return $this->getDistrictDtl($district_id);
    }

    function getDistrictDtl($district_id){
        // get the district Detail
        // $district_id: PK
        $sql = "SELECT *, name_".$this->lang . " AS name
                FROM `tbl_deli_district`
                WHERE `activate` = 'Y' && `district_id` = '$district_id'";
        $res = $this->DB->get_Sql($sql)[0];

        if ($res){
            $sql = "SELECT *, r.name_".$this->lang . " AS name
                    FROM `tbl_deli_region` AS r
                    WHERE `activate` = 'Y' && `district_id` = '$district_id'";
            
            $res['regionList'] = $this->DB->get_Sql($sql);
        }

        return $res;
    }

    function getDistrictList($shop_id = 1, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: which shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $sql = "SELECT d.*, d.name_".$this->lang . " AS name
                FROM `tbl_deli_district` AS d
                LEFT JOIN `tbl_deli_region` AS r ON d.`district_id` = r.`district_id` AND r.`activate` ='Y'
                WHERE d.`activate` = 'Y'";
        if ($shop_id)
            $sql .= " && d.`shop_id` = '$shop_id'";
            
        if ($filter){
            foreach($filter as $k => $i){
                if ($i || $i == '0'){
                    switch ($k){
                        case 'skey':
                            $sql .= " AND (d.name_tc LIKE '%$i%' || d.name_en LIKE '%$i%' || r.name_tc LIKE '%$i%' || r.name_en LIKE '%$i%' || d.district_id = '$i')";
                            break;
                        
                        default: 
                            break;
                    }                    
                }
            }
        }

        $sql .= " GROUP BY d.district_id";

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        } else {
            $sql .= " ORDER BY d.sort";
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        $res = $this->DB->get_Sql($sql);

        //dispatchDistrict

        foreach ($res as $k => $district){
            $res[$k]['region_list'] = $this->getRegionList(null, ["district_id" => $district['district_id']]);
        }

        return $res;
    }

    function editCustDeliRegion($cust_id, $data){
        // edit tbl_deli_cust_region, which about the region of customize delivery group 
        // $cust_id: tbl_deli_cust_region PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($cust_id == 0)
            $cust_id = $this->DB->insert_db("tbl_deli_cust_region", null);

        $sql = $this->DB->buildSql_update("tbl_deli_cust_region", "cust_id", $cust_id, $data);
        $this->DB->update($sql);
        return $this->getRegionDtl($cust_id);
    }

    function editCustDeliTimeslot($cust_id, $data){
        // edit tbl_deli_cust_timeslot, which about the timeslot of customize delivery group
        // $cust_id: tbl_deli_cust_timeslot PK, which edit. Inserting a new row when 0
        // $data: the data of it 
        
        if ($cust_id == 0)
            $cust_id = $this->DB->insert_db('tbl_deli_cust_timeslot', null);

        $sql = $this->DB->buildSql_update("tbl_deli_cust_timeslot", "cust_id", $cust_id, $data);
        $this->DB->update($sql);
        return $this->getRegionDtl($cust_id);
    }

    function editCustDeliGrp($grp_id, $data){
        // edit tbl_deli_cust_grp
        // $grp_id: tbl_deli_cust_grp PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($grp_id == 0)
            $grp_id = $this->DB->insert_db('tbl_deli_cust_grp', null);
       
        $sql = $this->DB->buildSql_update("tbl_deli_cust_grp", "grp_id", $grp_id, $data);
        $this->DB->update($sql);
        return $this->getCustDeliGrpDtl($grp_id);
    }

    function getCustDeliGrpList($shop_id, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: which shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by
        $sql = "SELECT * FROM `tbl_deli_cust_grp` WHERE `activate` ='Y' && `shop_id` = '$shop_id'";
        
        if ($filter){
            foreach($filter as $k => $i){
                if ($i || $i == '0'){
                    switch ($k){
                        case 'skey':
                            $sql .= " && (adm_name LIKE '%$i%' || grp_id = '$i')";
                            
                            break;
                        default: 
                            break;
                    }                    
                }
            }
        }

        if ($sorting){
            $sql .= " ORDER BY " . $sorting['sort'] . " " . $sorting['order'];
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }
        
        $res = $this->DB->get_Sql($sql);
        return $res;
    }

    function getCustDeliGrpRegionDtl($grp_id, $region_id){
        $sql = "SELECT * FROM `tbl_deli_cust_region` WHERE activate='Y' AND show_in_web = 'Y' AND grp_id = '$grp_id' AND region_id ='$region_id'";
        $res = $this->DB->get_Sql($sql)[0];

        return $res;
    }

    function getCustDeliGrpTimeslotDtl($grp_id, $region_id, $timeslot_id){
        $sql = "SELECT * FROM `tbl_deli_cust_timeslot` WHERE activate='Y' AND show_in_web = 'Y' AND grp_id = '$grp_id' AND slot_id ='$timeslot_id' AND (region_id IS NULL OR region_id = '$region_id')";
        $res = $this->DB->get_Sql($sql)[0];

        return $res;
    }

    function getCustDeliGrpDtl($grp_id){
        // get the customize delivery group Detail
        // $grp_id: PK

        $sql = "SELECT *
                FROM `tbl_deli_cust_grp`
                WHERE `activate` = 'Y' && `grp_id` = '$grp_id'";
                
        $res = $this->DB->get_Sql($sql)[0];


        // // get the product which using the customize delivery group
        // $sql = "SELECT p.* 
        //         FROM `tmap_grp_product` as m
        //         LEFT JOIN `tbl_product` as p ON p.product_id = m.product_id AND p.`activate` = 'Y'
        //         WHERE m.`activate` = 'Y' && grp_id = '$grp_id'";
        //         echo $sql;
        // $res['productList'] = $this->DB->get_Sql($sql);

        $sql = "SELECT * FROM `tbl_deli_cust_region` WHERE activate='Y' AND grp_id = '$grp_id'";
        $res['custRegionList'] = $this->DB->get_Sql($sql);

        $sql = "SELECT * FROM `tbl_deli_cust_timeslot` WHERE activate='Y' AND grp_id = '$grp_id'";
        $res['custTimeslotList'] = $this->DB->get_Sql($sql);
                
        return $res;
    }

    function editRegion($region_id, $data){
        // edit region
        // $region_id: tbl_deli_region PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($region_id == 0)
            $region_id = $this->DB->insert_db('tbl_deli_region', null);

        $sql = $this->DB->buildSql_update("tbl_deli_region", "region_id", $region_id, $data);
        $this->DB->update($sql);
        return $this->getRegionDtl($region_id);
    }

    function getTimeslotListClient($region_id, $deli_group_id_arr, $shop_id){
        // SELECT merged.*, dt.adm_name, dt.start_time, dt.end_time, dt.default, dt.sort
        // FROM (
        //     SELECT slot_id, 
        //         MIN(ava_b4_cutoff) AS ava_b4_cutoff,
        //         MAX(ava_b4_day) AS ava_b4_day,
        //         SUM(same_day_deli_fee) AS same_day_deli_fee,
        //         SUM(deli_fee) AS deli_fee,
        //         IF (sum(online_pay_same_day = 'Y') > 0, 'Y', 'N') AS online_pay_same_day,
        //         IF (sum(show_in_web = 'N') > 0, 'N', 'Y') AS show_in_web 
        //     FROM (
        //         SELECT dgt.slot_id, 
        //             IFNULL(dg.ava_b4_cutoff, dgt.ava_b4_cutoff) AS ava_b4_cutoff, 
        //             IFNULL(dg.ava_b4_day, dgt.ava_b4_day) AS ava_b4_day, 
        //             IFNULL(dg.same_day_deli_fee, dgt.same_day_deli_fee) AS same_day_deli_fee, 
        //             IFNULL(dg.deli_fee, dgt.deli_fee) AS deli_fee,
        //             IFNULL(dg.online_pay_same_day, dgt.online_pay_same_day) AS online_pay_same_day, 
        //             IFNULL(dg.show_in_web, dgt.show_in_web) AS show_in_web
        //         FROM `tbl_deli_group_timeslot` AS dgt
        //         LEFT JOIN (
        //             SELECT dgrt.slot_id, dgrt.ava_b4_cutoff, dgrt.ava_b4_day, dgrt.same_day_deli_fee, dgrt.online_pay_same_day, dgrt.show_in_web , dgrt.deli_fee
        //             FROM `tbl_deli_group_region_timeslot` AS dgrt
        //             LEFT JOIN `tbl_deli_group_region` AS dgr ON dgrt.deli_group_region_id = dgr.deli_group_region_id AND dgr.activate ='Y'
        //             LEFT JOIN `tmap_deli_group_region` AS mdgr ON mdgr.deli_group_region_id = dgr.deli_group_region_id AND mdgr.activate ='Y' 
        //             WHERE dgrt.activate ='Y' AND dgr.deli_group_id = 2 AND region_id = 61
        //         ) AS dg ON dg.slot_id = dgt.slot_id 
        //         WHERE dgt.activate ='Y' AND dgt.deli_group_id = 2 
        //     ) AS src
        //     GROUP BY slot_id
        // ) AS merged
        // LEFT JOIN `tbl_deli_timeslot` AS dt ON dt.slot_id = merged.slot_id 
        // WHERE merged.show_in_web = 'Y'
        // ORDER BY dt.sort

        $sql = "SELECT merged.*, dt.adm_name, dt.start_time, dt.end_time, dt.default, dt.sort
                FROM (
                    SELECT slot_id, 
                        MIN(ava_b4_cutoff) AS ava_b4_cutoff,
                        MAX(ava_b4_day) AS ava_b4_day,
                        SUM(same_day_deli_fee) AS same_day_deli_fee,
                        SUM(deli_fee) AS deli_fee,
                        IF (sum(online_pay_same_day = 'Y') > 0, 'Y', 'N') AS online_pay_same_day,
                        IF (sum(show_in_web = 'N') > 0, 'N', 'Y') AS show_in_web 
                    FROM (";

        foreach($deli_group_id_arr as $k => $deli_group_id){
            if ($k > 0){
                $sql .= " UNION ALL ";
            }

            $sql .= "SELECT dgt.slot_id, 
                        IFNULL(dg.ava_b4_cutoff, dgt.ava_b4_cutoff) AS ava_b4_cutoff, 
                        IFNULL(dg.ava_b4_day, dgt.ava_b4_day) AS ava_b4_day, 
                        IFNULL(dg.same_day_deli_fee, dgt.same_day_deli_fee) AS same_day_deli_fee, 
                        IFNULL(dg.deli_fee, dgt.deli_fee) AS deli_fee,
                        IFNULL(dg.online_pay_same_day, dgt.online_pay_same_day) AS online_pay_same_day, 
                        IFNULL(dg.show_in_web, dgt.show_in_web) AS show_in_web
                    FROM `tbl_deli_group_timeslot` AS dgt
                    LEFT JOIN (
                        SELECT dgrt.slot_id, dgrt.ava_b4_cutoff, dgrt.ava_b4_day, dgrt.same_day_deli_fee, dgrt.online_pay_same_day, dgrt.show_in_web , dgrt.deli_fee
                        FROM `tbl_deli_group_region_timeslot` AS dgrt
                        LEFT JOIN `tbl_deli_group_region` AS dgr ON dgrt.deli_group_region_id = dgr.deli_group_region_id AND dgr.activate ='Y'
                        LEFT JOIN `tmap_deli_group_region` AS mdgr ON mdgr.deli_group_region_id = dgr.deli_group_region_id AND mdgr.activate ='Y' 
                        WHERE dgrt.activate ='Y' AND dgr.deli_group_id = $deli_group_id AND region_id = $region_id
                    ) AS dg ON dg.slot_id = dgt.slot_id 
                    WHERE dgt.activate ='Y' AND dgt.deli_group_id = $deli_group_id";
        }
                        
        $sql .= "       ) AS src
                    GROUP BY slot_id
                ) AS merged
                LEFT JOIN `tbl_deli_timeslot` AS dt ON dt.slot_id = merged.slot_id 
                WHERE merged.show_in_web = 'Y' AND dt.shop_id
                ORDER BY dt.sort";

        return $this->DB->get_Sql($sql);
    }

    function getRegionListClient($district_id, $deli_group_id_arr, $shop_id){
        $sql = "SELECT merged.*, dr.name_tc, dr.name_en, dr.name_tc AS name, dr.district_id
                FROM (
                    SELECT region_id, SUM(shipping_fee) AS shipping_fee, IF (sum(address_required = 'Y') >0, 'Y' , 'N') AS address_required, IF (sum(show_in_web = 'N') > 0, 'N', 'Y') AS show_in_web
                    FROM (";

        foreach($deli_group_id_arr as $k => $deli_group_id){
            if ($k > 0){
                $sql .= " UNION ALL ";
            }
            $sql .= "SELECT dr.region_id, 
                        IFNULL(dg.shipping_fee, dr.shipping_fee) AS shipping_fee,
                        IFNULL(dg.address_required, dr.address_required) AS address_required,
                        IFNULL(dg.show_in_web, dr.show_in_web) AS show_in_web
                    FROM `tbl_deli_region` AS dr
                    LEFT JOIN (
                        SELECT dgr.shipping_fee, dgr.address_required, dgr.show_in_web, mgdr.region_id
                        FROM `tbl_deli_group_region` AS dgr
                        LEFT JOIN `tmap_deli_group_region` AS mgdr ON mgdr.deli_group_region_id = dgr.deli_group_region_id AND mgdr.activate ='Y'
                        WHERE dgr.activate='Y' AND deli_group_id = $deli_group_id 
                    ) AS dg ON dg.region_id = dr.region_id
                    WHERE dr.activate = 'Y' AND dr.district_id = $district_id AND dr.shop_id = $shop_id 
            ";
        }
        $sql .= "            ) AS src
                    GROUP BY region_id
                ) AS merged
                LEFT JOIN `tbl_deli_region` AS dr ON dr.region_id = merged.region_id
                WHERE merged.show_in_web = 'Y'";
        // $sql = "SELECT dr.region_id, 
        //             IFNULL(dg.shipping_fee, dr.shipping_fee) AS shipping_fee,
        //             IFNULL(dg.address_required, dr.address_required) AS address_required,
        //             IFNULL(dg.show_in_web, dr.show_in_web) AS show_in_web
        //         FROM `tbl_deli_region` AS dr
        //         LEFT JOIN (
        //             SELECT dgr.shipping_fee, dgr.address_required, dgr.show_in_web, mgdr.region_id
        //             FROM `tbl_deli_group_region` AS dgr
        //             LEFT JOIN `tmap_deli_group_region` AS mgdr ON mgdr.deli_group_region_id = dgr.deli_group_region_id AND mgdr.activate ='Y'
        //             WHERE dgr.activate='Y' AND deli_group_id in ( $deli_group_id_arr )
        //         ) AS dg ON dg.region_id = dr.region_id
        //         WHERE dr.activate = 'Y' AND dr.district_id = $district_id AND dr.shop_id = $shop_id AND IFNULL(dg.show_in_web, dr.show_in_web) = 'Y'";
        return $this->DB->get_Sql($sql);
    }

    function getRegionList($shop_id = null, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: which shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $fromShort = "dr";
        $selectSql = "SELECT $fromShort.region_id, $fromShort.show_in_web, $fromShort.shop_id, $fromShort.district_id, $fromShort.name_tc, $fromShort.name_en, $fromShort.name_". $this->lang ." AS name ";
        $subSelectSql = ", $fromShort.shipping_fee, $fromShort.address_required ";
        $fromSql  = "FROM `tbl_deli_region` AS $fromShort
                     LEFT JOIN `tbl_deli_district` AS d ON d.`district_id` = $fromShort.`district_id` AND d.`activate` = 'Y'";
        $whereSql = "WHERE $fromShort.`activate` ='Y' ";
        if ($shop_id){
            $whereSql .= " AND $fromShort.`shop_id` = '$shop_id'";
        }


        $afterSql = "";
        if ($filter){
            foreach($filter as $k => $i){
                if ($i || $i == '0'){
                    switch ($k){
                        case 'cust_deli_id_arr':
                            $fromSql .= " LEFT JOIN `tbl_deli_cust_region` AS cust ON ";
                            // $fromSql .= "  AND grp_id = '$i'";
                            // $whereSql .= " AND IF(c.show_in_web IS NULL, $fromShort.show_in_web = 'Y', c.show_in_web='Y') ";
                            // $afterSql .= " AND if(cust_show_in_web IS NOT NULL, cust_show_in_web in ('Y'), show_in_web = 'Y')";
                            break;
                        case 'district_id':
                            $whereSql .= " AND ($fromShort.district_id = '$i')";
                            break;
                        case 'show_in_web':
                            $whereSql .= " AND ($fromShort.show_in_web = '$i')";
                            break;
                        case 'deli_group_id':
                            $subSelectSql = ", dgr.edit_group_id, dgr.map_id as deli_group_region_map_id, dgr.deli_group_id, IFNULL(dgr.shipping_fee, $fromShort.shipping_fee) AS shipping_fee, IFNULL(dgr.address_required, $fromShort.address_required) AS address_required ";
                            $fromSql .= "LEFT JOIN `tbl_deli_group_region` AS dgr ON dgr.activate = 'Y'";
                            $whereSql .= " AND (dgr.deli_group_id = '$i')";
                            break;
                        default: 
                            break;
                    }                    
                }
            }
        }

        $selectSql .= $subSelectSql;
        $sql = $selectSql . $fromSql . $whereSql . " GROUP BY $fromShort.region_id";

        if ($sorting){
            $sql .= " ORDER BY $fromShort." . $sorting['sort'] . " " . $sorting['order'];
        } else {
            $sql .= " ORDER BY $fromShort.region_id ";
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }

        if ($afterSql){
            $sql = "SELECT * FROM ($sql) AS src WHERE 1 = 1 $afterSql";
        }
        
        $res = $this->DB->get_Sql($sql);
        return $res;
    }

    function getRegionDtl($region_id){
        // get the delivery region Detail
        // $region_id: PK
        $sql = "SELECT *
                FROM `tbl_deli_region`
                WHERE `activate` = 'Y' && `region_id` = '$region_id'";
        $res = $this->DB->get_Sql($sql)[0];
        return $res;
    }
    
    function editTimeslot($slot_id, $data){
        // edit timeslot
        // $slot_id: PK, which edit. Inserting a new row when 0
        // $data: the data of it 

        if ($slot_id == 0)
            $slot_id = $this->DB->insert_db('tbl_deli_timeslot', null);

        $sql = $this->DB->buildSql_update("tbl_deli_timeslot", "slot_id", $slot_id, $data);
        $this->DB->update($sql);
        return $this->getTimeslotDtl($slot_id);
    }

    function getTimeslotListByGrpIdArrInRegion($shop_id, $grp_id_arr, $region_id){
        // special handling for checkout_api getTimeslotList which using in the checkoutConfirmAddress
        // $shop_id = not in use
        // $grp_id_arr: the arr of the tbl_deli_cust_grp.grp_id
        // $region_id: the selected region
        $grp_id_arr = array_filter($grp_id_arr);
        if ($grp_id_arr){
            $sql = "SELECT t.*, c.grp_id, IFNULL(c.deli_fee, t.deli_fee) AS deli_fee, IFNULL(c.same_day_deli_fee, t.same_day_deli_fee) AS same_day_deli_fee, IFNULL(c.show_in_web, t.show_in_web) AS show_in_web, IFNULL(c.online_pay_same_day, t.online_pay_same_day) AS online_pay_same_day
                    FROM `tbl_deli_timeslot` AS t
                    LEFT JOIN `tbl_deli_cust_timeslot` AS c ON t.slot_id = c.slot_id AND c.`activate` ='Y' AND c.grp_id in (" . implode(",", $grp_id_arr) .") AND (c.region_id IS NULL OR c.region_id = '$region_id')
                    WHERE  t.`activate` = 'Y'
                    GROUP BY t.sort
                    order by t.slot_id, c.show_in_web = 'Y' DESC
            ";
        } else {
            $sql = "SELECT t.*, c.grp_id, IFNULL(c.deli_fee, t.deli_fee) AS deli_fee, IFNULL(c.same_day_deli_fee, t.same_day_deli_fee) AS same_day_deli_fee, IFNULL(c.show_in_web, t.show_in_web) AS show_in_web, IFNULL(c.online_pay_same_day, t.online_pay_same_day) AS online_pay_same_day
                    FROM `tbl_deli_timeslot` AS t
                    LEFT JOIN `tbl_deli_cust_timeslot` AS c ON t.slot_id = c.slot_id AND c.`activate` ='Y' AND (c.region_id IS NULL OR c.region_id = '$region_id')
                    WHERE  t.`activate` = 'Y'
                    GROUP BY t.sort
                    order by t.slot_id, c.show_in_web = 'Y' DESC
            ";
        }


        $res = $this->DB->get_Sql($sql);
        return $res;   
    }

    function getDeliGroupRegionList($deli_group_id){
        $sql = "SELECT dgr.*
                FROM `tbl_deli_group_region` AS dgr
                WHERE dgr.`deli_group_id` = $deli_group_id AND dgr.`activate` = 'Y'";
        $res = $this->DB->get_Sql($sql);

        foreach ($res as $k => $i){
            $deli_group_reigon_id = $i['deli_group_region_id'];

            // $sql = "SELECT dgrt.*, dt.adm_name, dt.start_time, dt.end_time
            //             ,IFNULL(dgrt.deli_fee, dt.deli_fee) AS deli_fee, IF(dgrt.deli_fee IS NULL, 'Y', 'N') AS default_deli_fee
            //             , IFNULL(dgrt.same_day_deli_fee, dt.same_day_deli_fee) AS same_day_deli_fee, IF(dgrt.same_day_deli_fee IS NULL, 'Y', 'N') AS default_same_day_deli_fee
            //             , IFNULL(dgrt.online_pay_same_day, dt.online_pay_same_day) AS online_pay_same_day, IF(dgrt.online_pay_same_day IS NULL, 'Y', 'N') AS default_online_pay_same_day
            //         FROM `tbl_deli_group_region_timeslot` AS dgrt
            //         LEFT JOIN `tbl_deli_timeslot` AS dt ON dgrt.timeslot_id = dt.slot_id
            //         WHERE dgrt.activate = 'Y' AND deli_group_region_id = $deli_group_reigon_id";
            // $res[$k]['timeslot_list'] = $this->DB->get_Sql($sql);

            $sql = "SELECT dt.slot_id, dt.adm_name, dt.start_time, dt.end_time,  dt.default
                        , IFNULL(dgrt.deli_fee, dt.deli_fee) AS deli_fee, IF(dgrt.deli_fee IS NULL, 'Y', 'N') AS default_deli_fee
                        , IFNULL(dgrt.same_day_deli_fee, dt.same_day_deli_fee) AS same_day_deli_fee, IF(dgrt.same_day_deli_fee IS NULL, 'Y', 'N') AS default_same_day_deli_fee
                        , IFNULL(dgrt.online_pay_same_day, dt.online_pay_same_day) AS online_pay_same_day, IF(dgrt.online_pay_same_day IS NULL, 'Y', 'N') AS default_online_pay_same_day
                        , IFNULL(dgrt.show_in_web, dt.show_in_web) AS show_in_web, IF(dgrt.show_in_web IS NULL, 'Y', 'N') AS default_show_in_web
                        , IFNULL(dgrt.ava_b4_day, dt.ava_b4_day) AS ava_b4_day, IF(dgrt.ava_b4_day IS NULL, 'Y', 'N') AS default_ava_b4_day
                        , IFNULL(dgrt.ava_b4_cutoff, dt.ava_b4_cutoff) AS ava_b4_cutoff, IF(dgrt.ava_b4_cutoff IS NULL, 'Y', 'N') AS default_ava_b4_cutoff 
                    FROM `tbl_deli_timeslot` AS dt
                    LEFT JOIN `tbl_deli_group_region_timeslot` AS dgrt ON dgrt.slot_id = dt.slot_id AND dgrt.activate = 'Y' AND (dgrt.`deli_group_region_id` = '$deli_group_reigon_id')
                    WHERE dt.activate ='Y'
                    ORDER BY dt.sort";
            $res[$k]['timeslot_list'] = $this->DB->get_Sql($sql);

            $sql = "SELECT m.*, dr.name_tc, dr.district_id
                    FROM `tmap_deli_group_region` AS m
                    LEFT JOIN `tbl_deli_region` AS dr ON m.region_id = dr.region_id
                    WHERE m.activate ='Y' AND deli_group_region_id = $deli_group_reigon_id";
            $res[$k]['region_list'] = $this->DB->get_Sql($sql);
        }


        return $res;
    }

    function getDeliGroupRegionTimeslotList($deli_group_region_map_id){
        $sql = "SELECT *
                FROM `tbl_deli_group_region_timeslot` AS dgrt
                WHERE dgrt.deli_group_region_map_id = $deli_group_region_map_id";
        $res = $this->DB->get_Sql($sql);

        return $res;
    }

    function getTimeslotList($shop_id = 1, $filter = null, $limiter = null, $sorting = null){
        // get the list
        // $shop_id: which shop in use
        // $filter: [obj]
        // $limiter: [obj] - page, limit
        // $sorting: [obj] - sort, order, 
        //          order: ASC/ DESC
        //          sort: field sort by

        $fromShort = "dt";
        $selectSql = "SELECT $fromShort.slot_id, $fromShort.adm_name, $fromShort.start_time, $fromShort.end_time,  $fromShort.default ";
        $subSelectSql = ",$fromShort.deli_fee, $fromShort.same_day_deli_fee, $fromShort.online_pay_same_day ";
        $fromSql  = "FROM `tbl_deli_timeslot` AS $fromShort ";
        $whereSql = "WHERE $fromShort.`activate` ='Y' && $fromShort.`shop_id` = '$shop_id' ";

        if ($filter){
            foreach($filter as $k => $i){
                if ($i || $i == '0'){
                    switch ($k){
                        case 'skey':
                            $fromSql .= " AND grp_id = '$i'";
                            break;
                        case 'cust_deli_id_arr':
                            $fromSql .= " AND grp_id = '$i'";
                            break;
                        case 'default':
                            $whereSql .= " AND ($fromShort.`default` = '$i')";
                            break;
                        case 'deli_group_id': // deli_group_id
                            $subSelectSql .= ",IFNULL(dgt.deli_fee, $fromShort.deli_fee) AS deli_fee, IF(dgt.deli_fee IS NULL, 'Y', 'N') AS default_deli_fee
                                            , IFNULL(dgt.same_day_deli_fee, $fromShort.same_day_deli_fee) AS same_day_deli_fee, IF(dgt.same_day_deli_fee IS NULL, 'Y', 'N') AS default_same_day_deli_fee
                                            , IFNULL(dgt.online_pay_same_day, $fromShort.online_pay_same_day) AS online_pay_same_day, IF(dgt.online_pay_same_day IS NULL, 'Y', 'N') AS default_online_pay_same_day
                                            , IFNULL(dgt.show_in_web, $fromShort.show_in_web) AS show_in_web, IF(dgt.show_in_web IS NULL, 'Y', 'N') AS default_show_in_web 
                                            , IFNULL(dgt.ava_b4_day, $fromShort.ava_b4_day) AS ava_b4_day, IF(dgt.ava_b4_day IS NULL, 'Y', 'N') AS default_ava_b4_day
                                            , IFNULL(dgt.ava_b4_cutoff, $fromShort.ava_b4_cutoff) AS ava_b4_cutoff, IF(dgt.ava_b4_cutoff IS NULL, 'Y', 'N') AS default_ava_b4_cutoff ";
                            $fromSql .= "LEFT JOIN `tbl_deli_group_timeslot` AS dgt ON dgt.slot_id = $fromShort.slot_id AND dgt.activate ='Y' AND (dgt.`deli_group_id` = '$i') ";
                            break;
                        default: 
                            break;
                    }                    
                }
            }
        }

        $selectSql .= $subSelectSql;
        $sql = $selectSql . $fromSql . $whereSql . " GROUP BY slot_id";

        if ($sorting){
            $sql .= " ORDER BY `" . $sorting['sort'] . "` " . $sorting['order'];
        } else {
            $sql .= " ORDER BY sort ";
        }

        if ($limiter){
            $sql .= " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];
        }
        
        $res = $this->DB->get_Sql($sql);
        return $res;
    }

    function getTimeslotDtl($slot_id){
        // get the timeslot Detail
        // $slot_id: PK
        
        $sql = "SELECT *
                FROM `tbl_deli_timeslot`
                WHERE `activate` = 'Y' && `slot_id` = '$slot_id'";
                
        $res = $this->DB->get_Sql($sql)[0];
        return $res;
    }

//////////////////////////////////////// DELIVERY GROUP START   /////////////////////////////////////
    function getDeliveryGroupList($filter = null, $sorting = null, $limiter = null){
		$fromShort = "dg";
		$selectSql = "SELECT $fromShort.* ";
		$fromSql = "FROM `tbl_deli_group` AS $fromShort";
		$whereSql = " WHERE $fromShort.`activate` = 'Y'";
		$groupSql = " GROUP BY $fromShort.deli_group_id";
		if ($filter && $filter['includeInactive'] && $filter['includeInactive'] == 'Y')
			$whereSql = " WHERE 1=1";

		if ($sorting)
			$sortSql = " ORDER BY $sorting";
		
		if ($limiter)
			$limitSql = " LIMIT " . (($limiter['page']-1) * $limiter['limit']) . ", " . $limiter['limit'];

		if ($filter){
			foreach ($filter as $k => $i){
                if ($i != ''){
                    switch ($k){
                        case 'deli_group_id':
                            $whereSql .= " AND ($fromShort.`deli_group_id` = '$i')";
                            break;
                    } 
                }
			}
		}
		
		$sql = $selectSql . $fromSql . $whereSql . $groupSql . $sortSql  . $limitSql;
		$res = $this->DB->get_Sql($sql);

		foreach($res as $k => $i){
			$res[$k] = $this->dispatchDeliveryGroup($i);
		}

		return $res;
	}

    function getDeliveryGroup($deli_group_id){
        if ($deli_group_id)
            return $this->getDeliveryGroupList(["deli_group_id" => $deli_group_id], null, $this->DB->getUnique)[0];
        else 
            return null;
    }

    function dispatchDeliveryGroup($deli_group){
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->lang, $this->isAdmin);

        $deli_group['product_list'] = $productCls->getProductList(1, ["deli_group_id" => $deli_group['deli_group_id']]);
        return $deli_group;
    }

    function editDeliveryGroup($data){
        if (isset($data['deli_group_id']) && $data['deli_group_id'] != 0) {
			$deli_group_id = $this->DB->update_Sql("tbl_deli_group", "deli_group_id", $data);
		} else {
			$deli_group_id = $this->DB->insert_db("tbl_deli_group", $data);
		} 

        // default timeslot
        if (isset($data['defaultJson'])){
            $sql = "UPDATE `tbl_deli_group_timeslot` SET activate ='N' WHERE deli_group_id = $deli_group_id ";
            $this->DB->update($sql);

            foreach ($data['defaultJson'] as $timeslot){
                // check if it is not default
                if ($timeslot['online_pay_same_day'] != 'NULL' || $timeslot['show_in_web'] != 'NULL' || $timeslot['same_day_deli_fee'] != 'NULL' || $timeslot['deli_fee'] != 'NULL' || $timeslot['ava_b4_day'] || $timeslot['ava_b4_cutoff']){
                    $timeslot['activate'] = 'Y';
                    $timeslot['deli_group_id'] = $deli_group_id;

                    $sql = "SELECT * 
                            FROM `tbl_deli_group_timeslot` 
                            WHERE deli_group_id = $deli_group_id AND slot_id = " . $timeslot['slot_id'];
                    $map_id = $this->DB->get_Sql_Mode($sql, "singleColumn", ["col" => "map_id"])[0];

                    if ($map_id){
                        $timeslot['map_id'] = $map_id;
                        $this->DB->update_Sql("tbl_deli_group_timeslot", "map_id", $timeslot);
                    } else {
                        $this->DB->insert_db("tbl_deli_group_timeslot", $timeslot);
                    }
                }                
            }
        }
        if (isset($data['ruleJson'])){
            $sql = "UPDATE `tbl_deli_group_region` SET activate ='N' WHERE deli_group_id = $deli_group_id ";
            $this->DB->update($sql);        

            foreach($data['ruleJson'] as $rule){
                $rule['activate'] = 'Y';
                $rule['deli_group_id'] = $deli_group_id;

                $sql = "SELECT * 
                        FROM `tbl_deli_group_region`
                        WHERE deli_group_id = $deli_group_id AND activate ='N'";
                $deli_group_region_id = $this->DB->get_Sql_Mode($sql, "singleColumn", ["col" => "deli_group_region_id"])[0];

                if ($deli_group_region_id){
                    $rule['deli_group_region_id'] = $deli_group_region_id;
                    $this->DB->update_Sql("tbl_deli_group_region", "deli_group_region_id", $rule);
                } else {
                    $deli_group_region_id = $this->DB->insert_db("tbl_deli_group_region", $rule);
                }
                
                $this->DB->updateMap("tmap_deli_group_region", "deli_group_region_id", $deli_group_region_id, "region_id", $rule['deli_region_id_arr']);

                $sql = "UPDATE `tbl_deli_group_region_timeslot` SET activate ='N' WHERE deli_group_region_id = $deli_group_region_id ";
                $this->DB->update($sql);

                foreach ($rule['timeslot_json'] as $timeslot){
                    // check if it is not default
                    if ($timeslot['online_pay_same_day'] != 'NULL' || $timeslot['show_in_web'] != 'NULL' || $timeslot['same_day_deli_fee'] != 'NULL' || $timeslot['deli_fee'] != 'NULL' || $timeslot['ava_b4_day'] || $timeslot['ava_b4_cutoff']){
                        $timeslot['activate'] = 'Y';
                        $timeslot['deli_group_region_id'] = $deli_group_region_id;

                        $sql = "SELECT * 
                                FROM `tbl_deli_group_region_timeslot` 
                                WHERE deli_group_region_id = $deli_group_region_id AND slot_id = " . $timeslot['slot_id'];
                        $map_id = $this->DB->get_Sql_Mode($sql, "singleColumn", ["col" => "map_id"])[0];

                        if ($map_id){
                            $timeslot['map_id'] = $map_id;
                            $this->DB->update_Sql("tbl_deli_group_region_timeslot", "map_id", $timeslot);
                        } else {
                            $this->DB->insert_db("tbl_deli_group_region_timeslot", $timeslot);
                        }
                    }                
                }


            }
        }
        

        return $deli_group_id;
    }
//////////////////////////////////////// DELIVERY GROUP END   ///////////////////////////////////////
}
?>
