<?php
/* 
    Objective: category related
    Author: Sing
    Last major update: 03-11-2021

    Table scheme: 
        tbl_cate
        tmap_cate_greeting
        tmap_cate_product
*/


require_once dirname(__FILE__). '/abstract.class.php';
class cate extends baseClass{
    function init(){
        $this->multiLangFieldArr = ['name'];
    }

    function sortCateSubcate($parent_cate_id, $subcate_arr){
        
        $this->DB->sortMap("tbl_cate", "parent_cate_id", $parent_cate_id, "cate_id", $subcate_arr);

        return true;
    }

    function sortSubcateProduct($cate_id, $product_id_arr){
        // sorting function for the subcategory product
        // $cate_id: category which in use
        // $product_id_arr: ascending sequence base on the product_id_arr 

        $this->DB->sortMap("tmap_cate_product", "cate_id", $cate_id, "product_id", $product_id_arr);
        return $this->getCateDtl($cate_id);
    }

    function editCate($cate_id, $data){
        // edit category
        // $cate_id: category which edit. Inserting a new row when 0
        // $data: the data of that category 
        if ($cate_id == 0)
            $cate_id = $this->DB->insert_db('tbl_cate', null);
            
        $this->DB->updateMap("tmap_cate_greeting", "cate_id", $cate_id, "greeting_cate_id", $data['greeting_cate_id_arr']);
        unset($data['greeting_cate_id_arr']);

        if ($data['cate_banner'])
            $data['cate_banner'] = $this->DB->uploadImg($data['cate_banner'], "cate_banner", $cate_id);

        $sql = $this->DB->buildSql_update("tbl_cate", "cate_id", $cate_id, $data);
        $this->DB->update($sql);

        return $this->getCateDtl($cate_id);
    }

    function getCateList($shop_id){
        // get the 1st level category
        // $shop_id: shop which in use

        $prefix = $this->getPrefix($shop_id);

        $sql = "SELECT c.*, GROUP_CONCAT(cp.product_id) AS product_id_arr, count(cp.product_id) as product_count ";
        foreach($this->multiLangFieldArr AS $field){
            $sql .= ", c.$field" . "_" . $this->lang . " AS $field ";
        }
        $sql .= " FROM tbl_cate AS c
                    LEFT JOIN `tmap_cate_product` AS cp ON cp.cate_id = c.cate_id AND cp.activate = 'Y'
                    WHERE c.activate = 'Y' && c.shop_id = '$shop_id' && c.parent_cate_id IS NULL
                    GROUP BY c.cate_id";
        $cateList = $this->DB->get_Sql($sql);
        
        
        for ($i = 0 ; $i < sizeof($cateList) ; $i++){
            $lv_1st = $cateList[$i];
            $sql = "SELECT c.*, GROUP_CONCAT(cp.product_id) AS product_id_arr, count(cp.product_id) as product_count ";
            foreach($this->multiLangFieldArr AS $field){
                $sql .= ", c.$field" . "_" . $this->lang . " AS $field ";
            }
            $sql .= " FROM tbl_cate AS c 
                        LEFT JOIN `tmap_cate_product` AS cp ON cp.cate_id = c.cate_id AND cp.activate = 'Y'
                        WHERE c.activate = 'Y' && c.parent_cate_id ='". $lv_1st['cate_id'] ."'
                        GROUP BY c.cate_id";

            $cateList[$i]['subcate'] = $this->DB->get_Sql($sql);


            // $lv_1st = $cateList[$i];
            // $sql = "SELECT c.* ";
            // foreach($this->multiLangFieldArr AS $field){
            //     $sql .= ", c.$field" . "_" . $this->lang . " AS $field ";
            // }
            // $sql .= " , GROUP_CONCAT(cp.product_id) AS product_id_arr ";
            // $sql .= " FROM tbl_cate AS c
            //           LEFT JOIN `tmap_cate_product` AS cp ON cp.cate_id = c.cate_id AND cp.activate = 'Y'
            //           WHERE c.activate = 'Y' && c.parent_cate_id ='". $lv_1st['cate_id'] ."'";
        }
        
        return $cateList;
    }

    function getCateDtl($cate_id = null, $cateName = null){
        // get the category detail
        // $cate_id: category_id which in use
        
        $sql = "SELECT * ";
        foreach($this->multiLangFieldArr AS $field){
            $sql .= ", $field" . "_" . $this->lang . " AS $field ";
        }
        $sql .= " FROM tbl_cate WHERE activate ='Y' AND ";

        if ($cate_id)
            $sql .= "cate_id ='" .$this->sql_escape($cate_id) . "'";
        else 
            $sql .= "replace(replace(name_en, ' ', '_'), '&','') = '" .$this->sql_escape($cateName) . "'";
        
        $res = $this->DB->get_Sql($sql)[0];
        $cate_id = $res['cate_id'];

        if ($res['parent_cate_id']){
            $sql = "SELECT * ";
            foreach($this->multiLangFieldArr AS $field){
                $sql .= ", $field" . "_" . $this->lang . " AS $field ";
            }
            $sql .= "FROM `tbl_cate` WHERE activate = 'Y' AND cate_id ='" . $res['parent_cate_id'] . "'";
            $res['parent_cate'] =$this->DB->get_Sql($sql)[0];
        }

        if ($cate_id != 0){ 
            $sql = "SELECT c.*";
            foreach($this->multiLangFieldArr AS $field){
                $sql .= ", $field" . "_" . $this->lang . " AS $field ";
            }
            $sql .= "FROM `tbl_cate` AS c
                    WHERE c.activate = 'Y' && parent_cate_id = '$cate_id'
                    GROUP BY c.cate_id
                    ORDER BY c.sort";

            $subcateList = $this->DB->get_Sql($sql);

            foreach($subcateList as $k => $subcate){
                $sql = "SELECT COUNT(m.product_id) AS product_count
                        FROM `tmap_cate_product` AS m 
                        WHERE m.`activate` = 'Y' && m.`cate_id`=" . $subcate['cate_id'];

                $subcateList[$k]['product_count'] = $this->DB->get_One($sql);
            }

            $res['subcateList'] = $subcateList;
        }

        return $res;
    }

}
?>