<?php
class extraPHP {
	public function __construct($gthis,$g_DB) {
		$this->gthis = $gthis;
		$this->DB = $g_DB;

		require_once ROOT. '/include/apiDataBuilder.class.php';
		
		switch ($this->gthis->thirdmode) {
			case 'productList':
				$this->productList();
				break;
			default:
				new apiDataBuilder(-84);
				break;
		}

		$result_array['lang'] = $this->lang;
		@$this->gthis->assign('result_array',$result_array);
	}

	function productList(){
		if (intval($_SESSION['right']['product']) < 30){
			return array("right"=>"Failure");
		}
		require_once ROOT.'/include/class/product.class.php';
		$productCls = new product($this->DB, $this->gthis->lang);

		$filter = $this->gthis->paramBuilder([
			["key" => "skey"],
			["key" => "subcate_id"],
			["key" => "cate_id"],
			["key" => "price_min"],
			["key" => "price_max"],
		]);

		$shop_id = $this->gthis->param(["key" => "shop_id", "request" => "session"]);
		$limiter = $this->gthis->paramBuilder([
			["key" => "page"],
			["key" => "limit"]
		]);

		$sorting = $this->gthis->paramBuilder([
			["key" => "sort", "defaultValue" => "product_code"],
			["key" => "order", "order" => "ASC"]
		]);
		
		$res = $productCls->getProductList($shop_id, $filter, $limiter, $sorting);

		new apiDataBuilder(0,$res);
	}
}
?>